﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using EAGetMail;
using NUnit.Framework;

namespace GWRFramework
{
    public class ReceiveEmail
    {
        public TimeSpan startTime;
        public TimeSpan endTime;
        public TimeSpan elapsed;
        public double totalSeconds;
        public int elapsedInSecs;
        public int tCounter;
        public String exitWhile;
        public Boolean emailFlag;
        public static string ForgotPassUrl;
        public static string UnlockPassUrl;

        public Boolean CheckWelcomeEmail(String timeCounter)
        {
            
            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while(exitWhile.Equals("False"))
                {

                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        //Console.WriteLine("From: {0}", oMail.From.ToString());
                        //Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.From.ToString().Contains("Great Wolf Lodge") && oMail.Subject.Contains("Welcome from Great Wolf Lodge"))
                        {
                            emailFlag = true;
                            oClient.MarkAsRead(info, true);
                            oClient.Delete(info);
                            exitWhile = "True";
                            break;
                        }
                    }

                    //Delete mails 
                    MailInfo[] infos1 = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos1.Length; i++)
                    {
                        MailInfo info = infos[i];

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);
                        oClient.Delete(info);
                    }

                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);
                    
                    if(elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }

        public Boolean CheckForgotPassEmail(String timeCounter)
        {

            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while (exitWhile.Equals("False"))
                {
                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        //Console.WriteLine("From: {0}", oMail.From.ToString());
                        //Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.From.ToString().Contains("Great Wolf Lodge") && oMail.Subject.Contains("Forgot Your Password?"))
                        {
                            emailFlag = true;
                           
                            //oClient.MarkAsRead(info, true);
                            
                            //oClient.Delete(info);
                            exitWhile = "True";
                            break;
                        }
                    }

                    //Delete mails 
                    MailInfo[] infos1 = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos1.Length; i++)
                    {
                        MailInfo info = infos[i];

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);
                        oClient.Delete(info);
                    }

                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);

                    if (elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }

        public Boolean CheckForgotPassEmailandGetEmailLink(String timeCounter)
        {
            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while (exitWhile.Equals("False"))
                {
                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        //Console.WriteLine("From: {0}", oMail.From.ToString());
                        //Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.From.ToString().Contains("Great Wolf Lodge") && oMail.Subject.Contains("Forgot Your Password?"))
                        {
                            String RegaxPattern = "CHANGE PASSWORD(.*)";
                            String ReGax1 = "your password(.*)";
                            String ReGax2 = "qs=(.*)..";
                            emailFlag = true;

                            oClient.MarkAsRead(info, true);
                            String message = oMail.TextBody;
                            String[] matchLink = Regex.Split(message, RegaxPattern);
                            String[] Split = Regex.Split(matchLink[0], ReGax1);
                            String[] Split1 = Regex.Split(Split[2], ReGax2);
                            String defaulturl = "https://click.email.greatwolfmail.com/?qs=";
                            String urlVal = Split1[1];

                            ForgotPassUrl = defaulturl + urlVal;

                            Browser.webDriver.Navigate().GoToUrl(ForgotPassUrl);

                            oClient.Delete(info);
                            exitWhile = "True";
                            break;
                        }
                    }

                    //Delete mails 
                    MailInfo[] infos1 = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos1.Length; i++)
                    {
                        MailInfo info = infos[i];

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);
                        oClient.Delete(info);
                    }

                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);

                    if (elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }

        public Boolean CheckAcountLockEmail(String timeCounter)
        {

            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while (exitWhile.Equals("False"))
                {
                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        if (oMail.From.ToString().Contains("Great Wolf Lodge") && oMail.Subject.Contains("Suspicious Account Activity"))
                        {
                            emailFlag = true;

                            oClient.MarkAsRead(info, true);

                            oClient.Delete(info);
                            exitWhile = "True";
                            break;
                        }
                    }
                                   

                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);

                    if (elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }

        public Boolean CheckAcountLockEmailandGetUnlockLink(String timeCounter)
        {

            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while (exitWhile.Equals("False"))
                {
                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        //Console.WriteLine("From: {0}", oMail.From.ToString());
                        //Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.From.ToString().Contains("Great Wolf Lodge") && oMail.Subject.Contains("Suspicious Account Activity"))
                        {
                            String RegaxPattern = "To unlock your account please click here(.*)";
                            String ReGax1 = "Unlock Account(.*)";
                            String ReGax2 = "qs=(.*)..";
                            emailFlag = true;

                            oClient.MarkAsRead(info, true);
                            String message = oMail.TextBody;
                            String[] matchLink = Regex.Split(message, RegaxPattern);
                            String[] Split = Regex.Split(matchLink[2], ReGax1);
                            String[] Split1 = Regex.Split(Split[0], ReGax2);
                            String defaulturl = "https://click.email.greatwolfmail.com/?qs=";
                            String urlVal = Split1[1];

                            UnlockPassUrl = defaulturl + urlVal;

                            Browser.webDriver.Navigate().GoToUrl(UnlockPassUrl);

                            oClient.Delete(info);
                            exitWhile = "True";
                            break;


                            //oClient.MarkAsRead(info, true);

                            //oClient.Delete(info);                            
                        }
                    }

                    //Delete mails 
                    MailInfo[] infos1 = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos1.Length; i++)
                    {
                        MailInfo info = infos[i];

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);
                        oClient.Delete(info);
                    }

                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);

                    if (elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }

        public Boolean CheckAcountLockEmailandGetResetLink(String timeCounter)
        {

            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while (exitWhile.Equals("False"))
                {
                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                                        
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        //Console.WriteLine("From: {0}", oMail.From.ToString());
                        //Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.From.ToString().Contains("Great Wolf Lodge") && oMail.Subject.Contains("Suspicious Account Activity"))
                        {
                            String RegaxPattern = "change your password please click here(.*)";
                            String ReGax1 = "Reset Password(.*)";
                            String ReGax2 = "qs=(.*)..";
                            emailFlag = true;

                            oClient.MarkAsRead(info, true);
                            String message = oMail.TextBody;
                            String[] matchLink = Regex.Split(message, RegaxPattern);
                            String[] Split = Regex.Split(matchLink[2], ReGax1);
                            String[] Split1 = Regex.Split(Split[0], ReGax2);
                            String defaulturl = "https://click.email.greatwolfmail.com/?qs=";
                            String urlVal = Split1[1];

                            UnlockPassUrl = defaulturl + urlVal;

                            Browser.webDriver.Navigate().GoToUrl(UnlockPassUrl);

                            oClient.Delete(info);
                            exitWhile = "True";
                            break;

                            //oClient.MarkAsRead(info, true);

                            //oClient.Delete(info);                            
                        }
                    }

                    //Delete mails 
                    MailInfo[] infos1 = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos1.Length; i++)
                    {
                        MailInfo info = infos[i];

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);
                        oClient.Delete(info);
                    }

                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);

                    if (elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }

        public Boolean CheckConfirmationEmail(String timeCounter)
        {

            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while (exitWhile.Equals("False"))
                {
                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        //Console.WriteLine("From: {0}", oMail.From.ToString());
                        //Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.From.ToString().Contains("Great Wolf Lodge") && oMail.Subject.Contains("Confirmation for Reservation #"+ NewConfirmationPage.afterSplitResNum))
                        {
                            emailFlag = true;

                            oClient.MarkAsRead(info, true);
                            String message = oMail.TextBody;

                            oClient.Delete(info);
                            exitWhile = "True";
                            break;
                        }
                    }

                    //Delete mails 
                    MailInfo[] infos1 = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos1.Length; i++)
                    {
                        MailInfo info = infos[i];

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);
                        oClient.Delete(info);
                    }

                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);

                    if (elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }

        public Boolean CheckCabanaConfirmationEmail(String timeCounter)
        {

            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while (exitWhile.Equals("False"))
                {
                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        //Console.WriteLine("From: {0}", oMail.From.ToString());
                        //Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.From.ToString().Contains("Great Wolf Lodge") && oMail.Subject.Contains("Confirmation for Cabana Reservation #"))
                        {
                            emailFlag = true;

                            oClient.MarkAsRead(info, true);
                            String message = oMail.TextBody;

                            oClient.Delete(info);
                            exitWhile = "True";
                            break;
                        }
                    }

                    //Delete mails 
                    MailInfo[] infos1 = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos1.Length; i++)
                    {
                        MailInfo info = infos[i];

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);
                        oClient.Delete(info);
                    }

                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);

                    if (elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }

        public Boolean CheckAddPackageConfirmationEmail(String timeCounter)
        {

            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while (exitWhile.Equals("False"))
                {
                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        //Console.WriteLine("From: {0}", oMail.From.ToString());
                        //Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.From.ToString().Contains("Great Wolf Lodge") && oMail.Subject.Contains("Add a Package Confirmation"))
                        {
                            emailFlag = true;

                            oClient.MarkAsRead(info, true);
                            String message = oMail.TextBody;

                            oClient.Delete(info);
                            exitWhile = "True";
                            break;
                        }
                    }

                    //Delete mails 
                    MailInfo[] infos1 = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos1.Length; i++)
                    {
                        MailInfo info = infos[i];

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);
                        oClient.Delete(info);
                    }

                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);

                    if (elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }

        public Boolean CheckCostSummaryOfAddedPackages(String timeCounter)
        {
            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while (exitWhile.Equals("False"))
                {
                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        //Console.WriteLine("From: {0}", oMail.From.ToString());
                        //Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.From.ToString().Contains("Great Wolf Lodge") && oMail.Subject.Contains("Add a Package Confirmation"))
                        {
                            String RegaxPattern = "Summary of Charges(.*)";
                            String ReGax1 = "Packages";
                            String ReGax2 = "Room:(.*).";
                            emailFlag = true;
                            
                            //Suite Price ============================
                            oClient.MarkAsRead(info, true);
                            String message = oMail.TextBody;
                            String[] matchLink = Regex.Split(message, RegaxPattern);
                            String[] Split = Regex.Split(matchLink[2], ReGax1);
                            String[] Split1 = Regex.Split(Split[0], ReGax2);
                            NewConfirmationPage.EmailSuitePrice = Split1[1];

                            //Suite total =============================
                            String suiteTotalReg = "Total:(.*)";
                            String suiteTotalReg1 = "(.*).";
                            String[] matchLink1 = Regex.Split(message, suiteTotalReg);
                            String[] Split2 = Regex.Split(matchLink1[1], suiteTotalReg1);
                            NewConfirmationPage.EmailSuiteTotal = Split2[1];

                            //Package total ==============================
                            String PckgTotlReg = "Packages:(.*)";
                            String ReGax3 = "Dining Package (.*)";
                            String ReGax4 = "<br>\r";
                            String[] matchLink2 = Regex.Split(message, PckgTotlReg);
                            String[] matchLink3 = Regex.Split(matchLink2[1], ReGax3);
                            String[] matchLink4 = Regex.Split(matchLink3[1], ReGax4);
                            NewConfirmationPage.EmailPackageTotal = matchLink4[0];


                            oClient.Delete(info);
                            exitWhile = "True";
                            break;
                          
                        }
                    }

                    //Delete mails 
                    MailInfo[] infos1 = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos1.Length; i++)
                    {
                        MailInfo info = infos[i];

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);
                        oClient.Delete(info);
                    }

                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);

                    if (elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }

        public Boolean CheckCostSummaryOfAddedPackagesForCabana(String timeCounter)
        {
            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while (exitWhile.Equals("False"))
                {
                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        //Console.WriteLine("From: {0}", oMail.From.ToString());
                        //Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.From.ToString().Contains("Great Wolf Lodge") && oMail.Subject.Contains("Confirmation for Cabana Reservation #"))
                        {
                            String RegaxPattern = "TOTAL(.*)";
                            String ReGax1 = "DEPOSIT AMOUNT";
                            String ReGax2 = "(.*)\r\n";
                            emailFlag = true;

                            //Suite Price ============================
                            oClient.MarkAsRead(info, true);
                            String message = oMail.TextBody;
                            String[] matchLink = Regex.Split(message, RegaxPattern);
                            String[] Split = Regex.Split(matchLink[2], ReGax1);
                            String[] Split1 = Regex.Split(Split[0], ReGax2);
                            NewConfirmationPage.EmailSuitePrice = Split1[1];

                            //Suite total =============================
                            String suiteTotalReg = "REMAINING BALANCE(.*)";
                            String suiteTotalReg1 = "Need to change your(.*)";
                            String suiteTotalReg2 = "(.*)\r\n\r\n \r\n  \r\n\r\n \r\n\r\n\r\n \r\n  ";
                            String[] matchLink1 = Regex.Split(message, suiteTotalReg);
                            String[] Split2 = Regex.Split(matchLink1[2], suiteTotalReg1);
                            String[] Split3 = Regex.Split(Split2[0], suiteTotalReg2);
                            
                            NewConfirmationPage.EmailSuiteTotal = Split3[1];

                            oClient.Delete(info);
                            exitWhile = "True";
                            break;

                        }
                    }

                    //Delete mails 
                    MailInfo[] infos1 = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos1.Length; i++)
                    {
                        MailInfo info = infos[i];

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);
                        oClient.Delete(info);
                    }

                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);

                    if (elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }

        public Boolean DeleteEmail(String timeCounter)
        {

            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while (exitWhile.Equals("False"))
                {
                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        emailFlag = true;

                        oClient.MarkAsRead(info, true);
                      
                        oClient.Delete(info);
                        exitWhile = "True";
                        //break;                        
                    }


                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);

                    if (elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }

    }
}
