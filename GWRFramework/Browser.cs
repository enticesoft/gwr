﻿using System;
using System.Configuration;
using System.Globalization;
using System.Threading;
using GWRFramework.TestData;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;


namespace GWRFramework
{
    public class Browser
    {
        public IWebDriver WebDriver { get; set; }
        public string environmentURL { get; set; }

        public static IWebDriver webDriver;

        Actions action = new Actions(webDriver);

        private static string baseUrl = "https://www.greatwolf.com/"; //use app config for determining environment

        //Base_URL_Prod

        //public static IWebDriver webDriver = new ChromeDriver();
        //  public static IWebDriver webDriver = new InternetExplorerDriver();
        //public static IWebDriver webDriver = new FirefoxDriver();
        
        public static ChromeOptions opt = new ChromeOptions();
        



        public Browser(IWebDriver webDriver)
        {
            environmentURL = ConfigurationManager.AppSettings["Base_URL"];
            WebDriver = webDriver;
        }

        public static void Initialize()
        {

            Goto("");
        }

        public static void LaunchBrowser()
        {
            string browser = ConfigurationManager.AppSettings["Browser"];

            switch (browser)
            {
                case "Firefox":
                    webDriver = new FirefoxDriver();
                    break;
                case "Chrome":
                    webDriver = new ChromeDriver();
                    break;
                case "IE":
                    webDriver = new InternetExplorerDriver();
                    break;
                case "Edge":
                    webDriver = new EdgeDriver();
                    break;
            }

        }

        public static void NavigateTo()
        {
            webDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["Base_URL"]);
            webDriver.Manage().Window.Maximize();   
        }


        public static void NavigateToProperty()
        {
            String Property = DataAccess.getData(15, 2);
            webDriver.Navigate().GoToUrl(baseUrl+ Property);
            //webDriver.Manage().Window.Maximize();
        }

        public static void NavigateToProperty(String Property)
        {
            webDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["Base_URL"]+Property);
            //webDriver.Manage().Window.Maximize();
        }
        public static void NavigateToPropertyPlan(String Property)
        {
            webDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["Base_URL"] + Property +"/plan/");
            webDriver.Manage().Window.Maximize();
        }

        public static string Title
        {
            get { return webDriver.Title; }
        }

        public static string CurrentURL
        {
            get { return webDriver.Url; }
        }

        public static ISearchContext Driver
        {
            get { return webDriver; }
        }

        public static IWebDriver _Driver
        {
            get { return webDriver; }
        }

        public static IWebDriver driver
        {
            get { return webDriver; }
        }

        public static void Goto(string url)
        {
            webDriver.Url = baseUrl + url;
        }

        public static void Close()
        {
            webDriver.Close();
            webDriver.Quit();
            webDriver.Dispose();
        }

        public static void waitForImplicitTime(int waitTime)
        {
            //IWebDriver driver = new ChromeDriver();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(waitTime);
        }

        public static void waitForElementToBeClickable(IWebElement element, int waitTime)
        {           
            WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(waitTime));
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public static void waitForElementToBeInvisible(int waitTime, String xpath)
        {
            WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(waitTime));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath(xpath)));
        }

        public static void waitForElementToDisplayed(IWebElement element, int waitTime)
        {
            for (int i = 0; i < waitTime; i++)
            {
                if(element.Exists())
                {
                    break;
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }

        }

        public static string getText(IWebElement element)
        {
            var text = element.Text;
            return text;
        }

        public static string getTextboxValue(IWebElement element)
        {
            var text = element.GetAttribute("value");
            return text;
        }

        public static void clearTextboxValue(IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            js.ExecuteScript("arguments[0].value='';", element);
        }

        public static void javaScriptClick(IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            js.ExecuteScript("arguments[0].click();", element);
        }

        public static void setAttributeValue(IWebElement element, String conVal, String setVal)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            js.ExecuteScript("arguments[0].removeAttribute('disabled')", element);
            element.GetAttribute("value").Contains(conVal);
            js.ExecuteScript("arguments[0].value.display = '4'", element);
        }

        public static void clearTextboxValueACtion(IWebElement element)
        {
            String val = element.GetAttribute("value");
            if (val != null)
            {
                element.Click();
                element.SendKeys(Keys.Control + "a");
                element.SendKeys(Keys.Delete);

            }
        }

        public static void alert()
        {
            IAlert simpleAlert = webDriver.SwitchTo().Alert();
            simpleAlert.Accept();
        }

        public static void scrollToAnElement(IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            js.ExecuteScript("arguments[0].scrollIntoView();", element);
        }

        public static void scrollDownToBottomOfPage()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
            Thread.Sleep(2000);
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
        }

        public static double stringToDoubleConvert(IWebElement element)
        {
            waitForElementToDisplayed(element, 10);

            String value = getText(element);

            value = value.Substring(1);

            double convertVal = Convert.ToDouble(value);

            return convertVal;
        }

        public static decimal stringToDecimalConvert(IWebElement element)
        {
            waitForElementToDisplayed(element, 10);

            String value = getText(element);

            value = value.Substring(1);

            decimal convertVal = Convert.ToDecimal(value);

            return convertVal;
        }

        public static string convertToCamleCase(String convertSting)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            String CamlTitle = textInfo.ToTitleCase(convertSting.ToLower());
            return CamlTitle;
        }

        public static double stringToDoubleConvert1(IWebElement element, int n)
        {
            waitForElementToDisplayed(element, 10);
            String value = getText(element);
            //  int n = 3;
            value = value.Substring(n);
            double convertVal = Convert.ToDouble(value);
            return convertVal;
        }

        public static decimal stringToDecimalConvert1(IWebElement element, int n)
        {
            waitForElementToDisplayed(element, 10);
            String value = getText(element);
            //  int n = 3;
            value = value.Substring(n);
            decimal convertVal = Convert.ToDecimal(value);
            return convertVal;
        }

        public static string getSpecificCharactersFromString(IWebElement element, int n1, int n2)
        {
            waitForElementToDisplayed(element, 30);
            var text = element.GetAttribute("value");
            text = text.Substring(n1, n2);
            return text;
        }

        public static int stringToIntConvert(IWebElement element)
        {
            int convertVal = Convert.ToInt32(element);
            return convertVal;
        }

        public static void SwitchToNewWindow()
        {
            var newWindow = driver.WindowHandles[1];
            driver.SwitchTo().Window(driver.WindowHandles[1]);
        }

        public static void CloseNewWindow()
        {
            var newWindow = driver.WindowHandles[1];
            driver.SwitchTo().Window(driver.WindowHandles[1]).Close();
        }

        public static void SwitchToParentWindow()
        {
            var parentWindow = driver.WindowHandles[0];
            driver.SwitchTo().Window(driver.WindowHandles[0]);
        }

        public static void scrollVerticalBy(string scrollLength)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            js.ExecuteScript("window.scrollBy(0," + scrollLength + ")");
        }

        public static void SwitchToFrameWindow(IWebElement element)
        {
            driver.SwitchTo().Frame(element);
        }

    }
}