using System;

namespace GWRFramework
{
    public class EmailAddressGenerator
    {
        public string emailId;

        public string GenerateEmail(string mainEmail)
        {
            string[] emailComp = mainEmail.Split('@');

            Random r = new Random();
            int rInt = r.Next(1, 99999);

            emailId = emailComp[0] + "+0" + rInt + "@" + emailComp[1];

            return emailId;
        }
    }
}