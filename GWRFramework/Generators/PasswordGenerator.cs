using System;
using System.Text;

namespace GWRFramework.Generators
{
    public class PasswordGenerator
    {


        //implement methods to generate *randomess* or for unique validation test cases

        private static bool toggle = true;
        public string Generate()
        {
            var password = "";
            password = toggle ? "Password" : "New Password";

            toggle = !toggle;
            LastGeneratedPassword = password;

            //The following code generates a password of length 11 with first 4 letters lowercase, next 4 letters numbers, and last 2 letters as uppercase and 1 special character.
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(4, true));
            builder.Append(RandomNumber(1000, 9999));
            builder.Append(RandomString(2, false));
            password = builder.ToString() + "$";

            return password;
        }

        public static string LastGeneratedPassword { get; set; }

        // Generate a random number between two numbers  
        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        // Generate a random string with a given size  
        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

    }
}