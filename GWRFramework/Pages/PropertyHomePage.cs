﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Linq;
using System.Threading;

namespace GWRFramework
{
    public class PropertyHomePage
    {
        [FindsBy(How = How.Id, Using = "checkInDateSelection")]
        public IWebElement checkInDateTextbox;

        [FindsBy(How = How.Id, Using = "checkOutDateSelection")]
        public IWebElement checkOutDateTextbox;

        [FindsBy(How = How.XPath, Using = "//button[@aria-label='Booking Widget Submit Button']")]
        public IWebElement bookNowButton;

        [FindsBy(How = How.XPath, Using = "//*[@id='sign-in-create-account']/div/span[contains(text(), 'Sign In')]")]
        public IWebElement signInLink;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(), 'Sign Up for Exclusive Deals!')]|//div[contains(text(), 'Sign up for exclusive deals!')]")]
        public IWebElement dealsSignUpLabel;

        [FindsBy(How = How.XPath, Using = "//button[@title='Close']")]
        public IWebElement dealsSignUpCloseBtn;

        [FindsBy(How = How.XPath, Using = "//*[@id='sign-in-create-account']/descendant::summary[contains(text(), 'Hello')]")]
        public IWebElement userSignedInMenu;

        [FindsBy(How = How.XPath, Using = "//p[contains(text(),'Please enter valid user name or password.')]")]
        public IWebElement LoginFailedValMsg;

        [FindsBy(How = How.XPath, Using = "//*[@id='sign-in-create-account']/descendant::a[contains(text(), 'Sign Out')]")]
        public IWebElement signOutLink;

        [FindsBy(How = How.XPath, Using = "(//div[@class='room-card__card']//a[@class='button'])[1]")]
        public IWebElement bookNowBtnOnRoomCard;

        [FindsBy(How = How.XPath, Using = "//h3[@class='clickable-tiles__content__card-header-title'][contains(.,'Customer Service')]")]
        public IWebElement customerServiceClickableTile;

        [FindsBy(How = How.XPath, Using = "//h1[contains(translate(.,'CUSTOMER SERVICE','Customer Service'), search)]")]
        public IWebElement customerServiceLabel;

        [FindsBy(How = How.Id, Using = "minimap__iframe")]
        public IWebElement miniMapIFrame;

        [FindsBy(How = How.XPath, Using = "//div[@class='minimap__iframe-container']")]
        public IWebElement miniMapContainer;

        [FindsBy(How = How.Id, Using = "scene")]
        public IWebElement googleMap;

        [FindsBy(How = How.XPath, Using = "//a[@class='hero-banner__link']")]
        public IWebElement viewOfferLink;

        [FindsBy(How = How.XPath, Using = "//h3[@class='clickable-tiles__content__card-header-title'][contains(.,'Day Passes')]")]
        public IWebElement dayPassTile;

        [FindsBy(How = How.Id, Using = "zip")]
        public IWebElement CanadianPostalCode;

        [FindsBy(How = How.XPath, Using = "//input[@id='chk-consent']//parent::div")]
        public IWebElement CheckboxForCanadianPostalCode;

        [FindsBy(How = How.XPath, Using = "//input[@id='chk-consent']")]
        public IWebElement CheckboxForCanadianPostalCode1;

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Book Your Stay')]")]
        public IWebElement BookYourStayLink;

        [FindsBy(How = How.XPath, Using = "//div[contains(@aria-label,'Start and End Dates')]//ancestor::form")]
        public IWebElement PlanYourStayPage;

        [FindsBy(How = How.XPath, Using = "//a[@class='local']")]
        public IWebElement PropertyDropdown;

        String property1 = "//ul[@class='menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active']//a[contains(text(), '";
        String property2 = "')]";

        String property3 = "//*[contains(@aria-label, '";
        String property4 = "')]";

        [FindsBy(How = How.XPath, Using = "//li[@class='first']//a[contains(text(),'Customer Service')]")]
        public IWebElement CustomerServiceOnHeader;

        [FindsBy(How = How.XPath, Using = "//div[@class='hero-banner__wrapper']")]
        public IWebElement HeroBannerSection;

        [FindsBy(How = How.XPath, Using = "(//div[@class='room-card'])[1]")]
        public IWebElement RoomCardrSection;

        [FindsBy(How = How.XPath, Using = "//div[@class='amenities__container']")]
        public IWebElement AminitiesSection;

        [FindsBy(How = How.XPath, Using = "//div[@class='clickable-tiles__content']")]
        public IWebElement clicableTileSection;

        [FindsBy(How = How.XPath, Using = "//h3[@class='clickable-tiles__content__card-header-title'][contains(.,'Dining')]")]
        public IWebElement diningClickbleTile;

        [FindsBy(How = How.XPath, Using = "//h3[@class='clickable-tiles__content__card-header-title'][contains(.,'Attraction')]")]
        public IWebElement attractionClickbleTile;

        [FindsBy(How = How.XPath, Using = "//h3[@class='clickable-tiles__content__card-header-title'][contains(.,'Activities')]")]
        public IWebElement activityClickbleTile;

        [FindsBy(How = How.XPath, Using = "//div[@class='about-section__container ']")]
        public IWebElement AboutSection;

        [FindsBy(How = How.XPath, Using = "//div[@class='gallery-container']")]
        public IWebElement GallerySection;

        [FindsBy(How = How.XPath, Using = "//div[@class='deals-card__card']")]
        public IWebElement DealSection;

        [FindsBy(How = How.XPath, Using = "//div[@class='deals-card__cta-section cta-button']//a[@class='button']")]
        public IWebElement StarSavingBtn;

        [FindsBy(How = How.XPath, Using = "//h1[@class='details-venue__title']|//h1[@class='header-subheader__header ']")]
        public IWebElement SpecialOffertitle;

        [FindsBy(How = How.XPath, Using = "//div[@class='header-container__logo']//a")]
        public IWebElement GWRTopLogo;

        [FindsBy(How = How.XPath, Using = "//span[@class='waterpark-hours__title']")]
        public IWebElement waterParkHours;

        [FindsBy(How = How.XPath, Using = "//div[@class='global-deals__bar']")]
        public IWebElement globalDeallinkOnHomePage;

        [FindsBy(How = How.XPath, Using = "//div[@class='global-deals__icon']")]
        public IWebElement globalDealCloselinkOnHomePage;

        [FindsBy(How = How.XPath, Using = "//div[@class='global-deals__content collapsed']")]
        public IWebElement globalDealCollapseOnHomePage;

        [FindsBy(How = How.XPath, Using = "(//div[@class='global-deals-list__card'][@style='display: flex;'])[1]")]
        public IWebElement availableDealsInGridBar;

        [FindsBy(How = How.XPath, Using = "(//div[@class='global-deals-list__card'][@style='display: flex;']//h3)[1]")]
        public IWebElement GlobalDealTitle;

        [FindsBy(How = How.XPath, Using = "(//div[@class='global-deals-list__card'][@style='display: flex;']//a[@class='global-deals-list__card-options--apply-promo-code'])[1]")]
        public IWebElement applyOfferCodeBtn;

        [FindsBy(How = How.XPath, Using = "(//div[@class='global-deals-list__card'][@style='display: flex;']//a[@class='global-deals-list__card-options--apply-promo-code'])[1]//following-sibling::p")]
        public IWebElement OfferCodeValue;

        [FindsBy(How = How.XPath, Using = "//input[@name='offerCode']")]
        public IWebElement OfferCodeValueOnPlan;

        [FindsBy(How = How.XPath, Using = "(//div[@class='global-deals-list__card'][@style='display: flex;']//div[@class='global-deals-list__is-exclusive']//p)[1]//parent::div//following-sibling::div//a[@class='global-deals-list__card-options--sign-up']")]
        public IWebElement ExclvDealSignUpNowBtn;

        [FindsBy(How = How.XPath, Using = "(//div[@class='global-deals-list__card'][@style='display: flex;']//div[@class='global-deals-list__is-exclusive']//p)[1]//parent::div//following-sibling::div//a[@class='global-deals-list__card-options--sign-in']")]
        public IWebElement ExclvDealSignInlink;

        [FindsBy(How = How.XPath, Using = "(//div[@class='global-deals-list__card'][@style='display: flex;']//div[@class='global-deals-list__is-exclusive']//p)[1]//parent::div//following-sibling::div//a[@class='global-deals-list__card-options--apply-promo-code']")]
        public IWebElement ExclvDealApplyOferCdBtn;

        [FindsBy(How = How.XPath, Using = "(//div[@class='global-deals-list__card'][@style='display: flex;']//div[@class='global-deals-list__is-exclusive']//p)[1]//parent::div//following-sibling::div//p[@class='global-deals-list__card-promo-code']")]
        public IWebElement ExclvDealOferCd;

        [FindsBy(How = How.XPath, Using = "//div[@id='global-deals__see-all-offers']//a")]
        public IWebElement seeAllOfferDetailLink;
        

        public static Boolean dealCompoOnHome = false;
        public static string GbDealofferCd;

        
        public Boolean specialOfferPageT2()
        {
            if (dealCompoOnHome == true)
            {
                return Pages.Deals.specialOffersLabel.Exists();
            }
            else { return true; }

        }

        public void seeOfferDetaiLink()
        {
            if (dealCompoOnHome == true)
            {
                Browser.waitForElementToBeClickable(seeAllOfferDetailLink, 10);
                seeAllOfferDetailLink.Click();
            }
        }

        public Boolean SignInPopUpOpen()
        {
            if (dealCompoOnHome == true)
            {
                Browser.waitForElementToBeClickable(Pages.SignIn.emailtxt, 20);
                return Pages.SignIn.emailtxt.Exists();
            }
            else { return true; }

        }

        public Boolean CreateAcPopOpen()
        {
            if (dealCompoOnHome == true)
            {
                Browser.waitForElementToBeClickable(Pages.DealSignUp.firstNameTextbox, 20);
                return Pages.DealSignUp.firstNameTextbox.Exists();
            }
            else { return true; }
                
        }

        public void clickOnExclDealSignUpBtn()
        {
            if (dealCompoOnHome == true)
            {
                Browser.waitForElementToBeClickable(ExclvDealSignUpNowBtn, 20);
                Browser.javaScriptClick(ExclvDealSignUpNowBtn);
                //ExclvDealSignUpNowBtn.Click();
            }            
        }

        public void clickOnExclDealSignInBtn()
        {
            if (dealCompoOnHome == true)
            {
                Browser.waitForElementToBeClickable(ExclvDealSignInlink, 20);
                Browser.javaScriptClick(ExclvDealSignInlink);
                //ExclvDealSignUpNowBtn.Click();
            }
        }

        public Boolean verifyExclDealOfferCode()
        {
            if (dealCompoOnHome == true)
            {
                return ExclvDealOferCd.Exists();
            }
            else { return true; }
        }

        public Boolean verifyExclDealApplyCodeBtn()
        {
            Browser.waitForElementToDisplayed(userSignedInMenu, 80);
            if (dealCompoOnHome == true)
            {
                return ExclvDealApplyOferCdBtn.Exists();
            }
            else { return true; }
        }

        public Boolean verifyExclDealSignUpBtn()
        {
            if (dealCompoOnHome == true)
            {
                return ExclvDealSignUpNowBtn.Exists();
            }
            else { return true; }
        }

        public Boolean verifyExclDealSignInLink()
        {
            if (dealCompoOnHome == true)
            {
                return ExclvDealSignInlink.Exists();
            }
            else { return true; }
        }

        public Boolean verifyOffrCdOnPlanPage()
        {
            if (dealCompoOnHome == true)
            {
                Browser.waitForElementToBeClickable(OfferCodeValueOnPlan, 10);
                String planOdfCd = OfferCodeValueOnPlan.GetAttribute("value");
                return planOdfCd.Equals(GbDealofferCd);
            }
            else { return true; }
        }

        public void clickOnApplyCodeBtn()
        {
            if (dealCompoOnHome == true)
            {
                Browser.waitForElementToBeClickable(applyOfferCodeBtn, 10);
                GbDealofferCd = OfferCodeValue.Text;
                applyOfferCodeBtn.Click();
            }
        }

        public void clickOnGloblDealTitle()
        {
            if (dealCompoOnHome == true)
            {
                Browser.waitForElementToBeClickable(GlobalDealTitle, 10);
                GlobalDealTitle.Click();
            }
        }

        public Boolean globalDealsisDisplayedOnHomePage()
        {
            if (dealCompoOnHome == true)
            {
                return availableDealsInGridBar.Exists();
            }
            else { return true; }
        }

        public Boolean globalDealOnHomePage()
        {
            Browser.waitForElementToDisplayed(globalDeallinkOnHomePage, 8);
            if (globalDeallinkOnHomePage.Exists())
            {
                bool result = globalDeallinkOnHomePage.Displayed;
                return result;
            }
            else { return true; }
        }        

        public void clickOnGlobalDealBarOnHomePage()
        {
            Browser.waitForElementToDisplayed(globalDeallinkOnHomePage, 8);
            if (globalDeallinkOnHomePage.Exists())
            {
                dealCompoOnHome = true;
                globalDeallinkOnHomePage.Click();
            }
            else
            {
                dealCompoOnHome = false;
            }
        }

        public Boolean dealbarIsCollapse()
        {
            if (dealCompoOnHome == true)
            {
                bool result = globalDealCollapseOnHomePage.Exists();
                return result;
            }
            else { return true; }
        }
        
        public void clickOnCloseDealGridCompnt()
        {
            if(dealCompoOnHome == true)
            {
                Browser.waitForElementToBeClickable(globalDealCloselinkOnHomePage, 8);
                globalDealCloselinkOnHomePage.Click();
            }
        }

        public void clickOnGWRLogoBtn()
        {
            Browser.waitForElementToBeClickable(GWRTopLogo, 30);
            GWRTopLogo.Click();
        }

        public Boolean specialOfferTitleDisplayed()
        {
            Browser.waitForElementToDisplayed(SpecialOffertitle, 10);
            bool specialOffer = SpecialOffertitle.Displayed;
            return specialOffer;
        }

        public void clickOnStartSavingBtn()
        {
            Browser.scrollToAnElement(StarSavingBtn);
            //Browser.scrollVerticalBy("900");
            Browser.waitForElementToBeClickable(StarSavingBtn, 30);
            Browser.javaScriptClick(StarSavingBtn);
            //StarSavingBtn.Click();
        }

        public Boolean propertyHomePageDealSecDisplayed()
        {
            Browser.waitForElementToDisplayed(DealSection, 10);
            bool DealSec = DealSection.Displayed;
            return DealSec;
        }

        public Boolean propertyHomePageGalleryDisplayed()
        {
            Browser.waitForElementToDisplayed(GallerySection, 10);
            bool GallerySec = GallerySection.Displayed;
            return GallerySec;
        }

        public Boolean propertyHomePageWaterParkHrsDisplayed()
        {
            Browser.waitForElementToDisplayed(waterParkHours, 10);
            bool waterParkhrs = waterParkHours.Displayed;
            return waterParkhrs;
        }
        

        public Boolean propertyHomePageAboutDisplayed()
        {
            Browser.waitForElementToDisplayed(AboutSection, 10);
            bool AboutSec = AboutSection.Displayed;
            return AboutSec;
        }

        public Boolean propertyHomePageRoomCardDisplayed()
        {
            Browser.waitForElementToDisplayed(RoomCardrSection, 10);
            bool RoomCard = RoomCardrSection.Displayed;
            return RoomCard;
        }

        public Boolean propertyHomePageAminitiesSection()
        {
            Browser.waitForElementToDisplayed(AminitiesSection, 10);
            bool Aminities = AminitiesSection.Displayed;
            return Aminities;            
        }

        public Boolean propertyHomePageclicableTileSection()
        {
            Browser.waitForElementToDisplayed(clicableTileSection, 10);
            bool clicbleTile = clicableTileSection.Displayed;
            return clicbleTile;
        }

        public Boolean propertyHomePageDiningClickbleTile()
        {
            Browser.waitForElementToDisplayed(diningClickbleTile, 10);
            if (diningClickbleTile.Exists())
            {
                Browser.javaScriptClick(diningClickbleTile);
                Browser.waitForElementToBeClickable(Pages.DiningAndShoppingPage.DiningHeader, 10);
                if (Pages.DiningAndShoppingPage.DiningHeader.Exists())
                {
                    return true;
                }
                else {
                    return false;
                }                
            }
            else {
                return true;
            }         
          
        }

        public Boolean propertyHomePageAttractionClickbleTile()
        {
            Browser.waitForElementToDisplayed(attractionClickbleTile, 10);
            if (attractionClickbleTile.Exists())
            {
                Browser.javaScriptClick(attractionClickbleTile);
                Browser.waitForElementToBeClickable(Pages.WPAPage.AttractionsPageHeader, 10);
                if (Pages.WPAPage.AttractionsPageHeader.Exists())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        }

        public Boolean propertyHomePageActivityClickbleTile()
        {
            Browser.waitForElementToDisplayed(activityClickbleTile, 10);
            if (activityClickbleTile.Exists())
            {
                Browser.javaScriptClick(activityClickbleTile);
                Browser.waitForElementToBeClickable(Pages.WPAPage.ActivitiesPageHeader, 10);
                if (Pages.WPAPage.ActivitiesPageHeader.Exists())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }


        public Boolean propertyHomePageHeroBannerDisplayed()
        {
            Browser.waitForElementToDisplayed(HeroBannerSection, 10);
            bool HeroBanner = HeroBannerSection.Displayed;
            return HeroBanner;
        }

        public Boolean propertyHomePageDisplayed()
        {
            Browser.waitForElementToDisplayed(checkInDateTextbox, 10);
            bool CheckInDate = checkInDateTextbox.Displayed;
            return CheckInDate;            
        }

        public Boolean checkBookingWidgetDisplayed()
        {
            Browser.waitForElementToDisplayed(checkInDateTextbox, 20);
            bool CheckInDate = checkInDateTextbox.Displayed;
            return CheckInDate;
        }

        public Boolean checkCustomerServicePage()
        {
            Browser.waitForElementToDisplayed(customerServiceLabel, 20);
            bool CustomerService = customerServiceLabel.Displayed;
            return CustomerService;
        }

        public void SignInToAppBeforeCheckLogOut(String Email, String Pass) 
        {
            Browser.waitForElementToBeClickable(Pages.Suite.checkInDate, 20);
            if (signInLink.Exists() == false)
            {
                Pages.PropertyHome.closeDealsSignUp();
                Pages.PropertyHome.clickToSignOut();
            }

            Pages.PropertyHome.closeDealsSignUp();
            Browser.waitForElementToBeClickable(signInLink, 20);
            signInLink.Click();

            Pages.SignIn.sendEmail(Email);

            Browser.waitForElementToDisplayed(Pages.SignIn.passwordtxt, 10);
            Pages.SignIn.passwordtxt.SendKeys(Pass);

            Pages.SignIn.clickSignInbtn();
            //Thread.Sleep(3000);

        }

        public void InvalidAttmpt5SignInToAppBeforeCheckLogOut(String Email, String Pass)
        {
            Browser.waitForElementToBeClickable(Pages.Suite.checkInDate, 20);
            if (signInLink.Exists() == false)
            {
                Pages.PropertyHome.closeDealsSignUp();
                Pages.PropertyHome.clickToSignOut();
            }

            Pages.PropertyHome.closeDealsSignUp();
            Browser.waitForElementToBeClickable(signInLink, 20);
            signInLink.Click();

            Pages.SignIn.sendEmail(Email);

            Browser.waitForElementToDisplayed(Pages.SignIn.passwordtxt, 10);
            Pages.SignIn.passwordtxt.SendKeys(Pass);

            for (int i = 1; i <= 5; i++)
            {
                Pages.SignIn.clickSignInbtn();
                Thread.Sleep(1000);
            }


        }

        public void SignInToAppWithNewPass(String Email, String Pass)
        {            
            Browser.waitForElementToBeClickable(signInLink, 20);
            signInLink.Click();

            Browser.waitForElementToDisplayed(Pages.SignIn.passwordtxt, 10);
            Pages.SignIn.sendEmail(Email);

            //Browser.waitForElementToDisplayed(Pages.SignIn.passwordtxt, 10);
            Pages.SignIn.passwordtxt.SendKeys(Pass);

            Pages.SignIn.clickSignInbtn();
            //Thread.Sleep(3000);

        }

        public void SignInToAppBeforeCheckLogOutfromOtherpage(String Email, String Pass)
        {
            //Browser.waitForElementToBeClickable(Pages.Suite.checkInDate, 90);
            Browser.waitForImplicitTime(15);
            if (signInLink.Exists() == false)
            {
                Pages.PropertyHome.closeDealsSignUp();
                Pages.PropertyHome.clickToSignOut();
            }

            //Pages.PropertyHome.closeDealsSignUp();
            Browser.waitForElementToBeClickable(signInLink, 20);
            signInLink.Click();

            //Browser.waitForElementToDisplayed(Pages.SignIn.passwordtxt, 10);
            Pages.SignIn.sendEmail(Email);

            Browser.waitForElementToDisplayed(Pages.SignIn.passwordtxt, 10);
            Pages.SignIn.passwordtxt.SendKeys(Pass);

            Pages.SignIn.clickSignInbtn();

        }

        public void clickSignIn()
        {
            Browser.waitForElementToBeClickable(signInLink, 30);
            signInLink.Click();
        }

        public void waitForDealsSignUpPopup()
        {
            Browser.waitForElementToDisplayed(dealsSignUpLabel, 50);
        }
       
        public Boolean IObserveLeadGenPopUp()
        {
            Browser.waitForElementToDisplayed(dealsSignUpLabel, 20);
            return dealsSignUpLabel.Exists();
        }

        public void closeDealsSignUp()
        {
            if(dealsSignUpLabel.Exists())
            {
                Browser.webDriver.Navigate().Refresh();
            }
        }
        public Boolean shouldNotSeeDealsSignUpPopUp()
        {
            if (dealsSignUpLabel.Exists())
            {
                return false;
            }
            else {
                return true;
            }
        }

        public Boolean checkUserSingedInMenu()
        {
            Browser.waitForElementToDisplayed(userSignedInMenu, 30);
            return userSignedInMenu.Exists();
        }

        public Boolean loginFailedValMessage()
        {
            Browser.waitForElementToDisplayed(LoginFailedValMsg, 20);
            return LoginFailedValMsg.Exists();
        }

        public void clickToSignOut()
        {
            Browser.waitForElementToDisplayed(userSignedInMenu, 20);
            Browser.waitForElementToBeClickable(userSignedInMenu, 20);
            userSignedInMenu.Click();

            Browser.waitForImplicitTime(10);
            Browser.waitForElementToDisplayed(signOutLink, 20);
            Browser.waitForElementToBeClickable(signOutLink, 20);
            signOutLink.Click();            
        }

        public void clickBookNowOnRoomCard()
        {
            Browser.waitForElementToBeClickable(bookNowBtnOnRoomCard, 20);
            Browser.scrollToAnElement(bookNowBtnOnRoomCard);
            Browser.javaScriptClick(bookNowBtnOnRoomCard);
            //bookNowBtnOnRoomCard.Click();
        }

        public void clickOnCustomerService()
        {
            Browser.waitForElementToBeClickable(customerServiceClickableTile, 20);
            Browser.javaScriptClick(customerServiceClickableTile);
            //customerServiceClickableTile.Click();
        }

        public void clickOnGetDirections()
        {
            //Browser.scrollToAnElement(miniMapIFrame);

            //Browser.webDriver.SwitchTo().Frame(miniMapIFrame);

            //Browser.waitForElementToBeClickable(getDirectionsLink, 10);
            //getDirectionsLink.Click();
        }

        public Boolean checkGoogleMapInNewTab()
        {
            //switch to new window.
            //Browser.webDriver.SwitchTo().Window(Browser.webDriver.WindowHandles.Last());

            Browser.waitForElementToDisplayed(miniMapContainer, 10);
            bool gMap = miniMapContainer.Displayed;

            //Browser.webDriver.Close();

            //switch back to first window
            //Browser.webDriver.SwitchTo().Window(Browser.webDriver.WindowHandles.First());

            return gMap;
        }

        public void clearCheckInDate()
        {
            Browser.waitForElementToDisplayed(checkInDateTextbox, 10);
            Browser.clearTextboxValue(checkInDateTextbox);
        }

        public void enterCheckInDate(String checkInDate)
        {
            Pages.PropertyHome.closeDealsSignUp();
            Browser.waitForElementToDisplayed(checkInDateTextbox, 20);
            checkInDateTextbox.Clear();
            checkInDateTextbox.SendKeys(checkInDate);
        }

        public void clearCheckOutDate()
        {
            Pages.PropertyHome.closeDealsSignUp();
            Browser.waitForElementToDisplayed(checkOutDateTextbox, 20);
            Browser.clearTextboxValue(checkOutDateTextbox);
        }

        public void enterCheckOutDate(String checkOutDate)
        {
            Pages.PropertyHome.closeDealsSignUp();
            Browser.waitForElementToDisplayed(checkOutDateTextbox, 20);
            checkOutDateTextbox.SendKeys(checkOutDate);
        }

        public void scrollToBookingWidget()
        {
            Browser.waitForImplicitTime(3);
            Browser.scrollToAnElement(checkInDateTextbox);
        }

        public void clickViewOfferLink()
        {
            Browser.waitForElementToBeClickable(viewOfferLink, 20);
            Browser.javaScriptClick(viewOfferLink);
        }

        public void clickBookNowBtn()
        {   

            Browser.waitForElementToBeClickable(bookNowButton, 20);
            Browser.javaScriptClick(bookNowButton);
            //bookNowButton.Click();
        }

        public static Boolean dayPassAvlblHome;

        public void clickOnDayPassTile()
        {
            if (dayPassTile.Exists())
            {
                Browser.waitForElementToBeClickable(dayPassTile, 20);
                Browser.javaScriptClick(dayPassTile);
                //dayPassTile.Click();
                dayPassAvlblHome = true;
            }
            else { dayPassAvlblHome = false; }
        }

        public void EnterPostalCode(String PostalCode)
        {
            Browser.waitForElementToBeClickable(CanadianPostalCode, 20);
            CanadianPostalCode.SendKeys(PostalCode);
        }
        public void CheckCheckbox()
        {
            Browser.waitForElementToDisplayed(CheckboxForCanadianPostalCode, 20);
            Browser.waitForElementToBeClickable(CheckboxForCanadianPostalCode, 20);
            Boolean checkbox = CheckboxForCanadianPostalCode.Selected;
            if (checkbox.Equals(false))
            {
                CheckboxForCanadianPostalCode.Click();

            }
            else
            {
                CheckboxForCanadianPostalCode.Click();
                CheckboxForCanadianPostalCode.Click();
            }

        }
        public Boolean IsCheckBoxChecked()
        {
            Browser.waitForElementToDisplayed(CheckboxForCanadianPostalCode, 20);
            String value = CheckboxForCanadianPostalCode.GetCssValue("background-image");
            if (value == "none")
            {
                return false;
            }
            else { return true; }
        }

        public void UncheckCheckbox()
        {
            Browser.waitForElementToDisplayed(CheckboxForCanadianPostalCode, 20);
            Browser.waitForElementToBeClickable(CheckboxForCanadianPostalCode, 20);
            Boolean checkbox = CheckboxForCanadianPostalCode.Selected;
            if (checkbox.Equals(false))
            {
                CheckboxForCanadianPostalCode.Click();
                CheckboxForCanadianPostalCode.Click();
            }
            else
            {
                CheckboxForCanadianPostalCode.Click();
            }

        }
        public Boolean IsCheckBoxUnchecked()
        {
            Browser.waitForElementToDisplayed(CheckboxForCanadianPostalCode, 20);
            return CheckboxForCanadianPostalCode.Selected;
        }

        public void clickOnCustomerServiceFromHeader()
        {
            Browser.waitForElementToBeClickable(CustomerServiceOnHeader, 20);
            CustomerServiceOnHeader.Click();
        }

        public void ClickOnBookYourStay()
        {
            Browser.waitForElementToBeClickable(BookYourStayLink, 20);
            BookYourStayLink.Click();
        }

        public Boolean IShouldNavigateOnPlanYourStaySection()
        {
            Browser.waitForElementToBeClickable(PlanYourStayPage, 20);
            bool IsPlanYourStayDisplayed = PlanYourStayPage.Displayed;
            return IsPlanYourStayDisplayed;
        }

        public void ClickOnPropertyDropdown()
        {
            Browser.waitForElementToBeClickable(PropertyDropdown, 20);
            PropertyDropdown.Click();

        }

        public void ClickOnAnyPropertyLink(String propertyName)
        {

            IWebElement propertyLink = Browser.driver.FindElement(By.XPath(property1 + propertyName + property2));
            Browser.waitForElementToDisplayed(propertyLink, 20);
            Browser.javaScriptClick(propertyLink);
        }

        public Boolean IShouldNavigateToPerticularPropertyPage(String propertyName)
        {
            IWebElement propertyLink = Browser.driver.FindElement(By.XPath(property3 + propertyName + property4));
            Browser.waitForElementToDisplayed(propertyLink, 20);
            return propertyLink.Displayed;
        }
    }
}
