﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using MongoDB.Driver.Core.Operations;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;




namespace GWRFramework
{
   public class ForgotPasswordPage
    {
        [FindsBy(How = How.XPath, Using = "//div[@label='Email Address']//following-sibling::span[text()='Forgot password?']")]
        public IWebElement ForgotPasswordLink;

        [FindsBy(How = How.XPath, Using = "//input[@id='password']")]
        public IWebElement NewPassField;

        [FindsBy(How = How.XPath, Using = "//button[@type='submit'][text()='Change Password']")]
        public IWebElement ChangePassBtn;

        [FindsBy(How = How.XPath, Using = "//button[@type='submit'][text()='Continue']")]
        public IWebElement ContinueBtn;

        [FindsBy(How = How.XPath, Using = "//button[@type='submit'][contains(text(),'Reset Password')]")]
        public IWebElement ResetPassword;

        [FindsBy(How = How.XPath, Using = "//input[@id='password']")]
        public IWebElement PasswordTextBox;

        
        [FindsBy(How = How.XPath, Using = "//button[@type='submit']")]
        public IWebElement ChangePasswordBtn;
                
        
        public void IClickForgotPasswordLnk()
        {
            Actions action = new Actions(Browser.driver);
            action.MoveToElement(ForgotPasswordLink).Build().Perform();
            //Browser.waitForElementToBeClickable(ForgotPasswordLink, 20);
            ForgotPasswordLink.Click();
            Thread.Sleep(3000);
        }

        public void IEnterNewPass(String NewPass)
        {
            Browser.waitForElementToDisplayed(NewPassField, 20);
            NewPassField.SendKeys(NewPass);
        }

        public void ClickChangePasswordBtn()
        {
            Browser.waitForElementToBeClickable(ChangePassBtn, 20);
            ChangePassBtn.Click();
            //Thread.Sleep(5000);
        }

        public void ClickContinueBtn()
        {
            Browser.waitForElementToBeClickable(ContinueBtn, 60);
            ContinueBtn.Click();
            //Thread.Sleep(5000);
        }

        public void ClickResetPassword()
        {
            Browser.waitForElementToBeClickable(ResetPassword, 20);
            ResetPassword.Click();
            //Thread.Sleep(5000);
        }

        public void EnterNewPassword()
        {
            PasswordTextBox.SendKeys("$Reset12$");


        }

        public void ClickChangePassBtn()
        {
            ChangePasswordBtn.Click();
        
        }
    }
}
