﻿using Google.Protobuf.WellKnownTypes;
using GWRFramework.TestData;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GWRFramework
{
    
    public class PaymentPage
    {

        public static Double suitePrice;
        public static decimal suitePrice1;
        public static Double taxPrice;
        public static decimal taxPrice1;
        public static Double resortPrice;
        public static decimal resortPrice1;
        public static Double totalcost;
        public static Double dueTodayPrice;
        public static Decimal dueTodayPrice1;
        public static Double dueATCheckInPrice;
        public static decimal dueATCheckInPrice1;
        public static Double PckageTotlPrice;

        //public static Double dueTodayPrice;
        //public static Double dueATCheckInPrice;
        public static Double FlexTripPrice;
        public static Double FlexTripSectionPrice;
        public static Double CreditCardChargedPrice;
        public static String FirstName;
        public static String LastName;
        public static String PhNumber;
        public static String EMailAddress;
        public static String BillingAddress;
        public static String PostalCode;
        public static String City;
        public static String State;
        public static String Country;

        [FindsBy(How = How.Id, Using = "firstName")]
        public IWebElement firstNameTxt;

        [FindsBy(How = How.Id, Using = "lastName")]
        public IWebElement lastNameTxt;

        [FindsBy(How = How.Id, Using = "email")]
        public IWebElement emailIdTxt;

        [FindsBy(How = How.Id, Using = "phone")]
        public IWebElement phoneTxt;

        [FindsBy(How = How.Id, Using = "nameOnCard")]
        public IWebElement nameOnCardTxt;
    
        [FindsBy(How = How.Id, Using = "cardNumber")]
        public IWebElement cardNumberTxt;

        [FindsBy(How = How.Id, Using = "month")]
        public IWebElement monthDrp;

        [FindsBy(How = How.XPath, Using = "//select[@id='month']//option[5]")]
        public IWebElement monthDrpVal;

        [FindsBy(How = How.Id, Using = "Year")]
        public IWebElement yearDrp;

        [FindsBy(How = How.XPath, Using = "//select[@id='Year']//option[5]")]
        public IWebElement yearDrpVal;

        [FindsBy(How = How.Id, Using = "cvv")]
        public IWebElement cvvTxt;

        [FindsBy(How = How.Id, Using = "billingAddress")]
        public IWebElement billingAddressTxt;

        [FindsBy(How = How.Id, Using = "postalCode")]
        public IWebElement postalCodeTxt;

        [FindsBy(How = How.Id, Using = "city")]
        public IWebElement cityTxt;

        [FindsBy(How = How.Id, Using = "state")]
        public IWebElement statedrp;

        [FindsBy(How = How.XPath, Using = "//select[@id='state']//option[@value='NY']")]
        public IWebElement statedrpVal;

        [FindsBy(How = How.Id, Using = "ck-flex-fee")]
        public IWebElement flexTripChkbx;

        [FindsBy(How = How.XPath, Using = "//button[text()='Agree & Book']")]
        public IWebElement agreeNdBookBtn;

        [FindsBy(How = How.XPath, Using = "(//div[@aria-label='Summary Item Promotional savings']//div[contains(.,'$')])[4]")]
        public IWebElement promotionalSavngPrice;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Suite Total']//div[text()='$']")]
        public IWebElement suiteTotaPricelLable;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Suite Details']//div[text()='$']|//div[@aria-label='Summary Item Day Passes']//div[text()='$']")]
        public IWebElement SuiteTotalPrice;
        

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Taxes']//div[text()='$']")]
        public IWebElement taxCostLable;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Package Total']//div[text()='$']")]
        public IWebElement packageTotalCost;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item ']//div[text()='$']")]
        public IWebElement resortFeetLable;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Resort Fee']//div[text()='$']")]
        public IWebElement resortFeetCost;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Total Price']//div[text()='$']")]
        public IWebElement toatlCostLable;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Total']//div[text()='$']")]
        public IWebElement summaryCostTotal;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Due Today']//div[text()='$']")]
        public IWebElement dueTodayLable;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Due at check-in']//div[text()='$']")]
        public IWebElement dueAtCheckInLable;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Amount Due or Due at check-in']//div[text()='$']")]
        public IWebElement dueAtCheckInCost;
        //private object table;

        [FindsBy(How = How.XPath, Using = "//div[@role='banner']//h1[text()='Checkout As Guest']")]
        public IWebElement paymentPageHeading;

        [FindsBy(How = How.XPath, Using = "//div[@role='banner']//span")]
        public IWebElement alreadyHaveAccountLink;

        [FindsBy(How = How.XPath, Using = "//*[@id='email'][contains(@class,'sign-in-input')]")]
        public IWebElement emailtxtSigninPopup;

        // Atul for sprint 3

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Progress Bar']//li[3]//a")]
        public IWebElement diningIconProgressBar;

        [FindsBy(How = How.XPath, Using = "//*[@name='contactInfo.textAgreement']//parent::div")]
        public IWebElement smsCheckbox;

        /*[FindsBy(How = How.XPath, Using = "//*[contains(text(),'Already have an account')]")]
        public IWebElement alreadyHaveAccountLink;

        [FindsBy(How = How.XPath, Using = "//*[@id='email'][contains(@class,'sign-in-input')]")]
        public IWebElement emailtxtSigninPopup;
        */

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Add flexibility to your trip']")]
        public IWebElement flexTripSection;

        [FindsBy(How = How.XPath, Using = "//input[@id='ck-flex-fee']//parent::div")]
        public IWebElement flexTripCheckBox;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Add flexibility to your trip']//*[contains(text(),'$')]")]
        public IWebElement flexTripSectionValue;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Flex Trip')]/parent::div/parent::div/following-sibling::div//*[contains(text(),'$')]")]
        public IWebElement flexTripValue;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Flex Trip')]/parent::div/parent::div/following-sibling::div//button")]
        public IWebElement flexTripRemoveLink;

        [FindsBy(How = How.XPath, Using = "//label[@for='creditCard']")]
        public IWebElement creditCardRadioBtn;

        [FindsBy(How = How.XPath, Using = "//label[@for='affirm']")]
        public IWebElement affirmRadioBtn;

        [FindsBy(How = How.XPath, Using = "//p[contains(.,' you will be redirected to Affirm to securely complete your purchase')]")]
        public IWebElement affirmMessage;
        

        /*[FindsBy(How = How.XPath, Using = "//input[@id='affirm'][@disabled]")]
        public IWebElement affirmRadioBtn;*/

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Electronic signature compliance']//following-sibling::div//button")]
        public IWebElement checkOutWithAffirmBtn;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Electronic signature compliance']//a[contains(.,'Privacy Policy')]")]
        public IWebElement privacyPolicylink;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Electronic signature compliance']//a[contains(.,'Terms')]")]
        public IWebElement termsAndConditionslink;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Agree & Book']//*[contains(text(),'$')]")]
        public IWebElement creditCardChargedPrice;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Credit Card Info']//*[starts-with(text(),'Card holder')]")]
        public IWebElement crdHldrNotPresentAtChkInQstionMarkIcon;

        [FindsBy(How = How.XPath, Using = "//button//*[text()='Back to payment']")]
        public IWebElement backToPaymentBtn;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'ReactModal__Content')]")]
        public IWebElement doNtWarryPopUp;

        [FindsBy(How = How.XPath, Using = "//a[text()='Download Credit Card Authorization Form']")]
        public IWebElement dwnldCredtCrdAuthorForm;

        [FindsBy(How = How.Name, Using = "email")]
        public IWebElement CreditCrdEmailFld;

        [FindsBy(How = How.Name, Using = "fileNameField_Guest Name")]
        public IWebElement CreditCrdGuestNameFld;

        [FindsBy(How = How.Name, Using = "fileNameField_Arrival Date")]
        public IWebElement CreditCrdArrivalDtFld;

        [FindsBy(How = How.XPath, Using = "//*[@value='Submit']")]
        public IWebElement CreditCrdSubmitBtn;

        [FindsBy(How = How.XPath, Using = "//*[@class='container-fluid signing-container']")]
        public IWebElement CreditCrdAuthForm;

        [FindsBy(How = How.Id, Using = "country")]
        public IWebElement countryDrp;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Water Park Passes']")]
        public IWebElement WaterParkPassStection;

        [FindsBy(How = How.XPath, Using = "//label[@for='paypal']")]
        public IWebElement paypalRadioBtn;

        [FindsBy(How = How.XPath, Using = "//*[@id='paypal-button']")]
        public IWebElement payWithPaypalBtn;

        [FindsBy(How = How.XPath, Using = "//*[@class='affirm-promo-underline']")]
        public IWebElement AffirmLkUnderCostSummary;

        [FindsBy(How = How.XPath, Using = "(//*[@class='affirm-promo-underline'])[1]")]
        public IWebElement AffirmLkOnSuitePage;

        [FindsBy(How = How.XPath, Using = "//iframe[@class='affirm-sandbox-iframe']")]
        public IWebElement Affirmsandboxframe;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Affirm logo']")]
        public IWebElement AffirmSandBoxpopup;

        [FindsBy(How = How.XPath, Using = "//*[@name='contactInfo.isCASLMarked']")]
        public IWebElement IAgreeCheckBox;

        [FindsBy(How = How.XPath, Using = "//button[@type='submit'][text()='Continue']")]
        public IWebElement ContactUsContinueBtn;

        [FindsBy(How = How.XPath, Using = "(//div[contains(text(),'Edit')])[1]")]
        public IWebElement ClickOnContactInfoEdit;

        [FindsBy(How = How.XPath, Using = "(//div[contains(text(),'Edit')])[2]")]
        public IWebElement ClickOnBillingInfoEdit;

        [FindsBy(How = How.XPath, Using = "//div[contains(@aria-label,'miss out on this great deal!')]")]
        public IWebElement DontMissOutSection;
        

        public void clickONContactInfoEdit() {
            Browser.waitForElementToBeClickable(ClickOnContactInfoEdit,20);
            Browser.javaScriptClick(ClickOnContactInfoEdit);

        }

        public void clickONBillingInfoEdit()
        {
            Browser.waitForElementToBeClickable(ClickOnBillingInfoEdit, 20);
            Browser.javaScriptClick(ClickOnBillingInfoEdit);
        }
        
        public void enterfirstName(String fName)
        {
            Browser.waitForElementToDisplayed(firstNameTxt, 10);
            //Browser.clearTextboxValue(firstNameTxt);
            String fStName = firstNameTxt.GetAttribute("value");
            if (fStName == "")
            {
                firstNameTxt.SendKeys(fName);
            }
        }

        public void enterlastName(String lName)
        {
            Browser.waitForElementToDisplayed(lastNameTxt, 10);
            //Browser.clearTextboxValue(lastNameTxt);
            String LaStName = lastNameTxt.GetAttribute("value");
            if (LaStName == "")
            {
                lastNameTxt.SendKeys(lName);
            }
        }

        public void enterEmail(String email)
        {
            Browser.waitForElementToDisplayed(emailIdTxt, 10);
            //Browser.clearTextboxValue(lastNameTxt);
            String emailId = emailIdTxt.GetAttribute("value");
            if (emailId == "")
            {
                emailIdTxt.SendKeys(email);
            }
        }

        public void enterPhoneNumber(String phoneNum)
        {            
            Browser.waitForImplicitTime(6);
            String PhoneVal = phoneTxt.GetAttribute("value");
            if (PhoneVal == "")
            {
                Browser.waitForElementToDisplayed(phoneTxt, 10);
                Browser.clearTextboxValue(phoneTxt);
                phoneTxt.SendKeys(phoneNum);
            }
        }

        public void waitLoginPhoneNumLoad()
        {
            for (int i = 1; i <= 40; i++)
            {
                String PhoneVal = phoneTxt.GetAttribute("value");
                if (PhoneVal == "")
                {
                    Thread.Sleep(500);
                }
                else
                {
                    break;
                }
            }
        }
        

        public void enterNameOnCard(String cardName)
        {
            Browser.waitForElementToDisplayed(nameOnCardTxt, 10);
            Thread.Sleep(1000);
            nameOnCardTxt.SendKeys(cardName);
        }

        public void enterNumOnCard(String cardNum)
        {
            Browser.waitForElementToDisplayed(cardNumberTxt, 10);
            Thread.Sleep(1000);
            cardNumberTxt.SendKeys(cardNum);
        }

        public void selectMonth()
        {
            Browser.waitForElementToBeClickable(monthDrp, 20);
            SelectElement oSelect = new SelectElement(monthDrp);
            oSelect.SelectByValue("05");           
        }

        public void selectYear()
        {
            Browser.waitForElementToBeClickable(yearDrp, 20);
            SelectElement oSelect = new SelectElement(yearDrp);
            oSelect.SelectByValue("2025");           
        }

        public void enterCVV(String CVV)
        {
            Browser.waitForElementToDisplayed(cvvTxt, 10);
            cvvTxt.SendKeys(CVV);
        }

        public void enterBillingAddress(String billingAddress)
        {
            Browser.waitForElementToDisplayed(billingAddressTxt, 10);
            //Browser.clearTextboxValue(billingAddressTxt);
            //Thread.Sleep(5000);
            Browser.waitForImplicitTime(6);
            String billVal = billingAddressTxt.GetAttribute("value");
            if (billVal == "")
            {
                Browser.clearTextboxValueACtion(billingAddressTxt);
                Browser.waitForImplicitTime(3);
                billingAddressTxt.SendKeys(billingAddress);
            }
        }

        public void waitLoginBiiligAddress()
        {
            Thread.Sleep(3000);
            for (int i = 1; i <= 15; i++)
            {
                String billVal = billingAddressTxt.GetAttribute("value");
                if (billVal == "")
                {
                    Thread.Sleep(1000);
                }
                else
                {
                    break;
                }
            }
        }

        public void enterPostalCode()
        {

            //Browser.clearTextboxValue(postalCodeTxt);
            String posatlVal = postalCodeTxt.GetAttribute("value");
            if (posatlVal == "")
            {
                String postalCode = DataAccess.getData(2, 2);
                //Thread.Sleep(2000);
                Browser.waitForImplicitTime(4);
                Browser.clearTextboxValueACtion(postalCodeTxt);
                Browser.waitForElementToDisplayed(postalCodeTxt, 2);
                postalCodeTxt.SendKeys(postalCode);
            }
        }
                

        public void waitLoginPostlCodeLoad()
        {
            Browser.opt.AddArgument("--disable - geolocation");
            Browser.opt.AddArguments("disable-geolocation");
            Thread.Sleep(10000);
            for (int i = 1; i <= 40; i++)
            {
                String posatlVal = postalCodeTxt.GetAttribute("value");
                if (posatlVal == "")
                {
                    Thread.Sleep(500);
                }
                else
                {
                    break;
                }
            }
        }

        public void enterCity(String city)
        {
            Browser.waitForElementToDisplayed(cityTxt, 10);
            //Thread.Sleep(7000);
            Browser.waitForImplicitTime(5);
            String cityVal = cityTxt.GetAttribute("value");
            if (cityVal == "")
            {
                Browser.clearTextboxValueACtion(cityTxt);
                Thread.Sleep(1000);
                cityTxt.SendKeys(city);
            }
        }

        public void selectState()
        {
            //Thread.Sleep(2000);
            Browser.waitForImplicitTime(4);
            if (statedrp.Exists())
            {
                Browser.waitForElementToBeClickable(statedrp, 20);
                SelectElement oSelect = new SelectElement(statedrp);
                oSelect.SelectByValue("NY");

            }
        }

        public void clickOnBookAndAgree()
        {
            Browser.waitForElementToDisplayed(agreeNdBookBtn, 10);
            Thread.Sleep(2000);
            Browser.waitForImplicitTime(4);
            agreeNdBookBtn.Click();
            //Pages.NewConfirmation.waitForPaymentSprinnerOut(NewConfirmationPage.paymentSpinner);
        }

        public void clickOnContinue()
        {
            Browser.waitForElementToDisplayed(ContactUsContinueBtn, 10);
            //Browser.javaScriptClick(ContactUsContinueBtn);
            ContactUsContinueBtn.Click();            
        }
        public void convertSuiteTotal()
        {
            suitePrice = Browser.stringToDoubleConvert(suiteTotaPricelLable);
            //suitePrice = Browser.stringToDecimalConvert(suiteTotaPricelLable);
        }

        public void convertSuiteTotal1()
        {
            //suitePrice = Browser.stringToDoubleConvert(suiteTotaPricelLable);
            suitePrice1 = Browser.stringToDecimalConvert(suiteTotaPricelLable);
        }

        public void convertTaxesToDouble()
        {
            taxPrice = Browser.stringToDoubleConvert(taxCostLable);
        }

        public void convertTaxesToDouble1()
        {
            taxPrice1 = Browser.stringToDecimalConvert(taxCostLable);
        }

        public void convertResortFeeToDouble()
        {
            resortPrice = Browser.stringToDoubleConvert(resortFeetLable);
        }

        public void convertResortFeeToDouble1()
        {
            resortPrice1 = Browser.stringToDecimalConvert(resortFeetLable);
        }

        public void convertTotalCostToDouble()
        {
            totalcost = Browser.stringToDoubleConvert(toatlCostLable);
        }

        public void convertDueTodayToDouble()
        {
           dueTodayPrice = Browser.stringToDoubleConvert(dueTodayLable);
        }

        public void convertDueTodayToDouble1()
        {
            dueTodayPrice1 = Browser.stringToDecimalConvert(dueTodayLable);
        }

        public void convertDueAtCheckInToDouble()
        {
            dueATCheckInPrice = Browser.stringToDoubleConvert(dueAtCheckInLable);
        }

        public void convertDueAtCheckInToDouble1()
        {
            dueATCheckInPrice1 = Browser.stringToDecimalConvert(dueAtCheckInLable);
        }

        public void convertPckgTotal()
        {
            PckageTotlPrice = Browser.stringToDoubleConvert(packageTotalCost);
        }

        public Boolean verifyPaymentPageHeading()
        {
            Browser.waitForElementToDisplayed(paymentPageHeading, 20);
            return paymentPageHeading.Displayed;
        }

        public void clickAlreadyHaveAccountLink()
        {
            if (Pages.PropertyHome.signInLink.Exists() == false)
            {
                Pages.PropertyHome.clickToSignOut();
            }
            //Browser.waitForElementToDisplayed(alreadyHaveAccountLink, 20);
            Browser.waitForElementToBeClickable(alreadyHaveAccountLink, 20);
            alreadyHaveAccountLink.Click();
        }

        public void sendEmailonSigninPopup(String email)
        {
            Browser.waitForElementToDisplayed(emailtxtSigninPopup, 20);
            emailtxtSigninPopup.SendKeys(email);
        }

        //Atul for sprint 3
        public void clickOnDiningIcon()
        {
            Browser.waitForElementToBeClickable(diningIconProgressBar, 20);
            diningIconProgressBar.Click();
        }

        public Boolean iAmOnDiningPage()
        {
            Browser.waitForElementToDisplayed(Pages.Suite.continueToPaymentPageBtn, 20);
            bool ContinueToPaymentBtn = Pages.Suite.continueToPaymentPageBtn.Displayed;
            return ContinueToPaymentBtn;
        }
        public void clickOnSMSCheckBox()
        {
            Browser.waitForElementToBeClickable(smsCheckbox, 20);
            smsCheckbox.Click();
        }
        public Boolean verifySMSCheckBoxChecked()
        {
            Browser.waitForElementToDisplayed(smsCheckbox, 20);
            if (smsCheckbox.Exists())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean verifySMSCheckBoxunChecked()
        {
            Browser.waitForElementToDisplayed(smsCheckbox, 20);
            if (smsCheckbox.Exists())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

       /* public void clickAlreadyHaveAccountLink()
        {
            Browser.waitForElementToDisplayed(alreadyHaveAccountLink, 10);
            Browser.waitForElementToBeClickable(alreadyHaveAccountLink, 10);
            alreadyHaveAccountLink.Click();
        }*/
        /*public void sendEmailonSigninPopup(String email)
        {
            Browser.waitForElementToDisplayed(emailtxtSigninPopup, 20);
            emailtxtSigninPopup.SendKeys(email);
        }*/


        public void clickOnFlextTripCheckBox()
        {
            // Thread.Sleep(1000);
            //Browser.waitForElementToDisplayed(flexTripCheckBox, 20);
            Browser.waitForElementToBeClickable(flexTripCheckBox, 20);
            flexTripCheckBox.Click();
        }

        public Boolean verifyFlexTripCheckBoxChecked()
        {
            Browser.waitForElementToDisplayed(flexTripCheckBox, 20);
            if (flexTripCheckBox.Exists())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean verifyFlexTripCheckBoxUnChecked()
        {
            Browser.waitForElementToDisplayed(flexTripCheckBox, 20);
            if (flexTripCheckBox.Exists())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void getFlexTripSectionPrice()
        {
            Browser.waitForElementToDisplayed(flexTripSectionValue, 20);
            FlexTripSectionPrice = Browser.stringToDoubleConvert1(flexTripSectionValue, 2);
        }

        public void getFlexTripPriceunderCostSummary()
        {
            Browser.waitForElementToDisplayed(flexTripValue, 20);
            FlexTripPrice = Browser.stringToDoubleConvert(flexTripValue);
        }

        public Boolean verifyCreditCardRadioBtnSelected()
        {
            Browser.waitForElementToBeClickable(nameOnCardTxt, 20);
            return nameOnCardTxt.Exists();
        }
        public void clickOnCreditCardRadioBtn()
        {
            Browser.waitForElementToBeClickable(creditCardRadioBtn, 20);
            creditCardRadioBtn.Click();
        }

        public void clickOnAffirmRadioBtn()
        {
            Browser.waitForElementToBeClickable(affirmRadioBtn, 20);
            affirmRadioBtn.Click();
        }

        public Boolean verifyAffirmRadioBtnSelected()
        {
            Browser.waitForElementToDisplayed(affirmMessage, 20);
            return affirmMessage.Exists();
        }

        public Boolean verifyCheckOutwithAffirmBtn()
        {
            Browser.waitForElementToDisplayed(checkOutWithAffirmBtn, 20);
            bool CheckOutWithAffirmBtn = checkOutWithAffirmBtn.Displayed;
            return CheckOutWithAffirmBtn;
        }

        public void clickOnCheckOutwithAffirmBtn()
        {
            Browser.waitForElementToBeClickable(checkOutWithAffirmBtn, 20);
            checkOutWithAffirmBtn.Click();
        }

        public void enterPostalCode(String PostalCode)
        {
            Browser.waitForElementToDisplayed(billingAddressTxt, 20);
            Browser.clearTextboxValue(billingAddressTxt);
            Browser.waitForElementToDisplayed(postalCodeTxt, 20);
            Thread.Sleep(3000);
            Browser.clearTextboxValue(postalCodeTxt);
            postalCodeTxt.SendKeys(PostalCode);
            Thread.Sleep(5000);
            Browser.clearTextboxValue(postalCodeTxt);
            postalCodeTxt.SendKeys(PostalCode);
            Thread.Sleep(5000);
        }
        public Boolean verifyAffirmRadioBtnEnable()
        {
            //Thread.Sleep(10000);
            Browser.waitForElementToDisplayed(affirmRadioBtn, 20);
            //  return creditCardRadioBtn.Enabled;
            if (affirmRadioBtn.Exists())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean verifyFlexTripSection()
        {
            Browser.waitForElementToDisplayed(flexTripSection, 20);
            if (flexTripSection.Exists())
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void clickFlexTripRemoveLink()
        {
            Browser.waitForElementToDisplayed(flexTripRemoveLink, 20);
            Browser.waitForElementToBeClickable(flexTripRemoveLink, 20);
            flexTripRemoveLink.Click();
        }

        public void clickPrivacyPolicyLink()
        {
            Browser.waitForElementToDisplayed(privacyPolicylink, 20);
            Browser.waitForElementToBeClickable(privacyPolicylink, 20);
            privacyPolicylink.Click();
        }

        public void clickTermsAndConditionsLink()
        {
            //Browser.waitForElementToDisplayed(termsAndConditionslink, 20);
            Browser.waitForElementToBeClickable(termsAndConditionslink, 20);
            termsAndConditionslink.Click();
        }

        public void getYourCredCrdChargedPrice()
        {
            Browser.waitForElementToDisplayed(creditCardChargedPrice, 20);
            CreditCardChargedPrice = Browser.stringToDoubleConvert1(creditCardChargedPrice, 3);
        }

        public void clickCrdHldNtPrsntAtChkInQuestionMarkIcon()
        {
            //Browser.waitForElementToDisplayed(crdHldrNotPresentAtChkInQstionMarkIcon, 20);
            Browser.waitForElementToBeClickable(crdHldrNotPresentAtChkInQstionMarkIcon, 20);
            Browser.javaScriptClick(crdHldrNotPresentAtChkInQstionMarkIcon);
            //crdHldrNotPresentAtChkInQstionMarkIcon.Click();
        }

        public void clickBackToPaymentBtn()
        {
            Browser.waitForElementToDisplayed(backToPaymentBtn, 20);
            Browser.waitForElementToBeClickable(backToPaymentBtn, 20);
            backToPaymentBtn.Click();
        }

        public Boolean verifyDoNotWarryPopUp()
        {
            //Browser.waitForElementToBeClickable(doNtWarryPopUp, 8);
            if (doNtWarryPopUp.Exists())
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public Boolean verifyprivacyPolicyPage()
        {
            Browser.waitForImplicitTime(10);
            Browser.SwitchToNewWindow();
            String Title = Browser.Title;
            if (Title.Contains("Privacy Policy"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean verifyTermsAndConditionsPage()
        {
            Browser.waitForImplicitTime(10);
            Browser.SwitchToNewWindow();
            String Title = Browser.Title;
            if (Title.Contains("Terms and Conditions"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void clickDwnldCredtCrdAuthorForm()
        {
            Browser.waitForElementToDisplayed(dwnldCredtCrdAuthorForm, 20);
            Browser.waitForElementToBeClickable(dwnldCredtCrdAuthorForm, 20);
            dwnldCredtCrdAuthorForm.Click();
        }
        public void sendEmailonCredtCardAuthSigninForm(String email)
        {
            Browser.waitForElementToDisplayed(CreditCrdEmailFld, 20);
            CreditCrdEmailFld.SendKeys(email);
        }
        public void sendGuestNameonCredtCardAuthSigninForm(String guestName)
        {
            Browser.waitForElementToDisplayed(CreditCrdGuestNameFld, 10);
            CreditCrdGuestNameFld.SendKeys(guestName);
        }

        public void enterArrivalDate()
        {
            String arrivalDate = DataAccess.getData(3, 2);
            Browser.waitForElementToDisplayed(CreditCrdArrivalDtFld, 10);
            CreditCrdArrivalDtFld.Clear();
            CreditCrdArrivalDtFld.SendKeys(arrivalDate);
        }
        public void clickSubmitBtn()
        {
            Browser.waitForElementToDisplayed(CreditCrdSubmitBtn, 10);
            Browser.waitForElementToBeClickable(CreditCrdSubmitBtn, 10);
            CreditCrdSubmitBtn.Click();
        }
        public Boolean VerifyCeditCardAuthorizationForm()
        {
            WebDriverWait wait = new WebDriverWait(Browser.webDriver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='container-fluid signing-container']")));
            if (CreditCrdAuthForm.Exists())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //  List<IWebElement> lstTrElem = new List<IWebElement>(Browser.driver.FindElements(By.XPath("//tr")));

        public Boolean verifyfirstName(string firstNm)
        {
            Browser.waitForElementToDisplayed(firstNameTxt, 20);
            for (int i = 0; i < 10; i++)
            {
                FirstName = firstNameTxt.GetAttribute("value");
                if (FirstName == "")
                {
                    Thread.Sleep(2000);
                }
                else
                {
                    break;
                }
            }
            return FirstName.Equals(firstNm);
        }
        public Boolean verifylastName(string lastNm)
        {
            Browser.waitForElementToDisplayed(lastNameTxt, 20);
            LastName = lastNameTxt.GetAttribute("value");
            return LastName.Equals(lastNm);
        }

        public Boolean verifyphoneNumber(string phNum)
        {
            Browser.waitForElementToDisplayed(phoneTxt, 20);
            PhNumber = phoneTxt.GetAttribute("value");
            return PhNumber.Equals(phNum);
        }

        public Boolean verifyEmailAddress(string MailAdrss)
        {
            Browser.waitForElementToDisplayed(emailIdTxt, 20);
            EMailAddress = emailIdTxt.GetAttribute("value");
            return EMailAddress.Equals(MailAdrss);
        }

        public Boolean verifyBillingAdrss(string BilAds)
        {
            Browser.waitForElementToDisplayed(billingAddressTxt, 20);
            /*for (int i = 0; i < 15; i++)
            {
                FirstName = firstNameTxt.GetAttribute("value");
                if (FirstName == "")
                {
                    Thread.Sleep(1500);
                }
                else
                {
                    break;
                }
            }*/
            for (int i = 0; i < 15; i++)
            {
                BillingAddress = billingAddressTxt.GetAttribute("value");
                if (BillingAddress == "")
                {
                    Thread.Sleep(1500);
                }
                else
                {
                    break;
                }
            }
            return BillingAddress.Equals(BilAds);
        }

        public Boolean verifyPostalCode(string pstCode)
        {
            Browser.waitForElementToDisplayed(postalCodeTxt, 20);
            PostalCode = postalCodeTxt.GetAttribute("value");
            return PostalCode.Equals(pstCode);
        }
        public Boolean verifyCity(string city)
        {
            Browser.waitForElementToDisplayed(cityTxt, 20);
            City = cityTxt.GetAttribute("value");
            return City.Equals(city);
        }
        public Boolean verifyState(string state)
        {
            Browser.waitForElementToDisplayed(statedrp, 20);
            State = statedrp.GetAttribute("value");
            return State.Equals(state);
        }
        public Boolean verifyCountry(string cutry)
        {
            Browser.waitForElementToDisplayed(countryDrp, 20);
            Country = countryDrp.GetAttribute("value");
            return Country.Equals(cutry);
        }

        public void selectCanadaCountry()
        {
            Browser.waitForElementToDisplayed(countryDrp, 20);
            SelectElement oSelect = new SelectElement(countryDrp);
            oSelect.SelectByValue("CA");
            //Country = countryDrp.GetAttribute("value");
            //return Country.Equals(cutry);            
        }

        public Boolean verifyWaterParkPassesSection()
        {
            Boolean WaterPkPass = WaterParkPassStection.Exists();
            return WaterPkPass;
        }
        public void clickOnPaypalRadioBtn()
        {
            Browser.waitForElementToBeClickable(paypalRadioBtn, 20);
            paypalRadioBtn.Click();
        }
        public Boolean verifyPayWithPayPalBtn()
        {
            Browser.waitForImplicitTime(3);
            Browser.waitForElementToBeClickable(payWithPaypalBtn, 10);
            // Thread.Sleep(5000);
            Boolean PaypalBtn = payWithPaypalBtn.Exists();
            return PaypalBtn;
        }

        public void clickAffirmLkUnderCostSummary()
        {
            Browser.waitForElementToBeClickable(AffirmLkUnderCostSummary, 20);
            Browser.scrollVerticalBy("400");
            Browser.javaScriptClick(AffirmLkUnderCostSummary);
            //AffirmLkUnderCostSummary.Click();
        }

        public void clickAffirmLkOnSuitePage()
        {
            Browser.waitForElementToBeClickable(AffirmLkOnSuitePage, 20);
            Browser.scrollVerticalBy("400");
            Browser.javaScriptClick(AffirmLkOnSuitePage);
            //AffirmLkUnderCostSummary.Click();
        }
        public Boolean verifyAffirmsandboxpopup()
        {

            Browser.SwitchToFrameWindow(Affirmsandboxframe);
            Browser.waitForElementToDisplayed(AffirmSandBoxpopup, 20);
            Boolean AffirmSandbox = AffirmSandBoxpopup.Exists();
            return AffirmSandbox;
        }

        public Boolean verifyIAgreeCheckBox()
        {
            Boolean IAgree = IAgreeCheckBox.Exists();
            return IAgree;
        }

        public Boolean verifyDontMissOutSec()
        {
            Browser.waitForElementToBeClickable(DontMissOutSection, 20);
            Boolean dontMssOut = DontMissOutSection.Exists();
            return dontMssOut;
        }
        
    }
}
