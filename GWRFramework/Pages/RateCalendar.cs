﻿using java.awt;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GWRFramework
{
    public class RateCalendar
    {
        public static String SuiteTitle;
        public static String SuiteTitle1;

        [FindsBy(How = How.XPath, Using = "//button[@disabled][text()='Continue to packages']")]
        public IWebElement disabledContinueToPackgBtn;

        [FindsBy(How = How.XPath, Using = "//button//div[contains(.,'Close')]")]
        public IWebElement rateCalCloselink;

        [FindsBy(How = How.XPath, Using = "//div[starts-with(@class,'DayPicker DayPicker')]/preceding-sibling::div")]
        public IWebElement suiteNameOnRateCal;

        //String ratCalSuiteName1 = "//div[starts-with(@class,'DayPicker DayPicker')]/preceding-sibling::div//h3[text()='";
        //String ratCalSuiteName2 = "']";
        String ratCalSuiteName1 = "//div[starts-with(@class,'DayPicker DayPicker')]/preceding-sibling::div//h3";

        [FindsBy(How = How.XPath, Using = "//div[starts-with(@class,'DayPicker DayPicker')]/preceding-sibling::div//div[text()='Offer Code:']")]
        public IWebElement offerCodeOnRateCal;

        String ratCalOffercode1 = "//span[text()='";
        String ratCalOffercode2 = "']";

        [FindsBy(How = How.XPath, Using = "(//div[contains(.,'Offer Code')]//parent::div//*[local-name()='svg'])[1]")]
        public IWebElement DeleteOfferCodeOnRateCal;

       
        [FindsBy(How = How.XPath, Using = "//div[starts-with(@class,'DayPicker DayPicker')]/preceding-sibling::div//div[text()='Best Available Rate']")]
        public IWebElement bestAvailableRateLableOnRateCal;

        [FindsBy(How = How.XPath, Using = "(//div[@class='CalendarMonthGrid_month__horizontal CalendarMonthGrid_month__horizontal_1']//strong)[2]")]
        public IWebElement monthTitle;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Move forward to switch to the next month.']")]
        public IWebElement nxtMonthlink;

      //  [FindsBy(How = How.XPath, Using = "//div[@class='sc-htpNat cCwWAO']")]
      //  public IWebElement waitSpinner;

        public static String waitSpinnerStr = "(//div[contains(.,'Updating Your Stay')])[3]";


        String notAvailableLabel = "(//table[@class='CalendarMonth_table CalendarMonth_table_1'][@role='presentation'])[2]//tr//td[starts-with(@aria-label,'Not available')]";
        String tableDate = "((//table[@class='CalendarMonth_table CalendarMonth_table_1'][@role='presentation'])[2]//tr//td)[";
        String tableDate2 = "]";

        [FindsBy(How = How.XPath, Using = "((//table[@class='CalendarMonth_table CalendarMonth_table_1'][@role='presentation'])[2]//tr//td[@role='button'])[21]")]
        public IWebElement rateCalCheckInDate;

        [FindsBy(How = How.XPath, Using = "((//table[@class='CalendarMonth_table CalendarMonth_table_1'][@role='presentation'])[2]//tr//td)[21][starts-with(@aria-label,'Not available')]")]
        public IWebElement notAvaDates;

        [FindsBy(How = How.XPath, Using = "((//table[@class='CalendarMonth_table CalendarMonth_table_1'][@role='presentation'])[2]//tr//td[@role='button'])[22]")]
        public IWebElement rateCalCheckOutDate;

        [FindsBy(How = How.XPath, Using = "//button[text()='Continue to packages']")]
        public IWebElement continueToPackageBtn;

        [FindsBy(How = How.XPath, Using = "(//table[@class='CalendarMonth_table CalendarMonth_table_1'][@role='presentation'])[2]//tr//td[@role='button']//div[contains(.,'$')]")]
        public IWebElement availableOnRateCal;

        [FindsBy(How = How.XPath, Using = "//*[starts-with(@aria-label,'Selected as start date')]//*[contains(text(),'$')]/preceding-sibling::div[1]")]
        public IWebElement selectedCheckindate;

        [FindsBy(How = How.XPath, Using = "//*[starts-with(@aria-label,'Selected as end date')]//*[contains(text(),'$')]/preceding-sibling::div[1]")]
        public IWebElement selectedCheckoutdate;

        [FindsBy(How = How.XPath, Using = "//*[starts-with(@aria-label,'Selected as start date')]//*[contains(text(),'$')]")]
        public IWebElement RateCaldrSuitePrice;

        public static String CheckInDate;
        public static String CheckOutDate;
        public static Double RateCalendarSuitePrice;
        public static decimal RateCalendarSuitePrice1;
        public static Boolean Avilableresult = true;

        public Boolean verifyOfferCodeIsDisplayed(int numOfNextMnth)
        {
            //Thread.Sleep(3000);
            Browser.waitForImplicitTime(4);
            Avilableresult = true;
            for (int i = 1; i <= numOfNextMnth; i++)
            {
                Browser.waitForElementToBeClickable(nxtMonthlink, 20);
                nxtMonthlink.Click();
                //waitForRateCalSprinnerOut(waitSpinnerStr);
                //Thread.Sleep(8000);
                //Browser.waitForImplicitTime(8);
                Browser.waitForElementToBeClickable(availableOnRateCal, 20);
                if (availableOnRateCal.Exists())
                {
                    Avilableresult = true;
                }
                else {
                    Avilableresult = false;
                }
            }
            return Avilableresult;


        }

        public Boolean verifyOfferCodeIsDisplayed(String offercode)
        {
            //Thread.Sleep(3000);
            Browser.waitForImplicitTime(4);
            IWebElement ratecalOfferCode = offerCodeOnRateCal.FindElement(By.XPath(ratCalOffercode1 + offercode + ratCalOffercode2));
            Browser.waitForElementToDisplayed(ratecalOfferCode, 20);
            return ratecalOfferCode.Displayed;
        }

        public void waitForRateCalSprinnerOut(String xpath)
        {
            //Browser.waitForImplicitTime(5);
           // Thread.Sleep(3000);
            Browser.waitForElementToBeInvisible(20, xpath);
            Browser.waitForImplicitTime(10);            
        }

        public void waitForMyResSprinnerOut(String xpath)
        {
            //Browser.waitForImplicitTime(5);
            //Thread.Sleep(3000);
            Browser.waitForElementToBeInvisible(20, xpath);
            //Browser.waitForImplicitTime(10);
        }

        public void waitForSuitelSprinnerOut(String xpath)
        {
            //Browser.waitForImplicitTime(4);
            Browser.waitForElementToBeInvisible(20, xpath);
            //Browser.waitForImplicitTime(6);            
        }

        public void selectCheckInDate(int numOfNextMnth)
        {            

            for (int i = 1; i <= numOfNextMnth; i++)
            {
                Browser.waitForElementToBeClickable(nxtMonthlink, 30);
                nxtMonthlink.Click();
                
            }

            Browser.waitForElementToBeClickable(rateCalCheckInDate, 30);
            if (notAvaDates.Exists())
            {
                Assert.Pass("Date is not available for selection");
            }
            else
            {
                Browser.waitForImplicitTime(5);
                rateCalCheckInDate.Click();

                Browser.waitForElementToBeClickable(rateCalCheckOutDate, 20);
                rateCalCheckOutDate.Click();
            }
          
        }

        public Boolean verifySuiteNameIsDisplayed()
        {
            //Thread.Sleep(6000);
            Browser.waitForImplicitTime(8);
            //String CamlTitle = Browser.convertToCamleCase(SuiteTitle);
            //IWebElement ratecalSUiteName = suiteNameOnRateCal.FindElement(By.XPath(ratCalSuiteName1 + SuiteTitle + ratCalSuiteName2));
            IWebElement ratecalSUiteName = suiteNameOnRateCal.FindElement(By.XPath(ratCalSuiteName1));

            Browser.waitForElementToDisplayed(ratecalSUiteName, 20);
            return ratecalSUiteName.Displayed;
        }


        public Boolean verifyBBARIsDisplayed()
        {
            //Thread.Sleep(3000);
            //Browser.waitForImplicitTime(5);
            Browser.waitForElementToDisplayed(bestAvailableRateLableOnRateCal, 20);
            return bestAvailableRateLableOnRateCal.Displayed;
        }

        public void getSuiteName() {
            Browser.waitForElementToDisplayed(Pages.Suite.suiteTitle, 20);
            String CamlSuiteTitle = Browser.getText(Pages.Suite.suiteTitle);
            SuiteTitle = Browser.convertToCamleCase(CamlSuiteTitle);
        }

        public void getSuiteName1()
        {
            Browser.waitForElementToDisplayed(Pages.Suite.suiteTitle, 20);
            String CamlSuiteTitle = Browser.getText(Pages.Suite.suiteTitle);
            SuiteTitle1 = Browser.convertToCamleCase(CamlSuiteTitle);
        }

        public void getSuiteNamewithOption()
        {
            Browser.waitForElementToDisplayed(Pages.Suite.suiteTitle, 20);
            String CamlSuiteTitle = Browser.getText(Pages.Suite.suiteTitle);
            SuiteTitle = Browser.convertToCamleCase(CamlSuiteTitle);
        }

        public void clickOnRateCalLink()
        {
            getSuiteName();
            Pages.Suite.clickCookiesClose();
            Browser.waitForElementToBeClickable(Pages.Suite.chkRateCalLink, 20);
            //Pages.Suite.chkRateCalLink.Click();
            Browser.javaScriptClick(Pages.Suite.chkRateCalLink);
            waitForRateCalSprinnerOut(waitSpinnerStr);
            Browser.scrollVerticalBy("400");
        }
        
        public void clickOnRateCalLinkWithSuiteOption()
        {
            getSuiteName();
            Pages.Suite.clickCookiesClose();
            Browser.waitForElementToBeClickable(Pages.Suite.chkRateCalLinkWithOption, 20);
            Pages.Suite.chkRateCalLinkWithOption.Click();
            waitForRateCalSprinnerOut(waitSpinnerStr);
            Browser.scrollVerticalBy("400");
        }

        public Boolean verifyDissabledContToPackgBtn()
        {
            Browser.waitForElementToDisplayed(disabledContinueToPackgBtn, 20);
            return disabledContinueToPackgBtn.Displayed;
        }

        public void clickOnCloseRateCalLink()
        {
            //Thread.Sleep(6000);
            //Browser.waitForElementToDisplayed(rateCalCloselink, 20);
            Browser.waitForElementToBeClickable(rateCalCloselink, 20);
            rateCalCloselink.Click();
        }

        public Boolean seeCloseRateCalLink()
        {
            //Browser.waitForElementToDisplayed(rateCalCloselink, 40);
            Browser.waitForElementToBeClickable(rateCalCloselink, 20);
            return rateCalCloselink.Displayed;
        }

        public static Boolean result = true;

        public void nextMonthChangeTest(String ExpctdMonth)
        {
            nxtMonthlink.Click();
            Thread.Sleep(3000);
            //waitForRateCalSprinnerOut(waitSpinnerStr);
            Browser.waitForElementToDisplayed(monthTitle, 20);
            String GtText = Browser.getText(monthTitle);
            //String FebYr = Feb.Remove(mnthTitle.Length - 4);
            String mnthNameRateCal = GtText.Substring(0, 3);
            result = mnthNameRateCal.Equals(ExpctdMonth);
            //break;
        }

        public Boolean nextMnthVerfication(int monthCnt)
        {
            for (int i = 1; i <= monthCnt; i++)
            {
                if (result == true)
                {
                    String mnthTitle = Browser.getText(monthTitle);
                    //String removeYear = mnthTitle.Remove(mnthTitle.Length - 5);
                    String removeYear = mnthTitle.Substring(0,3);

                    switch (removeYear)
                    {
                        case "Jan":
                            nextMonthChangeTest("Feb");
                            break;

                        case "Feb":
                            nextMonthChangeTest("Mar");
                            break;

                        case "Mar":
                            nextMonthChangeTest("Apr");
                            break;

                        case "Apr":
                            nextMonthChangeTest("May");
                            break;

                        case "May":
                            nextMonthChangeTest("Jun");
                            break;

                        case "Jun":
                            nextMonthChangeTest("Jul");
                            break;

                        case "Jul":
                            nextMonthChangeTest("Aug");
                            break;

                        case "Aug":
                            nextMonthChangeTest("Sep");
                            break;

                        case "Sep":
                            nextMonthChangeTest("Oct");
                            break;

                        case "Oct":
                            nextMonthChangeTest("Nov");
                            break;

                        case "Nov":
                            nextMonthChangeTest("Dec");                            
                            break;

                        case "Dec":
                            nextMonthChangeTest("Jan");                           
                            break;
                    }
                    //return result;
                }
                else
                {
                    return false;
                }
            }
            return result;
        }

        public Boolean notAvailableDatesLabel()
        {
            Browser.waitForElementToDisplayed(notAvaDates, 15);
            //bool Dates;
            if (notAvaDates.Exists())
            {
                return true;
            }
            else {
                return true;
            }

        }

        //Atul -
        public void getCheckInDate()
        {
            Browser.waitForElementToDisplayed(selectedCheckindate, 20);
            CheckInDate = Browser.getText(selectedCheckindate);

        }
        public void getCheckOutDate()
        {
            Browser.waitForElementToDisplayed(selectedCheckoutdate, 20);
            CheckOutDate = Browser.getText(selectedCheckoutdate);

        }

        public void getSuitePriceOnRateCalendar()
        {
            Thread.Sleep(4000);
            Browser.waitForElementToBeClickable(RateCaldrSuitePrice, 40);
            RateCalendarSuitePrice1 = Browser.stringToDecimalConvert(RateCaldrSuitePrice);
        }

        public void clickOnSoldOutSuiteRateCalLink()
        {
           // Browser.waitForElementToDisplayed(Pages.Suite.SoldOutchkRateCalLink, 20);
            Browser.waitForElementToBeClickable(Pages.Suite.SoldOutchkRateCalLink, 20);
            Pages.Suite.SoldOutchkRateCalLink.Click();
        }


        //public static IWebDriver webDriver;
        public void deleteOffercode()
        {
            for (int i = 1; i <= 2; i++)
            {
                Browser.waitForElementToBeClickable(nxtMonthlink, 20);
                nxtMonthlink.Click();                
            }

            Browser.waitForElementToBeClickable(DeleteOfferCodeOnRateCal, 40);
            DeleteOfferCodeOnRateCal.Click();

            //IJavaScriptExecutor js = (IJavaScriptExecutor)Browser.webDriver;
            //Actions actions = new Actions(Browser.webDriver);
            //actions.SendKeys(Keys.F12);
            //actions.Click();
            //actions.Perform();

            //js.ExecuteScript("--auto - open - devtools -for-tabs");
            
                 //js.ExecuteScript("document.body.style.zoom='80%'");
            
            //Browser.javaScriptClick(DeleteOfferCodeOnRateCal);

            //IJavaScriptExecutor js = (IJavaScriptExecutor)Browser.webDriver;
            //js.ExecuteScript("document.body.style.zoom='70%'");
            //js.ExecuteScript("arguments[0].click();", DeleteOfferCodeOnRateCal);
            //Thread.Sleep(7000);
            //Browser.waitForElementToBeClickable(DeleteOfferCodeOnRateCal, 40);
            
            //js.ExecuteScript("document.body.style.zoom='75%'", null);
            //Dimension d = new Dimension(300, 1080);
            //webDriver.Manage().Window.Size.Width.;
            
            //actions.MoveToElement(DeleteOfferCodeOnRateCal).Build().Perform();
            //actions.Click(DeleteOfferCodeOnRateCal).Build().Perform();
            //actions.MoveToElement(DeleteOfferCodeOnRateCal).Click().Perform();
            
            //Thread.Sleep(7000);

            //Browser.waitForElementToBeClickable(DeleteOfferCodeOnRateCal, 40);
            //DeleteOfferCodeOnRateCal.Displayed;
            //DeleteOfferCodeOnRateCal.Click();
            //DeleteOfferCodeOnRateCal.Click();
            //DeleteOfferCodeOnRateCal.Click();
            //return true;
        }

        public void clickOnConToPackgBtn() 
        {
            Browser.waitForElementToBeClickable(continueToPackageBtn, 20);
            continueToPackageBtn.Click();
        }


    }
}
