﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWRFramework
{
    public class FooterSection
    {
        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'FAQ')]")]
        public IWebElement FAQLink;

        [FindsBy(How = How.XPath, Using = "//div[@class='footer__links aos-init aos-animate']//a[contains(text(),'Customer Service')]")]
        public IWebElement CustomerServiceLink;

        [FindsBy(How = How.XPath, Using = "//div[@class='footer__links aos-init aos-animate']//a[contains(text(),'Birthday Parties')]")]
        public IWebElement BirthdayPartiesLink;

        [FindsBy(How = How.XPath, Using = "//li//a[@title='Groups & Meetings']")]
        public IWebElement GroupsAndEventsLink;

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Privacy Policy & Terms and Conditions')]")]
        public IWebElement TermsAndConditionLink;

        //[FindsBy(How = How.XPath, Using = "//div[@class='grid-container']")]
        //public IWebElement FAQPage;
        
        [FindsBy(How = How.XPath, Using = "//h1[contains(text(),'FAQ')]")]
        public IWebElement FAQPage;

        [FindsBy(How = How.XPath, Using = "//h1[text()='Customer Service']")]
        public IWebElement CustomerServicePage;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'cmp-title')]//h1[contains(.,'Birthday Parties')]")]
        public IWebElement BirthdayPartiesPage;
        
        [FindsBy(How = How.XPath, Using = "//h3[contains(.,'GROUPS & MEETINGS')]")]
        public IWebElement GroupsAndEventPage;
        
        [FindsBy(How = How.XPath, Using = "//*[text()='Terms and Conditions']")]
        public IWebElement PrivacyPolicyAndTermsPage;

        [FindsBy(How = How.XPath, Using = "//a[@class='button grid-item__actions--lead-gen lead-gen-callout-button']")]
        public IWebElement SubScribeAndSaveLeadGenBtn;

        public Boolean verifyLeadGenButtonAtFooter() {
            Browser.scrollDownToBottomOfPage();
            Browser.waitForElementToBeClickable(SubScribeAndSaveLeadGenBtn, 20);
            Boolean leadGenBtnFooer = SubScribeAndSaveLeadGenBtn.Exists();
            return leadGenBtnFooer;            
        }

        public void clickOnLeadGenButtonAtFooter()
        {
            Browser.scrollDownToBottomOfPage();
            Browser.waitForElementToBeClickable(SubScribeAndSaveLeadGenBtn, 20);
            Browser.javaScriptClick(SubScribeAndSaveLeadGenBtn);
        }

        public void ClickOnFAQLink()
        {            
            //Browser.waitForElementToDisplayed(FAQLink, 30);
            Browser.waitForElementToBeClickable(FAQLink, 20);
            Browser.javaScriptClick(FAQLink);
            //FAQLink.Click();
        }
        public void ClickOnCustomerServiceLink()
        {
            //Browser.waitForElementToDisplayed(CustomerServiceLink,30);
            Browser.waitForElementToBeClickable(CustomerServiceLink, 20);
            CustomerServiceLink.Click();
        }
        public void ClickOnBirthdayPartiesLink()
        {
            //Browser.waitForElementToDisplayed(BirthdayPartiesLink,30);
            Browser.waitForElementToBeClickable(BirthdayPartiesLink,20);
            BirthdayPartiesLink.Click();
        }
        public void ClickOnGroupAndEventsLink()
        {
            //Browser.waitForElementToDisplayed(GroupsAndEventsLink,30);
            Browser.waitForElementToBeClickable(GroupsAndEventsLink, 20);
            GroupsAndEventsLink.Click();
        }

        public void ClickOnTermsAndConditionLink()
        {
            //Browser.waitForElementToDisplayed(TermsAndConditionLink, 30);
            Browser.waitForElementToBeClickable(TermsAndConditionLink, 20);
            TermsAndConditionLink.Click();
        }

        public Boolean IsFAQPageDisplayed()
        {
            //Browser.SwitchToNewWindow();
            Browser.waitForElementToDisplayed(FAQPage, 20);
            Boolean FAQPageDisplayed = FAQPage.Exists();
            return FAQPageDisplayed;
        }

        public Boolean IsCustomerServicePageDisplayed()
        {
            //Browser.SwitchToNewWindow();
            Browser.waitForElementToDisplayed(CustomerServicePage, 20);
            Boolean CustomerServicePageDisplayed = CustomerServicePage.Exists();
            return CustomerServicePageDisplayed;
        }

        public Boolean IsBirthdayPartiesPageDisplayed()
        {
            //Browser.SwitchToNewWindow();
            Browser.waitForElementToDisplayed(BirthdayPartiesPage, 20);
            Boolean BirthdayPartiesPageDisplayed = BirthdayPartiesPage.Exists();
            return BirthdayPartiesPageDisplayed;
        }

        public Boolean IsGroupAndEventsPageDisplayed()
        {
            //Browser.SwitchToNewWindow();
            Browser.waitForElementToDisplayed(GroupsAndEventPage, 20);
            Boolean GroupAndEventsPageDisplayed = GroupsAndEventPage.Exists();
            return GroupAndEventsPageDisplayed;
        }

        public Boolean IsPrivacyPolicyAndTermsPageDisplayed()
        {
            Browser.waitForElementToDisplayed(PrivacyPolicyAndTermsPage, 20);
            Boolean PrivacyPolicyAndTermsPageDisplayed = PrivacyPolicyAndTermsPage.Exists();
            return PrivacyPolicyAndTermsPageDisplayed;
        }


    }
}
