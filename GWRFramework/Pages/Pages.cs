﻿using GWRFramework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TechTalk.SpecFlow;

namespace GWRFramework
{
    public static class Pages
    {

       // public static IWebDriver _driver;

        private static T GetPage<T>() where T : new()
        {
            var page = new T();
            IWebDriver driver = Browser._Driver;
            PageFactory.InitElements(Browser.Driver, page);

            return page;
        }

        public static PropertyHomePage PropertyHome
        {
            get { return GetPage<PropertyHomePage>(); }
        }

        public static GlobalHomePage GlobalHome
        {
            get { return GetPage<GlobalHomePage>(); }
        }

        public static SignInPage SignIn
        {
            get { return GetPage<SignInPage>(); }
        }

        public static CreateAccountPage CreateAccount
        {
            get { return GetPage<CreateAccountPage>(); }
        }

        public static DealSignUpPage DealSignUp
        {
            get { return GetPage<DealSignUpPage>(); }
        }

        public static SuitePage Suite
        {
            get { return GetPage<SuitePage>(); }
        }

        public static DealsPage Deals
        {
            get { return GetPage<DealsPage>(); }
        }

        public static DayPassPage DayPass
        {
            get { return GetPage<DayPassPage>(); }
        }

        public static PlanPage Plan
        {
            get { return GetPage<PlanPage>(); }
        }

        public static PaymentPage Payment
        {
            get { return GetPage<PaymentPage>(); }
        }

        public static NewConfirmationPage NewConfirmation
        {
            get { return GetPage<NewConfirmationPage>(); }
        }

        public static TopNavigations TopNavigation
        {
            get { return GetPage<TopNavigations>(); }
        }
        
        public static RateCalendar rateCalendar
        {
            get { return GetPage<RateCalendar>(); }
        }

        public static ActivitiesPage Activities
        {
            get { return GetPage<ActivitiesPage>(); }
        }

        public static DiningPage Dining
        {
            get { return GetPage<DiningPage>(); }
        }

        public static MyReservationPage MyReserv
        {
            get { return GetPage<MyReservationPage>(); }
        }

        public static AddAPackagePage AddAPackage
        {
            get { return GetPage<AddAPackagePage>(); }
        }

        public static SearchYourReservationPage SearchYourReservation
        {
            get { return GetPage<SearchYourReservationPage>(); }
        }

        public static MyProfilePage MyProfile
        {
            get { return GetPage<MyProfilePage>(); }

        }
        
        public static MyRecentSearchesPage MyRecentSearchesPage
        {
            get { return GetPage<MyRecentSearchesPage>(); }

        }

        public static DiningAndShoppingPage DiningAndShoppingPage
        {
            get { return GetPage<DiningAndShoppingPage>(); }
        }
        public static WPAPage WPAPage
        {
            get { return GetPage<WPAPage>(); }
        }
        
        public static HeaderSuitePage HeaderSuite
        {
            get { return GetPage<HeaderSuitePage>(); }

        }

        public static FooterSection Footer

        {
            get { return GetPage<FooterSection>(); }

        }

        public static ForgotPasswordPage ForgotPasswordPage
        {
            get { return GetPage<ForgotPasswordPage>(); }

        }

        public static GroupsPage GroupPage
        {
            get { return GetPage<GroupsPage>(); }

        }

    }
}
