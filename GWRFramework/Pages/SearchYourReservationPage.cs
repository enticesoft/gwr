﻿using GWRFramework.TestData;
using Microsoft.Office.Interop.Excel;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GWRFramework
{
    public class SearchYourReservationPage

    {
        [FindsBy(How = How.XPath, Using = "//*[@id='sign-in-create-account']//*[text()='Find Reservation']")]
        public IWebElement findYrReservationLink;

        [FindsBy(How = How.Id, Using = "reservationId")]
        public IWebElement confirmationNumTx;

        [FindsBy(How = How.Id, Using = "lastName")]
        public IWebElement lastNameTx;

        [FindsBy(How = How.XPath, Using = "//button[text()='Search']")]
        public IWebElement searchBtn;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'We could not find your reservation')]")]
        public IWebElement coundNotFindReservationError;

        public static String waityouReservationSpinnerStr = "//*[contains(@transform,'rotate')]";

        [FindsBy(How = How.XPath, Using = "//*[@id='sign-in-create-account']//*[text()='Find Reservation']")]
        public IWebElement FindReservation;

        [FindsBy(How = How.XPath, Using = "//button[@title='Close']")]
        public IWebElement dealsSignUpCloseBtn;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(), 'Sign up for exclusive deals!')]")]
        public IWebElement dealsSignUpLabel;

        public void clickOnFindYourReservationLink()
        {
            if (Pages.PropertyHome.userSignedInMenu.Exists() == true)
            {
                Pages.PropertyHome.clickToSignOut();
            }
            Browser.waitForElementToDisplayed(findYrReservationLink, 20);
            Browser.waitForElementToBeClickable(findYrReservationLink, 20);
            findYrReservationLink.Click();
        }

        public void sendConfirmationNum(String confirmationNum)
        {
            Browser.waitForElementToDisplayed(confirmationNumTx, 15);
            confirmationNumTx.SendKeys(confirmationNum);
        }

        public void sendLastName(String lastName)
        {
            Browser.waitForElementToDisplayed(lastNameTx, 15);
            lastNameTx.SendKeys(lastName);
        }
        public void clickOnSearchBtn()
        {
            //Browser.waitForElementToDisplayed(searchBtn, 15);
            Browser.waitForElementToBeClickable(searchBtn, 15);
            searchBtn.Click();
            waitForFindYourResrSprinnerOut(waityouReservationSpinnerStr);
        }

        public Boolean verifyWeCoundNotFindReservationError()
        {

            waitForFindYourResrSprinnerOut(waityouReservationSpinnerStr);
            if (coundNotFindReservationError.Exists())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean verifyCorrectConfirmationNum(String ConfNum)
        {
            Browser.waitForElementToDisplayed(Pages.NewConfirmation.reservationNumber, 20);
            //waitForFindYourResrSprinnerOut(waityouReservationSpinnerStr);
            String splitNum = Browser.getText(Pages.NewConfirmation.reservationNumber);
            String ActualConfNum = splitNum.Substring(13);
            if (ActualConfNum == ConfNum)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void waitForFindYourResrSprinnerOut(String xpath)
        {
            //Browser.waitForImplicitTime(4);
            Browser.waitForElementToBeInvisible(20, xpath);
            //Browser.waitForImplicitTime(4);
        }

        public void getReserva()
        {
            DataAccess.putReservationData("11111", 1, 2);
        }

        public Boolean verifySignupPopUp()
        {
            Boolean SignUp = Pages.CreateAccount.firstNameTextbox.Exists();
            return SignUp;
        }

        public void ClickFindYourReservationtopLink()
        {
            Pages.PropertyHome.waitForDealsSignUpPopup();
            if (dealsSignUpLabel.Exists())
            {
                Browser.webDriver.Navigate().Refresh();
                if (dealsSignUpLabel.Exists())
                {
                    Browser.waitForElementToBeClickable(dealsSignUpCloseBtn, 20);
                    dealsSignUpCloseBtn.Click();
                }
                Browser.waitForElementToDisplayed(FindReservation, 20);

                FindReservation.Click();
            }

        }
    }
}
