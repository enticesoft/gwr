﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using MongoDB.Driver.Core.Operations;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;

namespace GWRFramework
{
    public class WPAPage
    {
        [FindsBy(How = How.XPath, Using = "//a[@class='is-menu-dropdown'][contains(text(),'Water Park')]|//a[@class='is-menu-dropdown'][contains(text(),'WATER PARK')]")]
        public IWebElement WPAMenu;

        [FindsBy(How = How.XPath, Using = "(//a[contains(text(),'Activities')])[1]")]
        public IWebElement ActivitiesSubmenu;

        [FindsBy(How = How.XPath, Using = "(//div[@class='grid-item__content'])[1]//a//h4")]
        public IWebElement ActivityLearnMorelink;
        

        [FindsBy(How = How.XPath, Using = "//a[contains(.,'Indoor Water Park')]")]
        public IWebElement SwimSplashSubmenu;

        [FindsBy(How = How.XPath, Using = "(//div[@class='park-attractions-list__text']//span[text()='See More Details'])[1]")]
        public IWebElement SeeMoreDetailsLink;

        [FindsBy(How = How.XPath, Using = "//a[@role='tab']//h4[text()='View Safety Rules']")]
        public IWebElement ViewSafetyLink;       


        [FindsBy(How = How.XPath, Using = "(//a[contains(text(),'Attractions')])[1]")]
        public IWebElement AttractionsSubmenu;

       public static String AttractionsSubmenu1 = "(//a[contains(text(),'Attractions')])[1]";  

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Fitness')]")]
        public IWebElement FitnessSubmenu;

        [FindsBy(How = How.XPath, Using = "(//a[contains(text(),'Explore By Age')])[1]")]
        public IWebElement ExploreByAgeSubmenu;

        [FindsBy(How = How.XPath, Using = "(//a[contains(text(),'Calendar')])[1]")]
        public IWebElement CalendarSubmenu;

        
        [FindsBy(How = How.XPath, Using = "//h1[contains(text(),'Activities')]")]
        public IWebElement ActivitiesPageHeader;

        [FindsBy(How = How.XPath, Using = "//h1[contains(text(),'Attractions')]")]
        public IWebElement AttractionsPageHeader;

        [FindsBy(How = How.XPath, Using = "//h1[@class='details-venue__title']")]
        public IWebElement FitnessPageHeader;

        [FindsBy(How = How.XPath, Using = "//h1[contains(text(),'Indoor')]")]
        public IWebElement SwimSplashPageHeader;

        [FindsBy(How = How.XPath, Using = "//h1[contains(text(),'Event Calendar')]")]
        public IWebElement CalendarPageHeader;

        [FindsBy(How = How.XPath, Using = "//h1[contains(text(),'Explore By Age')]")]
        public IWebElement ExploreByAgePageHeader;

        [FindsBy(How = How.XPath, Using = "//h5[contains(text(),'Activities')]")]
        public IWebElement ActivitiesPkgTag;

        public String LearnMore = "//a[contains(text(),'Learn More')]";

        [FindsBy(How = How.XPath, Using = "(//article[@style='display: flex;'])[1]//h4")]
        public IWebElement LearnMoreLink;


        public String ActivitiesPkgCount = "//h5[contains(text(),'Activities')][1]";

        public String AttractionsPkgCount = "//h5[contains(text(),'Attractions')][1]";

        public String FitnessPkgCount = "//h4[contains(text(),'Fitness & Spa')]";

        public String SwimSplashPkgCount = "//h4[contains(text(),'Swim')]";

        [FindsBy(How = How.XPath, Using = "(//div[@class='activity-grid-item__badge--is-included-container'])[1]")]
        public IWebElement ActivitiesTag;

        [FindsBy(How = How.XPath, Using = "(//div[@class='attraction-grid-item__badge--is-included-container'])[1]")]
        public IWebElement AttractionsTag;

        [FindsBy(How = How.XPath, Using = "(//div[@class='fitness-spa-grid-item__badge--is-included-container'])[1]")]
        public IWebElement FitnessTag;

        [FindsBy(How = How.XPath, Using = "(//h4[@class='grid-item__category h5'])[1]")]
        public IWebElement SwimSplashTag;

        [FindsBy(How = How.XPath, Using = "(//label[@class='option'])[1]")]
        public IWebElement FamilyChBox;

        [FindsBy(How = How.XPath, Using = "(//label[@class='option'])[2]")]
        public IWebElement AdultsChBox;

        [FindsBy(How = How.XPath, Using = "(//label[@class='option'])[3]")]
        public IWebElement TeensChBox;

        [FindsBy(How = How.XPath, Using = "(//label[@class='option'])[4]")]
        public IWebElement ChildrenChBox;

        [FindsBy(How = How.XPath, Using = "(//label[@class='option'])[5]")]
        public IWebElement TodlersChBox;

        
        [FindsBy(How = How.XPath, Using = "//div[@class='icon']//following::span[contains(text(),'Adult')]")]
        public IWebElement AdultIcon;

        [FindsBy(How = How.XPath, Using = "//div[@class='icon']//following::span[contains(text(),'Family')]")]
        public IWebElement FamilyIcon;

        [FindsBy(How = How.XPath, Using = "//div[@class='icon']//following::span[contains(text(),'Teens')]")]
        public IWebElement TeensIcon;

        [FindsBy(How = How.XPath, Using = "//div[@class='icon']//following::span[contains(text(),'Children')]")]
        public IWebElement ChildrenIcon;

        [FindsBy(How = How.XPath, Using = "//div[@class='icon']//following::span[contains(text(),'Toddlers')]")]
        public IWebElement TodlersIcon;

        [FindsBy(How = How.XPath, Using = "//h1[@class='details-waterpark__title']")]
        public IWebElement T3Title;

        [FindsBy(How = How.XPath, Using = "//h1[@class='details-venue__title']")]
        public IWebElement T3TitleActivtiy;
        

        [FindsBy(How = How.XPath, Using = "//h3[contains(.,'Hours')]")]
        public IWebElement T3HoursSection;

        [FindsBy(How = How.XPath, Using = "(//div[@class='flex-viewport'])[2]")]
        public IWebElement T3ThumbnailsSection;

        [FindsBy(How = How.XPath, Using = "(//div[@class='details-venue__sidebar--greatfor'])")]
        public IWebElement T3BestForSection;        

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'cell small-12 medium-8 details-waterpark__desc')]")]
        public IWebElement T3WaterPrkDescription;

        [FindsBy(How = How.XPath, Using = "//div[@class='accordion-content'][@aria-hidden='false']")]
        public IWebElement ExpandSafetyRuleLink;


        public bool VerifySafetyRuleSec()
        {
            Browser.scrollVerticalBy("500");
            Browser.waitForElementToBeClickable(ExpandSafetyRuleLink, 10);
            return ExpandSafetyRuleLink.Exists();
        }

        public bool VerifyT3Title()
        {
            Browser.waitForElementToBeClickable(T3Title, 20);
            return T3Title.Exists();
        }

        public bool VerifyT3BestForSec()
        {
            Browser.waitForElementToBeClickable(T3BestForSection, 20);
            return T3BestForSection.Exists();
        }
        
        public bool VerifyT3TitleActivity()
        {
            Browser.waitForElementToBeClickable(T3TitleActivtiy, 20);
            return T3TitleActivtiy.Exists();
        }
        

        public bool VerifyT3HoursSection()
        {
            return T3HoursSection.Exists();
        }

        public bool VerifyT3ThumbSection()
        {
            return T3ThumbnailsSection.Exists();
        }

        public bool VerifyT3WaterParkSection()
        {
            return T3WaterPrkDescription.Exists();
        }

        public void ClickOnWPAMenu()
        {
            Browser.waitForElementToDisplayed(WPAMenu, 20);
            Actions actions = new Actions(Browser.driver);
            actions.MoveToElement(WPAMenu).Perform();

        }

        public bool VerifySwimSplashSubmenu()
        {
            return SwimSplashSubmenu.Exists();     
        }
        public bool VerifyActivitiesSubmenu()
        {
            return ActivitiesSubmenu.Exists();
        }
        public bool VerifyAttractionSubmenu()
        {
            return AttractionsSubmenu.Exists();
        }
        public bool VerifyFitnessSubmenu()
        {
            return FitnessSubmenu.Exists();
        }
        public bool VerifyExploreAgeSubmenu()
        {      
            return ExploreByAgeSubmenu.Exists();
        }
        public bool VerifyCalendarSubmenu()
        {           
            return CalendarSubmenu.Exists();
        }

        public void ClickOnLearMoreLink()
        {
            Browser.waitForElementToBeClickable(ActivityLearnMorelink, 20);
            ActivityLearnMorelink.Click();
        }
        
        public void NavigateToActivitiesPage()
        {
            Browser.waitForElementToBeClickable(ActivitiesSubmenu, 20);
            ActivitiesSubmenu.Click();        
        }

        public Boolean VerifyActivitiesPageOpened()
        {
            Browser.waitForElementToDisplayed(ActivitiesPageHeader, 20);
        
            if (ActivitiesPageHeader.Displayed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean VerifyTotalPkgAndActivityPkgCount()
        {
            int TotalPkgCount = Browser.driver.FindElements(By.XPath(LearnMore)).Count;
            
            int ActivitCount = Browser.driver.FindElements(By.XPath(ActivitiesPkgCount)).Count;
            if (TotalPkgCount == ActivitCount)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        public void ClickOnAttractionSubmenu()
            {
            Browser.waitForElementToBeClickable(AttractionsSubmenu, 20);
            AttractionsSubmenu.Click();
            
            }

        public Boolean VerifyAttractionPageOpend()
        {
            Browser.waitForElementToDisplayed(AttractionsPageHeader, 20);
            if (AttractionsPageHeader.Displayed)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean VerifyTotalPkgAndAttractionsPkgCount( )
        {
            int TotalPkgCount = Browser.driver.FindElements(By.XPath(LearnMore)).Count;
            int AttractionCount = Browser.driver.FindElements(By.XPath(AttractionsPkgCount)).Count;
            if (TotalPkgCount == AttractionCount)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void ClickOnFitnessSubmenu()
        {
            Browser.waitForElementToBeClickable(FitnessSubmenu, 20);
            FitnessSubmenu.Click();

        }

        public Boolean VerifyFitnessPageOpened()
        {
            Browser.waitForElementToDisplayed(FitnessPageHeader, 20);
            if (FitnessPageHeader.Displayed)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean VerifyTotalPkgAndFitnessPkgCount()
        {
            int TotalPkgCount = Browser.driver.FindElements(By.XPath(LearnMore)).Count;
            int FitnessCount = Browser.driver.FindElements(By.XPath(FitnessPkgCount)).Count;
            if (TotalPkgCount == FitnessCount)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void ClickOnSwimSplashSubmenu()
        {
            Browser.waitForElementToBeClickable(SwimSplashSubmenu, 20);
            SwimSplashSubmenu.Click();        
        }

        public void ClickOnSeeMoreDetails()
        {
            Browser.waitForElementToBeClickable(SeeMoreDetailsLink, 20);
            Browser.javaScriptClick(SeeMoreDetailsLink);
            //SeeMoreDetailsLink.Click();
        }

        public void ClickOnViewStaftyLink()
        {
            Browser.waitForElementToBeClickable(ViewSafetyLink, 20);
            //ViewSafetyLink.Click();
            Browser.javaScriptClick(ViewSafetyLink);
        }
        

        public Boolean VerifySwimSplashPageOpened()
        {
            Browser.waitForElementToDisplayed(SwimSplashPageHeader, 20);

            if (SwimSplashPageHeader.Displayed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ClickOnCalendarSubmenu()
        {
            Browser.waitForElementToBeClickable(CalendarSubmenu, 20);
            CalendarSubmenu.Click();
        }
        public Boolean VerifyCalendarPageOpened()
        {
            Browser.waitForElementToDisplayed(CalendarPageHeader, 20);
            if (CalendarPageHeader.Displayed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ClickOnExploreByAgeSubmenu()
        {
            Browser.waitForElementToBeClickable(ExploreByAgeSubmenu, 20);
            ExploreByAgeSubmenu.Click();

        }

        public Boolean VerifyExploreByAgePageOpened()
        {
            Browser.waitForElementToDisplayed(ExploreByAgePageHeader, 20);
            if (ExploreByAgePageHeader.Displayed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean VerifyTotalPkgAndSwimSplashPkgCount()
        {
            int TotalPkgCount = Browser.driver.FindElements(By.XPath(LearnMore)).Count;
            int SwimSplashCount = Browser.driver.FindElements(By.XPath(SwimSplashPkgCount)).Count;
            if (TotalPkgCount == SwimSplashCount)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean VerifyExploreByAgePkg()
        {
            Browser.webDriver.Navigate().Refresh();
            if (!ActivitiesTag.Exists())
            {
                return false;
            }

            if (!AttractionsTag.Exists())
            {
                return false;
            }
           // Browser.scrollDownToBottomOfPage();

            /*if (!FitnessTag.Exists())
           {
               return false;
            }*/
            /*if (!SwimSplashTag.Exists())
            {
                return false;
            }*/

            return true;
        }

        public void UncheckAllCheckbox()
        {
            Browser.waitForElementToBeClickable(FamilyChBox,20);
            FamilyChBox.Click();
            Browser.waitForElementToBeClickable(AdultsChBox, 20);
            AdultsChBox.Click();
            Browser.waitForElementToBeClickable(TeensChBox, 20);
            TeensChBox.Click();
            Browser.waitForElementToBeClickable(ChildrenChBox, 20);
            ChildrenChBox.Click();
            Browser.waitForElementToBeClickable(TodlersChBox, 20);
            TodlersChBox.Click();
        }

        public void CheckAdultCheckbox()
        {
            Browser.waitForElementToBeClickable(AdultsChBox, 20);
            AdultsChBox.Click();
            //Browser.waitForImplicitTime(3000);
        }

        public void IClickLearnMoreLink()
        {
            Browser.waitForElementToBeClickable(LearnMoreLink, 20);
            LearnMoreLink.Click();
            //Browser.javaScriptClick(LearnMoreLink);
        }

        public bool VerifyAdultIcon()
        {
            return AdultIcon.Exists();
        }

        public void CheckChildrenCheckbox()
        {
            Browser.waitForElementToBeClickable(ChildrenChBox, 20);
            ChildrenChBox.Click();
            //Browser.waitForImplicitTime(3000);
        }
        public bool VerifyChildrenIcon()
        {
            return ChildrenIcon.Exists();
        }

        //
        public void CheckFamilyCheckbox()
        {
            Browser.waitForElementToBeClickable(FamilyChBox, 20);
            FamilyChBox.Click();
            //Browser.waitForImplicitTime(2000);

        }

        public bool VerifyFamilyIcon()
        {
            Browser.waitForElementToBeClickable(FamilyIcon,15);
            return FamilyIcon.Exists();

        }
        public void CheckTeensCheckbox()
        {
            Browser.waitForElementToBeClickable(TeensChBox, 20);
            TeensChBox.Click();
            //Browser.waitForImplicitTime(2000);

        }
        public bool VerifyTeensIcon()
        {
            Browser.scrollVerticalBy("500");
            Browser.waitForElementToBeClickable(TeensIcon, 20);
            return TeensIcon.Exists();

        }

        public void CheckToddlersCheckbox()
        {
            Browser.waitForElementToBeClickable(TodlersChBox, 20);
            TodlersChBox.Click();
            //Browser.waitForImplicitTime(2000);
        }
        public bool VerifyToddlersIcon()
        {
            Browser.waitForElementToBeClickable(TodlersIcon, 10);
            Browser.scrollVerticalBy("400");
            return TodlersIcon.Exists();
        }

    }
}
