﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using AventStack.ExtentReports.Utils;
using AventStack.ExtentReports.Utils;
using MongoDB.Driver.Core.Operations;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace GWRFramework
{
    public class DealsPage
    {


        public static String DealtitleT2Page = "";

        [FindsBy(How = How.XPath, Using = "//h1[text()='Special Offers']")]
        public IWebElement specialOffersLabel;

        [FindsBy(How = How.XPath, Using = "(//li//a[contains(.,'Deals')])[1]")]
        public IWebElement specialOffersLink;

        //[FindsBy(How = How.XPath, Using = "//*[@role='navigation']//a[contains(text(),'Special Offers')]/parent::li//a[contains(text(),'Deals')]")]
        //public IWebElement dealsLink;

        [FindsBy(How = How.XPath, Using = "(//li//a[contains(.,'Deals')])[2]")]
        public IWebElement dealsLink;

        [FindsBy(How = How.XPath, Using = "(//li//a[contains(.,'Birthday Parties')])[1]")]
        public IWebElement birthdayPartyLink;

        //[FindsBy(How = How.XPath, Using = "(//*[@class='exclusive-buttons']/a[contains(text(),'Sign In')])[1]")]
        //public IWebElement dealSignInLink;

        //[FindsBy(How = How.XPath, Using = "(//*[@class='grid-item__actions--sign-in-modal-button']//*[contains(text(),'Sign In')])[2]")]
        //public IWebElement dealSignInLinkStage;

        [FindsBy(How = How.XPath, Using = "(//article[not(contains(@style,'display: none;'))]//*[@class='grid-item__actions--sign-in-modal-button'])[1]")]
        public IWebElement dealSignInLinkNew;

        [FindsBy(How = How.XPath, Using = "(//div[@class='grid-item__wrapper']//div[@class='grid-item__content']//a)[1]")]
        public IWebElement GetUrl;


        //[FindsBy(How = How.XPath, Using = "(//a[@class='grid-item__actions--sign-up-modal-button button'])[2]")]
        //public IWebElement dealSignUpNowBtn;

        [FindsBy(How = How.XPath, Using = "(//article[not(contains(@style,'display: none;'))]//a[@class='grid-item__actions--sign-up-modal-button button'])[1]")]
        public IWebElement dealSignUpNowBtnNew;

        //[FindsBy(How = How.XPath, Using = "(//a[@class='grid-item__actions--sign-up-modal-button button'])[1]")]
        //public IWebElement dealSignUpNowBtnStage;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'for Early Saver Deal')]/parent::a[contains(text(),'APPLY CODE')]")]
        public IWebElement esaverApplyCodeBtn;

        String applyCodeBtn = "//*[contains(@data-url,'";
        String applyCodeBtn1 = "')]";

        [FindsBy(How = How.XPath, Using = "//*[@class='grid-item__actions']")]
        public IWebElement ApplyCodeDealsPage;

        [FindsBy(How = How.XPath, Using = "(//article[not(contains(@style,'display: none;'))]//p[@class='overlay-see-more-layer'])[1]")]
        public IWebElement learnMoreLink;

        [FindsBy(How = How.XPath, Using = "(//article[not(contains(@style,'display: none;'))]//a[@class='grid-item__actions--learn-more'])[1]/ancestor::article//*[@class='grid-item__headline grid-item__underline']")]
        public IWebElement dealtitleT2Page;

        [FindsBy(How = How.XPath, Using = "(//h1[@class='deals-hero__headline'])[1]")]
        public IWebElement dealtitleT3Page;

        [FindsBy(How = How.XPath, Using = "((//*[@class='grid-item__actions--sign-in-modal-button'])[2]//*[contains(text(),'Sign In')])[1]/ancestor::article//*[@class='grid-item__headline grid-item__underline']")]
        public IWebElement SignInDealTitleLink;

        [FindsBy(How = How.XPath, Using = "((//*[@class='grid-item__actions--sign-in-modal-button'])[1]//*[contains(text(),'Sign In')])[1]/ancestor::article//*[@class='grid-item__headline grid-item__underline']")]
        public IWebElement SignInDealTitleLinkStage;

        [FindsBy(How = How.XPath, Using = "//a[@class='sign-in button']")]
        public IWebElement dealSignInBtnT3Page;

        //[FindsBy(How = How.XPath, Using = "(//a[@class='sign-in button'])[1]")]
        //public IWebElement dealSignInBtnT3Page;

        [FindsBy(How = How.XPath, Using = "(//strong[text()='Offer Code']/parent::p/following-sibling::p[1])[1]")]
        public IWebElement offercodeT3Pg;

        [FindsBy(How = How.XPath, Using = "//*[@class='grid-item__headline grid-item__underline'][contains(text(),'Early Saver Deal')]")]
        public IWebElement ESAVERSignInDealTitleLink;

        [FindsBy(How = How.XPath, Using = "//*[@name='offerCode']")]
        public IWebElement OfferCodeFld;

        [FindsBy(How = How.XPath, Using = "//input[@name='totalGuests']")]
        public IWebElement totalGuestsFld;

        [FindsBy(How = How.XPath, Using = "//*[text()='ADULTS']/parent::div/following::div[1]//button[2]")]
        public IWebElement AdultsPlusBtn;

        [FindsBy(How = How.XPath, Using = "//*[text()='ADULTS']/parent::div/following::div[1]//input")]
        public IWebElement AdultCntTxt;

        [FindsBy(How = How.XPath, Using = "//*[text()='ADULTS']/parent::div/following::div[1]//button[1]")]
        public IWebElement AdultsMinusBtn;

        [FindsBy(How = How.XPath, Using = "//label[text()='First Name']//following-sibling::input")]
        public IWebElement firstNameTxt;

        [FindsBy(How = How.XPath, Using = "//label[text()='Last Name']//following-sibling::input")]
        public IWebElement lastNameTxt;

        [FindsBy(How = How.XPath, Using = "//label[text()='Address']//following-sibling::input")]
        public IWebElement addressTxt;

        [FindsBy(How = How.XPath, Using = "//label[text()='Postal Code']//following-sibling::input")]
        public IWebElement postalCodeTxt;

        [FindsBy(How = How.XPath, Using = "//label[text()='City']//following-sibling::input")]
        public IWebElement cityTxt;

        [FindsBy(How = How.XPath, Using = "//label[text()='Email']//following-sibling::input")]
        public IWebElement emailTxt;

        [FindsBy(How = How.XPath, Using = "//label[text()='Phone']//following-sibling::input")]
        public IWebElement phoneTxt;

        [FindsBy(How = How.XPath, Using = "//label[text()='Preferred Contact Method']//following-sibling::select")]
        public IWebElement ContactMethodDrp;

        [FindsBy(How = How.XPath, Using = "//label[text()='Country']//following-sibling::select")]
        public IWebElement ContryDrp;

        [FindsBy(How = How.XPath, Using = "//label[text()='Number of Children, 3 Years and Older']//following-sibling::select//option")]
        public IWebElement NumOfChildDrpVal;


        [FindsBy(How = How.XPath, Using = "(//input[@value='MDY']//parent::div[@class='fieldset-content']//select)[1]")]
        public IWebElement monthDrp;

        [FindsBy(How = How.XPath, Using = "(//input[@value='MDY']//parent::div[@class='fieldset-content']//select)[2]")]
        public IWebElement dateDrp;

        [FindsBy(How = How.XPath, Using = "(//input[@value='MDY']//parent::div[@class='fieldset-content']//select)[3]")]
        public IWebElement yearDrp;

        [FindsBy(How = How.XPath, Using = "//input[@type='radio'][@value='Yes']")]
        public IWebElement underChildrenYesOption;

        [FindsBy(How = How.XPath, Using = "//input[@type='radio'][@value='No']")]
        public IWebElement underChildrenNoOption;

        [FindsBy(How = How.XPath, Using = "(//label[contains(.,'s First Name')]//following-sibling::input)[1]")]
        public IWebElement childFName;

        [FindsBy(How = How.XPath, Using = "(//label[contains(.,'s Last Name')]//following-sibling::input)[1]")]
        public IWebElement childLName;

        [FindsBy(How = How.XPath, Using = "(//input[@value='MDY']//parent::div[@class='fieldset-content']//select)[4]")]
        public IWebElement childBirthMonth;

        [FindsBy(How = How.XPath, Using = "(//input[@value='MDY']//parent::div[@class='fieldset-content']//select)[5]")]
        public IWebElement childBirthDate;

        [FindsBy(How = How.XPath, Using = "(//input[@value='MDY']//parent::div[@class='fieldset-content']//select)[6]")]
        public IWebElement childBirthYear;

        [FindsBy(How = How.XPath, Using = "//input[@value='Submit Form']")]
        public IWebElement SubmitFormBtn;


        [FindsBy(How = How.XPath, Using = "//p//strong[text()='Promo Code:']")]
        public IWebElement PromoCodeOnT3;

        [FindsBy(How = How.XPath, Using = "(//div[@class='deals-hero__dates']//span[@class='deals-hero__dates--title'][text()='Promo Code: '])[1]")]
        public IWebElement offercodeONT3Page;

        [FindsBy(How = How.XPath, Using = "//p[@class='stay-date'][@data-stay-end-date]")]
        public IWebElement StayEndDate;

        [FindsBy(How = How.XPath, Using = "(//div[@aria-label='Best Available Rate']//div[contains(text(),'Promo Code Applied')])[1]//following-sibling::div")]
        public IWebElement appliedOfferCodeOnSuite;
        


        public static String PromoCode;
        public static String offerCodeValueBkEngn;

        public Boolean offerCodeOnRT3Detail()
        {
            if (dealSignInBtnT3Page.Exists())
            {
                dealSignInBtnT3Page.Click();
                Pages.SignIn.sendEmail("qatester301+1@gmail.com");
                Pages.SignIn.sendPass("$Reset123$");

                Browser.waitForElementToBeClickable(Pages.SignIn.signInBtn, 10);
                Pages.SignIn.signInBtn.Click();
                Pages.SignIn.waitSignInSprinnerOut(SignInPage.waitSignInSpinnerStr);

                return offercodeONT3Page.Exists();
            }
            else
            {
                Browser.waitForElementToBeClickable(Pages.Suite.OfferCodeFld, 20);
                return offercodeONT3Page.Exists();
            }
        }

        public void getEndDateOfercodeAvailbilty() {
            String endDate = StayEndDate.GetAttribute("data-stay-end-date");
            Pages.Suite.selectCheckIn(endDate);
            Pages.Suite.SelectAdults(1);
            offerCodeValueBkEngn = Pages.Suite.OfferCodeFld.GetAttribute("value");
            Pages.Suite.clickBookNowBtn();
        }
        
        public Boolean appliedOfrCode()
        {
            String offerCd = appliedOfferCodeOnSuite.Text;
            return offerCd.Equals(offerCodeValueBkEngn);
        }

        public Boolean offerCodeOnSholdNotOnT3Detail()
        {
            if (dealSignInBtnT3Page.Exists())
            {
                Browser.waitForElementToBeClickable(Pages.Suite.OfferCodeFld, 20);
                String offerCodeValue = Pages.Suite.OfferCodeFld.GetAttribute("value");
                return offerCodeValue.Equals("");
            }
            else
            {
                return true;
            }
        }

        public Boolean PromoSecNotShow()
        {
            if (dealSignInBtnT3Page.Exists())
            {
                if (PromoCodeOnT3.Exists())
                {
                    return false;
                }
                else { return true; }
            }
            else {
                if (PromoCodeOnT3.Exists())
                {
                    return true;
                }
                else { return true; }
            }
        }

        public void BirthdayPatyFormContactInfo(String Fname, String Lname, String Address, String Pcode, String City, String Email, String Phone, String ContactMeth, String Contry)
        {
            Browser.waitForElementToBeClickable(firstNameTxt, 20);
            firstNameTxt.SendKeys(Fname);

            Browser.waitForElementToBeClickable(lastNameTxt, 10);
            lastNameTxt.SendKeys(Lname);

            Browser.waitForElementToBeClickable(addressTxt, 10);
            addressTxt.SendKeys(Address);

            Browser.waitForElementToBeClickable(postalCodeTxt, 10);
            postalCodeTxt.SendKeys(Pcode);

            Browser.waitForElementToBeClickable(cityTxt, 10);
            cityTxt.SendKeys(City);

            Browser.waitForElementToBeClickable(emailTxt, 10);
            emailTxt.SendKeys(Email);

            Browser.waitForElementToBeClickable(phoneTxt, 10);
            phoneTxt.SendKeys(Phone);

            Browser.waitForElementToBeClickable(ContactMethodDrp, 10);
            SelectElement oSelect = new SelectElement(ContactMethodDrp);
            oSelect.SelectByValue(ContactMeth);

            if (Contry == "Canada")
            {
                Browser.waitForElementToBeClickable(ContryDrp, 10);
                SelectElement oSelect1 = new SelectElement(ContryDrp);
                oSelect1.SelectByValue(Contry);
            }

        }


        public Boolean ChildDrpValueVerify()
        {
            Browser.waitForElementToBeClickable(NumOfChildDrpVal, 20);
            int drpCnt = Browser.driver.FindElements(By.XPath("//label[text()='Number of Children, 3 Years and Older']//following-sibling::select//option")).Count;
            if (drpCnt == 13)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean AdultDrpValueVerify()
        {
            Browser.waitForElementToBeClickable(NumOfChildDrpVal, 20);
            int drpCnt = Browser.driver.FindElements(By.XPath("//label[text()='Number of Adults']//following-sibling::select//option")).Count;
            if (drpCnt == 7)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void BirthdayPartDetailsForm(String MM, String DD, String YY, String ChildOption)
        {
            Browser.waitForElementToBeClickable(monthDrp, 10);
            SelectElement oSelect = new SelectElement(monthDrp);
            oSelect.SelectByValue(MM);

            Browser.waitForElementToBeClickable(dateDrp, 10);
            SelectElement DDVal = new SelectElement(dateDrp);
            DDVal.SelectByValue(DD);

            Browser.waitForElementToBeClickable(yearDrp, 10);
            SelectElement YYVal = new SelectElement(yearDrp);
            YYVal.SelectByValue(YY);

            if (ChildOption == "No")
            {
                Browser.waitForElementToBeClickable(underChildrenNoOption, 10);
                underChildrenNoOption.Click();
            }

            if (ChildOption == "Yes")
            {
                Browser.waitForElementToBeClickable(underChildrenYesOption, 10);
                underChildrenYesOption.Click();
            }

        }

        public void WhoIsBirthdayChildForm(String ChildFname, String ChildLname, String ChMM, String ChDD, String ChYY)
        {
            Browser.waitForElementToBeClickable(childFName, 10);
            childFName.SendKeys(ChildFname);

            Browser.waitForElementToBeClickable(childLName, 10);
            childLName.SendKeys(ChildLname);

            Browser.waitForElementToBeClickable(childBirthMonth, 10);
            SelectElement oSelect = new SelectElement(childBirthMonth);
            oSelect.SelectByValue(ChMM);

            Browser.waitForElementToBeClickable(childBirthDate, 10);
            SelectElement DD = new SelectElement(childBirthDate);
            DD.SelectByValue(ChDD);

            Browser.waitForElementToBeClickable(childBirthYear, 10);
            SelectElement YY = new SelectElement(childBirthYear);
            YY.SelectByValue(ChYY);
        }

        public void clickOnSubmitFormBtn()
        {
            Browser.waitForElementToBeClickable(SubmitFormBtn, 20);
            SubmitFormBtn.Click();
        }

        public Boolean checkDealsPageDisplayed()
        {
            Browser.waitForImplicitTime(4);
            //Browser.waitForElementToDisplayed(specialOffersLabel, 30);
            bool dealsPage = specialOffersLabel.Exists() || Pages.Deals.dealtitleT3Page.Exists();
            return dealsPage;
        }

        public void naviagteOnSpecialOffersLink()
        {
            //Browser.waitForElementToDisplayed(specialOffersLink, 30);
            //  Browser.scrollToAnElement(specialOffersLink);
            Browser.waitForElementToBeClickable(specialOffersLink, 20);
            Actions actions = new Actions(Browser.driver);
            actions.MoveToElement(specialOffersLink).Perform();
            //specialOffersLink.Click();

        }

        public void clickOnDealsLink()
        {
            Browser.waitForElementToBeClickable(dealsLink, 20);
            //Actions actions = new Actions(Browser.driver);
            //actions.MoveToElement(dealsLink).Perform();
            dealsLink.Click();
        }

        public void clickOnBirthdayPartiesLink()
        {
            Browser.waitForElementToBeClickable(birthdayPartyLink, 20);
            //Actions actions = new Actions(Browser.driver);
            //actions.MoveToElement(birthdayPartyLink).Perform();
            birthdayPartyLink.Click();
        }

        public void clickOnDealSignInLink()
        {
            Browser.waitForElementToDisplayed(dealSignInLinkNew, 10);
            Browser.javaScriptClick(dealSignInLinkNew);
            //Browser.waitForElementToBeClickable(dealSignInLink, 20);
            /*String url = GetUrl.GetAttribute("href");
            if (url.Contains("stage"))
            {
                //dealSignInLinkStage.Click();
                Browser.javaScriptClick(dealSignInLinkStage);
            }
            else
            {
                //dealSignInLink.Click();
                Browser.javaScriptClick(dealSignInLinkStage);
            }*/

        }
        public void clickOnDealSignUpBtn()
        {
            Browser.waitForElementToDisplayed(dealSignUpNowBtnNew, 5);
            Browser.javaScriptClick(dealSignUpNowBtnNew);

            //Browser.waitForElementToBeClickable(dealSignUpNowBtn, 20);
            //dealSignUpNowBtn.Click();

            /*String url = GetUrl.GetAttribute("href");
            if (url.Contains("stage"))
            {
                //dealSignUpNowBtnStage.Click();
                Browser.javaScriptClick(dealSignUpNowBtnStage);
            }
            else
            {
                //dealSignUpNowBtn.Click();
                Browser.javaScriptClick(dealSignUpNowBtn);
            }*/


        }

        public Boolean verifyDealSignUpBtn()
        {
            /*if (Pages.PropertyHome.userSignedInMenu.Exists() == true)
            {
                Pages.PropertyHome.clickToSignOut();
            }*/
            bool SignupBtn = dealSignUpNowBtnNew.Exists();
            return SignupBtn;
        }

        /*   public void clickOnEsaverApplyCodeBtn()
           {
               Browser.waitForElementToDisplayed(esaverApplyCodeBtn, 30);
               Browser.waitForElementToBeClickable(esaverApplyCodeBtn, 30);
               esaverApplyCodeBtn.Click();
           }

           public Boolean verifyBtn()
           {
               bool SignupBtn = dealSignUpNowBtn.Exists();
               return SignupBtn;
           }*/

        public void clickOnApplyCodeBtn(String Offercode)
        {
            Browser.waitForImplicitTime(5);
            String applycodebutton = applyCodeBtn + Offercode + applyCodeBtn1;
            IWebElement ApplyCodeBtn = ApplyCodeDealsPage.FindElement(By.XPath(applycodebutton));
            ApplyCodeBtn.Click();
        }

        public Boolean verifyOffercodeUnderBookingEngine(String Offercode)
        {

            Browser.waitForElementToDisplayed(Pages.Suite.OfferCodeFld, 20);
            //  Browser.clearTextboxValueACtion(Pages.Suite.OfferCodeFld);
            String offerCodeValue = Pages.Suite.OfferCodeFld.GetAttribute("value");
            Boolean OfferCdValue = offerCodeValue.Equals(Offercode);
            return OfferCdValue;
        }

        public Boolean verifySignUpNowBtnText(string signup)
        {
            if (Pages.PropertyHome.userSignedInMenu.Exists() == true)
            {
                Pages.PropertyHome.clickToSignOut();
            }

            string SignupBtn = dealSignUpNowBtnNew.Text;
            return SignupBtn.Equals(signup);
            //Browser.waitForElementToDisplayed(dealSignUpNowBtnNew, 5);
            //Browser.javaScriptClick(dealSignUpNowBtnNew);
            /*String url = GetUrl.GetAttribute("href");
            if (url.Contains("stage"))
            {
                //dealSignUpNowBtnStage.Click();
                string SignupBtn = dealSignUpNowBtnStage.Text;
                return SignupBtn.Equals(signup);
            }
            else
            {
                string SignupBtn = dealSignUpNowBtn.Text;
                return SignupBtn.Equals(signup);
            }*/


        }

        public void clickOnLearnMoreLink()
        {
            Browser.waitForElementToDisplayed(learnMoreLink, 10);
            Actions actions = new Actions(Browser.driver);
            actions.MoveToElement(learnMoreLink).Perform();

            
            DealtitleT2Page = dealtitleT2Page.Text;
            Browser.javaScriptClick(learnMoreLink);
            //learnMoreLink.Click();
        }

        public Boolean VerifyDealspkgDetailPage()
        {
            Browser.waitForElementToDisplayed(dealtitleT3Page, 20);
            if (dealtitleT3Page.Exists())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean VerifyDealspkgDetailPageIfDealBarAvailable()
        {
            if (PropertyHomePage.dealCompoOnHome == true)
            {
                Browser.waitForElementToDisplayed(dealtitleT3Page, 20);
                if (dealtitleT3Page.Exists())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else { return true; }
        }

        public void clickOnSignInDealTitleLink()
        {
            if (Pages.PropertyHome.userSignedInMenu.Exists() == true)
            {
                Pages.PropertyHome.clickToSignOut();
            }

            Thread.Sleep(4000);
            Browser.waitForElementToDisplayed(SignInDealTitleLink, 5);

            String url = GetUrl.GetAttribute("href");
            if (url.Contains("stage"))
            {
                //SignInDealTitleLinkStage.Click();
                Browser.javaScriptClick(SignInDealTitleLinkStage);
            }
            else
            {
                //SignInDealTitleLink.Click();
                Browser.javaScriptClick(SignInDealTitleLink);
            }

        }

        public Boolean VerifyOfferCodeFieldEmptyunderBookingEngine()
        {
            Browser.waitForElementToDisplayed(Pages.Suite.OfferCodeFld, 20);
            String offercode = Pages.Suite.OfferCodeFld.GetAttribute("value");
            if (offercode.IsNullOrEmpty())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void clickOnSignInBtnT3Page()
        {
            //Browser.waitForElementToDisplayed(dealSignInBtnT3Page, 30);
            Browser.waitForElementToBeClickable(dealSignInBtnT3Page, 20);
            dealSignInBtnT3Page.Click();
        }

        /*public void clickOnSignInBtnT3Page()
        {
            Browser.waitForElementToDisplayed(dealSignInBtnT3Page, 30);
            Browser.waitForElementToBeClickable(dealSignInBtnT3Page, 30);
            // OfferCodeT3Page = offercodeT3Pg.Text;
            dealSignInBtnT3Page.Click();
        }*/

        public Boolean VerifyOfferCodeunderBookingEngine()
        {
            Browser.waitForElementToDisplayed(Pages.Suite.OfferCodeFld, 20);
            String offercode = Pages.Suite.OfferCodeFld.GetAttribute("value");
            if (!offercode.IsNullOrEmpty())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean VerifySignInBtnT3Page()
        {
            Browser.waitForImplicitTime(3);
            bool SignInbtn = dealSignInBtnT3Page.Exists();
            return SignInbtn;
        }

        public void clickOnESAVERSignInDealTitleLink()
        {
            if (Pages.PropertyHome.userSignedInMenu.Exists() == true)
            {
                Pages.PropertyHome.clickToSignOut();
            }
            Browser.waitForElementToDisplayed(ESAVERSignInDealTitleLink, 20);
            Actions actions = new Actions(Browser.driver);
            actions.MoveToElement(ESAVERSignInDealTitleLink).Perform();
            Browser.waitForElementToBeClickable(ESAVERSignInDealTitleLink, 20);
            ESAVERSignInDealTitleLink.Click();
        }

        public void SelectAdults(int AdultValue)
        {
            //Browser.clearTextboxValue(AdultValue);
            //Pages.PropertyHome.closeDealsSignUp();
            OfferCodeFld.Click();
            Browser.waitForElementToDisplayed(totalGuestsFld, 20);
            Browser.waitForElementToBeClickable(totalGuestsFld, 20);

            totalGuestsFld.Click();

            String exstnfAdulCnt = AdultCntTxt.GetAttribute("value");
            int adults = Int32.Parse(exstnfAdulCnt);
            if (adults == 0 || adults == 1)
            {
                int adultmins = AdultValue - adults;
                for (int i = 1; i <= adultmins; i++)
                {
                    Browser.waitForElementToDisplayed(AdultsPlusBtn, 5);
                    AdultsPlusBtn.Click();
                    Thread.Sleep(1000);
                }
            }
            else
            {
                int existngAdultrem = adults - 1;
                for (int i = 1; i <= existngAdultrem; i++)
                {
                    Browser.waitForElementToDisplayed(AdultsMinusBtn, 5);
                    AdultsMinusBtn.Click();
                    Thread.Sleep(1000);
                }

                int addAdult = AdultValue - 1;
                for (int i = 1; i <= addAdult; i++)
                {
                    Browser.waitForElementToBeClickable(AdultsPlusBtn, 5);
                    AdultsPlusBtn.Click();
                    Thread.Sleep(1000);
                }
            }

        }
    }
}
