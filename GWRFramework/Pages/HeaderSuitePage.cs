﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWRFramework
{
    public class HeaderSuitePage
    {
        
        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Suites')]")]
        public IWebElement SuitesMenu;

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Premium')]")]
        public IWebElement PremiumType;

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Standard')]")]
        public IWebElement StandardType;

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Themed')]")]
        public IWebElement ThemedType;

        [FindsBy(How = How.XPath, Using = "//article[1]//div[@class='grid-item__actions--links']//a")]
        public IWebElement CheckAvailabilityButtonForPremiumSuite;

        [FindsBy(How = How.XPath, Using = "//article[1]//div[@class='grid-item__actions--links']//a")]
        public IWebElement CheckAvailabilityButtonForStandardSuite;

        [FindsBy(How = How.XPath, Using = "//article[1]//div[@class='grid-item__actions--links']//a")]
        public IWebElement CheckAvailabilityButtonForThemedSuite;

        [FindsBy(How = How.XPath, Using = "//h1[@class='details-suite__title']")]
        public IWebElement T3SuiteTitle;

        [FindsBy(How = How.XPath, Using = "//div//img[@class='floorplan']")]
        public IWebElement T3SuiteFloorPlan;

        [FindsBy(How = How.XPath, Using = "//a[contains(.,'Check Availability')]|//a[contains(.,'CHECK AVAILABILITY')]")]
        public IWebElement T3SuiteCheckAvaBtn;

        [FindsBy(How = How.XPath, Using = "//div[@class='details__text']")]
        public IWebElement T3SuiteSleepsSec;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'cell small-12 medium-8 details-suite__desc')]")]
        public IWebElement T3SuiteDescription;

        public void ClickOnSuitesMenu()
        {
            //Browser.waitForElementToDisplayed(SuitesMenu, 20);
            Browser.waitForElementToBeClickable(SuitesMenu, 20);
            SuitesMenu.Click();
        }

        public void ClickOnPremiumSuites()
        {
            //Browser.waitForElementToDisplayed(PremiumType, 20);
            Browser.waitForElementToBeClickable(PremiumType, 20);
            PremiumType.Click();
        }

        public void ClickOnCheckAvailabilityButton()
        {
           // Browser.waitForElementToDisplayed(CheckAvailabilityButtonForPremiumSuite, 20);
            Browser.waitForElementToBeClickable(CheckAvailabilityButtonForPremiumSuite, 20);
            CheckAvailabilityButtonForPremiumSuite.Click();
        }
        public void ClickOnStadardSuites()
        {
            //Browser.waitForElementToDisplayed(StandardType, 20);
            Browser.waitForElementToBeClickable(StandardType, 20);
            StandardType.Click();
        }

        public void ClickOnCheckAvailabilityButtonOfStandardSuite()
        {
            //Browser.waitForElementToDisplayed(CheckAvailabilityButtonForStandardSuite, 20);
            Browser.waitForElementToBeClickable(CheckAvailabilityButtonForStandardSuite, 20);
            CheckAvailabilityButtonForStandardSuite.Click();
        }

        public void ClickOnThemedSuites()
        {
            //Browser.waitForElementToDisplayed(ThemedType, 20);
            Browser.waitForElementToBeClickable(ThemedType, 20);
            ThemedType.Click();
        }
        public void ClickOnCheckAvailabilityButtonOfThemedSuite()
        {
            //Browser.waitForElementToDisplayed(CheckAvailabilityButtonForThemedSuite, 20);
            Browser.waitForElementToBeClickable(CheckAvailabilityButtonForThemedSuite, 20);
            CheckAvailabilityButtonForThemedSuite.Click();
        }

        public Boolean IsPremiumRelatedSuiteDisplayed()
        {
           // Browser.waitForElementToDisplayed(PremiumLabel, 20);
            IList<IWebElement> allElements = Browser.driver.FindElements(By.XPath("//h5[@class='grid-item__category']"));
            int elementsCount = allElements.Count;
            String premiumLabel;
            Boolean Result=true;
            foreach (IWebElement label in allElements)
            {
                 premiumLabel = label.Text;
                 Result=premiumLabel.Equals("premium");
                if (Result == false)
                { break; }
                 
            }
            return Result;

        }

        public Boolean IsThemedRelatedSuiteDisplayed()
        {
            
            IList<IWebElement> allElements = Browser.driver.FindElements(By.XPath("//h5[@class='grid-item__category']"));
            int elementsCount = allElements.Count;
            String themedLabel;
            Boolean Result = true;
            foreach (IWebElement label in allElements)
            {
                themedLabel = label.Text;
                Result = themedLabel.Equals("themed");
                if (Result == false)
                { break; }

            }
            return Result;
        }

        public Boolean IsStandardRelatedSuiteDisplayed()
        {
            
            IList<IWebElement> allElements = Browser.driver.FindElements(By.XPath("//h5[@class='grid-item__category']"));
            int elementsCount = allElements.Count;
            String standardLabel;
            Boolean Result = true;
            foreach (IWebElement label in allElements)
            {
                standardLabel = label.Text;
                Result = standardLabel.Equals("standard");
                if (Result == false)
                { break; }

            }
            return Result;
        }

        public bool VerifyT3TitleOFSuites()
        {
            Browser.waitForElementToBeClickable(T3SuiteTitle, 20);
            return T3SuiteTitle.Exists();
        }

        public bool VerifyT3Floorplan()
        {
            Browser.waitForElementToBeClickable(T3SuiteFloorPlan, 20);
            return T3SuiteFloorPlan.Exists();
        }

        public bool VerifyT3CheckAvaBtn()
        {
            Browser.waitForElementToBeClickable(T3SuiteCheckAvaBtn, 20);
            return T3SuiteCheckAvaBtn.Exists();
        }

        public void clickOnT3CheckAvaBtn()
        {
            Browser.scrollVerticalBy("300");
            Browser.waitForElementToBeClickable(T3SuiteCheckAvaBtn, 20);
            Browser.javaScriptClick(T3SuiteCheckAvaBtn);
        }

        public bool VerifyT3SleepSec()
        {
            Browser.waitForElementToBeClickable(T3SuiteSleepsSec, 20);
            return T3SuiteSleepsSec.Exists();
        }

        public bool VerifyT3SuiteDescription()
        {
            Browser.waitForElementToBeClickable(T3SuiteDescription, 20);
            return T3SuiteDescription.Exists();
        }
        
    }
}
