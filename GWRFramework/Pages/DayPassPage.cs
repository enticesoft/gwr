﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using GWRFramework.TestData;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using AventStack.ExtentReports;
using System.Diagnostics.CodeAnalysis;
using Dynamitey.DynamicObjects;

namespace GWRFramework
{
    public class DayPassPage
    {

        [FindsBy(How = How.XPath, Using = "//*[@id='root']/descendant::span[contains(text(),'PLAY FOR A DAY')]")]
        public IWebElement dayPassLabel;

        [FindsBy(How = How.XPath, Using = "//*[@role='navigation']//a[contains(text(),'Day Pass')]")]
        public IWebElement DayPassMenu;

        [FindsBy(How = How.XPath, Using = "//input[@id='promocode']")]
        public IWebElement DayPassPromoTextbox;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Full Day Retail Waterpark Pass')]")]
        public IWebElement DayPassPromoTerms;

        [FindsBy(How = How.XPath, Using = "//input[@id='date']")]
        public IWebElement DayPassArrivalDate;

        [FindsBy(How = How.XPath, Using = "//div[@role='button'][@aria-label='Move forward to switch to the next month.']")]
        public IWebElement DayPassNextMonthIcon;

        [FindsBy(How = How.XPath, Using = "//div[@class='CalendarMonthGrid_month__horizontal CalendarMonthGrid_month__horizontal_1']//tr[3]//td[2]")]
        public IWebElement DayPassDate;

        [FindsBy(How = How.XPath, Using = "//input[@id='input-numberOfGuests']")]
        public IWebElement NumberOfGuestField;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'Total')]")]
        public IWebElement DayPassTotalLabel;

        [FindsBy(How = How.XPath, Using = "//button[@type='submit'][text()='CHECKOUT']")]
        public IWebElement DayPassCheckoutButton;

        public static String checkOutBtn = "//button[@type='submit'][text()='CHECKOUT']";

        [FindsBy(How = How.XPath, Using = "//*[@id='root']//h1[text()='Payment']")]
        public IWebElement DayPassPaymentPage;

        [FindsBy(How = How.XPath, Using = "//*[@id='card-box']//*[contains(@title,'INVALID')]")]
        public IWebElement InvalidDayPassPromo;

        [FindsBy(How = How.XPath, Using = "//*[@class='accordion__item']")]
        public IWebElement NumberofPassesPaymentPage;

        String daypassqty = "(//div[@class='accordion__item'])[2]//div[text()='# of Day Passes']//following-sibling::div[text()='";
        String daypassqty1 = "']";

        [FindsBy(How = How.XPath, Using = "//*[@role='complementary']")]
        public IWebElement dayPassPromoOnPayment;

        String daypasspromo = "//*[@role='complementary']//div[contains(text(),'";
        String daypasspromo1 = "')]";

        [FindsBy(How = How.XPath, Using = "(//div[@class='accordion__item'])[2]//div[text()='Valid On']//following-sibling::div")]
        public IWebElement dayPassdDateOnPayment;

        [FindsBy(How = How.XPath, Using = "//div[text()='Unit Price']//preceding-sibling::span")]
        public IWebElement unitPriceDPpage;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Unit Price']//div[text()='$']")]
        public IWebElement unitPriceDPPayment;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Price per guest']//div[text()='$']")]
        public IWebElement unitPriceWithOutOfferCode;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Sub Total']//div[text()='$']")]
        public IWebElement dpSubTotalPayment;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Day Pass Total']//div[text()='$']")]
        public IWebElement DayPassTotalPayment;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Taxs and Fees']//div[text()='$']")]
        public IWebElement dpTaxesPayment;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Total Price']//div[text()='$']")]
        public IWebElement dpTotalPayment;

        [FindsBy(How = How.XPath, Using = "//*[@class='clickable-tiles__content__card-header']//div[text()='day passes']")]
        public IWebElement dpHomeTile;

        [FindsBy(How = How.XPath, Using = "//div[@id='card-box']//span[contains(text(),'$50.00')]")]
        public IWebElement fullDayPass;

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'UH OH! WE WANDERED TOO FAR OFF THE PATH.')]")]
        public IWebElement NotFounMessage;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Price per guest']//div[text()='$']")]
        public IWebElement fullDayPassPerGuestPayment;


        [FindsBy(How = How.XPath, Using = "//div[@title='ERROR']//li[text()='numberOfGuests must be greater than or equal to 1']")]
        public IWebElement numberOfGuestsError;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'4pm - close')]")]
        public IWebElement halfdayPassHours;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Your credit card will be charged')]/span")]
        public IWebElement YourCreditCardCharged;

        [FindsBy(How = How.XPath, Using = "//div[@id='card-box']//span[contains(text(),'$40.00')]")]
        public IWebElement HalfDayPass;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Price per guest']//div[text()='$']")]
        public IWebElement halfDayPassPerGuestPayment;

        [FindsBy(How = How.XPath, Using = "//button[@type='submit']")]
        public IWebElement AgreeAndBookbtn;

        [FindsBy(How = How.XPath, Using = "//section[@id='info-section']//p[text()='Reservation #']")]
        public IWebElement reservationNumber;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Account Options']//a[text()='My Reservations']")]
        public IWebElement myReservationMenuLink;

        [FindsBy(How = How.XPath, Using = "(//*[@id='root']//span)[1]")]
        public IWebElement DateOnConfirmation;

        [FindsBy(How = How.XPath, Using = "(//div[contains(text(),'Pass')]//following::div[5])[1]")]
        public IWebElement PassQty;

        [FindsBy(How = How.XPath, Using = "(//div[contains(text(),'# of Day Passes')]//following::div[1])[1]")]
        public IWebElement PassQtyOnPayemnt;


        [FindsBy(How = How.XPath, Using = "(//div[contains(text(),'Valid On')]//following::div[1])[1]")]
        public IWebElement DateOnDaypassPayment;

        [FindsBy(How = How.XPath, Using = "(//div[@aria-label='Summary Item Promo Code']//following::div[5])[1]")]
        public IWebElement DayPassPromo;

        public string NumReservation = "//div[text()[contains(.,'*Reservation1*')]]";

        public string Viewdetails = "//div[text()[contains(.,'*Reservation1*')]]//parent::div//following-sibling::a";

        public static String waitforSpinnerStr = "//*[contains(@transform,'rotate')]";

        [FindsBy(How = How.XPath, Using = "//*[@id='accordion__heading-itemsList']")]
        public IWebElement costSummaryTab;

        [FindsBy(How = How.XPath, Using = "//*[@id='accordion__heading-itemsList'][@aria-expanded='false']")]
        public IWebElement costSummarycollapse;

        [FindsBy(How = How.XPath, Using = "//*[@id='accordion__heading-itemsList'][@aria-expanded='true']")]
        public IWebElement costSummaryExpand;

        [FindsBy(How = How.XPath, Using = "//*[@title='INVALID PROMO CODE']")]
        public IWebElement InvalidPromoCodeError;

        [FindsBy(How = How.XPath, Using = "(//*[@title='INVALID PROMO CODE']//*[name()='path'])[2]")]
        public IWebElement InvalidPromoCodeErrorcloseIcn;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Total')]/parent::div/following-sibling::div//*[contains(text(),'$')]")]
        public IWebElement Totaldaypasspage;

        [FindsBy(How = How.XPath, Using = "//*[@data-type='WPHLF']//*[contains(text(),'$')]")]
        public IWebElement halfDayPass;

//=============================================
        [FindsBy(How = How.XPath, Using = "//div[@id='additional-package-section']//div[contains(text(),'Attraction Passes')]")]
        public IWebElement AttractionPassTitle;

        [FindsBy(How = How.XPath, Using = "//div[@id='additional-package-section']//button[contains(text(),'Add Passes')]")]
        public IWebElement AddpassBtnONAttractionTile;

        [FindsBy(How = How.XPath, Using = "//div[@id='additional-package-section']//div[contains(text(),'Birthday Packages')]")]
        public IWebElement BirthdayPakgTitle;

        [FindsBy(How = How.XPath, Using = "//div[@id='additional-package-section']//div[contains(text(),'Birthday Packages')]//following-sibling::a[contains(.,'Add a Package')]")]
        public IWebElement AddpkgBtnONBirthdayTile;

        [FindsBy(How = How.XPath, Using = "//div[@id='additional-package-section']//div[contains(text(),'Dining Packages')]")]
        public IWebElement DiningPakgTitle;

        [FindsBy(How = How.XPath, Using = "//div[@id='additional-package-section']//div[contains(text(),'Dining Packages')]//following-sibling::button[contains(.,'Add a Package')]")]
        public IWebElement AddpkgBtnONDiningTile;

        [FindsBy(How = How.XPath, Using = "//div[@id='additional-package-section']//div[contains(text(),'Cool Cabana')]")]
        public IWebElement CabanaPkgTitle;

        [FindsBy(How = How.XPath, Using = "//div[@id='additional-package-section']//div[contains(text(),'Cool Cabana Rentals')]//following-sibling::a[contains(.,'Add a Cabana')]")]
        public IWebElement CabanaBtnOnCabanaTile;

        [FindsBy(How = How.XPath, Using = "//h2[contains(.,'Pup Pass')]")]
        public IWebElement PupPasstitleOnPopUp;

        [FindsBy(How = How.XPath, Using = "//h2[contains(.,'Paw Pass')]")]
        public IWebElement PawPasstitleOnPopUp;

        [FindsBy(How = How.XPath, Using = "//h2[contains(.,'Wolf Pass')]")]
        public IWebElement WolfPasstitleOnPopUp;

        [FindsBy(How = How.XPath, Using = "//h2[contains(.,'Pup Pass')]//following-sibling::div//div[contains(text(),'SEE ALL INCLUDES >')]")]
        public IWebElement PupPassSeeAllIncldLink;

        [FindsBy(How = How.XPath, Using = "//h2[contains(.,'Paw Pass')]//following-sibling::div//div[contains(text(),'SEE ALL INCLUDES >')]")]
        public IWebElement PawPassSeeAllIncldLink;

        [FindsBy(How = How.XPath, Using = "//h2[contains(.,'Wolf Pass')]//following-sibling::div//div[contains(text(),'SEE ALL INCLUDES >')]")]
        public IWebElement WolfPassSeeAllIncldLink;

        [FindsBy(How = How.XPath, Using = "//h2[contains(.,'Pup Pass')]//following-sibling::div//h2[contains(text(),'$')]")]
        public IWebElement PupPassPriceSec;

        [FindsBy(How = How.XPath, Using = "//h2[contains(.,'Paw Pass')]//following-sibling::div//h2[contains(text(),'$')]")]
        public IWebElement PawPassPriceSec;

        [FindsBy(How = How.XPath, Using = "//h2[contains(.,'Wolf Pass')]//following-sibling::div//h2[contains(text(),'$')]")]
        public IWebElement WolfPassPriceSec;

        [FindsBy(How = How.XPath, Using = "//div[@class='ReactModal__Content ReactModal__Content--after-open Modal']")]
        public IWebElement dinignModal;

        [FindsBy(How = How.XPath, Using = "//div[@id='root']//h3[contains(.,'Add a package')]")]
        public IWebElement addAPackagPage;


        public static Double guestQty;
        public static Double TotalonDaypasspage;
        public static Double HalfDayPrice;

        public static String Reservation;
        public static String StayDates;
        public static String Guests;
        public static String PromoDayPass;
        public static String Pass = "(//div[contains(text(),'Pass')]//following::div[5])[1]";


        public static string gethalfdayPerGuest;
        public static string halfDPUnitPriceFromDaypassPge;
        public static string halfDPunitPriceCovert;

        public static string dpDates;
        public static string unitPriceDP;
        public static string fullDPUnitPriceFromDaypassPge;
        public static string fullDPunitPriceCovert;
        public static string getFulldayPerGuest;
        public static String waitForSignIn= "//button[@type='button'][text()='Sign In']";
        public static String waitForLodingIdicatorOut = "//div[@class='loading-indicator']";
        public static decimal unitTotalTypeChanged;
        public static decimal unitSubTotalTypeChanged;
        public static decimal NumberofValDec;

        public void waitForElemntOutorInvisible(String xpath)
        {
            Browser.waitForImplicitTime(5);
            Browser.waitForElementToBeInvisible(15, xpath);
            Browser.waitForImplicitTime(10);
        }

        public Boolean NotFoundMessageHandle()
        {
        //    Thread.Sleep(3000);
            if (NotFounMessage.Exists())
            {
                clickOnDayPassHeaderLink();
                Pages.PropertyHome.clickToSignOut();
                return false;
            }
            else {                
                Pages.PropertyHome.clickToSignOut();
                return true;
            }
        }

        


    public Boolean checkTotalOnDaypassPayment()
        {
            /*String getUnitTotal = Browser.getText(unitPriceDPPayment);
            String getUnitTotalCovert = getUnitTotal.Substring(1, getUnitTotal.Length - 1);
            unitTotalTypeChanged = Convert.ToDecimal(getUnitTotalCovert);*/

            String getSubTotal = Browser.getText(dpSubTotalPayment);
            String getSubTotalCovert = getSubTotal.Substring(1, getSubTotal.Length - 1);
            unitSubTotalTypeChanged = Convert.ToDecimal(getSubTotalCovert);

            String getTaxesTotal = Browser.getText(dpTaxesPayment);
            String getTaxesTotalCovert = getTaxesTotal.Substring(1, getTaxesTotal.Length - 1);
            Decimal taxTotalTypeChanged = Convert.ToDecimal(getTaxesTotalCovert);

            String getFinalTotal = Browser.getText(dpTotalPayment);
            String getFinalTotalCovert = getFinalTotal.Substring(1, getFinalTotal.Length - 1);
            Decimal finalTotalTypeChanged = Convert.ToDecimal(getFinalTotalCovert);


            Decimal dptotal = unitSubTotalTypeChanged + taxTotalTypeChanged;
            Console.WriteLine("unitSubTotalTypeChanged :" + unitSubTotalTypeChanged);
            Console.WriteLine("taxTotalTypeChanged :" + taxTotalTypeChanged);
            Console.WriteLine("dptotal :" + dptotal);
            Console.WriteLine("finalTotalTypeChanged :" + finalTotalTypeChanged);
            return dptotal.Equals(finalTotalTypeChanged);           

        }

        public Boolean checkPromoCodeOnPaymentPage(String DaypasPromo)
        {
            if (Pages.PropertyHome.signInLink.Exists() == false)
            {
                Pages.PropertyHome.clickToSignOut();
            }
        //    Thread.Sleep(7000);
            String DPPromoStr = daypasspromo + DaypasPromo + daypasspromo1;
            IWebElement DaypassPromoElemnt = dayPassPromoOnPayment.FindElement(By.XPath(DPPromoStr));
            return DaypassPromoElemnt.Displayed;
        }

        public Boolean checkPromoCodeOnPaymentPageWithLogin(String DaypasPromo)
        {
       //     Thread.Sleep(7000);
            String DPPromoStr = daypasspromo + DaypasPromo + daypasspromo1;
            IWebElement DaypassPromoElemnt = dayPassPromoOnPayment.FindElement(By.XPath(DPPromoStr));
            return DaypassPromoElemnt.Displayed;
        }

        public Boolean checkNumberOfPassesOnPaymentPage(String DaypassQty)
        {
            Browser.waitForImplicitTime(10);
        //    Thread.Sleep(7000);
            String DPNumberofpassStr = daypassqty + DaypassQty + daypassqty1;
            IWebElement DaypassCountElemnt = NumberofPassesPaymentPage.FindElement(By.XPath(DPNumberofpassStr));
            String NumberofVal = DaypassCountElemnt.Text;
            NumberofValDec = Convert.ToDecimal(NumberofVal);

            if (unitPriceWithOutOfferCode.Exists())
            {
                String getUnitTotal = Browser.getText(unitPriceWithOutOfferCode);
                String getUnitTotalCovert = getUnitTotal.Substring(1, getUnitTotal.Length - 1);
                unitTotalTypeChanged = Convert.ToDecimal(getUnitTotalCovert);
            }
            else
            {
                String getUnitTotal = Browser.getText(unitPriceDPPayment);
                String getUnitTotalCovert = getUnitTotal.Substring(1, getUnitTotal.Length - 1);
                unitTotalTypeChanged = Convert.ToDecimal(getUnitTotalCovert);
            }
            return DaypassCountElemnt.Displayed;
        }

        public void clickOnDayPassHeaderLink()
        {
            Pages.PropertyHome.closeDealsSignUp();
            Pages.Suite.clickCookiesClose();
            Browser.waitForElementToBeClickable(DayPassMenu, 20);
            DayPassMenu.Click();
        }

        public Boolean checkDayPassPageDisplayed()
        {
            if (PropertyHomePage.dayPassAvlblHome == true)
            {
                Browser.waitForImplicitTime(3);
                bool dPassPage = dayPassLabel.Exists();
                return dPassPage;
            }
            else { return true; }
        }

        public void sendDaypassPromo(String promocode)
        {
            waitForElemntOutorInvisible(waitForSignIn);
       //     Browser.waitForElementToDisplayed(DayPassPromoTextbox, 90);
            Browser.waitForElementToBeClickable(DayPassPromoTextbox, 20);
            DayPassPromoTextbox.SendKeys(promocode);
        }
        public void selectArrivalDate()
        {
            waitForElemntOutorInvisible(waitForLodingIdicatorOut);
            Browser.waitForImplicitTime(10);
      //      Browser.waitForElementToDisplayed(DayPassArrivalDate, 90);
            Browser.waitForElementToBeClickable(DayPassArrivalDate, 20);
            DayPassArrivalDate.Click();           
            for (int i = 1; i <= 3; i++)
            {
                //Thread.Sleep(2000);
                Browser.waitForElementToBeClickable(DayPassNextMonthIcon, 20);
                DayPassNextMonthIcon.Click();
                //Thread.Sleep(2000);
             //   Browser.waitForImplicitTime(3);
            }
            Browser.waitForElementToBeClickable(DayPassDate, 20);
            DayPassDate.Click();
            waitForElemntOutorInvisible(waitForLodingIdicatorOut);
            dpDates = Browser.getTextboxValue(DayPassArrivalDate);
            Pages.CreateAccount.waitCreateAccountSprinnerOut(CreateAccountPage.waitCreateAccountSpinnerStr); 
        }
        public Boolean checkDayPassPromoCodeInfoDisplayed()
        {
          //  Browser.waitForElementToDisplayed(DayPassPromoTerms, 30);
         //   bool DayPassPrpmpInfo = DayPassPromoTerms.Displayed;
            bool DayPassPrpmpInfo = DayPassPromoTerms.Exists();
            return DayPassPrpmpInfo;
        }

        public Boolean checkDayPassNumberOfGuestDisplayed()
        {
       //     Browser.waitForElementToDisplayed(NumberOfGuestField, 10);
      //      bool DayPassGuestField = NumberOfGuestField.Displayed;
            bool DayPassGuestField = NumberOfGuestField.Exists();
            return DayPassGuestField;
        }
        public Boolean checkDayPassTotalLabelIsDisplayed()
        {
      //      Browser.waitForElementToDisplayed(DayPassTotalLabel, 10);
       //     bool DayPassTotalLbl = DayPassTotalLabel.Displayed;
            bool DayPassTotalLbl = DayPassTotalLabel.Exists();
            return DayPassTotalLbl;
        }

        public Boolean checkDayPassCheckoutBtnIsDisplayed()
        {
        //    Browser.waitForElementToDisplayed(DayPassCheckoutButton, 10);
       //     bool DayPassCheckoutbtn = DayPassCheckoutButton.Displayed;
            bool DayPassCheckoutbtn = DayPassCheckoutButton.Exists();
            return DayPassCheckoutbtn;
        }

        public void sendDaypassGuestQty(String GuestQty)
        {
            Browser.waitForElementToDisplayed(NumberOfGuestField, 15);
            //Thread.Sleep(8000);
            waitForElemntOutorInvisible(waitForLodingIdicatorOut);
            NumberOfGuestField.SendKeys(GuestQty);
            //Thread.Sleep(8000);
        }

        public void clickOnDayPassCheckoutBtn()
        {
      //      Browser.waitForElementToDisplayed(DayPassCheckoutButton, 50);
            Browser.waitForElementToBeClickable(DayPassCheckoutButton, 20);
            //Thread.Sleep(7000);
            if (unitPriceDPpage.Exists())
            {
                unitPriceDP = Browser.getText(unitPriceDPpage);
            }            
            DayPassCheckoutButton.Click();
            waitForElemntOutorInvisible(checkOutBtn);
            //Thread.Sleep(7000);
        }

        public Boolean checkDayPassPaymentPageDisplayed()
        {
      //      Browser.waitForElementToDisplayed(DayPassPaymentPage, 10);
       //     bool DayPassPayment = DayPassPaymentPage.Displayed;
            bool DayPassPayment = DayPassPaymentPage.Exists();
            return DayPassPayment;
        }

        public Boolean checkInvalidPromoMessageDisplayed()
        {
         //   Browser.waitForElementToDisplayed(InvalidDayPassPromo, 10);
            bool DayPassInvalidPromo = InvalidDayPassPromo.Exists();
            return DayPassInvalidPromo;
        }

        public Boolean checkDateOnDaypassPayment()
        {
            Browser.waitForImplicitTime(10);
            String DPCovert = dpDates.Substring(0, dpDates.Length - 5);
            Browser.waitForElementToDisplayed(dayPassdDateOnPayment, 20);
            String dpDates1 = Browser.getText(dayPassdDateOnPayment);
            Console.WriteLine("DPCovert :" + DPCovert);
            Console.WriteLine("dpDates1 :" + dpDates1);
            return dpDates1.Equals(DPCovert);
         
        }
        public Boolean checkUnitPriceOnDaypassPayment()
        {
            String unitPricePaymentPage = Browser.getText(unitPriceDPPayment);

            String unitPriceCovert = unitPricePaymentPage.Substring(0, unitPricePaymentPage.Length - 3);
            Console.WriteLine("unitPriceCovert :" + unitPriceCovert);
            Console.WriteLine("unitPriceDP :" + unitPriceDP);
            return unitPriceCovert.Equals(unitPriceDP);   

        }
        public void clickOnDayPassHomeTile()
        {
            Browser.waitForElementToBeClickable(dpHomeTile, 20);
            dpHomeTile.Click();

        }

        public Boolean checkSubTotalPriceOnDaypassPayment()
        {
            Decimal FinalSubTotal = NumberofValDec * unitTotalTypeChanged;
            Console.WriteLine("NumberofValDec :" + NumberofValDec);
            Console.WriteLine("unitTotalTypeChanged :" + unitTotalTypeChanged);
            Console.WriteLine("FinalSubTotal :" + FinalSubTotal);

            Browser.waitForElementToDisplayed(dpSubTotalPayment, 15);
            String getSubTotal = Browser.getText(dpSubTotalPayment);
            String getSubTotalCovert = getSubTotal.Substring(1, getSubTotal.Length - 1);
            unitSubTotalTypeChanged = Convert.ToDecimal(getSubTotalCovert);

            return FinalSubTotal.Equals(unitSubTotalTypeChanged);            

        }
        public Boolean checkDayPassTaxesDisplayed()
        {
            Browser.waitForElementToDisplayed(dpTaxesPayment, 10);
        //    bool DayPassTaxesOnPayment = dpTaxesPayment.Displayed;
            bool DayPassTaxesOnPayment = dpTaxesPayment.Exists();
            return DayPassTaxesOnPayment;
        }

        public void clickOnFullDayPass()
        {
            Browser.waitForElementToBeClickable(fullDayPass, 10);
            fullDPUnitPriceFromDaypassPge = Browser.getText(fullDayPass);
            fullDPunitPriceCovert = fullDPUnitPriceFromDaypassPge.Substring(0, fullDPUnitPriceFromDaypassPge.Length - 3);
            fullDayPass.Click();
        }

        public Boolean checkFullDayPassPerGuestonPaymentPageDisplayed()
        {
            Browser.waitForElementToDisplayed(fullDayPassPerGuestPayment, 10);
            getFulldayPerGuest = Browser.getText(fullDayPassPerGuestPayment);
            return getFulldayPerGuest.Equals(fullDPunitPriceCovert);
        }

        public Boolean checkFullDayPassTotalonPaymentPageDisplayed()
        {
            
            Decimal FullDayPassTotal = NumberofValDec * unitTotalTypeChanged;
            Console.WriteLine("NumberofValDec" + NumberofValDec);
            Console.WriteLine("unitTotalTypeChanged" + unitTotalTypeChanged);
            Console.WriteLine("FullDayPassTotal" + FullDayPassTotal);

            Browser.waitForElementToDisplayed(DayPassTotalPayment, 15);
            String getSubTotal = Browser.getText(DayPassTotalPayment);
            String getSubTotalCovert = getSubTotal.Substring(1, getSubTotal.Length - 1);
            unitSubTotalTypeChanged = Convert.ToDecimal(getSubTotalCovert);

            return FullDayPassTotal.Equals(unitSubTotalTypeChanged);
        }

        public Boolean checkTotalOnDaypassPaymentForFullDayPass()
        {
            Browser.waitForElementToDisplayed(DayPassTotalPayment, 15);
            String getSubTotal = Browser.getText(DayPassTotalPayment); 
            String getSubTotalCovert = getSubTotal.Substring(1, getSubTotal.Length - 1);
            unitSubTotalTypeChanged = Convert.ToDecimal(getSubTotalCovert);

            String getTaxesTotal = Browser.getText(dpTaxesPayment);
            String getTaxesTotalCovert = getTaxesTotal.Substring(1, getTaxesTotal.Length - 1);
            Decimal taxTotalTypeChanged = Convert.ToDecimal(getTaxesTotalCovert);

            String getFinalTotal = Browser.getText(dpTotalPayment);
            String getFinalTotalCovert = getFinalTotal.Substring(1, getFinalTotal.Length - 1);
            Decimal finalTotalTypeChanged = Convert.ToDecimal(getFinalTotalCovert);


            Decimal dptotal = unitSubTotalTypeChanged + taxTotalTypeChanged;
            return dptotal.Equals(finalTotalTypeChanged);

        }

        //
        public Boolean checkValidationMessageWhenEnterZeroGuestDisplayed()
        {
        //    Browser.waitForElementToDisplayed(numberOfGuestsError, 20);
       //     bool numberOfGuestsErrorMessage = numberOfGuestsError.Displayed;
            bool numberOfGuestsErrorMessage = numberOfGuestsError.Exists();
            return numberOfGuestsErrorMessage;
        }

        public Boolean checkHalfDaypassHoursDisplayed()
        {
        //    Browser.waitForElementToDisplayed(halfdayPassHours, 20);
        //    bool halfdaypasshours = halfdayPassHours.Displayed;
            bool halfdaypasshours = halfdayPassHours.Exists();
            return halfdaypasshours;
        }

        public void clickOnDayPassCheckoutBtntoSeeError()
        {
       //     Browser.waitForElementToDisplayed(DayPassCheckoutButton, 50);
            Browser.waitForElementToBeClickable(DayPassCheckoutButton, 20);
            DayPassCheckoutButton.Click();
        }

        public Boolean verifyAffirmRadioBtn()
        {
            Boolean Affirmsection = Pages.Payment.affirmRadioBtn.Exists();
            return Affirmsection;
        }

        public Boolean YourCreditCardChargedAndTotalCostSame()
        {
            Browser.waitForElementToDisplayed(dpTotalPayment, 20);
            Browser.waitForElementToDisplayed(YourCreditCardCharged, 20);
            String TotalPrice = "  " + dpTotalPayment.Text;
            String CreditCardPrice = YourCreditCardCharged.Text;
            return (TotalPrice).Equals(CreditCardPrice);
        }

        public void ClickOnHalfDayPass()
        {
            Browser.waitForElementToBeClickable(HalfDayPass, 10);
            halfDPUnitPriceFromDaypassPge = Browser.getText(HalfDayPass);
            halfDPunitPriceCovert = halfDPUnitPriceFromDaypassPge.Substring(0, halfDPUnitPriceFromDaypassPge.Length - 3);
            HalfDayPass.Click();
        }

        public Boolean checkhalfDayPassPerGuestonPaymentPageDisplayed()
        {
            Browser.waitForElementToDisplayed(halfDayPassPerGuestPayment, 10);
            gethalfdayPerGuest = Browser.getText(halfDayPassPerGuestPayment);
            return gethalfdayPerGuest.Equals(halfDPunitPriceCovert);
        }

        public Boolean checkhalfDayPassTotalonPaymentPageDisplayed()
        {

            Decimal HalfDayPassTotal = NumberofValDec * unitTotalTypeChanged;
            Console.WriteLine("NumberofValDec" + NumberofValDec);
            Console.WriteLine("unitTotalTypeChanged" + unitTotalTypeChanged);
            Console.WriteLine("FullDayPassTotal" + HalfDayPassTotal);

            Browser.waitForElementToDisplayed(DayPassTotalPayment, 15);
            String getSubTotal = Browser.getText(DayPassTotalPayment);
            String getSubTotalCovert = getSubTotal.Substring(1, getSubTotal.Length - 1);
            unitSubTotalTypeChanged = Convert.ToDecimal(getSubTotalCovert);

            return HalfDayPassTotal.Equals(unitSubTotalTypeChanged);
        }

        public Boolean checkTotalOnDaypassPaymentForHalfDayPass()
        {
            Browser.waitForElementToDisplayed(DayPassTotalPayment, 15);
            String getSubTotal = Browser.getText(DayPassTotalPayment);
            String getSubTotalCovert = getSubTotal.Substring(1, getSubTotal.Length - 1);
            unitSubTotalTypeChanged = Convert.ToDecimal(getSubTotalCovert);

            String getTaxesTotal = Browser.getText(dpTaxesPayment);
            String getTaxesTotalCovert = getTaxesTotal.Substring(1, getTaxesTotal.Length - 1);
            Decimal taxTotalTypeChanged = Convert.ToDecimal(getTaxesTotalCovert);

            String getFinalTotal = Browser.getText(dpTotalPayment);
            String getFinalTotalCovert = getFinalTotal.Substring(1, getFinalTotal.Length - 1);
            Decimal finalTotalTypeChanged = Convert.ToDecimal(getFinalTotalCovert);


            Decimal dptotal = unitSubTotalTypeChanged + taxTotalTypeChanged;
            return dptotal.Equals(finalTotalTypeChanged);

        }

        public void sendDaypassGuestQtyMoreThanTwo(String GuestQty)
        {
            Browser.waitForElementToDisplayed(NumberOfGuestField, 15);
            waitForElemntOutorInvisible(waitForLodingIdicatorOut);
            NumberOfGuestField.Clear();
            NumberOfGuestField.SendKeys(GuestQty);

        }

        public Boolean checkDayPassPaymentPageExist()
        {
            bool DayPassPayment = dpTotalPayment.Exists();
            return DayPassPayment;
        }

        public void ClickOnAgreeAndBookBtn()
        {
            Browser.waitForElementToBeClickable(AgreeAndBookbtn, 20);
            AgreeAndBookbtn.Click();
        }

        public void NavigateToReservationPage()
        {
            Pages.NewConfirmation.clickAllDone();
            Browser.waitForElementToDisplayed(reservationNumber, 20);
            Reservation = Browser.getText(reservationNumber).Replace("Reservation #", "").Trim(); ;
            StayDates = Browser.getText(DateOnConfirmation);
            String splitNum = Browser.getText(reservationNumber);
            String afterSplitResNum = splitNum.Substring(13);
            //afterSplitResNum = splitNum.Substring(20);
            Console.WriteLine("reservation No : " + afterSplitResNum);
            DataAccess.putData(afterSplitResNum);
            // Browser.waitForElementToDisplayed(Pages.PropertyHome.userSignedInMenu, 90);
            Browser.waitForElementToBeClickable(Pages.PropertyHome.userSignedInMenu, 90);
            Pages.PropertyHome.userSignedInMenu.Click();
      //      Browser.waitForElementToDisplayed(myReservationMenuLink, 40);
            Browser.waitForElementToBeClickable(myReservationMenuLink, 20);
            myReservationMenuLink.Click();
            //      Thread.Sleep(4000);
            waitForSprinnerOut(waitforSpinnerStr);
       }

        public Boolean MatchReservationNumOnreservationPage()
        {
            //  Browser.waitForImplicitTime(4000);
            waitForSprinnerOut(waitforSpinnerStr);
            IWebElement ReservationDisplay = Browser.driver.FindElement(By.XPath(NumReservation.Replace("*Reservation1*", Reservation)));
            Browser.scrollToAnElement(ReservationDisplay);
            if (ReservationDisplay.Displayed)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        public void ClickViewdetailsBtn()
        {
            Browser.waitForImplicitTime(4000);
            IWebElement ViewDetailsEL = Browser.driver.FindElement(By.XPath(Viewdetails.Replace("*Reservation1*", Reservation)));
            Browser.scrollToAnElement(ViewDetailsEL);
            ViewDetailsEL.Click();
         //   Browser.waitForImplicitTime(4000);
            waitForSprinnerOut(waitforSpinnerStr);
        }

        public Boolean VerifyDayPassreservationdetails()
        {
            waitForSprinnerOut(waitforSpinnerStr);
            String ActualReserID = reservationNumber.GetAttribute("innerText");
            String ActualDaypassDate = DateOnConfirmation.Text;
            String Pass = PassQty.Text;

            if (ActualReserID.Contains(Reservation) && ActualDaypassDate.Equals(StayDates))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void DataOnDayPassPaymentPage()
        {
            Pass = PassQtyOnPayemnt.Text;
            StayDates = DateOnDaypassPayment.Text;
            PromoDayPass = DayPassPromo.Text;
        }

        public Boolean ISMatchingDaypAssDate()
        {
            String PassOnConfirmation = PassQty.Text;
            String DateOnConfirmation1 = DateOnConfirmation.Text;

            if (Pass.Contains(PassOnConfirmation))
            {
                return true;
            }
            return false;
        }

        public Boolean ISMatchingDayPassPass()
        {
            String PassOnConfirmation = PassQty.Text;
            if (Pass.Contains(PassOnConfirmation))
            {
                return true;
            }
            return false;
        }

        //updation
        public void waitForSprinnerOut(String xpath)
        {
            //Browser.waitForImplicitTime(8);
            Browser.waitForElementToBeInvisible(20, xpath);
            //Browser.waitForImplicitTime(8);
        }

        public void clickOnCostSummaryTab()
        {
            //       Browser.waitForElementToDisplayed(costSummaryTab, 30);
            Browser.waitForElementToBeClickable(costSummaryTab, 20);
            costSummaryTab.Click();
        }

        public Boolean verifyCostSummarycollapse()
        {
            Boolean CostSmmyCllpse = costSummarycollapse.Exists();
            return CostSmmyCllpse;
        }
        public Boolean verifyCostSummaryExpand()
        {
            Boolean CostSmmyExpand = costSummaryExpand.Exists();
            return CostSmmyExpand;
        }

        public Boolean verifyInvalidPromoCodeError()
        {
            Boolean InvldProCodeErr = InvalidPromoCodeError.Exists();
            return InvldProCodeErr;
        }
        public void clickInvalidPrmCodErrClsIcon()
        {
            //Browser.waitForElementToDisplayed(InvalidPromoCodeErrorcloseIcn, 20);
            Browser.waitForElementToBeClickable(InvalidPromoCodeErrorcloseIcn, 20);
            InvalidPromoCodeErrorcloseIcn.Click();
        }

        public void getTotalonDayPassPage()
        {
            TotalonDaypasspage = Browser.stringToDoubleConvert(Totaldaypasspage);

        }

        public void clickOnhalfDayPass()
        {
            Browser.waitForElementToBeClickable(halfDayPass, 15);
            HalfDayPrice = Browser.stringToDoubleConvert(halfDayPass);
            halfDayPass.Click();
        }

        public void clickOnAddPassBtn()
        {
            Browser.waitForElementToBeClickable(AddpassBtnONAttractionTile, 15);
            AddpassBtnONAttractionTile.Click();
        }

        public void clickOnAddPkgDinigBtn()
        {
            Browser.waitForElementToBeClickable(AddpkgBtnONDiningTile, 15);
            AddpkgBtnONDiningTile.Click();
        }

        public void clickOnAddPkgBirthBtn()
        {
            Browser.waitForElementToBeClickable(AddpkgBtnONBirthdayTile, 15);
            AddpkgBtnONBirthdayTile.Click();
        }

        public void clickOnAddCabanaBtn()
        {
            Browser.waitForElementToBeClickable(CabanaBtnOnCabanaTile, 15);
            CabanaBtnOnCabanaTile.Click();
        }

        public Boolean verifyattractionTileTitle()
        {
            Boolean result = AttractionPassTitle.Exists();
            return result;
        }

        public Boolean verifyattractionTileButton()
        {
            Boolean result = AddpassBtnONAttractionTile.Exists();
            return result;
        }

        public Boolean verifyBirthdayTileTitle()
        {
            Boolean result = BirthdayPakgTitle.Exists();
            return result;
        }

        public Boolean verifyBirthdayTileButton()
        {
            Boolean result = AddpkgBtnONBirthdayTile.Exists();
            return result;
        }

        public Boolean verifyDiningTileTitle()
        {
            Boolean result = DiningPakgTitle.Exists();
            return result;
        }

        public Boolean verifyDiningTileButton()
        {
            Boolean result = AddpkgBtnONDiningTile.Exists();
            return result;
        }

        public Boolean verifyCabanaTileTitle()
        {
            Boolean result = CabanaPkgTitle.Exists();
            return result;
        }

        public Boolean verifyCabanaTileButton()
        {
            Boolean result = CabanaBtnOnCabanaTile.Exists();
            return result;
        }

        public Boolean verifyPupPssTitlOnPopUp()
        {
            Boolean result = PupPasstitleOnPopUp.Exists();
            return result;
        }

        public Boolean verifyPawPssTitlOnPopUp()
        {
            Boolean result = PawPasstitleOnPopUp.Exists();
            return result;
        }

        public Boolean verifyWolfPssTitlOnPopUp()
        {
            Boolean result = WolfPasstitleOnPopUp.Exists();
            return result;
        }

        public Boolean verifyPupSeeAllLnkOnPopUp()
        {
            Boolean result = PupPassSeeAllIncldLink.Exists();
            return result;
        }

        public Boolean verifyPawSeeAllLnkOnPopUp()
        {
            Boolean result = PawPassSeeAllIncldLink.Exists();
            return result;
        }

        public Boolean verifyWolfSeeAllLnkOnPopUp()
        {
            Boolean result = WolfPassSeeAllIncldLink.Exists();
            return result;
        }

        public Boolean verifyPupPassPriceSecOnPopUp()
        {
            Boolean result = PupPassPriceSec.Exists();
            return result;
        }

        public Boolean verifyPawPassPriceSecOnPopUp()
        {
            Boolean result = PawPassPriceSec.Exists();
            return result;
        }

        public Boolean verifyWolfPassPriceSecOnPopUp()
        {
            Boolean result = WolfPassPriceSec.Exists();
            return result;
        }

        String dingFr1 = "(//div[@class='ReactModal__Content ReactModal__Content--after-open Modal']//div[contains(.,'";
        String dingFr2 = "')])[5]";
        String seeWhatYouGet = "')])[5]//following-sibling::div//a[contains(.,'See what you get')]";

        String dingCost = "((//div[@class='ReactModal__Content ReactModal__Content--after-open Modal']//div[contains(.,'";
        String dingCost2 = "')])[5]//following-sibling::div//div[contains(.,'$')])[2]";
        String dingCostPckgPop = "')])[5]//following-sibling::div//div[contains(.,'$')])[2]//div[1]";

        public Boolean verifyDiningpckgFirstCostOnDingPkgPopUp()
        {
            if (SuitePage.DiningFirstPckgAvl == true)
            {
                string fistCost = Browser.convertToCamleCase(SuitePage.DiningFirstPckgTitl);
                IWebElement reseNum = dinignModal.FindElement(By.XPath(dingCost + fistCost + dingCostPckgPop));
                String adPckDiningcost = reseNum.Text;
                return adPckDiningcost.Equals(SuitePage.DiningFirstPckgTitlCost);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyDiningpckgFirstCost()
        {
            if (SuitePage.DiningFirstPckgAvl == true)
            {
                string fistCost = Browser.convertToCamleCase(SuitePage.DiningFirstPckgTitl);
                IWebElement reseNum = dinignModal.FindElement(By.XPath(dingCost + fistCost + dingCost2));
                String adPckDiningcost = reseNum.Text;
                return adPckDiningcost.Equals(SuitePage.DiningFirstPckgTitlCost);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyDiningpckgScndCost()
        {
            if (SuitePage.DiningScndPckgAvl == true)
            {
                string scndCost = Browser.convertToCamleCase(SuitePage.DiningSecondPckgTitl);
                IWebElement reseNum = dinignModal.FindElement(By.XPath(dingCost + scndCost + dingCost2));
                String adPckDiningcost = reseNum.Text;
                return adPckDiningcost.Equals(SuitePage.DiningSecondPckgTitlCost);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyDiningpckgThirdCost()
        {
            if (SuitePage.DiningThirdPckgAvl == true)
            {
                string thrdCost = Browser.convertToCamleCase(SuitePage.DiningThirdPckgTitl);
                IWebElement reseNum = dinignModal.FindElement(By.XPath(dingCost + thrdCost + dingCost2));
                String adPckDiningcost = reseNum.Text;
                return adPckDiningcost.Equals(SuitePage.DiningThirdPckgTitlCost);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyDiningpckgfirstSeeWhatLink()
        {
            if (SuitePage.DiningFirstPckgAvl == true)
            {
                string firstTitle = Browser.convertToCamleCase(SuitePage.DiningFirstPckgTitl);
                IWebElement reseNum = dinignModal.FindElement(By.XPath(dingFr1 + firstTitle + seeWhatYouGet));
                return reseNum.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyDiningpckgScndSeeWhatLink()
        {
            if (SuitePage.DiningScndPckgAvl == true)
            {
                string scndTitle = Browser.convertToCamleCase(SuitePage.DiningSecondPckgTitl);
                IWebElement reseNum = dinignModal.FindElement(By.XPath(dingFr1 + scndTitle + seeWhatYouGet));
                return reseNum.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyDiningpckgTthirdSeeWhatLink()
        {
            if (SuitePage.DiningThirdPckgAvl == true)
            {
                string thirdTitle = Browser.convertToCamleCase(SuitePage.DiningThirdPckgTitl);
                IWebElement reseNum = dinignModal.FindElement(By.XPath(dingFr1 + thirdTitle + seeWhatYouGet));
                return reseNum.Exists();
            }
            else
            {
                return true;
            }
        }

        String dingpckg = "(//div[@class='ReactModal__Content ReactModal__Content--after-open Modal']//div[contains(.,'";
        String dingpckg1 = "')])[7]";
        public Boolean verifyDiningpckgfirstOnModal()
        {
            if (SuitePage.DiningFirstPckgAvl == true)
            {
                string firstTitle = Browser.convertToCamleCase(SuitePage.DiningFirstPckgTitl);
                IWebElement reseNum = dinignModal.FindElement(By.XPath(dingpckg + firstTitle + dingpckg1));
                return reseNum.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyDiningpckgfirst()
        {
            if (SuitePage.DiningFirstPckgAvl == true)
            {
                string firstTitle = Browser.convertToCamleCase(SuitePage.DiningFirstPckgTitl);
                IWebElement reseNum = dinignModal.FindElement(By.XPath(dingFr1 + firstTitle + dingFr2));
                return reseNum.Exists();
            }
            else { 
                return true; 
            }           
        }

        public Boolean verifyDiningpckgScnd()
        {
            if (SuitePage.DiningScndPckgAvl == true)
            {
                string sndTitle = Browser.convertToCamleCase(SuitePage.DiningSecondPckgTitl);
                IWebElement reseNum = dinignModal.FindElement(By.XPath(dingFr1 + sndTitle + dingFr2));
                return reseNum.Exists();
            }
            else { return true; }
        }

        public Boolean verifyDiningpckgThird()
        {
            if (SuitePage.DiningThirdPckgAvl == true)
            {
                string thirdTitle = Browser.convertToCamleCase(SuitePage.DiningThirdPckgTitl);
                IWebElement reseNum = dinignModal.FindElement(By.XPath(dingFr1 + thirdTitle + dingFr2));
                return reseNum.Exists();
            }
            else { return true; }
        }

        String birthFirstTitle1 = "//h2[contains(text(),'";
        String birthFirstTitle2 = "')]";
        String birthdayCost = "')]//parent::div//following-sibling::div//div[contains(.,'Per Day')]//preceding-sibling::div";
        String birthdayPckgAddBtn = "')]//parent::div//following-sibling::div//div//button";
        String birthdayPckgCount = "')]//parent::div//following-sibling::div//div//select/option";

        public Boolean verifyBirthFirstpckgCount()
        {
            if (ActivitiesPage.BirthdayFirstPckgAvl == true)
            {
                string firstTitle = Browser.convertToCamleCase(ActivitiesPage.BirthdayFirstPckgTitl);
                int pckgCnt = addAPackagPage.FindElements(By.XPath(birthFirstTitle1 + firstTitle + birthdayPckgCount)).Count;
                return pckgCnt.Equals(3);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyBirthSecndpckgCount()
        {
            if (ActivitiesPage.BirthdayScndPckgAvl == true)
            {
                string scndTitle = Browser.convertToCamleCase(ActivitiesPage.BirthdaySecondPckgTitl);
                int pckgCnt = addAPackagPage.FindElements(By.XPath(birthFirstTitle1 + scndTitle + birthdayPckgCount)).Count;
                return pckgCnt.Equals(3);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyBirthThirdpckgCount()
        {
            if (ActivitiesPage.BirthdayThirdPckgAvl == true)
            {
                string thrdTitle = Browser.convertToCamleCase(ActivitiesPage.BirthdayThirdPckgTitl);
                int pckgCnt = addAPackagPage.FindElements(By.XPath(birthFirstTitle1 + thrdTitle + birthdayPckgCount)).Count;
                return pckgCnt.Equals(3);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyBirthpckgFirstAddBtn()
        {
            if (ActivitiesPage.BirthdayFirstPckgAvl == true)
            {
                string firstTitle = Browser.convertToCamleCase(ActivitiesPage.BirthdayFirstPckgTitl);
                IWebElement reseNum = addAPackagPage.FindElement(By.XPath(birthFirstTitle1 + firstTitle + birthdayPckgAddBtn));
                return reseNum.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyBirthpckgScndAddBtn()
        {
            if (ActivitiesPage.BirthdayScndPckgAvl == true)
            {
                string scndTitle = Browser.convertToCamleCase(ActivitiesPage.BirthdaySecondPckgTitl);
                IWebElement reseNum = addAPackagPage.FindElement(By.XPath(birthFirstTitle1 + scndTitle + birthdayPckgAddBtn));
                return reseNum.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyBirthpckgthirdAddBtn()
        {
            if (ActivitiesPage.BirthdayThirdPckgAvl == true)
            {
                string thirdTitle = Browser.convertToCamleCase(ActivitiesPage.BirthdayThirdPckgTitl);
                IWebElement reseNum = addAPackagPage.FindElement(By.XPath(birthFirstTitle1 + thirdTitle + birthdayPckgAddBtn));
                return reseNum.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyBirthpckgfirstCost()
        {
            if (ActivitiesPage.BirthdayFirstPckgAvl == true)
            {
                string firstTitle = Browser.convertToCamleCase(ActivitiesPage.BirthdayFirstPckgTitl);
                IWebElement reseNum = addAPackagPage.FindElement(By.XPath(birthFirstTitle1 + firstTitle + birthdayCost));
                String adPckBirthcost = reseNum.Text;
                return adPckBirthcost.Equals(ActivitiesPage.BirthdayFirstPckgTitlCost);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyBirthpckgscndCost()
        {
            if (ActivitiesPage.BirthdayScndPckgAvl == true)
            {
                string scndTitle = Browser.convertToCamleCase(ActivitiesPage.BirthdaySecondPckgTitl);
                IWebElement reseNum = addAPackagPage.FindElement(By.XPath(birthFirstTitle1 + scndTitle + birthdayCost));
                String adPckBirthcost = reseNum.Text;
                return adPckBirthcost.Equals(ActivitiesPage.BirthdaySecondPckgTitlCost);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyBirthpckgthrdCost()
        {
            if (ActivitiesPage.BirthdayThirdPckgAvl == true)
            {
                string thirdTitle = Browser.convertToCamleCase(ActivitiesPage.BirthdayThirdPckgTitl);
                IWebElement reseNum = addAPackagPage.FindElement(By.XPath(birthFirstTitle1 + thirdTitle + birthdayCost));
                String adPckBirthcost = reseNum.Text;
                return adPckBirthcost.Equals(ActivitiesPage.BirthdayThirdPckgTitlCost);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyBirthpckgfirsttitl()
        {
            if (ActivitiesPage.BirthdayFirstPckgAvl == true)
            {
                string firstTitle = Browser.convertToCamleCase(ActivitiesPage.BirthdayFirstPckgTitl);
                IWebElement reseNum = addAPackagPage.FindElement(By.XPath(birthFirstTitle1 + firstTitle + birthFirstTitle2));
                return reseNum.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyBirthpckgScndtitl()
        {
            if (ActivitiesPage.BirthdayScndPckgAvl == true)
            {
                string scndTitle = Browser.convertToCamleCase(ActivitiesPage.BirthdaySecondPckgTitl);
                IWebElement reseNum = addAPackagPage.FindElement(By.XPath(birthFirstTitle1 + scndTitle + birthFirstTitle2));
                return reseNum.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyBirthpckgthrdtitl()
        {
            if (ActivitiesPage.BirthdayThirdPckgAvl == true)
            {
                string thrdTitle = Browser.convertToCamleCase(ActivitiesPage.BirthdayThirdPckgTitl);
                IWebElement reseNum = addAPackagPage.FindElement(By.XPath(birthFirstTitle1 + thrdTitle + birthFirstTitle2));
                return reseNum.Exists();
            }
            else
            {
                return true;
            }
        }

    }
}

   
