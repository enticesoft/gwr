﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GWRFramework
{
    public class DiningPage
    {
        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Progress Bar']//li[2]//a")]
        public IWebElement navigationONActivity;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Progress Bar']//li[1]//a")]
        public IWebElement navigationONSuite;

        [FindsBy(How = How.XPath, Using = "(//div[@aria-label='Summary Item Party Size']//div)[2]")]
        public IWebElement guestSection;
        
        String Adult = "(//div[@aria-label='Summary Item Party Size']//div)[2][contains(.,'";
        String Adult2 = " Adults')]";
        String Kids = " Kids')]";

        [FindsBy(How = How.XPath, Using = "(//div[@id='root']//button[text()='ADD'])[1]")]
        public IWebElement diningFirstAddBtn;

        [FindsBy(How = How.XPath, Using = "(//select)[1]")]
        public IWebElement diningFirstPckDrp;

        [FindsBy(How = How.XPath, Using = "(//select)[2]")]
        public IWebElement diningSecondPckDrp;

        [FindsBy(How = How.XPath, Using = "(//div[@id='root']//button)[2]")]
        public IWebElement diningSecndAddBtn;

        [FindsBy(How = How.XPath, Using = "(//div[@id='root']//button[text()='UPDATE'])[1]")]
        public IWebElement diningFirstUpdateBtn;        

        [FindsBy(How = How.XPath, Using = "((//div[@aria-label='Summary Item Packages']//following-sibling::div)[3]//div)[4]")]
        public IWebElement costSumryAddePackgTitle;

        [FindsBy(How = How.XPath, Using = "(((//div[@id='root']//button[text()='ADD'])[1]//ancestor::div[4]//child::div[2])[1]//div[@font-weight='extraBold'])[1]")]
        public IWebElement fisrtDiningPckgTitle;

        [FindsBy(How = How.XPath, Using = "(((//div[@id='root']//button[text()='ADD'])[2]//ancestor::div[4]//child::div[2])[1]//div)[1]")]
        public IWebElement secondDiningPckgTitle;

        [FindsBy(How = How.XPath, Using = "(((//div[@id='root']//button[text()='ADD'])[3]//ancestor::div[4]//child::div[2])[1]//div)[3]")]
        public IWebElement thirdDiningPckgTitle;

        [FindsBy(How = How.XPath, Using = "(((//div[@id='root']//button[text()='ADD'])[3]//ancestor::div[4]//child::div[2])[1]//div)[1]")]
        public IWebElement thirdBirthdayPckgTitle;
        

        [FindsBy(How = How.XPath, Using = "//div[@id='root']//button[text()='REMOVE']")]
        public IWebElement removePackageLink;

        [FindsBy(How = How.XPath, Using = "(//select)[1]")]
        public IWebElement firstPckgQuntityDrp;

        [FindsBy(How = How.XPath, Using = "((//div[@aria-label='Summary Item Packages']//following-sibling::div)[3]//div[contains(.,'$')])[4]")]
        public IWebElement packageCostAtCostSummry;

        [FindsBy(How = How.XPath, Using = "(//div[@aria-label='Check-In and Check-Out dates']//div)[7]")]
        public IWebElement costSumyCheckInDt;

        [FindsBy(How = How.XPath, Using = "(//div[@aria-label='Check-In and Check-Out dates']//div)[8]")]
        public IWebElement costSumyCheckOutDt;

        [FindsBy(How = How.XPath, Using = "(//button[@type='button'][text()='ADD'])[3]//parent::div//preceding-sibling::div//select")]
        public IWebElement thirdPckgDrp;

        [FindsBy(How = How.XPath, Using = "(//div[@aria-label='Summary Item Packages']//following-sibling::div//select)[1]")]
        public IWebElement costSummryFirstPackgDrp;
        

        /*[FindsBy(How = How.XPath, Using = "(//section//div[contains(.,'Person / Per Day')])[9]")]
        public IWebElement personPerdayCost;*/

        [FindsBy(How = How.XPath, Using = "(//div[@id='root']//button[text()='ADD']//parent::div//parent::div//preceding-sibling::div[contains(.,'Person / Per Day')]//div)[2]")]
        public IWebElement personPerdayCost;

        [FindsBy(How = How.XPath, Using = "(//div[@aria-label='Summary Item Promotional Savings']//div)[7]")]
        public IWebElement dinignPromSavngCost;

        [FindsBy(How = How.XPath, Using = "//div[text()='Your Savings:']//following-sibling::div")]
        public IWebElement dinignYourSavngCost;

        [FindsBy(How = How.XPath, Using = "((((//button[@type='button'][text()='REMOVE'])[1]//ancestor::div)[21])//following-sibling::div//div[contains(.,'$')])[2]")]
        public IWebElement firstAddedPackgeCost;

        [FindsBy(How = How.XPath, Using = "((((//button[@type='button'][text()='REMOVE'])[2]//ancestor::div)[21])//following-sibling::div//div[contains(.,'$')])[2]")]
        public IWebElement secondAddedPackgeCost;

        [FindsBy(How = How.XPath, Using = "((//div[@id='root']//button[text()='ADD'])[1]/../../..//div[contains(.,'$')])[2]")]
        public IWebElement firstPackageCost;

        [FindsBy(How = How.XPath, Using = "((//div[@id='root']//button[text()='ADD'])[2]/../../..//div[contains(.,'$')])[2]")]
        public IWebElement secondPackageCost;

        /*[FindsBy(How = How.XPath, Using = "(//section//div[contains(.,'Person / Per Day')])[9]//preceding-sibling::div")]
        public IWebElement personPerdayTotalCost;*/

        [FindsBy(How = How.XPath, Using = "(//div[@id='root']//button[text()='ADD']//parent::div//parent::div//preceding-sibling::div[contains(.,'Person / Per Day')]//div)[1]")]
        public IWebElement personPerdayTotalCost;

        [FindsBy(How = How.XPath, Using = "((//div[@id='root']//button[text()='ADD'])[3]//ancestor::div[3]//div[contains(.,'$')])[2]")]
        public IWebElement thirdPackageCost;

        [FindsBy(How = How.XPath, Using = "(//select)[1]//option")]
        public IWebElement firstPckgQuntityDrpOptionCount;
               
        
        [FindsBy(How = How.XPath, Using = "(//div[contains(text(),'Person / Per Day')])[1]")]
        public IWebElement perDayPersonTextOnDining;

        [FindsBy(How = How.XPath, Using = "(//div[contains(text(),'Person / Per Day')])[1]//parent::div//following-sibling::div//select//option")]
        public IWebElement PersonPerDayPackgOption;

        [FindsBy(How = How.XPath, Using = "(//div[text()='Per Package'])[1]")]
        public IWebElement perPackageTextOnDining;

        [FindsBy(How = How.XPath, Using = "(//div[text()='Per Package'])[1]//parent::div//following-sibling::div//select//option")]
        public IWebElement perPackageDrpDwnDefautlVal;

        

        public static string firstTitlePckg;
        public static double singlePckgCost;
        public static decimal firstPckgCost;
        public static decimal scndPckgCost;

        public Boolean verifyPackageaddedAfterAddBtn()
        {
            Browser.waitForElementToDisplayed(costSumryAddePackgTitle, 20);
            String addedPckg = costSumryAddePackgTitle.Text;

            String expAddPckg = Browser.convertToCamleCase(addedPckg);
            String actulAddPckg = Browser.convertToCamleCase(firstTitlePckg);

            return expAddPckg.Equals(actulAddPckg);
        }

        public Boolean verifyAdultcount(String AdultClVal)
        {
            //Thread.Sleep(3000);
            Browser.waitForImplicitTime(5);
            String adultCnt = Adult + AdultClVal + Adult2;
            IWebElement adultCountElemnt = guestSection.FindElement(By.XPath(adultCnt));
            return adultCountElemnt.Displayed;
        }

        public Boolean verifyPckgRemoved()
        {
            //Thread.Sleep(3000);
            Browser.waitForImplicitTime(6);
            if (costSumryAddePackgTitle.Exists()) {
                return false;
            }
            else {
                return true;
            }
           
        }
        

        public Boolean verifyKidscount(String KidsClVal)
        {
            //Thread.Sleep(3000);
            Browser.waitForImplicitTime(6);
            String kidCnt = Adult + KidsClVal + Kids;
            IWebElement KidCountElemnt = guestSection.FindElement(By.XPath(kidCnt));
            return KidCountElemnt.Displayed;
        }

        public void navigateOnActivity() {
            Browser.waitForElementToBeClickable(navigationONActivity, 20);
            navigationONActivity.Click();
        }

        public void navigateOnSuite()
        {
            Browser.waitForElementToBeClickable(navigationONSuite, 20);
            navigationONSuite.Click();
        }

        public void clickONAddButton()
        {
            //Thread.Sleep(3000);
            Browser.waitForImplicitTime(6);
            firstTitlePckg = fisrtDiningPckgTitle.Text;
            Browser.waitForElementToBeClickable(diningFirstAddBtn, 20);
            diningFirstAddBtn.Click();
        }

        public void clickONUpdateButton()
        {
            //Thread.Sleep(3000);
            //firstTitlePckg = fisrtDiningPckgTitle.Text;
            Browser.waitForElementToBeClickable(diningFirstUpdateBtn, 20);
            diningFirstUpdateBtn.Click();
        }

        public void clickONRemoveLink()
        {
            Browser.waitForElementToBeClickable(removePackageLink, 20);
            removePackageLink.Click();
        }

        public void selectPackgeQuantityOption(String NumOfPckg)
        {
            Browser.waitForElementToBeClickable(firstPckgQuntityDrp, 20);
            SelectElement oSelect = new SelectElement(firstPckgQuntityDrp);
            oSelect.SelectByValue(NumOfPckg);
        }

        public void verifyCostOfOneDiningPckg()
        {
            //singlePckgCost = packageCostAtCostSummry.Text;
            singlePckgCost = Browser.stringToDoubleConvert(packageCostAtCostSummry);
        }

        public Boolean verifyCostAftrDiningPckgQuantityUpdate(int numOfDay)
        {
            double afterUpdtquantity = Browser.stringToDoubleConvert(packageCostAtCostSummry);
            double mutlCostPckg = singlePckgCost * numOfDay;
            if (afterUpdtquantity == mutlCostPckg)
            {
                return true;
            }
            else {
                return false;
            }
        }

        public Boolean verifyCheckinDateOnCost()
        {
            String chkInDt = costSumyCheckInDt.Text;
            String suitChInDt = SuitePage.checkIndtTxt;
            string mmDDDate = suitChInDt.Substring(0, 5);
            return chkInDt.Equals(mmDDDate);
        }

        public Boolean verifyCheckOutDateOnCost()
        {
            String chkOutDt = costSumyCheckOutDt.Text;
            String suitChOutDt = SuitePage.checkOutdtTxt;
            string mmDDDate = suitChOutDt.Substring(0, 5);
            return chkOutDt.Equals(mmDDDate);
        }

        public Boolean packgDrpVal()
        {
            //Thread.Sleep(3000);
            Browser.waitForImplicitTime(6);
            int expctdVal = 2;
            if (perDayPersonTextOnDining.Exists())
            {
                Browser.waitForElementToDisplayed(perDayPersonTextOnDining, 20);
                int options = Browser.driver.FindElements(By.XPath("(//div[contains(text(),'Person / Per Day')])[1]//parent::div//following-sibling::div//select//option")).Count();
                int actual = options;
                return actual.Equals(expctdVal);
            }
            else { return true; }
            
        }

        public Boolean verifyBillableCost(int noOfGuest)
        {
            //Thread.Sleep(3000);
            Browser.waitForImplicitTime(4);
            Browser.waitForElementToDisplayed(diningFirstAddBtn, 20);
            if (personPerdayCost.Exists())
            {

                String perDayCost = personPerdayCost.Text;
                String actulPerDyaVal = perDayCost.Substring(0, 6);
                String remvDlr = actulPerDyaVal.Substring(1);
                decimal perDayVal = Convert.ToDecimal(remvDlr);
                decimal ExpTotal = perDayVal * noOfGuest;

                String totalCostStr = personPerdayTotalCost.Text;
                String remvDolrVal = totalCostStr.Substring(1);
                decimal ActualTotal = Convert.ToDecimal(remvDolrVal);

                return ActualTotal.Equals(ExpTotal);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyPersonPerDayCost(int noOfGuest)
        {
            //Thread.Sleep(3000);
            Browser.waitForImplicitTime(6);
            Browser.waitForElementToDisplayed(diningFirstAddBtn, 20);
            if (personPerdayTotalCost.Exists())
            {
                String totalCostStr = personPerdayTotalCost.Text;
                String remvDolrVal = totalCostStr.Substring(1);
                decimal ActualTotal = Convert.ToDecimal(remvDolrVal);
                decimal ExpPersonPerDayCost = ActualTotal / noOfGuest;

                //double ActualTotal = Convert.ToDouble(remvDolrVal);
                //double ExpPersonPerDayCost = ActualTotal / noOfGuest;

                Browser.waitForElementToDisplayed(personPerdayCost, 20);
                String perDayCost = personPerdayCost.Text;
                String actulPerDyaVal = perDayCost.Substring(0, 6);
                String remvDlr = actulPerDyaVal.Substring(1);
                decimal perDayVal = Convert.ToDecimal(remvDlr);

                return ExpPersonPerDayCost.Equals(perDayVal);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyPromotioanlSaving()
        {
            String promotionlStr = dinignPromSavngCost.Text;
            String promoCost = promotionlStr.Substring(2);

            String yourSavingStr = dinignYourSavngCost.Text;

            return promoCost.Equals(yourSavingStr);
        }

        public Boolean verifyAddintionOfPackage()
        {
            String suiteTotal = Pages.Payment.suiteTotaPricelLable.Text;
            String Total = Pages.Payment.toatlCostLable.Text;
            return suiteTotal.Equals(Total);
        }

        public void addmutilpleDiningPackage()
        {
            Browser.waitForElementToBeClickable(diningFirstAddBtn, 20);
            String frstcostStr = firstPackageCost.Text;
            String RemovedDlrfrstcostStrStr = frstcostStr.Substring(1);
            firstPckgCost = Convert.ToDecimal(RemovedDlrfrstcostStrStr);

           /* if (secondDiningPckgTitle.Exists())
            {
                String scndCostStr = secondPackageCost.Text;
                String RemovedDlrscndCostStrStr = scndCostStr.Substring(1);
                scndPckgCost = Convert.ToDecimal(RemovedDlrscndCostStrStr);
            }

            if (secondDiningPckgTitle.Exists())
            {

                Browser.waitForElementToBeClickable(diningSecondPckDrp, 20);
                SelectElement oSelect1 = new SelectElement(diningSecondPckDrp);
                oSelect1.SelectByValue("1");
                diningSecndAddBtn.Click();
            }*/

            Browser.waitForElementToBeClickable(diningFirstPckDrp, 20);
            SelectElement oSelect = new SelectElement(diningFirstPckDrp);
            oSelect.SelectByValue("1");
            diningFirstAddBtn.Click();

            
        }

        public static decimal secondPckgCost;

        public Boolean verifyaddedPackageCostTotal(int noOfDays)
        {
            //Thread.Sleep(3000);
            Browser.waitForImplicitTime(6);
            decimal firstPckgExpctdAddtion = firstPckgCost * noOfDays;
            decimal scndPckgExpctdAddtion = scndPckgCost * noOfDays;

            String firstPckgCostStr = firstAddedPackgeCost.Text;
            String RemovedDlrfirstPckgCostStr = firstPckgCostStr.Substring(1);
            decimal frstPckgCost = Convert.ToDecimal(RemovedDlrfirstPckgCostStr);

            if (secondAddedPackgeCost.Exists())
            {
                String secondPckgCostStr = secondAddedPackgeCost.Text;
                String RemovedDlrsecondPckgCostStr = secondPckgCostStr.Substring(1);
                secondPckgCost = Convert.ToDecimal(RemovedDlrsecondPckgCostStr);
            }

            Boolean firstPckRes = firstPckgExpctdAddtion.Equals(frstPckgCost);
            Boolean scndPckRes = scndPckgExpctdAddtion.Equals(secondPckgCost);

            if (firstPckRes == scndPckRes)
            {
                decimal actualTotal = frstPckgCost + secondPckgCost;

                String packageTotal = Pages.Payment.packageTotalCost.Text;
                String packageTotalStr = packageTotal.Substring(1);
                decimal expctdPackageTotalCost = Convert.ToDecimal(packageTotalStr);

                return actualTotal.Equals(expctdPackageTotalCost);
            }
            else {
                return false;
            }            
        }

        public Boolean verifyTotalAfterPckgAdd()
        {
            String packageTotal = Pages.Payment.packageTotalCost.Text;
            String packageTotalStr = packageTotal.Substring(1);
            decimal expctdPackageTotalCost = Convert.ToDecimal(packageTotalStr);

            /*String promoPriceStr = Pages.Payment.promotionalSavngPrice.Text;
            String promoPriceRemvDlrStr = promoPriceStr.Substring(3);
            decimal expctdPromotionalCost = Convert.ToDecimal(promoPriceRemvDlrStr);*/

            String SuiteTotalStr = Pages.Payment.suiteTotaPricelLable.Text;
            String SuiteTotalStrRemvDlrStr = SuiteTotalStr.Substring(1);
            decimal expctdSuiteTotal = Convert.ToDecimal(SuiteTotalStrRemvDlrStr);

            decimal ExptdTotal = expctdPackageTotalCost + expctdSuiteTotal;

            String totalCostStr = Pages.Payment.toatlCostLable.Text;
            String totalCostRemvDlrStr = totalCostStr.Substring(1);
            decimal ActualTotalCost = Convert.ToDecimal(totalCostRemvDlrStr);

            return ExptdTotal.Equals(ActualTotalCost);

        }

        public Boolean verifyUpdatePckgCost()
        {
            String packageTotal = Pages.Payment.packageTotalCost.Text;
            String packageTotalStr = packageTotal.Substring(1);
            decimal PackageTotalCostAfterAddedPckg = Convert.ToDecimal(packageTotalStr);

            Browser.waitForElementToBeClickable(costSummryFirstPackgDrp, 20);
            SelectElement oSelect = new SelectElement(costSummryFirstPackgDrp);
            oSelect.SelectByValue("2");

            String packageTotal1 = Pages.Payment.packageTotalCost.Text;
            String packageTotalStr1 = packageTotal1.Substring(1);
            decimal afterUpdatePackageTotalCost = Convert.ToDecimal(packageTotalStr1);


            decimal expSuiteTotalafterUpdate = PackageTotalCostAfterAddedPckg + firstPckgCost;

            return afterUpdatePackageTotalCost.Equals(expSuiteTotalafterUpdate);
        }

        public Boolean perPackgeDrpVal()
        {
            if (perPackageTextOnDining.Exists())
            {
                int options = Browser.driver.FindElements(By.XPath("(//div[text()='Per Package'])[1]//parent::div//following-sibling::div//select//option")).Count();
                int expVal = 4;
                return expVal.Equals(options);
            }
            else { return true; }
        }

    }
}
