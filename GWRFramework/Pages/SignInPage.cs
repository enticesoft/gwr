﻿using GWRFramework.TestData;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace GWRFramework
{
    public class SignInPage
    {
        [FindsBy(How = How.XPath, Using = "//button[contains(text(), 'Create An Account')]")]
        public IWebElement createAccountBtn;

        [FindsBy(How = How.XPath, Using = "//li[@id='sign-in-create-account']//span[text()='Sign In']")]
        public IWebElement signInlink;

        [FindsBy(How = How.Id, Using = "email")]
        public IWebElement emailtxt;

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement passwordtxt;

        [FindsBy(How = How.XPath, Using = "//button[text()='Sign In']")]
        public IWebElement signInBtn;

        public static String signBtnStr = "//button[@type='button'][text()='Sign In']";

        [FindsBy(How = How.XPath, Using = " //div[@role='navigation'][@aria-label='Account Options']")]
        public IWebElement userMenulink;

        [FindsBy(How = How.XPath, Using = "//button/descendant::div[contains(text(), 'Sign in with Google')]")]
        public IWebElement signInWithGoogleBtn;

        [FindsBy(How = How.XPath, Using = "//button/descendant::div[contains(text(), 'Sign up with Google')]")]
        public IWebElement signUpWithGoogleBtn;

        [FindsBy(How = How.XPath, Using = "//button/descendant::div[contains(text(), 'Continue with Facebook')]")]
        public IWebElement continueWithFacebookBtn;

        [FindsBy(How = How.XPath, Using = "//button[@type='button']//span[text()='Close X']")]
        public IWebElement CloseSingBtn;

        [FindsBy(How = How.XPath, Using = "(//*[contains(., 'Log in with Google')])[7]")]
        public IWebElement testGoogle;


        //[FindsBy(How = How.Id, Using = "identifierId")]
        //public IWebElement emailTextboxOnGoogle;

        [FindsBy(How = How.XPath, Using = "//input[@id='identifierId']|//input[@id='Email']")]
        public IWebElement emailTextboxOnGoogle;

        [FindsBy(How = How.Id, Using = "email")]
        public IWebElement emailTextboxOnFacebook;

        [FindsBy(How = How.Id, Using = "pass")]
        public IWebElement passextboxOnFacebook;

        [FindsBy(How = How.Name, Using = "login")]
        public IWebElement loginBtnOnFacebook;
        

        [FindsBy(How = How.XPath, Using = "//*[@id='identifierNext']/descendant::span[contains(text(),'Next')]")]
        public IWebElement emailNextBtnOnGoogle;
                

        [FindsBy(How = How.Name, Using = "password")]
        public IWebElement passwordTextboxOnGoogle;

        [FindsBy(How = How.XPath, Using = "//*[@id='passwordNext']/descendant::span[contains(text(),'Next')]")]
        public IWebElement passNextBtnOnGoogle;

        public static String waitSignInSpinnerStr = "//*[@class='loading-indicator']";

        [FindsBy(How = How.XPath, Using = "//p[contains(.,'Sorry! you exceeded max attempts. Your account is locked. Please verify your mail to unlock your account')]")]
        public IWebElement lockAcMessage;

        [FindsBy(How = How.XPath, Using = "//p[contains(.,'Please enter valid user name or password. You have 4 out of 5 attempts.')]")]
        public IWebElement lockAcMessageForInvalFirstAttmpt;

        [FindsBy(How = How.XPath, Using = "//div[text()='Your password has succesfully changed.']")]
        public IWebElement ChangePasswordSuccessMsg;

        public static string emailIdForLogin;

        public Boolean verifySuccessMsgAfterResetPass()
        {
            Browser.waitForElementToBeClickable(ChangePasswordSuccessMsg, 30);
            return ChangePasswordSuccessMsg.Exists();
        }

        public Boolean verifyLockAcMessage()
        {
            Browser.waitForElementToBeClickable(lockAcMessage, 20);
            return lockAcMessage.Exists();
        }

        public Boolean verifyLockAcMessageForFirstMessage()
        {
            Browser.waitForElementToBeClickable(lockAcMessageForInvalFirstAttmpt, 20);
            return lockAcMessageForInvalFirstAttmpt.Exists();
        }
        

        public void clickCreateAccountBtn()
        {
            //Browser.waitForElementToDisplayed(createAccountBtn, 25);
            Browser.waitForElementToBeClickable(createAccountBtn, 20);
            Browser.javaScriptClick(createAccountBtn);
            //createAccountBtn.Click();
        }

         public void sendEmail(String email)
        {
            Browser.waitForElementToDisplayed(emailtxt, 20);
            //Browser.clearTextboxValue(emailtxt);
            emailtxt.SendKeys(email);
        }

        public void closeSingInPopUp()
        {
            Browser.waitForElementToBeClickable(CloseSingBtn, 20);
            CloseSingBtn.Click();
        }

        public void sendPass(String pass)
        {
            Browser.waitForElementToDisplayed(passwordtxt, 10);
            //Browser.clearTextboxValue(emailtxt);
            passwordtxt.SendKeys(pass);
        }

        public void clickSignInbtn()
        {
            Browser.waitForElementToBeClickable(signInBtn, 10);
            signInBtn.Click();
            waitSignInSprinnerOut(waitSignInSpinnerStr);
        }

        public void clickSignInWithGoogleBtn()
        {
            Browser.waitForElementToBeClickable(signInWithGoogleBtn, 10);
            signInWithGoogleBtn.Click();

            Thread.Sleep(10000);
            int windwCnt = Browser.webDriver.WindowHandles.Count;
            if (windwCnt < 2)
            {
                CloseSingBtn.Click();
                Pages.PropertyHome.clickSignIn();
                Thread.Sleep(5000);
                Browser.waitForElementToBeClickable(signInWithGoogleBtn, 10);
                signInWithGoogleBtn.Click();
                Thread.Sleep(10000);
            }
        }

        public void clickContinueWithFacebookBtn()
        {
            Browser.waitForElementToBeClickable(continueWithFacebookBtn, 10);
            continueWithFacebookBtn.Click();

            Thread.Sleep(10000);
            int windwCnt = Browser.webDriver.WindowHandles.Count;
            if (windwCnt < 2) {
                CloseSingBtn.Click();
                Pages.PropertyHome.clickSignIn();
                Thread.Sleep(5000);
                Browser.waitForElementToBeClickable(continueWithFacebookBtn, 10);
                continueWithFacebookBtn.Click();
                Thread.Sleep(10000);
            }
        }



        public void completeLoginStepsOnGoogle(String email, String password)
        {
            //Browser.NavigateTo
            Browser.webDriver.Navigate().GoToUrl("https://stackoverflow.com/users/login?ssrc=head&returnurl=https%3a%2f%2fstackoverflow.com%2f");
            testGoogle.Click();

            Browser.waitForElementToDisplayed(emailTextboxOnGoogle, 40);
            emailTextboxOnGoogle.SendKeys(email);
            Browser.waitForElementToBeClickable(emailNextBtnOnGoogle, 10);
            Browser.javaScriptClick(emailNextBtnOnGoogle);

            Browser.waitForElementToDisplayed(passwordTextboxOnGoogle, 60);
            passwordTextboxOnGoogle.SendKeys(password);
            Browser.waitForElementToBeClickable(passNextBtnOnGoogle, 10);
            Browser.javaScriptClick(passNextBtnOnGoogle);

            Thread.Sleep(7000);
            Browser.NavigateToProperty();

            Pages.PropertyHome.clickSignIn();

            Browser.waitForElementToBeClickable(signInWithGoogleBtn, 20);
            Browser.javaScriptClick(signInWithGoogleBtn);
            Thread.Sleep(10000);
            if (Pages.PropertyHome.userSignedInMenu.Exists())
            {
                Thread.Sleep(2000);
            }
            else
            {
                Browser.webDriver.Navigate().Refresh();
                Pages.PropertyHome.clickSignIn();
                Thread.Sleep(3000);
                Browser.waitForElementToBeClickable(signInWithGoogleBtn, 20);
                Browser.javaScriptClick(signInWithGoogleBtn);
            }

        }

        public void completeSignUpStepsOnGoogle(String email, String password)
        {
            //Browser.NavigateTo
            Browser.webDriver.Navigate().GoToUrl("https://stackoverflow.com/users/login?ssrc=head&returnurl=https%3a%2f%2fstackoverflow.com%2f");
            testGoogle.Click();

            Browser.waitForElementToDisplayed(emailTextboxOnGoogle, 40);
            emailTextboxOnGoogle.SendKeys(email);
            Browser.waitForElementToBeClickable(emailNextBtnOnGoogle, 10);
            Browser.javaScriptClick(emailNextBtnOnGoogle);

            Browser.waitForElementToDisplayed(passwordTextboxOnGoogle, 60);
            passwordTextboxOnGoogle.SendKeys(password);
            Browser.waitForElementToBeClickable(passNextBtnOnGoogle, 10);
            Browser.javaScriptClick(passNextBtnOnGoogle);

            Thread.Sleep(7000);
            
            Browser.NavigateToProperty();

            Pages.PropertyHome.clickSignIn();
            Browser.javaScriptClick(createAccountBtn);

            Browser.waitForElementToBeClickable(signUpWithGoogleBtn, 20);
            Browser.javaScriptClick(signUpWithGoogleBtn);
            Thread.Sleep(10000);
            if (Pages.PropertyHome.userSignedInMenu.Exists())
            {
                Thread.Sleep(2000);
            }
            else
            {
                Browser.webDriver.Navigate().Refresh();
                Pages.PropertyHome.clickSignIn();
                Browser.javaScriptClick(createAccountBtn);
                
                Thread.Sleep(3000);
                Browser.waitForElementToBeClickable(signUpWithGoogleBtn, 20);
                Browser.javaScriptClick(signUpWithGoogleBtn);
            }

        }

        public void completeLoginStepsOnFacebook(String email, String password)
        {
            String parentWindow = Browser.webDriver.CurrentWindowHandle;

            String lstWindow = Browser.webDriver.WindowHandles.Last();

            //Switch to the last window
            Browser.webDriver.SwitchTo().Window(lstWindow);

            Browser.waitForElementToDisplayed(emailTextboxOnFacebook, 10);
            emailTextboxOnFacebook.SendKeys(email);
            //Browser.opt.AddArgument("['--disable-web-security', '--user-data-dir', '--allow-running-insecure-content' ]");
            //Browser.waitForElementToBeClickable(emailNextBtnOnGoogle, 10);
            //emailNextBtnOnGoogle.Click();

            Browser.waitForElementToDisplayed(passextboxOnFacebook, 10);
            passextboxOnFacebook.SendKeys(password);
            
            Browser.waitForElementToBeClickable(loginBtnOnFacebook, 10);
            loginBtnOnFacebook.Click();

            //Switch to the parent window
            Browser.webDriver.SwitchTo().Window(parentWindow);
        }

        public void waitSignInSprinnerOut(String xpath)
        {
            //Browser.waitForImplicitTime(3);
            Browser.waitForElementToBeInvisible(20, xpath);
            //Browser.waitForImplicitTime(4);
        }

    }
}
