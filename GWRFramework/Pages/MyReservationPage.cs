﻿using GWRFramework.TestData;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GWRFramework
{
    public class MyReservationPage
    {
        
        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Account Options']//a[text()='My Reservations']")]
        public IWebElement myReservationMenuLink;

        [FindsBy(How = How.XPath, Using = "//span[text()='Find your reservation by Confirmation #.']")]
        public IWebElement FindYourResConfLink;

        [FindsBy(How = How.Id, Using = "reservationId")]
        public IWebElement FindYourResPopResNumber;

        [FindsBy(How = How.Id, Using = "lastName")]
        public IWebElement FindYourResPoplastName;

        [FindsBy(How = How.XPath, Using = "//button[@type='submit'][text()='Search']")]
        public IWebElement FindYourResSeachBtn;        


        [FindsBy(How = How.XPath, Using = "//h3//div[contains(.,'We cannot find any upcoming reservations.')]")]
        public IWebElement upcomingResNotAvalblMsg;

        [FindsBy(How = How.XPath, Using = "//h3//div[contains(.,'We cannot find any past reservations.')]")]
        public IWebElement pastResNotAvalblMsg;

        [FindsBy(How = How.XPath, Using = "//h3//div[contains(.,'We cannot find any canceled reservations.')]")]
        public IWebElement cancelledResNotAvalblMsg;

        [FindsBy(How = How.XPath, Using = "//a[text()='Make a Reservation']")]
        public IWebElement makeAReservationLink;

        [FindsBy(How = How.XPath, Using = "//div[@class='root responsivegrid']")]
        public IWebElement ResponsiveGrid;

        [FindsBy(How = How.XPath, Using = "//a[text()='ADD A PACKAGE']")]
        public IWebElement AddAPackageBtn;

        [FindsBy(How = How.XPath, Using = "//a[text()='ADD A PASS']")]
        public IWebElement AddAPassBtn;

        [FindsBy(How = How.XPath, Using = "//a[text()='ADD CABANAS']")]
        public IWebElement AddCabanaBtn;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Offer Code']//div[text()='AMTCERT']")]
        public IWebElement OfferCodeCostSummary;

        [FindsBy(How = How.XPath, Using = "//div//h2[contains(.,'Indoor Cabana')]")]
        public IWebElement indoorCabanaTitle;

        [FindsBy(How = How.XPath, Using = "//div//h2[contains(.,'Outdoor Cabana')]")]
        public IWebElement outdoorCabanaTitle;

        [FindsBy(How = How.XPath, Using = "//div[text()='Please select package to add in reservation']")]
        public IWebElement CabanaNotAvailableMSG;        

        [FindsBy(How = How.XPath, Using = "(//div//h2[contains(.,'Indoor Cabana')]//parent::div//div[contains(.,'Package Not Available')])[3]")]
        public IWebElement indoorCabanaNotAvailable;        

        [FindsBy(How = How.XPath, Using = "(//h2[text()='Indoor Cabana']//parent::div//following-sibling::div//div[contains(.,'$')])[2]")]
        public IWebElement indoorCabanaPrice;

        [FindsBy(How = How.XPath, Using = "(//div//h2[contains(.,'Outdoor Cabana')]//parent::div//div[contains(.,'Package Not Available')])[3]")]
        public IWebElement outdoorCabanaNotAvailable;

        [FindsBy(How = How.XPath, Using = "(//h2[text()='Outdoor Cabana']//parent::div//following-sibling::div//div[contains(.,'$')])[2]")]
        public IWebElement outdoorCabanaPrice;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Indoor Cabana']//parent::div//following-sibling::div//summary")]
        public IWebElement indoorCabanaselectDatesDrp;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Indoor Cabana']//parent::div//following-sibling::div//summary//following-sibling::ul//li[2]//input")]
        public IWebElement indoorCabanacheckBox;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Indoor Cabana']//parent::div//following-sibling::div//summary//following-sibling::ul//li[2]//label")]
        public IWebElement indoorCabanadrpPrice;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Indoor Cabana']//parent::div//following-sibling::button")]
        public IWebElement indoorCabanaAddBtn;

        public static string InDoorCabanaDatePrice;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Indoor Cabana']//parent::div//following-sibling::div//summary")]
        public IWebElement outdoorCabanaselectDatesDrp;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Indoor Cabana']//parent::div//following-sibling::div//summary//following-sibling::ul//li[2]//input")]
        public IWebElement outdoorCabanacheckBox;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Indoor Cabana']//parent::div//following-sibling::div//summary//following-sibling::ul//li[2]//label")]
        public IWebElement outdoorCabanadrpPrice;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Indoor Cabana']//parent::div//following-sibling::button")]
        public IWebElement outdoorCabanaAddBtn;

        [FindsBy(How = How.XPath, Using = "//button[text()='Save to My Reservation']")]
        public IWebElement saveToMyResBtn;

        [FindsBy(How = How.XPath, Using = "(//div[@overflow='hidden']//div[contains(.,'has been added to your stay.')])[3]")]
        public IWebElement addePckagesNotifctionMsg;

        [FindsBy(How = How.XPath, Using = "//div[contains(.,'Indoor')]//a[text()='[View]']")]
        public IWebElement indoorCabanaViewLink;

        [FindsBy(How = How.XPath, Using = "//div[contains(.,'Outdoor')]//a[text()='[View]']")]
        public IWebElement outdoorCabanaViewLink;

        [FindsBy(How = How.XPath, Using = "//div//a[@intent='reverse'][text()='Add Late Check-out']")]
        public IWebElement addLateCheckOutBtnOnCMP;

        [FindsBy(How = How.XPath, Using = "//div[text()='Check-out']//following-sibling::div[text()='2 p.m.']")]
        public IWebElement TwoPMAddedLCOTxt;

        [FindsBy(How = How.XPath, Using = "//button//div[text()='Add To My Reservation']")]
        public IWebElement addToMyReservationBtn;

        public static string outDoorCabanaDatePrice;

        String indoorCabanaAddedPrice = "//div[@aria-label='Summary Item Packages']//following-sibling::div//div[contains(.,'Indoor Cabana')]//following-sibling::div[contains(.,'";
        String outdoorCabanaAddedPrice = "//div[@aria-label='Summary Item Packages']//following-sibling::div//div[contains(.,'Outdoor Cabana')]//following-sibling::div[contains(.,'";

        String indoorCabanaAddedPrice1 = "')]";

        String viewOffer = "//div[text()='";
        String viewOffer1 = "']/parent::div//following-sibling::a[text()='View Details']";

        String resId = "(//div[@id='root']//div[contains(.,'";
        String resId1 = "')])[7]";

        String DinigPckgTitle = "//div//h2[text()='";
        String DinigPckgTitleforCOst = "(//div//h2[text()='";
        String DinigPckgTitle1 = "']";
        String DinigPckgTitleforCOstAddeCpkg = "((//div//h2[text()='";
        String DinigPckgTitleforCOstAddeCpkg1 = "')])[2]";

        String packageAddBtn = "']//parent::div//button";

        String DinigPckgTitleCost = "//parent::div//following-sibling::div)[4]//div[contains(.,'";
        String DinigPckgTitleCost1 = "')]";

        String SuiteTitle = "//div[@aria-label='Summary Item Suite Details']//div[contains(.,'";
        String SuiteTitle1 = "')]";

        String addedPckgTitle = "//div[@aria-label='Summary Item Packages']//following-sibling::div//div[text()='";
        String addedPckgCost = "']//parent::div//following-sibling::div//div[contains(.,'$";

        String ResNumOnMYResPage = "//div[text()='";
        String ResNumOnMYResPage1 = "']";

        String checkInDateResPage = "(//div[text()='";
        String checkInDateResPage1 = "']//parent::div)[1]//following-sibling::div[contains(.,'";

        String GuestDetails = "']//parent::div)[1]//following-sibling::div[contains(.,'2 Adult, 1 Kid')]";


        String ReserVDetailSpinner = "//*[contains(@transform,'rotate')]";
        String MyResSprinner = "//*[contains(@transform,'rotate')]";

        String LOCPriceATCost = "(//div[@aria-label='Summary Item Package Amount']//div[contains(.,'";
        String LOCPriceATCost1 = "')])[2]";
        public static Boolean LCOCost = true;

        public static decimal addedFrstPckgCostAtCostSummary;
        public static decimal addedScndPckgCostAtCostSummary;
        public static decimal suiteTotalBefrPassesAdd;

        public static decimal indoorCabanaPriceafterConvert;
        public static decimal outdoorCabanaPriceafterConvert;

        public Boolean verifySuiteTitleAtCOst()
        {
            Browser.waitForImplicitTime(10);
            IWebElement SuiteTitleCost = ResponsiveGrid.FindElement(By.XPath(SuiteTitle + RateCalendar.SuiteTitle + SuiteTitle1));
            return SuiteTitleCost.Exists();
        }

        public Boolean verifyResNumOnMyResPage()
        {
            Browser.waitForImplicitTime(10);
            IWebElement reseNum = ResponsiveGrid.FindElement(By.XPath(ResNumOnMYResPage + NewConfirmationPage.afterSplitResNum + ResNumOnMYResPage1));
            return reseNum.Exists();
        }

        public Boolean verifyCheckInDate() {
            //String checkIn = DataAccess.getData(3, 2);          
            //DateTime oDate = DateTime.Parse(checkIn);
            DateTime oDate = DateTime.Parse("25/11/2020");
            string DateFor = oDate.ToString("ddddddd") + ", " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(oDate.Month) + ", " + oDate.Day + ", " + oDate.Year;
            Console.WriteLine(DateFor);

            IWebElement checkInDate = ResponsiveGrid.FindElement(By.XPath(checkInDateResPage + NewConfirmationPage.afterSplitResNum + checkInDateResPage1 + SuiteTitle1));
            return checkInDate.Exists();            
        }

        public Boolean verifyCheckOutDate()
        {
            //String checkIn = DataAccess.getData(3, 2);          
            //DateTime oDate = DateTime.Parse(checkIn);
            DateTime oDate = DateTime.Parse("26/11/2020");
            string DateFor = oDate.ToString("ddddddd") + ", " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(oDate.Month) + ", " + oDate.Day + ", " + oDate.Year;
            Console.WriteLine(DateFor);

            IWebElement checkOutDate = ResponsiveGrid.FindElement(By.XPath(checkInDateResPage + NewConfirmationPage.afterSplitResNum + checkInDateResPage1 + SuiteTitle1));
            return checkOutDate.Exists();
        }

        public Boolean verifyGuestCOunt()
        {
            IWebElement GeuestCount = ResponsiveGrid.FindElement(By.XPath(checkInDateResPage + NewConfirmationPage.afterSplitResNum + GuestDetails));
            return GeuestCount.Exists();
        }

        public Boolean verifyFirstDinignPckgsTitles()
        {
            if (SuitePage.DiningFirstPckgTitl != null)
            {
                Browser.waitForImplicitTime(10);
                //IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitle + SuitePage.DiningFirstPckgTitl + DinigPckgTitle1));
                IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath("(//div//h2)[1]"));
                String firstPackgTtil = diningPckgTitle.Text;
                String actual = Browser.convertToCamleCase(firstPackgTtil);
                String expectd = Browser.convertToCamleCase(SuitePage.DiningFirstPckgTitl);

                return actual.Equals(expectd);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifySecondDinignPckgsTitles()
        {
            if (SuitePage.DiningSecondPckgTitl != null)
            {
                Browser.waitForImplicitTime(10);
                //IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitle + SuitePage.DiningSecondPckgTitl + DinigPckgTitle1));
                //return diningPckgTitle.Exists();

                IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath("(//div//h2)[2]"));
                String scndPackgTtil = diningPckgTitle.Text;
                String actual = Browser.convertToCamleCase(scndPackgTtil);
                String expectd = Browser.convertToCamleCase(SuitePage.DiningSecondPckgTitl);

                return actual.Equals(expectd);
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyThirdDinignPckgsTitles()
        {
            if (SuitePage.DiningThirdPckgTitl != null)
            {
                Browser.waitForImplicitTime(10);
                //IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitle + SuitePage.DiningThirdPckgTitl + DinigPckgTitle1));
                //return diningPckgTitle.Exists();

                IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath("(//div//h2)[3]"));
                String thrdPackgTtil = diningPckgTitle.Text;
                String actual = Browser.convertToCamleCase(thrdPackgTtil);
                String expectd = Browser.convertToCamleCase(SuitePage.DiningThirdPckgTitl);

                return actual.Equals(expectd);
            }
            else {
                return true;
            }
        }

        public Boolean verifyFirstDinignPckgscost()
        {
            if (SuitePage.DiningFirstPckgTitl != null)
            {
                String camlTitlStr = Browser.convertToCamleCase(SuitePage.DiningFirstPckgTitl);
                String costWthoutPoint = SuitePage.DiningFirstPckgTitlCost.Substring(0,3);
                Browser.waitForImplicitTime(10);
                //IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitleforCOst + SuitePage.DiningFirstPckgTitl + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleCost1));
                IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath("("+DinigPckgTitleforCOst + camlTitlStr + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleCost1+")[2]"));

                return diningPckgTitle.Exists();
            }
            else {
                return true;
            }
        }

        public Boolean verifySecondDinignPckgscost()
        {
            if (SuitePage.DiningSecondPckgTitl != null)
            {
                String camlTitlStr = Browser.convertToCamleCase(SuitePage.DiningSecondPckgTitl);

                String costWthoutPoint = SuitePage.DiningSecondPckgTitlCost.Substring(0, 3);
                Browser.waitForImplicitTime(10);
                //IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitleforCOst + SuitePage.DiningSecondPckgTitl + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleCost1));
                IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath("(" + DinigPckgTitleforCOst + camlTitlStr + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleCost1 + ")[2]"));

                return diningPckgTitle.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyThirdDinignPckgscost()
        {
            if (SuitePage.DiningThirdPckgTitl != null)
            {
                String camlTitlStr = Browser.convertToCamleCase(SuitePage.DiningThirdPckgTitl);

                String costWthoutPoint = SuitePage.DiningThirdPckgTitlCost.Substring(0, 3);
                Browser.waitForImplicitTime(10);
                //IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitleforCOst + SuitePage.DiningThirdPckgTitl + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleCost1));
                IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath("(" + DinigPckgTitleforCOst + camlTitlStr + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleCost1 + ")[2]"));

                return diningPckgTitle.Exists();
            }
            else
            {
                return true;
            }
        }

        public void clickAddBtnFrsPckg()
        {
            if (SuitePage.DiningFirstPckgTitl != null)
            {
                //String costWthoutPoint = SuitePage.DiningThirdPckgTitl.Substring(0, 3);
                String CamlTitl = Browser.convertToCamleCase(SuitePage.DiningFirstPckgTitl);
                Browser.waitForImplicitTime(10);
                //IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitle + SuitePage.DiningFirstPckgTitl + packageAddBtn));
                IWebElement diningPckgTitle = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitle + CamlTitl + packageAddBtn));

                diningPckgTitle.Click();
                //diningPckgTitle.Click();

                /*if (SuitePage.DiningSecondPckgTitl != null)
                {
                    Browser.waitForImplicitTime(10);
                    IWebElement diningPckgTitle1 = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitle + SuitePage.DiningSecondPckgTitl + packageAddBtn));
                    diningPckgTitle1.Click();
                }*/
            }
        }

        public Boolean verifyAddedPackageFisrTitle()
        {
            if (SuitePage.DiningFirstPckgTitl != null)
            {
                //String costWthoutPoint = SuitePage.DiningThirdPckgTitl.Substring(0, 3);
                String converCm = Browser.convertToCamleCase(SuitePage.DiningFirstPckgTitl);
                Browser.waitForImplicitTime(10);
                //IWebElement diningPckgFirstTitle = ResponsiveGrid.FindElement(By.XPath(addedPckgTitle + SuitePage.DiningFirstPckgTitl + DinigPckgTitle1));
                IWebElement diningPckgFirstTitle = ResponsiveGrid.FindElement(By.XPath(addedPckgTitle + converCm + DinigPckgTitle1));

                return diningPckgFirstTitle.Exists();
            }
            else {
                return true;
            }
        }

        public Boolean verifyAddedPackageScndTitle()
        {
            if (SuitePage.DiningSecondPckgTitl != null)
            {
                //String costWthoutPoint = SuitePage.DiningThirdPckgTitl.Substring(0, 3);
                Browser.waitForImplicitTime(10);
                IWebElement diningPckgScndTitle = ResponsiveGrid.FindElement(By.XPath(addedPckgTitle + SuitePage.DiningSecondPckgTitl + DinigPckgTitle1));
                return diningPckgScndTitle.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyAddedPackagefirstCost()
        {
            if (SuitePage.DiningFirstPckgTitl != null)
            {
                String costWthoutPoint = SuitePage.DiningFirstPckgTitlCost.Substring(0, 3);
                String camlTitl = Browser.convertToCamleCase(SuitePage.DiningFirstPckgTitl);
                Browser.waitForImplicitTime(10);
                IWebElement diningPckgfrsCost = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitleforCOstAddeCpkg + camlTitl + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleforCOstAddeCpkg1));
                String addedFirstCost = diningPckgfrsCost.Text;
                String remvDolr = addedFirstCost.Substring(1);

                addedFrstPckgCostAtCostSummary = Browser.stringToDecimalConvert(diningPckgfrsCost);

                IWebElement addedDiningPckgfrsCost = ResponsiveGrid.FindElement(By.XPath(addedPckgTitle + camlTitl + addedPckgCost + remvDolr + DinigPckgTitleCost1));
                
                return addedDiningPckgfrsCost.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyAddedPackagescndCost()
        {
            if (SuitePage.DiningSecondPckgTitl != null)
            {
                String costWthoutPoint = SuitePage.DiningSecondPckgTitl.Substring(0, 3);
                Browser.waitForImplicitTime(10);
                IWebElement diningPckgsndCost = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitleforCOstAddeCpkg + SuitePage.DiningSecondPckgTitl + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleforCOstAddeCpkg1));
                String addedSecndCost = diningPckgsndCost.Text;
                String remvDolr = addedSecndCost.Substring(1);

                addedScndPckgCostAtCostSummary = Browser.stringToDecimalConvert(diningPckgsndCost);

                IWebElement addedDiningPckgscndCost = ResponsiveGrid.FindElement(By.XPath(addedPckgTitle + SuitePage.DiningSecondPckgTitl + addedPckgCost + remvDolr + DinigPckgTitleCost1));

                return addedDiningPckgscndCost.Exists();
            }
            else
            {
                return true;
            }
        }

        //Add A Pass TC code ====================
        public Boolean verifyFirstActivityPckgsTitles()
        {
            if (SuitePage.PupPassTitle != null)
            {
                Browser.waitForImplicitTime(10);
                IWebElement actvityPassFrstPckgTitle = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitle + SuitePage.PupPassTitle + DinigPckgTitle1));
                return actvityPassFrstPckgTitle.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifySecondActivityPckgsTitles()
        {
            if (SuitePage.PawPassTitle != null)
            {
                Browser.waitForImplicitTime(10);
                IWebElement actvityPasssecondPckgTitle = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitle + SuitePage.PawPassTitle + DinigPckgTitle1));
                return actvityPasssecondPckgTitle.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyThirdActivityPckgsTitles()
        {
            if (SuitePage.WolfPassTitle != null)
            {
                Browser.waitForImplicitTime(10);
                IWebElement actvityPassThirdPckgTitle = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitle + SuitePage.WolfPassTitle + DinigPckgTitle1));
                return actvityPassThirdPckgTitle.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyFirstActivityPckgscost()
        {
            if (SuitePage.PupPassTitle != null)
            {
                String costWthoutPoint = SuitePage.PupPassCost;
                Browser.waitForImplicitTime(10);
                IWebElement activityPckgCostFrst = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitleforCOst + SuitePage.PupPassTitle + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleCost1));
                return activityPckgCostFrst.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifySecondActivityPckgscost()
        {
            if (SuitePage.PawPassTitle != null)
            {
                String costWthoutPoint = SuitePage.PawPassCost;
                Browser.waitForImplicitTime(10);
                IWebElement activityPckgCostSnd = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitleforCOst + SuitePage.PawPassTitle + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleCost1));
                return activityPckgCostSnd.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyThirdActivityPckgscost()
        {
            if (SuitePage.WolfPassTitle != null)
            {
                String costWthoutPoint = SuitePage.WolfPassCost;
                Browser.waitForImplicitTime(10);
                IWebElement activityPckgCostThird = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitleforCOst + SuitePage.WolfPassTitle + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleCost1));
                return activityPckgCostThird.Exists();
            }
            else
            {
                return true;
            }
        }

        public void clickAddBtnActivityPckg()
        {
            if (SuitePage.PupPassTitle != null)
            {
                //String costWthoutPoint = SuitePage.DiningThirdPckgTitl.Substring(0, 3);
                Browser.waitForImplicitTime(10);                
                suiteTotalBefrPassesAdd = Browser.stringToDecimalConvert(Pages.Payment.packageTotalCost);
                IWebElement ActvtyPckgTitleBtn = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitle + SuitePage.PupPassTitle + packageAddBtn));
                ActvtyPckgTitleBtn.Click();


                if (SuitePage.WolfPassTitle != null)
                {
                    Browser.waitForImplicitTime(10);
                    IWebElement ActvtyPckgScndTitleBtn = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitle + SuitePage.WolfPassTitle + packageAddBtn));
                    ActvtyPckgScndTitleBtn.Click();
                }
            }
        }

        public Boolean verifyAddedActvityPackageFisrTitle()
        {
            if (SuitePage.PupPassTitle != null)
            {
                //String costWthoutPoint = SuitePage.DiningThirdPckgTitl.Substring(0, 3);
                Browser.waitForImplicitTime(10);
                IWebElement actvityPckgFirstTitle = ResponsiveGrid.FindElement(By.XPath(addedPckgTitle + SuitePage.PupPassTitle + DinigPckgTitle1));
                return actvityPckgFirstTitle.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyAddedActvityPackageScndTitle()
        {
            if (SuitePage.WolfPassTitle != null)
            {
                //String costWthoutPoint = SuitePage.DiningThirdPckgTitl.Substring(0, 3);
                Browser.waitForImplicitTime(10);
                IWebElement actvityPckgScndTitle = ResponsiveGrid.FindElement(By.XPath(addedPckgTitle + SuitePage.WolfPassTitle + DinigPckgTitle1));
                return actvityPckgScndTitle.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyAddedActivityPackagefirstCost()
        {
            if (SuitePage.PupPassTitle != null)
            {
                String costWthoutPoint = SuitePage.PupPassCost.Substring(0, 3);
                Browser.waitForImplicitTime(10);
                IWebElement activityPckgfrsCost = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitleforCOstAddeCpkg + SuitePage.PupPassTitle + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleforCOstAddeCpkg1));
                String addedFirstCost = activityPckgfrsCost.Text;
                String remvDolr = addedFirstCost.Substring(1);

                addedFrstPckgCostAtCostSummary = Browser.stringToDecimalConvert(activityPckgfrsCost);

                IWebElement addedDiningPckgfrsCost = ResponsiveGrid.FindElement(By.XPath(addedPckgTitle + SuitePage.PupPassTitle + addedPckgCost + remvDolr + DinigPckgTitleCost1));

                return addedDiningPckgfrsCost.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyAddedActivityPackageScndCost()
        {
            if (SuitePage.WolfPassTitle != null)
            {
                String costWthoutPoint = SuitePage.WolfPassCost.Substring(0, 3);
                Browser.waitForImplicitTime(10);
                IWebElement activityPckgScndCost = ResponsiveGrid.FindElement(By.XPath(DinigPckgTitleforCOstAddeCpkg + SuitePage.WolfPassTitle + DinigPckgTitle1 + DinigPckgTitleCost + costWthoutPoint + DinigPckgTitleforCOstAddeCpkg1));
                String addedFirstCost = activityPckgScndCost.Text;
                String remvDolr = addedFirstCost.Substring(1);

                addedScndPckgCostAtCostSummary = Browser.stringToDecimalConvert(activityPckgScndCost);

                IWebElement addedDiningPckgfrsCost = ResponsiveGrid.FindElement(By.XPath(addedPckgTitle + SuitePage.WolfPassTitle + addedPckgCost + remvDolr + DinigPckgTitleCost1));

                return addedDiningPckgfrsCost.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyAddedPackagesSUiteTotal()
        {
            decimal expectdSuitTotal = addedFrstPckgCostAtCostSummary + addedScndPckgCostAtCostSummary;
            decimal actualSuiteTotal = Browser.stringToDecimalConvert(Pages.Payment.packageTotalCost);
            return expectdSuitTotal.Equals(actualSuiteTotal);
        }

        public Boolean verifyAddedActivityPackagesSUiteTotal()
        {
            decimal addedPassesTotal = addedFrstPckgCostAtCostSummary + addedScndPckgCostAtCostSummary;
            //decimal prevAddedPckgPrice =  addedPassesTotal - suiteTotalBefrPassesAdd;

            decimal actualSuiteTotal = Browser.stringToDecimalConvert(Pages.Payment.packageTotalCost);
            decimal ExpctdsuiteTotal = addedPassesTotal + suiteTotalBefrPassesAdd;
            return actualSuiteTotal.Equals(ExpctdsuiteTotal);
        }

        public Boolean verifyAddedCabanaPackagesSUiteTotal()
        {
            decimal addedCabanaTotal = indoorCabanaPriceafterConvert + outdoorCabanaPriceafterConvert;
            
            decimal actualSuiteTotal = Browser.stringToDecimalConvert(Pages.Payment.packageTotalCost);
            decimal ExpctdsuiteTotal = addedCabanaTotal + suiteTotalBefrPassesAdd;
            return actualSuiteTotal.Equals(ExpctdsuiteTotal);
        }

        
        public Boolean verifyCostSummaryTotal()
        {
            //decimal expectdSuitTotal = addedFrstPckgCostAtCostSummary + addedScndPckgCostAtCostSummary;
            decimal actualSuiteTotal = Browser.stringToDecimalConvert(Pages.Payment.packageTotalCost);
            decimal SuiteTotalCost = Browser.stringToDecimalConvert(Pages.Payment.SuiteTotalPrice);
            decimal TaxCost = Browser.stringToDecimalConvert(Pages.Payment.taxCostLable);
            decimal resortFeeCost;
            if (Pages.Payment.resortFeetCost.Exists())
            {
                resortFeeCost = Browser.stringToDecimalConvert(Pages.Payment.resortFeetCost);
            }
            else { resortFeeCost = 0; }

            decimal expectedTotal = actualSuiteTotal + SuiteTotalCost + TaxCost + resortFeeCost;
            decimal actualTotal = Browser.stringToDecimalConvert(Pages.Payment.summaryCostTotal);
            
            return expectedTotal.Equals(actualTotal);
        }

        public static decimal addition = 0;
        //public static decimal total;
        decimal pckageCost;
        public Boolean verifyAddedPckgTotal()
        {
            int countVal = Browser.Driver.FindElements(By.XPath("//div[@aria-label='Summary Item Package Amount']//div[text()='$']")).Count;
            
            String PcksgString = "(//div[@aria-label='Summary Item Package Amount']//div[text()='$'])[";
            String PcksgString1 = "]";
           
            for (int i = 1; i <= countVal; i++) {
                String cnt = i.ToString();
                IWebElement costSummaryPckgs = ResponsiveGrid.FindElement(By.XPath(PcksgString+ cnt + PcksgString1));
                pckageCost = Browser.stringToDecimalConvert(costSummaryPckgs);
                addition = addition + pckageCost;
            }            

            decimal actualSuiteTotal = Browser.stringToDecimalConvert(Pages.Payment.packageTotalCost);
            return actualSuiteTotal.Equals(addition);            
        }

        public Boolean verifyCostSummaryTotalOfCabanaRes()
        {
            decimal actualTotal = Browser.stringToDecimalConvert(Pages.Payment.summaryCostTotal);
            return indoorCabanaPriceafterConvert.Equals(actualTotal);
        }

        public Boolean verifyTotalandCheckInSame()
        {
            decimal dueAtCheckIn = Browser.stringToDecimalConvert(Pages.Payment.dueAtCheckInCost);            
            decimal actualTotal = Browser.stringToDecimalConvert(Pages.Payment.summaryCostTotal);
            return dueAtCheckIn.Equals(actualTotal);
        }

        public Boolean verifyReseNum()
        {
            Browser.waitForImplicitTime(10);
            IWebElement resNumber = ResponsiveGrid.FindElement(By.XPath(resId + NewConfirmationPage.afterSplitResNum + resId1));
            Browser.waitForElementToDisplayed(resNumber, 20);
            return resNumber.Exists();
        }


        public void clickOnViewDetails()
        {
            Browser.waitForImplicitTime(10);
            IWebElement viewOfferBtn = ResponsiveGrid.FindElement(By.XPath(viewOffer + NewConfirmationPage.afterSplitResNum + viewOffer1));
            viewOfferBtn.Click();

            Pages.rateCalendar.waitForRateCalSprinnerOut(ReserVDetailSpinner);            
        }

        public void clickOnAddAPckgBtn()
        {
            if (AddAPackageBtn.Exists() == false)
            {
                Browser.webDriver.Navigate().Refresh();
            }
            Pages.SearchYourReservation.waitForFindYourResrSprinnerOut(SearchYourReservationPage.waityouReservationSpinnerStr);
            //Browser.waitForElementToDisplayed(AddAPackageBtn, 40);
            Browser.scrollVerticalBy("500");
            Browser.waitForElementToBeClickable(AddAPackageBtn, 20);
            AddAPackageBtn.Click();
        }

        public void clickOnAddAPassBtn()
        {
            if (AddAPassBtn.Exists() == false)
            {
                Browser.webDriver.Navigate().Refresh();
            }
            Pages.SearchYourReservation.waitForFindYourResrSprinnerOut(SearchYourReservationPage.waityouReservationSpinnerStr);
            //Browser.waitForElementToDisplayed(AddAPassBtn, 40);
            Browser.scrollVerticalBy("500");
            Browser.waitForElementToBeClickable(AddAPassBtn, 20);
            AddAPassBtn.Click();
        }

        public void clickOnAddCabanaBtn()
        {
            if (AddCabanaBtn.Exists() == false)
            {
                Browser.webDriver.Navigate().Refresh();
            }
            Pages.SearchYourReservation.waitForFindYourResrSprinnerOut(SearchYourReservationPage.waityouReservationSpinnerStr);
            Browser.scrollVerticalBy("500");
            Browser.waitForElementToBeClickable(AddCabanaBtn, 20);
            AddCabanaBtn.Click();
        }

        public void clickOnAddCabanaBtnOnCofirmation()
        {
            //Browser.webDriver.Navigate().Refresh();
            Pages.SearchYourReservation.waitForFindYourResrSprinnerOut(SearchYourReservationPage.waityouReservationSpinnerStr);
            //Browser.waitForElementToDisplayed(AddCabanaBtn, 40);
            Browser.scrollVerticalBy("500");
            Browser.waitForElementToBeClickable(AddCabanaBtn, 20);
            AddCabanaBtn.Click();
        }

        public Boolean verifyValidOfferCode()
        {
            //Browser.waitForElementToDisplayed(OfferCodeCostSummary, 40);
            Browser.waitForElementToBeClickable(OfferCodeCostSummary, 20);
            return OfferCodeCostSummary.Exists();
        }

        public Boolean indoorCabanaTitlVerify()
        {
            if (indoorCabanaTitle.Exists())
            {
                return true;
            }
            else
            {
                //In some cases cabana not available so else part also be true.
                return true;
            }
        }

        public Boolean outdoorCabanaTitlVerify()
        {
            if (outdoorCabanaTitle.Exists())
            {
                return true;
            }
            else
            {
                //In some cases cabana not available so else part also be true.
                return true;
            }
        }

        public Boolean indoorCabanaPriceverify()
        {
            if (indoorCabanaTitle.Exists())
            {
                if (indoorCabanaNotAvailable.Exists() == false)
                {
                    return indoorCabanaPrice.Exists();
                }
                else
                {
                    return true;
                }
            }
            else {
                return true;
            }
        }

        public Boolean outdoorCabanaPriceverify()
        {
            if (outdoorCabanaTitle.Exists())
            {
                if (outdoorCabanaNotAvailable.Exists() == false)
                {
                    return outdoorCabanaPrice.Exists();
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public static Boolean cabanAvailableForAdd;

        public void indoorCabanaaddCabana()
        {
            if (Pages.Payment.packageTotalCost.Exists() == true)
            {
                suiteTotalBefrPassesAdd = Browser.stringToDecimalConvert(Pages.Payment.packageTotalCost);
            }
            if (indoorCabanaTitle.Exists())
            {
                if (indoorCabanaNotAvailable.Exists() == false)
                {
                    cabanAvailableForAdd = true;

                    Browser.waitForElementToDisplayed(indoorCabanaselectDatesDrp, 10);
                    indoorCabanaselectDatesDrp.Click();

                    Browser.waitForElementToDisplayed(indoorCabanadrpPrice, 10);
                    String InDoorCabanaDatePricewithotuSubstr = Browser.getText(indoorCabanadrpPrice);
                    InDoorCabanaDatePrice = InDoorCabanaDatePricewithotuSubstr.Substring(14);

                    Browser.waitForElementToBeClickable(indoorCabanacheckBox, 20);
                    indoorCabanacheckBox.Click();

                    Browser.waitForElementToBeClickable(indoorCabanaAddBtn, 20);
                    indoorCabanaAddBtn.Click();
                }                
            }            
        }

        public Boolean indoorCabanaAddedPriceVeri()
        {
            if (indoorCabanaTitle.Exists())
            {
                if (indoorCabanaNotAvailable.Exists() == false)
                {
                    IWebElement indoorCabanaPrice = ResponsiveGrid.FindElement(By.XPath(indoorCabanaAddedPrice + InDoorCabanaDatePrice + indoorCabanaAddedPrice1));
                    indoorCabanaPriceafterConvert = Browser.stringToDecimalConvert(indoorCabanaPrice);
                    return indoorCabanaPrice.Exists();                    
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public void outdoorCabanaaddCabana()
        {
            if (outdoorCabanaTitle.Exists())
            {
                if (outdoorCabanaNotAvailable.Exists() == false)
                {
                    Browser.waitForElementToBeClickable(outdoorCabanaselectDatesDrp, 20);
                    outdoorCabanaselectDatesDrp.Click();

                    Browser.waitForElementToDisplayed(outdoorCabanadrpPrice, 10);
                    //outDoorCabanaDatePrice = Browser.getText(outdoorCabanadrpPrice);
                    String outDoorCabanaDatePricewithotuSubstr = Browser.getText(outdoorCabanadrpPrice);
                    outDoorCabanaDatePrice = outDoorCabanaDatePricewithotuSubstr.Substring(14);

                    Browser.waitForElementToBeClickable(outdoorCabanacheckBox, 20);
                    outdoorCabanacheckBox.Click();

                    Browser.waitForElementToBeClickable(outdoorCabanaAddBtn, 20);
                    outdoorCabanaAddBtn.Click();
                }
            }
        }

        public Boolean outdoorCabanaAddedPriceVeri()
        {
            if (outdoorCabanaTitle.Exists())
            {
                if (outdoorCabanaNotAvailable.Exists() == false)
                {
                    IWebElement outdoorCabanaPrice = ResponsiveGrid.FindElement(By.XPath(outdoorCabanaAddedPrice + outDoorCabanaDatePrice + indoorCabanaAddedPrice1));
                    outdoorCabanaPriceafterConvert = Browser.stringToDecimalConvert(outdoorCabanaPrice);
                    return outdoorCabanaPrice.Exists();
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public void navigateOnMyReservation()
        {
            Pages.PropertyHome.closeDealsSignUp();
            //Browser.waitForElementToDisplayed(Pages.PropertyHome.userSignedInMenu, 90);
            Browser.waitForElementToBeClickable(Pages.PropertyHome.userSignedInMenu, 20);
            Pages.PropertyHome.userSignedInMenu.Click();

            //Browser.waitForElementToDisplayed(myReservationMenuLink, 40);
            Browser.waitForElementToBeClickable(myReservationMenuLink, 20);
            myReservationMenuLink.Click();
            Pages.rateCalendar.waitForMyResSprinnerOut(MyResSprinner);
        }
                

        public Boolean verifyMyUpcomingMsg()
        {
            Browser.waitForElementToDisplayed(upcomingResNotAvalblMsg, 15);
            return upcomingResNotAvalblMsg.Exists();
        }

        public Boolean verifyPastResMsg()
        {
            Browser.waitForElementToDisplayed(pastResNotAvalblMsg, 20);
            return pastResNotAvalblMsg.Exists();
        }

        public Boolean verifyCanclledResMsg()
        {
            Browser.waitForElementToDisplayed(cancelledResNotAvalblMsg, 10);
            return cancelledResNotAvalblMsg.Exists();
        }

        public Boolean verifyFindResLink()
        {
            Browser.waitForElementToDisplayed(FindYourResConfLink, 10);
            return FindYourResConfLink.Exists();
        }

        public Boolean verifyFindResLinkNotDisplayed()
        {
            if (FindYourResConfLink.Exists())
            {
                return false;
            }
            else {
                return true;
            }
        }

        public void clickOnFindResLink()
        {
            Browser.waitForElementToBeClickable(FindYourResConfLink, 20);
            FindYourResConfLink.Click();
        }

        public Boolean verifyMakeResLink()
        {
            Browser.waitForElementToDisplayed(makeAReservationLink, 20);
            return makeAReservationLink.Exists();
        }

        public void clickMakeResLink()
        {
            //Browser.waitForElementToDisplayed(makeAReservationLink, 40);
            Browser.waitForElementToBeClickable(makeAReservationLink, 32);
            makeAReservationLink.Click();
        }

        public void clickOnSavetoResBtn()
        {
            //Browser.waitForElementToDisplayed(saveToMyResBtn, 40);
            Browser.waitForElementToBeClickable(saveToMyResBtn, 20);
            saveToMyResBtn.Click();

            Pages.rateCalendar.waitForRateCalSprinnerOut(ReserVDetailSpinner);
        }

        public void clickOnSavetoResBtnIfAvlbl()
        {
                //Browser.waitForElementToDisplayed(saveToMyResBtn, 40);
                Browser.waitForElementToBeClickable(saveToMyResBtn, 20);
                saveToMyResBtn.Click();

                Pages.rateCalendar.waitForRateCalSprinnerOut(ReserVDetailSpinner);
            
            if (CabanaNotAvailableMSG.Exists() == true)
            {
                Browser.driver.Navigate().Back();
                Pages.rateCalendar.waitForRateCalSprinnerOut(ReserVDetailSpinner);
                
                if (Pages.NewConfirmation.allDoneBtn.Exists() == true) {
                    Pages.NewConfirmation.allDoneBtn.Click();
                    Pages.rateCalendar.waitForRateCalSprinnerOut(ReserVDetailSpinner);
                }
            }
        }

        public void indoorCabanaClickLink()
        {
            if (indoorCabanaViewLink.Exists())
            {
                //Browser.waitForElementToDisplayed(indoorCabanaViewLink, 40);
                Browser.waitForElementToBeClickable(indoorCabanaViewLink, 20);
                indoorCabanaViewLink.Click();
            }
        }

        public void indoorCabanaClickLinkNdVerifyResNum()
        {
            if (indoorCabanaViewLink.Exists())
            {
                Browser.waitForElementToBeClickable(indoorCabanaViewLink, 20);
                indoorCabanaViewLink.Click();

                Pages.NewConfirmation.reservationNumberDisplayed();
            }
        }

        public void indoorCabanaClickLinkIfAdded()
        {
            if (indoorCabanaViewLink.Exists())
            {
                //Browser.waitForElementToDisplayed(indoorCabanaViewLink, 40);
                Browser.waitForElementToBeClickable(indoorCabanaViewLink, 20);
                indoorCabanaViewLink.Click();

                Pages.NewConfirmation.clickCostSummaryBtn();

                Pages.MyReserv.verifyCostSummaryTotalOfCabanaRes();

                Pages.MyReserv.verifyTotalandCheckInSame();

                Pages.NewConfirmation.clickcloselink();
            }
        }

        public void outdoorCabanaClickLink()
        {
            if (outdoorCabanaTitle.Exists())
            {
                //Browser.waitForElementToDisplayed(outdoorCabanaViewLink, 40);
                Browser.waitForElementToBeClickable(outdoorCabanaViewLink, 20);
                outdoorCabanaViewLink.Click();
            }
        }

        public Boolean veriAddedPackageNotificationMsg()
        {
            //Browser.waitForElementToDisplayed(addePckagesNotifctionMsg, 40);
            Browser.waitForElementToBeClickable(addePckagesNotifctionMsg, 20);
            return addePckagesNotifctionMsg.Exists();
        }

        public Boolean checkLCOBtnAvailable()
        {
            if (SuitePage.LCOExist == true)
            {
                if (addLateCheckOutBtnOnCMP.Exists() == false)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else {
                if (addLateCheckOutBtnOnCMP.Exists() == false)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }            
        }

        public Boolean checkLCOBtnAvailableAsNotAddedBookingTime()
        {
            if (SuitePage.LCOExist == true)
            {
                if (addLateCheckOutBtnOnCMP.Exists() == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (addLateCheckOutBtnOnCMP.Exists() == false)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public Boolean check2PMLCOTxt()
        {
            if (SuitePage.LCOExist == true)
            {
                return TwoPMAddedLCOTxt.Exists();
            }
            else
            {
                return true;
            }
        }

        public Boolean checkLCOBtnAvailableOrNotWhenLCONA()
        {
            if (SuitePage.LCOExist == false)
            {
                if (addLateCheckOutBtnOnCMP.Exists() == false)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public Boolean addLCOClickLink()
        {

            if (SuitePage.LCOExist == true)
            {
                //Browser.waitForElementToDisplayed(addLateCheckOutBtnOnCMP, 40);
                Browser.waitForElementToBeClickable(addLateCheckOutBtnOnCMP, 20);
                addLateCheckOutBtnOnCMP.Click();

                //Browser.waitForElementToDisplayed(addToMyReservationBtn, 40);
                Browser.waitForElementToBeClickable(addToMyReservationBtn, 20);
                addToMyReservationBtn.Click();
                Thread.Sleep(5000);
                IWebElement LCOPrice = ResponsiveGrid.FindElement(By.XPath(LOCPriceATCost + SuitePage.PopupPrice + LOCPriceATCost1));
                //Browser.waitForElementToDisplayed(LCOPrice, 20);
                LCOCost = LCOPrice.Exists();
            }

            return LCOCost;
        }

        public Boolean verifyURlIsNavigatewithReservation()
        {
            Thread.Sleep(5000);
            String currentBrowseURL = Browser.driver.Url;
            String property = DataAccess.getData(15, 2);
            return currentBrowseURL.Contains(property);
        }

    }
}
