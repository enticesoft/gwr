﻿using GWRFramework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using GWRFramework.TestData;
using System.Threading;

namespace GWRFramework
{

    public class GlobalHomePage
    {

        IWebDriver driver;
        public String propertyName;

        [FindsBy(How = How.XPath, Using = "(//*[@id='choose-a-lodge-button']/span[contains(text(), 'Choose A Lodge')])[1]")]
        public IWebElement chooseLocationBtn;

        /*[FindsBy(How = How.XPath, Using = "(//*[@id='choose-a-lodge-button']/span[contains(text(), 'Choose A Lodge')])[1]|(//*[@id='choose-a-lodge-button']/span[contains(text(), 'Choose A Lodge')])[2]|(//*[@id='choose-a-lodge-button']/span[contains(text(), 'Choose A Lodge')])[3]|(//*[@id='choose-a-lodge-button']/span[contains(text(), 'Choose A Lodge')])[4]")]
        public IWebElement chooseLocationBtn;*/

        [FindsBy(How = How.Id, Using = "location-overlay")]
        public IWebElement locationOverlay;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Anaheim, CA']")]
        public IWebElement SoucthCalLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Scottsdale / Talking Stick, AZ']")]
        public IWebElement ScotDaleLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='San Francisco /  Manteca, CA']")]
        public IWebElement SanFransiscoLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Grand Mound, WA']")]
        public IWebElement GrandMountLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Colorado Springs, CO']")]
        public IWebElement ColoradoSpringLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Williamsburg, VA']")]
        public IWebElement WillBurgLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Charlotte / Concord, NC']")]
        public IWebElement ConcordLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Grapevine, TX']")]
        public IWebElement GrapevineLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Atlanta / LaGrange, GA']")]
        public IWebElement AtlantaLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Wisconsin Dells, WI']")]
        public IWebElement WisconsinDellLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Traverse City, MI']")]
        public IWebElement TravercityLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Sandusky, OH']")]
        public IWebElement SanduskyLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Kansas City, KS']")]
        public IWebElement KansasLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Chicago / Gurnee, IL']")]
        public IWebElement GurneeLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Cincinnati / Mason, OH']")]
        public IWebElement MasonLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Minneapolis / Bloomington, MN']")]
        public IWebElement MiniSottaLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Pocono Mountains, PA']")]
        public IWebElement PoconosLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Boston / Fitchburg, MA']")]
        public IWebElement BostonLocation;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'menu locationSelector submenu is-dropdown-submenu first-sub vertical js-dropdown-active')]//a[text()='Niagara Falls, Ontario']")]
        public IWebElement NigaraLocation;

        [FindsBy(How = How.XPath, Using = "//li[@class='is-dropdown-submenu-parent opens-right']//a")]
        public IWebElement LocationDropdown;
        

        String property1 = "//div[@class='ghp-location-selector']/descendant::li/a[contains(text(), '";
        String property2 = "')]";

        public Boolean westSouthCalLocation()
        {
            Browser.waitForElementToBeClickable(SoucthCalLocation, 20);
            return SoucthCalLocation.Exists();            
        }

        public Boolean westScotDaleLocation()
        {
            Browser.waitForElementToBeClickable(ScotDaleLocation, 20);
            return ScotDaleLocation.Exists();
        }

        public Boolean westSanFransiscoLocation()
        {
            Browser.waitForElementToBeClickable(SanFransiscoLocation, 20);
            return SanFransiscoLocation.Exists();
        }

        public Boolean westGrandMountLocation()
        {
            Browser.waitForElementToBeClickable(GrandMountLocation, 20);
            return GrandMountLocation.Exists();
        }

        public Boolean westColoradoSpringLocation()
        {
            Browser.waitForElementToBeClickable(ColoradoSpringLocation, 20);
            return ColoradoSpringLocation.Exists();
        }

        public Boolean southWillBurgLocation()
        {
            Browser.waitForElementToBeClickable(WillBurgLocation, 20);
            return WillBurgLocation.Exists();
        }

        public Boolean southConcordLocation()
        {
            Browser.waitForElementToBeClickable(ConcordLocation, 20);
            return ConcordLocation.Exists();
        }

        public Boolean southGrapevineLocation()
        {
            Browser.waitForElementToBeClickable(GrapevineLocation, 20);
            return GrapevineLocation.Exists();
        }

        public Boolean southAtlantaLocation()
        {
            Browser.waitForElementToBeClickable(AtlantaLocation, 20);
            return AtlantaLocation.Exists();
        }

        public Boolean midwestWisconsinDellLocation()
        {
            Browser.waitForElementToBeClickable(WisconsinDellLocation, 20);
            return WisconsinDellLocation.Exists();
        }

        public Boolean midwestTravercityLocation()
        {
            Browser.waitForElementToBeClickable(TravercityLocation, 20);
            return TravercityLocation.Exists();
        }

        public Boolean midwestSanduskyLocation()
        {
            Browser.waitForElementToBeClickable(SanduskyLocation, 20);
            return SanduskyLocation.Exists();
        }

        public Boolean midwestKansasLocation()
        {
            Browser.waitForElementToBeClickable(KansasLocation, 20);
            return KansasLocation.Exists();
        }

        public Boolean midwestGurneeLocation()
        {
            Browser.waitForElementToBeClickable(GurneeLocation, 20);
            return GurneeLocation.Exists();
        }

        public Boolean midwestMasonLocation()
        {
            Browser.waitForElementToBeClickable(MasonLocation, 20);
            return MasonLocation.Exists();
        }

        public Boolean midwestMiniSottaLocation()
        {
            Browser.waitForElementToBeClickable(MiniSottaLocation, 20);
            return MiniSottaLocation.Exists();
        }

        public Boolean northestPoconosLocation()
        {
            Browser.waitForElementToBeClickable(PoconosLocation, 20);
            return PoconosLocation.Exists();
        }

        public Boolean northestBostonLocation()
        {
            Browser.waitForElementToBeClickable(BostonLocation, 20);
            return BostonLocation.Exists();
        }

        public Boolean CandaNigaraLocation()
        {
            Browser.waitForElementToBeClickable(NigaraLocation, 20);
            return NigaraLocation.Exists();
        }

        public void clickChooseLocationBtn()
        {
            Browser.waitForElementToBeClickable(chooseLocationBtn, 20);
            chooseLocationBtn.Click();
        }

        public Boolean checkLocationList()
        {
            Browser.waitForElementToDisplayed(locationOverlay, 20);
            return locationOverlay.Exists();
        }

        public void selectProperty()
        {
            propertyName = DataAccess.getData(15, 2);
            Browser.NavigateToProperty(propertyName);

            /* IWebElement propertyLink = locationOverlay.FindElement(By.XPath(property1+ propertyName + property2));

             Browser.waitForElementToBeClickable(propertyLink, 5);
             propertyLink.Click();*/
        }

        public void NavigateOnPlanProperty()
        {
            propertyName = DataAccess.getData(15, 2);
            Browser.NavigateToPropertyPlan(propertyName);
        }

        public void selectPropertyandNavigateOnGurnee()
        {
            Browser.NavigateToProperty("illinois");
        }

        public void clickOnLocationdrpDown()
        {
            Browser.waitForElementToBeClickable(LocationDropdown, 20);
            LocationDropdown.Click();
        }
    }
}
