﻿using GWRFramework.TestData;
using Microsoft.Office.Interop.Excel;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GWRFramework
{
    public class ActivitiesPage
    {
        public static string PackageQty;
        public static double PackagesAdditionprice;
        public static decimal PackagesAdditionprice1;
        public static double totalPrice;
        public static decimal totalPrice1;
        public static double suiteTotal;
        public static decimal suiteTotal1;
        public static double promotionalSavingVal;
        public static decimal promotionalSavingVal1;
        public static double yourSavingPrice;
        public static int PackageQtyLastValue;
        public static double packagefinalprice;
        public static double packageTotalPrice;
        public static decimal packageTotalPrice1;
        public static double packagePrice;
        public static decimal packagePrice1;
        public static double packageQtyValue;
        public static double suiteBasePrice;
        public static decimal suiteBasePrice1;
        public static string AdultsCountUnderCostSummary;
        public static string KidsCountUnderCostSummary;
        public static double LCOPriceCostSummary;
        public static string CostSummaryCheckIntDate;
        public static string CostSummaryCheckOutDate;

        [FindsBy(How = How.XPath, Using = "//*[text()='Attractions']")]
        public IWebElement attractionsTab;
        
        [FindsBy(How = How.XPath, Using = "//*[text()='Family']")]
        public IWebElement familyTab;

        [FindsBy(How = How.XPath, Using = "(//div[@class='accordion__item'])[1]")]
        public IWebElement familyViewDetailsLink;

        [FindsBy(How = How.XPath, Using = "(//div[@class='accordion__item'])[1]//div[@aria-expanded='true']")]
        public IWebElement familyViewDetailsLinkExpanded;
        

        [FindsBy(How = How.XPath, Using = "//*[text()='Birthdays']|//*[text()='Birthday']")]
        public IWebElement birthdayTab;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Progress Bar']//li[1]//a")]
        public IWebElement suitesIcon;

        [FindsBy(How = How.XPath, Using = "(//section//button[text()='ADD'])[1]")]
        public IWebElement packageAddBtn;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Packages']/following-sibling::div[1]")]
        public IWebElement packageUnderCostSummary;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Packages']/following-sibling::div[1]//button[text()='REMOVE']")]
        public IWebElement packageRemoveLink;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Promotional Savings']//*[contains(text(),'$')]")]
        public IWebElement promotionalSaving;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Your Savings:')]/following-sibling::div")]
        public IWebElement yourSavings;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Packages']/following-sibling::div[contains(.,'Late Checkout')]")]
        public IWebElement LOCPackage;

        [FindsBy(How = How.XPath, Using = "(//section//select)[1]")]
        public IWebElement packagequantityDpDn;

        [FindsBy(How = How.XPath, Using = "//button[text()='UPDATE']/parent::div//preceding-sibling::div[contains(text(),'$')]")]
        public IWebElement Attractionpackageprice;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Packages']/following-sibling::div[1]//div[contains(text(),'$')]")]
        public IWebElement packagepriceunderCostSummary;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Packages']/following-sibling::div[1]//select")]
        public IWebElement packageDpDnSection;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Packages']/following-sibling::div[1]//div[contains(text(),'$')]")]
        public IWebElement package1priceunderCostSummary;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Packages']/following-sibling::div[2]//div[contains(text(),'$')]")]
        public IWebElement package2priceunderCostSummary;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Package Total']//*[contains(text(),'$')]")]
        public IWebElement packageTotal;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Suite Total']//*[contains(text(),'$')]")]
        public IWebElement suiteTotalPrice;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Total Price']//*[contains(text(),'$')]")]
        public IWebElement TotalPrice;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Party Size']//*[contains(text(),'$')]")]
        public IWebElement suiteBaseValue;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Party Size']//*[contains(text(),'Adults')]")]
        public IWebElement adultsUnderCostSummary;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Party Size']//*[contains(text(),'Adults')]")]
        public IWebElement kidsUnderCostSummary;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Packages']/following-sibling::div//*[contains(text(),'2 PM Themed')]")]
        public IWebElement themed2pmLCOunderCostSummary;

        [FindsBy(How = How.XPath, Using = "//*[@aria-label='Summary Item Packages']/following-sibling::div//*[contains(text(),'2 PM')]")]
        public IWebElement standard2pmLCOunderCostSummary;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Late Checkout')]/parent::div/parent::div/following-sibling::div//*[contains(text(),'$')]")]
        public IWebElement LCOPriceunderCostSummary;

        //Rohit -
        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Check-In and Check-Out dates']//following-sibling::div//div[contains(.,'Offer Code Applied: ')]")]
        public IWebElement costSummaryOffcd;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Selected Suite Title']")]
        public IWebElement SuiteTitleCost;

        [FindsBy(How = How.XPath, Using = "//span[text()='Pup Pass']")]
        public IWebElement PupPassPckgTitle;

        [FindsBy(How = How.XPath, Using = "//span[text()='Paw Pass']")]
        public IWebElement PawPassPckgTitle;

        [FindsBy(How = How.XPath, Using = "//span[text()='Wolf Pass']")]
        public IWebElement WolfPassPckgTitle;

    /*  [FindsBy(How = How.XPath, Using = "//div[text()='Pup Pass']//following-sibling::div[contains(.,'$')]")]
        public IWebElement PupPassPckgCost;

        [FindsBy(How = How.XPath, Using = "//div[text()='Paw Pass']//following-sibling::div[contains(.,'$')]")]
        public IWebElement PawPassPckgCost;

        [FindsBy(How = How.XPath, Using = "//div[text()='Wolf Pass']//following-sibling::div[contains(.,'$')]")]
        public IWebElement WolfPassPckgCost;*/

      /*  [FindsBy(How = How.XPath, Using = "//div[text()='Perfect for Toddlers']//following-sibling::div[contains(.,'$')]")]
        public IWebElement PupPassPckgCost;

        [FindsBy(How = How.XPath, Using = "//div[text()='Great Deal for Ages 6+']//following-sibling::div[contains(.,'$')]")]
        public IWebElement PawPassPckgCost;

        [FindsBy(How = How.XPath, Using = "//div[text()='Most Fun On One Pass']//following-sibling::div[contains(.,'$')]")]
        public IWebElement WolfPassPckgCost;*/

        [FindsBy(How = How.XPath, Using = "//div[text()='Perfect for Toddlers']//following-sibling::div[contains(.,'$')] | //div[text()='Pup Pass']//following-sibling::div[contains(.,'$')]")]
        public IWebElement PupPassPckgCost;

        [FindsBy(How = How.XPath, Using = "//div[text()='Great Deal for Ages 6+']//following-sibling::div[contains(.,'$')] | //div[text()='Paw Pass']//following-sibling::div[contains(.,'$')]")]
        public IWebElement PawPassPckgCost;

        [FindsBy(How = How.XPath, Using = "//div[text()='Most Fun On One Pass']//following-sibling::div[contains(.,'$')] | //div[text()='Wolf Pass']//following-sibling::div[contains(.,'$')]")]
        public IWebElement WolfPassPckgCost;

        String suiteTitle = "(//div[@aria-label='Summary Item Selected Suite Title']//div[contains(.,'";
        String suiteTitle1 = " (";
        String suiteTitle2 = "')])[1]";

        [FindsBy(How = How.XPath, Using = "((//div[@aria-label='Summary Item Packages']//following-sibling::div)[3]//div)[1]")]
        public IWebElement LCOCost;

        String LCOPrice = "((//div[@aria-label='Summary Item Packages']//following-sibling::div)[3]//div[contains(.,'";
        String LCOPrice1 = "')])[3]";

        //=================================================
        [FindsBy(How = How.XPath, Using = "(((//div[@id='root']//button[text()='ADD'])[1]//ancestor::div[4]//child::div[2])[1]//div)[1]")]
        public IWebElement fisrtBirthdayPckgTitle;

        [FindsBy(How = How.XPath, Using = "(((//div[@id='root']//button[text()='ADD'])[2]//ancestor::div[4]//child::div[2])[1]//div)[1]")]
        public IWebElement secondBirthdayPckgTitle;

        [FindsBy(How = How.XPath, Using = "(((//div[@id='root']//button[text()='ADD'])[3]//ancestor::div[4]//child::div[2])[1]//div)[1]")]
        public IWebElement thirdBirthdayPckgTitle;

        [FindsBy(How = How.XPath, Using = "((//div[@id='root']//button[text()='ADD'])[1]/../../..//div[contains(.,'$')])[2]")]
        public IWebElement firstBirthPackageCost;

        [FindsBy(How = How.XPath, Using = "((//div[@id='root']//button[text()='ADD'])[2]/../../..//div[contains(.,'$')])[2]")]
        public IWebElement secondBirthPackageCost;

        [FindsBy(How = How.XPath, Using = "((//div[@id='root']//button[text()='ADD'])[3]//ancestor::div[3]//div[contains(.,'$')])[2]")]
        public IWebElement thirdBirthPackageCost;

        public static string BirthdayFirstPckgTitl;
        public static string BirthdaySecondPckgTitl;
        public static string BirthdayThirdPckgTitl;

        public static string BirthdayFirstPckgTitlCost;
        public static string BirthdaySecondPckgTitlCost;
        public static string BirthdayThirdPckgTitlCost;

        public static Boolean BirthdayFirstPckgAvl;
        public static Boolean BirthdayScndPckgAvl;
        public static Boolean BirthdayThirdPckgAvl;

        public void getBirthdayPckgsTitleAndCost()
        {
            Thread.Sleep(2000);
            if (fisrtBirthdayPckgTitle.Exists())
            {
                BirthdayFirstPckgAvl = true;
                BirthdayFirstPckgTitl = Browser.getText(fisrtBirthdayPckgTitle);
                BirthdayFirstPckgTitlCost = Browser.getText(firstBirthPackageCost);
            }

            if (secondBirthdayPckgTitle.Exists())
            {
                BirthdayScndPckgAvl = true;
                BirthdaySecondPckgTitl = Browser.getText(secondBirthdayPckgTitle);
                BirthdaySecondPckgTitlCost = Browser.getText(secondBirthPackageCost);
            }

            if (thirdBirthdayPckgTitle.Exists())
            {
                BirthdayThirdPckgAvl = true;
                BirthdayThirdPckgTitl = Browser.getText(thirdBirthdayPckgTitle);
                BirthdayThirdPckgTitlCost = Browser.getText(thirdBirthPackageCost);
            }
        }



        public void navigateSuiteToActivities()
        {
           
            Browser.waitForElementToBeClickable(Pages.Suite.selectBtn, 20);
            if (Pages.Suite.qualifyingIDTx.Exists())
            {
                Pages.Suite.qualifyingIDTx.SendKeys("123");
            }

            Browser.javaScriptClick(Pages.Suite.selectBtn);
            //Pages.Suite.selectBtn.Click();
            //Thread.Sleep(3000);
            Browser.waitForImplicitTime(5);
            if (Pages.Suite.lcoPopup.Exists())
            {
                //Browser.waitForElementToDisplayed(Pages.Suite.LCONoThanksBtn, 30);
                Browser.waitForElementToBeClickable(Pages.Suite.LCONoThanksBtn, 20);
                Pages.Suite.LCONoThanksBtn.Click();
            }
        }

        public void navigateToAttractionsTab()
        {
            //Browser.waitForElementToDisplayed(attractionsTab, 45);
            Browser.waitForElementToBeClickable(attractionsTab, 20);
            attractionsTab.Click();
        }

        public void navigateToFamilyTab()
        {
            //Browser.waitForElementToDisplayed(familyTab, 45);
            Browser.waitForElementToBeClickable(familyTab, 20);
            familyTab.Click();
        }

        public void clickOnFamilyViewDetailsLink()
        {
            //Browser.waitForElementToDisplayed(familyTab, 45);
            Browser.waitForElementToBeClickable(familyViewDetailsLink, 20);
            familyViewDetailsLink.Click();
        }

        public Boolean verifyExpandedLink()
        {
            //Browser.waitForElementToDisplayed(familyTab, 45);
            Browser.waitForElementToBeClickable(familyViewDetailsLinkExpanded, 5);
            if (familyViewDetailsLinkExpanded.Exists())
            {
                return true;
            }
            else {
                return false;
            }            
        }
        
        public void navigateToBirthdayTab()
        {
            //Browser.waitForElementToDisplayed(birthdayTab, 45);
            Browser.waitForElementToBeClickable(birthdayTab, 20);
            birthdayTab.Click();
        }

        public void clickOnPackageAddButton()
        {
            Browser.waitForElementToBeClickable(packageAddBtn, 15);
            packageAddBtn.Click();
        }

        public Boolean addedPackageUnderCostSummary()
        {
            Browser.waitForElementToDisplayed(packageUnderCostSummary, 15);
            bool PackageUnderCostSummary = packageUnderCostSummary.Displayed;
            return PackageUnderCostSummary;
        }
        public void clickOnPackageRemoveLink()
        {
            Browser.waitForElementToBeClickable(packageRemoveLink, 15);
            packageRemoveLink.Click();
        }

        public Boolean removePackageUnderCostSummary()
        {
            if (packageUnderCostSummary.Exists())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void clickOnSuitesIcon()
        {
            Browser.waitForElementToBeClickable(suitesIcon, 15);
            suitesIcon.Click();
        }

        public Boolean iAmOnSuitePage()
        {
            Browser.waitForElementToDisplayed(Pages.Suite.suiteRoom, 15);
            bool SuiteRoom = Pages.Suite.suiteRoom.Displayed;
            return SuiteRoom;
        }

        public void clickOnContinueToDiningPackagesButton()
        {
            Browser.waitForElementToBeClickable(Pages.Suite.continueToDiningPageBtn, 15);
            Pages.Suite.continueToDiningPageBtn.Click();
        }
        public Boolean iAmOnDiningPage()
        {
            Browser.waitForElementToDisplayed(Pages.Suite.continueToPaymentPageBtn, 15);
            bool ContinueToPayment = Pages.Suite.continueToPaymentPageBtn.Displayed;
            return ContinueToPayment;
        }

        public Boolean observe2PMLOCPackageUnderCostSummary()
        {
            Browser.waitForElementToDisplayed(LOCPackage, 15);
            if (LOCPackage.Exists())
            {
                bool LOC = LOCPackage.Displayed;
                return LOC;
            }
            else {
                return true;
            }
        }

        public void getActivitiesPackageLastDropDownValue()
        {
            Browser.waitForElementToDisplayed(packagequantityDpDn, 5);
            SelectElement PackageQuantityDropDown = new SelectElement(packagequantityDpDn);
            IList<IWebElement> allOptions = PackageQuantityDropDown.Options;
            int size = allOptions.Count;
            IWebElement lastvalue = allOptions[allOptions.Count - 1];
            String lastValue = lastvalue.Text;
            PackageQtyLastValue = Int32.Parse(lastValue);

        }

        public void SelectMultipleQuantityPackage(IWebElement element)
        {
            Browser.waitForElementToDisplayed(element, 5);
            SelectElement PackageQuantityDropDown = new SelectElement(element);
            IList<IWebElement> allOptions = PackageQuantityDropDown.Options;
            IWebElement lastvalue = allOptions[allOptions.Count - 1];
            PackageQty = lastvalue.Text;
            PackageQuantityDropDown.SelectByValue(PackageQty);
        }

        public Boolean addedPackageDpDnUnderCostSummary()
        {
            Browser.waitForElementToDisplayed(packageDpDnSection, 15);
            bool PackageDnDnUnderCostSummary = packageDpDnSection.Displayed;
            return PackageDnDnUnderCostSummary;
        }

        public void getPackageTotalPriceWithQty()
        {
            int packageQty = Int32.Parse(PackageQty);
            double Packageqty = Convert.ToDouble(packageQty);
            double PackagePrice = Browser.stringToDoubleConvert(Pages.Activities.Attractionpackageprice);
            double PackageFinalprice = packageQty * PackagePrice;
            packagefinalprice = Math.Round((Double)PackageFinalprice, 2);
        }

        public void packagesaddition(IWebElement element1, IWebElement element2)
        {
            //double Package1Price = Browser.stringToDoubleConvert(element1);
            //decimal Package1Price = Convert.ToDecimal(element1);
            decimal Package1Price = Browser.stringToDecimalConvert(element1);
            //double Package2Price = Browser.stringToDoubleConvert(element2);
            //decimal Package2Price = Convert.ToDecimal(element2);
            decimal Package2Price = Browser.stringToDecimalConvert(element2);
            PackagesAdditionprice1 = Package1Price + Package2Price;
        }

        public void getPackageTotalPrice()
        {
            Browser.waitForElementToDisplayed(packageTotal, 20);
            //packageTotalPrice = Browser.stringToDoubleConvert(packageTotal);
            packageTotalPrice1 = Browser.stringToDecimalConvert(packageTotal);
            //packageTotalPrice1 = Convert.ToDecimal(packageTotal);
        }

        public void getSuiteTotal()
        {
            Thread.Sleep(6000);
            Browser.waitForElementToBeClickable(suiteTotalPrice, 20);
            //suiteTotal = Browser.stringToDoubleConvert(suiteTotalPrice);
            suiteTotal1 = Browser.stringToDecimalConvert(suiteTotalPrice);
        }

        public void getTotalPrice()
        {
            Browser.waitForElementToDisplayed(TotalPrice, 20);
            //totalPrice = Browser.stringToDoubleConvert(TotalPrice);
            totalPrice1 = Browser.stringToDecimalConvert(TotalPrice);
        }

        public void getSuiteBasePrice()
        {
            Browser.waitForElementToDisplayed(suiteBaseValue, 20);
            suiteBasePrice = Browser.stringToDoubleConvert(suiteBaseValue);
        }

        public void getSuiteBasePrice1()
        {
            Browser.waitForElementToDisplayed(suiteBaseValue, 20);
            suiteBasePrice1 = Browser.stringToDecimalConvert(suiteBaseValue);
        }

        public void getAddedPackagePrice()
        {
            Browser.waitForElementToDisplayed(packagepriceunderCostSummary, 20);
            //packagePrice = Browser.stringToDoubleConvert(packagepriceunderCostSummary);
            packagePrice1 = Browser.stringToDecimalConvert(packagepriceunderCostSummary);
        }

        public void getPromotionalSavingPrice()
        {
            Browser.waitForElementToDisplayed(promotionalSaving, 20);
            promotionalSavingVal = Browser.stringToDoubleConvert1(promotionalSaving, 3);
            //promotionalSavingVal1 = Browser.stringToDecimalConvert(promotionalSaving);
        }

        public void getPromotionalSavingPrice1()
        {
            Browser.waitForElementToDisplayed(promotionalSaving, 20);
            promotionalSavingVal1 = Browser.stringToDecimalConvert1(promotionalSaving, 3);
        }
        public void getYourSavingPrice()
        {
            Browser.waitForElementToDisplayed(yourSavings, 20);
            yourSavingPrice = Browser.stringToDoubleConvert(yourSavings);
        }

        public void getPackageSelectedQtyValue(IWebElement element)
        {
            string value = Browser.getTextboxValue(element);
            packageQtyValue = Convert.ToDouble(value);
        }

        public Boolean verifyUpdatedPackageQty()
        {
            if (packageQtyValue != 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void getAdultsCountunderCostSummary()
        {
            Browser.waitForElementToDisplayed(adultsUnderCostSummary, 20);
            String AdultsCount = Browser.getText(adultsUnderCostSummary);
            AdultsCount = AdultsCount.Substring(0, 1);
            AdultsCountUnderCostSummary = AdultsCount;
        }
        public void getKidsCountunderCostSummary()
        {
            Browser.waitForElementToDisplayed(kidsUnderCostSummary, 20);
            String KidsCount = Browser.getText(kidsUnderCostSummary);
            KidsCount = KidsCount.Substring(10, 1);
            KidsCountUnderCostSummary = KidsCount;
        }
        public Boolean verifylatest2pmLCOUnderCOstSummary()
        {
            if (SuitePage.LCOExist == true)
            {
                Browser.waitForElementToDisplayed(standard2pmLCOunderCostSummary, 20);
                bool Standard2pmLCOunderCostSummary = standard2pmLCOunderCostSummary.Displayed;
                return Standard2pmLCOunderCostSummary;
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyOld2pmLCOUnderCOstSummary()
        {
            if (SuitePage.LCOExist == true)
            {
                if (themed2pmLCOunderCostSummary.Exists())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else {
                return true;
            }
        }

        public void get2pmLCOpriceunderCostSummary()
        {
            if (SuitePage.LCOExist == true)
            {
                Browser.waitForElementToDisplayed(LCOPriceunderCostSummary, 20);
                LCOPriceCostSummary = Browser.stringToDoubleConvert(LCOPriceunderCostSummary);
            }           

        }

        public void getCheckInDateUnderCostSummary()
        {
            Browser.waitForElementToDisplayed(Pages.Dining.costSumyCheckInDt, 20);
            CostSummaryCheckIntDate = Browser.getText(Pages.Dining.costSumyCheckInDt);
        }
        public void getCheckOutDateUnderCostSummary()
        {
            Browser.waitForElementToDisplayed(Pages.Dining.costSumyCheckOutDt, 20);
            CostSummaryCheckOutDate = Browser.getText(Pages.Dining.costSumyCheckOutDt);
        }

        public Boolean verifyOffercodeOnCostSumry(String offerCode)
        {
            Browser.waitForElementToDisplayed(costSummaryOffcd, 20);
            String actvityCostOffrCode = costSummaryOffcd.Text;
            String expctdOffercode = "Offer Code Applied: " + offerCode;
            return actvityCostOffrCode.Equals(expctdOffercode);
        }

        public Boolean verifyOffercodeNotOnCostSumry()
        {
            //Thread.Sleep(5000);
            Browser.waitForImplicitTime(5);
            if (costSummaryOffcd.Exists())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public Boolean verifSuiteTitleOnActivity(String SuiteOption)
        {
            //+ RateCalendar.SuiteTitle 
            //Thread.Sleep(5000);
            Browser.waitForImplicitTime(5);
            String title = suiteTitle + suiteTitle1 + SuitePage.SuiteOptionVal + suiteTitle2;
            IWebElement ActivitySuiteTitle = SuiteTitleCost.FindElement(By.XPath(title));
            return ActivitySuiteTitle.Displayed;
        }

        public void clickONContnueToDiningPckgBtn()
        {
            Browser.waitForElementToBeClickable(Pages.Suite.continueToDiningPageBtn, 20);
            Pages.Suite.continueToDiningPageBtn.Click();
        }

        public Boolean shouldSeeContnueToDiningPckgBtn()
        {
            Browser.waitForElementToDisplayed(Pages.Suite.continueToDiningPageBtn, 20);
            return Pages.Suite.continueToDiningPageBtn.Displayed;
        }

        public Boolean verifyLCOCostInSummary()
        {
            //Thread.Sleep(6000);
            if (SuitePage.LCOExist == true)
            {
                Browser.waitForImplicitTime(15);
                String LCOCostLocStr = LCOPrice + SuitePage.PopupPrice + LCOPrice1;
                IWebElement LCOCostSummry = LCOCost.FindElement(By.XPath(LCOCostLocStr));

                if (LCOCostSummry.Exists())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else {
                return true;
            }

        }


    }
}
