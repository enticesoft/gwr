﻿using GWRFramework.TestData;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GWRFramework
{
    public class NewConfirmationPage
    {
        [FindsBy(How = How.XPath, Using = "//div[@role='dialog']//button[text()='All Done']")]
        public IWebElement allDoneBtn;

        [FindsBy(How = How.XPath, Using = "//h1[text()='Welcome to your reservation!']")]
        public IWebElement CMPPageWelcomeTitle;

        /*[FindsBy(How = How.XPath, Using = "//section[@id='info-section']//p[text()='Reservation']")]
        public IWebElement reservationNumber;*/

        [FindsBy(How = How.XPath, Using = "//section[@id='info-section']//p[text()='Reservation #']")]
        public IWebElement reservationNumber;

        [FindsBy(How = How.XPath, Using = "//a[@type='button'][text()='ADD CABANAS']")]
        public IWebElement cabanasBtn;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Rent a cabana for your day of relaxation in the water park.']")]
        public IWebElement cabanasHeadLineTitle;

        [FindsBy(How = How.XPath, Using = "//section[@id='info-section']//button[text()='Cost Summary']")]
        public IWebElement costSummaryBtn;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Suite Details']//div[text()='$']")]
        public IWebElement CMPSuiteDetailsPriceLable;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Package Total']//div[text()='$ ']")]
        public IWebElement CMPPckageTotal;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Taxes']//div[text()='$']")]
        public IWebElement CMPTaxPriceLable;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Day Passes']//div[text()='$']")]
        public IWebElement dayPassCost;
        

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Resort Fee']//div[text()='$']")]
        public IWebElement CMPResortFeeLable;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Total']//div[text()='$']")]
        public IWebElement CMPSuiteTotalLable;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Amount Paid or Deposit Paid']//div[contains(text(), '$')]")]
        public IWebElement CMPDepositePaidLable;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Amount Due or Due at check-in']//div[contains(text(), '$')]")]
        public IWebElement CMPDueATCheckInLable;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Summary Item Offer Code']//div[contains(text(), 'AMTCERT')]")]
        public IWebElement offerCodeOnCMP;

        [FindsBy(How = How.XPath, Using = "//button[@title='Close']")]
        public IWebElement costSummaryClose;

        [FindsBy(How = How.XPath, Using = "//div[@class='ReactModal__Content ReactModal__Content--after-open Modal']//button[@type='button'][text()='✕']")]
        public IWebElement ModalClose;
        

        [FindsBy(How = How.XPath, Using = "//button[@type='button'][text()='Add Late Check-out']")]
        public IWebElement addLCOCheckOutBtn;

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'ADD PACKAGE')]")]
        public IWebElement addAPackagebtn;


        public static String paymentSpinner = "//*[@role='dialog']";

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'receiving SMS updates')]")]
        public IWebElement SMSsubscribeMsg;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Keep me posted on any updates in my reservation')]")]
        public IWebElement SMSunsubcribeMsg;

        [FindsBy(How = How.XPath, Using = "//input[@id='phoneNumber']")]
        public IWebElement SMSPhoneNum;

        [FindsBy(How = How.XPath, Using = "//div//button[contains(text(),'See More Details')]")]
        public IWebElement SeeMoreDetailsBtn;

        [FindsBy(How = How.XPath, Using = "//input[@type='checkbox']//parent::div")]
        public IWebElement SMSCheckbox;

        [FindsBy(How = How.XPath, Using = "//div//button[@type='submit']")]
        public IWebElement SmsSignUpBtn;

        [FindsBy(How = How.XPath, Using = "//div[text()='Your Add-Ons']//parent::div//div[text()='Dining Package']")]
        public IWebElement DiningPackageTileTitle;

        [FindsBy(How = How.XPath, Using = "//div[text()='Your Add-Ons']//parent::div//div[text()='Cabanas Rental']")]
        public IWebElement CabanaTileTitle;        

        [FindsBy(How = How.XPath, Using = "//div[text()='Your Add-Ons']//parent::div//div[text()='Attraction Passes']")]
        public IWebElement AttractionPassTileTitle;

        [FindsBy(How = How.XPath, Using = "//div[text()='Your Add-Ons']//parent::div//div[text()='Birthday Package']")]
        public IWebElement BirthdayTileTitle;
        

        [FindsBy(How = How.XPath, Using = "//div[text()='Your Add-Ons']//parent::div//div[text()='2 Dining Packages']")]
        public IWebElement DiningPackagesCount;

        [FindsBy(How = How.XPath, Using = "//button[text()='Add more Passes']|//button[text()='Add Passes']")]
        public IWebElement AddMorePassesBtn;

        [FindsBy(How = How.XPath, Using = "(//button[text()='ADD'])[1]")]
        public IWebElement AddBtnFirst;

        [FindsBy(How = How.XPath, Using = "(//button[text()='ADD'])[1]//preceding-sibling::div//div[contains(.,'$')]//h2")]
        public IWebElement FirstPassCost;

        [FindsBy(How = How.XPath, Using = "//div[text()='Dining Packages']//following-sibling::button[text()='Add a Package']")]
        public IWebElement AddAPckBtnDining;

        [FindsBy(How = How.XPath, Using = "//div[text()='Birthday Packages']//following-sibling::a[text()='Add a Package']")]
        public IWebElement AddAPckBtnBirthDay;

        [FindsBy(How = How.XPath, Using = "//div[text()='Cool Cabana Rentals']//following-sibling::a[text()='Add a Cabana']")]
        public IWebElement AddACabanBtn;

        [FindsBy(How = How.XPath, Using = "(//button[text()='ADD'])[1]")]
        public IWebElement firstAddBtnBithPckg;

        [FindsBy(How = How.XPath, Using = "(//button[text()='ADD'])[2]")]
        public IWebElement scndAddBtnBithPckg;

        [FindsBy(How = How.XPath, Using = "((//h2)[1]//following-sibling::div//div[contains(.,'$')])[2]")]
        public IWebElement firstBirthPckCOst;

        [FindsBy(How = How.XPath, Using = "((//h2)[2]//following-sibling::div//div[contains(.,'$')])[7]")]
        public IWebElement scndBirthPckCOst;

        [FindsBy(How = How.XPath, Using = "//div[@id='additional-package-section']/div[2]/div[1]")]
        public IWebElement AttractionTile;

        [FindsBy(How = How.XPath, Using = "//div[@id='additional-package-section']/div[2]/div[2]")]
        public IWebElement BirthdayPackagesTile;

        [FindsBy(How = How.XPath, Using = "//div[@id='additional-package-section']/div[2]/div[3]")]
        public IWebElement DiningPackagesTile;

        [FindsBy(How = How.XPath, Using = "//div[@id='additional-package-section']/div[2]/div[4]")]
        public IWebElement CabanaTileLoc;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Enhance your experience')]/parent::div/following-sibling::button")]
        public IWebElement learnMoreBoxCloseIcn;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Your Add-Ons')]/parent::div//*[contains(text(),'Dining Package')]")]
        public IWebElement DiningPackageTilelable;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Your Add-Ons')]/parent::div//*[contains(text(),'Birthday Package')]")]
        public IWebElement BirthdayPackageTilelable;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Your Add-Ons')]/parent::div//*[contains(text(),'Attraction Passes')]")]
        public IWebElement AttractionPassTilelable;

        [FindsBy(How = How.XPath, Using = "(//div[text()='SEE ALL INCLUDES >'])[1]")]
        public IWebElement SeeAllIncludesLink;

        [FindsBy(How = How.XPath, Using = "(//a[contains(.,'See what you get >')])[1]")]
        public IWebElement SeeWhatYouGetLink;
        

        [FindsBy(How = How.XPath, Using = "(//div[@class='ReactModal__Content ReactModal__Content--after-open Modal']//p)[1]")]
        public IWebElement PassDetaiilsSection;

        [FindsBy(How = How.XPath, Using = "//div[@class='ReactModal__Content ReactModal__Content--after-open Modal']//h2[contains(.,'$')]")]
        public IWebElement PriceSectionOnModal;

        [FindsBy(How = How.XPath, Using = "//div[@class='ReactModal__Content ReactModal__Content--after-open Modal']//img")]
        public IWebElement PassImageOnModal;

        [FindsBy(How = How.XPath, Using = "(//div[@class='ReactModal__Content ReactModal__Content--after-open Modal']//li)[1]")]
        public IWebElement AddPackageDetaiilsSection;

        [FindsBy(How = How.XPath, Using = "(//button[text()='ADD TO MY RESERVATION']//../..//preceding-sibling::div//div[contains(.,'$')])[2]")]
        public IWebElement PackagePriceSectionOnModal;

        [FindsBy(How = How.XPath, Using = "(//Select[@title='Select Days'])[1]")]
        public IWebElement NoOfDayDrpDinignModal;

        [FindsBy(How = How.XPath, Using = "//div[text()='Room Type']")]
        public IWebElement roomTypeSection;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Upgrade your room for only')]//preceding-sibling::div[contains(.,'$')]")]
        public IWebElement upgradeRoomPriceSec;

        [FindsBy(How = How.XPath, Using = "//a[text()='Upgrade Now']")]
        public IWebElement upgradeNowBtn;

        [FindsBy(How = How.XPath, Using = "(//button[text()='See More'])[2]")]
        public IWebElement seeMoteBtnOfSpecialReq;

        [FindsBy(How = How.XPath, Using = "//div[text()='Requests & Celebrations']")]
        public IWebElement specialReqPopUpTitle;

        [FindsBy(How = How.XPath, Using = "//label[@for='EC']//div[text()='Close to elevator']")]
        public IWebElement closeToElevatrOption;

        [FindsBy(How = How.XPath, Using = "//label[@for='CWP']//div[text()='Close to the Waterpark']")]
        public IWebElement closeToWaterprkOption;

        [FindsBy(How = How.XPath, Using = "//label[@for='EF']//div[text()='Far from Elevator']")]
        public IWebElement FarFormElvatorOption;

        [FindsBy(How = How.XPath, Using = "//label[@for='LL']//div[text()='Lobby Level']")]
        public IWebElement LobyLevelOption;

        [FindsBy(How = How.XPath, Using = "//label[@for='CRB']//div[contains(text(),'n Play')]")]
        public IWebElement PlayAnParkOption;

        [FindsBy(How = How.XPath, Using = "//label[@for='TF']//div[contains(text(),'Top Floor')]")]
        public IWebElement TopfloorOption;

        [FindsBy(How = How.XPath, Using = "//label[@for='OTHR']//div[contains(text(),'Other')]")]
        public IWebElement OtherOption;

        [FindsBy(How = How.Id, Using = "EC")]
        public IWebElement closeToElevtrCheckBox;

        [FindsBy(How = How.XPath, Using = "//input[@id='EC']//parent::div")]
        public IWebElement ElevtrCloseCheckBx;

        [FindsBy(How = How.XPath, Using = "//input[@id='TF']//parent::div")]
        public IWebElement TopFloorCheckBx;

        [FindsBy(How = How.Id, Using = "TF")]
        public IWebElement topFloorCheckBox;

        [FindsBy(How = How.XPath, Using = "//button//div[text()='Add to my reservation']")]
        public IWebElement AddToMyReservationBtn;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'I added requests to my reservation')]")]
        public IWebElement AddedReqToMyReserMessage;

        [FindsBy(How = How.XPath, Using = "//input[@id='birthday']//parent::div")]
        public IWebElement BirthdayCheckBox;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Name of person celebrating']")]
        public IWebElement NameOfPersonCelebratingTxtBox;

        [FindsBy(How = How.XPath, Using = "(//input[@placeholder='Name of person celebrating'])[2]")]
        public IWebElement NameOfPersonCelebratingSecndTxtBox;

        [FindsBy(How = How.XPath, Using = "((//*[@id='0'])//parent::div)[2]")]
        public IWebElement FirstNameDelete;

        [FindsBy(How = How.XPath, Using = "//div[text()='+ Add another person']")]
        public IWebElement addAnotherPersonlink;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Online Check-in will be available 24 hours prior to your reservation.')]")]
        public IWebElement onlineCheckInMSG;
        

        public static decimal firstBirthdayPckgCst;
        public static decimal scndBirthdayPckgCst;
        public static decimal firstPassCostTxt;

        // public static String paymentSpinner = "//div[@class='ReactModal__Content ReactModal__Content--after-open Modal']";

        //class="ReactModal__Content ReactModal__Content--after-open Modal"
        //public static string afterSplitResNum;
        public static string afterSplitResNum = "31649818"; //Add A Package

        public void clickOnAddAnotherPerson()
        {
            Browser.waitForElementToBeClickable(addAnotherPersonlink, 20);
            addAnotherPersonlink.Click();            
        }

        public void clickOnBirthDayCheckBtn()
        {
            Browser.waitForElementToBeClickable(BirthdayCheckBox, 20);
            BirthdayCheckBox.Click();
        }


        public void SendNamePersonOfCelebrating()
        {
            Browser.waitForElementToBeClickable(NameOfPersonCelebratingTxtBox, 20);
            NameOfPersonCelebratingTxtBox.SendKeys("TestName1");
        }

        public void SendScndNamePersonOfCelebrating()
        {
            Browser.waitForElementToBeClickable(NameOfPersonCelebratingSecndTxtBox, 20);
            NameOfPersonCelebratingSecndTxtBox.SendKeys("TestName2");
        }
        

        public void DeleteCelebratyName()
        {
            Browser.waitForElementToBeClickable(FirstNameDelete, 20);
            Actions actions = new Actions(Browser.driver);
            actions.MoveToElement(FirstNameDelete);
            actions.Click(FirstNameDelete).Build().Perform();
        }

        public Boolean verifyNameIsDeleteOrNot()
        {
            String currentVal = NameOfPersonCelebratingTxtBox.GetAttribute("value");
            String expectdVal = "TestName1";
            if(currentVal == expectdVal)
            {
                return false;
            }
            else { return true; }
        }

        public Boolean verifySecndNamesIsAdded()
        {
            String currentVal = NameOfPersonCelebratingSecndTxtBox.GetAttribute("value");
            String expectdVal = "TestName2";
            if (currentVal == expectdVal)
            {
                return true;
            }
            else { return false; }
        }

        public Boolean verifyFirstNamesIsAdded()
        {
            String currentVal = NameOfPersonCelebratingTxtBox.GetAttribute("value");
            String expectdVal = "TestName1";
            if (currentVal == expectdVal)
            {
                return true;
            }
            else { return false; }
        }

        public Boolean IsTopfloorCheckBoxChecked()
        {
            Browser.waitForElementToDisplayed(TopFloorCheckBx, 20);
            String value = TopFloorCheckBx.GetCssValue("background-image");
            if (value == "none")
            {
                return false;
            }
            else { return true; }
        }

        public void IsElvtrCloseCheckBoxChecked()
        {
            Browser.waitForElementToDisplayed(ElevtrCloseCheckBx, 20);
            String value = ElevtrCloseCheckBx.GetCssValue("background-image");
            if (value == "none")
            {
                ElevtrCloseCheckBx.Click();
            }           
        }

        public Boolean verifyRequestmessageAfterAdded()
        {
            Browser.waitForElementToBeClickable(AddedReqToMyReserMessage, 30);
            return AddedReqToMyReserMessage.Exists();
        }

        public void clickOnAddToMyreservationBtn()
        {
            Browser.waitForElementToBeClickable(AddToMyReservationBtn, 20);
            AddToMyReservationBtn.Click();
        }

        public void clickOnCloseToEvltrCheckBx()
        {
            Browser.waitForElementToBeClickable(ElevtrCloseCheckBx, 20);
            ElevtrCloseCheckBx.Click();
        }

        public void clickOnTopFloorCheckBx()
        {
            Browser.waitForElementToBeClickable(TopFloorCheckBx, 20);
            TopFloorCheckBx.Click();
        }
        
        public void clickOnSeeMoreSpecialReqBtn()
        {
            waitForPaymentSprinnerOut(paymentSpinner);
            Browser.waitForElementToBeClickable(seeMoteBtnOfSpecialReq, 20);
            Browser.javaScriptClick(seeMoteBtnOfSpecialReq);
            //seeMoteBtnOfSpecialReq.Click();
        }

        public Boolean specialrePopTitleverify()
        {
            Browser.waitForElementToBeClickable(specialReqPopUpTitle, 20);
            return specialReqPopUpTitle.Exists();
        }

        public Boolean closeToElevatrOptionverify()
        {
            Browser.waitForElementToBeClickable(closeToElevatrOption, 20);
            return closeToElevatrOption.Exists();
        }

        public Boolean closeToWaterprkOptionverify()
        {
            Browser.waitForElementToBeClickable(closeToWaterprkOption, 20);
            return closeToWaterprkOption.Exists();
        }

        public Boolean FarFormElvatorOptionverify()
        {
            Browser.waitForElementToBeClickable(FarFormElvatorOption, 20);
            return FarFormElvatorOption.Exists();
        }

        public Boolean LobyLevelOptionverify()
        {
            Browser.waitForElementToBeClickable(LobyLevelOption, 20);
            return LobyLevelOption.Exists();
        }

        public Boolean PlayAnParkOptionverify()
        {
            Browser.waitForElementToBeClickable(PlayAnParkOption, 20);
            return PlayAnParkOption.Exists();
        }

        public Boolean TopfloorOptionverify()
        {
            Browser.waitForElementToBeClickable(TopfloorOption, 20);
            return TopfloorOption.Exists();
        }

        public Boolean OtherOptionverify()
        {
            Browser.waitForElementToBeClickable(OtherOption, 20);
            return OtherOption.Exists();
        }

        public void clickAllDone()
        {           
            //waitForPaymentSprinnerOut(paymentSpinner);
            Browser.waitForElementToBeClickable(allDoneBtn, 30);
            allDoneBtn.Click();
            waitForPaymentSprinnerOut(paymentSpinner);
        }

        public void waitForPaymentSprinnerOut(String xpath)
        {
            Browser.waitForImplicitTime(6);
            Browser.waitForElementToBeInvisible(20, xpath);
            //Browser.waitForImplicitTime(10);
        }

        public void clickcloselink()
        {
            Browser.waitForElementToBeClickable(costSummaryClose, 20);
            costSummaryClose.Click();
        }

        public Boolean verifyLCOBtnNotAvailableOnConfrPage()
        {
            if (SuitePage.LCOExist == true) { 
                if (addLCOCheckOutBtn.Exists())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }else
            {
                return true;
            }
        }

        public Boolean newConfirmationPageDisplayed()
        {
            Browser.waitForElementToDisplayed(CMPPageWelcomeTitle, 20);
            //Thread.Sleep(7000);
            //Browser.waitForImplicitTime(8);
            bool cmpPageTitle = CMPPageWelcomeTitle.Displayed;
            return cmpPageTitle;
        }

        public Boolean reservationNumberDisplayed()
        {
            Browser.waitForElementToDisplayed(reservationNumber, 10);
            String splitNum = Browser.getText(reservationNumber);
            afterSplitResNum = splitNum.Substring(13);
            //afterSplitResNum = splitNum.Substring(20);
            Console.WriteLine("reservation No : " + afterSplitResNum);
            DataAccess.putData(afterSplitResNum);
            DataAccess.putReservationData(afterSplitResNum, 2, 2);
            bool resNum = reservationNumber.Displayed;
            return resNum;            
        }

        public Boolean cabanaSectionDisplayed()
        {
            Browser.waitForElementToDisplayed(cabanasHeadLineTitle, 10);
            bool cabanaTitle = cabanasHeadLineTitle.Displayed;
            return cabanaTitle;
        }

        public void clickCostSummaryBtn()
        {
            waitForPaymentSprinnerOut(paymentSpinner);
            //Browser.waitForElementToDisplayed(costSummaryBtn, 60);
            Browser.waitForElementToBeClickable(costSummaryBtn, 20);
            costSummaryBtn.Click();
        }

        public Boolean compareCMPAndPaymentSuiteTotalPrice()
        {
            Browser.waitForElementToDisplayed(CMPSuiteDetailsPriceLable, 20);
            double CMPSuitePrice = Browser.stringToDoubleConvert(Pages.NewConfirmation.CMPSuiteDetailsPriceLable);
            double PaymentSuitePrice = PaymentPage.suitePrice;
            if (CMPSuitePrice == PaymentSuitePrice)
            {
                return true;
            }
            else
            {
                return false;
            }            
        }

        public Boolean compareCMPAndPaymentPackagesTotalPrice()
        {
            Browser.waitForElementToDisplayed(CMPPckageTotal, 10);
            double CMPPckgTotal = Browser.stringToDoubleConvert(CMPPckageTotal);
            double PaymentPckgTotal = PaymentPage.PckageTotlPrice;
            if (CMPPckgTotal == PaymentPckgTotal)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean compareCMPAndPaymentPckgTotalPrice()
        {
            Browser.waitForElementToDisplayed(CMPSuiteDetailsPriceLable, 10);
            double CMPSuitePrice = Browser.stringToDoubleConvert(Pages.NewConfirmation.CMPSuiteDetailsPriceLable);
            double PaymentSuitePrice = PaymentPage.suitePrice;
            if (CMPSuitePrice == PaymentSuitePrice)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean compareCMPAndPaymentTaxesPrice()
        {
            Browser.waitForElementToDisplayed(CMPTaxPriceLable, 10);
            double CMPTaxes = Browser.stringToDoubleConvert(Pages.NewConfirmation.CMPTaxPriceLable);
            double PaymentTaxes = PaymentPage.taxPrice;
            if (CMPTaxes == PaymentTaxes)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean compareCMPAndPaymentResortFee()
        {
            Browser.waitForElementToDisplayed(CMPResortFeeLable, 10);
            double CMPResortFee = Browser.stringToDoubleConvert(Pages.NewConfirmation.CMPResortFeeLable);
            double PaymentResortFee = PaymentPage.resortPrice;
            if (CMPResortFee == PaymentResortFee)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean compareCMPAndPaymentTotalCost()
        {
            Browser.waitForElementToDisplayed(CMPSuiteTotalLable, 10);
            double CMPTotal = Browser.stringToDoubleConvert(Pages.NewConfirmation.CMPSuiteTotalLable);
            double PaymentTotal = PaymentPage.totalcost;
            if (CMPTotal == PaymentTotal)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean compareCMPAndPaymentDepositePaid()
        {
            Browser.waitForElementToDisplayed(CMPDepositePaidLable, 10);
            double CMPDepositePaid = Browser.stringToDoubleConvert(Pages.NewConfirmation.CMPDepositePaidLable);
            double PaymentDueToday = PaymentPage.dueTodayPrice;
            if (CMPDepositePaid == PaymentDueToday)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean compareCMPAndPaymentDueAtCheckin()
        {
            Browser.waitForElementToDisplayed(CMPDueATCheckInLable, 10);
            double CMPCheckInDue = Browser.stringToDoubleConvert(Pages.NewConfirmation.CMPDueATCheckInLable);
            double PaymentCheckInDue = PaymentPage.dueATCheckInPrice;
            if (CMPCheckInDue == PaymentCheckInDue)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean daypassreservationNumberDisplayed()
        {
            Browser.waitForElementToDisplayed(reservationNumber, 20);
            String splitNum = Browser.getText(reservationNumber);
            String afterSplit = splitNum.Substring(13);
            Console.WriteLine("reservation No : " + afterSplit);
            DataAccess.putData(afterSplit);
            DataAccess.putReservationData(afterSplit, 1, 2);
            bool resNum = reservationNumber.Displayed;
            return resNum;
        }

        public void clickOnAddAPackageBtn()
        {
            Browser.waitForElementToBeClickable(addAPackagebtn, 20);
            addAPackagebtn.Click();
        }

        public Boolean costSummarysectionDisplayed()
        {
            Browser.waitForElementToDisplayed(costSummaryBtn, 20);
            bool costsummaryDaypass = costSummaryBtn.Displayed;
            return costsummaryDaypass;
        }

        public void IclickSeeMoreDetailsBtn()
        {
            Browser.scrollToAnElement(SeeMoreDetailsBtn);
            Browser.waitForElementToBeClickable(SeeMoreDetailsBtn, 20);
            SeeMoreDetailsBtn.Click();
        }

        public void SubscribeSMSsignup()
        {
            Browser.waitForElementToDisplayed(SMSCheckbox, 20);
            if (SMSCheckbox.Enabled == true)
            {
                SMSCheckbox.Click();
            }
            else
            {
                SMSCheckbox.Click();
                SmsSignUpBtn.Click();
                Browser.waitForElementToDisplayed(SeeMoreDetailsBtn, 20);
                SeeMoreDetailsBtn.Click();
                SMSCheckbox.Click();
            }
            //SMSPhoneNum.Clear();
            //SMSPhoneNum.SendKeys("1234567890");
            SmsSignUpBtn.Click();
        }

        public Boolean IseeSubscribeMessage()
        {
            Browser.waitForElementToDisplayed(SMSsubscribeMsg, 20);
            Boolean ISSubscribed = SMSsubscribeMsg.Exists();
            return ISSubscribed;
        }

        public void IUnsubcribeSMSsignUp()
        {
            SeeMoreDetailsBtn.Click();
            Browser.waitForElementToBeClickable(SMSCheckbox, 20);
            SMSCheckbox.Click();
            //SMSPhoneNum.SendKeys("1234567890");
            Browser.waitForElementToBeClickable(SmsSignUpBtn, 20);
            SmsSignUpBtn.Click();
        }

        public Boolean UnsubscribeMessageDisplay()
        {
            Browser.waitForElementToDisplayed(SMSunsubcribeMsg, 20);
            Boolean ISUnSubscribed = SMSunsubcribeMsg.Exists();
            return ISUnSubscribed;
        }

        public void NaviagteOnCabanaReservationPage()
        {
            Pages.DayPass.waitForSprinnerOut(DayPassPage.waitforSpinnerStr);
            if (Pages.MyReserv.indoorCabanaViewLink.Exists())
            {
                Browser.waitForElementToBeClickable(Pages.MyReserv.indoorCabanaViewLink, 20);
                Pages.MyReserv.indoorCabanaViewLink.Click();
                Pages.DayPass.waitForSprinnerOut(DayPassPage.waitforSpinnerStr);
                Browser.waitForElementToDisplayed(reservationNumber, 20);
                String splitNum = Browser.getText(reservationNumber);
                String afterSplit = splitNum.Substring(13);
                Console.WriteLine("reservation No : " + afterSplit);
                DataAccess.putData(afterSplit);
                DataAccess.putReservationData(afterSplit, 3, 2);
            }
        }

        public Boolean DiningPackageTile()
        {
            Browser.waitForElementToDisplayed(DiningPackageTileTitle, 20);
            Boolean DiningPckTitle = DiningPackageTileTitle.Exists();
            return DiningPckTitle;
        }

        public Boolean DiningPackagesAddedTileCount()
        {
            Browser.waitForElementToDisplayed(DiningPackagesCount, 10);
            Boolean DiningPckTileCount = DiningPackagesCount.Exists();
            return DiningPckTileCount;
        }

        public Boolean CabanaTile()
        {
            Browser.waitForElementToDisplayed(CabanaTileTitle, 20);
            Boolean CabanaTitle = CabanaTileTitle.Exists();
            return CabanaTitle;
        }

        public Boolean AttractionPackageTile()
        {
            Browser.waitForElementToDisplayed(AttractionPassTileTitle, 20);
            Boolean AttractionPckTitle = AttractionPassTileTitle.Exists();
            return AttractionPckTitle;
        }

        public Boolean BirthdayPackageTile()
        {
            Browser.waitForElementToDisplayed(BirthdayTileTitle, 30);
            Boolean BirthdayTitle = BirthdayTileTitle.Exists();
            return BirthdayTitle;
        }
        

        public void IclickAddMorePassesBtn()
        {
            Browser.scrollToAnElement(AddMorePassesBtn);
            Browser.waitForElementToBeClickable(AddMorePassesBtn, 20);
            AddMorePassesBtn.Click();
        }

        public void IclickAddAPackageDingBtn()
        {
            Browser.scrollToAnElement(AddAPckBtnDining);
            Browser.waitForElementToBeClickable(AddAPckBtnDining, 20);
            AddAPckBtnDining.Click();
        }

        public void IclickAddAPackageBirthDayBtn()
        {
            Browser.scrollToAnElement(AddAPckBtnBirthDay);
            Browser.waitForElementToBeClickable(AddAPckBtnBirthDay, 20);
            AddAPckBtnBirthDay.Click();
        }
        

        public void IclickAddACabanaBtn()
        {
            Browser.scrollToAnElement(AddACabanBtn);
            Browser.waitForElementToBeClickable(AddACabanBtn, 20);
            AddACabanBtn.Click();
        }

        public void IclickAddBtnOfBirthDayPckg()
        {
            if (firstAddBtnBithPckg.Exists() == true)
            {
                Browser.scrollToAnElement(firstAddBtnBithPckg);
                firstBirthdayPckgCst = Browser.stringToDecimalConvert(firstBirthPckCOst);
                Browser.waitForElementToBeClickable(firstAddBtnBithPckg, 20);
                firstAddBtnBithPckg.Click();
            }

            if (scndAddBtnBithPckg.Exists() == true)
            {
                Browser.scrollToAnElement(scndAddBtnBithPckg);
                scndBirthdayPckgCst = Browser.stringToDecimalConvert(scndBirthPckCOst);
                Browser.waitForElementToBeClickable(scndAddBtnBithPckg, 20);
                scndAddBtnBithPckg.Click();
            }
        }

        public Boolean birthDayPckgTotal()
        {
            //Browser.waitForElementToDisplayed(BirthdayTileTitle, 30);
            decimal expectedPckgTotal = firstBirthdayPckgCst + scndBirthdayPckgCst;
            decimal actualSuiteTotal = Browser.stringToDecimalConvert(Pages.Payment.packageTotalCost);
            Boolean PckgTotal = expectedPckgTotal.Equals(actualSuiteTotal);
            return PckgTotal;
        }

        public void IclickAddBtnFirstPass()
        {
            Browser.waitForElementToBeClickable(AddBtnFirst, 20);
            AddBtnFirst.Click();
        }

        String firstPck = "//div[text()='";
        String firstPck1 = "']";

        public Boolean VerifyPackgesFirstTitle()
        {
            if (SuitePage.DiningFirstPckgAvl == true)
            {
                IWebElement reseNum = AddBtnFirst.FindElement(By.XPath(firstPck + SuitePage.DiningFirstPckgTitl + firstPck1));
                return reseNum.Exists();
            }
            else { return true;  }
        }

        public Boolean VerifyPackgesFirstCost()
        {
            if (SuitePage.DiningFirstPckgAvl == true)
            {
                IWebElement reseNum = AddBtnFirst.FindElement(By.XPath(firstPck + SuitePage.DiningFirstPckgTitlCost + firstPck1));
                return reseNum.Exists();                
            }
            else
            { return true;  }
        }

        public Boolean VerifyPackgesSecndTitle()
        {
            if (SuitePage.DiningScndPckgAvl == true)
            {
                IWebElement title = AddBtnFirst.FindElement(By.XPath(firstPck + SuitePage.DiningSecondPckgTitl + firstPck1));
                return title.Exists();
            }
            else { return true; }
        }

        public Boolean VerifyPackgesSecndCost()
        {
            if (SuitePage.DiningScndPckgAvl == true)
            {
                IWebElement Cost = AddBtnFirst.FindElement(By.XPath(firstPck + SuitePage.DiningSecondPckgTitlCost + firstPck1));
                return Cost.Exists();
            }
            else
            { return true; }
        }

        public Boolean DisplayPackagesTiles()
        {
            Browser.waitForElementToDisplayed(AttractionTile, 30);
            Browser.waitForElementToDisplayed(BirthdayPackagesTile, 30);
            Browser.waitForElementToDisplayed(DiningPackagesTile, 30);
            Browser.waitForElementToDisplayed(CabanaTileLoc, 30);
            Boolean IsPackagesDisplayed = AttractionTile.Displayed && BirthdayPackagesTile.Displayed && DiningPackagesTile.Displayed && CabanaTileLoc.Displayed;
            return IsPackagesDisplayed;
        }

        public void clickOnlearnMoreBoxCloseIcn()
        {
            //Browser.waitForElementToDisplayed(learnMoreBoxCloseIcn, 30);
            Browser.waitForElementToBeClickable(learnMoreBoxCloseIcn, 20);
            learnMoreBoxCloseIcn.Click();
        }

        public Boolean verifylearnMoreBoxCloseIcn()
        {
            Boolean learnmoreBox = learnMoreBoxCloseIcn.Exists();
            return learnmoreBox;
        }

        public Boolean verifyDiningPckgTileUnderAddOns()
        {
            Boolean DiningPackageTite = DiningPackageTilelable.Exists();
            return DiningPackageTite;
        }
        public Boolean verifyBirhtdayPckgTileUnderAddOns()
        {
            Boolean BirhtdayPackageTite = BirthdayPackageTilelable.Exists();
            return BirhtdayPackageTite;
        }
        public Boolean verifyAttractionPassesTileUnderAddOns()
        {
            Boolean AttractionPassTile = AttractionPassTilelable.Exists();
            return AttractionPassTile;
        }

        public void clickOnSeeAllIncludeLink()
        {
            Browser.waitForElementToBeClickable(SeeAllIncludesLink, 20);
            SeeAllIncludesLink.Click();
        }

        public void clickOnSeeSeeWhatYouGetLink()
        {
            Browser.waitForElementToBeClickable(SeeWhatYouGetLink, 20);
            SeeWhatYouGetLink.Click();
        }
        

        public Boolean verifyPassDetailsShouldbeDispalyed()
        {
            Boolean PassDetails = PassDetaiilsSection.Exists();
            return PassDetails;
        }

        public Boolean verifyPassPriceSecShouldbeDispalyed()
        {
            Boolean PassPriceSec = PriceSectionOnModal.Exists();
            return PassPriceSec;
        }

        public Boolean verifyPackagePriceSecShouldbeDispalyed()
        {
            Boolean PackgePriceSec = PackagePriceSectionOnModal.Exists();
            return PackgePriceSec;
        }
        

        public Boolean verifyPassImageSecShouldbeDispalyed()
        {
            Boolean PassImage = PassImageOnModal.Exists();
            return PassImage;
        }

        public Boolean verifyAddPackageDetailsShouldbeDispalyed()
        {
            Boolean PackageDetails = AddPackageDetaiilsSection.Exists();
            return PackageDetails;
        }

        public Boolean verifyRoomTypeSectionDispalyed()
        {
            Browser.waitForElementToBeClickable(roomTypeSection, 20);
            Boolean RoomTypeSec = roomTypeSection.Exists();
            return RoomTypeSec;
        }

        public Boolean verifyRoomTypePriceSectionDispalyed()
        {
            Browser.waitForElementToBeClickable(upgradeRoomPriceSec, 10);
            Boolean RoomTypePriceSec = upgradeRoomPriceSec.Exists();
            return RoomTypePriceSec;
        }
        
        public Boolean verifyRoomTypeUpgradeNowDispalyed()
        {
            Boolean UpgradeBtn = upgradeNowBtn.Exists();
            return UpgradeBtn;
        }

        public void clickOnRoomTypeUpgradeNow()
        {
            Browser.waitForElementToBeClickable(upgradeNowBtn, 10);
            upgradeNowBtn.Click();            
        }

        public Boolean VerifyNewNorPage()
        {
            Browser.SwitchToNewWindow();
            String url = Browser.driver.Url;
            if (url.Contains("nor1upgrades"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean IObserveNoOfDays()
        {
            String DaysVal = NoOfDayDrpDinignModal.Text;
            String expectedVal = "1 Days";
            if (DaysVal.Equals(expectedVal))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string CMPSuitePrice;
        public static string CMPSuiteTotal;
        public static string CMPPackageTotal;
        public static string CMPTaxes;
        public static string CMPResort;

        public static string EmailSuitePrice;
        public static string EmailSuiteTotal;
        public static string EmailPackageTotal;

        public void GetPricesfromCostSummaryForCabana()
        {
            Browser.waitForElementToDisplayed(CMPSuiteDetailsPriceLable, 30);
            CMPSuitePrice = Browser.getText(CMPSuiteDetailsPriceLable);
            CMPSuiteTotal = Browser.getText(CMPSuiteTotalLable);            
        }

        public void GetPricesfromCostSummary()
        {
            Browser.waitForElementToDisplayed(CMPSuiteDetailsPriceLable,30);
            CMPSuitePrice = Browser.getText(CMPSuiteDetailsPriceLable);
            CMPSuiteTotal = Browser.getText(CMPSuiteTotalLable);
            CMPPackageTotal = Browser.getText(CMPPckageTotal);
            CMPTaxes = Browser.getText(CMPTaxPriceLable);
            CMPResort = Browser.getText(CMPResortFeeLable);
        }
        
        public void GetPricesfromCostSummaryForDayPass()
        {
            Browser.waitForElementToDisplayed(dayPassCost, 30);
            CMPSuitePrice = Browser.getText(dayPassCost);
            CMPSuiteTotal = Browser.getText(CMPSuiteTotalLable);
            CMPPackageTotal = Browser.getText(CMPPckageTotal);
            CMPTaxes = Browser.getText(CMPTaxPriceLable);
        }

        public Boolean compareSuiteTotalAndEmailSuitetotal()
        {
            return CMPSuitePrice.Equals(EmailSuitePrice);
        }

        public Boolean compareSuitepriceAndEmailSuitePrice()
        {
            return CMPSuiteTotal.Equals(EmailSuiteTotal);
        }

        public Boolean comparePackageTotalAndEmailPackageTotal()
        {
            return CMPPackageTotal.Equals(EmailPackageTotal);
        }
                
        public Boolean onLinecheckInMSGOnConfirmPage()
        {
            Browser.waitForElementToBeClickable(onlineCheckInMSG, 20);
            return onlineCheckInMSG.Exists();
        }
    }
}
