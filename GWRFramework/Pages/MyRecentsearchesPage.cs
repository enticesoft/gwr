﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using TechTalk.SpecFlow;
using GWRFramework.TestData;
using System.Globalization;
using System.Security.Cryptography.X509Certificates;
using OpenQA.Selenium.Interactions;
using MongoDB.Driver.Core.Events;

namespace GWRFramework
{
   public class MyRecentSearchesPage
    {
        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'My Recent Searches')]")]
        public IWebElement MyRecentSearchLink;
        
        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Account Options'][1]")]
        public IWebElement UserMenu ;

        
        [FindsBy(How = How.XPath, Using = "//div[@id='root']")]
        public IWebElement RootRecentPage;

        String GuestCount = "//div[text()[contains(.,'*Date*')]]//following::div[text()[contains(.,'*AdultCnt* Adult')]]";

        [FindsBy(How = How.Id, Using = "email")]
        public IWebElement emailtxt;

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement passwordtxt;

        [FindsBy(How = How.XPath, Using = "//button[@type='button'][text()='Sign In']")]
        public IWebElement signInBtn;
        [FindsBy(How = How.XPath, Using = "//*[@id='sign-in-create-account']/div/span[contains(text(), 'Sign In')]")]
        public IWebElement signInLink;

        [FindsBy(How = How.XPath, Using = "//div[@id='root']//following::h3[contains(text(),'My Recent Searches')]")]
        public IWebElement MyRecentText;

        
        [FindsBy(How = How.XPath, Using = "//li[@role='menuitem']//a[contains(text(),'Book Your Stay')]")]
        public IWebElement BookYourStay;

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Make a Reservation')]")]
        public IWebElement MakeReservation;
        
        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Delete All Searches')]")]
        public IWebElement DeleteAllSearch;

        

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Recent Searches List']//following-sibling::span")]
        public IWebElement RecentSerachCount;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Continue Search')]//following::div[1]")]
        public IWebElement DeleteRecentSearchBtn;

        
        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Continue Search')]")]
        public IWebElement ContinueSearchLink;

        [FindsBy(How = How.XPath, Using = "//div//h3[contains(text(),'Chicago')]//following::div[contains(text(),'Continue Search')][1]")]
        public IWebElement CntnueSrchForGurnee;
        

        [FindsBy(How = How.XPath, Using = "//button[@aria-label='Booking Widget Submit Button']")]
        public IWebElement UpdateStayBtn;
               

          [FindsBy(How = How.XPath, Using = "(//ul[@direction='s']//following::div)[1]")]
        public IWebElement RecentSrchLst;

        
        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Select Current Location']")]
        public IWebElement PropertyDrDn;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Recent Searches List']")]
        public IWebElement RecntSrchDrDp;

        [FindsBy(How = How.XPath, Using = "//ul[@direction='s']")]
        public IWebElement RecntSrchDrDpOption;

        string RecentSrcOptPath = "//div[contains(text(),'*SrchDate*')]";

        string ListOfPrty = "//a[contains(text(),'*Property1*')]";
        
        [FindsBy(How = How.XPath, Using ="//li[@role='menuitem']")]
        public IWebElement ListOfPrty1;
        

        [FindsBy(How = How.XPath, Using = "//div[@class='header-container__logo']")]
        public IWebElement GWLogo;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Recent Searches List']//ul//li[1]")]
        public IWebElement RecentSearchFirstOption;
        

        public static String waitRecentSearchSpinnerStr = "//*[@class='loading-indicator']";

        String guestCnt1 = "(//div[text()='";
        String guestCnt2 = " Adults, AMTCERT'])[1]";


        public void IamLogIn(String email, String pass)
        {
            
            Browser.waitForElementToDisplayed(signInLink, 20);
            signInLink.Click();
            Browser.waitForElementToDisplayed(emailtxt, 15);
            emailtxt.SendKeys(email);
            Browser.waitForElementToDisplayed(passwordtxt, 15);
            passwordtxt.SendKeys(pass);
            Browser.waitForElementToBeClickable(signInBtn, 20);
            signInBtn.Click();

        }

        public void clickOnMyRecentSearchesLink()
        {
            //Browser.waitForElementToDisplayed(UserMenu, 90);        
            Browser.waitForElementToBeClickable(UserMenu, 20);
            UserMenu.Click();
            Browser.waitForElementToBeClickable(MyRecentSearchLink, 20);
            MyRecentSearchLink.Click();
            waitRecentSearchSprinnerOut(waitRecentSearchSpinnerStr);
        }

        public Boolean IMyRecentSearchpage()
        {
            Browser.waitForElementToDisplayed(MyRecentText, 20);
            bool MyRecentPage = MyRecentText.Displayed;
            return MyRecentPage;
        }

        public Boolean VerifyGuestCount(Table table)

        {          
            String Guest = table.Rows[0]["Adults"];            
            Browser.waitForElementToDisplayed(RootRecentPage, 20);
            IWebElement GuestCount1 = RootRecentPage.FindElement(By.XPath(guestCnt1+ Guest+ guestCnt2));            
            if (GuestCount1.Exists())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ClickBookYourStay()
        {
            Browser.waitForElementToBeClickable(BookYourStay, 20);
            BookYourStay.Click();
        }

        public void DeleteAllSearches()
        {
            waitRecentSearchSprinnerOut(waitRecentSearchSpinnerStr);
            Browser.waitForElementToDisplayed(RecentSerachCount, 20);
            String RecentSearchCnt = RecentSerachCount.Text;
            if (!RecentSearchCnt.Equals("0"))
            {
                Browser.scrollToAnElement(DeleteAllSearch);
                Browser.waitForElementToBeClickable(DeleteAllSearch, 20);
                DeleteAllSearch.Click();
                waitRecentSearchSprinnerOut(waitRecentSearchSpinnerStr);
                Browser.scrollVerticalBy("-1800");
            }

        }

        public void clickOnMakeReservationsLink()
        {
            Thread.Sleep(4000);
            Browser.waitForElementToDisplayed(RecentSerachCount, 20);
            String RecentSearchCnt = RecentSerachCount.Text;
            if (RecentSearchCnt.Equals("0"))
            {
                Browser.waitForElementToBeClickable(MakeReservation, 20);
                MakeReservation.Click();
            }
            
        }

        public Boolean seeMakeMyReservationLink()
        {
            Browser.waitForElementToDisplayed(MakeReservation, 20);
            Boolean MakeReservationLink = MakeReservation.Displayed;
            return MakeReservationLink;
        }
        public void DeleteOneRecentSearch()
        {
            //Browser.waitForElementToDisplayed(RecentSerachCount, 40);
            Browser.waitForElementToBeClickable(DeleteRecentSearchBtn, 20);
            DeleteRecentSearchBtn.Click();
            waitRecentSearchSprinnerOut(waitRecentSearchSpinnerStr);
        }

        public Boolean IseeDeleteBtnOnRecentSerach()
        {
            Boolean IsDeteled = DeleteRecentSearchBtn.Displayed;
            return IsDeteled;
        }

        public void DeleteAllAddedSearchesFirst()
        {
            Pages.MyRecentSearchesPage.DeleteAllSearches();
            waitRecentSearchSprinnerOut(waitRecentSearchSpinnerStr);
        }


        public void ClickContinueSearchLnk()
        {            
            //Browser.waitForElementToDisplayed(CntnueSrchForGurnee, 90);
            Browser.waitForElementToBeClickable(CntnueSrchForGurnee, 20);
            CntnueSrchForGurnee.Click();      
        }
        public void ClickContinueSearchBtn()
        {           
            Browser.waitForElementToBeClickable(ContinueSearchLink, 20);
            ContinueSearchLink.Click();
        }

        public void IsearchSuiteForMultiTime(int Count)
        {
            Browser.waitForElementToBeClickable(UpdateStayBtn, 20);

            for (int i = 0; i < Count; i++)
            {
                UpdateStayBtn.Click();
            }
        }
        public Boolean VerifyRecentSearchCnt(int Count)
        {
            Browser.waitForElementToDisplayed(RecentSerachCount, 20);
            String SearchCount = RecentSerachCount.Text;
          
            if (SearchCount.Equals(Count))
            {
                return true;
            }else{
                return false;
            }
        }

        public void selectLocation(Table table)
        {
            Browser.waitForElementToBeClickable(PropertyDrDn, 20);
            PropertyDrDn.Click();
            String Property = table.Rows[0]["Property"];
            IWebElement Property2 = ListOfPrty1.FindElements(By.XPath(ListOfPrty.Replace("*Property1*" , Property))).FirstOrDefault();
            //Property2.Click();
            Browser.javaScriptClick(Property2);
        }
        public void clickOnRecentSrcDropDown()
        {
            Browser.waitForElementToBeClickable(RecntSrchDrDp, 20);
            RecntSrchDrDp.Click();
        }

        public void SelectOptionFromRecentList()
        {
            Browser.waitForElementToDisplayed(RecntSrchDrDpOption, 20);
            RecentSearchFirstOption.Click();
            //String checkIn = DataAccess.getData(3, 2);
            // DateTime dtCheckIn = Convert.ToDateTime("25/12/2020");
            //DateTime dtCheckIn = Convert.ToDateTime(checkIn);

            //string listCheckIn = dtCheckIn.ToString("ddd") + ", " + CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(dtCheckIn.Month) + " " + dtCheckIn.Day;

            //IWebElement searchOption = RecntSrchDrDpOption.FindElements(By.XPath(RecentSrcOptPath.Replace("*SrchDate*", listCheckIn))).FirstOrDefault();
            //Browser.waitForElementToDisplayed(searchOption, 20);
            //string text = searchOption.GetAttribute("innerText");
            //searchOption.Click();
        }
        public Boolean VerifyUpdatedSearch()
        {
            String checkIn = DataAccess.getData(3, 2);
               Browser.waitForElementToDisplayed(Pages.Suite.checkInDate, 20);
              String text = Pages.Suite.checkInDate.GetAttribute("Value");

            if (checkIn.Equals(text))
            {
                return true;
            }  else {
                return false;
            }


        }

        public Boolean VerifyCurrentSelectedProperty()
        {
            Browser.waitForElementToDisplayed(PropertyDrDn, 20);

            String CurrentProperty = PropertyDrDn.GetAttribute("innerText");
           // Console.WriteLine(CurrentProperty,  "Current property");
           String propertyName = DataAccess.getData(1, 2);

            if (CurrentProperty.Contains(propertyName))
            {
                return true;
            } else  {
                return false;
            }
        }
        public void navigatePropertyHomePage()
        {
            Browser.waitForElementToDisplayed(GWLogo, 20);
            GWLogo.Click();
        }

        public Boolean isNotSignIn()
        {
            try
            {
                return Pages.PropertyHome.signInLink.Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public Boolean isCloseDealPopup()
        {
            try
            {
                return Pages.PropertyHome.dealsSignUpLabel.Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void waitRecentSearchSprinnerOut(String xpath)
        {
            //Browser.waitForImplicitTime(4);
            Browser.waitForElementToBeInvisible(20, xpath);
            //Browser.waitForImplicitTime(4);
        }
    }
}
    

