﻿using GWRFramework.TestData;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWRFramework
{
    public class DealSignUpPage
    {
        [FindsBy(How = How.Id, Using = "email")]
        public IWebElement emailIdTextbox;

        [FindsBy(How = How.Id, Using = "firstName")]
        public IWebElement firstNameTextbox;

        [FindsBy(How = How.Id, Using = "lastName")]
        public IWebElement lastNameTextbox;

        [FindsBy(How = How.Id, Using = "zip")]
        public IWebElement postalCodeTextbox;

        [FindsBy(How = How.XPath, Using = "//button[contains(text(), 'Sign Up')]")]
        public IWebElement signUpBtn;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(), 'Success!')]")]
        public IWebElement successMsg;

        [FindsBy(How = How.XPath, Using = "//button[contains(text(), 'ALL DONE')]")]
        public IWebElement allDoneBtn;
               

        public void enterFirstNameTextbox(String firstName)
        {
            Browser.waitForElementToDisplayed(firstNameTextbox, 2);
            firstNameTextbox.SendKeys(firstName);
        }

        public void enterLastNameTextbox(String lastName)
        {
            Browser.waitForElementToDisplayed(lastNameTextbox, 2);
            lastNameTextbox.SendKeys(lastName);
        }

        public void enterEmailIdTextbox(String emailId)
        {
            Browser.waitForElementToDisplayed(emailIdTextbox, 2);
            emailIdTextbox.SendKeys(emailId);
        }

        public void enterPostalCodeTextbox()
        {
            String postalCode = DataAccess.getData(2, 2);
            Browser.waitForElementToDisplayed(postalCodeTextbox, 2);
            postalCodeTextbox.SendKeys(postalCode);
        }

        public void clickSignUpButton()
        {
            Browser.waitForElementToBeClickable(signUpBtn, 5);
            Browser.javaScriptClick(signUpBtn);
            //signUpBtn.Click();
        }

        public Boolean checkSuccessMsg()
        {
            Browser.waitForElementToDisplayed(successMsg, 20);
            return successMsg.Exists();
        }

        public void clickAllDoneButton()
        {
            Browser.waitForElementToBeClickable(allDoneBtn, 20);
            allDoneBtn.Click();
        }

    }
}
