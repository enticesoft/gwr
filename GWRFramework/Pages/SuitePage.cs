﻿using GWRFramework.TestData;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GWRFramework
{
    public class SuitePage
    {
        [OpenQA.Selenium.Support.PageObjects.FindsBy(How = How.XPath, Using = "//input[@id='checkInDateSelection']")]
        public OpenQA.Selenium.IWebElement CheckinField;

        [OpenQA.Selenium.Support.PageObjects.FindsBy(How = How.Id, Using = "checkInDateSelection")]
        public IWebElement checkInDate;

        [FindsBy(How = How.Id, Using = "checkOutDateSelection")]
        public IWebElement checkOutDate;

        [FindsBy(How = How.XPath, Using = "//input[@name='totalGuests']")]
        public IWebElement totalGuestsFld;

        [FindsBy(How = How.XPath, Using = "//*[text()='ADULTS']/parent::div/following::div[1]//button[2]")]
        public IWebElement AdultsPlusBtn;

        [FindsBy(How = How.XPath, Using = "//*[text()='ADULTS']/parent::div/following::div[1]//input")]
        public IWebElement AdultCntTxt;
        

        [FindsBy(How = How.XPath, Using = "//*[text()='ADULTS']/parent::div/following::div[1]//button[1]")]
        public IWebElement AdultsMinusBtn;

        [FindsBy(How = How.XPath, Using = "//input[@name='totalGuests'][contains(@value,'4')]")]
        public IWebElement IncreasedGuests;

        [FindsBy(How = How.XPath, Using = "//input[@name='totalGuests'][contains(@value,'2')]")]
        public IWebElement DcreasedGuests;

        [FindsBy(How = How.XPath, Using = "//*[text()='KIDS']/parent::div/following::div[1]//button[2]")]
        public IWebElement KidsPlusBtn;

        [FindsBy(How = How.XPath, Using = "//*[text()='KIDS']/parent::div/following::div[1]//button[1]")]
        public IWebElement KidsMinusBtn;

        [FindsBy(How = How.XPath, Using = "//*[text()='KIDS']/parent::div/following::div[1]//input")]
        public IWebElement KidsCntTxt;

        [FindsBy(How = How.XPath, Using = "//*[@name='kidsAges.0']")]
        public IWebElement Kids1AgeDpDn;

        [FindsBy(How = How.XPath, Using = "//*[@name='kidsAges.1']")]
        public IWebElement Kids2AgeDpDn;

        [FindsBy(How = How.XPath, Using = "//*[@name='kidsAges.2']")]
        public IWebElement Kids3AgeDpDn;

        [FindsBy(How = How.XPath, Using = "//*[@name='kidsAges.3']")]
        public IWebElement Kids4AgeDpDn;

        [FindsBy(How = How.XPath, Using = "//*[@name='offerCode']")]
        public IWebElement OfferCodeFld;

        [FindsBy(How = How.XPath, Using = "//button[text()='Book Now']")]
        public IWebElement bookNowBtn;

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'tab-panel--selected')] | //div[@aria-label='Items included in your reservation']//span[contains(.,'Included')]")]
        public IWebElement suiteRoom;

        /*[FindsBy(How = How.XPath, Using = "(//div[@role='tabpanel']//button[@type='button']//div[contains(text(), 'Select Room')])[1]")]
        public IWebElement selectBtn;*/

        [FindsBy(How = How.XPath, Using = "(//button[contains(text(), 'Select Room')])[2]")]
        public IWebElement selectBtn;
        

        [FindsBy(How = How.XPath, Using = "(//div[@role='dialog']//button[contains(text(), 'No thanks')])[1]")]
        public IWebElement LCONoThanksBtn;

        [FindsBy(How = How.XPath, Using = "//button[@aria-label='Continue to Dining Packages']")]
        public IWebElement continueToDiningPageBtn;

        [FindsBy(How = How.XPath, Using = "//button[@aria-label='Continue to Payment']")]
        public IWebElement continueToPaymentPageBtn;

        [FindsBy(How = How.XPath, Using = "//*[contains(@id,'react-tabs')]//span[contains(text(),'standard')]")]
        public IWebElement standardTab;

        public static string standardTagStr = "//li[contains(@id,'react-tabs')]//span[contains(text(),'standard')]";

        [FindsBy(How = How.XPath, Using = "//ul[@role='tablist']//li[3]//span[contains(text(),'premium')]")]
        public IWebElement premiumTab;

        public static string premiumTagStr = "//ul[@role='tablist']//li[3]//span[contains(text(),'premium')]";

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Items included in your reservation']//span[contains(.,'Included')]")]
        public IWebElement includeWithYourStaySectn;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Items included in your reservation']//div[contains(text(),'Water Park Passes')]")]
        public IWebElement waterParkPassesSectn;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Items included in your reservation']//div[contains(text(),'1 p.m. Access to Water Park')]")]
        public IWebElement accessToWaterParkSectn;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Items included in your reservation']//div[contains(text(),'Kids Activities')]")]
        public IWebElement kidsActivitiesSectn;

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Items included in your reservation']//div[contains(text(),'Unlimited Wifi')]")]
        public IWebElement unlimitedWifiSectn;

        //[FindsBy(How = How.XPath, Using = "(//*[text()='Select Room'])[1]")]
        //public IWebElement selectRoomBtn;

        [FindsBy(How = How.XPath, Using = "//div[@role='dialog']//div[@data-state='entered']")]
        public IWebElement lcoPopup;

        [FindsBy(How = How.XPath, Using = "//h3//div[contains(.,'Add 2:00 pm ')]")]
        public IWebElement lcoPopupTitle;

        [FindsBy(How = How.XPath, Using = "(//div[contains(.,'additional amount')]//preceding-sibling::div//div[contains(.,'+ $')])[4]")]
        public IWebElement lcoPopupPrice;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Stay beyond the')]")]
        public IWebElement lcoPopupDescrptn;

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Yes, add late checkout')]")]
        public IWebElement lcoPopupYesAddLCOBtn;

        [FindsBy(How = How.XPath, Using = "(//div[@role='dialog']//button[contains(text(), 'No thanks')])[1]")]
        public IWebElement lcoPopupNoThanksBtn;

        [FindsBy(How = How.XPath, Using = "(//h3[contains(@aria-label,'Suite Name')])[1]")]
        public IWebElement premiumSuiteTitle;

        [FindsBy(How = How.XPath, Using = "(//*[contains(text(),'Base')])[1]")]
        public IWebElement baseMaxOccuptnSectn;

        [FindsBy(How = How.XPath, Using = "(//*[text()='VIEW'])[1]")]
        public IWebElement viewDetailsLink;


        [FindsBy(How = How.XPath, Using = "(//*[contains(text(),'CHECK RATE CALENDAR')])[2]")]
        public IWebElement chkRateCalLink;

        [FindsBy(How = How.XPath, Using = "((//select[@title='Suite Options'])[1]// ancestor::div)[15]//following-sibling::div//div[@role='complementary']//span[text()='CHECK RATE CALENDAR']")]
        public IWebElement chkRateCalLinkWithOption;

        [FindsBy(How = How.XPath, Using = "((//div[@aria-label='Best Available Rate'])[1]//div)[3]")]
        public IWebElement suitePriceSectn;

        
        [FindsBy(How = How.XPath, Using = "//*[@aria-selected='true']//span[contains(.,'themed')]")]
        public IWebElement themedtab;

        [FindsBy(How = How.XPath, Using = "(//h3[starts-with(@aria-label,'Suite Name')])[2]")]
        public IWebElement suiteTitle;

        [FindsBy(How = How.XPath, Using = "(//span[contains(.,'Base')])[1]")]
        public IWebElement basemaxsection;

        [FindsBy(How = How.XPath, Using = "(//div[@role='region']//p)[1]")]
        public IWebElement suitedescription;

        [FindsBy(How = How.XPath, Using = "//span[contains(.,'CHECK RATE CALENDAR')][1]")]
        public IWebElement checkratecalendarlink;

        [FindsBy(How = How.XPath, Using = "((//div[@aria-label='Best Available Rate'])[1]//div[contains(.,'Select Room')])[1]")]
        public IWebElement suitepricesection;

        [FindsBy(How = How.XPath, Using = "(//div[@aria-label='Best Available Rate']//div[text()='AMTCERT'])[1]")]
        public IWebElement appliedoffercode;

        [FindsBy(How = How.XPath, Using = "//input[@name='offerCode']")]
        public IWebElement actualappliedoffercode;

        [FindsBy(How = How.XPath, Using = "(//div[@aria-label='Best Available Rate']//div[text()='Best Available Rate'])[1]")]
        public IWebElement bestAvailableRate;

        [FindsBy(How = How.XPath, Using = "//input[@id='totalGuests']")]
        public IWebElement selectguestbox;

        [FindsBy(How = How.XPath, Using = "//div[text()='Accessible Rooms Only']//following-sibling::label")]
        public IWebElement togglebbtn;

        [FindsBy(How = How.XPath, Using = "//button[contains(.,'Update Your Stay')]")]
        public IWebElement updateBtn;

        [FindsBy(How = How.XPath, Using = "(//span[contains(.,'Accessible')])[1]")]
        public IWebElement accesibletagintosuite;

        [FindsBy(How = How.XPath, Using = "(//span[contains(.,'Max')])[1]")]
        public IWebElement maxOccupanySection;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'themed')]")]
        public IWebElement themedTab;

        [FindsBy(How = How.XPath, Using = "(//p[contains(.,'Exceeded Maximum Occupancy')])[1]")]
        public IWebElement exceedMaxOccupancyalert;

 
        [FindsBy(How = How.XPath, Using = "//button[text()='Update Your Stay']")]
        public IWebElement updateyourstayBtn;

        [FindsBy(How = How.XPath, Using = "//*[text()='themed']")]
        public IWebElement ThemedTab;

        [FindsBy(How = How.XPath, Using = "(//*[text()='Exceeded Maximum Occupancy'])[1]")]
        public IWebElement suiteExceedMaxOccupanyMsg;

        [FindsBy(How = How.XPath, Using = "(//*[text()='NOT AVAILABLE'])[1]")]
        public IWebElement notAvailableLabel;

        [FindsBy(How = How.XPath, Using = "//*[@role='dialog']//*[name()='path']")]
        public IWebElement offerCodeErrorCloseIcon;

        [FindsBy(How = How.XPath, Using = "(//*[@aria-label='Best Available Rate']//div[contains(.,'Promo Code Applied')])[1]")]
        public IWebElement BBARSuite;

        [FindsBy(How = How.XPath, Using = "(//*[@aria-label='Best Available Rate'])[2]//*[contains(text(),'was')]/following-sibling::span")]
        public IWebElement suitePrice;

        [FindsBy(How = How.XPath, Using = "(//h3[starts-with(@aria-label,'Suite Name -')])[2]")]
        public IWebElement FirstsuiteName;

        [FindsBy(How = How.XPath, Using = "(//span[starts-with(text(),'Base')])[2]")]
        public IWebElement FirstSuiteBaseCount;
        


        //[FindsBy(How = How.XPath, Using = "//div[@id='root']//*[text()='Please enter number of adults']")]
        //public IWebElement adultValMsg;

        [FindsBy(How = How.XPath, Using = "(//select[@title='Suite Options'])[1]")]
        public IWebElement suiteOptionDrp;

        [FindsBy(How = How.XPath, Using = "(//select[@title='Suite Options'])[1]//option[2]")]
        public IWebElement suiteOptionDrpValue;

        [FindsBy(How = How.XPath, Using = "(//select[@title='Suite Options'])[1]/../../.././/following-sibling::div[@aria-label='Best Available Rate']//div//button")]
        public IWebElement selectBtnSuiteOption;
        

        [FindsBy(How = How.XPath, Using = "(//*[@aria-label='Best Available Rate'])[2]//*[contains(text(),'was')]")]
        public IWebElement suiteWasValue;

        [FindsBy(How = How.XPath, Using = "//*[text()='ADULTS']/parent::div/following::div[1]//button[1]/following-sibling::input")]
        public IWebElement adultsCount;

        [FindsBy(How = How.XPath, Using = "//*[text()='KIDS']/parent::div/following::div[1]//button[1]/following-sibling::input")]
        public IWebElement kidsCount;

        [FindsBy(How = How.XPath, Using = "(//*[contains(text(),'Flexible Dates')])[1]/parent::div//*[contains(text(),'Check Rate Calendar')]")]
        public IWebElement SoldOutchkRateCalLink;

        [FindsBy(How = How.XPath, Using = "(//*[@aria-label='Best Available Rate']//div[contains(.,'Promo Code Applied')]//following-sibling::div)[1]")]
        public IWebElement appliedOffercodeLable;

        [FindsBy(How = How.XPath, Using = "//*[@aria-selected='true']//span[contains(.,'standard')]")]
        public IWebElement standardtab;

        [FindsBy(How = How.XPath, Using = "//*[@aria-selected='true']//span[contains(.,'premium')]")]
        public IWebElement premiumtab;

        [FindsBy(How = How.XPath, Using = "//*[@id='qualifyingIDCode']")]
        public IWebElement qualifyingIDTx;

        [FindsBy(How = How.XPath, Using = "//div[@class='cookie-notification__container']//button[@class='cookie-notification__close-button']")]
        public IWebElement CoockieCLose;
               
        //[FindsBy(How = How.XPath, Using = "(//div[contains(text(),'Best Available Rate')])[1]")]
        //public IWebElement bestAvailableRate;

        [FindsBy(How = How.XPath, Using = "(//div[contains(text(),'Nightly Average')])[2]//preceding::span[1]")]
        public IWebElement Suiteprice;

        [FindsBy(How = How.XPath, Using = "//button[text()='Room Type']")]
        public IWebElement roomTypeFilterDrp;

        [FindsBy(How = How.XPath, Using = "//label[@for='themed']//input//parent::div")]
        public IWebElement ThemedCheckBx;

        [FindsBy(How = How.XPath, Using = "//label[@for='standard']//input//parent::div")]
        public IWebElement StandardCheckBx;

        [FindsBy(How = How.XPath, Using = "//label[@for='premium']//input//parent::div")]
        public IWebElement PremiumCheckBx;

        [FindsBy(How = How.XPath, Using = "//span[text()='Accessible Rooms Only']//following-sibling::label//input")]
        public IWebElement accessibleToggleON;

        [FindsBy(How = How.XPath, Using = "//button//span[text()='Close']")]
        public IWebElement CloseLinkPopUp;

        [FindsBy(How = How.XPath, Using = "(//h3[contains(@aria-label,'Suite Name')])[1]//parent::div/..//preceding-sibling::div//div[text()='themed']")]
        public IWebElement ThemedLableOnSuite;

        [FindsBy(How = How.XPath, Using = "(//h3[contains(@aria-label,'Suite Name')])[1]//parent::div/..//preceding-sibling::div//div[text()='standard']")]
        public IWebElement StandardLableOnSuite;

        [FindsBy(How = How.XPath, Using = "(//h3[contains(@aria-label,'Suite Name')])[1]//parent::div/..//preceding-sibling::div//div[text()='premium']")]
        public IWebElement PremiumLableOnSuite;

        [FindsBy(How = How.XPath, Using = "//li//div[text()='Lowest to Highest Price']")]
        public IWebElement LowestToHighestSortOption;

        [FindsBy(How = How.XPath, Using = "//li//div[text()='Highest to Lowest Price']")]
        public IWebElement HighestToLowestSortOption;

        [FindsBy(How = How.XPath, Using = "//li//div[text()='Recommended for you']")]
        public IWebElement RecommendedSortOption;

        [FindsBy(How = How.XPath, Using = "//button[text()='Sort By']")]
        public IWebElement SortByFilterDrpDown;

        [FindsBy(How = How.XPath, Using = "(//*[@aria-label='Best Available Rate'])[1]//*[contains(text(),'was')]/following-sibling::span")]
        public IWebElement FirstsuitePrice;

        [FindsBy(How = How.XPath, Using = "(//*[@aria-label='Best Available Rate'])[2]//*[contains(text(),'was')]/following-sibling::span")]
        public IWebElement SecondsuitePrice;

        [FindsBy(How = How.XPath, Using = "//p[text()='Recommended for you']")]
        public IWebElement recommenededLabel;

        [FindsBy(How = How.XPath, Using = "//a[text()='Clear Filters']")]
        public IWebElement clearFilterLink;

        [FindsBy(How = How.XPath, Using = "//div[text()='ESAVER']")]
        public IWebElement esaverOfferCod;

        [FindsBy(How = How.XPath, Using = "//div[text()='MOREFUN']")]
        public IWebElement moreFunOfferCod;


        static String SuitePriceBefore = "";

        public static string checkIndtTxt;
        public static string checkOutdtTxt;
        public static string PopupPrice;

        public static int GuestCount;
        public static int StayNights;
        public static Double suiteWasPrice;
        public static string AdultsCount;
        public static string KidsCount;
        public static Double LCOPrice;

        public static Boolean LCOExist = false;
        public static string PupPassTitle;
        public static string PawPassTitle;
        public static string WolfPassTitle;

        public static string PupPassCost;
        public static string PawPassCost;
        public static string WolfPassCost;

        public static string DiningFirstPckgTitl;
        public static string DiningSecondPckgTitl;
        public static string DiningThirdPckgTitl;

        public static string DiningFirstPckgTitlCost;
        public static string DiningSecondPckgTitlCost;
        public static string DiningThirdPckgTitlCost;

        public static Boolean DiningFirstPckgAvl;
        public static Boolean DiningScndPckgAvl;
        public static Boolean DiningThirdPckgAvl;

        public static string SuiteOptionVal;

        public static Decimal suitefirstPriceLow;
        public static Decimal suiteSecndPriceLow;

        public void selectCheckIn()
        {

            String checkIn = DataAccess.getData(3, 2);
            Pages.PropertyHome.closeDealsSignUp();
            clickCookiesClose();
            Browser.waitForElementToBeClickable(checkInDate, 20);
            checkInDate.Clear();
            checkInDate.SendKeys(checkIn);
        }

        public Boolean verifyCheckInField()
        {
            Browser.waitForElementToBeClickable(checkInDate, 20);
            return checkInDate.Exists();
        }

        public void selectCheckIn(String chckInDate)
        {

            //String checkIn = DataAccess.getData(3, 2);
            //Pages.PropertyHome.closeDealsSignUp();
            //clickCookiesClose();
            Browser.waitForElementToBeClickable(checkInDate, 20);
            checkInDate.Clear();
            checkInDate.SendKeys(chckInDate);
        }

        public void selectCheckOut()
        {
            String checkOut = DataAccess.getData(4, 2);
            //Pages.PropertyHome.closeDealsSignUp();
            //clickCookiesClose();
            //Browser.waitForElementToBeClickable(checkOutDate, 20);
            checkOutDate.Clear();
            checkOutDate.SendKeys(checkOut);
        }

        public void SelectAdults(int AdultValue)
        {
            Browser.scrollVerticalBy("300");         
            Browser.waitForElementToBeClickable(totalGuestsFld, 5);
            checkInDate.Click();
            totalGuestsFld.Click();

            String exstnfAdulCnt = AdultCntTxt.GetAttribute("value");
            int adults = Int32.Parse(exstnfAdulCnt);
            if (adults == 0 || adults == 1)
            {
                int adultmins = AdultValue - adults;
                for (int i = 1; i <= adultmins; i++)
                {
                    Browser.waitForElementToBeClickable(AdultsPlusBtn, 5);
                    AdultsPlusBtn.Click();
                    Thread.Sleep(1000);
                }
            }
            else {
                int existngAdultrem = adults - 1;
                for (int i = 1; i <= existngAdultrem; i++)
                {
                    Browser.waitForElementToBeClickable(AdultsMinusBtn, 5);                   
                    AdultsMinusBtn.Click();
                    Thread.Sleep(1000);
                }

                int addAdult = AdultValue - 1;
                for (int i = 1; i <= addAdult; i++)
                {
                    Browser.waitForElementToBeClickable(AdultsPlusBtn, 5);
                    AdultsPlusBtn.Click();
                    Thread.Sleep(1000);
                }
            }           
        }

        public void SelectKids1()
        {
            //Pages.PropertyHome.closeDealsSignUp();
            //Browser.waitForElementToDisplayed(totalGuestsFld, 5);
            Browser.waitForElementToBeClickable(totalGuestsFld, 5);
            checkInDate.Click();
            totalGuestsFld.Click();

            Browser.waitForElementToDisplayed(KidsPlusBtn, 5);
            KidsPlusBtn.Click();

        }

        String KidsAge = "//select[@name='kidsAges.";
        String KidsAge1 = "']";

        public void SelectKids(int KidsCount, String childAge)
        {
            //Pages.PropertyHome.closeDealsSignUp();
            //Browser.waitForElementToDisplayed(totalGuestsFld, 5);
            Browser.waitForElementToBeClickable(totalGuestsFld, 5);
            checkInDate.Click();
            totalGuestsFld.Click();
            
            String exstnfKidsCnt = KidsCntTxt.GetAttribute("value");
            //Browser.setValue(KidsCntTxt, exstnfKidsCnt);
            int kidsCnt = Int32.Parse(exstnfKidsCnt);

            if (kidsCnt == 0)
            {
                for (int i = 1; i <= KidsCount; i++)
                {
                    Browser.waitForElementToDisplayed(KidsPlusBtn, 5);
                    KidsPlusBtn.Click();
                    int minsI = i - 1;
                    String Age = Convert.ToString(minsI);
                    String AgeDrp = KidsAge + Age + KidsAge1;
                    IWebElement AgeDrpLoctr = KidsPlusBtn.FindElement(By.XPath(AgeDrp));

                    SelectElement KidsAgeDropdwon = new SelectElement(AgeDrpLoctr);
                    KidsAgeDropdwon.SelectByValue(childAge);
                    //*[@name='kidsAges.0']
                }
            }
            else 
            {
                for (int i = 1; i <= kidsCnt; i++)
                {
                    Thread.Sleep(1000);
                    Browser.waitForElementToDisplayed(KidsMinusBtn, 5);
                    KidsMinusBtn.Click();
                    Thread.Sleep(1000);
                }

                for (int i = 1; i <= KidsCount; i++)
                {
                    Browser.waitForElementToDisplayed(KidsPlusBtn, 5);
                    KidsPlusBtn.Click();
                    int minsI = i - 1;
                    String Age = Convert.ToString(minsI);
                    String AgeDrp = KidsAge + Age + KidsAge1;
                    IWebElement AgeDrpLoctr = KidsPlusBtn.FindElement(By.XPath(AgeDrp));

                    SelectElement KidsAgeDropdwon = new SelectElement(AgeDrpLoctr);
                    KidsAgeDropdwon.SelectByValue(childAge);
                    //*[@name='kidsAges.0']
                }
            }
        }

        public void SelectKids1Age(int KidsAgeValue)
        {
            Pages.PropertyHome.closeDealsSignUp();
            String kids1Age = Convert.ToString(KidsAgeValue);
            Browser.waitForElementToDisplayed(Kids1AgeDpDn, 5);
            SelectElement KidsAgeDropdwon = new SelectElement(Kids1AgeDpDn);
            KidsAgeDropdwon.SelectByValue(kids1Age);
        }

        public void SelectKids2Age(int KidsAgeValue)
        {
            Pages.PropertyHome.closeDealsSignUp();
            String kids2Age = Convert.ToString(KidsAgeValue);
            Browser.waitForElementToDisplayed(Kids2AgeDpDn, 5);
            SelectElement KidsAgeDropdwon = new SelectElement(Kids2AgeDpDn);
            KidsAgeDropdwon.SelectByValue(kids2Age);
        }

        public void enterOfferCodeTextbox(String offerCode)
        {
            //Pages.PropertyHome.closeDealsSignUp();
            checkInDate.Click();
            Browser.waitForElementToDisplayed(OfferCodeFld, 5);
            Browser.clearTextboxValueACtion(OfferCodeFld);
            OfferCodeFld.SendKeys(offerCode);
        }

        public void clickBookNowBtn()
        {
            Pages.PropertyHome.closeDealsSignUp();
            Browser.waitForElementToBeClickable(bookNowBtn, 10);
            bookNowBtn.Click();
            Browser.scrollVerticalBy("400");
        }

        public void clickCookiesClose()
        {
            if (CoockieCLose.Exists())
            {
                Browser.waitForElementToBeClickable(CoockieCLose, 10);
                CoockieCLose.Click();
            }
        }
        

        public Boolean suitepageDisplayed()
        {
            Browser.waitForElementToDisplayed(suiteRoom, 30);
            bool SuiteRoom = suiteRoom.Displayed;
            return SuiteRoom;
        }

        public void navigateSuiteToDinignPageAndVerifyPckgCostandTitle()
        {
            Browser.waitForElementToBeClickable(Pages.Suite.selectBtn, 20);
            if (Pages.Suite.qualifyingIDTx.Exists())
            {
                Pages.Suite.qualifyingIDTx.SendKeys("123");
            }
            selectBtn.Click();

            Thread.Sleep(3000);
            //Browser.waitForElementToBeClickable(LCONoThanksBtn, 10);
            if (lcoPopup.Exists())
            {
                LCONoThanksBtn.Click();
            }

            Browser.waitForElementToBeClickable(continueToDiningPageBtn, 20);
            continueToDiningPageBtn.Click();
            getDiningPckgsTitleAndCost();
          
        }

        public void navigateSuiteToActivityBirthdayAndVerifyPckgCostandTitle()
        {
            Browser.waitForElementToBeClickable(Pages.Suite.selectBtn, 20);
            if (Pages.Suite.qualifyingIDTx.Exists())
            {
                Pages.Suite.qualifyingIDTx.SendKeys("123");
            }
            selectBtn.Click();

            Thread.Sleep(3000);
            //Browser.waitForElementToBeClickable(LCONoThanksBtn, 10);
            if (lcoPopup.Exists())
            {
                LCONoThanksBtn.Click();
            }
            Pages.Activities.navigateToBirthdayTab();
            Pages.Activities.getBirthdayPckgsTitleAndCost();
        }

        public void navigateSuiteToPayment()
        {

            Browser.waitForElementToBeClickable(Pages.Suite.selectBtn, 20);
            if (Pages.Suite.qualifyingIDTx.Exists())
            {
                Pages.Suite.qualifyingIDTx.SendKeys("123");
            }
            selectBtn.Click();

            Thread.Sleep(3000);
            //Browser.waitForElementToBeClickable(LCONoThanksBtn, 10);
            if (lcoPopup.Exists())
            {
                LCONoThanksBtn.Click();
            }

            Browser.waitForElementToBeClickable(continueToDiningPageBtn, 20);
            continueToDiningPageBtn.Click();
            Browser.opt.AddArgument("--disable - geolocation");
            Browser.opt.AddArguments("disable-geolocation");

            Browser.waitForElementToBeClickable(continueToPaymentPageBtn, 20);
            Browser.opt.AddArgument("--disable - geolocation");
            Browser.opt.AddArguments("disable-geolocation");
            continueToPaymentPageBtn.Click();
            Browser.opt.AddArgument("--disable - geolocation");
            Browser.opt.AddArguments("disable-geolocation");
            Browser.scrollVerticalBy("500");
        }

        public void navigateSuiteToPaymentWithSuiteOption()
        {
            //Thread.Sleep(2000);
            clickSelectRoomBtnSuiteOption();
            
            Thread.Sleep(4000);
            if (lcoPopup.Exists())
            {
                LCONoThanksBtn.Click();
            }

            Browser.waitForElementToBeClickable(continueToDiningPageBtn, 20);
            continueToDiningPageBtn.Click();

            Browser.waitForElementToBeClickable(continueToPaymentPageBtn, 20);
            Browser.opt.AddArgument("--disable - geolocation");
            Browser.opt.AddArguments("disable-geolocation");
            continueToPaymentPageBtn.Click();
            Browser.opt.AddArgument("--disable - geolocation");
            Browser.opt.AddArguments("disable-geolocation");

        }

        public void navigateSuiteToPaymentWithPcksgs()
        {
            Browser.waitForElementToBeClickable(Pages.Suite.selectBtn, 20);
            if (Pages.Suite.qualifyingIDTx.Exists())
            {
                Pages.Suite.qualifyingIDTx.SendKeys("123");
            }
            selectBtn.Click();

            Thread.Sleep(3000);
            //Browser.waitForElementToBeClickable(LCONoThanksBtn, 10);
            if (lcoPopup.Exists())
            {
                LCONoThanksBtn.Click();
            }

            Browser.waitForElementToBeClickable(continueToDiningPageBtn, 20);
            continueToDiningPageBtn.Click();

            Pages.Dining.addmutilpleDiningPackage();

            Browser.waitForElementToBeClickable(continueToPaymentPageBtn, 20);
            continueToPaymentPageBtn.Click();
        }

        public void navigateSuiteToPaymentWithchoices(String LCO, String Activity, String Dining)
        {
            Browser.waitForElementToBeClickable(Pages.Suite.selectBtn, 20);
            if (Pages.Suite.qualifyingIDTx.Exists())
            {
                Pages.Suite.qualifyingIDTx.SendKeys("123");
            }
            Pages.rateCalendar.getSuiteName();
            selectBtn.Click();

            Thread.Sleep(3000);
            //Browser.waitForElementToBeClickable(LCONoThanksBtn, 10);
            if(LCO == "Add LCO If Exists") { 
                if (lcoPopup.Exists())
                {
                    lcoPopupYesAddLCOBtn.Click();
                    LCOExist = true;
                }
            }
            else
            {
                if (lcoPopup.Exists())
                {
                    LCONoThanksBtn.Click();
                    LCOExist = true;
                }
            }

            if (Activity == "Add Pass")
            {
                getActivityPassTitleAndCost();
                Pages.Activities.clickOnPackageAddButton();
                Browser.waitForElementToBeClickable(continueToDiningPageBtn, 20);
                continueToDiningPageBtn.Click();
            }
            else {

                getActivityPassTitleAndCost();
                Browser.waitForElementToBeClickable(continueToDiningPageBtn, 20);
                continueToDiningPageBtn.Click();
            }

            if (Dining == "Add Dining")
            {
                getDiningPckgsTitleAndCost();

                Pages.Dining.addmutilpleDiningPackage();
                Browser.waitForElementToBeClickable(continueToPaymentPageBtn, 20);
                Browser.opt.AddArgument("--disable - geolocation");
                Browser.opt.AddArguments("disable-geolocation");
                continueToPaymentPageBtn.Click();
            }
            else
            {
                getDiningPckgsTitleAndCost();
                Browser.waitForElementToBeClickable(continueToPaymentPageBtn, 20);
                Browser.opt.AddArgument("--disable - geolocation");
                Browser.opt.AddArguments("disable-geolocation");
                continueToPaymentPageBtn.Click();
            }            

        }

        public void getActivityPassTitleAndCost()
        {
            if (Pages.Activities.PupPassPckgTitle.Exists())
            {
                PupPassTitle = Browser.getText(Pages.Activities.PupPassPckgTitle);
                PupPassCost = Browser.getText(Pages.Activities.PupPassPckgCost);
            }

            if (Pages.Activities.PawPassPckgTitle.Exists())
            {
                PawPassTitle = Browser.getText(Pages.Activities.PawPassPckgTitle);
                PawPassCost = Browser.getText(Pages.Activities.PawPassPckgCost);
            }

            if (Pages.Activities.WolfPassPckgTitle.Exists())
            {
                WolfPassTitle = Browser.getText(Pages.Activities.WolfPassPckgTitle);
                WolfPassCost = Browser.getText(Pages.Activities.WolfPassPckgCost);
            }
        }
               

        public void getDiningPckgsTitleAndCost() {
            if (Pages.Dining.fisrtDiningPckgTitle.Exists())
            {
                DiningFirstPckgAvl = true;
                DiningFirstPckgTitl = Browser.getText(Pages.Dining.fisrtDiningPckgTitle);
                DiningFirstPckgTitlCost = Browser.getText(Pages.Dining.firstPackageCost);
            }

            if (Pages.Dining.secondDiningPckgTitle.Exists())
            {
                DiningScndPckgAvl = true;
                DiningSecondPckgTitl = Browser.getText(Pages.Dining.secondDiningPckgTitle);
                DiningSecondPckgTitlCost = Browser.getText(Pages.Dining.secondPackageCost);
            }

            if (Pages.Dining.thirdDiningPckgTitle.Exists())
            {
                DiningThirdPckgAvl = true;
                DiningThirdPckgTitl = Browser.getText(Pages.Dining.thirdDiningPckgTitle);
                DiningThirdPckgTitlCost = Browser.getText(Pages.Dining.thirdPackageCost);                
                //DiningThirdPckgTitlCost = Browser.getText(Pages.Dining.personPerdayTotalCost);
            }
        }



        public void navigateSuiteToPaymentWithLCOAndOtherPcksgs()
        {
            Browser.waitForElementToBeClickable(Pages.Suite.selectBtn, 20);
            if (Pages.Suite.qualifyingIDTx.Exists())
            {
                Pages.Suite.qualifyingIDTx.SendKeys("123");
            }
            selectBtn.Click();

            Thread.Sleep(3000);
            //Browser.waitForElementToBeClickable(LCONoThanksBtn, 10);
            if (lcoPopup.Exists())
            {
                lcoPopupYesAddLCOBtn.Click();
                LCOExist = true;
            }

            Browser.waitForElementToBeClickable(continueToDiningPageBtn, 20);
            continueToDiningPageBtn.Click();

            Pages.Dining.addmutilpleDiningPackage();

            Browser.waitForElementToBeClickable(continueToPaymentPageBtn, 20);
            continueToPaymentPageBtn.Click();

        }

        public Boolean checkincludeWithYourStaySection()
        {
            Browser.waitForElementToDisplayed(includeWithYourStaySectn, 20);
            return includeWithYourStaySectn.Exists();
        }

        public Boolean checkinStayWaterParkPassesSection()
        {
            return waterParkPassesSectn.Exists();
        }

        public Boolean checkinAccessToWaterParkSection()
        {
            return accessToWaterParkSectn.Exists();
        }

        public Boolean checkinKidsActivitiesSection()
        {
            return kidsActivitiesSectn.Exists();
        }

        public Boolean checkinUnlimitedWifiSection()
        {
            return unlimitedWifiSectn.Exists();
        }

        public void clickOnStandardTab()
        {
            //Thread.Sleep(15000);
            Browser.waitForElementToBeInvisible(10, SignInPage.signBtnStr);
            //Browser.waitForElementToDisplayed(standardTab, 60);
            
            for (int i = 1; i <= 2; i++)
            {
                Browser.waitForElementToBeClickable(standardTab, 20);
                Browser.javaScriptClick(standardTab);
                //standardTab.Click();
            }
            
        }

        public void clickSelectRoomBtn()
        {
            //Thread.Sleep(2000);
            //Browser.waitForElementToDisplayed(selectBtn, 180);
            Browser.waitForElementToBeClickable(Pages.Suite.selectBtn, 20);
            if (Pages.Suite.qualifyingIDTx.Exists())
            {
                Pages.Suite.qualifyingIDTx.SendKeys("123");
            }
            selectBtn.Click();
        }

        public void clickSelectRoomBtnSuiteOption()
        {
            Thread.Sleep(2000);
            //Browser.waitForElementToDisplayed(selectBtnSuiteOption, 90);
            Browser.waitForElementToBeClickable(selectBtnSuiteOption, 20);
            Browser.waitForElementToBeClickable(Pages.Suite.selectBtn, 20);
            if (Pages.Suite.qualifyingIDTx.Exists())
            {
                Pages.Suite.qualifyingIDTx.SendKeys("123");
            }
            selectBtnSuiteOption.Click();
        }

        public Boolean verifyLCOTitle()
        {
            if (lcoPopupTitle.Exists())
            {
                return true;
            }
            else {
                return true;
            }
        }

        public Boolean verifyLCOPopupDescrptn()
        {
            //return lcoPopupDescrptn.Exists();
            if (lcoPopupDescrptn.Exists())
            {
                return true;
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyLCOPopupBtn()
        {
            //return lcoPopupYesAddLCOBtn.Exists();
            if (lcoPopupYesAddLCOBtn.Exists())
            {
                return true;
            }
            else
            {
                return true;
            }
        }
        
        public Boolean verifyLCOPopupNoThanksBtn()
        {
            //return lcoPopupNoThanksBtn.Exists();
            if (lcoPopupNoThanksBtn.Exists())
            {
                return true;
            }
            else
            {
                return true;
            }
        }

        public void clickONLCOPopupNoThanksBtn()
        {
            //Thread.Sleep(3000);
            Browser.waitForImplicitTime(5);
            if (lcoPopupNoThanksBtn.Exists()) {
                Browser.waitForElementToBeClickable(lcoPopupNoThanksBtn, 20);
                lcoPopupNoThanksBtn.Click();
                Pages.rateCalendar.waitForSuitelSprinnerOut(RateCalendar.waitSpinnerStr);
            }
        }

        public void clickONLCOPopupAddChecOtBtn()
        {
            //Thread.Sleep(3000);
            if (lcoPopup.Exists())
            {
                //lcoPopupYesAddLCOBtn.Click();
                LCOExist = true;
            }
            if (lcoPopupYesAddLCOBtn.Exists())
            {                
                Browser.waitForElementToBeClickable(lcoPopupYesAddLCOBtn, 20);
                lcoPopupYesAddLCOBtn.Click();
                Pages.rateCalendar.waitForSuitelSprinnerOut(RateCalendar.waitSpinnerStr);                
            }
        }

        public Boolean verifyLCOPopupPriceBtn()
        {
            if (lcoPopupTitle.Exists())
            {
                PopupPrice = Browser.getText(lcoPopupPrice);
                String ThemeSuitePrice = "+ $84.99";
                //if (PopupPrice.Equals(ThemeSuitePrice)||PopupPrice.Equals("+ $79.99")||PopupPrice.Equals("+ $89.99"))
                if (lcoPopupPrice.Exists())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
            
        }

        public void observeLcoPrice() {
            if (lcoPopupPrice.Exists())
            {
                Browser.waitForElementToDisplayed(lcoPopupPrice, 20);
                String PopupPrice1 = Browser.getText(lcoPopupPrice);
                String PopupPrice2 = PopupPrice1.Trim('+');
                PopupPrice = PopupPrice2.Trim(' ');
            }
        }

        public Boolean verifyStandardTab()
        {

            return standardTab.Exists();
        }
        public Boolean verifySuiteTitle()
        {
            return premiumSuiteTitle.Exists();
        }

        public Boolean verifyBaseAndMaxOccptnSectn()
        {
            return baseMaxOccuptnSectn.Exists();

        }

        /*public Boolean verifyViewDetailsLink()
        {
            return viewDetailsLink.Exists();
        }*/
        public Boolean verifyRateCalLink()
        {
            return chkRateCalLink.Exists();
        }
        public Boolean verifyPriceSection()
        {
            return suitePriceSectn.Exists();
        }

        public Boolean verifyPremiumTab()
        {
            return premiumTab.Exists();
        }

        public Boolean popUpHandle()
        {
            if (lcoPopup.Exists())
            {
                return true;
            }
            else
            {
                return true;
            }
        }

        public void clickOnPremiumTab()
        {
            Browser.waitForElementToBeInvisible(10, SignInPage.signBtnStr);
           
            for (int i = 1; i <= 2; i++)
            {
                Browser.waitForElementToBeClickable(premiumTab, 20);
                Browser.javaScriptClick(premiumTab);
            }
        }

        //Ishwars function
        public Boolean themedtabDisplayedDefault()
        {
            Browser.waitForElementToDisplayed(themedtab, 20);
            bool ThemedTab = themedtab.Displayed;
            return ThemedTab;
        }

        public Boolean suiteTitleDisplay()
        {
            Browser.waitForElementToDisplayed(suiteTitle, 20);
            bool SuiteTitle = suiteTitle.Displayed;
            return SuiteTitle;
        }

        /*public void baseMaxsection()
        {
            Browser.waitForElementToDisplayed(basemaxsection, 30);
            String Section = Browser.getText(basemaxsection);
            Assert.AreEqual("Base", Section.Contains("Base"));
            Assert.AreEqual("Max", Section.Contains("Max"));
        }*/

        public Boolean suiteDescriptiondiscplay()
        {
            Browser.waitForElementToDisplayed(suitedescription, 20);
            bool SuiteDescription = suitedescription.Displayed;
            return SuiteDescription;
        }
        public Boolean checkRateCalendarlink()
        {
            Browser.waitForElementToDisplayed(checkratecalendarlink, 20);
            bool CheckRateCalendar = checkratecalendarlink.Displayed;
            return CheckRateCalendar;
        }
        public Boolean suitePricesSectionDisplay()
        {
            Browser.waitForElementToDisplayed(suitepricesection, 20);
            bool SuitePriceSection = suitepricesection.Displayed;
            return SuitePriceSection;
        }
        public Boolean appliedOfferCodeDisplay()
        {
            Browser.waitForElementToDisplayed(appliedoffercode, 20);
            bool AppliedOfferCode = appliedoffercode.Displayed;
            return AppliedOfferCode;
        }
        public Boolean bestAvailableRateDisplayed()
        {
            Browser.waitForElementToDisplayed(BBARSuite, 10);

            if (BBARSuite.Exists())
            { return false; }
            else { return true; }
            
        }

        public void selectAccessibleroomsonly()
        {
            Browser.waitForElementToDisplayed(selectguestbox, 20);
            selectguestbox.Click();
            Thread.Sleep(3000);
            if (togglebbtn.Exists())
            {
                togglebbtn.Click();
            }
            else {
                selectguestbox.Click();
                Thread.Sleep(3000);
                togglebbtn.Click();
            }
        }
        public void updateYourStay()
        {
            Browser.waitForElementToDisplayed(updateBtn, 20);
            for(int i=1; i <= 2; i++) { 
                updateBtn.Click();
            }
        }
        public Boolean accessibleLabelTagDisplay()
        {
            //Thread.Sleep(4000);
            Browser.scrollVerticalBy("300");
            Browser.waitForElementToDisplayed(accesibletagintosuite, 20);
            bool AccessibleTag = accesibletagintosuite.Displayed;
            return AccessibleTag;
        }

        public void maxOccupancy()
        {
            Browser.waitForElementToDisplayed(maxOccupanySection, 20);
            Boolean MaxOccupancy = maxOccupanySection.Displayed;
            Assert.IsTrue(MaxOccupancy, "Max Occupancy is display");

        }
        public void selectGuestMoreThanMaxOccupancy()
        {
            Browser.waitForElementToDisplayed(maxOccupanySection, 20);
            Boolean MaxOccupancy = maxOccupanySection.Displayed;
            Assert.IsTrue(MaxOccupancy, "Max Occupancy is display");
        }
        public Boolean exceededMaxOccupancyalert()
        {
            //Browser.waitForElementToDisplayed(themedTab, 30);
            /*Browser.waitForElementToBeClickable(themedTab, 30);
            for(int i = 1; i<= 2; i++)
            {
                themedTab.Click();
            }*/
                        
            //Thread.Sleep(2000);
            if (exceedMaxOccupancyalert.Exists())
            {
                //Browser.waitForElementToDisplayed(exceedMaxOccupancyalert, 30);
                bool ExceedMaxOccupanyalert = exceedMaxOccupancyalert.Displayed;
                return ExceedMaxOccupanyalert;
            }
            else {
                return true;
            }
        }

        public Boolean suitePriceSection()
        {
            bool SuitePriceSection = bestAvailableRate.Displayed;
            return SuitePriceSection;
        }
        
 //Atuls functions
        public void clickUpdateyourStayBtn()
        {
            //Thread.Sleep(10000);
            Browser.waitForElementToBeClickable(updateyourstayBtn, 20);
            for(int i = 1; i <= 3; i++)
            {
                //Browser.waitForElementToDisplayed(updateyourstayBtn, 30);
                Browser.waitForElementToBeClickable(updateyourstayBtn, 20);
                updateyourstayBtn.Click();
            }
            
            //Thread.Sleep(5000);
           // updateyourstayBtn.Click();
            //Thread.Sleep(15000);
        }
        public void clickThemedTab()
        {
            Browser.waitForElementToBeClickable(ThemedTab, 20);
            ThemedTab.Click();
        }

        public Boolean exceededMaximumOccupancyDisplayed()
        {
            Browser.waitForElementToDisplayed(suiteExceedMaxOccupanyMsg, 15);
            if (suiteExceedMaxOccupanyMsg.Exists())
            {
                bool ExceedMaxOccupancy = suiteExceedMaxOccupanyMsg.Displayed;
                return ExceedMaxOccupancy;
            }
            else {
                return true;
            }
        }

        public Boolean notAvailablelabelDisplayed()
        {
            Browser.waitForElementToDisplayed(notAvailableLabel, 10);
            bool NotAvailableLabel = notAvailableLabel.Displayed;
            return NotAvailableLabel;
        }

        public void clickOfferCodeErrorCloseIcon()
        {
            Browser.waitForElementToBeClickable(offerCodeErrorCloseIcon, 20);
            offerCodeErrorCloseIcon.Click();
        }

        public Boolean BBARSuiteDisplayed()
        {
                if (BBARSuite.Exists())
                {
                    return false;
                }
                else
                {
                    return true;
                }           
        }

        public void selectCheckInWeekday()
        {
            String checkIn = DataAccess.getData(5, 2);
            Browser.waitForElementToBeClickable(checkInDate, 20);
            checkInDate.SendKeys(checkIn);
        }

        public void selectCheckOutWeekday()
        {
            String checkOut = DataAccess.getData(6, 2);
            Browser.waitForElementToBeClickable(checkOutDate, 10);
            checkOutDate.Clear();
            checkOutDate.SendKeys(checkOut);
        }

        public void selectCheckInLongWeekday()
        {
            String checkIn = DataAccess.getData(7, 2);
            Browser.waitForElementToBeClickable(checkInDate, 20);
            checkInDate.SendKeys(checkIn);
        }

        public void selectCheckOutLongWeekday()
        {
            String checkOut = DataAccess.getData(8, 2);
            Browser.waitForElementToBeClickable(checkOutDate, 10);
            checkOutDate.Clear();
            checkOutDate.SendKeys(checkOut);
        }

        public void selectCheckInWeekend()
        {
            String checkIn = DataAccess.getData(9, 2);
            Browser.waitForElementToBeClickable(checkInDate, 20);
            checkInDate.SendKeys(checkIn);
        }

        public void selectCheckOutWeekend()
        {
            String checkOut = DataAccess.getData(10, 2);
            Browser.waitForElementToBeClickable(checkOutDate, 10);
            checkOutDate.Clear();
            checkOutDate.SendKeys(checkOut);
        }

        public void waitforCheckin()
        {
            Browser.waitForElementToBeClickable(checkInDate, 20);
        }

        public void selectCheckInforDining()
        {
            String checkIn = DataAccess.getData(11, 2);
            Pages.PropertyHome.closeDealsSignUp();
            clickCookiesClose();
            Browser.waitForElementToBeClickable(checkInDate, 20);
            checkInDate.Clear();
            Thread.Sleep(2000);
            checkInDate.SendKeys(checkIn);
        }

        public void selectCheckOutDining()
        {
            String checkOut = DataAccess.getData(12, 2);
            Browser.waitForElementToBeClickable(checkOutDate, 20);
            checkOutDate.Clear();
            Browser.clearTextboxValueACtion(checkOutDate);
            //Thread.Sleep(2000);
            checkOutDate.SendKeys(checkOut);
        }

        public void clickToOpenGuestSection()
        {
            //Browser.waitForElementToDisplayed(totalGuestsFld, 5);
            Browser.waitForElementToBeClickable(totalGuestsFld, 5);
            OfferCodeFld.Click();
            totalGuestsFld.Click();
        }

        public void decreamentAdults(int AdultValue)
        {
            //Browser.waitForElementToDisplayed(totalGuestsFld, 5);
            Browser.waitForElementToBeClickable(totalGuestsFld, 5);
            OfferCodeFld.Click();
            totalGuestsFld.Click();

            for (int i = 1; i <= AdultValue; i++)
            {
                Browser.waitForElementToBeClickable(AdultsMinusBtn, 5);
                AdultsMinusBtn.Click();
            }

            OfferCodeFld.Click();
        }

        public void clearGuestsTextbox()
        {
            Browser.waitForElementToDisplayed(totalGuestsFld, 5);
            Browser.clearTextboxValue(totalGuestsFld);
        }

        public void decrementKids(int KidsCount)
        {
            
            //Browser.waitForElementToDisplayed(totalGuestsFld, 5);
            Browser.waitForElementToBeClickable(totalGuestsFld, 5);
            //OfferCodeFld.Click();
            CheckinField.Click();
            totalGuestsFld.Click();

            for (int i = 1; i <= KidsCount; i++)
            {
                Browser.waitForElementToBeClickable(KidsMinusBtn, 5);
                KidsMinusBtn.Click();
            }
            CheckinField.Click();
        }

        public void clearOfferCodeTextbox()
        {
            Browser.waitForElementToDisplayed(OfferCodeFld, 5);
            Browser.clearTextboxValue(OfferCodeFld);
        }

        public void getCheckInDateText()
        {
            Browser.waitForElementToDisplayed(checkInDate, 20);
            checkIndtTxt = Browser.getTextboxValue(checkInDate);
            //return checkIn;
        }

        public void getCheckOutDateText()
        {
            Browser.waitForElementToDisplayed(checkOutDate, 20);
            checkOutdtTxt = Browser.getTextboxValue(checkOutDate);
            //return checkOut;
        }

        public string getGuestsText()
        {
            String guests = Browser.getTextboxValue(totalGuestsFld);
            return guests;
        }

        public string getOfferCodeText()
        {
            String offerCode = Browser.getTextboxValue(OfferCodeFld);
            return offerCode;
        }

        public void selectSUiteOption(String suiteOption)
        {
            Browser.scrollVerticalBy("300");
            Browser.waitForElementToBeClickable(suiteOptionDrp, 20);
            SelectElement oSelect = new SelectElement(suiteOptionDrp);
            //oSelect.SelectByValue(suiteOption);
            oSelect.SelectByIndex(1);
            SuiteOptionVal = suiteOptionDrpValue.Text;
        }

        //Atul 
        public void observeGuestsCount()
        {
            String GuestText = Pages.Suite.getGuestsText();
            String GuestValue = GuestText.Substring(0, 1);
            GuestCount = Int32.Parse(GuestValue);
        }

        public void observeStayNights()
        {
            String checkIndate = Browser.getSpecificCharactersFromString(Pages.PropertyHome.checkInDateTextbox, 3, 2);
            int CheckInDate = Int32.Parse(checkIndate);
            String checkOutdate = Browser.getSpecificCharactersFromString(Pages.PropertyHome.checkOutDateTextbox, 3, 2);
            int CheckOutDate = Int32.Parse(checkOutdate);
            StayNights = CheckOutDate - CheckInDate;
        }

        public void getSuiteWasPrice()
        {
            Browser.waitForElementToBeClickable(suiteWasValue,20);
            suiteWasPrice = Browser.stringToDoubleConvert1(suiteWasValue, 4);
        }

        public void getAdultsCount()
        {
            AdultsCount = Browser.getTextboxValue(adultsCount);
        }

        public void getKidsCount()
        {
            KidsCount = Browser.getTextboxValue(kidsCount);
        }

        public void getLCOPrice()
        {
            LCOPrice = Browser.stringToDoubleConvert1(lcoPopupPrice, 3);
        }

        public void selectCheckInSoldOutSuite()
        {
            String checkIn = DataAccess.getData(13, 2);
            Browser.waitForElementToBeClickable(checkInDate, 20);
            clickCookiesClose();
            checkInDate.Clear();
            checkInDate.SendKeys(checkIn);
        }
        public void selectCheckOutSoldOutSuite()
        {
            String checkOut = DataAccess.getData(14, 2);
            Browser.waitForElementToBeClickable(checkOutDate, 10);
            clickCookiesClose();
            checkOutDate.Clear();
            checkOutDate.SendKeys(checkOut);
        }

        public void navigateSuiteToActivitiesWithLCO()
        {
            Browser.waitForElementToBeClickable(Pages.Suite.selectBtn, 20);
            if (qualifyingIDTx.Exists())
            {
                qualifyingIDTx.SendKeys("123");
            }
            Pages.Suite.selectBtn.Click();
            Thread.Sleep(3000);
            if (Pages.Suite.lcoPopup.Exists())
            {
                LCOExist = true;
                Browser.waitForElementToDisplayed(Pages.Suite.lcoPopupPrice, 20);
                Pages.Suite.getLCOPrice();
                //Browser.waitForElementToDisplayed(Pages.Suite.lcoPopupYesAddLCOBtn, 30);
                Browser.waitForElementToBeClickable(Pages.Suite.lcoPopupYesAddLCOBtn, 20);
                Pages.Suite.lcoPopupYesAddLCOBtn.Click();
                Pages.rateCalendar.waitForSuitelSprinnerOut(RateCalendar.waitSpinnerStr);
            }
        }

        public void navigateSuiteToActivitiesWithoutLCO()
        {
            Browser.waitForElementToBeClickable(Pages.Suite.selectBtn, 20);
            Browser.waitForImplicitTime(5);
            //Browser.waitForElementToBeClickable(qualifyingIDTx, 5);
            if (qualifyingIDTx.Exists())
            {
                qualifyingIDTx.SendKeys("123");
            }
            Pages.Suite.selectBtn.Click();
            Thread.Sleep(3000);
            if (Pages.Suite.lcoPopup.Exists())
            {
                //Browser.waitForElementToDisplayed(Pages.Suite.LCONoThanksBtn, 30);
                Browser.waitForElementToBeClickable(Pages.Suite.LCONoThanksBtn, 20);
                Pages.Suite.LCONoThanksBtn.Click();
                Pages.rateCalendar.waitForSuitelSprinnerOut(RateCalendar.waitSpinnerStr);                
            }
        }

        public void clickBookNowBtnORupdateyourStay()
        {
            Pages.PropertyHome.closeDealsSignUp();
            if (bookNowBtn.Exists())
            {
                Browser.waitForElementToBeClickable(bookNowBtn, 20);
                bookNowBtn.Click();
            }
            if (updateBtn.Exists())
            {
                Browser.waitForElementToBeClickable(updateBtn, 20);
                updateBtn.Click();
            }
            Thread.Sleep(2000);
        }

        public Boolean verfiyAppliedOfferCodeSuiteDisplayed(String OfferCode)
        {
            //Browser.waitForElementToDisplayed(selectBtn, 180);
            //Browser.waitForElementToBeClickable(selectBtn, 180);
            if (selectBtn.Exists())
            {

                if (qualifyingIDTx.Exists())
                {
                    qualifyingIDTx.SendKeys("123");
                }
                Browser.waitForElementToDisplayed(appliedOffercodeLable, 20);
                String AppliedOffercodeLable = Browser.getText(appliedOffercodeLable);
                bool AppliedOfferCode = AppliedOffercodeLable.Equals(OfferCode);
                return AppliedOfferCode;
            }
            else
            {
                Browser.waitForElementToDisplayed(selectBtn, 20);

                if (qualifyingIDTx.Exists())
                {
                    qualifyingIDTx.SendKeys("123");
                }
                Browser.waitForElementToDisplayed(appliedOffercodeLable, 20);
                String AppliedOffercodeLable = Browser.getText(appliedOffercodeLable);
                bool AppliedOfferCode = AppliedOffercodeLable.Equals(OfferCode);
                return AppliedOfferCode;
            }
        }

        public Boolean standardtabDisplayed()
        {
            //   Browser.waitForElementToDisplayed(standardtab, 30);
            bool StandardTab = standardtab.Exists();
            return StandardTab;
        }
        public Boolean premiumtabDisplayed()
        {
         //   Browser.waitForElementToDisplayed(premiumtab, 30);
            bool PremiumTab = premiumtab.Exists();
            return PremiumTab;
        }

        public Boolean VerifyBBRratetext()
        {
            Browser.waitForElementToDisplayed(bestAvailableRate, 20);
            bool BestAvailableRate = bestAvailableRate.Displayed;
            return BestAvailableRate;
        }

        public void VerifySuitePriceBefore()
        {
            Browser.waitForElementToBeClickable(Suiteprice, 30);
            SuitePriceBefore = Suiteprice.Text;

        }

        public Boolean VerifySuitePriceBeforeAfterIsSame()
        {
            Browser.waitForElementToBeClickable(Suiteprice, 20);
            Pages.rateCalendar.getSuiteName1();

            if (RateCalendar.SuiteTitle == RateCalendar.SuiteTitle1)
            {
                String SuitePriceAfter = Suiteprice.Text;

                if (SuitePriceAfter.Equals(SuitePriceBefore))
                {
                    return true;
                }
                return false;
            }
            else
            {
                return true;
            }
        }

        public void clickOnRoomTypeFilter()
        {
            Browser.waitForElementToBeClickable(roomTypeFilterDrp, 10);
            Browser.javaScriptClick(roomTypeFilterDrp);
        }

        public void clickOnCloseRoomTypeFilter()
        {
            Browser.waitForElementToBeClickable(CloseLinkPopUp, 10);
            Browser.javaScriptClick(CloseLinkPopUp);
        }

        public void selectRoomType(String Themed, String Premium, String Standard, String Accessible) {
            Browser.waitForElementToBeClickable(roomTypeFilterDrp, 10);
            //Browser.scrollToAnElement(roomTypeFilterDrp);
            //roomTypeFilterDrp.Click();
            Browser.javaScriptClick(roomTypeFilterDrp);

            Browser.scrollToAnElement(ThemedCheckBx);
            Browser.waitForElementToBeClickable(ThemedCheckBx, 10);
            if (Themed == "Theme") {
                Browser.javaScriptClick(ThemedCheckBx);
                //ThemedCheckBx.Click();
            }

            if(Premium == "Premium"){
                Browser.javaScriptClick(PremiumCheckBx);
                //PremiumCheckBx.Click();
            }

            if (Standard == "Standard")
            {
                Browser.javaScriptClick(StandardCheckBx);
                //StandardCheckBx.Click();
            }

            if(Accessible == "Yes")
            {
                Browser.javaScriptClick(accessibleToggleON);
                //accessibleToggleON.Click();
            }

            Browser.waitForElementToBeClickable(CloseLinkPopUp, 10);
            Browser.javaScriptClick(CloseLinkPopUp);
            //CloseLinkPopUp.Click();
        }

        public Boolean verifyThemeRoomTypeOption()
        {
            Browser.waitForElementToBeClickable(ThemedCheckBx, 10);
            return ThemedCheckBx.Exists();
        }

        public Boolean verifyThemeRoomTypeOptionShouldNotDisplayed()
        {
            //Browser.waitForElementToBeClickable(ThemedCheckBx, 10);
            if (ThemedCheckBx.Exists())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean verifyPremiumRoomTypeOption()
        {
            Browser.waitForElementToBeClickable(PremiumCheckBx, 10);
            return PremiumCheckBx.Exists();
        }

        public Boolean verifyStandardRoomTypeOption()
        {
            Browser.waitForElementToBeClickable(StandardCheckBx, 10);
            return StandardCheckBx.Exists();
        }

        public Boolean verifyThemedLabelOnSuite()
        {
            Browser.waitForElementToBeClickable(ThemedLableOnSuite, 10);
            return ThemedLableOnSuite.Exists();
        }

        public Boolean verifyStandardLabelOnSuite()
        {
            Browser.waitForElementToBeClickable(StandardLableOnSuite, 10);
            return StandardLableOnSuite.Exists();
        }

        public Boolean verifyPremiumLabelOnSuite()
        {
            Browser.waitForElementToBeClickable(PremiumLableOnSuite, 10);
            return PremiumLableOnSuite.Exists();
        }

        public Boolean verifyLowestToHighestSortOption()
        {
            Browser.waitForElementToBeClickable(LowestToHighestSortOption, 10);
            return LowestToHighestSortOption.Exists();
        }

        public Boolean verifyHighestToLowestSortOption()
        {
            Browser.waitForElementToBeClickable(HighestToLowestSortOption, 10);
            return HighestToLowestSortOption.Exists();
        }

        public Boolean verifyRecommendedSortOption()
        {
            Browser.waitForElementToBeClickable(RecommendedSortOption, 10);
            return RecommendedSortOption.Exists();
        }

        public void clickOnSortOptionDrpDwn()
        {
            Browser.waitForElementToBeClickable(SortByFilterDrpDown, 10);
            Browser.javaScriptClick(SortByFilterDrpDown);
        }

        public void clickOnLowToHighOption()
        {
            Browser.waitForElementToBeClickable(LowestToHighestSortOption, 10);
            Browser.javaScriptClick(LowestToHighestSortOption);
        }

        public void clickOnHighToLowOption()
        {
            Browser.waitForElementToBeClickable(HighestToLowestSortOption, 10);
            Browser.javaScriptClick(HighestToLowestSortOption);
        }

        public void clickOnRecommendedOption()
        {
            Browser.waitForElementToBeClickable(RecommendedSortOption, 10);
            Browser.javaScriptClick(RecommendedSortOption);
        }

        public void clickOnClearFilterLink()
        {
            Browser.waitForElementToBeClickable(clearFilterLink, 10);
            Browser.javaScriptClick(clearFilterLink);
        }

        public Boolean clearFilterIsNotDisplayed()
        { 
            if(clearFilterLink.Exists())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public Boolean verifyRecommendedLabel()
        {
            Browser.waitForElementToBeClickable(recommenededLabel, 10);
            return recommenededLabel.Exists();
        }

        public Boolean verifyEsaverOfferCod()
        {
            Browser.waitForElementToBeClickable(esaverOfferCod, 10);
            return esaverOfferCod.Exists();
        }

        public Boolean verifyMoreFunOfferCod()
        {
            Browser.waitForElementToBeClickable(moreFunOfferCod, 10);
            return moreFunOfferCod.Exists();
        }

        public Boolean VerifyLowToHighSortOfSuite()
        { 
            if (FirstsuitePrice.Exists())
            {
                suitefirstPriceLow = Browser.stringToDecimalConvert(FirstsuitePrice);
            }

            if (SecondsuitePrice.Exists())
            {
                suiteSecndPriceLow = Browser.stringToDecimalConvert(SecondsuitePrice);
            }

            if (suitefirstPriceLow <= suiteSecndPriceLow)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean VerifyHighToLowSortOfSuite()
        {
            if (FirstsuitePrice.Exists())
            {
                suitefirstPriceLow = Browser.stringToDecimalConvert(FirstsuitePrice);
            }

            if (SecondsuitePrice.Exists())
            {
                suiteSecndPriceLow = Browser.stringToDecimalConvert(SecondsuitePrice);
            }

            if (suitefirstPriceLow >= suiteSecndPriceLow)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
