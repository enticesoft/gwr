﻿using GWRFramework.TestData;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWRFramework
{

    public class CreateAccountPage
    {
        [FindsBy(How = How.Id, Using = "firstName")]
        public IWebElement firstNameTextbox;

        [FindsBy(How = How.Id, Using = "lastName")]
        public IWebElement lastNameTextbox;

        [FindsBy(How = How.Id, Using = "zip")]
        public IWebElement postalCodeTextbox;

        [FindsBy(How = How.Id, Using = "Email")]
        public IWebElement emailIdTextbox;

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement passwordTextbox;

        [FindsBy(How = How.XPath, Using = "//button[contains(text(), 'Create')]")]
        public IWebElement createBtn;

        public static String waitCreateAccountSpinnerStr = "//*[@class='loading-indicator']";

        //Sprint 5 locators
        [FindsBy(How = How.XPath, Using = "//label[@for='canadaOptInChecked']//a[text()='Privacy Policy']")]
        public IWebElement PrivacyPolicyLink;

        [FindsBy(How = How.XPath, Using = "//label[@for='canadaOptInChecked']//a[text()='contact us']")]
        public IWebElement ContactUsLink;

        [FindsBy(How = How.XPath, Using = "//input[@name='canadaOptInChecked']")]
        public IWebElement CheckBoxWhenCanadaPostalCode;

        [FindsBy(How = How.XPath, Using = "//button[contains(text(), 'Sign In')]")]
        public IWebElement SignInLink;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Sign in with Google')]")]
        public IWebElement SignInWithGoogle;

        [FindsBy(How = How.XPath, Using = "//label[@for='canadaOptInChecked']//a[contains(.,'Privacy Policy')]")]
        public IWebElement canadianPrivacyPolicy;

        [FindsBy(How = How.XPath, Using = "//label[@for='canadaOptInChecked']//a[contains(.,'contact us')]")]
        public IWebElement canadianContactUs;
        

        public void enterFirstNameTextbox(String firstName)
        {
            //Browser.waitForElementToDisplayed(firstNameTextbox, 30);
            Browser.waitForElementToDisplayed(firstNameTextbox, 20);
            firstNameTextbox.SendKeys(firstName);
        }

        public void enterLastNameTextbox(String lastName)
        {
            Browser.waitForElementToDisplayed(lastNameTextbox, 2);
            lastNameTextbox.SendKeys(lastName);
        }

        public void enterPostalCodeTextbox()
        {
            String postalCode = DataAccess.getData(2, 2);
            Browser.waitForElementToDisplayed(postalCodeTextbox, 2);
            postalCodeTextbox.SendKeys(postalCode);
        }

        public void enterEmailIdTextbox(String emailId)
        {
            Browser.waitForElementToDisplayed(emailIdTextbox, 2);
            emailIdTextbox.SendKeys(emailId);
        }

        public void enterPasswordTextbox(String password)
        {
            Browser.waitForElementToDisplayed(passwordTextbox, 2);
            passwordTextbox.SendKeys(password);
        }

        public void clickCreateButton()
        {
            Browser.waitForElementToBeClickable(createBtn, 10);
            Browser.javaScriptClick(createBtn);
            //createBtn.Click();
            waitCreateAccountSprinnerOut(waitCreateAccountSpinnerStr);
        }

        public void waitCreateAccountSprinnerOut(String xpath)
        {
            Browser.waitForImplicitTime(3);
            Browser.waitForElementToBeInvisible(120, xpath);
            Browser.waitForImplicitTime(4);
        }

        public void clickSignInLinkOnCreateAccount()
        {
            Browser.waitForElementToBeClickable(SignInLink, 20);
            SignInLink.Click();
        }

        public Boolean IsSignInPopUpDispalyed()
        {
            Browser.waitForElementToDisplayed(SignInWithGoogle, 20);
            return SignInWithGoogle.Displayed;
        }

        public Boolean IsCreateButtonEnabled()
        {
            Browser.waitForElementToDisplayed(createBtn, 20);
            return createBtn.Enabled;
        }

        public Boolean IsPrivacyPolicyDispalyed()
        {
            Browser.waitForElementToDisplayed(canadianPrivacyPolicy, 20);
            return canadianPrivacyPolicy.Displayed;            
        }

        public Boolean IsContactUsDispalyed()
        {
            Browser.waitForElementToDisplayed(canadianContactUs, 20);
            return canadianContactUs.Displayed;
        }
        

        public void EnterPostalCode(String postalCode)
        {
            Browser.waitForElementToDisplayed(postalCodeTextbox, 2);
            postalCodeTextbox.SendKeys(postalCode);
        }

        public Boolean IsPrivacyPolicyAndContactUsLinksDisplayed()
        {

            Browser.scrollVerticalBy("300");
            Browser.waitForElementToBeClickable(PrivacyPolicyLink, 20);
            //Browser.waitForElementToDisplayed(PrivacyPolicyLink, 20);
            Browser.waitForElementToDisplayed(ContactUsLink, 20);
            Boolean LinksDisplayed = PrivacyPolicyLink.Displayed && ContactUsLink.Displayed;
            return LinksDisplayed;
        }
        public Boolean IsIAgreeToReceiveGreatWolfResortCheckBoxDisplayed()
        {
            //Browser.waitForElementToDisplayed(CheckBoxWhenCanadaPostalCode, 20);
            Browser.waitForElementToBeClickable(CheckBoxWhenCanadaPostalCode, 20);
            Boolean CheckboxDisplayed = CheckBoxWhenCanadaPostalCode.Displayed;
            return CheckboxDisplayed;
        }

    }
}
