﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using MongoDB.Driver.Core.Operations;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;

namespace GWRFramework
{
    public class DiningAndShoppingPage
    {
        [FindsBy(How = How.XPath, Using = "(//li[@class='sub-menu-item-list-container']//a[contains(text(),'Dining')])[2]")]
        public IWebElement DiningPage;

        [FindsBy(How = How.XPath, Using = "//a[@class='is-menu-dropdown'][contains(text(),'Dining')]|//a[@class='is-menu-dropdown'][contains(text(),'DINING')]")]
        public IWebElement DiningAndshopping;
          
        [FindsBy(How = How.XPath, Using = "(//li[@class='sub-menu-item-list-container']//a[contains(text(),'Shopping')])[1]")]
        public IWebElement ShoppingPage;

        public String LearnMore = "//a[contains(text(),'Learn More')]";

        public String DiningpkgCount = "//h5[contains(text(),'Dining')]";

        String ShoppingPkgCount = "//h5[contains(text(),'Shopping')]";

        [FindsBy(How = How.XPath, Using = "//h1[contains(text(),'Dining')]")]
        public IWebElement DiningHeader;

        [FindsBy(How = How.XPath, Using = "//h1[contains(text(),'Shopping')]")]
        public IWebElement ShoppingHeader;


        [FindsBy(How = How.XPath, Using = "//div[@class='grid-item__content']")]
        public IWebElement PkgName;

        String PkgTitle = "//h4[contains(text(),'*PkgName*')]";

        [FindsBy(How = How.XPath, Using = "//h1[@class='details-venue__title']")]
        public IWebElement DiningDetail;


        [FindsBy(How = How.XPath, Using = "//h1[@class='details-venue__title']")]
        public IWebElement ShoppingDetail;

        [FindsBy(How = How.XPath, Using = "(//div[@class='grid-item__content']//h4)[1]")]
        public IWebElement ShoppingPackage1;

        public static String ShoppingPkgName = "";

        [FindsBy(How = How.XPath, Using = "(//div[@class='grid-item__content']//h4)[1]")]
        public IWebElement DiningPackage1;

        public static String DiningPkgName = "";



        public void NavigateToDiningPage()
        {
            Pages.PropertyHome.closeDealsSignUp();
            Browser.waitForElementToBeClickable(DiningAndshopping, 20);
            Actions actions = new Actions(Browser.driver);
            actions.MoveToElement(DiningAndshopping).Perform();
            DiningPage.Click();
            Browser.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }


        public Boolean VerifyTotalPkgAndDininPkgCount()
        {
            int pkgCount = Browser.driver.FindElements(By.XPath(LearnMore)).Count;
            int DiningPkgCount = Browser.driver.FindElements(By.XPath(DiningpkgCount)).Count;
            if (pkgCount.Equals(DiningPkgCount))
            {
                return true;
            } else {
                return false;
            }
        }
            public void NavigateToShoppingPage()
            {
                Pages.PropertyHome.closeDealsSignUp();
                //Browser.waitForElementToBeClickable(DiningAndshopping, 20);
                //Actions actions = new Actions(Browser.driver);
                //actions.MoveToElement(DiningAndshopping).Perform();
                ShoppingPage.Click();
                Browser.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            }


            public Boolean VerifyTotalPkgAndShoppingPkgCount()
            {
                int pkgCount1 = Browser.driver.FindElements(By.XPath(LearnMore)).Count;
                int ShoppingPkgCount1 = Browser.driver.FindElements(By.XPath(ShoppingPkgCount)).Count;
                if (pkgCount1.Equals(ShoppingPkgCount1))
                {
                    return true;
                } else {
                    return false;
                }
            }
        public Boolean VerifyDiningPageOpened()
        {
            Browser.waitForElementToDisplayed(DiningHeader, 20);
            if (DiningHeader.Displayed)
            {
                return true;
            }else {
                return false;
            }
        }

        public Boolean VerifyShoppingPageOpened()
        {
            Browser.waitForElementToDisplayed(ShoppingHeader, 20);
            if (ShoppingHeader.Displayed)
            {
                return true;
            } else {
                return false;
            }
        }

    /*    public void ClickOnTitleOfShoppingPkg(String ShoppingPkgName)
        {

            IWebElement ShoppingPkg = PkgName.FindElement(By.XPath(PkgTitle.Replace("*PkgName*", ShoppingPkgName)));
            ShoppingPkg.Click();


        }*/

        public void ClickOnTitleOfShoppingPkg()
        {
            Browser.waitForElementToBeClickable(ShoppingPackage1, 20);
            ShoppingPkgName = ShoppingPackage1.Text;
            ShoppingPackage1.Click();
        }

        public Boolean VerifyShoppingpkgDetailPage()
        {
            Browser.waitForElementToDisplayed(ShoppingDetail, 20);
            String ShoppingDetailPkg = ShoppingDetail.Text;

            if (ShoppingPkgName.Equals(ShoppingDetailPkg))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void ClickOnTitleOfPkg()
        {

            Browser.waitForElementToBeClickable(DiningPackage1, 30);
            DiningPkgName = DiningPackage1.Text;
            DiningPackage1.Click();

        }
        public Boolean VerifyDiningpkgDetailPage()
        {
            Browser.waitForElementToDisplayed(DiningDetail, 20);
            String DiningDetailPkg = DiningDetail.Text;

            if (DiningPkgName.Equals(DiningDetailPkg))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }

} 
