﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using GWRFramework.TestData;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using GWRFramework.TestData;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using AventStack.ExtentReports;
using System.Diagnostics.CodeAnalysis;
using Dynamitey.DynamicObjects;


namespace GWRFramework
{
    public class AddAPackagePage
    {
        [FindsBy(How = How.XPath, Using = "//div[@id='root']//h3[contains(text(),'Add a package')]")]
        public IWebElement addAPackagePage;

        [FindsBy(How = How.XPath, Using = "//section[@id='info-section']//div[text()='Lodge Location']")]
        public IWebElement LodgeLocationTile;

        [FindsBy(How = How.XPath, Using = "//section[@id='info-section']//div[text()='Stay Dates']")]
        public IWebElement StayDatesTile;

        [FindsBy(How = How.XPath, Using = "//section[@id='info-section']//div[text()='Guests']")]
        public IWebElement GuestTile;

        [FindsBy(How = How.XPath, Using = "//section[@id='info-section']//div[text()='Room Type']")]
        public IWebElement RoomTypeTile;

        [FindsBy(How = How.XPath, Using = "//div[@id='root']")]
        public IWebElement AddPckgPage;

        [FindsBy(How = How.XPath, Using = "(//div[@id='root']//div[text()='Reservation #']//following-sibling::div//div[contains(.,'11 a.m')])[2]")]
        public IWebElement CheckOutTime;
        


        String Adult = "//div[text()='Guests']//parent::div//following-sibling::div//div[text()='Adults']//preceding-sibling::div[text()='";
        String Adult1 = "']";

        String Kids = "//div[text()='Guests']//parent::div//following-sibling::div//div[text()='Kids']//preceding-sibling::div[text()='";
        String Kids1 = "']";

        String RoomType = "(//div[text()='Room Type']//parent::div//following-sibling::div)[1]//div[contains(.,'";
        String RoomType1 = "')]";

        String ResNum = "//div[@id='root']//div[text()='";
        String ResNum1 = "']";

        String SuiteName = "(//div[@id='root']//div[text()='Reservation #']//following-sibling::div//div[contains(.,'";
        String SuiteName1 = "')])[2]";

        String GustCnt = "(//div[@id='root']//div[text()='Reservation #']//following-sibling::div//div[contains(.,'";
        String GustCnt1 = " Adults')])[2]";

        public Boolean CheckAddAPackagePageDisplayed()
        {
            Browser.waitForElementToDisplayed(addAPackagePage, 10);
            bool AddApackagePage = addAPackagePage.Displayed;
            return AddApackagePage;
        }

        public Boolean CheckLodgeLoctionDisplayed()
        {
            Browser.scrollVerticalBy("900");
            Browser.waitForElementToDisplayed(LodgeLocationTile, 20);
            bool LodgeLocation = LodgeLocationTile.Displayed;
            return LodgeLocation;
        }

        public Boolean CheckStayDatesDisplayed()
        {
            Browser.waitForElementToDisplayed(StayDatesTile, 20);
            bool StayDateTile = StayDatesTile.Displayed;
            return StayDateTile;
        }

        public Boolean CheckGuestTileDisplayed()
        {
            Browser.waitForElementToDisplayed(GuestTile, 20);
            bool GuestTitlTile = GuestTile.Displayed;
            return GuestTitlTile;
        }

        public Boolean CheckAdultCountDisplayed(String AdltCnt)
        {
            Browser.waitForElementToDisplayed(GuestTile, 20);
            IWebElement AdultCnt = GuestTile.FindElement(By.XPath(Adult + AdltCnt + Adult1));
            bool AdltCntVal = AdultCnt.Displayed;
            return AdltCntVal;
        }

        public Boolean CheckKidsCountDisplayed(String KidsCnt)
        {
            Browser.waitForElementToDisplayed(GuestTile, 20);
            IWebElement KidCnt = GuestTile.FindElement(By.XPath(Kids + KidsCnt + Kids1));
            bool KidsCntVal = KidCnt.Displayed;
            return KidsCntVal;
        }

        public Boolean CheckRoomTypeSuiteName()
        {
            Browser.waitForElementToDisplayed(RoomTypeTile, 20);
            IWebElement RoomTypeval = RoomTypeTile.FindElement(By.XPath(RoomType + RateCalendar.SuiteTitle + RoomType1));
            bool roomTypeVal = RoomTypeval.Displayed;
            return roomTypeVal;
        }

        public Boolean CheckValResNumOnAddPCkGBtn()
        {
            Browser.waitForElementToDisplayed(AddPckgPage, 20);
            Pages.SearchYourReservation.waitForFindYourResrSprinnerOut(SearchYourReservationPage.waityouReservationSpinnerStr);
            IWebElement ResNumb = AddPckgPage.FindElement(By.XPath(ResNum + NewConfirmationPage.afterSplitResNum + ResNum1));
            bool ResrvNumbr = ResNumb.Displayed;
            return ResrvNumbr;
        }

        public Boolean CheckValSuiteName()
        {
            Browser.waitForElementToDisplayed(AddPckgPage, 20);
            IWebElement SuiteTtl = AddPckgPage.FindElement(By.XPath(SuiteName + RateCalendar.SuiteTitle + SuiteName1));
            bool ValSUitltl = SuiteTtl.Displayed;
            return ValSUitltl;
        }

        public Boolean CheckValGuestCnt(String AdltCnt)
        {
            Browser.waitForElementToDisplayed(AddPckgPage, 20);
            IWebElement GuestCnt = AddPckgPage.FindElement(By.XPath(GustCnt + AdltCnt + GustCnt1));
            bool ValGuestCnt = GuestCnt.Displayed;
            return ValGuestCnt;
        }

        public Boolean CheckCheckOutCnt()
        {
            Browser.waitForElementToDisplayed(CheckOutTime, 10);
            bool ChkOutTm = CheckOutTime.Displayed;
            return ChkOutTm;
        }


    }


}
