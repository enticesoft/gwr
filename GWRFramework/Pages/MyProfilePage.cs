﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using GWRFramework.TestData;
using Microsoft.Office.Interop.Excel;
using NUnit.Framework.Constraints;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow.CommonModels;

namespace GWRFramework
{
    
    public class MyProfilePage
    {
        public static int ProgressBarPercent;

        [FindsBy(How = How.XPath, Using = "//*[@id='sign-in-create-account']/descendant::a[contains(text(), 'My Profile')]")]
        public IWebElement MyProfileLink;

        [FindsBy(How = How.XPath, Using = "//*[text()='About Me']/parent::div/following-sibling::div//*[local-name() = 'svg']")]
        public IWebElement AboutMeIcon;

        [FindsBy(How = How.XPath, Using = "//input[@id='firstName'and @name='firstName']")]
        public IWebElement FirstName;

        [FindsBy(How = How.XPath, Using = "//input[@id='lastName'and @name='lastName']")]
        public IWebElement LastName;

        [FindsBy(How = How.Id, Using = "billingAddress")]
        public IWebElement BillingAdrress;

        [FindsBy(How = How.Id, Using = "city")]
        public IWebElement City;

        [FindsBy(How = How.Id, Using = "country")]
        public IWebElement Country;

        [FindsBy(How = How.Id, Using = "state")]
        public IWebElement State;

        [FindsBy(How = How.Id, Using = "zip")]
        public IWebElement PostalCode;

        [FindsBy(How = How.Id, Using = "phoneNumber")]
        public IWebElement PhoneNumber;

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Continue')]")]
        public IWebElement ContinueButton;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'About Me')]//*[local-name()='svg']")]
        public IWebElement AboutMeProgressBar;

        [FindsBy(How = How.Id, Using = "currentPassword")]
        public IWebElement CurrentPassword;

        [FindsBy(How = How.Id, Using = "newPassword")]
        public IWebElement NewPassword;

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Save')]")]
        public IWebElement SaveButton;

        [FindsBy(How = How.Id, Using = "newPasswordConfirmation")]
        public IWebElement ConfirmNewPassword;

        [FindsBy(How = How.XPath, Using = "//label[contains(text(),'New Password and Confirm Password do not match.')]")]
        public IWebElement PasswordNotMatchValidationMassage;

        [FindsBy(How = How.XPath, Using = "//*[@class='accordion']//input[@name='contactOptions.email']")]
        public IWebElement EmailPrefrenceCheckbox;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'My Preferences')]//*[local-name()='svg']")]
        public IWebElement MyprefrencesProgressBar;
        
        [FindsBy(How = How.XPath, Using = "//input[@name='contactOptions.sms']")]
        public IWebElement SMSPrefrenceCheckbox;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'+Add   Family Member')]")]
        public IWebElement AddFamilyMemberLink;
        
        [FindsBy(How = How.XPath, Using = "//input[@id='firstName'and @name='family[0].firstName']")]
        public IWebElement FirstNameInFamily;

        [FindsBy(How = How.XPath, Using = "//input[@id='lastName'and @name='family[0].lastName']")]
        public IWebElement LastNameInFamily;

        [FindsBy(How = How.Id, Using = "family.birthday")]
        public IWebElement BirthDayInFamily;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'My Family')]//ancestor::div[2]//following::span[1]")]
        public IWebElement VerifyAddedFamilyMember;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'My Family')]//*[local-name()='svg']")]
        public IWebElement MyFamilyProgressBar;

        [FindsBy(How = How.XPath, Using = "(//span[contains(text(), 'My Family')]//ancestor::div[2]//following-sibling::div[1]//*[local-name()='svg'])[1]")]
        public IWebElement EditMyFamilyMember;

        [FindsBy(How = How.XPath, Using = "(//span[contains(text(), 'My Family')]//ancestor::div[2]//following-sibling::div[1]//*[local-name()='svg'])[2]")]
        public IWebElement DeleteMyFamilyIcon;
        
        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'My Family')]//ancestor::div[2]//following-sibling::div[1]/child::div[1]")]
        public IWebElement VerifyMyFamilyMemberDeleted;

        [FindsBy(How = How.XPath, Using = "//button[text()='Google']")]
        public IWebElement GoogleButton;

        [FindsBy(How = How.XPath, Using = "//input[@id='identifierId']")]
        public IWebElement GmailId;

        [FindsBy(How = How.XPath, Using = "//input[@name='password']")]
        public IWebElement GmailPassword;

        [FindsBy(How = How.XPath, Using = "//button[@id='identifierNext']/span[text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Profile Completeness')]/preceding-sibling::div/div")]
        public IWebElement ProgressBar;


        [FindsBy(How = How.XPath, Using = "//*[@name='canContactByEmail']")]
        public IWebElement receiveEmailPromoCheckBox;

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Password Changed Successfully')]")]
        public IWebElement resetPwdMsg;

        //  public static String waitMyProfileSpinnerStr = "//div[@class='sc-htpNat cCwWAO']";

        public static String waitMyProfileSpinnerStr = "//*[contains(@transform,'rotate')]";

        [FindsBy(How = How.XPath, Using = "//input[@name='canContactByEmail']")]
        public IWebElement ReceivedEmailPromotionsCheckBox;

        public void ClickedOnMyProfile()
        {
            Browser.waitForElementToDisplayed(Pages.PropertyHome.userSignedInMenu, 20);
            Pages.PropertyHome.userSignedInMenu.Click();

            Browser.waitForElementToBeClickable(MyProfileLink, 20);
            MyProfileLink.Click();
            waitForMyProfileSprinnerOut(waitMyProfileSpinnerStr);
        }

        public void ClickOnAboutMeIcon()
        {
            //Browser.waitForElementToDisplayed(AboutMeIcon,90);
            Browser.waitForElementToBeClickable(AboutMeIcon,20);
            AboutMeIcon.Click();
        }

        public Boolean compareUserInfo(String firstName, String lastName)
        {
            String postalCode = DataAccess.getData(2, 2);
            Browser.waitForElementToDisplayed(FirstName, 20);
            String firstName1=Browser.getTextboxValue(FirstName);
            Browser.waitForElementToDisplayed(LastName, 20);
            String lastName1 = Browser.getTextboxValue(LastName);
            Browser.waitForElementToDisplayed(PostalCode, 20);
            String postalCode1= Browser.getTextboxValue(PostalCode);
            return firstName.Equals(firstName1) && lastName.Equals(lastName1) && postalCode.Equals(postalCode1);

        }

        public void EnterBillingAddress(String billingAdrress)
        {
            Browser.waitForElementToDisplayed(BillingAdrress, 20);
            Browser.clearTextboxValue(BillingAdrress);
            Thread.Sleep(4000);
            BillingAdrress.SendKeys(billingAdrress);
        }

        public void EnterPostalCode()
        {
            String postalCode = DataAccess.getData(2, 2);
            Browser.waitForElementToBeClickable(PostalCode, 20);
            PostalCode.SendKeys(postalCode);
        }

        public void EnterPhoneNumber(String phoneNumber)
        {
            Browser.waitForElementToDisplayed(PhoneNumber, 20);
            PhoneNumber.Clear();
            Browser.waitForImplicitTime(8);
            Thread.Sleep(3000);
            PhoneNumber.SendKeys(phoneNumber);
            Browser.waitForImplicitTime(8);
        }

        public void ClickContinueButton()
        {
            Browser.waitForElementToDisplayed(ContinueButton, 20);
            Browser.waitForElementToBeClickable(ContinueButton, 20);
            ContinueButton.Click();
            waitForMyProfileSprinnerOut(waitMyProfileSpinnerStr);
        }

        public Boolean EditedAboutMeInfoDisplayed(String BillingAdrr,String city, String country,String Stat, String phoneNumber)
        {    
            Browser.waitForElementToDisplayed(BillingAdrress, 20);
            String billingAddress1 = Browser.getTextboxValue(BillingAdrress);
            Browser.waitForElementToDisplayed(City, 10);
            String city1 = Browser.getTextboxValue(City);
            Browser.waitForElementToDisplayed(Country, 10);
            String country1 = Browser.getTextboxValue(Country);
            Browser.waitForElementToDisplayed(State, 10);
            String state1 = Browser.getTextboxValue(State);
            Browser.waitForElementToDisplayed(PhoneNumber,20);
            String phoneNumber1 = Browser.getTextboxValue(PhoneNumber);
            Boolean updatedInfo = billingAddress1.Equals(BillingAdrr) && city1.Equals(city) && country1.Equals(country) && state1.Equals(Stat) && phoneNumber1.Equals(phoneNumber);
            return updatedInfo;
        }

        public Boolean AboutMeIncreaseBy20Per()
        {
            Browser.waitForElementToDisplayed(ProgressBar, 20);
            String AboutMeProgressBarPer1 = ProgressBar.GetAttribute("width");
            String AboutMeProgressBarPer2 = AboutMeProgressBarPer1.Substring(0, AboutMeProgressBarPer1.Length - 1);
            int AboutMeProgressBarPercent = Convert.ToInt32(AboutMeProgressBarPer2);
            int ActualProgressIncr = AboutMeProgressBarPercent - ProgressBarPercent;
            Boolean result = ActualProgressIncr.Equals(20);
            return result;
        }
        public void EnterCurrentPassword(String currentPassword)
        {
            Browser.waitForElementToDisplayed(CurrentPassword, 20);
            //Browser.waitForImplicitTime(8);
            //Thread.Sleep(5000);
            CurrentPassword.SendKeys(currentPassword);
        }


        public void EnterNewPassword(String newPassword)
        {
            Browser.waitForElementToDisplayed(NewPassword, 20);
            NewPassword.SendKeys(newPassword);
        }

        public void EnterConfirmNewPassword(String confirmNewPassword)
        {
            Browser.waitForElementToDisplayed(ConfirmNewPassword, 20);
            ConfirmNewPassword.SendKeys(confirmNewPassword);
        }

        public void ClickOnSaveButton()
        {
            Browser.waitForElementToDisplayed(SaveButton, 20);
            Browser.waitForElementToBeClickable(SaveButton, 20);
            SaveButton.Click();
            waitForMyProfileSprinnerOut(waitMyProfileSpinnerStr);
        }

        public Boolean IsValidationMessageDisplayed()
        {
           
            Browser.waitForElementToDisplayed(PasswordNotMatchValidationMassage, 20);
            String validationMessage= PasswordNotMatchValidationMassage.Text;
            String expectedValidation = "New Password and Confirm Password do not match.";
            return validationMessage.Equals(expectedValidation);
        }

        public void ClickOnSelectedEmailPrefrencesCheckbox()
        {
            Browser.waitForElementToDisplayed(EmailPrefrenceCheckbox, 20);
            Browser.waitForElementToBeClickable(EmailPrefrenceCheckbox, 20);

             Boolean checkbox = EmailPrefrenceCheckbox.Selected;
            if (checkbox.Equals(true))
            {
                EmailPrefrenceCheckbox.Click();               
            }
            else 
            {
                EmailPrefrenceCheckbox.Click();
                Boolean checkbox1 = EmailPrefrenceCheckbox.Selected;
                if (checkbox1.Equals(true))
                {
                    EmailPrefrenceCheckbox.Click();
                }
            }
            
        }

        public Boolean IsEmailPrefrencesSelected()
        {
            
            Browser.waitForElementToDisplayed(EmailPrefrenceCheckbox, 20);
            Boolean checkbox = EmailPrefrenceCheckbox.Selected;
            return checkbox;
        }

        public void ClickOnDeselectedEmailPrefrencesCheckbox()
        {
            //Browser.waitForElementToDisplayed(EmailPrefrenceCheckbox, 40);
            Browser.waitForElementToBeClickable(EmailPrefrenceCheckbox, 20);
            Boolean checkbox = EmailPrefrenceCheckbox.Selected;
            if (checkbox.Equals(false))
            {
                EmailPrefrenceCheckbox.Click();
                Console.WriteLine("checkbox is checked");
            }else {
                EmailPrefrenceCheckbox.Click();
                Boolean checkbox1 = EmailPrefrenceCheckbox.Selected;
                if (checkbox1.Equals(false))
                {
                    EmailPrefrenceCheckbox.Click();
                }
            }
        }
        public Boolean MyPrefencesIncreaseBy20Per()
        {
            // Browser.waitForElementToBeClickable(MyprefrencesProgressBar, 20);
            // Boolean prgressBarEnable = MyprefrencesProgressBar.Enabled;

            Browser.waitForElementToDisplayed(ProgressBar, 20);
            String MyPrefeProgressBarPer1 = ProgressBar.GetAttribute("width");
            String MyPrefeProgressBarPer2 = MyPrefeProgressBarPer1.Substring(0, MyPrefeProgressBarPer1.Length - 1);
            int MyPrefeProgressBarPercent = Convert.ToInt32(MyPrefeProgressBarPer2);
            int ActualProgressIncr = MyPrefeProgressBarPercent - ProgressBarPercent;
            Boolean result = ActualProgressIncr.Equals(20);
            return result;
        }

        public void getProgressBarPercentage()
        {
            Browser.waitForElementToDisplayed(ProgressBar, 40);
            String ProgressBarPercent1 = ProgressBar.GetAttribute("width");
            String ProgressBarPercent2 = ProgressBarPercent1.Substring(0, ProgressBarPercent1.Length - 1);
            ProgressBarPercent = Convert.ToInt32(ProgressBarPercent2);
        }


        public void ClickOnDeselectedSMSPrefrencesCheckbox()
        {
            //Browser.waitForElementToDisplayed(SMSPrefrenceCheckbox, 40);
            Browser.waitForElementToBeClickable(SMSPrefrenceCheckbox, 20);
            Boolean checkbox = SMSPrefrenceCheckbox.Selected;
            if (checkbox.Equals(false))
            {
                SMSPrefrenceCheckbox.Click();            
            }else{
                EmailPrefrenceCheckbox.Click();
                Boolean checkbox1 = EmailPrefrenceCheckbox.Selected;
                if (checkbox1.Equals(false))
                {
                    EmailPrefrenceCheckbox.Click();
                }
            }
        }

        public Boolean IsSMSPrefrencesSelected()
        {           
            Browser.waitForElementToDisplayed(SMSPrefrenceCheckbox, 20);
            Boolean checkbox = SMSPrefrenceCheckbox.Selected;
            return checkbox;
        }

        public void ClickOnSelectedSMSPrefrencesCheckbox()
        {
            //Browser.waitForElementToDisplayed(SMSPrefrenceCheckbox, 40);
            Browser.waitForElementToBeClickable(SMSPrefrenceCheckbox, 20);

            Boolean checkbox = SMSPrefrenceCheckbox.Selected;
            if (checkbox.Equals(true))
            {
                SMSPrefrenceCheckbox.Click();               
            }         
        }

        public void ClickOnAddFamilyMemberLink()
        {
            //Browser.waitForElementToDisplayed(AddFamilyMemberLink,40);
            Browser.waitForElementToBeClickable(AddFamilyMemberLink,20);
            AddFamilyMemberLink.Click();
        }
        public void AddFirstName(String firstName)
        {
            Browser.waitForElementToDisplayed(FirstNameInFamily,20);
            FirstNameInFamily.SendKeys(firstName);
        }
        public void AddLastName(String lastName)
        {
            Browser.waitForElementToDisplayed(LastNameInFamily, 20);
            LastNameInFamily.SendKeys(lastName);
        }
        public void AddBirthday(String birthDay)
        {
            Browser.waitForElementToDisplayed(BirthDayInFamily, 20);
            BirthDayInFamily.SendKeys(birthDay);
        }

        public Boolean FamilyMemberDisplyed(String verifyAddedFamilyName)
        {
            Browser.waitForElementToDisplayed(VerifyAddedFamilyMember, 20);
            String addedFamilyName=VerifyAddedFamilyMember.Text;
            Boolean familyName = (addedFamilyName).Equals(verifyAddedFamilyName);
             return familyName ;
        }

        public Boolean MyFamilyIncreaseBy30Per()
        {     
            Browser.waitForElementToDisplayed(ProgressBar, 20);
            String MyFamilyProgressBarPer1 = ProgressBar.GetAttribute("width");
            String MyFamilyProgressBarPer2 = MyFamilyProgressBarPer1.Substring(0, MyFamilyProgressBarPer1.Length - 1);
            int MyFamilyProgressBarPercent = Convert.ToInt32(MyFamilyProgressBarPer2);
            int ActualProgressIncr = MyFamilyProgressBarPercent - ProgressBarPercent;
            Boolean result = ActualProgressIncr.Equals(30);
            return result;
        }

        public void ClickOnMyFamilyEditIcon()
        {
            Browser.waitForElementToDisplayed(EditMyFamilyMember, 20);
            Browser.waitForElementToBeClickable(EditMyFamilyMember,20);
            if (EditMyFamilyMember.Exists())
            {
                EditMyFamilyMember.Click();
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Family member not added");
            }


        }

        public void EditFirstName(String firstName)
        {
            Browser.waitForElementToDisplayed(FirstNameInFamily, 20);
            Browser.clearTextboxValue(FirstNameInFamily);
            //FirstNameInFamily.Clear();
            FirstNameInFamily.SendKeys(firstName);
        }


        public Boolean EditedFamilyMemberdisplayed(String editedFamilyMember)
        {
            Browser.waitForElementToDisplayed(VerifyAddedFamilyMember, 20);
            String firstName1=VerifyAddedFamilyMember.Text;
            return firstName1.Equals(editedFamilyMember);
        }

        public void ClickOnMyFamilyDeleteIcon()
        {
            Browser.waitForElementToDisplayed(DeleteMyFamilyIcon, 20);
            DeleteMyFamilyIcon.Click();
        }

        public Boolean FamilyMemberDeleted()
        {
            // Browser.waitForElementToDisplayed(VerifyAddedFamilyMember, 20);
            //return DeleteMyFamilyIcon.Exists();
           return VerifyMyFamilyMemberDeleted.Exists();
        }

        public Boolean MyFamilyDecreaseBy30Per()
        {          
            Browser.waitForElementToDisplayed(ProgressBar, 20);
            String MyFamilyProgressBarPer1 = ProgressBar.GetAttribute("width");
            String MyFamilyProgressBarPer2 = MyFamilyProgressBarPer1.Substring(0, MyFamilyProgressBarPer1.Length - 1);
            int MyFamilyProgressBarPercent = Convert.ToInt32(MyFamilyProgressBarPer2);
            int ActualProgressIncr = ProgressBarPercent - MyFamilyProgressBarPercent;
            Boolean result = ActualProgressIncr.Equals(30);
            return result;
        }

        public void ClickOnGoogleButton()
        {
            Browser.waitForElementToDisplayed(GoogleButton, 20);
            Browser.waitForElementToBeClickable(GoogleButton, 20);
            GoogleButton.Click();
        }

        /*public void EnterEmail(String gmail)
        {
            Browser.NavigateTo().;
            Browser.waitForElementToDisplayed(GmailId, 20);
            GmailId.SendKeys(gmail);
        }

        public void ClickOnNextButton()
        {
            
            Browser.waitForElementToBeClickable(NextButton, 20);
            NextButton.Click();

        }*/


        public void ClickOnEmailPrefrencesCheckbox()
        {
            //Browser.waitForElementToDisplayed(EmailPrefrenceCheckbox, 40);
            Browser.waitForElementToBeClickable(EmailPrefrenceCheckbox, 20);

            Boolean checkbox = EmailPrefrenceCheckbox.Selected;
            if (checkbox.Equals(true))
            {
                EmailPrefrenceCheckbox.Click();
            }
           
        }

        public void ClickOnSMSPrefrencesCheckbox()
        {
            //Browser.waitForElementToDisplayed(SMSPrefrenceCheckbox, 40);
            Browser.waitForElementToBeClickable(SMSPrefrenceCheckbox, 20);
            Boolean checkbox = SMSPrefrenceCheckbox.Selected;
            if (checkbox.Equals(true))
            {
                SMSPrefrenceCheckbox.Click();   
            }          
        }

        public void waitForMyProfileSprinnerOut(String xpath)
        {
            Browser.waitForImplicitTime(8);
            Browser.waitForElementToBeInvisible(300, xpath);
            Browser.waitForImplicitTime(8);
        }

        public void selectCountry(String element)
        {
            SelectElement CountryDrDp = new SelectElement(Country);
            IList<IWebElement> allOptions = CountryDrDp.Options;
            CountryDrDp.SelectByValue(element);
        }
        public void selectState(String element)
        {
            SelectElement StateDropDown = new SelectElement(State);
            IList<IWebElement> allOptions = StateDropDown.Options;
            StateDropDown.SelectByValue(element);
        }

        public void EnterCity(String city)
        {
            Browser.waitForElementToDisplayed(City, 20);
            Browser.clearTextboxValue(City);
            Thread.Sleep(4000);
            City.SendKeys(city);
        }

        public void ClickOnReceiveEmailPromotionscheckBox()
        {
            Browser.waitForElementToDisplayed(receiveEmailPromoCheckBox,15);
            receiveEmailPromoCheckBox.Click();
        }

        public void verifyResetPasswordMsg()
        {
            Browser.waitForElementToDisplayed(resetPwdMsg, 20);
        }

        public void IsReceivedEmailPromotionsCheckBoxChecked()
        {
            Browser.waitForElementToDisplayed(ReceivedEmailPromotionsCheckBox, 20);
            Boolean checkbox = ReceivedEmailPromotionsCheckBox.Selected;
            if (checkbox.Equals(true))
            {
                ReceivedEmailPromotionsCheckBox.Click();
                ReceivedEmailPromotionsCheckBox.Click();

            }
            else
            {
                ReceivedEmailPromotionsCheckBox.Click();
            }
        }

        public Boolean IsEmailPreferencesCheckboxSelected()
        {
            Browser.waitForElementToDisplayed(EmailPrefrenceCheckbox, 20);
            Boolean EmailPrefrenceCheckboxSelected = EmailPrefrenceCheckbox.Selected;
            return EmailPrefrenceCheckboxSelected;
        }

        public void IsReceivedEmailPromotionsCheckBoxUnchecked()
        {
            Browser.waitForElementToDisplayed(ReceivedEmailPromotionsCheckBox, 20);
            Boolean checkbox = ReceivedEmailPromotionsCheckBox.Selected;
            if (checkbox.Equals(true))
            {
                ReceivedEmailPromotionsCheckBox.Click();
            }
            else
            {
                ReceivedEmailPromotionsCheckBox.Click();
                ReceivedEmailPromotionsCheckBox.Click();
            }
        }

        public Boolean IsEmailPreferencesCheckboxNotSelected()
        {
            Browser.waitForElementToDisplayed(EmailPrefrenceCheckbox, 20);
            Boolean EmailPrefrenceCheckboxSelected = EmailPrefrenceCheckbox.Selected;
            return EmailPrefrenceCheckboxSelected;
        }

    }
}
