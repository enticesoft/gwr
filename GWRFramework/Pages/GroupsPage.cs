﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GWRFramework
{
    public class GroupsPage
    {
        [FindsBy(How = How.XPath, Using = "//li[@class='sub-menu-item-list-container']//a[contains(.,'Groups')]")]
        public IWebElement GroupHeaderMenu;

        [FindsBy(How = How.XPath, Using = "//li[@class='sub-menu-item-list-container']//a[contains(.,'Groups')]//following-sibling::ul//li//a[contains(.,'About')]")]
        public IWebElement GroupAboutSubMenu;

        [FindsBy(How = How.XPath, Using = "//li[@class='sub-menu-item-list-container']//a[contains(.,'Groups')]//following-sibling::ul//li//a[contains(.,'Meeting Spaces')]")]
        public IWebElement MeetingSpaceSubMenu;

        [FindsBy(How = How.XPath, Using = "//li[@class='sub-menu-item-list-container']//a[contains(.,'Groups')]//following-sibling::ul//li//a[contains(.,'Suites')]")]
        public IWebElement GroupSuiteSubMenu;

        [FindsBy(How = How.XPath, Using = "//li[@class='sub-menu-item-list-container']//a[contains(.,'Groups')]//following-sibling::ul//li//a[contains(.,'Catering')]")]
        public IWebElement GroupCateringSubMenu;

        [FindsBy(How = How.XPath, Using = "//li[@class='sub-menu-item-list-container']//a[contains(.,'Groups')]//following-sibling::ul//li//a[contains(.,'Special Events')]")]
        public IWebElement GroupSpecialeventSubMenu;

        [FindsBy(How = How.XPath, Using = "//li[@class='sub-menu-item-list-container']//a[contains(.,'Groups')]//following-sibling::ul//li//a[contains(.,'Activities')]")]
        public IWebElement GroupActivitySubMenu;

        [FindsBy(How = How.XPath, Using = "//li[@class='is-submenu-item-cta']//a[contains(.,'Request Proposal')]")]
        public IWebElement GroupRequestProposalBtn;
        

        [FindsBy(How = How.XPath, Using = "//div[@class='sticky-navigation__menu-item active']//a[contains(.,'ABOUT')]")]
        public IWebElement AboutSelectedTab;

        [FindsBy(How = How.XPath, Using = "//h2[@class='cards-row__title'][contains(.,'  Come together for your next event')]")]
        public IWebElement AboutComeTogetherSection;

        [FindsBy(How = How.XPath, Using = "//span[@class='h2'][contains(.,'Full Access to Lodge Amenities')]")]
        public IWebElement AboutFullAccessLodgeAmenitiesSection;

        [FindsBy(How = How.XPath, Using = "//div[@class='gallery-container']")]
        public IWebElement AboutFullAccessLodgeAmenitiesGallerySection;

        [FindsBy(How = How.XPath, Using = "//h2[contains(.,'Escape to Great Wolf Lodge.')]")]
        public IWebElement AboutEscapetoGreatwolgLodgeSection;

        [FindsBy(How = How.XPath, Using = "//div[@class='grid-container full ']")]
        public IWebElement AboutEscapetoGreatwolgLodgeGridContainerSection;

        [FindsBy(How = How.XPath, Using = "//div[@class='cmp-carousel']//div[@class='cmp-carousel__title'][contains(.,'Client Reviews')]")]
        public IWebElement AboutClientReviewSection;

        [FindsBy(How = How.XPath, Using = "//div[@class='minimap__iframe-container']")]
        public IWebElement AboutMiniMapSection;

        [FindsBy(How = How.XPath, Using = "(//a[@class='button'][contains(.,'Request Proposal')])")]
        public IWebElement RequestPropsalButton;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Tell Us About Your Event']")]
        public IWebElement TellUsAboutEvntSec;

        [FindsBy(How = How.XPath, Using = "//h2[@class='cards-row__title']")]
        public IWebElement ImagineToPosibilitySec;

        [FindsBy(How = How.XPath, Using = "//a[@class='button'][contains(.,'VIEW OUR FLOOR PLANS')]")]
        public IWebElement ViewFloorPlanButton;

        [FindsBy(How = How.XPath, Using = "//h2//b[contains(.,'Meeting Spaces')]")]
        public IWebElement MeetingSpaceSecTitle;

        [FindsBy(How = How.XPath, Using = "//h2[contains(.,'Frequently Asked Questions')]|(//h2//b[contains(.,'Frequently Asked Questions')])[2]")]
        public IWebElement FreAskQuestSecTitle;

        [FindsBy(How = How.XPath, Using = "//div[@class='cmp-carousel__content']")]
        public IWebElement roomCardOnSuite;

        [FindsBy(How = How.XPath, Using = "//a[@class='button'][contains(.,'EXPLORE OUR SPACIOUS SUITES')]")]
        public IWebElement exploreOurSuiteBtn;

        [FindsBy(How = How.XPath, Using = "//h1[contains(.,'Suites')]")]
        public IWebElement suiteTitleOnSuitePage;

        [FindsBy(How = How.XPath, Using = "//h2[@class='cards-row__title'][contains(.,'Reservations made easy')]")]
        public IWebElement ReserMadeEasySecSuitePage;

        [FindsBy(How = How.XPath, Using = "//h2[@class='cards-row__title'][contains(.,'Payment methods')]")]
        public IWebElement PaymentMethodSecSuitePage;

        [FindsBy(How = How.XPath, Using = "//div[@class='image-text-link__content-headline'][contains(.,'Banquets and Catering')]")]
        public IWebElement banQuetSecCateringPage;

        [FindsBy(How = How.XPath, Using = "//h2[@class='cards-row__title'][contains(.,'Lodge Dining Options')]")]
        public IWebElement DingngLodgeOptionCateringPage;

        [FindsBy(How = How.XPath, Using = "//a[@class='button'][contains(.,'ALL LODGE DINING OPTIONS')]")]
        public IWebElement ALLLODGEDININGOPTIONSButton;

        [FindsBy(How = How.XPath, Using = "//h1[@class='header-subheader__header '][contains(.,'Dining')]")]
        public IWebElement DiningPage;

        [FindsBy(How = How.XPath, Using = "//h2[@class='cards-row__title'][contains(.,'We’re here to host')]")]
        public IWebElement weHereToHostOnSpecialEventPage;

        [FindsBy(How = How.XPath, Using = "//a[@class='button'][contains(.,'EXPLORE BIRTHDAY PACKAGES')]")]
        public IWebElement ExploreBirthPackageBtn;

        [FindsBy(How = How.XPath, Using = "//h1[@class='cmp-title__text'][contains(.,'Birthday')]")]
        public IWebElement BirtdaytitleOnBirthdayPage;

        [FindsBy(How = How.XPath, Using = "//h2[@class='cards-row__title'][contains(.,'Book an event in our Water Park')]")]
        public IWebElement BookAnEventOnActivityPage;

        [FindsBy(How = How.XPath, Using = "//h2[@class='cards-row__title'][contains(.,'Team building and group activities ')]")]
        public IWebElement TeamBuildingAndGroupActivityOnActivityPage;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='Event Type']//select")]
        public IWebElement EventTypeDrpDwn;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='Event Start Date']//select[3]")]
        public IWebElement StartEventYeardrp;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='Event End Date']//select[3]")]
        public IWebElement EndDateEventYeardrp;

        [FindsBy(How = How.XPath, Using = "//input[@value='Specific Date']")]
        public IWebElement specificDateRadiobtn;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='Event Date']//select[3]")]
        public IWebElement DateEventYeardrpSpecificDate;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='Estimated Number of Attendees']//input")]
        public IWebElement numberOfAttdnceTxtbox;

        [FindsBy(How = How.XPath, Using = "(//div[@fs-field-validation-name='Do You Need Event/Meeting Space?']//input[@type='radio'])[1]")]
        public IWebElement meetinfSpaceEventRadioYESBtn;

        [FindsBy(How = How.XPath, Using = "(//div[@fs-field-validation-name='Do You Need Catering?']//input[@type='radio'])[1]")]
        public IWebElement needCateringRadioYESBtn;

        [FindsBy(How = How.XPath, Using = "(//div[@fs-field-validation-name='Do You Need Guest Rooms?']//input[@type='radio'])[2]")]
        public IWebElement needGuestRadioNoBtn;
//====================================================
        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='First Name']//input")]
        public IWebElement firstNameTxt;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='Last Name']//input")]
        public IWebElement LaststNameTxt;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='Email']//input")]
        public IWebElement EmailTxt;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='Phone Number']//input")]
        public IWebElement PhoneNumTxt;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='City']//input")]
        public IWebElement CityTxt;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='State / Province']//input")]
        public IWebElement StateProvinceTxt;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='Zip / Postal Code']//input")]
        public IWebElement ZipTxt;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='Organization or Group Name']//input")]
        public IWebElement OrgniserNameTxt;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='Organization Type']//select")]
        public IWebElement OrgnisationType;

        [FindsBy(How = How.XPath, Using = "//div[contains(@fs-field-validation-name,'Anything else you')]//textarea")]
        public IWebElement AnythignlikeTxt;

        [FindsBy(How = How.XPath, Using = "//input[@value='Send Information']")]
        public IWebElement SendInformationBtn;

        [FindsBy(How = How.XPath, Using = "//h1[@class='header-subheader__header '][contains(.,'Thank you')]")]
        public IWebElement ThanksPageAfterSubmitInfo;

        [FindsBy(How = How.XPath, Using = "//div[@fs-field-validation-name='Describe Your Event']//input")]
        public IWebElement descEventTxt;
        

        public void TellUsAboutYourSelfSec()
        {
            Browser.waitForElementToBeClickable(firstNameTxt, 10);
            firstNameTxt.SendKeys("QATest");

            Browser.waitForElementToBeClickable(LaststNameTxt, 10);
            LaststNameTxt.SendKeys("Nextgenqa");

            Browser.waitForElementToBeClickable(EmailTxt, 10);
            EmailTxt.SendKeys("gwr.nextgenqa1@gmail.com");

            Browser.waitForElementToBeClickable(PhoneNumTxt, 10);
            PhoneNumTxt.SendKeys("5896214575");

            Browser.waitForElementToBeClickable(CityTxt, 10);
            CityTxt.SendKeys("New York");

            Browser.waitForElementToBeClickable(StateProvinceTxt, 10);
            StateProvinceTxt.SendKeys("Kansas");

            Browser.waitForElementToBeClickable(ZipTxt, 10);
            ZipTxt.SendKeys("10005");

            Browser.waitForElementToBeClickable(OrgniserNameTxt, 10);
            OrgniserNameTxt.SendKeys("NextgenGroup");

            Browser.waitForElementToBeClickable(OrgnisationType, 10);
            SelectElement oSelect = new SelectElement(OrgnisationType);
            oSelect.SelectByValue("Government");

            Browser.waitForElementToBeClickable(AnythignlikeTxt, 10);
            AnythignlikeTxt.SendKeys("Test text for testing. ");

            Browser.waitForElementToBeClickable(SendInformationBtn, 10);
            SendInformationBtn.Click();
        }
        

        public void TellUsAboutYourEventSection(String EventType, String EventDate, String EvntStartDt, String EvntEndDt, String EvntDt)
        {
            if (EventType == "Other")
            {
                Browser.waitForElementToBeClickable(EventTypeDrpDwn, 20);
                SelectElement oSelect = new SelectElement(EventTypeDrpDwn);
                oSelect.SelectByValue(EventType);

                descEventTxt.SendKeys("Test Event");
            }
            else
            {
                Browser.waitForElementToBeClickable(EventTypeDrpDwn, 20);
                SelectElement oSelect = new SelectElement(EventTypeDrpDwn);
                oSelect.SelectByValue(EventType);
            }

            if(EventDate == "Date Range")
            {
                SelectElement oSelect1 = new SelectElement(StartEventYeardrp);
                oSelect1.SelectByValue(EvntStartDt);

                SelectElement oSelect2 = new SelectElement(EndDateEventYeardrp);
                oSelect2.SelectByValue(EvntEndDt);
            }
            else
            {
                Browser.waitForElementToBeClickable(specificDateRadiobtn, 10);
                specificDateRadiobtn.Click();

                Browser.waitForElementToBeClickable(DateEventYeardrpSpecificDate, 10);
                SelectElement oSelect3 = new SelectElement(DateEventYeardrpSpecificDate);
                oSelect3.SelectByValue(EvntDt);
            }

            Browser.waitForElementToBeClickable(numberOfAttdnceTxtbox, 10);
            numberOfAttdnceTxtbox.SendKeys("10");

            Browser.waitForElementToBeClickable(meetinfSpaceEventRadioYESBtn, 10);
            meetinfSpaceEventRadioYESBtn.Click();

            Browser.waitForElementToBeClickable(needCateringRadioYESBtn, 10);
            needCateringRadioYESBtn.Click();

            Browser.waitForElementToBeClickable(needGuestRadioNoBtn, 10);
            needGuestRadioNoBtn.Click();            
        }

        public void clickOnGroupHeader()
        {
            Browser.waitForElementToBeClickable(GroupHeaderMenu, 10);
            GroupHeaderMenu.Click();
        }
        public void clickOnGroupAboutSubMenu()
        {
            Browser.waitForElementToBeClickable(GroupAboutSubMenu, 10);
            GroupAboutSubMenu.Click();
        }

        public void clickOnGroupRequestPrpslBtn()
        {
            Browser.waitForElementToBeClickable(GroupRequestProposalBtn, 10);
            GroupRequestProposalBtn.Click();
        }
        
        public void clickOnGroupMeetingSubMenu()
        {
            Browser.waitForElementToBeClickable(MeetingSpaceSubMenu, 10);
            MeetingSpaceSubMenu.Click();
        }
        
        public Boolean ThanksPage()
        {
            Browser.waitForElementToBeClickable(ThanksPageAfterSubmitInfo, 10);
            return ThanksPageAfterSubmitInfo.Exists();
        }

        public Boolean AbotTabSelected() 
        {
            Browser.waitForElementToBeClickable(AboutSelectedTab, 10);
            return AboutSelectedTab.Exists();
        }

        public Boolean AboutComeTogetherSectionOnPage()
        {
            Browser.waitForElementToBeClickable(AboutComeTogetherSection, 10);
            return AboutComeTogetherSection.Exists();
        }

        public Boolean AboutFullAccessLodgeAmenitiesSectionOnPage()
        {
            Browser.waitForElementToBeClickable(AboutFullAccessLodgeAmenitiesSection, 10);
            return AboutFullAccessLodgeAmenitiesSection.Exists();
        }

        public Boolean AboutFullAccessLodgeAmenitiesGallerySectionOnPage()
        {
            Browser.waitForElementToBeClickable(AboutFullAccessLodgeAmenitiesGallerySection, 10);
            return AboutFullAccessLodgeAmenitiesGallerySection.Exists();
        }

        public Boolean AboutEscapetoGreatwolgLodgeSectionOnPage()
        {
            Browser.waitForElementToBeClickable(AboutEscapetoGreatwolgLodgeSection, 10);
            return AboutEscapetoGreatwolgLodgeSection.Exists();
        }
        public Boolean AboutEscapetoGreatwolgLodgeGridContainerOnPage()
        {
            Browser.waitForElementToBeClickable(AboutEscapetoGreatwolgLodgeGridContainerSection, 10);
            return AboutEscapetoGreatwolgLodgeGridContainerSection.Exists();
        }

        public Boolean AboutClientReviewOnPage()
        {
            Browser.waitForElementToBeClickable(AboutClientReviewSection, 10);
            return AboutClientReviewSection.Exists();
        }
        
        public Boolean AboutMiniMapOnPage()
        {
            Browser.waitForElementToBeClickable(AboutMiniMapSection, 10);
            return AboutMiniMapSection.Exists();
        }

        public Boolean EventTitleForm()
        {
            Browser.waitForElementToBeClickable(TellUsAboutEvntSec, 10);
            return TellUsAboutEvntSec.Exists();
        }

        public Boolean MeetingSpaceImagineToPosibilitySec()
        {
            Browser.waitForElementToBeClickable(ImagineToPosibilitySec, 10);
            return ImagineToPosibilitySec.Exists();
        }
        
        public void clickOnRequestProposalButton()
        {
            Browser.waitForElementToBeClickable(RequestPropsalButton, 10);
            Browser.javaScriptClick(RequestPropsalButton);
            //RequestPropsalButton.Click();
        }

        public void clickOnViewFloorPlanButton()
        {
            Browser.waitForElementToBeClickable(ViewFloorPlanButton, 10);
            Browser.javaScriptClick(ViewFloorPlanButton);
            //ViewFloorPlanButton.Click();
        }

        public Boolean verifyUserNavgtOnFloorPlanPage()
        {
            Thread.Sleep(3000);
            String currntUrl = Browser.driver.Url;
            String StrUrlExpectedProd = "https://static.greatwolf.com/";
            String StrUrlExpectedStage = "meeting-space-floorplan";
            return currntUrl.Contains(StrUrlExpectedProd) | currntUrl.Contains(StrUrlExpectedStage);
        }

        public Boolean MeetingSpaceTitleSec()
        {
            Browser.waitForElementToBeClickable(MeetingSpaceSecTitle, 10);
            return MeetingSpaceSecTitle.Exists();
        }

        public Boolean FreAskQuestSecTitleSec()
        {
            Browser.waitForElementToBeClickable(FreAskQuestSecTitle, 10);
            return FreAskQuestSecTitle.Exists();
        }
        
        public void clickOnSuiteSubMenu()
        {
            Browser.waitForElementToBeClickable(GroupSuiteSubMenu, 10);
            GroupSuiteSubMenu.Click();
        }

        public void clickOnCateringSubMenu()
        {
            Browser.waitForElementToBeClickable(GroupCateringSubMenu, 10);
            GroupCateringSubMenu.Click();
        }

        public void clickOnSpecialEventSubMenu()
        {
            Browser.waitForElementToBeClickable(GroupSpecialeventSubMenu, 10);
            GroupSpecialeventSubMenu.Click();
        }
        

        public void clickOnAcivitySubMenu()
        {
            Browser.waitForElementToBeClickable(GroupActivitySubMenu, 10);
            GroupActivitySubMenu.Click();
        }

        public Boolean RoomCardSecOnSuite()
        {
            Browser.waitForElementToBeClickable(roomCardOnSuite, 10);
            return roomCardOnSuite.Exists();
        }
        
        public void clickOnexploreOurSuiteBtn()
        {
            Browser.waitForElementToBeClickable(exploreOurSuiteBtn, 10);
            Browser.javaScriptClick(exploreOurSuiteBtn);
            //exploreOurSuiteBtn.Click();
        }

        public Boolean SuiteTitleOnSuiteNavigation()
        {
            Browser.waitForElementToBeClickable(suiteTitleOnSuitePage, 10);
            return suiteTitleOnSuitePage.Exists();
        }

        public Boolean reserMadeEasySectionOnSuitePage()
        {
            Browser.waitForElementToBeClickable(ReserMadeEasySecSuitePage, 10);
            return ReserMadeEasySecSuitePage.Exists();
        }
        
        public Boolean PaymentMethodSectionOnSuitePage()
        {
            Browser.waitForElementToBeClickable(PaymentMethodSecSuitePage, 10);
            return PaymentMethodSecSuitePage.Exists();
        }
        
        public Boolean BanquetsandCateringSecOnCateringPage()
        {
            Browser.waitForElementToBeClickable(banQuetSecCateringPage, 10);
            return banQuetSecCateringPage.Exists();
        }

        public Boolean DingngLodgeOptionSecOnCateringPage()
        {
            Browser.waitForElementToBeClickable(DingngLodgeOptionCateringPage, 10);
            return DingngLodgeOptionCateringPage.Exists();
        }
        
        public void clickOnALLLODGEDININGOPTIONSButton()
        {
            Browser.waitForElementToBeClickable(ALLLODGEDININGOPTIONSButton, 10);
            Browser.javaScriptClick(ALLLODGEDININGOPTIONSButton);
            //ALLLODGEDININGOPTIONSButton.Click();
        }

        public Boolean DingngPagetitle()
        {
            Browser.waitForElementToBeClickable(DiningPage, 10);
            return DiningPage.Exists();
        }
        
        public Boolean weHereToHostOnSpecialEventPageSec()
        {
            Browser.waitForElementToBeClickable(weHereToHostOnSpecialEventPage, 10);
            return weHereToHostOnSpecialEventPage.Exists();
        }
        
        public void clickOnexplorePackageBtn()
        {
            Browser.waitForElementToBeClickable(ExploreBirthPackageBtn, 10);
            Browser.javaScriptClick(ExploreBirthPackageBtn);
            //exploreOurSuiteBtn.Click();
        }
        
        public Boolean BirthdaypackaegPageTtitle()
        {
            Browser.waitForElementToBeClickable(BirtdaytitleOnBirthdayPage, 10);
            return BirtdaytitleOnBirthdayPage.Exists();
        }
        
        public Boolean BookAnEventOnActivityPageSection()
        {
            Browser.waitForElementToBeClickable(BookAnEventOnActivityPage, 10);
            return BookAnEventOnActivityPage.Exists();
        }
        
        public Boolean TeamBuildingOnActivityPageSection()
        {
            Browser.waitForElementToBeClickable(TeamBuildingAndGroupActivityOnActivityPage, 10);
            return TeamBuildingAndGroupActivityOnActivityPage.Exists();
        }
    }
}
