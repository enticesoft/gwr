﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWRFramework
{
    public class PlanPage
    {
        
        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Please check the offer details or select a different offer code.')]")]
        public IWebElement validationError;

        [FindsBy(How = How.XPath, Using = "//span[text()='Please enter number of adults']")]
        public IWebElement noAdultsValidationError;

        [FindsBy(How = How.XPath, Using = "//div[@data-checked='false'][2]")]
        public IWebElement accessibleRoomsOff;

        [FindsBy(How = How.XPath, Using = "//div[@data-checked='true'][2]")]
        public IWebElement accessibleRoomsOn;

        [FindsBy(How = How.XPath, Using = "//div[text()='Unfortunately that is not a valid offer code. Please re-enter or view our other offers.']")]
        public IWebElement invalidOfferCodeError;

        [FindsBy(How = How.XPath, Using = "//button[text()='SEARCH AGAIN']")]
        public IWebElement searchAgainButton;

        [FindsBy(How = How.XPath, Using = "//a[text()='VIEW SPECIAL OFFERS']")]
        public IWebElement viewSpecialOffersButton;

        [FindsBy(How = How.XPath, Using = "//h3[contains(text(),'wait to see you')]")]
        public IWebElement msgOnOfferCodeErrorPopup;

        [FindsBy(How = How.Id, Using = "checkInDateSelection")]
        public IWebElement checkInDateTextbox;

        String checkinDate1 = "//div[@class='CalendarMonthGrid_month__horizontal CalendarMonthGrid_month__horizontal_1']/div[@class='CalendarMonth CalendarMonth_1' ]/table[@class='CalendarMonth_table CalendarMonth_table_1']/tbody/tr/ td[ contains(text(),'";
        String checkinDate2 = "')]";

        [FindsBy(How = How.XPath, Using = "//*[@id='accordion__panel-offer-details']")]
        public IWebElement OfferCodeDescriptionSction;

        public Boolean validationAlertDisplayed()
        {
            Browser.waitForElementToDisplayed(validationError, 10);
            
            bool validation = validationError.Displayed;
            return validation;
        }

        public Boolean noAdultsValidationErrorDisplayed()
        {
            Browser.waitForElementToDisplayed(noAdultsValidationError, 10);
            bool validation = noAdultsValidationError.Displayed;
            return validation;
        }

        public void switchOnAccessibleRoomOption()
        {
            if (accessibleRoomsOn.Exists())
            {
                accessibleRoomsOn.Click();
            }
            Browser.waitForElementToBeClickable(accessibleRoomsOff, 10);
            accessibleRoomsOff.Click();
        }

        public Boolean checkAccessibleRoomOptionIsOn()
        {
            Browser.waitForElementToDisplayed(accessibleRoomsOn, 10);
            bool optionOn = accessibleRoomsOn.Displayed;
            return optionOn;
        }

        public void switchOffAccessibleRoomOption()
        {
            if(accessibleRoomsOff.Exists())
            {
                accessibleRoomsOff.Click();
            }
                Browser.waitForElementToBeClickable(accessibleRoomsOn, 10);
                accessibleRoomsOn.Click();
        }

        public Boolean checkAccessibleRoomOptionIsOff()
        {
            Browser.waitForElementToDisplayed(accessibleRoomsOff, 10);
            bool optionOff = accessibleRoomsOff.Displayed;
            return optionOff;
        }

        public Boolean checkInvalidOfferCodeErrorDisplayed()
        {
            Browser.waitForElementToDisplayed(invalidOfferCodeError, 10);
            bool invalidCode = invalidOfferCodeError.Displayed;
            return invalidCode;
        }

        public void clickSearchAgainButton()
        {
            Browser.waitForElementToBeClickable(searchAgainButton, 10);
            searchAgainButton.Click();
        }

        public Boolean checkMsgOnOfferCodeErrorDisplayed()
        {
            Browser.waitForElementToDisplayed(msgOnOfferCodeErrorPopup, 10);
            bool msgOnPopup = msgOnOfferCodeErrorPopup.Displayed;
            return msgOnPopup;
        }

        public void clickViewSpecialOffersButton()
        {
            Browser.waitForElementToBeClickable(viewSpecialOffersButton, 10);
            viewSpecialOffersButton.Click();
        }

        public void clickOnCalendar()
        {
            //Pages.PropertyHome.waitForDealsSignUpPopup();
            Pages.PropertyHome.closeDealsSignUp();
            Browser.scrollVerticalBy("400");
            Browser.waitForElementToBeClickable(checkInDateTextbox, 20);
            checkInDateTextbox.Click();
        }

        /* public void clickOnPastDate()
         {
             String futureCheckInDate = DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy");
             String newPastdate = futureCheckInDate.Substring(3,2);
             int pastDate = Int32.Parse(newPastdate);

             IWebElement PastDate = Browser.driver.FindElement(By.XPath(checkinDate1 + pastDate + checkinDate2));
             Browser.waitForElementToDisplayed(PastDate, 20);
             Browser.waitForElementToBeClickable(PastDate, 20);
             PastDate.Click();
         }*/

        public Boolean NotAbleToSelectPastDate()
        {
            String futureCheckInDate = DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy");
            String newPastdate = futureCheckInDate.Substring(3, 2);
            int pastDate = Int32.Parse(newPastdate);
            IWebElement PastDate = Browser.driver.FindElement(By.XPath(checkinDate1 + pastDate + checkinDate2));
            Browser.waitForElementToDisplayed(PastDate, 20);
            String value = PastDate.GetAttribute("aria-disabled");
            if (value == "true")
            {
                return true;
            }
            else { return false; }

        }

        public Boolean verifyOfferCodeDescription()
        {
            Boolean OffercodeDesc = OfferCodeDescriptionSction.Exists();
            return OffercodeDesc;
        }

    }
}
