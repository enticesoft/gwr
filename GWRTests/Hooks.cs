﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Gherkin.Model;
using AventStack.ExtentReports.Reporter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using GWRFramework;
using System.IO;
using OpenQA.Selenium;

namespace GWRTests
{
    
    [Binding]
    public class Hooks
    {
        private static ExtentTest featureName;
        private static ExtentTest scenario;
        private static ExtentReports extent;
        public static string finalDirName;
        public static string screenshotDir;

        [BeforeTestRun]
        public static void InitializeReport()
        {
            Browser.LaunchBrowser();

            var dir = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");
            var resultsDirName = dir + "TestResults";

            String timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            string dirName = "SmokeRun_" + timeStamp;

            finalDirName = resultsDirName + "\\" + dirName;
            Directory.CreateDirectory(finalDirName);

            var fileName = finalDirName + "\\index.html";

            var htmlReporter = new ExtentHtmlReporter(fileName);
            htmlReporter.Config.Theme = AventStack.ExtentReports.Reporter.Configuration.Theme.Dark;
            
            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);
        }

        [AfterTestRun]
        public static void TearDownReport()
        {
            //Flush report once test completes
            extent.Flush();
            Browser.Close();
        }

        [BeforeFeature]
        public static void BeforeFeature()
        {
            //Create dynamic feature name
            featureName = extent.CreateTest<Feature>(FeatureContext.Current.FeatureInfo.Title);
        }

        [BeforeScenario]
        public void Initialize()
        {
            scenario = featureName.CreateNode<Scenario>(ScenarioContext.Current.ScenarioInfo.Title);
        }

        [AfterStep]
        public void InsertReportingSteps()
        {
            var stepType = ScenarioStepContext.Current.StepInfo.StepDefinitionType.ToString();

            //PropertyInfo pInfo = typeof(ScenarioContext).GetProperty("TestStatus", BindingFlags.Instance | BindingFlags.NonPublic);
            //MethodInfo getter = pInfo.GetGetMethod(nonPublic: true);
            //object TestResult = getter.Invoke(ScenarioContext.Current, null);

            if (ScenarioContext.Current.TestError == null)
            {
                if (stepType == "Given")
                {
                    scenario.CreateNode<Given>(stepType + " " + ScenarioStepContext.Current.StepInfo.Text);
                }
                else if (stepType == "When")
                {
                    scenario.CreateNode<When>(stepType + " " + ScenarioStepContext.Current.StepInfo.Text);
                }
                else if (stepType == "Then")
                {
                    scenario.CreateNode<Then>(stepType + " " + ScenarioStepContext.Current.StepInfo.Text);
                }
                else if (stepType == "And")
                {
                    scenario.CreateNode<And>(stepType + " " + ScenarioStepContext.Current.StepInfo.Text);
                }
            }
            else if (ScenarioContext.Current.TestError != null)
            {
                if (stepType == "Given")
                {
                    string[] scenarioTags = ScenarioContext.Current.ScenarioInfo.Tags;
                    string ssPath = TakeScreenshot(scenarioTags[0]);
                    scenario.CreateNode<Given>(stepType + " " + ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message, MediaEntityBuilder.CreateScreenCaptureFromPath(ssPath).Build());
                 }
                else if (stepType == "When")
                {
                    string[] scenarioTags = ScenarioContext.Current.ScenarioInfo.Tags;
                    string ssPath = TakeScreenshot(scenarioTags[0]);
                    scenario.CreateNode<When>(stepType + " " + ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message, MediaEntityBuilder.CreateScreenCaptureFromPath(ssPath).Build());
                }
                else if (stepType == "Then")
                {
                    string[] scenarioTags = ScenarioContext.Current.ScenarioInfo.Tags;
                    string ssPath = TakeScreenshot(scenarioTags[0]);
                    scenario.CreateNode<Then>(stepType + " " + ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message, MediaEntityBuilder.CreateScreenCaptureFromPath(ssPath).Build());
                }
                else if (stepType == "And")
                {
                    string[] scenarioTags = ScenarioContext.Current.ScenarioInfo.Tags;
                    string ssPath = TakeScreenshot(scenarioTags[0]);
                    scenario.CreateNode<And>(stepType + " " + ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message, MediaEntityBuilder.CreateScreenCaptureFromPath(ssPath).Build());
                }
            }

            //Pending Status
         /*   if (TestResult.ToString() == "StepDefinitionPending")
            {
                if (stepType == "Given")
                    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text).Skip("Step Definition Pending");
                else if (stepType == "When")
                    scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text).Fail("Step Definition Pending");
                else if (stepType == "Then")
                    scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text).Fail("Step Definition Pending");
                else if (stepType == "And")
                    scenario.CreateNode<And>(ScenarioStepContext.Current.StepInfo.Text).Fail("Step Definition Pending");
            } */

         }

        public static string TakeScreenshot(String ScenarioTitle)
        {
            screenshotDir = finalDirName + "\\" + "Screenshots";
            Directory.CreateDirectory(screenshotDir);

            
            string path = screenshotDir + "\\" + ScenarioTitle + ".png";

            Screenshot screenshot = ((ITakesScreenshot)Browser.webDriver).GetScreenshot();
            screenshot.SaveAsFile(path, ScreenshotImageFormat.Png);
            return path;
        }

    }
}
