﻿Feature: PaymentPage
	In order to book suites for particular property
	As a end user
	I want to add all details on payment page

@TC5811 @Smoke
Scenario: 5811_Verify that user should be navigate on suite page from payment page after click on 'Suites' icon.
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I click on Suites Icon
	Then I should navigate on suite page

@TC5812 @Smoke
Scenario: 5812_Verify that user should be navigate on Activates page from payment page after click on 'Activities' icon.
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 2        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I click on Activities Icon in progress bar
	Then I should see Continue to Dining Package button

@TC5813 @Smoke
Scenario: 5813_Verify that user should be navigate on Dining page from payment page after click on 'Dining' icon.
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I click on Dining Icon in progress bar
	Then I should be navigate on dining page

@TC5822 @Smoke
Scenario: 5822_User able to check SMS check box on payment page
    Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I check SMS check box
	Then I should see SMS check box checked

@TC5950 @Smoke
Scenario: 5950_User able to uncheck SMS check box on payment page. 
    Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I check SMS check box
	And I Uncheck SMS check box
	Then I should see SMS check box Unchecked

@TC12877 @Smoke
Scenario: 12877_User able to log in into application through 'Already have account?' link on payment page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I Sign In to Application through Already have account link
	| Email Address             | Password   |
	| gwr.nextgenqa1@gmail.com | $Reset123$ |
	Then I should see User menu link at top
	
@TC12878 @Smoke
Scenario: 12878_User able to check 'Flex trip' check box on 'Payment' page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I check FlexTrip check box
	Then I should see FlexTrip check box checked

@TC12879 @Smoke
Scenario: 12879_User able to uncheck 'Flex trip' check box on 'Payment' page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I check FlexTrip check box
	And I uncheck FlexTrip check box
	Then I should see FlexTrip check box unchecked

@TC12880 @Smoke
Scenario: 12880_Verify that correct Flex trip package price display under cost summary on payment page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I observe FlexTrip price
	And I check FlexTrip check box
	Then I should see correct added FlexTrip price under cost summary

@TC5865 @Smoke
Scenario: 5865_Verify that by default 'Credit Card' radio button should be displayed selected in 'Payment Option' section
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
    Then I should see by default 'Credit Card' radio button selected in Payment Option section

@TC5868 @Smoke
Scenario: 5868_Verify that user able to select 'Affirm' monthly payment radio button
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I click on affirm radio button
	Then I should see Affirm radio button selected in Payment Option section

@TC5870 @Smoke
Scenario: 5870_User able to see 'Check out with affirm' button when user select 'Affirm monthly payment' radio button
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I click on affirm radio button
	And I entered billing info for Affirm Option
	| Billing Address | Postal Code | City |
	| MG street       | 10005       | New York |
    Then I should see Check Out With Affirm button

@TC5867 @Smoke
Scenario: 5867_Verify that user able to select 'Credit card' radio button
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I click on affirm radio button
	And I click on credit card radio button
	Then I should see 'Credit Card' radio button selected in Payment Option section
   
@TC5863 @Smoke
Scenario: 5863_verify that 'Affirm - Monthly Payments' radio button should be displayed disable when user select Canada country
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Canadian postal code
	| Postal Code |
	|  M3C0C1     |
	Then I should see Affirm Monthly Payment radio button disable
    
@TC5954 @Smoke
Scenario: 5954_Verify that ‘Add flexibility to your trips’ section should be displayed on payment page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
    Then I should see 'Add flexibility to your trips’ section

@TC12881 @Smoke
Scenario: 12881_Verify that user able to remove Flex trip package from cost summary on payment page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I check FlexTrip check box
	And I click on flexTrip remove link
	Then I should see FlexTrip check box unchecked

@TC5939 @Smoke
Scenario: 5939_Verify that user see correct check-In and check-out date in cost summary section
	Given I relaunch browser and navigate to Property Home page
    When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I observe selected Check In date
	And I observe selected Check Out date
	And I navigate from Suite page to Payment page
	Then I should see correct Check In Date
	And I should see correct Check Out Date

@TC5943 @Smoke
Scenario: 5943_Verify that user see correct 'Promotional savings' price under cost summary section
	Given I relaunch browser and navigate to Property Home page
   When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	Then I should see Promotional Saving price correctly under cost summary on activities page

@TC5945 @Smoke
Scenario: 5945_Verify that user see correct suite total under cost summary section.
	Given I relaunch browser and navigate to Property Home page
   When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 2        | AMTCERT    |
	And I navigate from Suite page to Payment page
	Then I should see Suite Total equals to Base price minus Promotional Saving price


@TC5941 @Smoke
Scenario: 5941_Verify that user see proper adult and Kids count in cost summary section
	Given I relaunch browser and navigate to Property Home page
	When I select check In date
	And I select check Out date
	And I select Adults
	| Adults |
	| 3   |
	And I select Kids
    | Kids | Kids1Age |
    |   1   |    2      |
	And I observe Adults and Kids count
	And I enter Offer Code
	| Offer Code |
	|   AMTCERT  |
	And I click on Book Now button
	And I navigate from Suite page to Payment page
	Then I should see correct adults count under cost summary
	And I should see correct kids count under cost summary

@TC5940 @Smoke
Scenario: 5940_Verify that user see selected suite with option name in cost summary section
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 0    | 0       | AMTCERT    |
	And I select Suite option
	| Suite Option       |
	| Accessible Bathtub |	
	And I navigate from Suite page to Payment page with suite option
	Then I should see valid suite title with Option Under cost summary
	| Suite Option Only First Word |
	| Accessible                   |

@TC7383 @smoke
Scenario: 7383_If User select 'Check In ' , 'Check out' dates within 10 days from current date then 'Add Flexibility to your trip' section should not display on 'Payment' page.
	Given I Open New browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within a 10 days
    | Check In Date | Check Out Date | Adults | Offer Code |
    |  Within a 10 Days | Within a 10 Days |   2  |  AMTCERT |
	And I click Book Now button
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	Then I should not see 'Add flexibility to your trips’ section

@TC7384 @smoke
Scenario: 7384_If user use 'Early Saver reservation' offer code then 'Add Flexbility to your trip' section should not display on payment page.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates more than 60 days
    | Check In Date | Check Out Date | Adults | Offer Code |
    |  More than 60 days from today | 2 days more than Check In date |   2  |  ESAVER |
	And I click Book Now button
	And I navigate from Suite page to Payment page
	Then I should not see 'Add flexibility to your trips’ section

@TC12883 @smoke
Scenario: 12883_Verify that user able to see correct applied offer code under cost summary on payment page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1    | 3        |  AMTCERT   |
	And I navigate from Suite page to Payment page
	Then I should see valid offercode under cost summary
	| Offer Code |
	| AMTCERT    |


#@TC12882 @smoke
#Scenario: 12882_Verify that 'Your credit card will be charged' price and 'Due Today' price should match on payment page.
	#Given I relaunch browser and navigate to Property Home page
	#When I navigate from Home page to Suite page
	#| Adults | Kids | Kids1Age | Offer Code |
	#| 3      | 1    | 3        |   AMTCERT  |
	#And I navigate from Suite page to Payment page
	#Then I should see 'Your credit card will be charged' price and 'Due Today' price are same

@TC12884 @Smoke
Scenario: 12884_Verify that package total should be addition of all packages under cost summary on payment page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Continue to Dining Package button
	And I click on Dining package Add button
	And I click on Continue to Payment page button
	Then I see Package Total price should equals to addition of all added packages

@TC12889 @Smoke
Scenario: 12889_Verify that 'Total' price minus 'Due today' price should equals to 'Due at Check-In' price.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I check FlexTrip check box
    Then I should see 'Total' price minus 'Due today' price should equals to 'Due at Check-In' price

@TC12887 @Smoke
Scenario: 12887_Verify that Total should be correct with having packages under cost summary section
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Continue to Dining Package button
	And I click on Dining package Add button
	And I click on Continue to Payment page button
	Then I should see correct Total with having packages under cost summary

@TC12891 @Smoke
Scenario: 12891_Verify that user able to remove package from cost summary on payment page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I navigate from suite Page to Activities page without LCO
	And I click on Continue to Dining Package button
	And I click on Dining package Add button
	And I click on Continue to Payment page button
	And I click on dining package Remove link
	Then I should see added package is removed from cost summary


@TC12886 @Smoke
Scenario: 12886_Verify that user able to see correct package total after update package quantity under cost summary on payment page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Continue to Dining Package button
	And I click on Continue to Payment page button
	And I update package quantity under cost summary
	Then I should see correct package total after update package quantity under cost summary

@TC12888 @Smoke
Scenario: 12888_Verify that user able to see correct Total after update package quantity under cost summary on payment page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Continue to Dining Package button
	And I click on Continue to Payment page button
	And I update package quantity under cost summary
    Then I should see correct Total with having packages under cost summary

@TC12885 @Smoke
Scenario: 12885_Verify that user able to update added package quantity under cost summary on payment page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1    | 2        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Continue to Dining Package button
	And I click on Continue to Payment page button
	And I update package quantity under cost summary
	Then I should see updated package quantity under cost summary

@TC5886 @Smoke
Scenario: 5886_Verify that user navigate on 'Payment' page after click on 'BACK TO PAYMENT' button on 'Don't Worry!' pop up.
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I click on Card holder not present at check-in question mark Icon
	And I click on Back to payment button
	Then I should see Payment Page Heading
	And I should not see Don't Warry pop up


@TC12890 @Smoke
Scenario: 12890_Verify that 'Promotional savings' price and 'Your Savings' price should match on payment page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
    Then I should see Promotional saving and Your Savings price are same


@TC5951 @Smoke
Scenario: 5951_Contact information automatically populated for logged user on payment page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I Sign In to Application but not from Home Page
	| Email Address             | Password   |
	| qatesternextgenqa9989+501@gmail.com | $Reset123$ |
	And I navigate from Suite page to Payment page
	And I click on Contact Info Edit link
	Then I should see correct first name
	| First Name |
	| Qatester   |
    And I should see correct last name
	| Last Name |
	| Nextgenqa |
	And I should see correct phone number
	| Phone Number |
	| (123) 456-7890 |
	And I should see correct email address
	| Email Address |
	| qatesternextgenqa9989+501@gmail.com |
	And I should see User menu link at top


@TC5934 @Smoke
Scenario:  5934_Verify that user navigate on 'Privacy Policy' page when user click on 'Privacy Policy' link
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Privacy Policy link
	Then I should navigate on privacy policy page

@TC5933 @Smoke
Scenario: 5933_Verify that user navigate on 'Terms & Condition' page when user click on 'Terms and Conditions' link
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Terms and Conditions link
	Then I should navigate on Terms and Conditions page


@TC12148 @Smoke
Scenario: 12148_Verify that user able to Download 'Credit Card Authorization Form' for all US Properties.
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I click on Card holder not present at check-in question mark Icon
    And I click on Download Credit Card Authorization Form link
	And I enter valid details on Credit Card Authorization signing form
	| Email Address             | Guest Name   |
	| qatesternextgenqa9989+501@gmail.com | Qatester |
	And I click on Submit button
	Then I should see Credit Card Authorization Form

@TC12892 @Smoke
Scenario: 12892_Billing information automatically populated for logged user on payment page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I Sign In to Application
	| Email Address             | Password   |
	| qatesternextgenqa9989+501@gmail.com | $Reset123$ |
	And I navigate from Suite page to Payment page	
	Then I should see correct billing address
	| Billing Address |
	| 1700 Nations Dr |
    And I should see correct postal code
	| Postal Code |
	| 60031 |
	And I should see correct city
	| City |
	| Gurnee|
	And I should see correct State
	| State|
	| IL |
	And I should see correct country
	| Country |
	| US |
	And I should see User menu link at top

@TC11662 @Smoke
Scenario: 11662_Verify that user not able  to see flex fare option for 'Niagara Property'.
    Given I relaunch browser and navigate to Property Home page
	When I select Another property location
		| Property      |
		| Niagara Falls, Ontario |
	And I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	Then I should not see 'Add flexibility to your trips’ section

@TC5947 @Smoke
Scenario: 5947_Verify that user see 'Water Park Passes' field under cost summary section
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	Then I should see 'Water Park Passes' field under cost summary section

@TC13569 @Smoke
Scenario: 13569_When select paypal radio button then pay with paypal button should displayed
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I click on paypal radio button
	Then I should see pay with paypal button

@TC13570 @Smoke
Scenario:13570_Payment Page: Affirm sandbox pop up open when click on affirm link in cost summary
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I click on Affirm link under cost summary
	Then I should see Affirm sandbox pop up open


@TC13567 @Smoke
Scenario: 13567_Verify that after enter Canada postal code then Agree check box should displayed
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I Sign In to Application
	| Email Address             | Password   |
	| qa80tester+219@gmail.com | $Reset123$ |
	And I navigate from Suite page to Payment page
	#And I entered Contact Info for Guest user
	#|First Name  |Last Name  |Email  | Phone Number |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | M3C0C1       | New York |
	And I click on billing Info Edit link
	And I select canada country
	And I entered Canadian postal code
	| Postal Code |
	|  M3C0C1     |
	And I click on Save changes button
	Then I should see I Agree check box displayed
    
@TC13568 @Smoke
Scenario: 13568_Verify that after enter postal code rather than canada then Agree check box should not displayed
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered postal code
	| Postal Code |
	|  60031    |
	Then I should not see I Agree check box

@TC16397 @Smoke
Scenario:16397_Payment page - User able to see Affirm pop up after click on 'Check out with affirm' button.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I Sign In to Application
	| Email Address             | Password   |
	| qa80tester+189@gmail.com | $Reset123$ |
	And I navigate from Suite page to Payment page
	And I click on affirm radio button
	And I entered billing info for Affirm Option
	| Billing Address | Postal Code | City |
	| 6170 W Grand Ave       | 60031       | New York |
    And I click on check out with affirm button
	Then I should see Affirm sandbox pop up open

@TC15464 @Smoke
Scenario: 15464_'Don't miss out on this great deal! If you book later, there's a chance the price will go up' message displayed on Payment page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	Then I should see dont miss section on Payment page
