﻿Feature: NewConfirmationPage
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@TC12903 @TC5988 @Smoke
Scenario: E2E_12903 - Verify that for guest user cost summary reflections should get match between payment and confirmation page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page with addign LCO and Other packages
	And I observe suite total on Payment Page
	And I observe package total on Payment Page
	And I observe taxes on Payment Page
	And I observe Resort fee Payment Page
	And I observe total Payment Page
	And I observe Due Today price on Payment Page
	And I observe Due at check in price on Payment Page
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see New Confirmation Page Welcome Title
	And I should see Reservation Number
	And I should see Cabana Section
	When I click on Cost Summarry button
	Then I should see correct Suite Total on Cost Summarry
	And I should see correct Package Total on Cost Summarry
	And I should see correct taxes price on Cost Summarry
	And I should see correct resort fee on Cost Summarry
	And I should see correct Total cost on Cost Summarry
	And I should see correct deposite paid on Cost Summarry
	And I should see correct Due at check in cost on Cost Summarry
	And I should not see Late Check Out button for add on Confirmation page

@TC12908 @Smoke
Scenario: E2E_12908 - Verify that logged user should able to see correct booking details on confirmation page and navigate on home page after sign out from confirmation page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1@gmail.com | $Reset123$ |
	And I navigate from Suite page to Payment page with addign packages
	And I observe suite total on Payment Page
	And I observe package total on Payment Page
	And I observe taxes on Payment Page
	And I observe Resort fee Payment Page
	And I observe total Payment Page
	And I observe Due Today price on Payment Page
	And I observe Due at check in price on Payment Page
	#And I entered payment details on Payment Page for log in user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see New Confirmation Page Welcome Title
	And I should see Reservation Number
	And I should see Cabana Section
	When I click on Cost Summarry button
	Then I should see correct Suite Total on Cost Summarry
	And I should see correct Package Total on Cost Summarry
	And I should see correct taxes price on Cost Summarry
	And I should see correct resort fee on Cost Summarry
	And I should see correct Total cost on Cost Summarry
	And I should see correct deposite paid on Cost Summarry
	And I should see correct Due at check in cost on Cost Summarry
	When I click on cost summarry close link
	Then I should see User menu link at top
	Then I land on home page of property

@TC12429 @Smoke
Scenario: : 12429_New Confirmation Page: Verify that logged user should successfully subscribed and unsubscribed the SMS sign up option.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      |   0  |          | AMTCERT    |	
	And I Sign In to Application
	| Email Address              | Password   |
	| qtesting874+6255@gmail.com | $Reset123$ |
	And I navigate from Suite page to Payment page
	#And I entered payment details on Payment Page for log in user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on SEE MORE DETAILS link
	And I Subscribe the SMS sign up
	Then I should see subscribe message 
	And I unsubscribe the SMS sign up		 
	And I should see unsubcribe message 

@TC11689 @Smoke
Scenario: 11689_Verify that If user add 'Dining Packages' while booking then 'Dining Packages' tile should display under 'Add Your Ons' Section with no CTA.
	Given I Open New browser and navigate to Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |		
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should see Dining Package tile on Confirmation page

@TC12254 @Smoke
Scenario: 12254_User should able to add 'attraction passes' from 'Add A Pass' tile and cost summary should get update accordingly.
	Given I Open New browser and navigate to Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add More Pass button
	And I click on Add button of pass
	Then I should see added different packages cost are correct
	And I should see correct total in cost summary of added packages
	When I click on cost summarry close link

@TC12255 @Smoke
Scenario: 12255_User should able to add 'dining' package from 'Add A Package' tile and cost summary should get update accordingly.
	Given I Open New browser and navigate to Property Home page			
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |		
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Package button of Dining tile
	Then I should see correct cost and package title of first dining package
	And I should see correct cost and package title of second dining package
	When I click on Add button of pass
	Then I should see added different packages cost are correct
	And I should see correct total in cost summary of added packages
	When I click on cost summarry close link

@TC11715 @Smoke
Scenario: 11715_Verify if user add 'Attraction Passes' and 'Dining' package from confirmation page then 'Attraction' & 'Dining' tiles should display under 'Add Your Ons' Section.
	Given I Open New browser and navigate to Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |		
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number	
	When I click on Add A Package button of Dining tile
	And I click on Add button of pass
	And I click on cost summarry close link
	When I click on Add More Pass button
	And I click on Add button of pass
	And I click on cost summarry close link
	Then I should see Dining Package tile on Confirmation page
	And I should see Attraction Package tile on Confirmation page
	
@TC11710 @Smoke
Scenario: 11710_Verify that If user add 'Cabana Package' from confirmation page then 'Cabanas' tile should display under 'Ons' Section with CTA 'Add Another One'
	Given I Open New browser and navigate to Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |		
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Cabana button of Cabana tile
	And I added available Cabanas
	And I click on Save to My Reservation button
	Then I should see Cabana tile on Confirmation page
	When I click on Indoor cabana view link and verify cost summary if cabana is added

@TC13581 @Smoke
Scenario: 13581_Verify that user able to see following things on 'Add A Package' page after click on 'Add A Package' CTA from birthday tile
	Given I Open New browser and navigate to Property Home page			
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Package button of Birthday tile
	Then I should see valid reservation number
	And I should see valid suite suite title
	When I click on Add buttons of Birthday Packages
	Then I should see correct total in cost summary of added birthday packages
	And I should see correct total in cost summary of added packages
	When I click on Save to My Reservation button
	Then I should see Birthday Package tile on Confirmation page

@TC13580 @Smoke
Scenario: 13580_CMP: New Confirmation page: Verify that user should see 'Attraction', 'Dining', 'Birthday' and 'Cabana' tiles
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	When I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1@gmail.com | $Reset123$ |
	And  I navigate from Suite page to Payment page
	#And I entered payment details on Payment Page for log in user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
    Then I should see 'Attraction', 'Dining', 'Birthday' and 'Cabana' tiles on Confirmation page

@TC11708 @Smoke
Scenario: 01_11708 New Confirmation Page: Verify that If user add 'Attraction Passes' while booking then 'Attraction Passes' tile should display under 'Add Your Ons' Section
	Given I relaunch browser and navigate to Property Home page
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2     | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I navigate to Birthday Tab
	And I add Birthday Package
	And I click on Continue to Dining Packages button
	And I click on Dining package Add button
	And I click on Continue to Payment page button
    #And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should see Attraction passes tile under Add Ons section
	And I should see User menu link at top

@TC11691 @Smoke
Scenario: 02_11691 New Confirmation Page: Verify that If user add 'Birthday Package' while booking then 'Birthday Package' tile should display under 'Add Your Ons' Section with no CTA
	Given I relaunch browser and navigate to Property Home page	
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |	
	When I navigate on My Reservation Page
	And I click on View Details button
	Then I should see Birthday package tile under Add Ons section
	And I should see User menu link at top

@TC9881 @Smoke 
Scenario: 03_9881 New Confirmation page-Verify that clicking on the X in the corner should close the 'Learn More' box
    Given I relaunch browser and navigate to Property Home page
	When I click on Find Your Reservation link under header section
	And I enter valid Suite confimation number and last name
	| Confirmation Number | Last Name |
	| Confirmation Number from excel | Nextgenqa |
	And I click on Search button
	And I click on learn more box close icon
	Then I should not see learn more box
	
@TC15458 @Smoke
Scenario: 15458_Confirmation Page > Verify that user click on See All Includes link and Verify Packages details on pop up.
    Given I relaunch browser and navigate to Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And  I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add More Pass button
	And I click on See All Includes link
	Then I should see Attraction Pass Details section
	And I should see Attraction Pass Price section
	And I should see Image section on Modal

@TC15459 @Smoke
Scenario: 15459_Confirmation Page > Verify that user click on What You Get link and Verify Packages details on pop up.
    Given I relaunch browser and navigate to Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And  I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Package button of Dining tile
	And I click on See What You Get link
	Then I should see Add Package Details section
	And I should see Add Package Price section
	And I should see Image section on Modal

@TC15460 @Smoke
Scenario: 15460_Verify that Dining package quantity should be correct on pop up.
    Given I relaunch browser and navigate to Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And  I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Package button of Dining tile
	Then I should see Count should be match

@TC15985 @Smoke
Scenario: 15985_Verify that user able to see upgrade room price on room type section on confirmation page
	Given I Open New browser and navigate to Property Home page			
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should see Room Type section on Confimation Page
	And I should see Room Type price section on Confimation Page
	And I should see Upgrade Now button on Confimation Page

@TC15990 @Smoke
Scenario: 15990_Verify that user navigate on 'nor1upgrades' site when click on 'Upgrade Now' button on room type section on confirmation page
	Given I Open New browser and navigate to Property Home page			
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Upgrade Now button
	Then I should see Nor upgrade page

@TC17771 @Smoke
Scenario: 17771_New Confirmation page - Verify that user able to see confirmation email after successful created reservation.
	Given I Open New browser and navigate to Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should see confirmation email received

@TC17783 @Smoke
Scenario: 17783_New Confirmation Page: When user add package on CMP page then user should received email on respective email ID.
	Given I Open New browser and navigate to Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |		
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Package button of Dining tile
	And I click on Add button of pass
	Then I should see Add A Package confirmation email received


@TC12934 @Smoke
Scenario: 12934_New Confirmation: Verify that cost summary should be reflected correct in mail which received for added packages after booking into respective mail id.
	Given I Open New browser and navigate to Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |		
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Package button of Dining tile
	And I click on Add button of pass
	When I should observe Cost summary on CMP Page
	Then I should see Added Packages cost summary into email
	And I should see correct Suite total on email
	And I should see correct Suite price on email
	And I should see correct Package total on email

@TC17928 @Smoke
Scenario: 17928_New Confirmation: When user added cabana then Cabana reservation email received on register email ID.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |		
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Cabana button of Cabana tile
	And I added available Cabanas
	And I click on Save to My Reservation button
	Then I should see Cabana confirmation email received

@TC17944 @Smoke
Scenario: 17944_New Confirmation: When user added cabana then on Cabana reservation email cost summary should be correct.
	Given I Open New browser and navigate to Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |		
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Cabana button of Cabana tile
	And I added available Cabanas
	And I click on Save to My Reservation button
	When I click on Indoor cabana view link and verify reservation number
	And I click on Cost Summarry button
	And I should observe Cabana cost summary on CMP Page
	Then I should see Added Packages cost summary into email
	And I should see Added Cabana Packages cost summary into email
	And I should see correct Suite total on email
	And I should see correct Suite price on email

@TC17710 @Smoke
Scenario: 04_17710_New Confirmation:Verify that Following  should displayed on  'Request and Celebrations' pop up.
	Given I Open New browser and navigate to Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	And I entered payment details on Payment Page for newly created user
	| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I entered Contact Info for Guest user
	#|First Name  |Last Name  |Email  | Phone Number |
	#|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |	
	#And I entered Payment and billing for Guest user
	#| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I should click on See More Button of Special Request Celebration section
	Then I should see Special request & celebration pop is open

@TC17711 @Smoke
Scenario: 05_17711_New Confirmation Page: Verify that  'Special Requests section' should have following options with checkbox.
	Given I relaunch browser and navigate to Property Home page	
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |	
	When I navigate on My Reservation Page
	And I click on View Details button
	When I should click on See More Button of Special Request Celebration section
	Then I should see Special request options

@TC17734 @Smoke
Scenario: 06_17734_New Confirmation: Verify that selected requests should get saved after clicked on 'Add to My Reservation' button and 'I added requests to my reservation' text should displayed on  'Special Request and Celebrations ' tile
	Given I relaunch browser and navigate to Property Home page	
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |	
	When I navigate on My Reservation Page
	And I click on View Details button
	When I should click on See More Button of Special Request Celebration section
	And  I should click on Close to elevator checkbox
	And I should click on Save to My Reservatio button
	Then I should see Request message added on My reservation

@TC17736 @Smoke
Scenario: 07_17736_New Confirmation:  Verify that User should be able to go back and edit preferences at any time by clicking on 'See More' button
	Given I relaunch browser and navigate to Property Home page		
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |	
	When I navigate on My Reservation Page
	And I click on View Details button
	When I should click on See More Button of Special Request Celebration section
	And  I should click on Close to elevator checkbox if it is not select already
	And I should click on Save to My Reservatio button
	And I should click on See More Button of Special Request Celebration section
	And I should click on Top floor checkbox
	And I should click on Save to My Reservatio button
	And I should click on See More Button of Special Request Celebration section
	Then I should see Top floor check box is checked

@TC17743 @Smoke
Scenario: 08_17743_New Confirmation Page: Verify that user able to delete Text field to add names of 'person celebrating'  by clicking on cross icon.
	Given I relaunch browser and navigate to Property Home page		
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |	
	When I navigate on My Reservation Page
	And I click on View Details button
	And I should click on See More Button of Special Request Celebration section
	And I should click on Birthday Checkbox
	And I should send name of celebrating person
	And I should click on Save to My Reservatio button
	And I should click on See More Button of Special Request Celebration section
	And I should Delete celebrating person name
	And I should click on Save to My Reservatio button
	And I should click on See More Button of Special Request Celebration section
	And I should click on Birthday Checkbox
	Then I should see name is deleted.

@TC17732 @Smoke
Scenario: 09_17732_New Confirmation Page: Verify that user should add more than one names of  'person celebrating' by clicking on 'Add another person' link.
	Given I relaunch browser and navigate to Property Home page	
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |	
	When I navigate on My Reservation Page
	And I click on View Details button
	And I should click on See More Button of Special Request Celebration section
	And I should click on Birthday Checkbox
	And I should send name of celebrating person
	And I should click on Add Another Person link
	And I should send second name of celebrating person
	And I should click on Save to My Reservatio button
	Then I should see Request message added on My reservation
	When I should click on See More Button of Special Request Celebration section
	Then I should see First name of celeration person added
	And I should see Second name of celeration person added

@TC17811 @Smoke
Scenario: 17811_New Confirmation- Verify that Before 24 hours prior to checking the following things should displayed under the check in window
	Given I Open New browser and navigate to Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	And I entered payment details on Payment Page for newly created user
	| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I entered Contact Info for Guest user
	#|First Name  |Last Name  |Email  | Phone Number |
	#|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |	
	#And I entered Payment and billing for Guest user
	#| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should see Online Check-in will be available 24 hours prior to your reservation Message