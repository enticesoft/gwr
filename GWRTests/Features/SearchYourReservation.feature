﻿Feature: SearchYourReservation
	In order to see My Reservation details
	As end user
	I need to search reservation
	
@TC6282 @Smoke 
Scenario: 01_6282 Verify that 'We could not find your reservation. Please check your information and try again' validation message is appear when user enter non existing reservation number & click on 'SEARCH' button.
    Given I relaunch browser and navigate to Property Home page
	When I click on Find Your Reservation link under header section
	And I enter invalid confimation number and last name
	| Confirmation Number | Last Name |
	| 111 | Nextgenqa |
	And I click on Search button
	Then I Should see error validation message

@TC12938 @Smoke 
Scenario:02_12938 Verify that user able to find day pass reservation.
    Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1@gmail.com  | $Reset123$ |
	And I navigate from Home page to DayPass page	
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 1             |
	And I click on the Checkout button
	#And I entered payment details on Payment Page for log in user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	And I should see Day pass Reservation Number
	And I am sign out from application
	And I click on Find Your Reservation link under header section
	And I enter valid Day pass confimation number and last name
	| Confirmation Number | Last Name |
	| Confirmation Number from excel | Nextgenqa |
	And I click on Search button
	Then I should see My Reservation Details Page Welcome Title
	And I should see correct reservation number on My reservation details page


@TC12940 @Smoke 
Scenario: 03_12940 Verify that user able to find suite reservation.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I Sign In to Application
	| Email Address             | Password   |
	| qa80tester+803@gmail.com  | $Reset123$ |
	And I navigate from Suite page to Payment page with addign LCO and Other packages
	#And I entered payment details on Payment Page for log in user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	And I should see suite Reservation Number
	And I am sign out from application
	And I click on Find Your Reservation link under header section
	And I enter valid Suite confimation number and last name
	| Confirmation Number | Last Name |
	| Confirmation Number from excel | Nextgenqa |
	And I click on Search button
	Then I should see My Reservation Details Page Welcome Title
	And I should see correct reservation number on My reservation details page

@TC11542 @smoke
Scenario: 04_11542 Verify that system should allow user to search his reservation from deals page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to deals page
	And I click on Find Your Reservation link under header section
	And I enter valid Suite confimation number and last name
	| Confirmation Number | Last Name |
	| Confirmation Number from excel | Nextgenqa |
	And I click on Search button
	Then I should see My Reservation Details Page Welcome Title
	And I should see correct reservation number on My reservation details page

@TC6866 @Smoke 
Scenario: 05_6866 Verify that user navigate on My Reservation page when user search reservation with valid data through 'Find Your Reservation' pop up on home page.
    Given I relaunch browser and navigate to Property Home page
	When I click on Find Your Reservation link under header section
	And I enter valid Suite confimation number and last name
	| Confirmation Number | Last Name |
	| Confirmation Number from excel | Nextgenqa |
	And I click on Search button
	Then I should see My Reservation Details Page Welcome Title
	And I should see correct reservation number on My reservation details page

@TC13551 @Smoke 
Scenario: 06_13551 User open 'Signup' pop up from 'Find your Reservation' pop up
    Given I relaunch browser and navigate to Property Home page
	When I click on Find Your Reservation link under header section
	And I click on Create an Account button
	Then I should see signup pop up

@TC12342 @Smoke 
Scenario: 07_12342 Find Your Reservation Page:  Verify that guest user should navigate on the 'Reservation Details Page' when go through 'Find Your Reservation'.
    Given I relaunch browser and navigate to Property Home page
	When I click on Find Your Reservation link under header section
	And I enter valid Suite confimation number and last name
	| Confirmation Number | Last Name |
	| Confirmation Number from excel | Nextgenqa |
	And I click on Search button
	Then I should see My Reservation Details Page Welcome Title
	And I should see correct reservation number on My reservation details page

@TC12939 @Smoke 
Scenario:08_12939 Verify that user able to find Cabana reservation.
    Given I relaunch browser and navigate to Property Home page
    When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I Sign In to Application
	| Email Address             | Password   |
	| qa80tester+803@gmail.com | $Reset123$ |
	And I navigate from Suite page to Payment page
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see New Confirmation Page Welcome Title
	And I should see Reservation Number
#	And I should see Cabana Section
    When I click on Add Cabana button
	And I added available Cabanas
	When I click on Save to My Reservation button if cabana available
	And I should Navigate on cabana Reservation page by click on caban view link
	And I am sign out from application
	And I click on Find Your Reservation link under header section
	And I enter valid Cabana confimation number and last name
	| Confirmation Number | Last Name |
	| Confirmation Number from excel | Nextgenqa |
	And I click on Search button
	Then I should see My Reservation Details Page Welcome Title
	And I should see correct reservation number on My reservation details page

@TC13583 @Smoke
Scenario: 09_13583 E2E: Find Your Reservation Page:  Verify that user should able to add 'Dining', 'Attraction', and 'Cabana' packages as an guest
	Given I Open New browser and navigate to Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Find Your Reservation link under header section
	And I search with valid Suite confimation number and last name
	| Confirmation Number | Last Name |
	| Confirmation Number from excel | Nextgenqa |
	And I click on Search button
	Then I should see My Reservation Details Page Welcome Title
	When I click on Add A Package button
	And I click on Packages Add button
	Then I should see valid reservation number
	And I should see valid suite suite title
	And I should see valid Dining titles
	And I should see valid Dining Package Cost
	And I should see valid offer code in cost summary
	When I click on Save to My Reservation button
	When I click on Add A Pass button
	And I click on Activity Packages Add button
	Then I should see valid reservation number
	And I should see valid suite suite title
	And I should see valid Activity Pass titles
	And I should see valid Acivity Packages Cost
	When I click on Save to My Reservation button
	When I click on Add Cabana button
	And I added available Cabanas
	Then I should see valid cabana price in cost summarry
	When I click on Save to My Reservation button if cabana available
	When I click on Cost Summarry button
	Then I should see added different packages cost are correct
	And I should see correct total in cost summary of added packages

@TC18010 @Smoke
Scenario: 18010_Find Your Reservation: When user find other property reservation then user should be navigate on correct property url.
	Given I Open New browser and navigate to Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	And I entered payment details on Payment Page for newly created user
	| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I entered Contact Info for Guest user
	#|First Name  |Last Name  |Email  | Phone Number |
	#|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |	
	#And I entered Payment and billing for Guest user
	#| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I am sign out from application
	And I navigate to Gurnee property page
	And I click on Find Your Reservation link under header section
	And I enter valid Suite confimation number and last name to verify Property redirection
	| Last Name |
	| Nextgenqa |
	And I click on Search button
    Then I should see user navigate on valid property
