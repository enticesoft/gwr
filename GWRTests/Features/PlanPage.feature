﻿Feature: PlanPage
	In order to search suites
	As a end user
	I want to access plan page

@TC9411 @smoke
Scenario: 9411_Verify that user able to see validation error message when select dates after 60 days from today for MOREFUN offer code.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates more than 60 days
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  More than 60 days from today             |        2 days more than Check In date        |    2    |     MOREFUN       |
	And I click Book Now button
	Then I should see validation error

@TC9429 @smoke
Scenario: 9429_Verify that user able to see validation error message when select dates within 60 days from today for ESAVER offer code.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within 60 days
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  Within 60 days from today             |        2 days more than Check In date        |    2    |     ESAVER       |
	And I click Book Now button
	Then I should see validation error

@TC5600 @smoke
Scenario: 5600_If user select 'Kids' count and ‘Adult’ count as 0 and click on ‘Book Now’ button then verify that validation message should be fired for adult is mandatory.
	Given I Open New browser and navigate to Property Home page
	When I select only kids
	| Kids | Kids1Age |
    | 1   | 4        |
	Then I should see validation error for no adults


@TC13006 @smoke
Scenario: 13006_User able to decrement Adults count.
	Given I am on Property Home page
	When I select adults
         | Adults | 
		 |   4    |
	And I decrement the no of adults
		| Adults |
		|   2    |
	Then I should see changed Guests count
		| Guests |
		|   2    |

@TC5564 @smoke
Scenario: 5564_User able to increment Adults count.
	Given I am on Property Home page
	When I select adults
         | Adults | 
		 |   2    |
	And I increament the no of adults
		| Adults |
		|    4   |
	Then I should see changed Guests count
		| Guests |
		|   4    |

@TC13007 @smoke
Scenario: 13007_User able to decrements Kids count.
	Given I Open New browser and navigate to Property Home page
	When I select 2 kids
        | Adults | Kids | Kids1Age | Kids2Age |
		| 1      |   2  |     3    |     2    |
	And I decreament the no of kids
		| Kids |
		|   1  |
	Then I should see changed Guests count
		| Guests |
		|   2    |

@TC5566 @smoke
Scenario: 5566_User able to increment Kids count.
	Given I am on Property Home page
	When I select 1 kid
        | Adults | Kids | Kids1Age |
		| 1      |   1  |     3    |
	And I increament the no of kids
		| Kids | Kids2Age |
		| 2    |     2    |
	Then I should see changed Guests count
		| Guests |
		|   3    |

#@TC5572 @smoke
#Scenario: 5572_User able to ON 'Accessible Rooms only' toggle under Guest section.
	#Given I am on Property Home page
	#When I enter booking dates
	#And I click to open Guest section
	#And I switch ON the Accessible Rooms only option
	#Then I should see the Accessible Rooms only option On

#@TC13008 @smoke
#Scenario: 13008_User able to OFF 'Accessible Rooms only' toggle under Guest section.
	#Given I am on Property Home page
	#When I enter booking dates
	#And I click to open Guest section
	#And I switch Off the Accessible Rooms only option
	#Then I should see the Accessible Rooms only option Off

@TC5567 @smoke
Scenario: 5567_User not able to increment kids count when user enter maximum adult count.
	Given I Open New browser and navigate to Property Home page
	When I select maximum adults
         | Adults | 
		 |   8   |
	And I try increasing the no of kids
		| Kids | Kids1Age |
		| 1    | 4        |
	Then I should not see change in Guests count
		| Guests |
		|   8   |

@TC5568 @smoke
Scenario: 5568_User not able to increment adults count when user enter maximum kids count.
	Given I relaunch browser and navigate to Property Home page
	When I select maximum kids
	| Kids | Kids1Age | Adults |
    | 6   | 4        | 2      |
	And I try increasing the no of adults
		| Adults | 
		| 2    |
	Then I should not see change in Guests count
		| Guests |
		|   8   |

@TC5576 @smoke
Scenario: 5576_User can see suites when user select weekdays on Play your stay section.
	Given I am on Property Home page
	When I enter weekdays on Booking Widget
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Weekday from excel | Weekday from excel | 2      | AMTCERT    |
	And I click Book Now button
	Then I should navigate to Suite Page

@TC8819 @smoke
Scenario: 8819_Verify that error message shown when Invalid Offer Code is entered.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Test data excel | Test data excel | 2      | ADFAD    |
	And I click Book Now button
	Then I should see invalid offer code error

@TC8823 @smoke
Scenario: 8823_Verify that user able to close 'Offer Code Error' pop up when click on 'Close' icon.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within 60 days and 1 night
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  Within 60 days from today             |        2 days more than Check In date        |    2    |     MOREFUN       |
	And I click Book Now button
	And I close Offer Code error pop up
	Then I should navigate to Suite Page

@TC12659 @smoke
Scenario: 12659_Verify that user able to see correct message on pop up when applied 'MOREFUN'  with 1 night stay.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within 60 days and 1 night
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  Within 60 days from today             |        2 days more than Check In date        |    2    |     MOREFUN       |
	And I click Book Now button
	Then I should see correct message for MOREFUN offer code

@TC12658 @smoke
Scenario: 12658_Verify that user able to see 'Offer Code Error' pop up when applied 'MOREFUN'  with 1 night stay.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within 60 days and 1 night
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  Within 60 days from today             |        2 days more than Check In date        |    2    |     MOREFUN       |
	And I click Book Now button
	Then I should see correct message for MOREFUN offer code

@TC8826 @smoke
Scenario: 8826_Verify that user navigates to 'deals' page when click on 'View Special Offers' button on Error message Popup.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within 60 days and 1 night
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  Within 60 days from today             |        2 days more than Check In date        |    2    |     MOREFUN       |
	And I click Book Now button
	And I click View Special Offers button
	Then I should navigate to Special Deal Details page

@TC5578 @smoke
Scenario: 5578_User see suites when user select long weekdays on Plan your stay section.
	Given I am on Property Home page
	When I enter long weekdays on Booking Widget
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Long Weekday from excel | Long Weekday from excel | 2      | AMTCERT    |
	And I click Book Now button
	Then I should navigate to Suite Page

@TC5577 @smoke
Scenario: 5577_User see suites when user select weekend on Plan your stay section.
	Given I relaunch browser and navigate to Property Home page
	When I enter weekend dates on Booking Widget
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Weekend from excel | Weekend from excel | 2      | AMTCERT    |
	And I click Book Now button
	Then I should navigate to Suite Page

@TC9427 @smoke
Scenario: 9427_Verify that user able to see offer code related suites when applied 'ESAVER' offer code.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates more than 60 days
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  More than 60 days from today             |        2 days more than Check In date        |    2    |     ESAVER      |
	And I click Book Now button
	Then I should navigate to Suite Page

@TC9424 @smoke
Scenario: 9424_Verify that user able to see offer code related suites when applied 'HEROES' offer code.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Test data excel | Test data excel | 2      | HEROES    |
	And I click Book Now button
	Then I should navigate to Suite Page

@TC9409 @smoke
Scenario: 9409_Verify that user able to see suites when applied 'MOREFUN' offer code for 2 or more than 2 nights stay.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within 60 days and more than 2 nights
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  Within 60 days from today   |      more than 2 nights    |    2    |     MOREFUN       |
	And I click Book Now button
	Then I should navigate to Suite Page


@TC8760 @smoke
Scenario: 8760_Verify that user not able to select past date as 'Check In' date
Given I relaunch browser and navigate to Property Home page
When I click On Check In date Calendar
Then I should not able to select past date as 'Check In' date

@TC11948 @smoke
Scenario:  11948_Verify that when user enter 'ESAVE' offer code then Offer Code description should show to user
	Given I Open New browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates more than 60 days
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  More than 60 days from today | 2 days more than Check In date | 2| ESAVER |
	And I click Book Now button
	Then I should see applied offer code related suites
	    | OfferCode |
	    | ESAVER |
	And I should see offer code description 

@TC17324 @smoke
Scenario:  17324_Lead Gen Popup: Verify that Lead Gen modal should not be automatically appearing on any /plan pages.
	Given I navigate to Property plan page
	When I wait for Led Gen popup to appear
	Then I should see not see Lead Gen Modal Pop Up

@TC15776 @smoke
Scenario: 15776_Plan page - Verify that user able to navigate on activities page from suites page having ESAVER offer with 2 or more nights.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates more than 60 days with 3 nights stay
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  More than 60 days from today             |        2 days more than Check In date        |    2    |     ESAVER      |
	And I click Book Now button
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	Then I should see Continue to Dining Package button
	And I should see ESAVER offer code is applied and displayed inside cost summary

@TC15777 @smoke
Scenario: 15777_Plan page - Verify that user able to navigate on activities page from suites page having MOREFUN offer with 3 or more nights.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates less than 60 days with 3 nights stay
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  More than 60 days from today             |        2 days more than Check In date        |    2    |     MOREFUN      |
	And I click Book Now button
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	Then I should see Continue to Dining Package button
	And I should see MOREFUN offer code is applied and displayed inside cost summary