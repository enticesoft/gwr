﻿Feature: MyProfilePage
As a user
I am able to create
update my profile


@TC11242 @Smoke
Scenario:11242_Verify that after account creation , correct information of user should be populated in on 'My profile' page
Given I relaunch browser and navigate to Property Home page
And I Create an Account into application
| First Name | Last Name | Postal Code    | Email                   | Password |
| Qatester   | Nextgenqa | As per Property| testingauto38@gmail.com | $Reset123$|
When I navigate from Home page to My Profile page
And I click on 'About Me' edit icon
Then I should able to see correct information populated under about me section
| First Name | Last Name | Postal Code    | 
| Qatester   | Nextgenqa | As per Property|
And I should see User menu link at top

@TC6230 @Smoke
Scenario:03_6230_Verify that user edits About Me information from My Profile page and progress bar increase by 20%
Given I Open New browser and navigate to Property Home page
And I Create an Account into application
| First Name | Last Name | Postal Code    | Email                    | Password |
| Qatester   | NextgenQA | As per Property| testingauto38@gmail.com | $Reset123$  |
When I navigate from Home page to My Profile page
And I observe progress Bar
And I click on 'About Me' edit icon
And I enter Billing Address
    |BillingAddress|
    | 1700 Nations Drive |
And I select country
    |Country|
    | US |
And I select state
    |State|
    | IL |
And I enter city
    |City|
    |Gurnee|
And I enter Phone Number
    |Phone|
    |1234567891|
And I click on 'Save' button
And I click Continue button
And I click on 'About Me' edit icon
Then I should able to see edited About Me information
| BillingAddress     | City   | Country       | State | Phone |
| 1700 Nations Drive | Gurnee | US |IL |(123) 456-7891|
And I should see About Me progress bar increase by 20%
And I should see User menu link at top

@TC6227 @Smoke
Scenario: 6227_Verify that 'New Password cannot match Confirm New Password' validation message should be displayed when user enter 'Confirm New Password' and 'New Password' mismatch.
Given I relaunch browser and navigate to Property Home page
And I Sign In to Application
| Email                    | Password   |
| devtested333+210@gmail.com| $Reset123$ |
When I navigate from Home page to My Profile page
And I enter current password
   |CurrentPassword|
    |$Reset123$|
And I enter New Password
    |NewPassword|
    |$Reset12345$|
And I enter Confirm New Password
    |ConfirmNewPassword|
     |$Reset12356$|
And I click on 'Save' button
Then I should able to see 'New Password cannot match Confirm New Password' validation message
And I should see User menu link at top

@TC6264 @Smoke
Scenario: 6264_Verify that user able to deselect Email Preferences checkbox under the Email preferences
Given I Open New browser and navigate to Property Home page
And I Sign In to Application
| Email                    | Password   |
|devtested333+501@gmail.com| $Reset123$ |
When I navigate from Home page to My Profile page
And I click on Selected Email Preferences checkbox
And I click on 'Save' button
Then I should able to deselect Email Preferences checkbox
And I should see User menu link at top 

@TC6263 @Smoke
Scenario: 6263_Verify that user should be select Email Preferences checkbox under the Email Preferences
Given I Open New browser and navigate to Property Home page
And I Sign In to Application
| Email                    | Password   |
| devtested333+501@gmail.com| $Reset123$ |
When I navigate from Home page to My Profile page
#And I click on Selected Email Preferences checkbox
#And I click on 'Save' button
And I click on Deselected Email Preferences checkbox
And I click on 'Save' button
Then I should able to select Email Preferences checkbox
And I should see User menu link at top

@TC6268 @Smoke
Scenario: 6268_Verify that if user added My Preferences then 20 % completeness progress bar should be increase.
Given I relaunch browser and navigate to Property Home page
And I Create an Account into application
| First Name | Last Name | Postal Code    | Email                   | Password |
| Qatester   | Nextgenqa | As per Property| testingauto38@gmail.com | $Reset123$|
When I navigate from Home page to My Profile page
And I click on 'About Me' edit icon
And I enter Phone Number
    |Phone|
    |1234567891|
And I click on 'Save' button
And I click Continue button
And I observe progress Bar
And I click on Deselected Email Preferences checkbox
And I click on 'Save' button
Then I should able to select Email Preferences checkbox
And I should see My Preferences progress bar increased by 20%
And I should see User menu link at top

@TC6266 @Smoke
Scenario: 6266_Verify that user should be select SMS Preferences checkbox under the Email Preferences
Given I Open New browser and navigate to Property Home page
#And I Sign In to Application
#| Email                    | Password   |
#| qatester101@gmail.com | $Reset123$ |
And I Create an Account into application
| First Name | Last Name | Postal Code    | Email                   | Password |
| Qatester   | Nextgenqa | As per Property| testingauto38@gmail.com | $Reset123$|
When I navigate from Home page to My Profile page
And I click on 'About Me' edit icon
And I enter Phone Number
    |Phone|
    |1234567891|
And I click on 'Save' button
And I click Continue button
And I click on Deselected SMS Preferences checkbox
And I click on 'Save' button
Then I should able to select SMS Preferences checkbox
And I should see User menu link at top
#And I Reset SMS Preferences checkbox

@TC6267 @Smoke
Scenario: 6267_Verify that user able to deselect SMS Preferences checkbox under the Email Preferences
Given I relaunch browser and navigate to Property Home page
And I Sign In to Application
| Email                    | Password   |
| QATester101@gmail.com | $Reset123$ |
When I navigate from Home page to My Profile page
And I click on Deselected SMS Preferences checkbox
And I click on 'Save' button
And I click on Selected SMS Preferences checkbox
And I click on 'Save' button
Then I should able to deselect SMS Preferences checkbox
And I should see User menu link at top

@TC6255 @Smoke
Scenario: 01_6255_Verify that if user add family member then 30 % Profile completeness progress bar should be increase
Given I relaunch browser and navigate to Property Home page
And I Create an Account into application
| First Name | Last Name | Postal Code    | Email                   | Password |
| Qatester   | Nextgenqa | As per Property| testingauto38@gmail.com | $Reset123$|
When I navigate from Home page to My Profile page
And I click on 'About Me' edit icon
And I enter Phone Number
    |Phone|
    |1234567891|
And I click on 'Save' button
And I click Continue button
And I observe progress Bar
And I click on Add Family Member link
And I enter First Name in family
    |FirstName|
    |Qatester1|
And I enter Last Name in family 
    |LastName|
    |Nextgenqa1|
And I click on 'Save' button
Then I should able to add family member
    |AddedFamilyName|
    |Qatester1 Nextgenqa1|
And I should able to see progress bar increased by 30 % of Profile completeness
And I should see User menu link at top


@TC6256 @Smoke
Scenario: 6256_Verify that if user should edit added family member
Given I relaunch browser and navigate to Property Home page
And I Create an Account into application
| First Name | Last Name | Postal Code    | Email                   | Password |
| Qatester   | Nextgenqa | As per Property| testingauto38@gmail.com | $Reset123$|
When I navigate from Home page to My Profile page
And I click on 'About Me' edit icon
And I enter Phone Number
    |Phone|
    |1234567891|
And I click on 'Save' button
And I click Continue button
And I observe progress Bar
And I click on Add Family Member link
And I enter First Name in family
    |FirstName|
    |Qatester1|
And I enter Last Name in family 
    |LastName|
    |Nextgenqa1|
And I click on 'Save' button
And I click on My Family edit icon
And I edit First Name
       |FirstName|
       |qatester123|
And I click on 'Save' button
Then I should able to see edited family member information
| FirstName | LastName  |
| qatester123 | Nextgenqa1 |
And I should see User menu link at top


@TC6258 @Smoke
Scenario: 02_6258_Verify that if user delete added family member then 30 % Profile completeness progress bar should be decrease.
Given I relaunch browser and navigate to Property Home page
And I Create an Account into application
| First Name | Last Name | Postal Code    | Email                   | Password |
| Qatester   | Nextgenqa | As per Property| testingauto38@gmail.com | $Reset123$|
When I navigate from Home page to My Profile page
And I click on 'About Me' edit icon
And I enter Phone Number
    |Phone|
    |1234567891|
And I click on 'Save' button
And I click Continue button
And I click on Add Family Member link
And I enter First Name in family
    |FirstName|
    |Qatester1|
And I enter Last Name in family 
    |LastName|
    |Nextgenqa1|
And I click on 'Save' button
And I observe progress Bar
And I click on My Family delete icon
And I click on 'Save' button
Then I should able to delete family member
And Profile completeness progress bar for Family Member should be decreased by 30%
And I should see User menu link at top

@TC6229 @Smoke
Scenario: 6229_Verify that user should login into application after change password from my profile page
Given I relaunch browser and navigate to Property Home page
And I Sign In to Application
| Email                    | Password   |
| devtested333+504@gmail.com  | $Reset123$ |
When I navigate from Home page to My Profile page
And  I enter current password
    |CurrentPassword|
    |$Reset123$|
And I enter New Password
    |NewPassword|
    |$Reset1234$|
And I enter Confirm New Password
    |ConfirmNewPassword|
    |$Reset1234$|
And I click on 'Save' button
And I click SignOut Link
And I click on Sign In link under header section
And I enter Email address
| Email                      | 
| devtested333+504@gmail.com |
And  I enter changed password
    |ChangedPassword|
    |$Reset1234$|
And I click Sign In button
Then I should able to login into application with changed password
And I reset password to original 
 |CurrentPassword|NewPassword|ConfirmNewPassword|
 |$Reset1234$    |$Reset123$ |$Reset123$        |
And I should see User menu link at top


@13571 @Smoke
Scenario: 13571_If user checked 'Received Email Promotions' check box then verify on profile page
Given I am on Property Home page
When I click on Sign In link under header section
And I click on Create an Account button
And I entered valid data on the create account popup
	| First Name | Last Name | Postal Code     | Email                    | Password |
	| Qatester   | NextgenQA | As per Property | testingauto38@gmail.com  | $Reset123$|
And I checked 'Received Email Promotions' check box
And I click on Create button
And I navigate from Home page to My Profile page
Then I should able to see Email Preferences checkbox as selected
And I should see User menu link at top

@13572 @Smoke
Scenario: 13572_If user unchecked 'Received Email Promotions' check box then verify on profile page
Given I am on Property Home page
When I click on Sign In link under header section
And I click on Create an Account button
And I entered valid data on the create account popup
	| First Name | Last Name | Postal Code     | Email                    | Password |
	| Qatester   | NextgenQA | As per Property | testingauto38@gmail.com  | $Reset123$|
And I unchecked 'Received Email Promotions' check box
And I click on Create button
And I navigate from Home page to My Profile page
Then I should able to see Email Preferences checkbox as not selected
And I should see User menu link at top





