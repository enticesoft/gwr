﻿Feature: GroupsPage
	In order to follow groups activity
	As a end user
	I want verify Groups pages

@TC16759 @Smoke
Scenario: 16759_Groups > About - Verify that user able to access About sub menu of group.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups About sub menu
	Then I should see About tab is selected

@TC16760 @Smoke
Scenario: 16760_Groups > About - Verify that user able to see 'Come together for your next event' section on About page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups About sub menu
	Then I should see Come together for your next event section on About Page

@TC16761 @Smoke
Scenario: 16761_Groups > About - Verify that user able to see 'Full Access to Lodge Amenities' section on About page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups About sub menu
	Then I should see Full Access to Lodge Amenities section on About Page
	And I should see Full Access to Lodge Amenities Gallery section on About Page

@TC16762 @Smoke
Scenario: 16762_Groups > About - Verify that user able to see 'Escape to Great Wolf Lodge.' section on About page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups About sub menu
	Then I should see Escape to Great Wolf Lodge on About Page
	And I should see Escape to Great Wolf Lodge grid section on About Page

@TC16763 @Smoke
Scenario: 16763_Groups > About - Verify that user able to see 'Client Review' section on About page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups About sub menu
	Then I should see Client review section on About Page

@TC16764 @Smoke
Scenario: 16764_Groups > About - Verify that user able to see 'Map' section on About page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups About sub menu
	Then I should see Mini map section on About Page

@TC16765 @Smoke
Scenario: 16765_Groups > About - Verify that user able to access Event form on About page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups About sub menu
	And I click on Request proposal button
	Then I should see Event tile form

@TC16766 @Smoke
Scenario: 16766_Groups > Meeting space - Verify that user able to see 'Imagine the possibilities for your event' section on Meeting space page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
    And I click on Groups Meeting space sub menu
	Then I should see  'Imagine the possibilities for your event' section on Meeting space page

@TC16767 @Smoke
Scenario: 16767_Groups > Meeting space - Verify that user able to see PDF of meeting space floor plan
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
    And I click on Groups Meeting space sub menu
	And I click on View Floor Plan button
	Then I should see floor plan on Meeting space page

@TC16768 @Smoke
Scenario: 16768_Groups > Meeting space - Verify that user able to see 'Meeting Space' section on Meeting space page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
    And I click on Groups Meeting space sub menu
	Then I should see Meeting space section on Meeting space page

@TC16769 @Smoke
Scenario: 16769_Groups > Meeting space - Verify that user able to see 'Frequently Asked Questions' section on Meeting space page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
    And I click on Groups Meeting space sub menu
	Then I should see Frequently Asked Questions section on Meeting space page

@TC16770 @Smoke
Scenario: 16770_Groups > Meeting space - Verify that user able to access Event form on About page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
    And I click on Groups Meeting space sub menu
	And I click on Request proposal button
	Then I should see Event tile form

@TC16771 @Smoke
Scenario: 16771_Groups > Suites - Verify that user able to see 'Room Card' section on Suite page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups Suite sub menu
	Then I should see Room card on Groups Suite page

@TC16772 @Smoke
Scenario: 16772_Groups > Suites - Verify that user able to navigate on Suite page when click on 'EXPLORE OUR SPACIOUS SUITES' button
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups Suite sub menu
	And I click on Explore our suite button
	Then I should see navigate on Suite page

@TC16774 @Smoke
Scenario: 16774_Groups > Suites - Verify that user able to see 'Reservations made easy' section on Suite page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups Suite sub menu
	Then I should see Reservation Made Easy section on Suite page

@TC16775 @Smoke
Scenario: 16775_Groups > Suites - Verify that user able to see 'Payment Method' section on Suite page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups Suite sub menu
	Then I should see Payment Method section on Suite page

@TC16777 @Smoke
Scenario: 16777_Groups > Suite - Verify that user able to see 'Frequently Asked Questions' section on Suite page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups Suite sub menu
	Then I should see Frequently Asked Questions section on Meeting space page

@TC16778 @Smoke
Scenario: 16778_Groups > Suite - Verify that user able to access Event form on Suite page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
    And I click on Groups Suite sub menu
	And I click on Request proposal button
	Then I should see Event tile form

@TC16779 @Smoke
Scenario: 16779_Groups > Catering - Verify that user able to see 'Banquets and Catering' section.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups Catering sub menu
	Then I should see 'Banquets and Catering' section on Suite page

@TC16780 @Smoke
Scenario: 16780_Groups > Catering - Verify that user able to see 'Lodge Dining Options' section.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups Catering sub menu
	Then I should see 'Lodge Dining Options' section on Suite page

@TC16781 @Smoke
Scenario: 16781_Groups > Catering - Verify that user able to navigate on Dining page when click on 'ALL LODGE DINING OPTION'.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups Catering sub menu
	And I click on  'ALL LODGE DINING OPTION' button on Catering Page.
	Then I should see user navigate on Dinign page

@TC16782 @Smoke
Scenario: 16782_Groups > Catering - Verify that user able to see 'Frequently Asked Questions' section on Catering page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Groups Catering sub menu
	Then I should see Frequently Asked Questions section on Meeting space page

@TC16783 @Smoke
Scenario: 16783_Groups > Catering - Verify that user able to access Event form on Catering page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
    And I click on Groups Catering sub menu
	And I click on Request proposal button
	Then I should see Event tile form

@TC16786 @Smoke
Scenario: 16786_Groups > Special Events - Verify that user able to see 'We’re here to host' section on Special events section.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Special event sub menu
	Then I should see We here to Host section on Special event page

@TC16787 @Smoke
Scenario: 16787_Groups > Special Events - Verify that user able to navigate on Birthday page when click on 'EXPLORE BIRTHDAY PACKAGES' button.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Special event sub menu
	And I click on  Explore birthday package button special event Page.
	Then I should navigate on birthday page

@TC16788 @Smoke
Scenario: 16788_Groups > Special Events  - Verify that user able to access Event form on Special Event page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Special event sub menu
	And I click on Request proposal button
	Then I should see Event tile form

@TC16789 @Smoke
Scenario: 16789_Groups > Activities - Verify that user able to see 'Book an event in our Water Park' section on Activities page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Activities sub menu
	Then I should see Book an event in our Water Park section on Activity Page

@TC16790 @Smoke
Scenario: 16790_Groups > Activities - Verify that user able to see 'Team building and group activities' section on Activities page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Activities sub menu
	Then I should see Team building and group activities section on Activity Page

@TC16791 @Smoke
Scenario: 16791_Groups > Groups > Activities - Verify that user able to access Event form on Activities page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Activities sub menu
	And I click on Request proposal button
	Then I should see Event tile form

@TC16793 @Smoke
Scenario: 16793_Groups > Event form > Verify that user able to send information with Event type as 'Meeting/Conference'.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Request proposal button of Group Sub menu
	And I fill Tell Us About Your Event Section info
	| Event Type         | Event Date Type | Evnt Start Date | Evnt End Date | Event specific Date |
	| Meeting/Conference | Date Range      | 2021            | 2021          | 2021                |
	And I fill Tell Us About Your self Section info
	Then I should see Thanks You Page after submit information

@TC16794 @Smoke
Scenario: 16794_Groups > Event form > Verify that user able to send information with Event type as 'Other'.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Request proposal button of Group Sub menu
	And I fill Tell Us About Your Event Section info
	| Event Type         | Event Date Type | Evnt Start Date | Evnt End Date | Event specific Date |
	| Other | Date Range      | 2021            | 2021          | 2021                |
	And I fill Tell Us About Your self Section info
	Then I should see Thanks You Page after submit information

@TC16795 @Smoke
Scenario: 16795_Groups > Event form > Verify that user able to send information with Event type as 'Birthday/Celebration' and 'Specific Date' option.
	Given I relaunch browser and navigate to Property Home page
	When I click on Groups header option
	And I click on Request proposal button of Group Sub menu
	And I fill Tell Us About Your Event Section info
	| Event Type         | Event Date Type | Evnt Start Date | Evnt End Date | Event specific Date |
	| Birthday/Celebration | Specific Date      | 2021            | 2021          | 2021                |
	And I fill Tell Us About Your self Section info
	Then I should see Thanks You Page after submit information