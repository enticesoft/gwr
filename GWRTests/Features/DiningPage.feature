﻿Feature: DiningPage
	In order to add particular dining package
	As a end user
	I want to access the GWR Dining page


@TC5780 @Smoke
Scenario: 5780_Verify that user should be navigate on Payment page after click on 'Continue To Payment' button.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I click on Continue to Payment page button
	Then I should see Payment Page Heading

@TC5781 @Smoke
Scenario: 5781_Verify that user should be navigate from dining page to activities page after click on 'Activities' icon.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I navigate Dining page to Activity through Progress bar
	Then I should see Continue to Dining Package button

@TC5782 @Smoke
Scenario: 5782_Verify that user should be navigate from dining page to Suites page after click on 'Suites' icon.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I navigate Dining page to Suite through Progress bar
	Then I Should see Include with Your Stay section

@TC5789 @Smoke
Scenario: 5789_Verify that user see selected suite with option name in cost summary section.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 0    | 0       | AMTCERT    |
	And I select Suite option
	| Suite Option       |
	| Accessible Bathtub |
	And I click on 'Select Room' button of suite option
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see valid suite title with Option Under cost summary
	| Suite Option Only First Word |
	| Accessible                   |

@TC5790 @Smoke
Scenario: 5790_Verify that user see proper adult and Kids count in cost summary section
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 2    | 4       | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see valid adult count
	| Adults |
	| 2      |
	And I should see valid Kid count
	| Kids |
	| 2    |

@TC12855 @Smoke
Scenario: 12855_Verify that user able to add dining packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I click on Dining package Add button
	Then I should see added package in cost summary

@TC12856 @Smoke
Scenario: 12856_Verify that user able to remove added Dining packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I click on Dining package Add button
	And I click on dining package Remove link
	Then I should see added package is removed from cost summary

@TC12857 @Smoke
Scenario: 12857_Verify that user able to add and removed Dining packages again.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I click on Dining package Add button
	And I click on dining package Remove link
	And I click on Dining package Add button
	Then I should see added package in cost summary

@TC12858 @Smoke
Scenario: 12858_Verify that user able to update added dining package quantity under cost summary on dining page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |	
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I select package quanitity
	| Package Quantity |
	| 1                |
	And I click on Dining package Add button
	And I observe package cost for single quantity
	And I select package quanitity
	| Package Quantity |
	| 2                |
	And I click on Dining package Update button
	Then I should see correct package cost after update quantity
	| Second Package Quantity |
	| 2                       |

@TC5788 @Smoke
Scenario: 5788_Verify that user see correct check-In and check-out date in cost summary section.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I observe selected Check In date
	And I observe selected Check Out date
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see correct Check In Date
	And I should see correct Check Out Date

@TC5804 @Smoke
Scenario: 5804_By default selected stay night quantity should be set in package drop down.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see no of nights set as quantity

@TC7517 @Smoke
Scenario: 7517_Verify that 'kids age greater than 3' calculated as billable guest for dining packages on dining page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see correct chareges applied for Kids
	| Billable Guest Count |
	| 3                    |

@TC7516 @Smoke
Scenario: 7516_Verify that 'kids age less than or equal to 3' not calculated as billable guest for dining packages on dining page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see correct chareges applied for Kids
	| Billable Guest Count |
	| 2                    |

@TC5803 @Smoke
Scenario: 5803_Verify that 'Promotional savings' value and 'Your Savings' value should match.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see correct Promotional and Your Saving cost

@TC5798 @Smoke
Scenario: 5798_Verify that 'Suite Total' and 'Total' should same without any package selection or added under cost summary on dining page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see Same suite total and Total when package not added

@TC5793 @Smoke
Scenario: 5793_Verify that user see correct suite total under cost summary section
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see Suite Total equals to Base price minus Promotional Saving price

@TC11361 @Smoke
Scenario: 11361_verify that Person/per day cost should displayed correctly for dining packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see correct person/day per cost for package
	| Billable Guest Count |
	| 3                    |

@TC11362 @Smoke
Scenario: 11362_Verify that packages cost should displayed correctly for per day price.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see correct package total for person/day per package
	| Billable Guest Count |
	| 3                    |
	
@TC5799 @Smoke
Scenario: 5799_Verify that package total should be addition of all packages under cost summary on Dining page
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I added multiple dining packages
	Then I should see correct suite total after added packages
	| No of Days |
	| 1          |

@TC5800 @Smoke
Scenario: 5800_Verify that Total should be correct after adding packages under cost summary section
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I added multiple dining packages
	Then I should see correct total after added packages

@TC12859 @Smoke
Scenario: 12859_Verify that user able to see correct package total after update dining package quantity under cost summary on Dining page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I added multiple dining packages
	Then I should see correct package total after update packages

@TC15457 @Smoke
Scenario: 15457_ Dining > Verify that user able to view details of Dining packages.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I click on Dining view details link
	Then I should see View details is expanded

@TC17626 @Smoke
Scenario: 17626_ Dining Page - By default selected Guest quantity should be set in package drop down for per package dining packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see dining Per Package drop down value if correct
	

