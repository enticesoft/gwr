﻿Feature: RecentSearchesPage
	I want to verify recent search count
	With actual searches 
	on Recent search page

@TC11439 @Smoke
Scenario: 11439_Verify that Guests count displayed correct on 'My Recent Searches' page.
	Given I Open New browser and navigate to Property Home page
	When I Sign In to Application
		| Email Address              | Password   |
		| qtesting874+513@gmail.com | $Reset123$ |
	And I click on My Recent Searches page link
	And I delete recently added all searches
	And I click on Make Reservation Link
	And I navigate from Home page to Suite page with valid data
		| Adults | Kids | Kids1Age | Offer Code |
		| 4      | 0    | 3        | AMTCERT    |
	And I click on My Recent Searches page link
	Then I should see correct Guest count as recently search
		| Adults |
		| 4      |
	And I should see User menu link at top

@TC6137 @Smoke
Scenario: 6137_My Recent Searches Page- User able to delete all recent searches by click on 'Delete All Searches' link on 'My Recent Searches' page
    Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
		| Email Address              | Password   |
		| qtesting874+513@gmail.com | $Reset123$ |
	And I navigate from Home page to Suite page with valid data
		| Adults | Kids | Kids1Age | Offer Code |
		| 4      | 0    | 3        | AMTCERT    |
	And I click on My Recent Searches page link
	And I delete recently added all searches
	Then I Should see Make My reservation Link
	And I should see User menu link at top

@TC6133 @Smoke
Scenario: 6133_My Recent Searches Page- User able to delete recent search on 'My Recent Searches' page
    Given I Open New browser and navigate to Property Home page
	When I Sign In to Application
		| Email Address              | Password   |
		| qtesting874+513@gmail.com | $Reset123$ |
	And I click on My Recent Searches page link
	And I delete recently added all searches
	And I click on Make Reservation Link
	And I navigate from Home page to Suite page with valid data
		| Adults | Kids | Kids1Age | Offer Code |
		| 4      | 0    | 0        | AMTCERT    |
	And I click on My Recent Searches page link
	And I click on Delete button of Recent Search
	And I click on Delete button of Recent Search
	Then I should not see deleted search item
	And I should see User menu link at top

@TC6135 @Smoke
Scenario: 6135_User able to navigate on suite page when click on 'Let's Continue search!' link for recent search on 'My Recent Searches' page
    Given I Open New browser and navigate to Property Home page
	When I Sign In to Application
		| Email Address              | Password   |
		| qtesting874+513@gmail.com | $Reset123$ |
	And I click on My Recent Searches page link
	And I delete recently added all searches
	And I click on Make Reservation Link
	And I navigate from Home page to Suite page with valid data
		| Adults | Kids | Kids1Age | Offer Code |
		| 2      | 1    | 3        | AMTCERT    |
	And I click on My Recent Searches page link
	And I click on Continue search button
	Then I should see Suite page
	And I should see User menu link at top

@TC6138 @Smoke
Scenario: 6138_My Recent Searches Page- User see 'Make a Reservation' link when no any recent search on 'My Recent Searches' page
    Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
		| Email Address              | Password   |
		| qtesting874+513@gmail.com | $Reset123$ |
	And I navigate from Home page to Suite page with valid data
		| Adults | Kids | Kids1Age | Offer Code |
		| 3      | 0    | 0        | AMTCERT    |
	And I click on My Recent Searches page link
	And I click on Delete All Searches button
	Then I should see Make My reservation Link when no any recent searches
	And I should see User menu link at top

@TC11429 @Smoke
Scenario: 11429_Recent Searches- Verify that Maximum count for recent searches under 'Recent searches' list should be five.
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with valid data
		| Adults | Kids | Kids1Age | Offer Code |
		| 4      | 0    | 3        | AMTCERT    |
	And I click Update your stay Button '6' times
	Then I should see recent seraches count as '5'
	And I should see User menu link at top

@TC11436 @Smoke
Scenario: 11436_My Recent Searches - Verify that if user searches the data for two different locations then continue search from My Recent Searches page for location which is not selected then saved search location is getting set again.
    Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
		| Email Address              | Password   |
		| qtesting874+513@gmail.com | $Reset123$ |
	And I navigate from Home page to Suite page with valid data
		| Adults | Kids | Kids1Age | Offer Code |
		| 4      | 0    | 0        | AMTCERT    |
	And I select Another property location
		| Property      |
		| Grapevine, TX |
	And I navigate from Home page to Suite page
		| Adults | Kids | Kids1Age | Offer Code |
		| 2      | 0    | 0        | AMTCERT    |
	And I click on My Recent Searches page link
	And I click on continue search button for not selected property
	Then I should see property selected
		| Property |
		| Chicago  |
	And I should see User menu link at top

@TC6119 @Smoke
Scenario: 6119_Recent Searches- User able to see suite when user click on recent search data in recent search list
    Given I Open New browser and navigate to Property Home page
	When I Sign In to Application
		| Email Address              | Password   |
		| qtesting874+513@gmail.com | $Reset123$ |
	And I click on My Recent Searches page link
	And I delete recently added all searches
	And I click on Make Reservation Link
	#And I click on My Recent Searches page link
	#Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with valid data
		| Adults | Kids | Kids1Age | Offer Code |
		| 4      | 0    | 3        | AMTCERT    |
	#And I search second time for suite
		#| Check In Date | Check Out Date | Adults | Kids | Kids1Age | 
		#|More than 60 days from today|More than 61 days from today| 1 | 0   | 0 |
	And I Click data on Recent seraches list
	Then I should see updated data in Booking widget
	And I should see User menu link at top
