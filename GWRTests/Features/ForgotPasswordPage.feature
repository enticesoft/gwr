﻿Feature: ForgotPasswordPage
	In order to check whether
	user able to change password
	And able to login with new password

@TC6616 @Smoke
Scenario: 01_6616 Forgot Password - Verify the 'You should receive 'Great Wolf Lodge Password Assistance' with Change Password button when user click on send button with valid email id.
	Given I relaunch browser and navigate to Property Home page
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	Then I should see User menu link at top	
	When I click on Sign In link under header section
	And I click on forgot Password link
	And I enter valid Email Address of forgot password	
	And I click on RESET PASSWORD button
	Then I should receive Forgot password email

@TC6643 @Smoke
Scenario: 02_6643 Forgot Password - Verify that when user should login into application with newly Created password.
	Given I relaunch browser and navigate to Property Home page
	When I should receive Forgot password email and click Change Password button
	And I enter new password on Change password pop up
	| New Password |
	| $Reset789$   |
	And I click on Change Password button
	And I click on Continue button
	When I Sign In to Application with newly created Password
	| Password   |
	| $Reset789$ |
	Then I should see User menu link at top

@TC6644 @Smoke
Scenario: 03_6644 Forgot Password - Make sure that user should not able to login into application with Old password.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |
	Then I should see validation message.
	

	