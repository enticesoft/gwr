﻿Feature: DayPass
	In order to Book Day Pass
	As a End User 
	I want to be on Day Pass page

@6284 @smoke
Scenario: 01_6284 Day Pass- Verify that user navigate on Day pass page when click on 'Day Pass' menu link
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	Then I should navigate to Day Pass page

@11796 @smoke
Scenario: 02_11796 Day Pass: Verify that offer related information should display correct under 'PROMO TERMS' information message
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP |
	And I Enter valid date in Arrival Date
	Then I should see Promo Terms Information

@12902 @smoke
Scenario: 03_12902 Day Pass: Verify that promo codes for regular booking should not be valid for day pass booking
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| AMTCERT   |
	And I Enter valid date in Arrival Date
	Then I should Invalid Promo Code Message

@6304 @smoke
Scenario: 04_6304 Day Pass - Verify that if Day pass is selected then 'ADD GUESTS' section, Total and Checkout button should be displayed
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP |
	And I Enter valid date in Arrival Date
	Then I should see ADD GUESTS section
	And I should see Total section
	And I should see Checkout button

@7871 @smoke
Scenario: 05_7871 Day Pass - Verify that user should navigate on the day pass payment page after click on 'Checkout' button
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP |
	And I Enter valid date in Arrival Date
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2 |
	And I click on the Checkout button
	Then I should see DayPass Payment Page

@12867 @smoke
Scenario: 06_12867 Day Pass - Verify that cost summary for guest user on payment page
	Given I am on Property Home page
	When I navigate from Home page to DayPass Payment page
	| PromoCode | GuestQuantity | 
	| TESTWP    | 3             |
	Then I should see correct PromoCode on Payment Page
	| PromoCode |
	| TESTWP    |
	And I should see correct Number of DayPasses on Payment Page
	| GuestQuantity |
	|      03       |
	And I should see correct DayPass Date on Payment Page
	And I should see correct Unit Price on Payment Page
	And I should see correct Sub Total on DayPass Payment Page
	And I should seee correct Taxes amount on DayPass Payment Page
	And I should see correct Total on Payment Page

#@9971 @smoke - Duplicate test case with Home page feature
#Scenario: 07 Homepage: Clickable Tiles: Verify that 'Day Pass' page should be displayed after click on 'Day Pass' tile.
	#Given I am on Property Home page
	#When I click on the DayPass Tile 
	#Then I should navigate to Day Pass page

@12865 @smoke
Scenario: 08_12865 Day Pass> Payment Page: Verify the cost summary on payment page when user select full day pass 
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I Enter valid date in Arrival Date
	And I click on the Full Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity |
	|      2       |
	And I click on the Checkout button
	Then I should see DayPass Payment Page
	And I should see correct Number of DayPasses on Payment Page
	| GuestQuantity |
	|      02       |
	And I should see correct DayPass Date on Payment Page
	And I should see correct Full Day Pass Per Guest Cost on Payment Page	
	And I should see correct DayPass Total for Full day Pass on Payment Page
	And I should seee correct Taxes amount on DayPass Payment Page
	And I should see correct Total on Payment Page for FullDay Pass
	

@8302 @smoke
Scenario: 09_8302 Day Pass > Payment page - User able to log in into application through 'Already have account?' link
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I Enter valid date in Arrival Date
	And I click on the Full Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity |
	|      2       |
	And I click on the Checkout button
	And I Sign In to Application through Already have account link
	| Email Address             | Password   |
	| gwr.nextgenqa1@gmail.com | $Reset123$ |
	Then I should successfully login to application with no error

@10337 @smoke
Scenario: 10_10337 Day Pass> Payment page: Verify the 'Cost Summary' of logged in user on 'Payment' page
	Given I am on Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1@gmail.com  | $Reset123$ |
	And I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button	
	Then I should see correct PromoCode on Payment Page for logged user
	| PromoCode |
	| TESTWP    |
	And I should see correct Number of DayPasses on Payment Page
	| GuestQuantity |
	|      02       |
	And I should see correct DayPass Date on Payment Page
	And I should see correct Unit Price on Payment Page
	And I should see correct Sub Total on DayPass Payment Page
	And I should seee correct Taxes amount on DayPass Payment Page
	And I should see correct Total on Payment Page
	Then I should see User menu link at top

@11553 @smoke
Scenario: 11_11553 Day Pass> Verify that user should not go payment page if quantity is less than 1.
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Checkout button to see error message
	Then I should see Validation message fired for less than one quantity

@11664 @smoke
Scenario: 12_11664 Day pass - Verify that user able to see 'Half day pass' hours as '4pm-Close' on day pass page.
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I Enter valid date in Arrival Date
	Then I should see correct hours for Half Day Pass

@13555 @smoke
Scenario: 13_13555 Day Pass Payment Page: Verify that user should able to see SMS checkbox uncheck as an guest user
	Given I am on Property Home page
	When I navigate from Home page to DayPass Payment page
	| PromoCode | GuestQuantity | 
	| TESTWP    | 3             |
	Then I should see SMS check box Unchecked

@13557 @smoke
Scenario: 14_13557 Day Pass Payment Page: Verify that by default credit card radio button should selected
	Given I am on Property Home page
	When I navigate from Home page to DayPass Payment page
	| PromoCode | GuestQuantity | 
	| TESTWP    | 3             |
	Then I should see by default 'Credit Card' radio button selected in Payment Option section

@13558 @smoke
Scenario: 15_13558 Day Pass Payment Page: Verify that user able to select SMS check box
	Given I am on Property Home page
	When I navigate from Home page to DayPass Payment page
	| PromoCode | GuestQuantity | 
	| TESTWP    | 3             |
	And I check SMS check box
	Then I should see SMS check box checked

@13747 @smoke
Scenario: 16_13747 Day Pass Payment Page: Verify that user able to de-select SMS check box for daypass
	Given I am on Property Home page
	When I navigate from Home page to DayPass Payment page
	| PromoCode | GuestQuantity | 
	| TESTWP    | 3             |
	And I check SMS check box
	And I Uncheck SMS check box
	Then I should see SMS check box Unchecked

@13565 @smoke
Scenario: 17_13565 Day Pass Page: Verify that user should navigate on 'Terms' page after click 'Terms and conditions' link above 'Agree & Book' button
	Given I am on Property Home page
	When I navigate from Home page to DayPass Payment page
	| PromoCode | GuestQuantity | 
	| TESTWP    | 3             |
	And I click on Terms and Conditions link
	Then I should navigate on Terms and Conditions page

@13592 @smoke
Scenario: 18_13592 Day Pass Page: Verify that user should navigate on 'Privacy Policy' page after click 'Privacy Policy' link above 'Agree & Book' button
	Given I am on Property Home page
	When I navigate from Home page to DayPass Payment page
	| PromoCode | GuestQuantity | 
	| TESTWP    | 3             |
	And I click on Privacy Policy link
	Then I should navigate on privacy policy page

@13556 @smoke
Scenario: 19_13556 Day Pass Payment Page: When select PayPal radio button then pay with PayPal button should displayed
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I Enter valid Guest Quantity
	| GuestQuantity |
	|      2       |
	And I click on the Checkout button
	And I click on paypal radio button
	Then I should see pay with paypal button

@13559 @smoke
Scenario: 20_13559 Day Pass Payment Page: Verify that Affirm option should not displayed on day pass payment page
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	#And I click on the Full Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity |
	|      2       |
	And I click on the Checkout button
	Then I should not see Affirm Section

@TC12866 @smoke
Scenario: 21_12866 Day Pass> Payment Page: Verify the cost summary on payment page when user select half day pass 
    Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	Then I should see DayPass Payment Page
	And I should see correct Number of DayPasses on Payment Page
	| GuestQuantity |
	|      02       |
	And I should see correct DayPass Date on Payment Page
	And I should see correct Half Day Pass Per Guest Cost on Payment Page	
	And I should see correct DayPass Total for Half day Pass on Payment Page
	And I should seee correct Taxes amount on DayPass Payment Page
	And I should see correct Total on Payment Page for Half day Pass

@TC11552 @smoke
Scenario: 22_11552 Daypass >Payement page> Verify that 'Your credit card will be charged-----'  under 'Book and Agree Button'  and  'Total Cost' on cost summary on payment page should be same .
    Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
    Then I should able to see that 'Your credit card will be charged-----' under 'Book and Agree Button' and 'Total Cost' on cost summary on payment page are same

@TC9231 @Smoke
Scenario: 23_9231 Day Pass-Verify that user not able to navigate on day pass payment page when enter guest count more than 2 digits in guest field.
    Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter Guest Quantity more than 2 digits
	| GuestQuantity |
	|       999     |
	And I click on the Checkout button to see error message
    Then I should not navigate on day pass payment page


@12869 @smoke
Scenario: 24_12869 Day pass> Reservation page: Verify that when user book day pass then user should see that booked reservation on ‘My reservation’ page for logged user
Given I am on Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| qtesting874+6255@gmail.com     | $Reset123$ |
	And I navigate from Home page to DayPass Payment page
	| PromoCode | GuestQuantity | 
	| TESTWP    | 1             |
	#And I entered payment details on Payment Page for log in user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 1234567890   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I navigate from payment to reservation page
	Then I should see recently booked daypass reservation
	
@12870 @smoke
Scenario: 25_12870 Day pass> Reservation Details page: Verify that when user book day pass then correct reservation details should display on reservation details page
Given I am on Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| qtesting874+6255@gmail.com     | $Reset123$ |
	And I navigate from Home page to DayPass Payment page
	| PromoCode | GuestQuantity | 
	| TESTWP    | 1             |
	#And I entered payment details on Payment Page for log in user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 1234567890   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I navigate from payment to reservation page
	And I navigate to Reservation details page
	Then I should see correct reservation details on reservation Page

@12860 @smoke
Scenario: 26_12860 Day Pass> Confirmation Page: Verify guest user is able to book day pass and reservation details should display correct also user should receive confirmation mail
Given I am on Property Home page
	When I navigate from Home page to DayPass Payment page
	| PromoCode | GuestQuantity | 
	| TESTWP    | 1             |	
	#And I entered payment details on day Pass Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qtesting874+6251@gmail.com	| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see New Confirmation Page Welcome Title
	And I should see Reservation Number			
	And I should see correct date on confirmation 
	And I should see correct pass quantity on confirmation
	And I should see confirmation email received


@12748 @smoke
Scenario: 27_12748  Day Pass > Confirmation Page: Verify that user should able to see cost summary section.
	Given I am on Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1@gmail.com  | $Reset123$ |
	And I navigate from Home page to DayPass page	
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	#And I entered payment details on Payment Page for log in user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see New Confirmation Page Welcome Title
	And I should see Daypass Reservation Number
	And I should see Cost Summary Section
	Then I should see User menu link at top

@12863 @smoke
Scenario: 28_12863 Day Pass Confirmation Page: Verify that user should navigate on the ' Add A Package' page after click on the 'Add Package' page.
	Given I am on Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1@gmail.com  | $Reset123$ |
	And I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	#And I entered payment details on Payment Page for log in user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see New Confirmation Page Welcome Title
	And I should see Daypass Reservation Number
	When I click on the Add a Package button
	And I should see Add a Package Page
	Then I should see User menu link at top

#//==========================================================
@10338 @smoke
Scenario: 10338_Day Pass> Payment page: Verify that user should able to collapse the 'Cost Summary' on 'Day Pass Payment' page.
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I Enter valid Guest Quantity
	| GuestQuantity |
	|      2       |
	And I click on the Checkout button
	And I click on cost summary Tab
	Then I should see cost summary Section Collapse

@14429 @smoke
Scenario: 14429_Day Pass> Payment page: Verify that user should able to expand the 'Cost Summary' on 'Day Pass Payment' page.
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I Enter valid Guest Quantity
	| GuestQuantity |
	|      2       |
	And I click on the Checkout button
	And I double click on cost summary Tab
	Then I should see cost summary Section Expanded

@11557 @smoke
Scenario: 11557_Day Pass> Verify that when user apply invalid promo code then 'INVALID PROMO CODE' error message displayed on day pass page.
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| 123    |
	And I Enter valid date in Arrival Date
	Then I should see Invalid promo code error message

@6301 @smoke
Scenario: 6301_Day Pass> Verify that user able to close 'INVALID PROMO CODE' error message on day pass
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| 123    |
	And I Enter valid date in Arrival Date
	And I click on invalid promo code error message close icon
	Then I should not see Invalid promo code error message

@13594 @smoke
Scenario: 13594_Day Pass Page: Verify that when user select 'Full Day' then 'Total' should display correct according to quantity selected
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I Enter valid date in Arrival Date
	And I click on the Full Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity |
	|      2       |
	Then I should see correct Total on day pass page

@13595 @smoke
Scenario: 13595_Day Pass Page: Verify that when user select 'Half Day' then 'Total' should display correct according to quantity selected
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity |
	|      2       |
	Then I should see correct Total on day pass page having selected half apss


@TC13364 @Smoke
Scenario: 13364_Day Pass > Payment page - Verify that logged user information should be populated.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| qatesternextgenqa9989+501@gmail.com | $Reset123$ |
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I Enter valid Guest Quantity
	| GuestQuantity |
	|      2       |
	And I click on the Checkout button
	Then I should see correct billing address
	| Billing Address |
	| 1700 Nations Dr |
    And I should see correct postal code
	| Postal Code |
	| 60031 |
	And I should see correct city
	| City |
	| Gurnee|
	And I should see correct State
	| State|
	| IL |
	And I should see correct country
	| Country |
	| US |
	And I should see User menu link at top

@TC15465 @smoke
Scenario: 15465_DayPass - Payment page - 'Don't miss out on this great deal! If you book later, there's a chance the price will go up' message displayed on Payment page.
    Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	Then I should see dont miss section on Payment page

@17061 @smoke
Scenario: 17061_Day Pass > CMP -  Verify that user should see 'Attraction', 'Dining', 'Birthday' and 'Cabana' tiles
	Given I am on Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1@gmail.com  | $Reset1234$ |
	And I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should see 'Attraction', 'Dining', 'Birthday' and 'Cabana' tiles on Confirmation page

@17062 @smoke
Scenario: 17062_Day Pass > CMP -  Verify that user able to see Attraction additional section with Title, CTA button.
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should see Attraction Tile title
	And I should see Attraction Tile button

@17063 @smoke
Scenario: 17063_Day Pass > CMP -  Verify that user able to see Birthday additional section with Title, CTA button and Description.
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should see Birthday Tile title
	And I should see Birthday Tile button

@17064 @smoke
Scenario: 17064_Day Pass > CMP -  Verify that user able to see Dining additional section with Title, CTA button.
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should see Dining Tile title
	And I should see Dining Tile button

@17065 @smoke
Scenario: 17065_Day Pass > CMP -  Verify that user able to see Cabana additional section with Title, CTA button.
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should see Cabana Tile title
	And I should see Cabana Tile button

@17066 @smoke
Scenario: 17066_Day Pass > CMP -  Verify that user able to open attraction section pop up and verify attraction passes details.
	Given I am on Property Home page
	When I navigate from Home page to DayPass page
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add Passes button of Attraction tile
	Then I should see Pup pass title on Pop up
	And I should see Paw pass title on Pop up
	And I should see Wolf pass title on Pop up
	And I should see Pup pass See All Include link on Pop up
	And I should see Paw pass See All Include link on Pop up
	And I should see Wolf pass See All Include link on Pop up
	And I should see Pup pass See Price Section on Pop up
	And I should see Paw pass See Price Section on Pop up
	And I should see Wolf pass See Price Section on Pop up

@17071 @smoke
Scenario: 17071_Day Pass > CMP -  Verify that user able to open Dining section pop up and verify Dining packages details.
	Given I am on Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Dining page to verify Cost and Title
	And I navigate from Home page to DayPass page	
	When I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add Package button of Dining tile
	Then I should see Title is correct on Pop up
	And I should see See What You Get link on Pop up
	And I should see correct cost on dining Pop up

@17073 @smoke
Scenario: 17073_Day Pass > CMP -  Verify that user able to click on Birthday Add Package button and verify Birthday packages details on Add A Package page.
	Given I am on Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Activity Birthday tab to verify Cost and Title
	And I navigate from Home page to DayPass page	
	When I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add Package button of Birthday tile
	Then I should see Birthday Titles are correct
	And I should see Birthday packages cost are correct
	And I should see Birthday packages Add button
	And I should see Birthday packages count

@17074 @smoke
Scenario: 17074_Day Pass > CMP -  Verify that user able to click on Add A Cabana button and verify Cabana packages details on Add A Package page.
	Given I am on Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Home page to DayPass page	
	When I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Cabana button of Cabana tile New confirmation
	Then I should see valid reservation number
	And I should see valid cabana titles
	And I should see valid cabana price

@17076 @smoke
Scenario: 17076_Day Pass > CMP -  Verify that user able to click on 'See All Include' link of attraction pass and verify passes details on 'Add A Pass To Your Stay' pop up.
	Given I am on Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Dining page to verify Cost and Title
	And I navigate from Home page to DayPass page		
	When I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Package button of Dining tile
	And I click on See What You Get link
	Then I should see Add Package Details section
	And I should see Title is correct on Dining Packages Pop up
	And I should see Add Package Price section
	And I should see Image section on Modal
	And I should see correct cost on dining package Pop up

@17077 @smoke
Scenario: 17077_Day Pass > CMP -  Verify that user able to click on 'SeeWhat You Get link' of Dining Package pop up and verify passes details on 'Dinign Packages' pop up.
	Given I am on Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Home page to DayPass page		
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add Passes button of Attraction tile
	And I click on See All Includes link
	Then I should see Attraction Pass Details section
	And I should see Attraction Pass Price section
	And I should see Image section on Modal

@17085 @smoke
Scenario: 17085_Day Pass > CMP -  Verify that user able to enable keep me posted service using See More Details pop up.
	Given I am on Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I Sign In to Application
	| Email Address              | Password   |
	| qtesting874+6255@gmail.com | $Reset123$ |
	When I navigate from Home page to DayPass page		
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on SEE MORE DETAILS link
	And I Subscribe the SMS sign up
	Then I should see subscribe message 
	And I unsubscribe the SMS sign up		 
	And I should see unsubcribe message

@17086 @smoke
Scenario: 17086_Day Pass > CMP -  Verify that user able to Deselect keep me posted service using See More Details pop up.
	Given I am on Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	#We have covered unsubscribe functionality inside #17085 to avoid reservation.

@17078 @smoke
Scenario: 17078_Day Pass > CMP -  Verify that user able to add Attraction pass from Additional package tile of attraction on CMP page.
	Given I am on Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Home page to DayPass page	
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add Passes button of Attraction tile
	And I click on Add button of pass
	Then I should see added different packages cost are correct
	And I should see correct total in cost summary of added packages
	When I click on cost summarry close link

@17082 @smoke
Scenario: 17082_Day Pass > CMP -  Day Pass > CMP -  Verify that user able to add Dining package from Additional package tile of Dining on CMP page.
	Given I am on Property Home page
	When I click on cost summarry close link
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Dining page to verify Cost and Title
	And I navigate from Home page to DayPass page		
	When I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Package button of Dining tile
	And  I click on Add button of pass
	Then I should see added different packages cost are correct
	And I should see correct total in cost summary of added packages
	When I click on cost summarry close link

@17083 @smoke
Scenario: 17083_Day Pass > CMP -  Verify that user able to add Birthday package from Additional package tile of Birthday on CMP page.
	Given I am on Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Activity Birthday tab to verify Cost and Title
	And I navigate from Home page to DayPass page	
	When I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add Package button of Birthday tile
	And I click on Add buttons of Birthday Packages
	Then I should see correct total in cost summary of added birthday packages
	And I should see correct total in cost summary of added packages
	When I click on Save to My Reservation button
	Then I should see Birthday Package tile on Confirmation page

@17084 @smoke
Scenario: 17084_Day Pass > CMP -  Verify that user able to add Cabana package from Additional package tile of Cabana on CMP page.
	Given I am on Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Activity Birthday tab to verify Cost and Title
	And I navigate from Home page to DayPass page	
	When I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Cabana button of Cabana tile New confirmation
	And I added available Cabanas
	Then I should see valid cabana price in cost summarry
	Then I should see correct suite total in cost summary of added cabana packages
	Then I should see correct total in cost summary of added packages
	Then I should see correct total and due at check in at cost summary of added packages
	When I click on Save to My Reservation button
	Then I should see added packages notification message
	When I click on Indoor cabana view link and verify cost summary if cabana is added


@17784 @smoke
Scenario: 17784_Day Pass> New confirmation Page: When user add package on CMP page then user should received email on respective email ID.
	Given I am on Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Dining page to verify Cost and Title
	And I navigate from Home page to DayPass page		
	When I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Package button of Dining tile
	And  I click on Add button of pass
	And I click on cost summarry close link
	Then I should see Add A Package confirmation email received

@17851 @smoke
Scenario: 17851_Day Pass> New Reservation Page:  Verify that cost summary should be reflected correct in mail which received for added packages after booking into respective mail id.
	Given I am on Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Home page to DayPass page		
	When I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Package button of Dining tile
	And  I click on Add button of pass
	When I should observe Cost summary on CMP Page for Day Pass
	Then I should see Added Packages cost summary into email
	And I should see correct Suite total on email
	And I should see correct Suite price on email
	And I should see correct Package total on email


@12753 @smoke
Scenario: 12753_Day Pass> Add A Package Page: Verify that user should able to add cabana for available date and should receive mail to respective id for added cabana
	Given I am on Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Activity Birthday tab to verify Cost and Title
	And I navigate from Home page to DayPass page	
	When I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Cabana button of Cabana tile
	And I added available Cabanas
	And I click on Save to My Reservation button
	Then I should see Cabana confirmation email received

@17964 @smoke
Scenario: 17964_Day Pass> Confirmation Page: Verify that user able to see correct cabana calculation on cabana reservation email.
	Given I am on Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Activity Birthday tab to verify Cost and Title
	And I navigate from Home page to DayPass page	
	When I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button	
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add A Cabana button of Cabana tile
	And I added available Cabanas
	And I click on Save to My Reservation button
	When I click on Indoor cabana view link and verify reservation number
	And I click on Cost Summarry button
	And I should observe Cabana cost summary on CMP Page
	Then I should see Added Packages cost summary into email
	And I should see Added Cabana Packages cost summary into email
	And I should see correct Suite total on email
	And I should see correct Suite price on email

@15728 @smoke
Scenario: 15728_Day pass>Payment Page - Verify that user not able to see flex trip section on day pass payment page
	Given I am on Property Home page		
	When I navigate from Home page to DayPass page	
	And I enter valid Promo Code details
	| PromoCode | 
	| TESTWP    |
	And I Enter valid date in Arrival Date
	And I click on the Half Day Pass
	And I Enter valid Guest Quantity
	| GuestQuantity | 
	| 2             |
	And I click on the Checkout button
	Then I should not see 'Add flexibility to your trips’ section

	