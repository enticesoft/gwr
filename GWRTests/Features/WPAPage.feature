﻿Feature: WPAPage
	I want to verify water park packages 
	And 
	Also need information regarding it

@TC10352 @Smoke
Scenario: 10352_WPA - Verify that when user hover mouse on the 'Water Park & Attraction' menu then following submenu should get display
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	Then I should see Swim,Spalsh,Slide submneu
	And I should see Activities submenu
	And I should see Attraction submenu
	And I should see Fitness submenu
	And I should see Explore By Age submenu
	And I should see Calendar submenu
	
@TC10394 @Smoke
Scenario: 10394_WPA>Activities- Verify that 'Activities' page should get open when user click on the 'Activities' submenu.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on 'Activities' submenu
	Then I should see Activities page is opened

@TC10403 @Smoke
Scenario: 10403_WPA>Activities- Verify that packages should display on 'Activities' page
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on 'Activities' submenu
	Then I should see only activities packages available

@TC10465 @Smoke
Scenario: 10465_WPA>Attractions: Verify that when clicked on 'Attractions' submenu , 'Attraction' page should get opened.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Attractions submenu
	Then I should see Attractions page is opened

@TC10475 @Smoke
Scenario: 10475_WPA>Attractions: Verify that all the packages are showing for 'Attractions' package only on 'Attraction' page.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Attractions submenu
	Then I should see only Attractions packages available

@TC10507 @Smoke
Scenario: 10507_WPA>Fitness: Verify that 'Fitness' page opens when clicked on 'Fitness' submenu.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Fitness submenu
	Then I should see Fitness page is opened

@TC10515 @Smoke
Scenario: 10515_WPA>Fitness: Verify that only fitness packages are showing on 'Fitness' page.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Fitness submenu
	Then I should see only Fitness packages available

@TC10371 @Smoke
Scenario: 10371_WPA>Swim, Splash, Slide- Verify that 'Swim, Splash, Slide' page should get open when user click on the 'Swim, Splash, Slide' submenu.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Swim Splash Slide submenu
	Then I should see Swim splash slide page is opened

@TC10545 @Smoke
Scenario: 10545_WPA>Calendar: Verify that user navigate to 'Calendar' page when clicked 'Calendar' submenu.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Calendar submenu
	Then I should see Calendar page is opened

	@TC10438 @Smoke
Scenario: 10438_WPA>Explore by Age : Verify that when user mouse hover on 'Waterpark and Attraction' then 'Explore by Age' sub menu should be displayed there.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Explore By Age submenu
	Then I should see Explore By Age page is opened	

	
	@TC10380 @Smoke
Scenario: 10380_WPA>Swim, Splash, Slide- Verify that packages should display on 'Swim, Splash, Slide' page

Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Swim Splash Slide submenu
	Then I should see Swim splash slide packages available 


		
	@TC10449 @Smoke
Scenario: 10449_WPA>Explore by Age : Verify that when user on 'Explore by Age' page then all category attractions should be displayed on page.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Explore By Age submenu
	Then I should see all type of packages available
	
				
	@TC10462 @Smoke
Scenario: 10462_WPA>Explore by Age : Verify that when user selected only Children check box and other are uncheck then only Children related attractions displayed below
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Explore By Age submenu
	And I uncheck all checked filter checkboxes	
	And I check only Children checkbox
	Then I should see only children related packages displaying

	#

@TC10459 @Smoke
Scenario:10459_WPA>Explore by Age : Verify that when user selected only family check box and other are uncheck then only family related attractions displayed below.
    Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Explore By Age submenu
	And I uncheck all checked filter checkboxes	
	And I check Family checkbox
	Then I should see only family related packages displaying

	@TC10461 @Smoke
Scenario:10461_WPA>Explore by Age : Verify that when user selected only Teens check box and other are uncheck then only teens related attractions displayed below.
    Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Explore By Age submenu
	And I uncheck all checked filter checkboxes	
	And I check Teens checkbox
	Then I should see only Teens related packages displaying

	@TC10463 @Smoke
Scenario:10463_WPA>Explore by Age : Verify that when user selected only toddler check box and other are uncheck then only toddler related attractions displayed below.
    Given I Open New browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Explore By Age submenu
	#And I uncheck all checked filter checkboxes	
	And I check Toddler checkbox
	Then I should see only Toddler related packages displaying

@TC10460 @Smoke
Scenario: 10460_WPA>Explore by Age : Verify that when user selected only Adult check box and other are uncheck then only adult related attractions displayed below.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Explore By Age submenu
	And I uncheck all checked filter checkboxes 
	And I check only adult checkbox
	Then I should see only adult related packages dispalying

@TC15426 @Smoke
Scenario: 15426_WPA - WAP > Indoor Water Park > Verify that user navigate on T3 page and verify page  details.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Swim Splash Slide submenu
	And I click on See More Details link
	Then I should see Title of package
	#And I should see Available Hours section
	And I should see Thumbnail section
	And I should see description section

@TC15427 @Smoke
Scenario: 15427_WAP > Indoor Water Park > Verify that user navigate on T3 page and Safety rules and link functionality.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Swim Splash Slide submenu
	And I click on See More Details link
	And I click on View Safety link
	Then I should see Safety Rule section

@TC15428 @Smoke
Scenario: 15428_WAP > Activities > Verify that user navigate on T3 page and verify page details.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on 'Activities' submenu
	And I click on Activities learn more link
	Then I should see Activity Title of package
	And I should see Available Hours section
	And I should see Thumbnail section
	And I should see Best For section

@TC15429 @Smoke
Scenario: 15429_WAP > Attractions > Verify that user navigate on T3 page and verify page details.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Attractions submenu
	And I click on Activities learn more link
	Then I should see Activity Title of package
	And I should see Available Hours section
	And I should see Thumbnail section
	And I should see Best For section

@TC15430 @Smoke
Scenario: 15430_WAP > fitness > Verify that user navigate on T3 page and verify following details.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I click on Fitness submenu
	#And I click on Activities learn more link
	Then I should see Activity Title of package
	#And I should see Thumbnail section
	And I should see Best For section

