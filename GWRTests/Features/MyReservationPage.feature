﻿Feature: MyReservationPage
	In order to verify reservation details and add More packages

@TC6021 @Smoke
Scenario: 01_6021 Verify that when reservation not able in Upcoming reservation then 'WE CANNOT FIND ANY UPCOMING RESERVATIONS... message should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1+3072@gmail.com | $Reset123$ |	
	And I navigate on My Reservation Page
	Then I should see Upcoming reservation val message
	Then I should see User menu link at top

@TC6022 @Smoke
Scenario: 02_6022 Verify that when reservation not able in Past reservation then 'WE CANNOT FIND ANY PAST RESERVATIONS.... message should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1+3072@gmail.com | $Reset123$ |	
	And I navigate on My Reservation Page
	Then I should see past reservation val message
	Then I should see User menu link at top

@TC6023 @Smoke
Scenario: 03_6023 Verify that when reservation not able in Cancelled reservation then 'WE CANNOT FIND ANY CANCELLED RESERVATIONS..... message should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1+3072@gmail.com | $Reset123$ |	
	And I navigate on My Reservation Page
	Then I should see cancelled reservation val message
	Then I should see User menu link at top

@TC12876 @Smoke
Scenario: 04_12876 Verify that when user don’t have reservations then user can see 'Find your reservation by Confirmation #' and 'Make a Reservation' link should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1+3072@gmail.com | $Reset123$ |	
	And I navigate on My Reservation Page
	Then I should see Find Your Reservation by Confirmation link
	Then I should see Make Reservation link
	Then I should see User menu link at top

@TC6025 @Smoke
Scenario: 05_6025 Verify that user able to navigate on Plan page using 'Make a Reservation' link.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1+3072@gmail.com | $Reset123$ |	
	And I navigate on My Reservation Page
	And I click on Make Reservation link
	Then I should see Booking Widget
	And I should see User menu link at top

@TC6008 @Smoke
Scenario: 06_6008 Verify that user able to search reservation by confirmation number on Find your reservation pop up
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| gwr.nextgenqa1+3072@gmail.com | $Reset123$ |	
	And I navigate on My Reservation Page
	And I click on Find Your Reservation by Confirmation link
	And I enter valid Suite confimation number and last name
	| Confirmation Number | Last Name |
	| Confirmation Number from excel | Nextgenqa |
	And I click on Search button
	Then I should see New Confirmation Page Welcome Title
	And I should see Lodge Location tile
	And I should see Select Dates tile
	And I should see Guest tile
	And I should not see Find Your Reservation by Confirmation link
	#Write now we are facing issue so above code not able to automate


@TC6063 @Smoke
Scenario: 07_6063 Verify that user is on Add A Package page from My reservation and verify dining packages details.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I navigate on My Reservation Page
	And I click on View Details button
	And I click on Add A Package button
	Then I should see valid reservation number
	And I should see valid suite suite title
	And I should see valid Dining titles
	And I should see valid Dining Package Cost
	And I should see valid offer code in cost summary
	Then I should see User menu link at top

@TC6064 @Smoke
Scenario: 08_6064 Verify that user is on Add A Package page from My reservation and verify that able to add Dining packages and cost summary should be correct.
	Given I relaunch browser and navigate to Property Home page		
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |	
	When I navigate on My Reservation Page
	And I click on View Details button
	And I click on Add A Package button
	And I click on Packages Add button
	Then I should see added packages titles are correct
	Then I should see added packages cost are correct
	Then I should see correct suite total in cost summary of added packages
	Then I should see correct total in cost summary of added packages
	Then I should see correct total and due at check in at cost summary of added packages
	Then I should see User menu link at top

@TC12921 @Smoke
Scenario: 09_12921 Verify that user save added package into my reservation on click 'Save to My reservation'.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |	
	When I navigate on My Reservation Page
	And I click on View Details button
	And I click on Add A Package button
	And I click on Packages Add button	
	Then I should see added packages titles are correct
	Then I should see added packages cost are correct
	When I click on Save to My Reservation button
	Then I should see added packages notification message
	When I click on Cost Summarry button
	Then I should see correct suite total in cost summary of added packages
	Then I should see correct total in cost summary of added packages
	Then I should see correct total and due at check in at cost summary of added packages
	When I click on cost summarry close link
	Then I should see User menu link at top
	

@TC6066 @Smoke
Scenario: 10_6066 Verify that user is on Add A Pass page from My reservation and verify Activity packages details.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining     |	
	#And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I navigate on My Reservation Page
	And I click on View Details button
	And I click on Add A Pass button
	Then I should see valid reservation number
	And I should see valid suite suite title
	And I should see valid Activity Pass titles
	And I should see valid Acivity Packages Cost
	And I should see valid offer code in cost summary

@TC6067 @Smoke
Scenario: 11_6067 Verify that user is on Add A Pass page from My reservation and verify that able to add Activity packages and cost summary should be correct.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |		
	And I navigate on My Reservation Page
	And I click on View Details button
	And I click on Add A Pass button
	And I click on Activity Packages Add button
	Then I should see added activity packages titles are correct
	Then I should see added packages cost are correct for Activity Pass
	Then I should see correct suite total in cost summary of added activity packages
	Then I should see correct total in cost summary of added packages
	Then I should see correct total and due at check in at cost summary of added packages

@TC12922 @Smoke
Scenario: 12_12922 Verify that user save added activity package into my reservation on click 'Save to My reservation'
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |		
	When I navigate on My Reservation Page
	And I click on View Details button
	And I click on Add A Pass button
	And I click on Activity Packages Add button
	Then I should see added activity packages titles are correct
	Then I should see added packages cost are correct for Activity Pass
	When I click on Save to My Reservation button
	Then I should see added packages notification message
	When I click on Cost Summarry button
	Then I should see correct suite total in cost summary of added activity packages
	Then I should see correct total in cost summary of added packages
	Then I should see correct total and due at check in at cost summary of added packages
	When I click on cost summarry close link
	Then I should see User menu link at top

@TC12923 @Smoke
Scenario: 13_12923 Verify that user is on Add Cabana from My reservation and verify Cabana packages details.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass           | Add Dining     |	
	#And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I navigate on My Reservation Page
	And I click on View Details button
	And I click on Add Cabana button
	Then I should see valid reservation number
	And I should see valid suite suite title
	And I should see valid cabana titles
	And I should see valid cabana price
	Then I should see User menu link at top

@TC12924 @Smoke
Scenario: 14_12924 Verify that user is on Add Cabana from My reservation and verify that able to add Cabana packages and cost summary should be correct.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |	
	When I navigate on My Reservation Page
	And I click on View Details button
	And I click on Add Cabana button
	And I added available Cabanas
	Then I should see valid cabana price in cost summarry
	Then I should see correct suite total in cost summary of added cabana packages
	Then I should see correct total in cost summary of added packages
	Then I should see correct total and due at check in at cost summary of added packages
	Then I should see User menu link at top

@TC12925 @Smoke
Scenario: 15_12925 Verify that user save added cabana package into my reservation on click 'Save to My reservation'
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |	
	When I navigate on My Reservation Page
	And I click on View Details button
	And I click on Add Cabana button
	And I added available Cabanas
	Then I should see valid cabana price in cost summarry
	When I click on Save to My Reservation button
	Then I should see added packages notification message
	When I click on Indoor cabana view link and verify cost summary if cabana is added
	Then I should see User menu link at top
	

@TC6050 @Smoke
Scenario: 16_6050 Verify that all added packages displayed under cost summary with correct costing on My reservation details page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists | Add Pass           | Add Dining     |
	And I observe suite total on Payment Page
	And I observe taxes on Payment Page
	And I observe Resort fee Payment Page
	And I observe total Payment Page
	And I observe Due Today price on Payment Page
	And I observe Due at check in price on Payment Page
	#And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I navigate on My Reservation Page
	And I click on View Details button
	When I click on Cost Summarry button
	Then I should see correct Suite Total on Cost Summarry
	And I should see correct taxes price on Cost Summarry
	And I should see correct resort fee on Cost Summarry
	And I should see correct Total cost on Cost Summarry
	And I should see correct deposite paid on Cost Summarry
	And I should see correct Due at check in cost on Cost Summarry
	When I click on cost summarry close link
	Then I should see User menu link at top

@TC12941 @Smoke
Scenario: 17_12941 Verify that Checking (4:00PM) and Checkout (2:00PM) time should be displayed when user book suite with 2 p.m. Late Check-out under the 'Stay Dates' section.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |	
	When I navigate on My Reservation Page
	And I click on View Details button
	Then I should not see Add LCO button on CMP as we added booking time
	Then I should see 2 P.M. Check Out text as LCO added booking time

@TC5969 @Smoke
Scenario: 18_5969 Verify that available contents of 'UPCOMING RESERVATION' section.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |		
	When I navigate on My Reservation Page
	Then I should see valid reservation number on My reservation Page
	Then I should see valid Guest count on My reservation Page
	#Then I should see valid Check In date on My reservation Page
	#Then I should see valid Check Out date on My reservation Page

@TC12926 @Smoke
Scenario: 19_12926 E2E - Verify that when user created reservation but no packages added at booking time then from My reservation user should be go in Add A Package > Add A Pass > Add A Cabana> Event Calendar flow and verify cost summary.
	Given I Open New browser and navigate to Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number		
	When I navigate on My Reservation Page
	And I click on View Details button
	And I click on Add A Package button
	And I click on Packages Add button	
	Then I should see added packages titles are correct
	Then I should see added packages cost are correct
	When I click on Save to My Reservation button
	Then I should see added packages notification message
	When I click on Add A Pass button
	And I click on Activity Packages Add button
	Then I should see added activity packages titles are correct
	Then I should see added packages cost are correct for Activity Pass
	When I click on Save to My Reservation button
	Then I should see added packages notification message	
	When I click on Add Cabana button
	And I added available Cabanas
	Then I should see valid cabana price in cost summarry
	When I click on Save to My Reservation button if cabana available
	When I click on Cost Summarry button
	Then I should see added different packages cost are correct
	And I should see correct total in cost summary of added packages
	When I click on cost summarry close link
	Then I should see User menu link at top

@TC12927 @Smoke
Scenario: 20_12927 E2E - Verify that when user created reservation and added only activity packages added at booking time then from My reservation user should be go in Add A Package > Add A Cabana> Event Calendar.
	Given I Open New browser and navigate to Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass           | Add Dining1     |	
	#And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number		
	When I navigate on My Reservation Page
	And I click on View Details button
	And I click on Add A Package button
	And I click on Packages Add button	
	Then I should see added packages titles are correct
	Then I should see added packages cost are correct
	When I click on Save to My Reservation button
	Then I should see added packages notification message	
	When I click on Add Cabana button
	And I added available Cabanas
	Then I should see valid cabana price in cost summarry
	When I click on Save to My Reservation button if cabana available
	When I click on Cost Summarry button
	Then I should see added different packages cost are correct
	And I should see correct total in cost summary of added packages
	When I click on cost summarry close link
	Then I should see User menu link at top

@TC12928 @Smoke
Scenario: 21_12928 E2E - Verify that when user created reservation and added only dining packages added at booking time then from My reservation user should be go in Add A Pass > Add A Cabana> Event Calendar.
	Given I Open New browser and navigate to Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining     |	
	#And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number		
	When I navigate on My Reservation Page
	And I click on View Details button
	When I click on Add A Pass button
	And I click on Activity Packages Add button
	Then I should see added activity packages titles are correct
	Then I should see added packages cost are correct for Activity Pass
	When I click on Save to My Reservation button
	Then I should see added packages notification message	
	When I click on Add Cabana button
	And I added available Cabanas
	Then I should see valid cabana price in cost summarry
	When I click on Save to My Reservation button if cabana available
	When I click on Cost Summarry button
	Then I should see added different packages cost are correct
	And I should see correct total in cost summary of added packages
	When I click on cost summarry close link
	Then I should see User menu link at top

@TC12929 @Smoke
Scenario: 22_12929 E2E - Verify that when user created reservation and added only cabana packages after booking time then from My reservation user should be go in Add A Package > Add A Pass> Event calendar.
	Given I Open New browser and navigate to Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add Cabana button on Confirmation
	And I added available Cabanas
	Then I should see valid cabana price in cost summarry
	When I click on Save to My Reservation button if cabana available
	When I navigate on My Reservation Page
	And I click on View Details button
	And I click on Add A Package button
	And I click on Packages Add button	
	Then I should see added packages titles are correct
	Then I should see added packages cost are correct
	When I click on Save to My Reservation button
	Then I should see added packages notification message	
	When I click on Add A Pass button
	And I click on Activity Packages Add button
	Then I should see added activity packages titles are correct
	Then I should see added packages cost are correct for Activity Pass
	When I click on Save to My Reservation button
	Then I should see added packages notification message	
	When I click on Cost Summarry button
	Then I should see added different packages cost are correct
	And I should see correct total in cost summary of added packages
	When I click on cost summarry close link
	Then I should see User menu link at top

@TC15996 @Smoke
Scenario: 15996_Verify that user able to see upgrade room price on room type section on My Reservation detail Page
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I navigate on My Reservation Page
	And I click on View Details button
	Then I should see Room Type section on Confimation Page
	And I should see Room Type price section on Confimation Page
	And I should see Upgrade Now button on Confimation Page

@TC15997 @Smoke
Scenario: 15997_Verify that user navigate on 'Nor1upgrades' site when click on 'Upgrade Now' button on room type section on My Reservation detail page
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I navigate on My Reservation Page
	And I click on View Details button
	When I click on Upgrade Now button
	Then I should see Nor upgrade page

@TC17746 @Smoke
Scenario: 17746_My Reservation Page: Verify the add a package flow when landing from different property to My Reservation page
	Given I Open New browser and navigate to Property Home page	
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	And I entered payment details on Payment Page for newly created user
	| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I entered Contact Info for Guest user
	#|First Name  |Last Name  |Email  | Phone Number |
	#|QATest	|Nextgenqa	|gwr.nextgenqa1+27Nov@gmail.com		| 9383227772   |	
	#And I entered Payment and billing for Guest user
	#| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I navigate to Gurnee property page
	When I navigate on My Reservation Page
	And I click on View Details button
	Then I should see user navigate on valid property

	







	






