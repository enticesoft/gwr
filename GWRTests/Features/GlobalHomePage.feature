﻿Feature: GlobalHomePage
	In order to follow the booking flow for particular property
	As a end user
	I want to access the GWR Global Home page

@TC12838 @Smoke
Scenario: 12838_Verify that region wise properties list open when user click on 'Choose a Location' button.
	Given I am on GWR Global Home page
	When I click on Choose a Location button
	Then I should see region wise location list

@TC8271 @Smoke
Scenario: 8271_Verify that user navigate to property home page when select property.
	Given I am on GWR Global Home page
	When I click on Choose a Location button
	And I select a property
	Then I land on home page of property


	 