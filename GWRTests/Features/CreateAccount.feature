﻿Feature: CreateAccount
	In order to use GWR portal
	As a user
	I want to create an account

@TC5515 @smoke
Scenario: 5515_Verify that user is able to create account with entering valid all fields data on 'CREATE AN ACCOUNT' pop up.
	Given I am on Property Home page
	When I click on Sign In link under header section
	And I click on Create an Account button
	And I entered valid data on the create account popup
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	Then I should see User menu link at top
	And I should receive GWR Welcome email

@TC11076 @smoke
Scenario: 11076_User able to sign up on 'Sign up for exclusive deals!' popup with valid data
	Given I Open New browser and navigate to Property Home page and wait for Lead Gen Pop up
	When I wait for Led Gen popup to appear
	And I enter valid data on the Led Gen popup
	| First Name | Last Name | Postal Code | Email |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com |
	And I click on Sign Up button
	Then I should see Success message
	And I should receive GWR Welcome email

@TC5528 @smoke
Scenario: 5528_Verify that 'Privacy Policy' & Contact Us' links should be displayed when enter Canadian postal code in 'Postal Code' on 'CREATE AN ACCOUNT' pop up
	Given I am on Property Home page
	When I click on Sign In link under header section
	And I click on Create an Account button
	And I enter Postal code Under postal code field
	| Postal Code |
	| T5T4M2      |
	Then I should see 'Privacy Policy' & 'Contact Us' links displayed on create account pop up

@TC5527 @smoke
	Scenario: 5527_Verify that 'I agree to receive Great Wolf Resorts' check box should be displayed when user enter Canadian postal code in 'Postal Code' on 'CREATE AN ACCOUNT' pop up
	Given I am on Property Home page
	When I click on Sign In link under header section
	And I click on Create an Account button
	And I enter Postal code Under postal code field
	| Postal Code |
	| T5T4M2      |
	Then I should able to see 'I agree to receive Great Wolf Resorts' check box

@TC5521 @smoke
Scenario: 5521_Verify that user switch on 'Sign In' pop up when user click on 'SIGN IN' link on 'CREATE AN ACCOUNT' pop up
    Given I am on Property Home page
	When I click on Sign In link under header section
	And I click on Create an Account button
	And I click on Sign In link on 'CREATE AN ACCOUNT' pop up
	Then I should navigate 'Sign In' pop up

@TC5497 @smoke
Scenario: 5497_Verify that 'Create' button should be enable after entering Email id and valid Password
    Given I am on Property Home page
	When I click on Sign In link under header section
	And I click on Create an Account button
	And I entered Email and Password on the create account popup
	| Email                    | Password |
	| gwr.nextgenqa1@gmail.com | $Reset123$|
	Then I should see that 'Create' button get enabled

@TC15466 @smoke
	Scenario: 15466_ When user use Canadian code then Privacy policy link is accessible.
	Given I am on Property Home page
	When I click on Sign In link under header section
	And I click on Create an Account button
	And I enter Postal code Under postal code field
	| Postal Code |
	| T5T4M2      |
	Then I should see Privacy policy link

@TC15467 @smoke
	Scenario: 15467_When user use Canadian code then Contact Us link is accessible
	Given I am on Property Home page
	When I click on Sign In link under header section
	And I click on Create an Account button
	And I enter Postal code Under postal code field
	| Postal Code |
	| T5T4M2      |
	Then I should see Contact Us link

@TC5518 @smoke
Scenario: 5518_Create Account-Verify that user is able to sign up with Google account
	Given I am on Property Home page
	When I click on Sign In link under header section
	And I click on Create an Account button
	And I Sign Up with valid details using google sign up
	| Email                    | Password   |
	| gwr.nextgenqa1@gmail.com | $Reset123$ |
	Then I should see User menu link at top

@TC5520 @smoke
Scenario: 5520_Create Account-Verify that user is able to sign up with Facebook account
	Given I Open New browser and navigate to Property Home page
	When I click on Sign In link under header section
	And I click on Create an Account button
	And I click on Sign In with Facebook button
	And I enter valid login details on Facebook
	| Email                    | Password   |
	| petercooper123@gmail.com | $Reset123$ |
	Then I should see User menu link at top





	