﻿Feature: HeaderSuite
    In order to get details about Suites 
	As a end user
	I want to access the GWR Suites

@10791 @Smoke
Scenario: 10791_Verify the functionality of 'Check Availability' button on Premium suites page
Given I Open New browser and navigate to Property Home page
When I click on Suites menu under Header
And I click on Suites menu under Header
And I click on Premium Suites type
And I click on Check Availability Button under any premium suite 
And I enter valid details on Booking Widget
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Test data excel | Test data excel | 2      | AMTCERT    |
And I click on Book Now button
Then I should navigate to Suite Page
#And I should see Premium Tab is selected


@10717 @Smoke
Scenario: 10717_Verify the functionality of 'Check Availability' button on Standard suites page
Given I Open New browser and navigate to Property Home page
When I click on Suites menu under Header
And I click on Standard Suites type option
And I click on Check Availability Button under any Standard suite 
And I enter valid details on Booking Widget
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Test data excel | Test data excel | 2      | AMTCERT    |
And I click on Book Now button
Then I should navigate to Suite Page
#And I should see Standard Tab is selected

@10754 @Smoke
Scenario: 10754_Verify the functionality of 'Check Availability' button on Themed suites page
Given I Open New browser and navigate to Property Home page
When I click on Suites menu under Header
And I click on Themed Suites type option
And I click on Check Availability Button under any Themed suite 
And I enter valid details on Booking Widget
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Test data excel | Test data excel | 2      | AMTCERT    |
And I click on Book Now button
Then I should navigate to Suite Page
#And I should see Themed Tab is selected

@10787 @Smoke
Scenario:10787_Verify that user able to see Premium related Suites on Premium Suites page
Given I relaunch browser and navigate to Property Home page
When I click on Suites menu under Header
And I click on Premium Suites type
Then I should able to see Only Premium type Suites on Premium Suites page

@10713 @Smoke
Scenario: 10713_Verify that user able to see Standard related Suites on Standard Suites page
Given I relaunch browser and navigate to Property Home page
When I click on Suites menu under Header
And I click on Standard Suites type option
Then I should able to see Only standard type Suites on Standard Suites page

@10750 @Smoke
Scenario: 10750_Verify that user able to see Themed related Suites on Themed Suites page
Given I relaunch browser and navigate to Property Home page
When I click on Suites menu under Header
And I click on Themed Suites type option
Then I should able to see Only Themed type Suites on Themed Suites page

@15437 @Smoke
Scenario: 15437_Suites > Standard > Verify that user navigate on T3 page and verify following details.
Given I relaunch browser and navigate to Property Home page
When I click on Suites menu under Header
And I click on Standard Suites type option
And I click on Activities learn more link
Then I should see suite title
And I should see Thumbnail section
And I should see suite floor plan
And I should see suite Check Availability button
And I should see suite sleep section
And I should see suite description section

@15438 @Smoke
Scenario: 15438_Suites > Standard > Verify that when click on Check Availability button then navigate on Plan page.
Given I relaunch browser and navigate to Property Home page
When I click on Suites menu under Header
And I click on Standard Suites type option
And I click on Activities learn more link
And I click on suite check availability button
Then I should see Booking Widget

@15439 @Smoke
Scenario: 15439_SuitesSuites > Themed > Verify that when click on Check Availability button then navigate on Plan page.
Given I relaunch browser and navigate to Property Home page
When I click on Suites menu under Header
And I click on Themed Suites type option
And I click on Activities learn more link
And I click on suite check availability button
Then I should see Booking Widget

@15440 @Smoke
Scenario: 15440_Suites > Premium > Verify that when click on Check Availability button then navigate on Plan page.
Given I relaunch browser and navigate to Property Home page
When I click on Suites menu under Header
And I click on Premium Suites type
And I click on Activities learn more link
And I click on suite check availability button
Then I should see Booking Widget

