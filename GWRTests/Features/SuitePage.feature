﻿Feature: SuitePage
	In order to follow the booking flow for particular property
	As a end user
	I want to access the GWR Suite page

@TC5640 @Smoke
Scenario: 5640_Suite Page - Verify that when user search a suite then Include with Your Stay section should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 3        | AMTCERT    |
	Then I Should see Include with Your Stay section
	

@TC7245 @Smoke
Scenario: 7245_Suite Page - Verify that when user click on Select button then LCO pop should be displayed.
     Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 2    | 3        | AMTCERT    |
	And I click on 'Select Room' button
	Then I should see LCO pop appear
		
@TC7246 @Smoke
Scenario: 7246_Suite Page -  Verify that Late check out details should be displayed LOC Pop Up.
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I click on 'Select Room' button
	Then I should see LCO pop appear
	And I should see LCO title
	And I should see LCO Price
	And I should see LCO Description
	And I should see LCO 'Yes, Add Late Checkout' and 'No Thanks' buttons.
	
@TC11562 @Smoke
Scenario: 11562_Suite Page - Verify that when user search a suite then on Theme tab to standard suite then user can see Standard suite details.
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	#And I Click on Standard Tab
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | NoPremium   | Standard     | No               |
	#Then I should see Tab Title as Standard
	Then I should see Suite Title
	And I should see Base and Max occupancy section
	And I should see Suite description
	And  I should see Check rate calendar link
	And I should see Suite price section.
	   	
@TC11565 @Smoke
Scenario: 11565_Suite Page - Verify that when user search a suite then on Theme tab to Premium suite then user can see Premium suite details.
     Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	#And I Click on Premium Tab
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | Premium   | NoStandard     | No               |
	#Then I should see Tab Title as Premium
	Then I should see Suite Title
	And I should see Base and Max occupancy section
	And I should see Suite description
	And I should see Check rate calendar link
	And I should see Suite price section.

#@TC12942 @Smoke
#Scenario: 12942_Suite Page - Verify that when user search suite then user navigate on Themed tab by default.
	#Given I relaunch browser and navigate to Property Home page
	#When I navigate from Home page to Suite page
	#| Adults | Kids | Kids1Age | Offer Code |
	#| 2      | 1    | 3        | AMTCERT    |
	#Then I should see Themed Tab is selected By default

@TC5615 @Smoke
Scenario: 5615_Suite Page - Verify that when user search a suite then on Theme tab user can see Theme suite details.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	#Then I should see Themed tab title
	Then I should see Suite title
	And I should see Base and Max occupancy section
	And I should see Suite description
	And I should see Check rate calendar link
	And I should see Suite price section. 

	@TC5626 @Smoke
Scenario: 5626_Suite Page - Verify that when user search suite with Offer code then Applied offer code should be displayed on Suite info.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	Then I should see Applied offer code into suite info

	@TC5616 @Smoke
Scenario: 5616_Suite Page - Verify that when user search suite without Offer code then 'Best Available Rate' should be displayed on Suite info.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        |     |
	Then I should see Best Available Rate into suite info

	@TC4125 @Smoke
Scenario: 4125_Suite Page - Verify that when user select accessible room toggle then for suites accessible tag should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | NoPremium   | NoStandard     | Yes               |
	#And I select Accessible Rooms Only toggle
	And I click on Update your Stay
	Then I should see Accessible label for suites

@TC12911 @Smoke
Scenario:12911_Suite Page - Verify that when user select occupancy is 1 as greater than max occupancy then suite should not be available for section.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I select Adult greater than Max Occupancy
	And I click on Update your Stay
	Then I should see Exceeded Maximum Occupancy alert
	
@TC12917 @Smoke
Scenario: 12917_Verify that when user select occupancy is 1 greater than base occupancy but select as child and age is 3 or greater than 3 then suite price should be increasing by $50.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      |   0  |     0    | AMTCERT    |
	#And I observe Suite price
	And I observe Suite price to price verification
	And I select Kids and update stay
	| Kids | Kids1Age |
	| 1   | 3        | 
	Then I should see suite price should be a increased by dollar 50


@TC12910 @Smoke
	Scenario: 12910_Verify that when user select occupancy is 1 greater than base occupancy then suite price increasing by $50.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      |   0	|     0    | AMTCERT    |
	#And I observe Suite price
	And I observe Suite price to price verification
	And I select adult greater than base and update stay
	#And I select adult and update stay
	#| Adults |
	#| 5   |
	Then I should see suite price should be a increased by dollar 50


@TC12916 @Smoke
Scenario: 12916_Verify that when user select occupancy is greater than base occupancy but select as child and age is 2 then suite price should not be increasing by $50.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      |   0  |     0    | AMTCERT    |
	#And I observe Suite price
	And I observe Suite price to price verification
	And I select Kids and update stay
	| Kids | Kids1Age |
	| 1   | 2        | 
	Then I should see suite price should not be a increased by dollar 50


	
@TC12943 @Smoke
Scenario: 12943_Verify that when user use Max occupancy then suite showing as 'Exceeded Maximum Occupancy' error message and 'Not Available' label
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      |   1  |     4    | AMTCERT    |
	And I select Adult greater than Max Occupancy
	And I click on Update your Stay
	Then I should see Exceeded Maximum Occupancy alert message for suite


@TC12662 @Smoke
Scenario: 12662_Verify that user able to see BBAR related suites after close 'Offer Code Error' pop up
	Given I relaunch browser and navigate to Property Home page
	When I select check In date
	And I select check Out date
	And I select Adults
	| Adults |
	| 2   |
	And I enter Offer Code
	| Offer Code |
	|   MOREFUN  |
	And I click on Book Now button
	And I close Offer code error pop up
	Then I should see BBAR related suites

@TC7252 @Smoke
Scenario: 7252_Verify that when user click on 'No Thank' button on LCO pop up then user should be navigate on Activity page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0   | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	Then I should see Continue to Dining Package button


@TC7251 @Smoke
Scenario: 7251_Verify that when user click on 'Yes, Add Late Check Out' button on LCO pop up then user should be navigate on Activity page and LCO price should be added correctly.
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I observe LCO Price
	And I click on Add Late Check Out button on LCO pop up
	Then I should see LCO price is correct on Activity Page

@TC13004 @Smoke
Scenario: 13004_Suite Page - Verify that suite price not considering kids having age 2 and under it. 
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      |   0  |          |            |
	And I observed price for above BW criteria
	And I select Kid with age two and update stay
	| Kids | Kids1Age |
	| 1    | 1        | 
	Then I should see suite price should not be a increased

@TC15453 @Smoke
Scenario:15453_Affirm link is accessible on Suite page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      |   0  |          |            |
	And I click on Affirm link under Suite
	Then I should see Affirm sandbox pop up open

@TC12674 @Smoke
Scenario:12674_Suite Page - Verify that user able to open room filter and see room categories as  'Themed', 'Standard' and 'Premium' after click on 'Room Type' filter. 
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I click on Room Type drop down
	Then I should see Theme room type option
	And I should see Premium room type option
	And I should see Standard room type option

@TC12711 @Smoke
Scenario:12711_Suite Page - Verify that user able Close 'Room Type' filter by pop up.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I click on Room Type drop down
	And I click on Close Room Type link
	Then I should see Not see Theme room type option

@TC12714 @Smoke
Scenario:12714_Suite Page - Verify that user able to select 'Theme check' box and Theme type suite should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| Theme   | NoPremium   | NoStandard     | No               |
	Then I should see Themed suites

@TC12719 @Smoke
Scenario:12719_Suite Page - Verify that user able to select 'Standard check' box and Standard type suite should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | NoPremium   | Standard     | No               |
	Then I should see Standard suites

@TC12720 @Smoke
Scenario:12720_Suite Page - Verify that user able to select 'Premium check' box and Premium type suite should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | Premium   | NoStandard     | No               |
	Then I should see Premium suites

@TC13462 @Smoke
Scenario:13462_Suite Page-Verify that user able to see 'Lowest to Highest Price', 'Highest to Lowest Price' and 'Recommended for you' categories under 'Sort By' filter.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I click on Sort Option fillter dropdown
	Then I should see Lowest To Highest Sort Option
	And I should see Highest to Lowest Sort Option
	And I should see Recommended Sort Option

@TC13463 @Smoke
Scenario:13463_Suite Page - Verify that when user click on 'Lowest to Highest Price' then suite sort accordingly.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I click on Sort Option fillter dropdown
	And I click on Low to High sort option
	Then I should see Suite sorted as Low to High

@TC13466 @Smoke
Scenario:13466_Suite Page - Verify that when user click on 'Highest to Lowest  Price' then suite sort accordingly.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I click on Sort Option fillter dropdown
	And I click on High to Low sort option
	Then I should see Suite sorted as High to Low

@TC13657 @Smoke
Scenario:13657_Suite Page- Verify that 'RECOMMENDED FOR YOU' label should display on premium suite when sort by 'Recommended for you' filter.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I click on Sort Option fillter dropdown
	And I click on Recommended sort option
	Then I should see Recommended label for suite

@TC16478 @Smoke
Scenario:16478_Suite Page - Verify that user able to clear filter by clicking on Clear filter link.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | Premium   | NoStandard     | No               |
	And I click on Clear filter link
	Then I should not see clear filter link


#@TC12912 @Smoke
#Scenario: E2E_12912 - Verify that logged in user search a suite and complete a booking with Theme suite and after booking add a package activity.
	#Given I relaunch browser and navigate to Property Home page
	#When I navigate from Home page to Suite page
	#| Adults | Kids | Kids1Age | Offer Code |
	#| 4      | 1   | 3        | AMTCERT    |
	#And I Sign In to Application
	#| Email Address             | Password   |
	#| gwr.nextgenqa1@gmail.com | $Reset123$ |
	#And I Select Room Type and accessible suite
	#| ThemeType | PremiumType | StandardType | AccessibleToggle |
	#| Theme   | NoPremium   | NoStandard     | No               |
	#And I navigate from Suite page to Payment page
	#And I observe suite total on Payment Page
	#And I observe taxes on Payment Page
	#And I observe Resort fee Payment Page
	#And I observe total Payment Page
	#And I observe Due Today price on Payment Page
	#And I observe Due at check in price on Payment Page
	#And I entered payment details on Payment Page for log in user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I click on See You Soon Pop Up All Done button
	#Then I should see New Confirmation Page Welcome Title
	#And I should see Reservation Number
	#And I should see Cabana Section
	#When I click on Cost Summarry button
	#Then I should see correct Suite Total on Cost Summarry
	#And I should see correct taxes price on Cost Summarry
	#And I should see correct resort fee on Cost Summarry
	#And I should see correct Total cost on Cost Summarry
	#And I should see correct deposite paid on Cost Summarry
	#And I should see correct Due at check in cost on Cost Summarry
	#When I click on cost summarry close link
	#Then I should see User menu link at top

#@TC12913 @Smoke
#Scenario: E2E_12913 - Verify that logged in user search a suite and complete a booking with Standard suite and after booking add a package activity.
	#Given I relaunch browser and navigate to Property Home page
	#When I navigate from Home page to Suite page
	#| Adults | Kids | Kids1Age | Offer Code |
	#| 2      | 1   | 3        | AMTCERT    |
	#And I Sign In to Application
	#| Email Address             | Password   |
	#| gwr.nextgenqa1@gmail.com | $Reset123$ |
	#And I Select Room Type and accessible suite
	#| ThemeType | PremiumType | StandardType | AccessibleToggle |
	#| NoTheme   | NoPremium   | Standard     | No               |
	#And I navigate from Suite page to Payment page
	#And I observe suite total on Payment Page
	#And I observe taxes on Payment Page
	#And I observe Resort fee Payment Page
	#And I observe total Payment Page
	#And I observe Due Today price on Payment Page
	#And I observe Due at check in price on Payment Page
	#And I entered payment details on Payment Page for log in user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I click on See You Soon Pop Up All Done button
	#Then I should see New Confirmation Page Welcome Title
	#And I should see Reservation Number
	#And I should see Cabana Section
	#When I click on Cost Summarry button
	#Then I should see correct Suite Total on Cost Summarry
	#And I should see correct taxes price on Cost Summarry
	#And I should see correct resort fee on Cost Summarry
	#And I should see correct Total cost on Cost Summarry
	#And I should see correct deposite paid on Cost Summarry
	#And I should see correct Due at check in cost on Cost Summarry
	#When I click on cost summarry close link
	#Then I should see User menu link at top

#@TC12914 @Smoke
#Scenario: E2E_12914 - Verify that logged in user search a suite and complete a booking with Premium suite and after booking add a package activity.
	#Given I relaunch browser and navigate to Property Home page
	#When I navigate from Home page to Suite page
	#| Adults | Kids | Kids1Age | Offer Code |
	#| 2      | 1   | 3        | AMTCERT    |
	#And I Sign In to Application
	#| Email Address             | Password   |
	#| gwr.nextgenqa1@gmail.com | $Reset123$ |
	#And I Select Room Type and accessible suite
	#| ThemeType | PremiumType | StandardType | AccessibleToggle |
	#| NoTheme   | Premium   | NoStandard     | No               |
	#And I navigate from Suite page to Payment page
	#And I observe suite total on Payment Page
	#And I observe taxes on Payment Page
	#And I observe Resort fee Payment Page
	#And I observe total Payment Page
	#And I observe Due Today price on Payment Page
	#And I observe Due at check in price on Payment Page
	#And I entered payment details on Payment Page for log in user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I click on See You Soon Pop Up All Done button
	#Then I should see New Confirmation Page Welcome Title
	#And I should see Reservation Number
	#And I should see Cabana Section
	#When I click on Cost Summarry button
	#Then I should see correct Suite Total on Cost Summarry
	#And I should see correct taxes price on Cost Summarry
	#And I should see correct resort fee on Cost Summarry
	#And I should see correct Total cost on Cost Summarry
	#And I should see correct deposite paid on Cost Summarry
	#And I should see correct Due at check in cost on Cost Summarry
	#When I click on cost summarry close link
	#Then I should see User menu link at top

#@TC12915 @Smoke
#Scenario: E2E_12915 - Verify that Guest user search a suite and complete a booking with Theme suite and after booking add a package activity.
	#Given I relaunch browser and navigate to Property Home page
	#When I navigate from Home page to Suite page
	#| Adults | Kids | Kids1Age | Offer Code |
	#| 2      | 1   | 3        | AMTCERT    |
	#And I Select Room Type and accessible suite
	#| ThemeType | PremiumType | StandardType | AccessibleToggle |
	#| Theme   | NoPremium   | NoStandard     | No               |
	#And I navigate from Suite page to Payment page
	#And I observe suite total on Payment Page
	#And I observe taxes on Payment Page
	#And I observe Resort fee Payment Page
	#And I observe total Payment Page
	#And I observe Due Today price on Payment Page
	#And I observe Due at check in price on Payment Page
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I click on See You Soon Pop Up All Done button
	#Then I should see New Confirmation Page Welcome Title
	#And I should see Reservation Number
	#And I should see Cabana Section
	#When I click on Cost Summarry button
	#Then I should see correct Suite Total on Cost Summarry
	#And I should see correct taxes price on Cost Summarry
	#And I should see correct resort fee on Cost Summarry
	#And I should see correct Total cost on Cost Summarry
	#And I should see correct deposite paid on Cost Summarry
	#And I should see correct Due at check in cost on Cost Summarry


#@TC11207 @Smoke
#Scenario: 11207_Verify that for guest user '2PM LOC' should not display under 'Stay Date' section if user added it while booking.
	#Given I Open New browser and navigate to Property Home page
	#When I navigate from Home page to Suite page
	#| Adults | Kids | Kids1Age | Offer Code |
	#| 2      | 1   | 3        | AMTCERT    |	
	#And I create an account to Application
	#| First Name | Last Name | Postal Code | Email                      | Password |
	#| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	#And I click on Create button
	#And I Select Room Type and accessible suite
	#| ThemeType | PremiumType | StandardType | AccessibleToggle |
	#| Theme   | NoPremium   | NoStandard     | No               |
	#And I navigate from Suite page to Payment page with Packages choice
	#| LCO Package       | Attraction Package | Dining Package |
	#| Add LCO If Exists | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	#And I click on See You Soon Pop Up All Done button
	#Then I should see Reservation Number
	#And I should not see Add LCO button on CMP as LCO not available at booking time