﻿Feature: SignIn
	In order to use GWR portal
	As a user
	I want to Sign In to the account

@TC11337 @smoke
Scenario: 01_11337_'Sign-In: Brute Force: Verify that "Suspicious Account Activity" mail should receive on user respective mail.
	Given I Open New browser and navigate to Property Home page
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	Then I should see User menu link at top
	When I Sign In to Application with newly created Email and invalid password
	| Password   |
	| $Reset124$ |	
	Then I should receive Suspicious Account Activity email


@TC11344 @smoke
Scenario: 02_11344_'Sign-In: Brute Force Email: Verify that user able to unlock the account using 'Unlock account' link
	Given I Open New browser and navigate to Property Home page
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	Then I should see User menu link at top
	When I Sign In to Application with newly created Email and invalid password
	| Password   |
	| $Reset124$ |	
	Then I should click on unlock link Suspicious Account Activity email
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |
	Then I should see User menu link at top
	
@TC11348 @smoke
Scenario: 03_11348_'Sign-In: Brute Force Email: Verify that user able to reset password using 'Reset Password' link
	Given I Open New browser and navigate to Property Home page
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	Then I should see User menu link at top
	When I Sign In to Application with newly created Email and invalid password
	| Password   |
	| $Reset124$ |
	Then I should click on Reset link Suspicious Account Activity email
	When I enter new password on Change password pop up
	| New Password |
	| $Reset789$   |
	And I click on Change Password button
	And I click on Continue button
	Then I should see user navigate on Plan pageafter change password

@TC11350 @smoke
Scenario: 04_11350_'Sign-In: Brute Force Email: Verify that user should able to login into application with new password.
	Given I Open New browser and navigate to Property Home page
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset789$ |
	Then I should see User menu link at top

@TC5554 @smoke
Scenario: 05_5554_Verify that user is able to login into application with GWR account.
	Given I Open New browser and navigate to Property Home page
	When I click on Sign In link under header section
	And I enter valid login details
	| Email                    | Password   |
	| gwr.nextgenqa1+0510@gmail.com | $Reset123$ |
	And I click Sign In button
	Then I should see User menu link at top

@TC5558 @smoke
Scenario: 06_5558_Verify that user is able to sign in with Facebook account
	Given I Open New browser and navigate to Property Home page
	When I click on Sign In link under header section
	And I click on Sign In with Facebook button
	And I enter valid login details on Facebook
	| Email                    | Password   |
	| petercooper123@gmail.com | $Reset123$ |
	Then I should see User menu link at top

@TC5556 @smoke
Scenario: 07_5556_Verify that user is able to sign in with Google account
	Given I Open New browser and navigate to Property Home page
	When I login with valid details using google sign in
	| Email                    | Password   |
	| gwr.nextgenqa1@gmail.com | $Reset123$ |
	Then I should see User menu link at top

@TC11335 @smoke
Scenario: 08_11335_Sign-In: Brute Force: Verify that if user enter correct email id and correct password for second time then user should able to login into application.
	Given I Open New browser and navigate to Property Home page
	When I click on Sign In link under header section
	And I enter valid login details
	| Email                    | Password   |
	| gwr.nextgenqa1+0530@gmail.com | $Reset124$ |
	And I click Sign In button
	And I click Sign In pop up close link
	And I click on Sign In link under header section
	And I enter valid login details
	| Email                    | Password   |
	| gwr.nextgenqa1+0530@gmail.com | $Reset123$ |
	And I click Sign In button
	Then I should see User menu link at top

@TC11335 @smoke
Scenario: 09_11331_'Sign-In: Brute Force: Verify that when user do 5 invalid attempts while 'Sign In' to application by entering invalid password then that account should get locked.
	Given I Open New browser and navigate to Property Home page
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	Then I should see User menu link at top
	When I Sign In to Application with newly created Email and invalid password
	| Password   |
	| $Reset124$ |	
	Then I should see User locked message on Sign In pop up
	When I click Sign In pop up close link
	And I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |
	Then I should see User menu link at top

@TC11333 @smoke
Scenario: 10_11333_'Sign-In: Brute Force: Verify that remains count of the attempt should display correct under error message when user enter invalid password.
	Given I Open New browser and navigate to Property Home page
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	Then I should see User menu link at top
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset124$ |
	Then I should see User locked message on Sign In pop up after first invalid attempt

@TC11336 @smoke
Scenario: 11_11336_Sign-In: Brute Force: Verify that "Sorry there were more than 5 attempts. Your account is locked. Please verify your mail to unlock your account' error message should display when user enter more than 5 attempts.
	Given I Open New browser and navigate to Property Home page
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	Then I should see User menu link at top
	When I Sign In to Application with newly created Email and invalid password
	| Password   |
	| $Reset124$ |	
	Then I should see User locked message on Sign In pop up




	


