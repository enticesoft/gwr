﻿Feature: X_SanityTest_Scenarios
	In order to take main functional test 
	And do Sanity test
	around the application

#====================================================== Sign In ====================================================================

@TC5554 @smoke @SignIN
Scenario: 01_5554_Verify that user is able to login into application with GWR account.
	Given I Open New browser and navigate to Property Home page
	When I click on Sign In link under header section
	And I enter valid login details
	| Email                    | Password   |
	| gwr.nextgenqa1+0510@gmail.com | $Reset123$ |
	And I click Sign In button
	Then I should see User menu link at top

@TC5558 @smoke @SignIN
Scenario: 02_5558_Verify that user is able to sign in with Facebook account
	Given I Open New browser and navigate to Property Home page
	When I click on Sign In link under header section
	And I click on Sign In with Facebook button
	And I enter valid login details on Facebook
	| Email                    | Password   |
	| petercooper123@gmail.com | $Reset123$ |
	Then I should see User menu link at top

@TC5556 @smoke @SignIN
Scenario: 03_5556_Verify that user is able to sign in with Google account
	Given I Open New browser and navigate to Property Home page
	When I login with valid details using google sign in
	| Email                    | Password   |
	| gwr.nextgenqa1@gmail.com | $Reset123$ |
	Then I should see User menu link at top

#=====================================================Create an Account =============================================================

@TC5515 @smoke @CreateAccount
Scenario: 5515_Verify that user is able to create account with entering valid all fields data on 'CREATE AN ACCOUNT' pop up.
	Given I am on Property Home page
	When I click on Sign In link under header section
	And I click on Create an Account button
	And I entered valid data on the create account popup
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	Then I should see User menu link at top
	And I should receive GWR Welcome email

@TC11076 @smoke @CreateAccount
Scenario: 11076_User able to sign up on 'Sign up for exclusive deals!' popup with valid data
	Given I Open New browser and navigate to Property Home page and wait for Lead Gen Pop up
	When I wait for Led Gen popup to appear
	And I enter valid data on the Led Gen popup
	| First Name | Last Name | Postal Code | Email |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com |
	And I click on Sign Up button
	Then I should see Success message
	And I should receive GWR Welcome email


#===================================================== Forgot password ==============================================================

@TC6616 @Smoke @ForgotPassword
Scenario: 01_6616 Forgot Password - Verify the 'You should receive 'Great Wolf Lodge Password Assistance' with Change Password button when user click on send button with valid email id.
	Given I relaunch browser and navigate to Property Home page
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	Then I should see User menu link at top	
	When I click on Sign In link under header section
	And I click on forgot Password link
	And I enter valid Email Address of forgot password	
	And I click on RESET PASSWORD button
	Then I should receive Forgot password email

@TC6643 @Smoke @ForgotPassword
Scenario: 02_6643 Forgot Password - Verify that when user should login into application with newly Created password.
	Given I relaunch browser and navigate to Property Home page
	When I should receive Forgot password email and click Change Password button
	And I enter new password on Change password pop up
	| New Password |
	| $Reset789$   |
	And I click on Change Password button
	And I click on Continue button
	When I Sign In to Application with newly created Password
	| Password   |
	| $Reset789$ |
	Then I should see User menu link at top

@TC6644 @Smoke @ForgotPassword
Scenario: 03_6644 Forgot Password - Make sure that user should not able to login into application with Old password.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application with newly created Email
	| Password   |
	| $Reset123$ |
	Then I should see validation message.

#====================================================== Home Page ===================================================================
@TC12839 @smoke @Home
Scenario: 12839_Verify that user see booking engine on home page.
	Given I am on GWR Global Home page
	When I click on Choose a Location button
	And I select a property
	Then I land on home page of property
	And I should see Booking Widget

@TC13195 @Smoke @Home
Scenario: 13195_Home Page > Global Deal - Verify that 'sign up now' button displayed instead of 'Apply Code' and 'Sign In' link instead of 'Offer code' for exclusive deal
     Given I relaunch browser and navigate to Property Home page
	 When I click on Deal Bar on Home page.
	 Then I observe exclusive deal have sign up button when user is not logged In
	 And I observe exclusive deal have sign in link when user is not logged In
	 When I click on Deal Bar close link on Home page.
	 And I click on Sign In link under header section
	 And I enter valid login details
	 | Email                    | Password   |
	 | gwr.nextgenqa1+0510@gmail.com | $Reset123$ |
	 And I click Sign In button
	 And I click on Deal Bar on Home page.
	 Then I should see exclusive deal have Apply Offer Code button when user is logged In
	 And I should see exclusive deal have Offer Code when user is logged In

@TC13196 @Smoke @Home
Scenario: 13196_Home Page > Global Deal - Verify that create an account pop up opens when user click on 'Sign Up' CTA .
     Given I Open New browser and navigate to Property Home page
	 When I click on Deal Bar on Home page.
	 And I click on Sign Up button of Global deal on Home page.
	 Then I should see Create account pop is open.

@TC13198 @Smoke @Home
Scenario: 13198_Home Page > Global Deal -Verify that Sign in text link opens the sign in modal when clicked on it.
     Given I Open New browser and navigate to Property Home page
	 When I click on Deal Bar on Home page.
	 And I click on Sign In button of Global deal on Home page.
	 Then I should see Sign In pop is open.

@TC17549 @smoke @Home
Scenario: 17549_User should not see Promo code on T3 page and Booking engline when navigate through Home Page Main banner when Sing in require.
    Given I Open New browser and navigate to Property Home page
	When I click on View Offer button
	Then I should not see Promo code on inside booking engine
	And I should not see Promo code on inside Deal Info section
	
@TC17567 @smoke @Home
Scenario: 17567_User should see Promo code on T3 page and Booking engline when navigate through Home Page Main banner when Sing in require.
    Given I Open New browser and navigate to Property Home page
	 When I click on Sign In link under header section
	 And I enter valid login details
	 | Email                    | Password   |
	 | gwr.nextgenqa1+0510@gmail.com | $Reset123$ |
	 And I click Sign In button
	 And I click on View Offer button
	 And I Select Date with Applied offer code
	 Then I should see correct offercode applied for suites

#============================================ Plan Page ===========================================================

@TC9411 @smoke @PlanPage
Scenario: 9411_Verify that user able to see validation error message when select dates after 60 days from today for MOREFUN offer code.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates more than 60 days
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  More than 60 days from today             |        2 days more than Check In date        |    2    |     MOREFUN       |
	And I click Book Now button
	Then I should see validation error

@TC9429 @smoke @PlanPage
Scenario: 9429_Verify that user able to see validation error message when select dates within 60 days from today for ESAVER offer code.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within 60 days
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  Within 60 days from today             |        2 days more than Check In date        |    2    |     ESAVER       |
	And I click Book Now button
	Then I should see validation error

@TC12659 @smoke @PlanPage
Scenario: 12659_Verify that user able to see correct message on pop up when applied 'MOREFUN'  with 1 night stay.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within 60 days and 1 night
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  Within 60 days from today             |        2 days more than Check In date        |    2    |     MOREFUN       |
	And I click Book Now button
	Then I should see correct message for MOREFUN offer code

@TC12658 @smoke @PlanPage
Scenario: 12658_Verify that user able to see 'Offer Code Error' pop up when applied 'MOREFUN'  with 1 night stay.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within 60 days and 1 night
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  Within 60 days from today             |        2 days more than Check In date        |    2    |     MOREFUN       |
	And I click Book Now button
	Then I should see correct message for MOREFUN offer code

@TC9427 @smoke @PlanPage
Scenario: 9427_Verify that user able to see offer code related suites when applied 'ESAVER' offer code.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates more than 60 days
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  More than 60 days from today             |        2 days more than Check In date        |    2    |     ESAVER      |
	And I click Book Now button
	Then I should navigate to Suite Page

@TC9424 @smoke @PlanPage
Scenario: 9424_Verify that user able to see offer code related suites when applied 'HEROES' offer code.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Test data excel | Test data excel | 2      | HEROES    |
	And I click Book Now button
	Then I should navigate to Suite Page

@TC9409 @smoke @PlanPage
Scenario: 9409_Verify that user able to see suites when applied 'MOREFUN' offer code for 2 or more than 2 nights stay.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within 60 days and more than 2 nights
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  Within 60 days from today   |      more than 2 nights    |    2    |     MOREFUN       |
	And I click Book Now button
	Then I should navigate to Suite Page

#===================================================Suite Page ==============================================================

@TC7246 @Smoke @SuitePage
Scenario: 7246_Suite Page -  Verify that Late check out details should be displayed LOC Pop Up.
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I click on 'Select Room' button
	Then I should see LCO pop appear
	And I should see LCO title
	And I should see LCO Price
	And I should see LCO Description
	And I should see LCO 'Yes, Add Late Checkout' and 'No Thanks' buttons.

@TC11562 @Smoke @SuitePage
Scenario: 11562_Suite Page - Verify that when user search a suite then on Theme tab to standard suite then user can see Standard suite details.
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	#And I Click on Standard Tab
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | NoPremium   | Standard     | No               |
	#Then I should see Tab Title as Standard
	Then I should see Suite Title
	And I should see Base and Max occupancy section
	And I should see Suite description
	And  I should see Check rate calendar link
	And I should see Suite price section.
	   	
@TC11565 @Smoke @SuitePage
Scenario: 11565_Suite Page - Verify that when user search a suite then on Theme tab to Premium suite then user can see Premium suite details.
     Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	#And I Click on Premium Tab
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | Premium   | NoStandard     | No               |
	#Then I should see Tab Title as Premium
	Then I should see Suite Title
	And I should see Base and Max occupancy section
	And I should see Suite description
	And I should see Check rate calendar link
	And I should see Suite price section.

@TC5626 @Smoke @SuitePage
Scenario: 5626_Suite Page - Verify that when user search suite with Offer code then Applied offer code should be displayed on Suite info.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	Then I should see Applied offer code into suite info

	@TC5616 @Smoke @SuitePage
Scenario: 5616_Suite Page - Verify that when user search suite without Offer code then 'Best Available Rate' should be displayed on Suite info.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        |     |
	Then I should see Best Available Rate into suite info

@TC4125 @Smoke @SuitePage
Scenario: 4125_Suite Page - Verify that when user select accessible room toggle then for suites accessible tag should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | NoPremium   | NoStandard     | Yes               |
	#And I select Accessible Rooms Only toggle
	And I click on Update your Stay
	Then I should see Accessible label for suites

@TC12917 @Smoke @SuitePage
Scenario: 12917_Verify that when user select occupancy is 1 greater than base occupancy but select as child and age is 3 or greater than 3 then suite price should be increasing by $50.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      |   0  |     0    | AMTCERT    |
	#And I observe Suite price
	And I observe Suite price to price verification
	And I select Kids and update stay
	| Kids | Kids1Age |
	| 1   | 3        | 
	Then I should see suite price should be a increased by dollar 50


@TC12910 @Smoke @SuitePage
	Scenario: 12910_Verify that when user select occupancy is 1 greater than base occupancy then suite price increasing by $50.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      |   0	|     0    | AMTCERT    |
	#And I observe Suite price
	And I observe Suite price to price verification
	And I select adult greater than base and update stay
	#And I select adult and update stay
	#| Adults |
	#| 5   |
	Then I should see suite price should be a increased by dollar 50

@TC12916 @Smoke @SuitePage
Scenario: 12916_Verify that when user select occupancy is greater than base occupancy but select as child and age is 2 then suite price should not be increasing by $50.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      |   0  |     0    | AMTCERT    |
	#And I observe Suite price
	And I observe Suite price to price verification
	And I select Kids and update stay
	| Kids | Kids1Age |
	| 1   | 2        | 
	Then I should see suite price should not be a increased by dollar 50

	@TC7252 @Smoke @SuitePage
Scenario: 7252_Verify that when user click on 'No Thank' button on LCO pop up then user should be navigate on Activity page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0   | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	Then I should see Continue to Dining Package button


@TC7251 @Smoke @SuitePage
Scenario: 7251_Verify that when user click on 'Yes, Add Late Check Out' button on LCO pop up then user should be navigate on Activity page and LCO price should be added correctly.
    Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I observe LCO Price
	And I click on Add Late Check Out button on LCO pop up
	Then I should see LCO price is correct on Activity Page

@TC12674 @Smoke @SuitePage
Scenario:12674_Suite Page - Verify that user able to open room filter and see room categories as  'Themed', 'Standard' and 'Premium' after click on 'Room Type' filter. 
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I click on Room Type drop down
	Then I should see Theme room type option
	And I should see Premium room type option
	And I should see Standard room type option

@TC12714 @Smoke @SuitePage
Scenario:12714_Suite Page - Verify that user able to select 'Theme check' box and Theme type suite should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| Theme   | NoPremium   | NoStandard     | No               |
	Then I should see Themed suites

@TC12719 @Smoke @SuitePage
Scenario:12719_Suite Page - Verify that user able to select 'Standard check' box and Standard type suite should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | NoPremium   | Standard     | No               |
	Then I should see Standard suites

@TC13462 @Smoke @SuitePage
Scenario:13462_Suite Page-Verify that user able to see 'Lowest to Highest Price', 'Highest to Lowest Price' and 'Recommended for you' categories under 'Sort By' filter.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I click on Sort Option fillter dropdown
	Then I should see Lowest To Highest Sort Option
	And I should see Highest to Lowest Sort Option
	And I should see Recommended Sort Option

@TC13463 @Smoke @SuitePage
Scenario:13463_Suite Page - Verify that when user click on 'Lowest to Highest Price' then suite sort accordingly.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I click on Sort Option fillter dropdown
	And I click on Low to High sort option
	Then I should see Suite sorted as Low to High

@TC13466 @Smoke @SuitePage
Scenario:13466_Suite Page - Verify that when user click on 'Highest to Lowest  Price' then suite sort accordingly.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I click on Sort Option fillter dropdown
	And I click on High to Low sort option
	Then I should see Suite sorted as High to Low

@TC13657 @Smoke @SuitePage
Scenario:13657_Suite Page- Verify that 'RECOMMENDED FOR YOU' label should display on premium suite when sort by 'Recommended for you' filter.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 1      | 1    | 3        | AMTCERT    |
	And I click on Sort Option fillter dropdown
	And I click on Recommended sort option
	Then I should see Recommended label for suite

#=================================================== Activity Page ===================================================================

@TC5702 @Smoke @Activity
Scenario: 5702_Verify that user able to add Attraction packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
    Then I should see added package under cost summary
	
@TC5728 @Smoke @Activity
Scenario: 5728_Verify that user should be remove added Attraction packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Package Remove link
	Then I should remove added package under cost summary
	 
@TC5710 @Smoke @Activity
Scenario: 5710_Verify that user should be add packages under the Family tab	
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Family Tab
	And I add Family Package
    Then I should see added package under cost summary

@TC12844 @Smoke @Activity
Scenario: 12844_Verify that user able to remove added Family packages.	
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Family Tab
	And I add Family Package
	And I click on Package Remove link
   	Then I should remove added package under cost summary

@TC5711 @Smoke @Activity
Scenario: 5711_Verify that user should be add packages under the Birthday tab
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Birthday Tab
	And I add Birthday Package
    Then I should see added package under cost summary

@TC12845 @Smoke @Activity
Scenario: 12845_Verify that user able to remove added Birthday packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Birthday Tab
	And I add Birthday Package
	And I click on Package Remove link
	Then I should remove added package under cost summary

@TC5735 @Smoke @Activity
Scenario: 5735_Verify that 2 pm Late Check Out package should be showing under cost summary on activities page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
    | Adults | Kids | Kids1Age | Offer Code |
    |   2    | 1    |    3     |  AMTCERT   |
    And I navigate from suite Page to Activities page with LCO
	Then I should see 2 PM LOC package added under cost summary

@TC10062 @Smoke @Activity
Scenario: 10062_Verify that packages drop down quantity should be up to guest count
	Given I relaunch browser and navigate to Property Home page
    When I select check In date
	And I select check Out date
	And I select Adults
	| Adults |
	| 2   |
	And I select Kids
	| Kids | Kids1Age |
	|  1   |  2       |
	And I observe Guests count
	And I enter Offer Code
	| Offer Code |
	|   AMTCERT  |
	And I click on Book Now button
	And I navigate from suite Page to Activities page without LCO
	And I navigate to Family Tab
    Then I should see package quantity upto selected guest count

@TC5738 @Smoke @Activity
Scenario:  5738_User able to add multiple package quantity from Attraction packages
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
    | Adults | Kids | Kids1Age | Offer Code |
    |   3    | 1    |    2     |  AMTCERT   |
    And I navigate from suite Page to Activities page without LCO
	And I navigate to Attractions Tab
	And I add attraction package for multiple quantity
	Then I should see added package under cost summary
	And  I should see correct attraction package price under cost summary

@TC5726 @Smoke @Activity
Scenario: 5726_Verify that attractions packages cost should be displayed as per selected quantity from packages drop down under cost summary.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 5      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I update package quantity under cost summary
	Then I should see correct reflected package cost under cost summary

@TC5750 @Smoke @Activity
Scenario: 5750_Verify that package total should be addition of all packages under cost summary on Activities page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I navigate to Birthday Tab
	And I add Birthday Package
	Then I see Package Total price should equals to addition of all added packages
 
@TC5749 @Smoke @Activity
Scenario: 5749_Verify that 'Suite Total' and 'Total' should same without any package selection or added under cost summary on Activities page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
    | Adults | Kids | Kids1Age | Offer Code |
    |   2    | 1    |    2     |  AMTCERT   |
    And I navigate from suite Page to Activities page without LCO
	Then I Should see Suite Total price equals to Total price on Activities page without any package added

@TC5751 @Smoke @Activity
Scenario: 5751_Verify that total should be correct after adding packages under cost summary section
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	Then I should see correct Total after added package

@TC12851 @Smoke @Activity
Scenario: 12851_Verify that user able to see correct package total after update package quantity under cost summary on activities page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I update package quantity under cost summary
	Then I should see correct package total after update package quantity under cost summary

@TC5736 @Smoke @Activity
Scenario: 5736_If user add packages from activities page and go back to suite page and select another suite then previously added packages should not displayed showing under cost summary on activities page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
    | Adults | Kids | Kids1Age | Offer Code |
    |   2    | 1    |    2     |  AMTCERT   |
    And I navigate from suite Page to Activities page without LCO
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I navigate to Family Tab
	And I add Family Package
	And I click on Suites Icon
	#And I Click on Standard Tab
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | NoPremium   | Standard     | No               |
	And I navigate from suite Page to Activities page without LCO
	Then I should not see previously added packages under cost summary

@TC5745 @Smoke @Activity
Scenario: 5745_Verify that user see correct suite total under cost summary section
	Given I relaunch browser and navigate to Property Home page
    When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1    | 2        | AMTCERT    |
	Then I should see Suite Total equals to Base price minus Promotional Saving price

@TC5743 @Smoke @Activity
Scenario: 5743_Verify that user should see 'Base' price correctly on Activities page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
    | Adults | Kids | Kids1Age | Offer Code |
    |   2    | 1    |    2     |  AMTCERT   |
	And I observe Suite Was price
	And I observe number of Stay Nights
    And I navigate from suite Page to Activities page without LCO
	Then I should see suite base price equals to multiplication of suite was price and number of stays nights

@TC5742 @Smoke @Activity
Scenario: 5742_Verify that user should see 'Promotional Saving' price correctly under 'Cost Summary' on 'Activities' page
	Given I relaunch browser and navigate to Property Home page
    When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1    | 2        | AMTCERT    |
	Then I should see Promotional Saving price correctly under cost summary on activities page

@TC11271 @smoke @Activity
Scenario: 11271_Verify that correct check in and check out dates should displayed on activities page when user select other dates on rate calendar rather than selected dates in booking engine.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	And I select Chek In and Check Out date
	| No Of Month |
	| 3		|
	And I observe Check In and Check out Date
	And I click on Continue to Package button
	Then I should see correct Check In and Check Out date under cost summary through rate calendar

#============================================== Dining Page =============================================================

@TC5790 @Smoke @Dining
Scenario: 5790_Verify that user see proper adult and Kids count in cost summary section
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 2    | 4       | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see valid adult count
	| Adults |
	| 2      |
	And I should see valid Kid count
	| Kids |
	| 2    |

@TC12855 @Smoke @Dining
Scenario: 12855_Verify that user able to add dining packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I click on Dining package Add button
	Then I should see added package in cost summary

@TC12856 @Smoke @Dining
Scenario: 12856_Verify that user able to remove added Dining packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I click on Dining package Add button
	And I click on dining package Remove link
	Then I should see added package is removed from cost summary
	
@TC12858 @Smoke @Dining
Scenario: 12858_Verify that user able to update added dining package quantity under cost summary on dining page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |	
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I select package quanitity
	| Package Quantity |
	| 1                |
	And I click on Dining package Add button
	And I observe package cost for single quantity
	And I select package quanitity
	| Package Quantity |
	| 2                |
	And I click on Dining package Update button
	Then I should see correct package cost after update quantity
	| Second Package Quantity |
	| 2                       |

@TC5804 @Smoke @Dining
Scenario: 5804_By default selected stay night quantity should be set in package drop down.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see no of nights set as quantity

@TC7517 @Smoke @Dining
Scenario: 7517_Verify that 'kids age greater than 3' calculated as billable guest for dining packages on dining page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see correct chareges applied for Kids
	| Billable Guest Count |
	| 3                    |

@TC7516 @Smoke @Dining
Scenario: 7516_Verify that 'kids age less than or equal to 3' not calculated as billable guest for dining packages on dining page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see correct chareges applied for Kids
	| Billable Guest Count |
	| 2                    |

@TC5798 @Smoke @Dining
Scenario: 5798_Verify that 'Suite Total' and 'Total' should same without any package selection or added under cost summary on dining page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see Same suite total and Total when package not added

@TC5793 @Smoke @Dining
Scenario: 5793_Verify that user see correct suite total under cost summary section
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see Suite Total equals to Base price minus Promotional Saving price
	 
@TC11361 @Smoke @Dining
Scenario: 11361_verify that Person/per day cost should displayed correctly for dining packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see correct person/day per cost for package
	| Billable Guest Count |
	| 3                    |

@TC11362 @Smoke @Dining
Scenario: 11362_Verify that packages cost should displayed correctly for per day price.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	Then I should see correct package total for person/day per package
	| Billable Guest Count |
	| 3                    |
	
@TC5799 @Smoke @Dining
Scenario: 5799_Verify that package total should be addition of all packages under cost summary on Dining page
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I added multiple dining packages
	Then I should see correct suite total after added packages
	| No of Days |
	| 1          |

@TC12859 @Smoke @Dining
Scenario: 12859_Verify that user able to see correct package total after update dining package quantity under cost summary on Dining page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page with 2 nights stay
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 4        | AMTCERT    |
	And I click on 'Select Room' button
	And I click on No Thanks button on LCO pop up
	And I click on Continue to Dining Package button
	And I added multiple dining packages
	Then I should see correct package total after update packages

#====================================== Payment Page ============================================================================

@TC12880 @Smoke @Payment
Scenario: 12880_Verify that correct Flex trip package price display under cost summary on payment page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I observe FlexTrip price
	And I check FlexTrip check box
	Then I should see correct added FlexTrip price under cost summary

@TC12881 @Smoke @Payment
Scenario: 12881_Verify that user able to remove Flex trip package from cost summary on payment page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I check FlexTrip check box
	And I click on flexTrip remove link
	Then I should see FlexTrip check box unchecked

@TC5943 @Smoke @Payment
Scenario: 5943_Verify that user see correct 'Promotional savings' price under cost summary section
	Given I relaunch browser and navigate to Property Home page
   When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	Then I should see Promotional Saving price correctly under cost summary on activities page

@TC5945 @Smoke @Payment
Scenario: 5945_Verify that user see correct suite total under cost summary section.
	Given I relaunch browser and navigate to Property Home page
   When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 2        | AMTCERT    |
	And I navigate from Suite page to Payment page
	Then I should see Suite Total equals to Base price minus Promotional Saving price

@TC7383 @smoke @Payment
Scenario: 7383_If User select 'Check In ' , 'Check out' dates within 10 days from current date then 'Add Flexibility to your trip' section should not display on 'Payment' page.
	Given I Open New browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within a 10 days
    | Check In Date | Check Out Date | Adults | Offer Code |
    |  Within a 10 Days | Within a 10 Days |   2  |  AMTCERT |
	And I click Book Now button
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	Then I should not see 'Add flexibility to your trips’ section

@TC7384 @smoke @Payment
Scenario: 7384_If user use 'Early Saver reservation' offer code then 'Add Flexbility to your trip' section should not display on payment page.
	Given I relaunch browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates more than 60 days
    | Check In Date | Check Out Date | Adults | Offer Code |
    |  More than 60 days from today | 2 days more than Check In date |   2  |  ESAVER |
	And I click Book Now button
	And I navigate from Suite page to Payment page
	Then I should not see 'Add flexibility to your trips’ section


@TC12884 @Smoke @Payment
Scenario: 12884_Verify that package total should be addition of all packages under cost summary on payment page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Continue to Dining Package button
	And I click on Dining package Add button
	And I click on Continue to Payment page button
	Then I see Package Total price should equals to addition of all added packages

@TC12889 @Smoke @Payment
Scenario: 12889_Verify that 'Total' price minus 'Due today' price should equals to 'Due at Check-In' price.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I check FlexTrip check box
    Then I should see 'Total' price minus 'Due today' price should equals to 'Due at Check-In' price

@TC12887 @Smoke @Payment
Scenario: 12887_Verify that Total should be correct with having packages under cost summary section
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Continue to Dining Package button
	And I click on Dining package Add button
	And I click on Continue to Payment page button
	Then I should see correct Total with having packages under cost summary

@TC12891 @Smoke @Payment
Scenario: 12891_Verify that user able to remove package from cost summary on payment page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        | AMTCERT    |
	And I navigate from suite Page to Activities page without LCO
	And I click on Continue to Dining Package button
	And I click on Dining package Add button
	And I click on Continue to Payment page button
	And I click on dining package Remove link
	Then I should see added package is removed from cost summary
	 
@TC11662 @Smoke @Payment
Scenario: 11662_Verify that user not able  to see flex fare option for 'Niagara Property'.
    Given I relaunch browser and navigate to Property Home page
	When I select Another property location
		| Property      |
		| Niagara Falls, Ontario |
	And I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page
	Then I should not see 'Add flexibility to your trips’ section

@TC12886 @Smoke @Payment
Scenario: 12886_Verify that user able to see correct package total after update package quantity under cost summary on payment page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Continue to Dining Package button
	And I click on Continue to Payment page button
	And I update package quantity under cost summary
	Then I should see correct package total after update package quantity under cost summary

@TC12888 @Smoke @Payment
Scenario: 12888_Verify that user able to see correct Total after update package quantity under cost summary on payment page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Continue to Dining Package button
	And I click on Continue to Payment page button
	And I update package quantity under cost summary
    Then I should see correct Total with having packages under cost summary

@TC12885 @Smoke @Payment
Scenario: 12885_Verify that user able to update added package quantity under cost summary on payment page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1    | 2        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Continue to Dining Package button
	And I click on Continue to Payment page button
	And I update package quantity under cost summary
	Then I should see updated package quantity under cost summary
