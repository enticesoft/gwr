﻿Feature: DiningAndShoppingPage
	In order to check best packages
	As a user 
	I want to check on Dining and shopping page

@TC10589 @Smoke
Scenario: 10589_Dining & Shopping > Dining page: Verify that when user click on 'Dining' submenu from 'Dining & Shopping' menu, Dining page should get open.
Given I relaunch browser and navigate to Property Home page
	When I navigate to 'Dining' Page
	Then I should see Dining page opened

	@TC10606 @Smoke
Scenario:10606_Dining & Shopping > Shopping pages- Verify that when user click on 'Shopping' submenu from 'Dining & Shopping' menu, shopping page should get open.
	Given I relaunch browser and navigate to Property Home page
	#When I navigate to 'Shopping' page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I navigate to 'Shopping' page sub menu
	Then I should see Shopping page is opened

@TC10600 @Smoke
Scenario: 10600_Dining & Shopping > Dining page- Verify that user able to see Dining related packages on 'Dining' page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate to 'Dining' Page
	Then I should see total packages available are equal to Dining packages 

@TC12874 @Smoke
Scenario: 12874_Dining & Shopping > Shopping page- Verify that user able to see Shopping related packages on 'Shopping' page.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I navigate to 'Shopping' page sub menu
	Then I should see total packages available are equal to Shopping package 


@TC10620 @Smoke	
	Scenario:10620_Dining & Shopping > Shopping page-Verify that user able to navigate on package detail page when click on Title of packages.
	Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I navigate to 'Shopping' page sub menu
	And I click on Title of the package	
	Then I should see details page of title	

	
@TC10603 @Smoke
    Scenario:10603_Dining & Shopping > Dining page-Verify that user able to navigate on package detail page when click on Title of packages.
    Given I relaunch browser and navigate to Property Home page
	When I navigate to 'Dining' Page
	And I click on Title of the Dining package	
	Then I should see details page of package

@TC15434 @Smoke
    Scenario:15434_Dining & Shopping > Dining > Verify that user navigate on T3 page and verify following details.
    Given I relaunch browser and navigate to Property Home page
	When I navigate to 'Dining' Page
	And I click on Activities learn more link
	Then I should see Activity Title of package
	And I should see Available Hours section
	And I should see Thumbnail section
	And I should see Best For section

@TC15435 @Smoke
    Scenario:15435_Dining & Shopping > Shopping> Verify that user navigate on T3 page and verify following details.
    Given I relaunch browser and navigate to Property Home page
	When I Mouse hover on 'WaterPark And Attraction' header
	And I navigate to 'Shopping' page sub menu
	And I click on Activities learn more link
	Then I should see Activity Title of package
	And I should see Available Hours section
	And I should see Thumbnail section
	And I should see Best For section

