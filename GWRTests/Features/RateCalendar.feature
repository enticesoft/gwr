﻿Feature: RateCalendar
	In order to user 
	GWR to get rate availability


@TC5625 @smoke
Scenario: 5625_User able to see 'CHECK RATE CALENDAR' link on suite page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	Then I should see Check rate calendar link

@TC5671 @smoke
Scenario: 5671_Verify that 'CONTINUE TO PACKAGES' button should be displayed disable before selecting check in & check out date
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	Then I should see 'CONTINUE TO PACKAGE' button disabled

@TC5678 @smoke
Scenario: 5678_Verify that 'Rate Calendar' should gets close after click on 'X Close' icon
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	And I click on 'Rate Calendar Close' link
	Then I should see Check rate calendar link

@TC8739 @smoke
Scenario: 8739_Verify that selected 'Offer code' and 'Suite Name' displayed on rate calendar page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	Then I should see valid suite name on Rate calendar
	Then I should see valid offercode on Rate calendar
	| Offer Code |
	| AMTCERT    |

@TC12919 @smoke
Scenario: 12919_Verify that when user search a suite without offer code then on Rate calendar Best Available Rate displayed.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        |     |
	And I click on 'Rate Calendar' link
	Then I should see 'Best Available Rate' on Rate calendar

@TC5669 @smoke
Scenario: 5669_Verify that when user click on 'Next' icon then user should be displayed next months
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	Then I click on 'Next Month Link' link and verify Month Change
	| Check Month Count |
	| 1                 |

@TC5681 @smoke
Scenario: 5681_Not Available label should be displayed which dates suites are not available
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	Then I should see Not Available label for Dates

@TC6308 @smoke
Scenario: 6308_Verify the available data on 'Rate Calendar'.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	Then I should see valid suite name on Rate calendar
	And I should see valid offercode on Rate calendar
	| Offer Code |
	| AMTCERT    |
	And I should see 'CONTINUE TO PACKAGE' button disabled
	And I should see 'Rate Calendar Close' link

@TC12918 @smoke
Scenario: 12918_Verify that when user select check in and Check out date from rate calendar and offer code is available then on Activity offer code should be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	And I select Chek In and Check Out date
	| No Of Month |
	| 3		|
	And I click on Continue to Package button
	Then I should see valid offercode on Actvity
	| Offer Code |
	| AMTCERT    |

@TC12920 @smoke
Scenario: 12920_Verify that when user select check in and Check out date from rate calendar and offer code is not available then on Activity offer code should not be displayed.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        |     |
	And I click on 'Rate Calendar' link
	And I select Chek In and Check Out date
	| No Of Month |
	| 3		|
	And I click on Continue to Package button
	Then I should not see offercode on Actvity

@TC9517 @smoke
Scenario: 9517_Verify that when user select out of range date of any offer code from rate calendar page then error message is displayed.
	Given I Open New browser and navigate to Property Home page
	When I enter valid details on Booking Widget with dates within 60 days and more than 2 nights
         | Check In Date | Check Out Date | Adults | Offer Code |
         |  Less than 60 days from today | 2 days more than Check In date|    2    | MOREFUN |
	And I click Book Now button
	And I click on 'Rate Calendar' link
	And I select Chek In and Check Out date
	| No Of Month |
	| 2		|
	And I click on Continue to Package button
	Then I should see validation error

@TC4129 @smoke
Scenario: 4129_Verify that when user select Accessible room type check in and check out date from rate calendar then Accessible displayed on cost summary on activity page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        |  AMTCERT   |
	And I select Suite option
	| Suite Option       |
	| Accessible Bathtub |
	And I click on 'Rate Calendar' link with suite option
	And I select Chek In and Check Out date
	| No Of Month |
	| 3		      |
	And I click on Continue to Package button	
	Then I should see valid suite title with Option Under cost summary
	| Suite Option Only First Word |
	| Accessible                   |

@TC5682 @smoke
Scenario: 5682_Verify that Suite Cost displayed on rate calendar when user open rate calendar.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 0    | 0        |  AMTCERT   |
	And I click on 'Rate Calendar' link
	Then I should see date available for selection
	| No Of Month |
	| 1			  |


@TC5684 @smoke
Scenario: 5684_Verify that user should be delete entered offer code on rate calendar and it should not show under cost summary on 'Activities', 'Dining' & 'Payment' page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        |    AMTCERT |
	And I click on 'Rate Calendar' link
	And I click on Delete Icon link
	And I select Chek In and Check Out date
	| No Of Month |
	| 1		      |
	And I click on Continue to Package button
	Then I should not see offercode on in cost summary
	When I click on Continue to Dining Package button
	Then I should not see offercode on in cost summary
	When I click on Continue to Payment page button
	Then I should not see offercode on in cost summary
