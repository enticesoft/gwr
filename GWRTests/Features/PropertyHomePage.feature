﻿Feature: PropertyHomePage
	In order to use property specific features
	As a end user
	I want to be on property page

@TC12839 @smoke
Scenario: 12839_Verify that user see booking engine on home page.
	Given I am on GWR Global Home page
	When I click on Choose a Location button
	And I select a property
	Then I land on home page of property
	And I should see Booking Widget

@TC9844 @smoke
Scenario: 9844_Verify that user navigate on suite page when click on 'Book Now' button of Room Card.
	Given I am on Property Home page
	When I click on Book Now button on Room Card
	Then I should navigate to Plan page

@TC11742 @smoke
Scenario: 11742_Verify that 'Customer Service' page should get open after click on 'Customer Service' tile.
	Given I am on Property Home page
	When I click on Customer Service tile on Clickable Tiles section
	Then I should navigate on Customer Service page

@TC11916 @smoke
Scenario: 11916_Verify that clicking 'Get Directions' on 'Mini Map' section user navigates to the Google map.
	Given I relaunch browser and navigate to Property Home page
	When I scroll to Mini Map and click on Get Direction button
	Then I should navigate to the Google Map

@TC8797 @smoke
Scenario: 8797_Verify that data entered on booking widget is retained after page scroll down and scroll up.
	Given I am on Property Home page
	When I enter valid details on Booking Widget
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Test data excel | Test data excel | 2      | AMTCERT    |
	And I scroll down the page to bottom
	And I scroll up the page to Booking Widget
	Then I should see data which was entered on Booking Widget is retained
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Test data excel | Test data excel | 2      | AMTCERT    |

@TC8186 @smoke
Scenario: 8186_Verify that user navigates to 'special deal details' page after clicking 'View Offer' button.
	Given I am on Property Home page
	When I click on View Offer button
	Then I should navigate to Special Deal Details page

@TC8188 @smoke
Scenario: 8188_Verify that user navigates to 'Suite' page with valid data entered in booking engine.
	Given I am on Property Home page
	When I enter valid details on Booking Widget
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Test data excel | Test data excel | 2      | AMTCERT    |
	And I click Book Now button
	Then I should navigate to Suite Page

@TC9971 @smoke
Scenario: 9971_Verify that 'Day Pass' page should be displayed after click on 'Day Pass' tile.
	Given I am on Property Home page
	When I click on Day pass tile
	Then I should navigate to Day Pass page

@TC11357 @smoke
Scenario: 11357_Lead Gen Pop Up-When user enter Canadian postal code then user can check and unchecked 'I Agree to receive email' checkbox
    Given I Open New browser and navigate to Property Home page and wait for Lead Gen Pop up
    When I enter Canadian postal code on Lead Gen pop-up
	| Postal Code |
	|    T5T4M2   |
    And I Check 'I Agree to receive email' checkbox
    Then I should able to Check checkbox

@TC14320 @smoke
Scenario: 14320_Lead Gen Pop Up-When user enter Canadian postal code then user can unchecked 'I Agree to receive email' checkbox
    Given I Open New browser and navigate to Property Home page and wait for Lead Gen Pop up
    When I enter Canadian postal code on Lead Gen pop-up
	| Postal Code |
	|    T5T4M2   |
    And I Check 'I Agree to receive email' checkbox
	And I Uncheck 'I Agree to receive email' checkbox
    Then I should able to Uncheck checkbox 

@TC8353 @smoke
Scenario:8353_Verify that user should get navigate on 'Plan Your Stay' section after click on the 'Book Your Stay' section
     Given I Open New browser and navigate to Property Home page 
	 When I click on Book Your Stay link under Header section
	 Then I should navigate on 'Plan Your Stay' section

@TC13560 @smoke
Scenario:01_13560 Verify that user should able to change location using location dropdown
      Given I Open New browser and navigate to Property Home page 
	  When I click on 'Property dropdown' link under header section
	  And I click on any property link
	  |propertyName        |
	  |Wisconsin Dells, WI|
	  Then I should navigate to that specific property
      |propertyName       |
	  |Wisconsin Dells, WI|

#@TC8720 @smoke
#Scenario: Verify that when user click 'Customer Service' Link on home page then user navigate to 'Customer Service' page
  #Given I am on Property Home page
  #When I click on Customer Service tile under Header section
  #Then I should navigate on Customer Service page

@TC15441 @Smoke
Scenario: 15441_Home > When user is on any page and click GWR logo then navigate on Property Home page.
     Given I relaunch browser and navigate to Property Home page
	 When I Mouse hover on 'WaterPark And Attraction' header
	 And I click on 'Activities' submenu
	 And I click on GWR Logo at top
	 Then I land on home page of property and verify Banner

@TC15442 @Smoke
Scenario: 15442_Home >Verify that Banner is displayed on Home page.
     Given I relaunch browser and navigate to Property Home page
	 Then I land on home page of property and verify Banner

@TC15443 @Smoke
Scenario: 15443_Home > Verify that Welcome card is displayed on Home page.
     Given I relaunch browser and navigate to Property Home page
	 Then I land on home page of property and verify about section

@TC15444 @Smoke
Scenario: 15444_Home >Verify that Gallery container is displayed on Home page.
     Given I relaunch browser and navigate to Property Home page
	 Then I land on home page of property and verify gallery section

@TC15445 @Smoke
Scenario: 15445_Home >Verify that Deal Card section is displayed on Home page.
     Given I relaunch browser and navigate to Property Home page
	 Then I land on home page of property and verify Deal section

@TC15446 @Smoke
Scenario: 15446_Home >Verify that When user click on Deal Card section > Start Saving button then navigate on Special offer page.
     Given I relaunch browser and navigate to Property Home page
	 When I click on Start Saving button
	 Then I should see special offer title

@TC15447 @Smoke
Scenario: 15447_Home >Verify that Room Card is displayed on Home page.
     Given I relaunch browser and navigate to Property Home page
	 Then I land on home page of property and verify Room Card

@TC15448 @Smoke
Scenario: 15448_Home >Verify that amenities  section is displayed on Home page.
     Given I relaunch browser and navigate to Property Home page
	 Then I land on home page of property and verify Amenities

@TC15449 @Smoke
Scenario: 15449_Home > Verify that Clickable tile section is displayed on Home page.
     Given I relaunch browser and navigate to Property Home page
	 Then I land on home page of property and verify clicable tile section

@TC15450 @Smoke
Scenario: 15450_Home > Verify that user navigate on Dining page from Dining clickable tile
     Given I relaunch browser and navigate to Property Home page
	 Then I click on dining clickable tile and should see Dining Page

@TC15451 @Smoke
Scenario: 15451_Home > Verify that user navigate on Attraction page from Attraction clickable tile.
     Given I relaunch browser and navigate to Property Home page
	 Then I click on attraction clickable tile and should see Attraction Page

@TC15452 @Smoke
Scenario: 15452_Home > Verify that user navigate on Activity page from Activity clickable tile.
     Given I relaunch browser and navigate to Property Home page
	 Then I click on activity clickable tile and should see Activity Page

@TC16290 @Smoke
Scenario: 16290_Home >Verify that Water Park Hours section is available on Home page.
     Given I relaunch browser and navigate to Property Home page
	 Then I land on home page of property and verify water park hours section

@TC13186 @Smoke
Scenario: 13186_Home Page > Global Deal -Verify that offer Bar is pinned to the bottom of the screen .
     Given I relaunch browser and navigate to Property Home page
	 Then I should see Global deal bar on Home Page

@TC13190 @Smoke
Scenario: 13190_Home Page > Global Deal - Verify that clicking on the X icon closes the component.
     Given I relaunch browser and navigate to Property Home page
	 When I click on Deal Bar on Home page.
	 And I click on Deal Bar close link on Home page.
	 Then I should see Global deal bar on Home Page is collapse

@TC13192 @Smoke
Scenario: 13192_Home Page > Global Deal - Verify that user able to see offer cards under bar section.
     Given I relaunch browser and navigate to Property Home page
	 When I click on Deal Bar on Home page.
	 Then I should see deals in Global deal bar on Home Page

@TC13193 @Smoke
Scenario: 13193_Home Page > Global Deal - Verify that user able to see offer cards under bar section.
     Given I relaunch browser and navigate to Property Home page
	 When I click on Deal Bar on Home page.
	 And I click on Deal title of Global deal on Home page.
	 Then I should navigate on package detail page if Deal bar available

@TC13194 @Smoke
Scenario: 13194_Home Page > Global Deal - Verify that when user click on "APPLY CODE" CTA the plan page opens and the promo code is applied .
     Given I relaunch browser and navigate to Property Home page
	 When I click on Deal Bar on Home page.
	 And I click on Apply code button of Global deal on Home page.
	 Then I should see offer code reflect on plan page.

@TC13195 @Smoke
Scenario: 13195_Home Page > Global Deal - Verify that 'sign up now' button displayed instead of 'Apply Code' and 'Sign In' link instead of 'Offer code' for exclusive deal
     Given I relaunch browser and navigate to Property Home page
	 When I click on Deal Bar on Home page.
	 Then I observe exclusive deal have sign up button when user is not logged In
	 And I observe exclusive deal have sign in link when user is not logged In
	 When I click on Deal Bar close link on Home page.
	 And I click on Sign In link under header section
	 And I enter valid login details
	 | Email                    | Password   |
	 | gwr.nextgenqa1+0510@gmail.com | $Reset123$ |
	 And I click Sign In button
	 And I click on Deal Bar on Home page.
	 Then I should see exclusive deal have Apply Offer Code button when user is logged In
	 And I should see exclusive deal have Offer Code when user is logged In

@TC13196 @Smoke
Scenario: 13196_Home Page > Global Deal - Verify that create an account pop up opens when user click on 'Sign Up' CTA .
     Given I Open New browser and navigate to Property Home page
	 When I click on Deal Bar on Home page.
	 And I click on Sign Up button of Global deal on Home page.
	 Then I should see Create account pop is open.

@TC13198 @Smoke
Scenario: 13198_Home Page > Global Deal -Verify that Sign in text link opens the sign in modal when clicked on it.
     Given I Open New browser and navigate to Property Home page
	 When I click on Deal Bar on Home page.
	 And I click on Sign In button of Global deal on Home page.
	 Then I should see Sign In pop is open.

@TC15203 @Smoke
Scenario: 15203_Home Page > Global Deal - Verify that 'SEE ALL OFFERS' link displays at bottom of Global Deals component and navigate to T2 page
     Given I relaunch browser and navigate to Property Home page
	 When I click on Deal Bar on Home page.
	 And I click on See More Offer link of Global deal on Home page.
	 Then I should see special offfer T2 page.

@TC15197 @Smoke
Scenario: 15197_Home Page > Global Deal -UI-Verify that 'Sign In' link displays on global Deals card instead of promo code when user is not logged in for Esaver/Exclusive deal
     Given I relaunch browser and navigate to Property Home page
	 When I click on Deal Bar on Home page.
	 Then I observe exclusive deal have sign up button when user is not logged In
	 And I observe exclusive deal have sign in link when user is not logged In

@TC17548 @smoke
Scenario: 17548_User able to see Promo code on T3 page and Booking engline when navigate through Home Page Main banner.
    Given I Open New browser and navigate to Property Home page
	When I click on View Offer button
	Then I should see Promo code on T3 page and inside booking engine

@TC17549 @smoke
Scenario: 17549_User should not see Promo code on T3 page and Booking engline when navigate through Home Page Main banner when Sing in require.
    Given I Open New browser and navigate to Property Home page
	When I click on View Offer button
	Then I should not see Promo code on inside booking engine
	And I should not see Promo code on inside Deal Info section
	
@TC17567 @smoke
Scenario: 17567_User should see Promo code on T3 page and Booking engline when navigate through Home Page Main banner when Sing in require.
    Given I Open New browser and navigate to Property Home page
	 When I click on Sign In link under header section
	 And I enter valid login details
	 | Email                    | Password   |
	 | gwr.nextgenqa1+0510@gmail.com | $Reset123$ |
	 And I click Sign In button
	 And I click on View Offer button
	 And I Select Date with Applied offer code
	 Then I should see correct offercode applied for suites

@TC18112 @smoke
Scenario: 18112_Verify that user able to see all lodge locations inside Choose your destination dropdown.
	Given I Open New browser and navigate to Property Home page
	When I click on Location drop down link
	Then I should see West locations
	And I should see South locations
	And I should see Mid West locations
	And I should see North locations
	And I should see Canada locations