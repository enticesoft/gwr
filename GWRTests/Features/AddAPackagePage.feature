﻿Feature: AddAPackagePage
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@TC12905 @Smoke
Scenario: 12905_Verify that for guest user '2PM LOC' should display under 'Stay Date' section if user not added it while booking
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |		
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |	
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should see Add LCO button on CMP as we not added at booking time

@TC12906 @Smoke
Scenario: 12906_Verify that for guest user '2PM LOC' should not display under 'Stay Date' section if user added it while booking.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |		
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should not see Add LCO button on CMP as we added booking time
	#And I should see 2 P.M. Check Out text as LCO added booking time		

@TC12904 @Smoke
Scenario: 12904_Verify that guest user should able to see correct booking details on confirmation page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |		
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see New Confirmation Page Welcome Title
	And I should see Reservation Number
	And I should see Cabana Section
	And I should see Cost Summary Section

@TC12907 @Smoke
Scenario: 12907_Verify that guest user should able to add '2 PM LOC' after click on 'Add Late Check Out' button and cost summary update accordingly on confirmation page
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for Guest user
	#|First Name  |Last Name  |Email  | Phone Number | Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	#|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Contact Info for Guest user
	|First Name  |Last Name  |Email  | Phone Number |
	|QATest	|Nextgenqa	|qa80tester+803@gmail.com		| 9383227772   |	
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	Then I add LCO package and verify in cost sumarry

@TC6075 @Smoke
Scenario: 6075_Verify that available contains of cost summary
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	And I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass           | Add Dining1     |
	And I observe suite total on Payment Page
	And I observe taxes on Payment Page
	And I observe Resort fee Payment Page
	And I observe total Payment Page
	And I observe Due Today price on Payment Page
	And I observe Due at check in price on Payment Page
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I navigate on My Reservation Page
	And I click on View Details button
	When I click on Cost Summarry button
	Then I should see correct Suite Total on Cost Summarry
	And I should see correct taxes price on Cost Summarry
	And I should see correct resort fee on Cost Summarry
	And I should see correct Total cost on Cost Summarry
	And I should see correct deposite paid on Cost Summarry
	And I should see correct Due at check in cost on Cost Summarry
	When I click on cost summarry close link
	Then I should see User menu link at top

@TC12936 @Smoke
Scenario: 12936_Add A Package> Verify that logged user should see following reservation details on 'Add A Package' page
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	And I should see New Confirmation Page Welcome Title
	And I should see Lodge Location tile
	And I should see Select Dates tile
	And I should see Guest tile
	And I should see valid Adult and Kids count
	| Adult | Kids |
	| 2     | 1    |
	And I should see Room type with suite name
	Then I should see User menu link at top

@TC6073 @Smoke
Scenario: 6073_Add A Package: Verify that user should able to lands on 'Add A Package' page after click on 'Add Cabana' tile and reservation details should display correct on 'Add A Package' page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      |  1  |     3    | AMTCERT    |	
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining1     |	
	#And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add Cabana button
	Then I should see valid reservation number on Add A Package page
	And I should see valid suite name on Add A Package page
	And I should see valid Adult count on Add Package Page
	| Adult |
	| 2     |
	And I should see valid Check Out time on Add Package Page
	And I should see User menu link at top

	
@TC12935 @Smoke
Scenario: 12935_Add A Package Page: Verify that user should see new reservation id for added cabana after click on the 'View' link on reservation details page
	Given I Open New browser and navigate to Property Home page		
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1   | 3        | AMTCERT    |	
	When I create an account to Application
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com | $Reset123$  |
	And I click on Create button
	And I navigate from Suite page to Payment page with Packages choice
	| LCO Package       | Attraction Package | Dining Package |
	| Add LCO If Exists1 | Add Pass1           | Add Dining     |	
	#And I entered payment details on Payment Page for newly created user
	#| Phone Number | Card Name | Card Number      | CVV | Billing Address | Postal Code | City     |
	#| 9383227772   | NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I entered Payment and billing for Guest user
	| Card Name | Card Number | CVV | Billing Address | Postal Code | City |
	| NextGenQA | 4444333322221111 | 123 | MG street       | 10005       | New York |
	And I click on Book And Agree button
	And I click on See You Soon Pop Up All Done button
	Then I should see Reservation Number
	When I click on Add Cabana button
	And I added available Cabanas
	Then I should see valid cabana price in cost summarry
	When I click on Save to My Reservation button if cabana available
	And I click on Indoor cabana view link and verify reservation number
	Then I should see User menu link at top

	
	
	

	
	
	
