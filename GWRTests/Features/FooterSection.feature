﻿Feature: FooterSection
As a user 
I want to navigate
on diff pages from footer link.

@11004 @Smoke
Scenario: 11004_Verify that when user click on FAQ link FAQ page gets opened
Given I am on Property Home page
When I scroll down from top of page to bottom
And I click on FAQ link
Then I should navigate to FAQ page

@11006 @Smoke
Scenario: 11006_Verify that when user click on Customer service link Customer Service page get opened
Given I am on Property Home page
When I scroll down from top of page to bottom
And I click on Customer Service link
Then I should navigate to Customer Service page

@11016 @Smoke
Scenario: 11016_Verify that when user click on Birthday Parties link Birthday page gets open properly
Given I am on Property Home page
When I scroll down from top of page to bottom
And I click on Birthday Parties link
Then I should navigate to Birthday Parties page

@11018 @Smoke
Scenario: 11018_Verify that when user click on Group and Events link Groups page gets open
Given I am on Property Home page
When I scroll down from top of page to bottom
And I click on Group and Events link
Then I should navigate to Group and Events page

@11047 @Smoke
Scenario: 11047_Verify the functionality of Privacy Policy & Terms and Conditions link 
Given I am on Property Home page
When I navigate to 'Dining' Page
And I scroll down from top of page to bottom on any page
And I click on Privacy Policy and Terms and Conditions link
Then I should navigate to Privacy Policy and Terms and Conditions page

@TC16949 @Smoke
Scenario: 16949_Footer > Lead Gen- Verify that 'Lead gen Callout' section is displaying at Footer on home page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      |  0   | 3        | AMTCERT    |
	Then I should see Lead Gen Call out section is available

@TC16958 @Smoke
Scenario: 16958_Footer > Lead Gen- Verify that when user click on 'SUBSCRIBE & SAVE' CTA at footer then Lead Gen pop up opens
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      |  0   | 3        | AMTCERT    |
	And I click on Save and Subscribe button
	Then I should see Lead Gen Pop Up Open

@TC17429 @Smoke
Scenario: 17429_Footer > Lead Gen- Verify that when user subscribe from footer to Lead Gen then user should receive email.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      |  0   | 3        | AMTCERT    |
	And I click on Save and Subscribe button
	And I enter valid data on the Led Gen popup
	| First Name | Last Name | Postal Code | Email |
	| Qatester   | NextgenQA | As per Property       | gwr.nextgenqa1@gmail.com |
	And I click on Sign Up button
	Then I should see Success message
	And I should receive GWR Welcome email