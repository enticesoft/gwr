﻿Feature: Deals
	In order to get special offers
	As a user
	I want to apply offers

@TC10827 @smoke
Scenario: 10827_Verify that user able to sign in into application through Sign in link of exclusive deal on special offers page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to deals page
	And I click on exclusive deal sign in link
	And I enter valid login details
	| Email                    | Password   |
	| QATester301+1@gmail.com | $Reset123$ |
	And I click Sign In button
	Then I should see User menu link at top

@TC10828 @smoke
Scenario: 10828_Verify that user able to sign up into application through 'Sign up Now' button of exclusive deal on special offers page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to deals page
	And I click on exclusive deal Sign Up Now button
	And I entered valid data on the create account popup
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | QATester301@gmail.com | $Reset123$  |
	And I click on Create button
	Then I should see User menu link at top

@TC10829 @smoke
Scenario:  10829_Verify that user able to navigate on package detail page when click on 'learn more' link.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to deals page
	And I click on learn more link
	Then I should navigate on package detail page

@TC12069 @smoke
Scenario: 12069_Verify that 'Sign Up Now' button changes to 'Apply Code' button after create account on deals page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to deals page
	And I click on exclusive deal Sign Up Now button
	And I entered valid data on the create account popup
	| First Name | Last Name | Postal Code | Email                      | Password |
	| Qatester   | NextgenQA | As per Property       | QATester301@gmail.com | $Reset123$  |
	And I click on Create button
	Then I should not see Sign up Now button
	And I should see User menu link at top

@TC13453 @smoke
Scenario: 13453_Verify that correct offer code should be populated automatically in booking engine when click on 'Apply Code' button on respective offer.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| QATester301+1@gmail.com | $Reset123$ |
	#When I click on Sign In link under header section
	#And I enter valid login details
	| Email                    | Password   |
	| QATester301+1@gmail.com | $Reset123$ |
	#And I click Sign In button
	And I navigate from Home page to deals page
	And I click on apply code button
	| OfferCode |
	| ESAVER |
	Then I should see automatically populated correct offer code in booking engine
	| OfferCode |
	| ESAVER |
	And I should see User menu link at top

@TC13455 @smoke
Scenario: 13455_Special Offers: Verify that user able to see ESAVER related suites on suite page when serach suites through 'Apply Code' button on Deal page.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| QATester301+1@gmail.com | $Reset123$ |
	#When I click on Sign In link under header section
	#And I enter valid login details
	    | Email                    | Password   |
	    | QATester301+1@gmail.com | $Reset123$ |
	#And I click Sign In button
	And I navigate from Home page to deals page
	And I click on apply code button
	    | OfferCode |
	    | ESAVER |
	When I enter valid details on Booking Widget with dates more than 60 days through deals page
        | Check In Date | Check Out Date | Adults | 
        |  More than 60 days from today |2 days more than Check In date  | 2 |
	And I click Book Now button
	Then I should see applied offer code related suites
	    | OfferCode |
	    | ESAVER |
	And I should see User menu link at top

	
@TC13456 @smoke
Scenario: 13456_Verify that user able to see MOREFUN related suites on suite page when serach suites through 'Apply Code' button on Deal page.
	Given I Open New browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| QATester301+1@gmail.com | $Reset123$ |
	#When I click on Sign In link under header section
	#And I enter valid login details
	    #| Email                    | Password   |
	    #| QATester301@gmail.com | $Reset123$ |
	#And I click Sign In button
	And I navigate from Home page to deals page
	And I click on apply code button
	    | OfferCode |
	    | MOREFUN |
	When I enter valid details on Booking Widget with dates within 60 days through deals page
        | Check In Date | Check Out Date | Adults | 
        |  Within 60 days from today |2 days more than Check In date  | 2 |
	And I click Book Now button
	Then I should see applied offer code related suites
	    | OfferCode |
	    | MOREFUN |
	And I should see User menu link at top

@TC13460 @smoke
Scenario: 13460_Verify that user able to see alert message for MOREFUN when select dates after 60 days from today's date and search suites 'Apply Code' button on Deal page.
	Given I relaunch browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| QATester301+1@gmail.com | $Reset123$ |
	#When I click on Sign In link under header section
	#And I enter valid login details
	    #| Email                    | Password   |
	    #| QATester301@gmail.com | $Reset123$ |
	#And I click Sign In button
	And I navigate from Home page to deals page
	And I click on apply code button
	    | OfferCode |
	    | MOREFUN |
	When I enter valid details on Booking Widget with dates more than 60 days through deals page
        | Check In Date | Check Out Date | Adults | 
        |  More than 60 days from today |2 days more than Check In date  | 2 |
	And I click Book Now button
	Then I should see validation error
	And I should see User menu link at top

@TC13461 @smoke
Scenario: 13461_Verify that user able to see BBAR related suites on suite page  when search suites with outside offer code criteria through 'Apply Code' button on Deal page.
	Given I Open New browser and navigate to Property Home page
	When I Sign In to Application
	| Email Address             | Password   |
	| QATester301+1@gmail.com | $Reset123$ |
	#When I click on Sign In link under header section
	#And I enter valid login details
	    #| Email                    | Password   |
	    #| QATester301@gmail.com | $Reset123$ |
	#And I click Sign In button
	And I navigate from Home page to deals page
	And I click on apply code button
	    | OfferCode |
	    | MOREFUN |
	When I enter valid details on Booking Widget with dates more than 60 days through deals page
        | Check In Date | Check Out Date | Adults | 
        |  More than 60 days from today |2 days more than Check In date  | 2 |
	And I click Book Now button
	Then I should see BBAR related suites
	And I should see User menu link at top

@TC13255 @smoke
Scenario:  13255_Verify that 'SIGN UP NOW!' button should be displayed instead of 'SIGN UP - IT'S FREE!' for the deals which require login.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to deals page
	Then I should see 'SIGN UP NOW!' button on deals page
	|SignUpButtonText |
	|SIGN UP NOW!|

@TC12652 @smoke
Scenario: 12652_Verify that If user is not logged In then offer code should not be auto fill into booking engine on T3 page for deal that required sign in.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to deals page
	And I click on deal title that required sign in
	Then I should not see offer code auto fill into booking engine

@TC12654 @smoke
Scenario: 12654_Verify that after sign in using 'SIGN IN NOW TO REVEAL THE OFFER CODE' button then offer code should be auto filled and displayed in offer code section.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to deals page
	And I click on deal title that required sign in
	And I Sign in into application through 'Sign In Now To Reveal The Offer Code' button
	| Email                 | Password   |
	| qatester301+1@gmail.com | $Reset123$ |
	Then I should see autofilled correct offer code under booking engine	
	And I should see User menu link at top

@TC12655 @smoke
Scenario:  12655_Verify that when user sign in using 'SIGN IN NOW TO REVEAL THE OFFER CODE' button then button should not be displayed on T3 page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to deals page
	And I click on ESAVER deal title that required sign in
	And I Sign in into application through 'Sign In Now To Reveal The Offer Code' button
	| Email                 | Password   |
	| qatester301+1@gmail.com | $Reset123$ |
	Then I should not see 'SIGN IN NOW TO REVEAL THE OFFER CODE' button on T3 Page
	And I should see User menu link at top

@TC12656 @smoke
Scenario: 12656_Verify that when user Sign out from T3 page of deal where sign in require for offer code then 'SIGN IN NOW TO REVEAL THE OFFER CODE' button should be displayed on T3 page.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to deals page
	And I click on ESAVER deal title that required sign in
	And I Sign In to Application
    | Email Address        | Password   |
    | qatester301+1@gmail.com| $Reset123$ |
	Then I should not see 'SIGN IN NOW TO REVEAL THE OFFER CODE' button on T3 Page
	When I am sign out from application
	Then I should see 'SIGN IN NOW TO REVEAL THE OFFER CODE' button on T3 Page


@TC13458 @smoke
Scenario: 13458_Verify that user able to see offer code related suites on suite page after filing all the info in Booking engine on 'deals details' page and click on 'Book Now' button.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to deals page
	And I click on ESAVER deal title that required sign in
	And I Sign In to Application
    | Email Address        | Password   |
    | qatester301+1@gmail.com| $Reset123$ |
	When I enter valid details on Booking Widget with dates more than 60 days through deals page
        | Check In Date | Check Out Date | Adults | 
        |  More than 60 days from today |2 days more than Check In date  | 2 |
	And I click Book Now button
	Then I should see applied offer code related suites
	    | OfferCode |
	    | ESAVER |
	And I should see User menu link at top

@TC13459 @smoke
Scenario: 13459_Verify that user able to see alert message for ESAVER when select next dates within 60 days from today's date and search suites through 'Apply Code' button on Deal page.
	Given I relaunch browser and navigate to Property Home page
	When I click on Sign In link under header section
	And I enter valid login details
	    | Email                    | Password   |
	    | qatester301+1@gmail.com | $Reset123$ |
	And I click Sign In button
	And I navigate from Home page to deals page
	And I click on apply code button
	    | OfferCode |
	    | ESAVER |
   And I enter check in dates within 60 days from today and check out date on booking widget of deals page
      | Check In Date | Check Out Date |  Adults |
      |  Within 60 days from today |1 days more than Check In date  |    2    |
   And I click Book Now button
   Then I should see validation error
   And I should see User menu link at top

@TC15419 @smoke
Scenario: 15419_Special offers > Birthday Parties - Submit birthday form with select Preferred Contact Method as Email.
	Given I Open New browser and navigate to Property Home page
	When I navigate from Home page to Birthday Parties page
	And I eneter contact information on birthday form
	| First Name | Last Name | Address   | Postal Code | City     | Email                    | Phone      | Contact Method | Country |
	| QA Test    | Nextgenqa | MG Street | 10005       | New York | gwr.nextgenqa1@gmail.com | 9392998188 | Phone          | No  |
	And I eneter Party Details on birthday form
	| Month | Date | Year | Children Attending Unser Age 3 |
	| Apr   | 11   | 2021 | No                             |
	And I eneter birthday child info on birthday form
	| Child FName  | Child LName | Month | Date | Year |
	| ChildQAFname | Nextgenqa   | Apr   | 11   | 2011 |
	And I click on Submit Form Button

@TC15420 @smoke
Scenario: 15420_Special offers > Birthday Parties - Submit birthday form with select Preferred Contact Method as Phone.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Birthday Parties page
	And I eneter contact information on birthday form
	| First Name | Last Name | Address   | Postal Code | City     | Email                    | Phone      | Contact Method | Country |
	| QA Test    | Nextgenqa | MG Street | 10005       | New York | gwr.nextgenqa1@gmail.com | 9392998188 | Phone          | No  |
	And I eneter Party Details on birthday form
	| Month | Date | Year | Children Attending Unser Age 3 |
	| Apr   | 11   | 2021 | No                             |
	And I eneter birthday child info on birthday form
	| Child FName  | Child LName | Month | Date | Year |
	| ChildQAFname | Nextgenqa   | Apr   | 11   | 2011 |
	And I click on Submit Form Button

@TC15421 @smoke
Scenario: 15421_Special offers > Birthday Parties - Submit birthday form with select 'Will Children Under 3 be Attending?' option as Yes
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Birthday Parties page
	And I eneter contact information on birthday form
	| First Name | Last Name | Address   | Postal Code | City     | Email                    | Phone      | Contact Method | Country |
	| QA Test    | Nextgenqa | MG Street | 10005       | New York | gwr.nextgenqa1@gmail.com | 9392998188 | Phone          | No  |
	And I eneter Party Details on birthday form
	| Month | Date | Year | Children Attending Unser Age 3 |
	| Apr   | 11   | 2021 | Yes                             |
	And I eneter birthday child info on birthday form
	| Child FName  | Child LName | Month | Date | Year |
	| ChildQAFname | Nextgenqa   | Apr   | 11   | 2011 |
	And I click on Submit Form Button

@TC15423 @smoke
Scenario: 15423_Special offers > Birthday Parties - Submit birthday form with select 'Will Children Under 3 be Attending?' option as No
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Birthday Parties page
	And I eneter contact information on birthday form
	| First Name | Last Name | Address   | Postal Code | City     | Email                    | Phone      | Contact Method | Country |
	| QA Test    | Nextgenqa | MG Street | 10005       | New York | gwr.nextgenqa1@gmail.com | 9392998188 | Phone          | No  |
	And I eneter Party Details on birthday form
	| Month | Date | Year | Children Attending Unser Age 3 |
	| Apr   | 11   | 2021 | No                             |
	And I eneter birthday child info on birthday form
	| Child FName  | Child LName | Month | Date | Year |
	| ChildQAFname | Nextgenqa   | Apr   | 11   | 2011 |
	And I click on Submit Form Button

@TC15424 @smoke
Scenario: 15424_Special offers > Birthday Parties - Submit birthday form with select Canadian address.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Birthday Parties page
	And I eneter contact information on birthday form
	| First Name | Last Name | Address   | Postal Code | City     | Email                    | Phone      | Contact Method | Country |
	| QA Test    | Nextgenqa | MG Street | 10005       | New York | gwr.nextgenqa1@gmail.com | 9392998188 | Phone          | Canada  |
	And I eneter Party Details on birthday form
	| Month | Date | Year | Children Attending Unser Age 3 |
	| Apr   | 11   | 2021 | No                             |
	And I eneter birthday child info on birthday form
	| Child FName  | Child LName | Month | Date | Year |
	| ChildQAFname | Nextgenqa   | Apr   | 11   | 2011 |
	And I click on Submit Form Button

@TC15425 @smoke
Scenario: 15425_Special offers > Birthday Parties - Verify dropdown values for number of adult and children.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Birthday Parties page
	Then I should see valid values in no of children drop down
	And I should see valid values in no of Adult drop down


		