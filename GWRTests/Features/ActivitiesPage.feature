﻿Feature: ActivitiesPage
	In order to follow the booking flow for particular property
	As a end user
	I want to add Activities Package

@TC5702 @Smoke
Scenario: 5702_Verify that user able to add Attraction packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
    Then I should see added package under cost summary
	
@TC5728 @Smoke
Scenario: 5728_Verify that user should be remove added Attraction packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Package Remove link
	Then I should remove added package under cost summary
    
@TC5729 @Smoke
Scenario: 5729_Verify that user should be add removed Attraction packages again.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I click on Package Remove link
	And I add Attractions Package
    Then I should see added package under cost summary
	
@TC5710 @Smoke
Scenario: 5710_Verify that user should be add packages under the Family tab	
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Family Tab
	And I add Family Package
    Then I should see added package under cost summary

@TC12844 @Smoke
Scenario: 12844_Verify that user able to remove added Family packages.	
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Family Tab
	And I add Family Package
	And I click on Package Remove link
   	Then I should remove added package under cost summary

@TC12846 @Smoke
Scenario: 12846_Verify that user should be add removed Family packages again.	
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Family Tab
	And I add Attractions Package
	And I click on Package Remove link
	And I add Attractions Package
    Then I should see added package under cost summary

@TC5711 @Smoke
Scenario: 5711_Verify that user should be add packages under the Birthday tab
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Birthday Tab
	And I add Birthday Package
    Then I should see added package under cost summary

@TC12845 @Smoke
Scenario: 12845_Verify that user able to remove added Birthday packages.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Birthday Tab
	And I add Birthday Package
	And I click on Package Remove link
	Then I should remove added package under cost summary

@TC12847 @Smoke
Scenario: 12847_Verify that user should be add removed Birthday packages again.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Birthday Tab
	And I add Birthday Package
	And I click on Package Remove link
	And I add Birthday Package
	Then I should see added package under cost summary

@TC5709 @Smoke
Scenario: 5709_Verify that user should be navigate on suite page from Activities page after click on 'Suite' icon.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on Suites Icon
	Then I should navigate on suite page

@TC5708 @Smoke
Scenario: 5708_Verify that user should be navigate on dining page after click on 'Continue To Dining Packages' button.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1    | 2        | AMTCERT    |
	And I click on Continue to Dining Packages button
	Then I should navigate on dining page

@TC5753 @Smoke
Scenario: 5753_Verify that Promotional savings value and Your Savings value should match
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
    Then I should see Promotional saving and Your Savings price are same

@TC5735 @Smoke
Scenario: 5735_Verify that 2 pm Late Check Out package should be showing under cost summary on activities page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
    | Adults | Kids | Kids1Age | Offer Code |
    |   2    | 1    |    3     |  AMTCERT   |
    And I navigate from suite Page to Activities page with LCO
	Then I should see 2 PM LOC package added under cost summary

@TC10062 @Smoke
Scenario: 10062_Verify that packages drop down quantity should be up to guest count
	Given I relaunch browser and navigate to Property Home page
    When I select check In date
	And I select check Out date
	And I select Adults
	| Adults |
	| 2   |
	And I select Kids
	| Kids | Kids1Age |
	|  1   |  2       |
	And I observe Guests count
	And I enter Offer Code
	| Offer Code |
	|   AMTCERT  |
	And I click on Book Now button
	And I navigate from suite Page to Activities page without LCO
	And I navigate to Family Tab
    Then I should see package quantity upto selected guest count

@TC5738 @Smoke
Scenario:  5738_User able to add multiple package quantity from Attraction packages
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
    | Adults | Kids | Kids1Age | Offer Code |
    |   3    | 1    |    2     |  AMTCERT   |
    And I navigate from suite Page to Activities page without LCO
	And I navigate to Attractions Tab
	And I add attraction package for multiple quantity
	Then I should see added package under cost summary
	And  I should see correct attraction package price under cost summary

@TC5721 @Smoke
Scenario: 5721_Verify that after add attractions packages then packages quantity drop down displayed under the cost summary.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
    Then I should see added package quantity drop down under cost summary

@TC5726 @Smoke
Scenario: 5726_Verify that attractions packages cost should be displayed as per selected quantity from packages drop down under cost summary.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 5      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I update package quantity under cost summary
	Then I should see correct reflected package cost under cost summary

@TC5750 @Smoke
Scenario: 5750_Verify that package total should be addition of all packages under cost summary on Activities page.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I navigate to Birthday Tab
	And I add Birthday Package
	Then I see Package Total price should equals to addition of all added packages

@TC5749 @Smoke
Scenario: 5749_Verify that 'Suite Total' and 'Total' should same without any package selection or added under cost summary on Activities page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
    | Adults | Kids | Kids1Age | Offer Code |
    |   2    | 1    |    2     |  AMTCERT   |
    And I navigate from suite Page to Activities page without LCO
	Then I Should see Suite Total price equals to Total price on Activities page without any package added

@TC5751 @Smoke
Scenario: 5751_Verify that total should be correct after adding packages under cost summary section
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	Then I should see correct Total after added package

@TC12851 @Smoke
Scenario: 12851_Verify that user able to see correct package total after update package quantity under cost summary on activities page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I update package quantity under cost summary
	Then I should see correct package total after update package quantity under cost summary

@TC12850 @Smoke
Scenario: 12850_Verify that user able to update added package quantity under cost summary on activities page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1    | 2        | AMTCERT    |
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I update package quantity under cost summary
	Then I should see updated package quantity under cost summary

@TC5733 @Smoke
Scenario: 5733_Verify that selected suite cost should be displayed under the cost summary
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
    | Adults | Kids | Kids1Age | Offer Code |
    |   2    | 1    |    2     |  AMTCERT   |
	And I observe Suite price
	And I observe number of Stay Nights
    And I navigate from suite Page to Activities page without LCO
	Then I should see suite total equals to selected suite price with number of stay nights

@TC5745 @Smoke
Scenario: 5745_Verify that user see correct suite total under cost summary section
	Given I relaunch browser and navigate to Property Home page
    When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1    | 2        | AMTCERT    |
	Then I should see Suite Total equals to Base price minus Promotional Saving price

@TC5743 @Smoke
Scenario: 5743_Verify that user should see 'Base' price correctly on Activities page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
    | Adults | Kids | Kids1Age | Offer Code |
    |   2    | 1    |    2     |  AMTCERT   |
	And I observe Suite Was price
	And I observe number of Stay Nights
    And I navigate from suite Page to Activities page without LCO
	Then I should see suite base price equals to multiplication of suite was price and number of stays nights

@TC5742 @Smoke
Scenario: 5742_Verify that user should see 'Promotional Saving' price correctly under 'Cost Summary' on 'Activities' page
	Given I relaunch browser and navigate to Property Home page
    When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 3      | 1    | 2        | AMTCERT    |
	Then I should see Promotional Saving price correctly under cost summary on activities page

@TC5731 @Smoke
Scenario: 5731_Verify that user see correct adult and Kids count in cost summary section
	Given I relaunch browser and navigate to Property Home page
	When I select check In date
	And I select check Out date
	And I select Adults
	| Adults |
	| 2   |
	And I select Kids
    | Kids | Kids1Age |
    |   1   |    2      |
	And I observe Adults and Kids count
	And I enter Offer Code
	| Offer Code |
	|   AMTCERT  |
	And I click on Book Now button
	And I navigate from suite Page to Activities page with LCO
	Then I should see correct adults count under cost summary
	And I should see correct kids count under cost summary

@TC5736 @Smoke
Scenario: 5736_If user add packages from activities page and go back to suite page and select another suite then previously added packages should not displayed showing under cost summary on activities page
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
    | Adults | Kids | Kids1Age | Offer Code |
    |   2    | 1    |    2     |  AMTCERT   |
    And I navigate from suite Page to Activities page without LCO
	And I navigate to Attractions Tab
	And I add Attractions Package
	And I navigate to Family Tab
	And I add Family Package
	And I click on Suites Icon
	#And I Click on Standard Tab
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | NoPremium   | Standard     | No               |
	And I navigate from suite Page to Activities page without LCO
	Then I should not see previously added packages under cost summary

@TC5756 @Smoke
Scenario: 5756_If user select 2 PM late check out suite and navigate on activities page then goback to suite and select other 2 PM late check out suite and navigate to activites page then latest 2 PM selected suite should display once
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
    | Adults | Kids | Kids1Age | Offer Code |
    |   2    | 1    |    2     |  AMTCERT   |
    And I navigate from suite Page to Activities page with LCO
	And I click on Suites Icon
	#And I Click on Standard Tab
	And I Select Room Type and accessible suite
	| ThemeType | PremiumType | StandardType | AccessibleToggle |
	| NoTheme   | NoPremium   | Standard     | No               |
	And I navigate from suite Page to Activities page with LCO
	Then I should see latest 2 PM LCO package added under cost summary
	And I should see correct latest 2 PM LCO package price under cost summary
	And I should not see old 2 PM LCO package under cost summary

@TC11271 @smoke
Scenario: 11271_Verify that correct check in and check out dates should displayed on activities page when user select other dates on rate calendar rather than selected dates in booking engine.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	And I select Chek In and Check Out date
	| No Of Month |
	| 3		|
	And I observe Check In and Check out Date
	And I click on Continue to Package button
	Then I should see correct Check In and Check Out date under cost summary through rate calendar

@TC11273 @smoke
Scenario: 11273_Verify that correct suite total should displayed on activities page when user select other dates on rate calendar rather than selected dates in booking engine.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	And I select Chek In and Check Out date
	| No Of Month |
	| 3		|
	And I observe Suite Price on Rate Calendar
	And I click on Continue to Package button
	Then I should see correct Suite Total under cost summary through rate calendar

@TC11272 @smoke
Scenario: 11272_Verify that correct suite base price should displayed on activities page when user select other dates on rate calendar rather than selected dates in booking engine.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	And I select Chek In and Check Out date
	| No Of Month |
	| 3		|
	And I click on Continue to Package button
	Then I should see correct suite base price should displayed on activities page through rate calendar

@TC11432 @smoke
Scenario: 11432_Verify that user able to see correct Strike through Base price and Promotional savings price and suite total under cost summary on activities page when delete offer code on rate calendar.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	And I click on Delete Icon link	
	And I select Chek In and Check Out date
	| No Of Month |
	| 2		|
	And I observe Suite Price on Rate Calendar
	And I click on Continue to Package button
	Then I should see correct Suite Total under cost summary through rate calendar
	Then I should see correct suite base price should displayed on activities page through rate calendar

@TC11274 @smoke
Scenario: 11274_Verify that correct total should displayed on activities page when user select other dates on rate calendar rather than selected dates in booking engine.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	And I select Chek In and Check Out date
	| No Of Month |
	| 3		|
	And I click on Continue to Package button
	Then I should see corret Total price should displayed on activities page through rate calendar

@TC11270 @smoke
Scenario: 11270_Verify that Promotional savings cost should displayed correctly on activities page when user select other dates on rate calendar rather than selected dates in booking engine.
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Suite page
	| Adults | Kids | Kids1Age | Offer Code |
	| 2      | 1    | 3        | AMTCERT    |
	And I click on 'Rate Calendar' link
	And I select Chek In and Check Out date
	| No Of Month |
	| 3		|
	And I click on Continue to Package button
	Then I should see correct promotional saving price on activities page through rate calendar

@TC9620 @smoke
Scenario: 9620_Verify that when user go through 'Check Rate Calendar' of sold out suite then cost should reflect correct under cost summary on 'Activities' page.
	Given I Open New browser and navigate to Property Home page
	When I enter sold out suite dates on Booking Widget
	| Check In Date   | Check Out Date  | Adults | Offer Code |
	| Sold out suite date from excel | Sold out suite date from excel | 2      | AMTCERT    |
	And I click Book Now button
	And I click on sold out suite 'Rate Calendar' link
	And I select Chek In and Check Out date
	| No Of Month |
	| 3			  |
	And I observe Suite Price on Rate Calendar
	And I click on Continue to Package button
	Then I should see correct Suite Total under cost summary through rate calendar
	And I should see corret Total price should displayed on activities page through rate calendar
	And I should see correct promotional saving price on activities page through rate calendar

@TC15455 @Smoke
Scenario: 15455_Activities > Verify that user able to view details of Family packages.	
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Family Tab
	And I click on Family tab view details link
	Then I should see View details is expanded

@TC15456 @Smoke
Scenario: 15456_Activities > Verify that user able to view details of Birthday packages.	
	Given I relaunch browser and navigate to Property Home page
	When I navigate from Home page to Activities page
	| Adults | Kids | Kids1Age | Offer Code |
	| 4      | 1    | 3        | AMTCERT    |
	And I navigate to Birthday Tab
	And I click on birthday tab view details link
	Then I should see View details is expanded
