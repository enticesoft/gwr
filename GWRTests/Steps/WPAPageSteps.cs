﻿using GWRFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class WPAPageSteps
    {
        [When(@"I Mouse hover on '(.*)' header")]
        public void WhenIMouseHoverOnHeader(string p0)
        {
            Pages.WPAPage.ClickOnWPAMenu();
        }

        [Then(@"I should see Swim,Spalsh,Slide submneu")]
        public void ThenIShouldSeeSwimSpalshSlideSubmneu()
        {
            Boolean ISSwimSplash = Pages.WPAPage.VerifySwimSplashSubmenu();
            Assert.IsTrue(ISSwimSplash, "Swim splash slide submneu is displaying properly");
        }
        [Then(@"I should see Activities submenu")]
        public void ThenIShouldSeeActivitiesSubmenu()
        {
            Boolean ISActivities = Pages.WPAPage.VerifyActivitiesSubmenu();
            Assert.IsTrue(ISActivities, "Activities submneu is displaying properly");
        }
        [Then(@"I should see Attraction submenu")]
        public void ThenIShouldSeeAttractionSubmenu()
        {
            Boolean ISAttraction = Pages.WPAPage.VerifyAttractionSubmenu();
            Assert.IsTrue(ISAttraction, "Attraction submneu is displaying properly");

        }
        [Then(@"I should see Fitness submenu")]
        public void ThenIShouldSeeFitnessSubmenu()
        {
            Boolean ISFitness = Pages.WPAPage.VerifyFitnessSubmenu();
            Assert.IsTrue(ISFitness, "Fitness submneu is displaying properly");
        }
        [Then(@"I should see Explore By Age submenu")]
        public void ThenIShouldSeeExploreByAgeSubmenu()
        {
            Boolean ISExploreAge = Pages.WPAPage.VerifyExploreAgeSubmenu();
            Assert.IsTrue(ISExploreAge, "Explore by Age submneu is displaying properly");
        }
        [Then(@"I should see Calendar submenu")]
        public void ThenIShouldSeeCalendarSubmenu()
        {
            Boolean ISCalendar = Pages.WPAPage.VerifyCalendarSubmenu();
            Assert.IsTrue(ISCalendar, "Calendar submneu is displaying properly");
        }

        [When(@"I click on '(.*)' submenu")]
        public void WhenIClickOnSubmenu(string p0)
        {
            Pages.WPAPage.NavigateToActivitiesPage();
        }

        [When(@"I click on Activities learn more link")]
        public void WhenIClickOnLearnMore()
        {
            Pages.WPAPage.ClickOnLearMoreLink();
        }
        

        [Then(@"I should see Activities page is opened")]
        public void ThenIShouldSeeActivitiesPageIsOpened()
        {
           Boolean ISActivitiesOpened = Pages.WPAPage.VerifyActivitiesPageOpened();
            Assert.IsTrue(ISActivitiesOpened, "I should see Activities page opened");
        }
        [Then(@"I should see only activities packages available")]
        public void ThenIShouldSeeOnlyActivitiesPackagesAvailable()
        {
            Boolean ISCountMatching =Pages.WPAPage.VerifyTotalPkgAndActivityPkgCount();
            Assert.IsTrue(ISCountMatching, "I should see Total package count and Activities package count is same");
        }
        [When(@"I click on Attractions submenu")]
        public void WhenIClickOnAttractionsSubmenu()
        {
            Pages.WPAPage.ClickOnAttractionSubmenu();
        }
        [Then(@"I should see Attractions page is opened")]
        public void ThenIShouldSeeAttractionsPageIsOpened()
        {
            Boolean ISAttractionOpened = Pages.WPAPage.VerifyAttractionPageOpend();
            Assert.IsTrue(ISAttractionOpened, "I should see attractions page opned");
        }
        [Then(@"I should see only Attractions packages available")]
        public void ThenIShouldSeeOnlyAttractionsPackagesAvailable()
        {
            Boolean ISCountMatching = Pages.WPAPage.VerifyTotalPkgAndAttractionsPkgCount();
            Assert.IsTrue(ISCountMatching, "I should see Total package count and Attractions package count is same");
        }
        [When(@"I click on Fitness submenu")]
        public void WhenIClickOnFitnessSubmenu()
        {
            Pages.WPAPage.ClickOnFitnessSubmenu();
        }
        [Then(@"I should see Fitness page is opened")]
        public void ThenIShouldSeeFitnessPageIsOpened()
        {
           Boolean ISFitnessOpened = Pages.WPAPage.VerifyFitnessPageOpened();
            Assert.IsTrue(ISFitnessOpened, "I should see Fitness page opned");
        }

        [Then(@"I should see only Fitness packages available")]
        public void ThenIShouldSeeOnlyFitnessPackagesAvailable()
        {
            Boolean ISCountMatching = Pages.WPAPage.VerifyTotalPkgAndFitnessPkgCount();
            Assert.IsTrue(ISCountMatching, "I should see Fitness packages available");
        }

        [When(@"I click on Swim Splash Slide submenu")]
        public void WhenIClickOnSwimSplashSlideSubmenu()
        {
            Pages.WPAPage.ClickOnSwimSplashSubmenu();
        }

        [When(@"I click on See More Details link")]
        public void WhenIClickOnSeeMoreDetailsLink()
        {
            Pages.WPAPage.ClickOnSeeMoreDetails();
        }

        [When(@"I click on View Safety link")]
        public void WhenIClickOnViewSafetyLink()
        {
            Pages.WPAPage.ClickOnViewStaftyLink();
        }
        

        [Then(@"I should see Swim splash slide page is opened")]
        public void ThenIShouldSeeSwimSplashSlidePageIsOpened()
        {
            Boolean ISSwimSplashOpened = Pages.WPAPage.VerifySwimSplashPageOpened();
            Assert.IsTrue(ISSwimSplashOpened, "I should see Swim Splash slide page is opened");
        }

        [When(@"I click on Calendar submenu")]
        public void WhenIClickOnCalendarSubmenu()
        {
            Pages.WPAPage.ClickOnCalendarSubmenu();
        }

        [Then(@"I should see Calendar page is opened")]
        public void ThenIShouldSeeCalendarPageIsOpened()
        {
            Boolean ISCalendarOpened = Pages.WPAPage.VerifyCalendarPageOpened();
            Assert.IsTrue(ISCalendarOpened, "I should see Calendar page is opened");
        }

        [When(@"I click on Explore By Age submenu")]
        public void WhenIClickOnExploreByAgeSubmenu()
        {
            Pages.WPAPage.ClickOnExploreByAgeSubmenu();
        }

        [Then(@"I should see Explore By Age page is opened")]
        public void ThenIShouldSeeExploreByAgePageIsOpened()
        {
            Boolean ISExplorePageOpened = Pages.WPAPage.VerifyExploreByAgePageOpened();
            Assert.IsTrue(ISExplorePageOpened, "I should see Explore By Age page is opened");
        }

        [Then(@"I should see Swim splash slide packages available")]
        public void ThenIShouldSeeSwimSplashSlidePackagesAvailable()
        {
            Boolean ISCountMatching = Pages.WPAPage.VerifyTotalPkgAndSwimSplashPkgCount();
            Assert.IsTrue(ISCountMatching, "I should see swim splash packages only");
        }

        [Then(@"I should see all type of packages available")]
        public void ThenIShouldSeeAllTypeOfPackagesAvailable()
        {
            Boolean ISPkgExist = Pages.WPAPage.VerifyExploreByAgePkg();
            Assert.IsTrue(ISPkgExist, "I should see all type packages");
        }

        [When(@"I uncheck all checked filter checkboxes")]
        public void WhenIUncheckAllCheckedFilterCheckboxes()
        {
            Pages.WPAPage.UncheckAllCheckbox();
        }
        [When(@"I check only adult checkbox")]
        public void WhenICheckOnlyAdultCheckbox()
        {
            Pages.WPAPage.CheckAdultCheckbox();
        }

        [Then(@"I should see only adult related packages dispalying")]
        public void ThenIShouldSeeOnlyAdultRelatedPackagesDispalying()
        {
            Pages.WPAPage.IClickLearnMoreLink();
            Boolean ISAdultIconPresent = Pages.WPAPage.VerifyAdultIcon();
            Assert.IsTrue(ISAdultIconPresent, "I should see Adult related package diplaying");
        }
        [When(@"I check only Children checkbox")]
        public void WhenICheckOnlyChildrenCheckbox()
        {
            Pages.WPAPage.CheckChildrenCheckbox();
        }
        [Then(@"I should see only children related packages displaying")]
        public void ThenIShouldSeeOnlyChildrenRelatedPackagesDisplaying()
        {
            Pages.WPAPage.IClickLearnMoreLink();
            Boolean ISChildrenIconPresent = Pages.WPAPage.VerifyChildrenIcon();
            Assert.IsTrue(ISChildrenIconPresent, "I should see Adult related package diplaying");
        }

        //
        [When(@"I check Family checkbox")]
        public void WhenICheckFamilyCheckbox()
        {
            Pages.WPAPage.CheckFamilyCheckbox();
        }

        [Then(@"I should see only family related packages displaying")]
        public void ThenIShouldSeeOnlyFamilyRelatedPackagesDisplaying()
        {
            Pages.WPAPage.IClickLearnMoreLink();
            Boolean ISFamilyIconPresent = Pages.WPAPage.VerifyFamilyIcon();
            Assert.IsTrue(ISFamilyIconPresent, "I should see  Family related package diplaying");
        }

        [When(@"I check Teens checkbox")]
        public void WhenICheckTeensCheckbox()
        {
            Pages.WPAPage.CheckTeensCheckbox();
        }

        [Then(@"I should see only Teens related packages displaying")]
        public void ThenIShouldSeeOnlyTeensRelatedPackagesDisplaying()
        {
            Pages.WPAPage.IClickLearnMoreLink();
            Boolean ISTeensIconPresent = Pages.WPAPage.VerifyTeensIcon();
            Assert.IsTrue(ISTeensIconPresent, "I should see Teens related package diplaying");
        }

        [When(@"I check Toddler checkbox")]
        public void WhenICheckToddlerCheckbox()
        {
            Pages.WPAPage.CheckToddlersCheckbox();
        }

        [Then(@"I should see only Toddler related packages displaying")]
        public void ThenIShouldSeeOnlyToddlerRelatedPackagesDisplaying()
        {
            Pages.WPAPage.IClickLearnMoreLink();
            Boolean ISToddlersIconPresent = Pages.WPAPage.VerifyToddlersIcon();
            Assert.IsTrue(ISToddlersIconPresent, "I should see Toddlers related package diplaying");
        }

        [Then(@"I should see Title of package")]
        public void ThenIShouldSeeTitleOnT3()
        {
            Boolean IST3Title = Pages.WPAPage.VerifyT3Title();
            Assert.IsTrue(IST3Title, "I should see Title of package.");
        }

        [Then(@"I should see Activity Title of package")]
        public void ThenIShouldSeeActivityTitleOnT3()
        {
            Boolean IST3Title = Pages.WPAPage.VerifyT3TitleActivity();
            Assert.IsTrue(IST3Title, "I should see Activity Title of package");
        }
        

        [Then(@"I should see Available Hours section")]
        public void ThenIShouldSeeHoursSecT3()
        {
            Boolean IST3Hours = Pages.WPAPage.VerifyT3HoursSection();
            Assert.IsTrue(IST3Hours, "I should see Available Hours section");
        }

        [Then(@"I should see Thumbnail section")]
        public void ThenIShouldSeeThumbnailSecT3()
        {
            Boolean IST3Thumbnail = Pages.WPAPage.VerifyT3ThumbSection();
            Assert.IsTrue(IST3Thumbnail, "I should see Thumbnail section");
        }

        [Then(@"I should see Best For section")]
        public void ThenIShouldSeeBestForSecT3()
        {
            Boolean IST3Thumbnail = Pages.WPAPage.VerifyT3BestForSec();
            Assert.IsTrue(IST3Thumbnail, "I should see Best For section");
        }
        

        [Then(@"I should see description section")]
        public void ThenIShouldSeeWaterParkDescriptioSecT3()
        {
            Boolean IST3Description = Pages.WPAPage.VerifyT3WaterParkSection();
            Assert.IsTrue(IST3Description, "I should see description section");
        }

        [Then(@"I should see Safety Rule section")]
        public void ThenIShouldSeeSafetyRuleSection()
        {
            Boolean IST3SafetyRule = Pages.WPAPage.VerifySafetyRuleSec();
            Assert.IsTrue(IST3SafetyRule, "I should see Safety Rule section");
        }
        
    }

}
