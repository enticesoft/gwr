﻿using System;
using TechTalk.SpecFlow;
using GWRFramework;
using NUnit.Framework;

namespace GWRTests.Steps
{
    [Binding]
    public class GlobalHomePageSteps
    {
        [Given(@"I am on GWR Global Home page")]
        public void GivenIAmOnGWRGlobalHomePage()
        {
          Browser.NavigateTo();
        }
        
        [When(@"I click on Choose a Location button")]
        public void WhenIClickOnButton()
        {
            Pages.GlobalHome.clickChooseLocationBtn();
        }
        
        [When(@"I select a property")]
        public void WhenISelect()
        {
            Pages.GlobalHome.selectProperty();
        }

        [Then(@"I should see region wise location list")]
        public void ThenIShouldSeeRegionList()
        {
            Boolean locationList = Pages.GlobalHome.checkLocationList();
            Assert.IsTrue(locationList, "Region wise location list displayed.");
        }

        [Then(@"I land on home page of property")]
        public void ThenILandOnHomePageOfProperty()
        {
            Boolean CheckInDate = Pages.PropertyHome.propertyHomePageDisplayed();
            Assert.IsTrue(CheckInDate, "Check In Date field is displayed on property page.");
        }

        [Then(@"I land on home page of property and verify Banner")]
        public void ThenILandOnHomePageAndBannerOfProperty()
        {
            Boolean HeroBanner = Pages.PropertyHome.propertyHomePageHeroBannerDisplayed();
            Assert.IsTrue(HeroBanner, "I land on home page of property and verify Banner");
        }

        [Then(@"I land on home page of property and verify Room Card")]
        public void ThenILandOnHomePageAndRoomCardOfProperty()
        {
            Boolean RoomCard = Pages.PropertyHome.propertyHomePageRoomCardDisplayed();
            Assert.IsTrue(RoomCard, "I land on home page of property and verify Room Card");
        }

        [Then(@"I land on home page of property and verify Amenities")]
        public void ThenILandOnHomePageAndAminitiesSec()
        {
            Boolean Aminity = Pages.PropertyHome.propertyHomePageAminitiesSection();
            Assert.IsTrue(Aminity, "I land on home page of property and verify Amenities");
        }

        [Then(@"I land on home page of property and verify clicable tile section")]
        public void ThenILandOnHomePageAndClickableTileSec()
        {
            Boolean ClickblTile = Pages.PropertyHome.propertyHomePageclicableTileSection();
            Assert.IsTrue(ClickblTile, "I land on home page of property and verify clicable tile section");
        }

        [Then(@"I click on dining clickable tile and should see Dining Page")]
        public void ThenIShouldSeeDinignPage()
        {
            Boolean ClickblTile = Pages.PropertyHome.propertyHomePageDiningClickbleTile();
            Assert.IsTrue(ClickblTile, "I should see Dining Page");
        }

        [Then(@"I click on attraction clickable tile and should see Attraction Page")]
        public void ThenIShouldSeeAttractionPage()
        {
            Boolean ClickblTileAttraction = Pages.PropertyHome.propertyHomePageAttractionClickbleTile();
            Assert.IsTrue(ClickblTileAttraction, "I should see Attraction Page");
        }

        [Then(@"I click on activity clickable tile and should see Activity Page")]
        public void ThenIShouldSeeActivityPage()
        {
            Boolean ClickblTileactivity = Pages.PropertyHome.propertyHomePageActivityClickbleTile();
            Assert.IsTrue(ClickblTileactivity, "I should see Activity Page");
        }

        [Then(@"I land on home page of property and verify about section")]
        public void ThenILandOnHomePageAndAboutOfProperty()
        {
            Boolean AboutSec = Pages.PropertyHome.propertyHomePageAboutDisplayed();
            Assert.IsTrue(AboutSec, "I land on home page of property and verify about section");
        }

        [Then(@"I land on home page of property and verify gallery section")]
        public void ThenILandOnHomePageAndGalleryOfProperty()
        {
            Boolean GallerySec = Pages.PropertyHome.propertyHomePageGalleryDisplayed();
            Assert.IsTrue(GallerySec, "I land on home page of property and verify gallery section");
        }

        [Then(@"I land on home page of property and verify water park hours section")]
        public void ThenILandOnHomePageAndWaterParkHrsOfProperty()
        {
            Boolean WaterParkHrs = Pages.PropertyHome.propertyHomePageWaterParkHrsDisplayed();
            Assert.IsTrue(WaterParkHrs, "I land on home page of property and verify water park hours section");
        }
        

        [Then(@"I land on home page of property and verify Deal section")]
        public void ThenILandOnHomePageAndDealOfProperty()
        {
            Boolean DealSec = Pages.PropertyHome.propertyHomePageDealSecDisplayed();
            Assert.IsTrue(DealSec, "I land on home page of property and verify Deal section");
        }

        [When(@"I click on Start Saving button")]
        public void clickONStarSvngBtn()
        {
            Pages.PropertyHome.clickOnStartSavingBtn();
        }

        [Then(@"I should see special offer title")]
        public void ThenILandOnSpecialOffer()
        {
            Boolean specialOffertitle = Pages.PropertyHome.specialOfferTitleDisplayed();
            Assert.IsTrue(specialOffertitle, "I should see special offer title");
        }

        [When(@"I click on GWR Logo at top")]
        public void clickONGWRLOGOBtn()
        {
            Pages.PropertyHome.clickOnGWRLogoBtn();
        }
        

        [Then(@"I should see Global deal bar on Home Page")]
        public void ThenIshldSeeGlobalDeal()
        {
            Boolean result = Pages.PropertyHome.globalDealOnHomePage();
            Assert.IsTrue(result, "I should see Global deal bar on Home Page");
        }

        [When(@"I click on Deal Bar on Home page.")]
        public void clickONDealbarOnHomePage()
        {
            Pages.PropertyHome.clickOnGlobalDealBarOnHomePage();
        }

        [When(@"I click on Deal Bar close link on Home page.")]
        public void clickONDealbarCloseLinkOnHomePage()
        {
            Pages.PropertyHome.clickOnCloseDealGridCompnt();
        }
        
        [Then(@"I should see Global deal bar on Home Page is collapse")]
        public void ThenIshldSeeDealBarIsCollapse()
        {
            Boolean result = Pages.PropertyHome.dealbarIsCollapse();
            Assert.IsTrue(result, "I should see Global deal bar on Home Page is collapse");
        }

        [Then(@"I should see deals in Global deal bar on Home Page")]
        public void ThenIshldSeeDealsInGlobalDealBar()
        {
            Boolean result = Pages.PropertyHome.globalDealsisDisplayedOnHomePage();
            Assert.IsTrue(result, "I should see deals in Global deal bar on Home Page");
        }
        
        [When(@"I click on Deal title of Global deal on Home page.")]
        public void clickONDealTitleOnHomePage()
        {
            Pages.PropertyHome.clickOnGloblDealTitle();
        }

        [When(@"I click on Apply code button of Global deal on Home page.")]
        public void clickONApplyCodeOnHomePage()
        {
            Pages.PropertyHome.clickOnApplyCodeBtn();
        }

        [Then(@"I should see offer code reflect on plan page.")]
        public void ThenIshldSeeValOfferCodeDisplayed()
        {
            Boolean result = Pages.PropertyHome.verifyOffrCdOnPlanPage();
            Assert.IsTrue(result, "I should see offer code reflect on plan page.");
        }

        [Then(@"I observe exclusive deal have sign up button when user is not logged In")]
        public void ThenIObsevreSignUpBtn()
        {
            Boolean result = Pages.PropertyHome.verifyExclDealSignUpBtn();
            Assert.IsTrue(result, "I observe exclusive deal have sign up button when user is not logged In");
        }
        
        [Then(@"I observe exclusive deal have sign in link when user is not logged In")]
        public void ThenIObsevreSignInlink()
        {
            Boolean result = Pages.PropertyHome.verifyExclDealSignInLink();
            Assert.IsTrue(result, "I observe exclusive deal have sign in link when user is not logged In");
        }

        [Then(@"I should see exclusive deal have Apply Offer Code button when user is logged In")]
        public void ThenIObsevreApplyOferCodeBtn()
        {
            Boolean result = Pages.PropertyHome.verifyExclDealApplyCodeBtn();
            Assert.IsTrue(result, "I should see exclusive deal have Apply Offer Code button when user is logged In");
        }
        
        [Then(@"I should see exclusive deal have Offer Code when user is logged In")]
        public void ThenIObsevreOferCode()
        {
            Boolean result = Pages.PropertyHome.verifyExclDealOfferCode();
            Assert.IsTrue(result, "I should see exclusive deal have Offer Code when user is logged In");
        }
        
        [When(@"I click on Sign Up button of Global deal on Home page.")]
        public void clickONSignUpOnHomePage()
        {
            Pages.PropertyHome.clickOnExclDealSignUpBtn();
        }
        
        [Then(@"I should see Create account pop is open.")]
        public void ThenIshouldSeeCreatAcPopUpOpen()
        {
            Boolean result = Pages.PropertyHome.CreateAcPopOpen();
            Assert.IsTrue(result, "I should see Create account pop is open.");
        }
        
        [When(@"I click on Sign In button of Global deal on Home page.")]
        public void clickONSignInOnHomePage()
        {
            Pages.PropertyHome.clickOnExclDealSignInBtn();
        }
        
        [Then(@"I should see Sign In pop is open.")]
        public void ThenIshouldSeeSignInPopUpOpen()
        {
            Boolean result = Pages.PropertyHome.SignInPopUpOpen();
            Assert.IsTrue(result, "I should see Sign In pop is open.");
        }
        
        [When(@"I click on See More Offer link of Global deal on Home page.")]
        public void clickONSeeOfferLinkOnHomePage()
        {
            Pages.PropertyHome.seeOfferDetaiLink();
        }
        
        [Then(@"I should see special offfer T2 page.")]
        public void ThenIshouldSeeSpecialOfferT2()
        {
            Boolean result = Pages.PropertyHome.specialOfferPageT2();
            Assert.IsTrue(result, "I should see special offfer T2 page.");
        }

        [Then(@"I should see West locations")]
        public void ThenIShouldSeewestLocationList()
        {
            Boolean locationList = Pages.GlobalHome.westSouthCalLocation();
            Assert.IsTrue(locationList, "I should see West South cal locations");

            Boolean locationList1 = Pages.GlobalHome.westScotDaleLocation();
            Assert.IsTrue(locationList1, "I should see West Scot dale locations");

            Boolean locationList2 = Pages.GlobalHome.westSanFransiscoLocation();
            Assert.IsTrue(locationList2, "I should see West San fransisco locations");

            Boolean locationList3 = Pages.GlobalHome.westGrandMountLocation();
            Assert.IsTrue(locationList3, "I should see West Grand mount locations");

            Boolean locationList4 = Pages.GlobalHome.westColoradoSpringLocation();
            Assert.IsTrue(locationList4, "I should see West Colorado locations");
        }

        [Then(@"I should see South locations")]
        public void ThenIShouldSeeSouthLocationList()
        {
            Boolean locationList = Pages.GlobalHome.southWillBurgLocation();
            Assert.IsTrue(locationList, "I should see Willumsburg locations");

            Boolean locationList1 = Pages.GlobalHome.southConcordLocation();
            Assert.IsTrue(locationList1, "I should see Concard locations");

            Boolean locationList2 = Pages.GlobalHome.southGrapevineLocation();
            Assert.IsTrue(locationList2, "I should see Grapevine locations");

            Boolean locationList3 = Pages.GlobalHome.southAtlantaLocation();
            Assert.IsTrue(locationList3, "I should see Atlanta locations");

        }

        [Then(@"I should see Mid West locations")]
        public void ThenIShouldSeeMidwestLocationList()
        {
            Boolean locationList = Pages.GlobalHome.midwestWisconsinDellLocation();
            Assert.IsTrue(locationList, "I should see Mid West Wisconsin locations");

            Boolean locationList1 = Pages.GlobalHome.midwestTravercityLocation();
            Assert.IsTrue(locationList1, "I should see Mid West Travercity locations");

            Boolean locationList2 = Pages.GlobalHome.midwestSanduskyLocation();
            Assert.IsTrue(locationList2, "I should see Mid West Sandusky locations");

            Boolean locationList3 = Pages.GlobalHome.midwestGurneeLocation();
            Assert.IsTrue(locationList3, "I should see Mid West Gurnee locations");

            Boolean locationList4 = Pages.GlobalHome.midwestKansasLocation();
            Assert.IsTrue(locationList4, "I should see Mid West Kansas locations");

            Boolean locationList5 = Pages.GlobalHome.midwestMasonLocation();
            Assert.IsTrue(locationList5, "I should see Mid West Mason locations");

            Boolean locationList6 = Pages.GlobalHome.midwestMiniSottaLocation();
            Assert.IsTrue(locationList6, "I should see Mid West Minisotta locations");
        }

        [Then(@"I should see North locations")]
        public void ThenIShouldSeeNorthLocationList()
        {
            Boolean locationList = Pages.GlobalHome.northestPoconosLocation();
            Assert.IsTrue(locationList, "I should see Poconos locations");

            Boolean locationList1 = Pages.GlobalHome.northestBostonLocation();
            Assert.IsTrue(locationList1, "I should see Boston locations");

        }

        [Then(@"I should see Canada locations")]
        public void ThenIShouldSeeCanadaLocationList()
        {
            Boolean locationList = Pages.GlobalHome.CandaNigaraLocation();
            Assert.IsTrue(locationList, "I should see Canada locations");

        }

        [When(@"I click on Location drop down link")]
        public void clickONLocationDrpDwnLink()
        {
            Pages.GlobalHome.clickOnLocationdrpDown();
        }
        
    }
}
