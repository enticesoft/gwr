﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using GWRFramework;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class MyProfilePageSteps
    {

        [Given(@"I Create an Account into application")]
        public void GivenICreateAnAccount(Table table)
        {
         //   Pages.PropertyHome.waitForDealsSignUpPopup();
            Pages.PropertyHome.closeDealsSignUp();
            if (Pages.PropertyHome.userSignedInMenu.Exists() == true)
            {
                Pages.PropertyHome.clickToSignOut();
            }
            Pages.PropertyHome.clickSignIn();
            Pages.SignIn.clickCreateAccountBtn();

            String firstName = table.Rows[0]["First Name"];
            Pages.CreateAccount.enterFirstNameTextbox(firstName);

            String lastName = table.Rows[0]["Last Name"];
            Pages.CreateAccount.enterLastNameTextbox(lastName);
           
            Pages.CreateAccount.enterPostalCodeTextbox();

            String emailId = Generator.EmailAddress.GenerateEmail(table.Rows[0]["Email"]);
            Pages.CreateAccount.enterEmailIdTextbox(emailId);

            String password = table.Rows[0]["Password"];
            Pages.CreateAccount.enterPasswordTextbox(password);
            Pages.MyProfile.ClickOnReceiveEmailPromotionscheckBox();
            Pages.CreateAccount.clickCreateButton();

        }

        [Given(@"I Sign In to Application")]
        public void GivenISignedInIntoAccount(Table table)
        {
          //  Pages.PropertyHome.waitForDealsSignUpPopup();
             Pages.PropertyHome.closeDealsSignUp();
            if (Pages.PropertyHome.userSignedInMenu.Exists() == true)
            {
                Pages.PropertyHome.clickToSignOut();
            }
            Pages.PropertyHome.clickSignIn();

            String email = table.Rows[0]["Email"];
            Pages.SignIn.sendEmail(email);

            String pass = table.Rows[0]["Password"];
            Pages.SignIn.sendPass(pass);

            Pages.SignIn.clickSignInbtn();
        }

        [When(@"I navigate from Home page to My Profile page")]
        public void WhenIClickedOnMyProfileLink()
        {
            Pages.PropertyHome.closeDealsSignUp();
            Pages.MyProfile.ClickedOnMyProfile();     
        }

        [When(@"I click on 'About Me' edit icon")]
        public void WhenIClickAboutMeIcon()
        {

            Pages.MyProfile.ClickOnAboutMeIcon();
        }

        [Then(@"I should able to see correct information populated under about me section")]
        public void ThenIShouldSeeCorrectInfoPopulatingOnMyProfilePage(Table table)
        {
            String firstName=table.Rows[0]["First Name"];
            String lastName = table.Rows[0]["Last Name"];
            Boolean Result=Pages.MyProfile.compareUserInfo(firstName, lastName);
            Assert.IsTrue(Result, "I should able to see correct information of mine populated  on My profile page which is used while creating account");

        }

        [When(@"I enter Billing Address")]
        public void WhenIEnterBilingAdress(Table table)
        {
            String billingAddress = table.Rows[0]["BillingAddress"];
            Pages.MyProfile.EnterBillingAddress(billingAddress);
        }

        [When(@"I enter city")]
        public void WhenIEnterCity(Table table)
        {
            String city = table.Rows[0]["City"];
            Pages.MyProfile.EnterCity(city);
        }

        [When(@"I select country")]
        public void WhenISelectCountry(Table table)
        {
            String country = table.Rows[0]["Country"];
            Pages.MyProfile.selectCountry(country);
        }

        [When(@"I select state")]
        public void WhenISelectState(Table table)
        {
            String state = table.Rows[0]["State"];
            Pages.MyProfile.selectState(state);
        }


        [When(@"I enter Postal Code")]
        public void WhenIEnterPostalCode()
        {
          
            Pages.MyProfile.EnterPostalCode();

        }

        [When(@"I enter Phone Number")]
        public void WhenIEnterPhoneNumber(Table table)
        {
            String PhoneNumber = table.Rows[0]["Phone"];
            Pages.MyProfile.EnterPhoneNumber(PhoneNumber);
        }




        [When(@"I click on 'Save' button")]
        public void WhenIClickOnSaveButton()
        
        {
            Pages.MyProfile.ClickOnSaveButton();
            
        }

        [When(@"I click Continue button")]
        public void WhenIClickOnContinueButton()
        {

            Pages.MyProfile.ClickContinueButton();
            
        }

        [Then(@"I should able to see edited About Me information")]
        public void ThenIshouldSeeEditedAboutMeInfo(Table table)
        {
            String billingAddress = table.Rows[0]["BillingAddress"];
            String city = table.Rows[0]["City"];
            String country = table.Rows[0]["Country"];
            String state = table.Rows[0]["State"];
            String phoneNumber = table.Rows[0]["Phone"];
            Boolean Result = Pages.MyProfile.EditedAboutMeInfoDisplayed(billingAddress, city, country, state, phoneNumber); 
           Assert.IsTrue(Result,"About Me Info get updated");
        }

        [Then(@"I should see About Me progress bar increase by 20%")]
        public void ThenIshouldSeeAboutMeProgressBarIncreaseBy20percent()
        {

            Boolean Result = Pages.MyProfile.AboutMeIncreaseBy20Per();
            Assert.IsTrue(Result, "About Me progress bar increase by 20%");
        }

        [When(@"I enter current password")]
        public void WhenIEnterCurrentPassword(Table table)
        {
            String currentPassword = table.Rows[0]["CurrentPassword"];
            Pages.MyProfile.EnterCurrentPassword(currentPassword);
        }

         [When(@"I enter New Password")]
        public void WhenIEnterNewPassword(Table table)
        {
            String newPassword = table.Rows[0]["NewPassword"];
            Pages.MyProfile.EnterNewPassword(newPassword);
        }

        [When(@"I enter Confirm New Password")]
        public void WhenIEnterConfirmNewPassword(Table table)
        {
            String confirmNewPassword = table.Rows[0]["ConfirmNewPassword"];
            Pages.MyProfile.EnterConfirmNewPassword(confirmNewPassword);
        }
        
        

        [Then(@"I should able to see 'New Password cannot match Confirm New Password' validation message")]
        public void ThenValidationMessageDispayed()
        {
            
            Boolean message=Pages.MyProfile.IsValidationMessageDisplayed();
            Assert.IsTrue(message, "Expected and Actual Validation message Match.");
        }

        [When(@"I click on Selected Email Preferences checkbox")]
        public void WhenIClickOnSelectedEmailPreferencesCheckbox()
        {
            Pages.MyProfile.ClickOnSelectedEmailPrefrencesCheckbox();
        }

        [Then(@"I should able to deselect Email Preferences checkbox")]
        public void ThenIAbleToDeselectEmailPreferencesCheckbox()
        {
            Boolean result=Pages.MyProfile.IsEmailPrefrencesSelected();
            Assert.IsFalse(result, "Checkbox is Deselected");
        }
        [When(@"I click on Deselected Email Preferences checkbox")]
        public void WhenIClickOnDeselectedEmailPreferencesCheckbox()
        {
            Pages.MyProfile.ClickOnDeselectedEmailPrefrencesCheckbox();
        }
        
        [Then(@"I should able to select Email Preferences checkbox")]
        public void ThenIAbleToselectEmailPreferencesCheckbox()
        {
            Boolean result = Pages.MyProfile.IsEmailPrefrencesSelected();
            Assert.IsTrue(result, "Checkbox is selected");
        }

        [Then(@"I should see My Preferences progress bar increased by 20%")]
        public void ThenIShouldSeePreferencesProgressBArIncreasedBy20per()
        {
            Boolean Result = Pages.MyProfile.MyPrefencesIncreaseBy20Per();
            Assert.IsTrue(Result, "My Preferences progress bar increase by 20%");
        }

        [When(@"I click on Deselected SMS Preferences checkbox")]
        public void WhenIClickOnDeselectedSMSPreferencesCheckbox()
        {
            Pages.MyProfile.ClickOnDeselectedSMSPrefrencesCheckbox();
        }

        [Then(@"I should able to select SMS Preferences checkbox")]
        public void ThenIAbleToselectSMSPreferencesCheckbox()
        {
            Boolean result = Pages.MyProfile.IsSMSPrefrencesSelected();
            Assert.IsTrue(result, "Checkbox is selected");
        }

        [When(@"I click on Selected SMS Preferences checkbox")]
        public void WhenIClickOnSelectedSMSPreferencesCheckbox()
        {
            Pages.MyProfile.ClickOnSelectedSMSPrefrencesCheckbox();
        }

        [Then(@"I should able to deselect SMS Preferences checkbox")]
        public void ThenIAbleToDeselectSMSPreferencesCheckbox()
        {
            Boolean result = Pages.MyProfile.IsSMSPrefrencesSelected();
            Assert.IsFalse(result, "Checkbox is Deselected");
        }
        [When(@"I click on Add Family Member link")]
        public void WhenIClickOnAddFamilyMemberLink()
        {
            Pages.MyProfile.ClickOnAddFamilyMemberLink();
        }

        [When(@"I enter First Name in family")]
        public void WhenIEnterFirstName(Table table)
        {
            String firstName = table.Rows[0]["FirstName"];
            Pages.MyProfile.AddFirstName(firstName);
        }

        [When(@"I enter Last Name in family")]
        public void WhenIEnterLastName(Table table)
        {
            String lastName = table.Rows[0]["LastName"];
            Pages.MyProfile.AddLastName(lastName);
        }

        [When(@"I select Birthday")]
        public void WhenIEnterBirthDay(Table table)
        {
            String birthDay = table.Rows[0]["Birthday"];
            Pages.MyProfile.AddBirthday(birthDay);
        }

        [Then(@"I should able to add family member")]
        public void ThenIshouldSeeAddedFamilyMember(Table table)
        {
            String verifyAddedFamilyName = table.Rows[0]["AddedFamilyName"];
           
             
            Boolean Result = Pages.MyProfile.FamilyMemberDisplyed(verifyAddedFamilyName);
            Assert.IsTrue(Result, "I should able to add family member");
        }

        [Then(@"I should able to see progress bar increased by 30 % of Profile completeness")]
        public void ThenIShouldSeeMyFamilyProgressBarIncreasedBy30per()
        {
            Boolean Result = Pages.MyProfile.MyFamilyIncreaseBy30Per();
            Assert.IsTrue(Result, "My Family progress bar increase by 30% when I add family member");
        }

        [When(@"I click on My Family edit icon")]
        public void WhenIclickOnMyFamilyEditIcon()
        {
           Pages.MyProfile.ClickOnMyFamilyEditIcon();
        }
        [When(@"I edit First Name")]
        public void WhenIEditFirstName(Table table)
        {
            String firstName = table.Rows[0]["FirstName"];
            Pages.MyProfile.EditFirstName(firstName);
        }

        [Then(@"I should able to see edited family member information")]
        public void ThenIShouldSeeEditedFamilyMemberInformation(Table table)
        {
            String editedFamilyMember= table.Rows[0]["FirstName"]+" " +table.Rows[0]["LastName"];
            Boolean Result = Pages.MyProfile.EditedFamilyMemberdisplayed(editedFamilyMember);
            Assert.IsTrue(Result, "I should able to see edited family member information correctly");
        }

        [When(@"I click on My Family delete icon")]
        public void WhenIClickOnMyFamilyDeleteIcon()
        {
           Pages.MyProfile.ClickOnMyFamilyDeleteIcon();
        }
        [Then(@"I should able to delete family member")]
        public void ThenIShouldableToDeleteFamilyMember()
        {
            Boolean Result = Pages.MyProfile.FamilyMemberDeleted();
            Assert.IsFalse(Result, "I should able to delete family member");
        }

        [Then(@"Profile completeness progress bar for Family Member should be decreased by 30%")]
        public void ThenFamilyMemberProgressBarDecreasedBy30per()
        {
            Boolean Result = Pages.MyProfile.MyFamilyDecreaseBy30Per();
            Assert.IsTrue(Result, "Profile completeness progress bar for Family Member should be decreased by 30%");
        }

        [When(@"I click SignOut Link")]
        public void WhenIClickSignOutLink()
        {
            Pages.PropertyHome.clickToSignOut();
        }

        [When(@"I enter Email address")]
        public void WhenIEnterEmailAddress(Table table)
        {
            String email = table.Rows[0]["Email"];
            Pages.SignIn.sendEmail(email);
        }

        [When(@"I enter changed password")]
        public void WhenIEnterChangedPassword(Table table)
        {
            String password = table.Rows[0]["ChangedPassword"];
            Pages.SignIn.sendPass(password);
        }

        [Then(@"I should able to login into application with changed password")]
        public void WhenILoginWithChangedPassword()
        {
            Boolean signInMenu = Pages.PropertyHome.checkUserSingedInMenu();
            Assert.IsTrue(signInMenu, "User able to see Menu link at the top.");
        }

        /*[When(@"I click on Google button")]
         public void WhenIClickOnGoogleButton()
         {
             Pages.MyProfile.ClickOnGoogleButton();
         }

         [When(@"I enter Email")]
         public void WhenIEnterEmail(Table table)
         {
             String gmail = table.Rows[0]["Email"];
             Pages.MyProfile.EnterEmail(gmail);
         }

         [When(@"I click on Next button")]
         public void WhenIClickOnNextButton()
         {

             Pages.MyProfile.ClickOnNextButton();
         }*/

        [When(@"I observe progress Bar")]
        public void WhenIObservePrgBar()
        {
            Pages.MyProfile.getProgressBarPercentage();
        }

        [Then(@"I Reset Email Preferences checkbox")]
        public void WhenIResetEmailPreferencesCheckbox()
        {
            Pages.MyProfile.ClickOnEmailPrefrencesCheckbox();
            Pages.MyProfile.ClickOnSaveButton();         
        }

        [Then(@"I Reset SMS Preferences checkbox")]
        public void WhenIResetSMSPreferencesCheckbox()
        {
            Pages.MyProfile.ClickOnSMSPrefrencesCheckbox();
            Pages.MyProfile.ClickOnSaveButton();
        }

        [Then(@"I Reset Added family")]
        public void WhenIResetAddedFamily()
        {
            Pages.MyProfile.ClickOnMyFamilyDeleteIcon();
            Pages.MyProfile.ClickOnSaveButton();
        }

        [Then(@"I reset password to original")]
        public void WhenIResetPwdToOriginal(Table table)
        {
            Pages.MyProfile.ClickedOnMyProfile();
            String currentPassword = table.Rows[0]["CurrentPassword"];
            Pages.MyProfile.EnterCurrentPassword(currentPassword);
            String newPassword = table.Rows[0]["NewPassword"];
            Pages.MyProfile.EnterNewPassword(newPassword);
            String confirmNewPassword = table.Rows[0]["ConfirmNewPassword"];
            Pages.MyProfile.EnterConfirmNewPassword(confirmNewPassword);
            Pages.MyProfile.ClickOnSaveButton();
            Pages.MyProfile.verifyResetPasswordMsg();
        }


        [When(@"I checked 'Received Email Promotions' check box")]
        public void WhenICheckedReceivedEmailPromotionsCheckBox()
        {
            Pages.MyProfile.IsReceivedEmailPromotionsCheckBoxChecked();

        }

        [Then(@"I should able to see Email Preferences checkbox as selected")]
        public void ThenISeeEmailPreferencesCheckboxSelected()
        {
            Boolean Result = Pages.MyProfile.IsEmailPreferencesCheckboxSelected();
            Assert.IsTrue(Result, "I should able to see Email Preferences checkbox as selected");
        }

        [When(@"I unchecked 'Received Email Promotions' check box")]
        public void WhenIUncheckedReceivedEmailPromotionsCheckBox()
        {
            Pages.MyProfile.IsReceivedEmailPromotionsCheckBoxUnchecked();

        }

        [Then(@"I should able to see Email Preferences checkbox as not selected")]
        public void ThenISeeEmailPreferencesCheckboxNotSelected()
        {
            Boolean Result = Pages.MyProfile.IsEmailPreferencesCheckboxNotSelected();
            Assert.IsFalse(Result, "I should able to see Email Preferences checkbox as not selected");
        }



    }
}
