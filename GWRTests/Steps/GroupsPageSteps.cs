﻿using GWRFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class GroupsPageSteps
    {
        [When(@"I click on Groups header option")]
        public void WhenIClickOnGroupHeader()
        {
            Pages.GroupPage.clickOnGroupHeader();
        }

        [When(@"I click on Groups About sub menu")]
        public void WhenIClickAboutSubMenu()
        {
            Pages.GroupPage.clickOnGroupAboutSubMenu();
        }

        [When(@"I click on Groups Meeting space sub menu")]
        public void WhenIClickMeetingSpaceSubMenu()
        {
            Pages.GroupPage.clickOnGroupMeetingSubMenu();
        }
        
        [When(@"I click on Groups Suite sub menu")]
        public void WhenIClickGroupSubMenu()
        {
            Pages.GroupPage.clickOnSuiteSubMenu();
        }
        
        [When(@"I click on Groups Catering sub menu")]
        public void WhenIClickGroupCateringSubMenu()
        {
            Pages.GroupPage.clickOnCateringSubMenu();
        }
        
        [When(@"I click on Special event sub menu")]
        public void WhenIClickGroupSpecialEventSubMenu()
        {
            Pages.GroupPage.clickOnSpecialEventSubMenu();
        }

        [When(@"I click on Activities sub menu")]
        public void WhenIClickGroupActivitiesSubMenu()
        {
            Pages.GroupPage.clickOnAcivitySubMenu();
        }

        [When(@"I click on Request proposal button of Group Sub menu")]
        public void WhenIClickGroupRequestProposalBtn()
        {
            Pages.GroupPage.clickOnGroupRequestPrpslBtn();
        }
        

        [When(@"I click on Request proposal button")]
        public void WhenIClickRequestProposalBtn()
        {
            Pages.GroupPage.clickOnRequestProposalButton();
        }
        

        [Then(@"I should see About tab is selected")]
        public void ThenIShouldSeeAboutTabIsSelected()
        {
            Boolean Result = Pages.GroupPage.AbotTabSelected();
            Assert.IsTrue(Result, "I should see About tab is selected");
        }

        [Then(@"I should see Come together for your next event section on About Page")]
        public void ThenIShouldSeeComeTogetherSection()
        {
            Boolean Result = Pages.GroupPage.AboutComeTogetherSectionOnPage();
            Assert.IsTrue(Result, "I should see Come together for your next event section on About Page");
        }
        
        [Then(@"I should see Full Access to Lodge Amenities section on About Page")]
        public void ThenIShouldSeeAccessLodgeFullAmenitiesSection()
        {
            Boolean Result = Pages.GroupPage.AboutFullAccessLodgeAmenitiesSectionOnPage();
            Assert.IsTrue(Result, "I should see Full Access to Lodge Amenities section on About Page");
        }

        [Then(@"I should see Full Access to Lodge Amenities Gallery section on About Page")]
        public void ThenIShouldSeeAccessLodgeFullAmenitiesGallerySection()
        {
            Boolean Result = Pages.GroupPage.AboutFullAccessLodgeAmenitiesGallerySectionOnPage();
            Assert.IsTrue(Result, "I should see Full Access to Lodge Amenities Gallery section on About Page");
        }

        [Then(@"I should see Escape to Great Wolf Lodge on About Page")]
        public void ThenIShouldSeeEscapetoGreatWolfLodgeSection()
        {
            Boolean Result = Pages.GroupPage.AboutEscapetoGreatwolgLodgeSectionOnPage();
            Assert.IsTrue(Result, "I should see Escape to Great Wolf Lodge on About Page");
        }

        [Then(@"I should see Escape to Great Wolf Lodge grid section on About Page")]
        public void ThenIShouldSeeEscapetoGreatWolfLodgeGridSection()
        {
            Boolean Result = Pages.GroupPage.AboutEscapetoGreatwolgLodgeGridContainerOnPage();
            Assert.IsTrue(Result, "I should see Escape to Great Wolf Lodge grid section on About Page");
        }

        [Then(@"I should see Client review section on About Page")]
        public void ThenIShouldSeeClientReviewSection()
        {
            Boolean Result = Pages.GroupPage.AboutClientReviewOnPage();
            Assert.IsTrue(Result, "I should see Client review section on About Page");
        }
        
        [Then(@"I should see Mini map section on About Page")]
        public void ThenIShouldSeeMiniMapReviewSection()
        {
            Boolean Result = Pages.GroupPage.AboutMiniMapOnPage();
            Assert.IsTrue(Result, "I should see Mini map section on About Page");
        }

        [Then(@"I should see Event tile form")]
        public void ThenIShouldSeeEventFromTitle()
        {
            Boolean Result = Pages.GroupPage.EventTitleForm();
            Assert.IsTrue(Result, "I should see Event tile form");
        }

        [Then(@"I should see  'Imagine the possibilities for your event' section on Meeting space page")]
        public void ThenIShouldSeeImaginethePosibilitySec()
        {
            Boolean Result = Pages.GroupPage.MeetingSpaceImagineToPosibilitySec();
            Assert.IsTrue(Result, "I should see  'Imagine the possibilities for your event' section on Meeting space page");
        }

        [When(@"I click on View Floor Plan button")]
        public void WhenIClickViewFloorPlanBtn()
        {
            Pages.GroupPage.clickOnViewFloorPlanButton();
        }

        [Then(@"I should see floor plan on Meeting space page")]
        public void ThenIShouldSeeFloorPlanPage()
        {
            Boolean Result = Pages.GroupPage.verifyUserNavgtOnFloorPlanPage();
            Assert.IsTrue(Result, "I should see floor plan on Meeting space page");
        }

        [Then(@"I should see Meeting space section on Meeting space page")]
        public void ThenIShouldSeeMeetingPageSec()
        {
            Boolean Result = Pages.GroupPage.MeetingSpaceTitleSec();
            Assert.IsTrue(Result, "I should see Meeting space section on Meeting space page");
        }
        
        [Then(@"I should see Frequently Asked Questions section on Meeting space page")]
        public void ThenIShouldSeeFreqAskQuetionPageSec()
        {
            Boolean Result = Pages.GroupPage.FreAskQuestSecTitleSec();
            Assert.IsTrue(Result, "I should see Frequently Asked Questions section on Meeting space page");
        }
        
        [Then(@"I should see Room card on Groups Suite page")]
        public void ThenIShouldSeeRoomCardOnSuitePage()
        {
            Boolean Result = Pages.GroupPage.RoomCardSecOnSuite();
            Assert.IsTrue(Result, "I should see Room card on Groups Suite page");
        }

        [When(@"I click on Explore our suite button")]
        public void WhenIClickOnExploreSuiteBtn()
        {
            Pages.GroupPage.clickOnexploreOurSuiteBtn();
        }

        [Then(@"I should see navigate on Suite page")]
        public void ThenIShouldSeeSuiteTitle()
        {
            Boolean Result = Pages.GroupPage.SuiteTitleOnSuiteNavigation();
            Assert.IsTrue(Result, "I should see navigate on Suite page");
        }
        
        [Then(@"I should see Reservation Made Easy section on Suite page")]
        public void ThenIShouldSeeResMadeEasySuiteTitle()
        {
            Boolean Result = Pages.GroupPage.reserMadeEasySectionOnSuitePage();
            Assert.IsTrue(Result, "I should see Reservation Made Easy section on Suite page");
        }
        
        [Then(@"I should see Payment Method section on Suite page")]
        public void ThenIShouldSeePaymentmethodSuiteTitle()
        {
            Boolean Result = Pages.GroupPage.PaymentMethodSectionOnSuitePage();
            Assert.IsTrue(Result, "I should see Payment Method section on Suite page");
        }
        
        [Then(@"I should see 'Banquets and Catering' section on Suite page")]
        public void ThenIShouldSeeBanqAndCatering()
        {
            Boolean Result = Pages.GroupPage.BanquetsandCateringSecOnCateringPage();
            Assert.IsTrue(Result, "I should see 'Banquets and Catering' section on Suite page");
        }

        [Then(@"I should see 'Lodge Dining Options' section on Suite page")]
        public void ThenIShouldSeeDinginLodgeOption()
        {
            Boolean Result = Pages.GroupPage.DingngLodgeOptionSecOnCateringPage();
            Assert.IsTrue(Result, "I should see 'Lodge Dining Options' section on Suite page");
        }

        [When(@"I click on  'ALL LODGE DINING OPTION' button on Catering Page.")]
        public void WhenIClickOnAlldinignOptionBtn()
        {
            Pages.GroupPage.clickOnALLLODGEDININGOPTIONSButton();
        }
        
        [Then(@"I should see user navigate on Dinign page")]
        public void ThenIShouldSeeDinginPagetitle()
        {
            Boolean Result = Pages.GroupPage.DingngPagetitle();
            Assert.IsTrue(Result, "I should see user navigate on Dinign page");
        }
        
        [Then(@"I should see We here to Host section on Special event page")]
        public void ThenIShouldWeareHereSection()
        {
            Boolean Result = Pages.GroupPage.weHereToHostOnSpecialEventPageSec();
            Assert.IsTrue(Result, "I should see We here to Host section on Special event page");
        }
        
        [When(@"I click on  Explore birthday package button special event Page.")]
        public void WhenIClickOnExploreBirthdayPckgBtn()
        {
            Pages.GroupPage.clickOnexplorePackageBtn();
        }

        [Then(@"I should navigate on birthday page")]
        public void ThenIShouldBirthayPage()
        {
            Boolean Result = Pages.GroupPage.BirthdaypackaegPageTtitle();
            Assert.IsTrue(Result, "I should navigate on birthday page");
        }
        
        [Then(@"I should see Book an event in our Water Park section on Activity Page")]
        public void ThenIShouldBookAnEventWaterParkSecPage()
        {
            Boolean Result = Pages.GroupPage.BookAnEventOnActivityPageSection();
            Assert.IsTrue(Result, "I should navigate on birthday page");
        }
        
        [Then(@"I should see Team building and group activities section on Activity Page")]
        public void ThenIShouldTeamBuildingSecPage()
        {
            Boolean Result = Pages.GroupPage.TeamBuildingOnActivityPageSection();
            Assert.IsTrue(Result, "I should see Team building and group activities section on Activity Page");
        }

        [Then(@"I should see Thanks You Page after submit information")]
        public void ThenIShouldSeeThanksPage()
        {
            Boolean Result = Pages.GroupPage.ThanksPage();
            Assert.IsTrue(Result, "I should see Thanks You Page after submit information");
        }
        

        [When(@"I fill Tell Us About Your Event Section info")]
        public void ienterTellAboutEvent(Table table)
        {                        
            String evnType = table.Rows[0]["Event Type"];
            String evnDateType = table.Rows[0]["Event Date Type"];
            String evnStrtDay = table.Rows[0]["Evnt Start Date"];
            String evnEndDay = table.Rows[0]["Evnt End Date"];
            String EvntSpDate = table.Rows[0]["Event specific Date"];
            Pages.GroupPage.TellUsAboutYourEventSection(evnType, evnDateType, evnStrtDay, evnEndDay, EvntSpDate);
        }

        [When(@"I fill Tell Us About Your self Section info")]
        public void ienterTellAboutSelf()
        {
            Pages.GroupPage.TellUsAboutYourSelfSec();
        }
    }
}
