﻿using GWRFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class HeaderSuiteSteps
    {
        [When(@"I click on Suites menu under Header")]
        public void WhenIclickOnSuitesMenuUnderHeader()
        {
            Pages.PropertyHome.closeDealsSignUp();
            Pages.HeaderSuite.ClickOnSuitesMenu();
        }

        [When(@"I click on Premium Suites type")]
        public void WhenIclickOnPremiumSuitesType()
        {
            Pages.HeaderSuite.ClickOnPremiumSuites();
        }

        [When(@"I click on Check Availability Button under any premium suite")]
        public void WhenIclickOnCheckAvailabilityButton()
        {
            Pages.HeaderSuite.ClickOnCheckAvailabilityButton();
        }

        [Then(@"I should navigate on plan page")]
        

       [When(@"I click on Standard Suites type option")]
        public void WhenIclickOnStandardSuitesType()
        {
            Pages.HeaderSuite.ClickOnStadardSuites();
        }

        [When(@"I click on Check Availability Button under any Standard suite")]
        public void WhenIclickOnCheckAvailabilityButtonOfStandardSuite()
        {
            Pages.HeaderSuite.ClickOnCheckAvailabilityButtonOfStandardSuite();
        }

        [When(@"I click on Themed Suites type option")]
        public void WhenIclickOnThemedSuitesType()
        {
            Pages.HeaderSuite.ClickOnThemedSuites();
        }

        [When(@"I click on Check Availability Button under any Themed suite")]
        public void WhenIclickOnCheckAvailabilityButtonOfThemedSuite()
        {
            Pages.HeaderSuite.ClickOnCheckAvailabilityButtonOfThemedSuite();
        }

        [Then(@"I should able to see Only Premium type Suites on Premium Suites page")]
        public void ThenIShouldSeePremiumRelatedSuites()
        {
            Boolean result = Pages.HeaderSuite.IsPremiumRelatedSuiteDisplayed();
            Assert.IsTrue(result, "I should able to see Only Premium related Suites on Premium Suites page");

        }

        [Then(@"I should able to see Only Themed type Suites on Themed Suites page")]
        public void ThenIShouldSeeThemedRelatedSuites()
        {
            Boolean result = Pages.HeaderSuite.IsThemedRelatedSuiteDisplayed();
            Assert.IsTrue(result, "I should able to see Only Themed type Suites on Themed Suites page ");

        }

        [Then(@"I should able to see Only standard type Suites on Standard Suites page")]
        public void ThenIShouldSeeStandardRelatedSuites()
        {
            Boolean result = Pages.HeaderSuite.IsStandardRelatedSuiteDisplayed();
            Assert.IsTrue(result, "I should able to see Only standard type Suites on Standard Suites page");
        }


        [Then(@"I should see suite title")]
        public void ThenIShouldSeeSUiteTitle()
        {
            Boolean suiteTitle = Pages.HeaderSuite.VerifyT3TitleOFSuites();
            Assert.IsTrue(suiteTitle, "I should see suite title");
        }

        [Then(@"I should see suite floor plan")]
        public void ThenIShouldSeeSUiteFloorPlan()
        {
            Boolean suiteFloorPlan = Pages.HeaderSuite.VerifyT3Floorplan();
            Assert.IsTrue(suiteFloorPlan, "I should see suite floor plan");
        }

        [Then(@"I should see suite Check Availability button")]
        public void ThenIShouldSeeSUiteAvailabiltyBtn()
        {
            Boolean suiteAvbltyBtn = Pages.HeaderSuite.VerifyT3CheckAvaBtn();
            Assert.IsTrue(suiteAvbltyBtn, "I should see suite Check Availability button");
        }

        [When(@"I click on suite check availability button")]
        public void clickOnSUiteAvailabiltyBtn()
        {
            Pages.HeaderSuite.clickOnT3CheckAvaBtn();
        }

        [Then(@"I should see suite sleep section")]
        public void ThenIShouldSeeSUiteSleepSec()
        {
            Boolean suiteSleepSec = Pages.HeaderSuite.VerifyT3SleepSec();
            Assert.IsTrue(suiteSleepSec, "I should see suite sleep section");
        }
        

        [Then(@"I should see suite description section")]
        public void ThenIShouldSeeSUiteDescriptionSec()
        {
            Boolean suiteDescSec = Pages.HeaderSuite.VerifyT3SuiteDescription();
            Assert.IsTrue(suiteDescSec, "I should see suite description section");
        }



    }
}
