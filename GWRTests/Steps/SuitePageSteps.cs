﻿using TechTalk.SpecFlow;
using GWRFramework;
using System;
using NUnit.Framework;
using System.Threading;
using GWRFramework.TestData;

namespace GWRTests.Steps
{
    [Binding]
    public class SuitePageSteps
    {
        public static Double SuitePrice;
        public static decimal SuitePrice1;
        public static String SuiteName;
        public static String SuiteBaseCount;
        public static int AdulttoAdd;

        [When(@"I navigate from Home page to Suite page")]
        public void homeToSuitePage(Table table)
        {
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            String kids1age = table.Rows[0]["Kids1Age"];
            Pages.Suite.SelectKids(kids, kids1age);
            
            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

            Pages.Suite.clickBookNowBtn();

            Browser.scrollVerticalBy("450");
            Browser.waitForElementToBeClickable(Pages.Suite.chkRateCalLink, 40);
        }

        [When(@"I navigate from Home page to Suite page with 2 nights stay")]
        public void homeToSuitePageTwoNightsStay(Table table)
        {
            Pages.Suite.selectCheckInforDining();
            Pages.Suite.selectCheckOutDining();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);

            if (kids != 0)
            {
                String kids1age = table.Rows[0]["Kids1Age"];
                Pages.Suite.SelectKids(kids, kids1age);
               
            }

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

            Pages.Suite.clickBookNowBtn();
            Browser.waitForElementToBeClickable(Pages.Suite.chkRateCalLink, 90);
        }

        [When(@"I navigate from Suite page to Payment page with Packages choice")]
        public void suitePageToPaymentPageWithOptionOfPackages(Table table)
        {
            String LCO = table.Rows[0]["LCO Package"];
            String AddPass = table.Rows[0]["Attraction Package"];
            String AddDining = table.Rows[0]["Dining Package"];
            Pages.Suite.navigateSuiteToPaymentWithchoices(LCO, AddPass, AddDining);
        }

        
        [When(@"I navigate from Suite page to Dining page to verify Cost and Title")]
        public void suitePageToDiningPage()
        {
            Pages.Suite.navigateSuiteToDinignPageAndVerifyPckgCostandTitle();
        }

        [When(@"I navigate from Suite page to Activity Birthday tab to verify Cost and Title")]
        public void suitePageToActivityBirthdayPage()
        {
            Pages.Suite.navigateSuiteToActivityBirthdayAndVerifyPckgCostandTitle();
        }
        

        [When(@"I navigate from Suite page to Payment page")]
        public void suitePageToPaymentPage()
        {
            Pages.Suite.navigateSuiteToPayment();
        }

        [When(@"I navigate from Suite page to Payment page with suite option")]
        public void suitePageToPaymentPageSuiteOption()
        {
            Pages.Suite.navigateSuiteToPaymentWithSuiteOption();
        }

        [When(@"I navigate from Suite page to Payment page with addign packages")]
        public void suitePageToPaymentPageWithPckgs()
        {
            Pages.Suite.navigateSuiteToPaymentWithPcksgs();
        }

        [When(@"I navigate from Suite page to Payment page with addign LCO and Other packages")]
        public void suitePageToPaymentPageWithLCOPckgs()
        {
            Pages.Suite.navigateSuiteToPaymentWithLCOAndOtherPcksgs();
        }

        [Then(@"I Should see Include with Your Stay section")]
        public void thenIShouldSeeIncludeWithStaySection()
        {
            Boolean includeWithYourStaySectn = Pages.Suite.checkincludeWithYourStaySection();
            Assert.IsTrue(includeWithYourStaySectn, "User able to see Include with your stay section.");

            Boolean includeWithYourStaySectn1 = Pages.Suite.checkinStayWaterParkPassesSection();
            Assert.IsTrue(includeWithYourStaySectn1, "User able to see Stay water park passes section.");

            Boolean includeWithYourStaySectn2 = Pages.Suite.checkinAccessToWaterParkSection();
            Assert.IsTrue(includeWithYourStaySectn2, "User able to see Access to water park section.");

            Boolean includeWithYourStaySectn3 = Pages.Suite.checkinKidsActivitiesSection();
            Assert.IsTrue(includeWithYourStaySectn3, "User able to see Access to kids activities section.");

            Boolean includeWithYourStaySectn4 = Pages.Suite.checkinUnlimitedWifiSection();
            Assert.IsTrue(includeWithYourStaySectn4, "User able to see Access to unlimited wifi section.");

        }

        [When(@"I click on 'Select Room' button")]
        public void whenIclickSelectRoomBtn()
        {
            Pages.Suite.clickSelectRoomBtn();
        }

        [When(@"I click on 'Select Room' button of suite option")]
        public void whenIclickSelectRoomBtnSuiteOption()
        {
            Pages.Suite.clickSelectRoomBtnSuiteOption();
        }

        [Then(@"I should see LCO pop appear")]
        public void whenIShouldSeeLCOPopUp()
        {
            Pages.Suite.popUpHandle();
        }

        [Then(@"I should see LCO title")]
        public void thenIShouldSeeLCOTitle()
        {
            Boolean lcoTitle = Pages.Suite.verifyLCOTitle();
            Assert.IsTrue(lcoTitle, "User able to see LCO title");
        }

        [Then(@"I should see LCO Price")]
        public void thenIShouldSeeLCOPrice()
        {
            Boolean lcoTitle = Pages.Suite.verifyLCOPopupPriceBtn();
            Assert.IsTrue(lcoTitle, "User able to see LCO Price section");

        }
        
        [When(@"I observe LCO Price")]
        public void whenIosbervLCOPrice()
        {
            Pages.Suite.observeLcoPrice();

        }

        [Then(@"I should see LCO Description")]
        public void thenIShouldSeeLCODescrptn()
        {
            Boolean lcoPopupDescription = Pages.Suite.verifyLCOPopupDescrptn();
            Assert.IsTrue(lcoPopupDescription, "User able to see popup description");
        }

        [Then(@"I should see LCO 'Yes, Add Late Checkout' and 'No Thanks' buttons.")]
        public void thenIShouldSeeAddAndNoThnksBtn()
        {
            Boolean lcoPopupAvlblBtn = Pages.Suite.verifyLCOPopupBtn();
            Boolean lcoPopupAvlblBtn1 = Pages.Suite.verifyLCOPopupNoThanksBtn();
            Assert.IsTrue(lcoPopupAvlblBtn && lcoPopupAvlblBtn1, "User able to see Add late checkout and Not Thanks button after click on Select Room button");

        }

        [When(@"I click on No Thanks button on LCO pop up")]
        public void clickILCONoThnksBtn()
        {
            Pages.Suite.clickONLCOPopupNoThanksBtn();            
        }

        [When(@"I click on Add Late Check Out button on LCO pop up")]
        public void clickILCOAddChotBtn()
        {
            Pages.Suite.clickONLCOPopupAddChecOtBtn();
        }

        [Then(@"I should see Tab Title as Standard")]
        public void thenIShouldStandardTab()
        {
            Boolean standardTabTitle = Pages.Suite.verifyStandardTab();
            Assert.IsTrue(standardTabTitle, "User able to see Standard tab as title.");
        }

        [Then(@"I should see Suite Title")]
        public void thenIShouldSeeSuiteTitle()
        {
            Boolean suiteTitle = Pages.Suite.verifySuiteTitle();
            Assert.IsTrue(suiteTitle, "User able to see Suite title.");
        }

        [Then(@"I should see Base and Max occupancy section")]
        public void thenIShouldSeeBaseMaxSectn()
        {
            Boolean baseAndMaxOccupationSection = Pages.Suite.verifyBaseAndMaxOccptnSectn();
            Assert.IsTrue(baseAndMaxOccupationSection, "User able to see Base and Max occupancy section.");
        }

        /*[Then(@"I should see Suite description")]
        public void thenIShouldSeeSuiteDescrptn()
        {

            Boolean suiteDetails = Pages.Suite.verifyViewDetailsLink();
            Assert.IsTrue(suiteDetails, "User able to see suite details.");
        }*/

        [Then(@"I should see Check rate calendar link")]
        public void thenIShouldSeeRateCalLink()
        {
            Boolean rateCalLink = Pages.Suite.verifyRateCalLink();
            Assert.IsTrue(rateCalLink, "User able to see rate calendar link.");

        }

        [Then(@"I should see Suite price section.")]
        public void thenIShouldSeePriceSectn()
        {
            Boolean rateCalLink = Pages.Suite.verifyPriceSection();
            Assert.IsTrue(rateCalLink, "User able to see Price Section.");

        }

        [Then(@"I should see Tab Title as Premium")]
        public void thenIShouldPremiumTab()
        {
            Boolean premiumTab = Pages.Suite.verifyPremiumTab();
            Assert.IsTrue(premiumTab, "User able to Premium tab");

        }

        [When(@"I Click on Standard Tab")]
        public void whenIClickOnStandardTab()
        {
            Pages.Suite.clickOnStandardTab();

        }

        [When(@"I Click on Premium Tab")]
        public void whenIClickOnPremiumTab()
        {
            Pages.Suite.clickOnPremiumTab();

        }

 //Ishwar stepsts defination
 
        [Then(@"I should see Themed Tab is selected By default")]
        public void ThenIshouldseeThemedTabisselectedBydefault()
        {
            Boolean ThemedTab = Pages.Suite.themedtabDisplayedDefault();
            Assert.IsTrue(ThemedTab, "Themed tab is selected by default.");
        }

        [Then(@"I should see Themed tab title")]
        public void ThenIShouldSeeThemedTabTitle()
        {
            Boolean ThemedTab = Pages.Suite.themedtabDisplayedDefault();
            Assert.IsTrue(ThemedTab, "Themed tab title is display");
        }

        [Then(@"I should see Suite title")]
        public void ThenIShouldSeeSuiteTitle()
        {
            Boolean SuiteTitle = Pages.Suite.suiteTitleDisplay();
            Assert.IsTrue(SuiteTitle, "Suite title is display");
        }

       /* [Then(@"I should see Base and Max occupancy section")]
        public void ThenIShouldSeeBaseAndMaxOccupancySection()
        {
            Pages.Suite.baseMaxsection();
        }*/

        [Then(@"I should see Suite description")]
        public void ThenIShouldSeeSuiteDescription()
        {
            Boolean SuiteDescription = Pages.Suite.suiteDescriptiondiscplay();
            Assert.IsTrue(SuiteDescription, "Suite Description is display");
        }

        [Then(@"I should Check rate calendar link")]
        public void ThenIShouldCheckRateCalendarLink()
        {
            Boolean CheckRateCalendar = Pages.Suite.checkRateCalendarlink();
            Assert.IsTrue(CheckRateCalendar, "Check rate calendar link is display");
        }

        [Then(@"I should see Suite price section")]
        public void ThenIShouldSeeSuitePriceSection()
        {
            Boolean SuitePriceSection = Pages.Suite.suitePricesSectionDisplay();
            Assert.IsTrue(SuitePriceSection, "Suite Price section is display");
        }

        [Then(@"I should see Applied offer code into suite info")]
        public void ThenIShouldSeeAppliedOfferCodeIntoSuiteInfo()
        {
            Boolean AppliedOfferCode = Pages.Suite.appliedOfferCodeDisplay();
            Assert.IsTrue(AppliedOfferCode, "Applied offer code is display correctly in Suite info");

        }

        [Then(@"I should see Best Available Rate into suite info")]
        public void ThenIShouldSeeBestAvailableRateIntoSuiteInfo()
        {
            Boolean BestAvailableRate = Pages.Suite.bestAvailableRateDisplayed();
            Assert.IsTrue(BestAvailableRate, "Best Available Rate is displayed in Suite info");
        }

        [When(@"I select Accessible Rooms Only toggle")]
        public void ThenISelectAccessibleRoomsOnlyToggle()
        {
            Pages.Suite.selectAccessibleroomsonly();
        }

        [When(@"I click on Update your Stay")]
        public void WhenIClickOnUpdateYourStay()
        {
            Pages.Suite.updateYourStay();
        }


        [Then(@"I should see Accessible label for suites")]
        public void ThenIShouldSeeAccessibleLabelForSuites()
        {
            Boolean AccessibleLabelforSuites = Pages.Suite.accessibleLabelTagDisplay();
            Assert.IsTrue(AccessibleLabelforSuites, "Accessible label tab for suite is display");
        }

        [When(@"I observred Max occupancy for that suite")]
        public void WhenIObservredMaxOccupancyForThatSuite()
        {
            Pages.Suite.maxOccupancy();
        }

        [When(@"I select Adult greater than Max Occupancy")]
        public void WhenISelectAdultGreaterThanMaxOccupancy()
        {
            String guestStrVal = DataAccess.getData(17, 2);
            int guestValbfSub = Convert.ToInt16(guestStrVal);
            int guestVal = guestValbfSub - 1;
            Pages.Suite.SelectAdults(guestVal);
        }

        [Then(@"I should see Exceeded Maximum Occupancy alert")]
        public void ThenIShouldSeeExceededMaximumOccupancyAlert()
        {
            Boolean ExceedMaxOccupanyalert = Pages.Suite.exceededMaxOccupancyalert();
            Assert.IsTrue(ExceedMaxOccupanyalert, "Exceeded Max Occupancy alert display after selecting guest more than Max occupancy");
        }

        [Then(@"I should not see Suite Price section")]
        public void ThenIShouldNotSeeSuitePriceSection()
        {
            Boolean SuitePriceSection = Pages.Suite.suitePriceSection();
            Assert.IsFalse(SuitePriceSection, "Suite Price section is not display after selecting guest more than Max occupancy");
        }

        [When(@"I observe Suite price")]
        public void WhenIObserveSuitePrice()
        {
            Browser.waitForElementToBeClickable(Pages.Suite.suitePrice, 20);
            SuitePrice = Browser.stringToDoubleConvert(Pages.Suite.suitePrice);
            SuitePrice1 = Browser.stringToDecimalConvert(Pages.Suite.suitePrice);
        }

        [When(@"I observe Suite price to price verification")]
        public void WhenIObserveSuitePriceVerification()
        {
            Browser.waitForElementToBeClickable(Pages.Suite.suitePrice, 20);
            SuitePrice = Browser.stringToDoubleConvert(Pages.Suite.suitePrice);
            SuitePrice1 = Browser.stringToDecimalConvert(Pages.Suite.suitePrice);
            SuiteName = Browser.getText(Pages.Suite.FirstsuiteName);
            //SuiteBaseCount 
            String BaseCntstring = Browser.getText(Pages.Suite.FirstSuiteBaseCount);
            String trime1 = BaseCntstring.Remove(6,7);
            String trim2 = trime1.Substring(5);
            int adults = Int32.Parse(trim2);
            AdulttoAdd = adults + 1;

            //Base 6, Max 8

        }

        [Then(@"I should see suite price should be a increased by dollar 50")]
        public void ThenSuitePriceIncreased()
        {
            Thread.Sleep(4000);
            String currentSuite = Browser.getText(Pages.Suite.FirstsuiteName);
            if (SuiteName == currentSuite)
            {
                Double SuitePrice3 = Browser.stringToDoubleConvert(Pages.Suite.suitePrice);
                Double SuitePrice2 = 50;
                Double Result = SuitePrice + SuitePrice2;
                Assert.IsTrue(SuitePrice3 == Result, "User sees that suite price increased by $50");
            }
        }

        [Then(@"I should see suite price should not be a increased by dollar 50")]
        public void ThenSuitePricenotIncreased()
        {
            Thread.Sleep(4000);
            String currentSuite = Browser.getText(Pages.Suite.FirstsuiteName);
            if (SuiteName == currentSuite)
            {
                Double SuitePrice3 = Browser.stringToDoubleConvert(Pages.Suite.suitePrice);
                Double Result = SuitePrice;
                Assert.IsTrue(SuitePrice3 == Result, "User sees that Suite price is not increasing by $50 for kids having age 2 or less.");

            }
        }

        [When(@"I select adult and update stay")]
        public void WhenISelectAdult(Table table)
        {
            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);
            Pages.Suite.clickUpdateyourStayBtn();
            Browser.scrollVerticalBy("300");
        }

        [When(@"I select adult greater than base and update stay")]
        public void WhenISelectAdult()
        {
            //String adults1 = table.Rows[0]["Adults"];
            //int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(AdulttoAdd);
            Pages.Suite.clickUpdateyourStayBtn();
            Browser.scrollVerticalBy("300");
        }
        

        [When(@"I select Kids and update stay")]
        public void WhenISelectKids(Table table)
        {
            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            String kids1age = table.Rows[0]["Kids1Age"];
            Pages.Suite.SelectKids(kids, kids1age);
            //String kids1age = table.Rows[0]["Kids1Age"];
            //int kids1Age = Int32.Parse(kids1age);
            //Pages.Suite.SelectKids1Age(kids1Age);
            Pages.Suite.clickUpdateyourStayBtn();
        }

        [When(@"I select check In date")]
        public void WhenISelectCheckInDate()
        {
            Pages.Suite.selectCheckIn();
        }

        [When(@"I select check Out date")]
        public void WhenISelectCheckOutDate()
        {
            Pages.Suite.selectCheckOut();
        }

        [When(@"I select Adults")]
        public void WhenISelectAdults(Table table)
        {
            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);
        }

        [When(@"I enter Offer Code")]
        public void WhenIEnterOfferCode(Table table)
        {
            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);
        }

        [When(@"I click on Book Now button")]
        public void WhenICLickOnBookNowBtn()
        {
            Pages.Suite.clickBookNowBtn();
        }

        [Then(@"I should see Exceeded Maximum Occupancy alert message for suite")]
        public void ThenIshouldSeeExceededMaximunOccupacy()
        {

            //Pages.Suite.clickThemedTab();
            Boolean ExceedMaxOccupancy = Pages.Suite.exceededMaxOccupancyalert(); 
            Assert.IsTrue(ExceedMaxOccupancy, "User see Exceeded Maximum Occupancy alert message for Exceeded Maximum Occupancy suite");
        }


        [Then(@"I should see Not Available lable for suite")]
        public void ThenIshouldSeeNotAvailablelable()
        {
            Boolean NotAvailableLabel = Pages.Suite.notAvailablelabelDisplayed();
            Assert.IsTrue(NotAvailableLabel, "User see Not Available Label for Exceeded Maximum Occupancy suite");
        }

        [When(@"I close Offer code error pop up")]
        public void WhenICLickOnOfferCodeErrorcloseIcon()
        {
            Pages.Suite.clickOfferCodeErrorCloseIcon();
        }

        [Then(@"I should see BBAR related suites")]
        public void ThenIshouldSeeBBARSuite()
        {
            Boolean BestAvailableRateSuite = Pages.Suite.BBARSuiteDisplayed();
            Assert.IsTrue(BestAvailableRateSuite, "User see Best Available Rate Suite when closed Offer code error pop up");
        }

        [When(@"I select Suite option")]
        public void WhenISelectSuiteOption(Table table)
        {
            String suiteOption = table.Rows[0]["Suite Option"];
            Pages.Suite.selectSUiteOption(suiteOption);
        }

        [When(@"I observe selected Check In date")]
        public void WhenIobserveChckInDt()
        {
             Pages.Suite.getCheckInDateText();
        }

        [When(@"I observe selected Check Out date")]
        public void WhenIobserveChckOutDt()
        {
            Pages.Suite.getCheckOutDateText();
        }

        [When(@"I select Kids")]
        public void WhenISelectkids(Table table)
        {
            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            String kids1age = table.Rows[0]["Kids1Age"];
            Pages.Suite.SelectKids(kids, kids1age);
        }

        [When(@"I observe number of Stay Nights")]
        public void WhenIObserveNumberOfStayNights()
        {
            Pages.Suite.observeStayNights();
        }

        [When(@"I observe Suite Was price")]
        public void WhenIObserveSuiteWasPrice()
        {
            Pages.Suite.getSuiteWasPrice();
            SuitePrice = Browser.stringToDoubleConvert(Pages.Suite.suitePrice);
        }
        [When(@"I observe Adults and Kids count")]
        public void WhenIObserveAdultsAndKidsCount()
        {
            Pages.Suite.getAdultsCount();
            Pages.Suite.getKidsCount();
        }

        [When(@"I enter sold out suite dates on Booking Widget")]
        public void WhenIEnterWeekendDatesOnBookingWidget(Table table)
        {
            Pages.Suite.selectCheckInSoldOutSuite();
            Pages.Suite.selectCheckOutSoldOutSuite();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);
        }

        [When(@"I navigate from Home page to Suite page with valid data")]
        public void HomeToSuitePage(Table table)
        {
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            String kids1age = table.Rows[0]["Kids1Age"];
            Pages.Suite.SelectKids(kids, kids1age);
            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);
            Pages.Suite.clickBookNowBtnORupdateyourStay();
            Browser.waitForElementToBeClickable(Pages.Suite.chkRateCalLink, 90);
        }

        [Then(@"I should see applied offer code related suites")]
        public void ThenIshouldSeeAppliedOfferCodeSuite(Table table)
        {
            String OfferCode = table.Rows[0]["OfferCode"];

            Boolean AppliedOfferCode = Pages.Suite.verfiyAppliedOfferCodeSuiteDisplayed(OfferCode);
            Assert.IsTrue(AppliedOfferCode, "I should see applied offer code related suites");
        }


        [Then(@"I should see Standard Tab is selected")]
        public void ThenIshouldseeSTandardTabisselected()
        {
            Boolean StandardTab = Pages.Suite.standardtabDisplayed();
            Assert.IsTrue(StandardTab, "Standard tab is selected");
        }
        [Then(@"I should see Premium Tab is selected")]
        public void ThenIshouldseeSPremiumTabisselected()
        {
            Boolean PremiumTab = Pages.Suite.premiumtabDisplayed();
            Assert.IsTrue(PremiumTab, "Premium tab is selected");
        }
        [Then(@"I should see Themed Tab is selected")]
        public void ThenIshouldseeThemedTabisselected()
        {
            Boolean ThemedTab = Pages.Suite.themedtabDisplayedDefault();
            Assert.IsTrue(ThemedTab, "Themed tab is selected");
        }

        [Then(@"I should see Best Available Rate into suite info suite page1")]
        public void ThenIShouldSeeBestAvailableRateIntoSuiteInfoSuitePage1()
        {
            Boolean BestAvailableRate = Pages.Suite.VerifyBBRratetext();
            Assert.IsTrue(BestAvailableRate, "Best Available Rate is displayed in Suite page");
        }
        [When(@"I observed price for above BW criteria")]
        public void WhenIObservedPriceForAboveBWCriteria()
        {
            Pages.Suite.VerifySuitePriceBefore();
        }

        [When(@"I select Kid with age two and update stay")]
        public void WhenISelectKidWithAgeTwoAndUpdateStay(Table table)
        {
            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            String kids1age = table.Rows[0]["Kids1Age"];
            Pages.Suite.SelectKids(kids, kids1age);
            Pages.Suite.clickUpdateyourStayBtn();
        }
        
        [Then(@"I should see suite price should not be a increased")]
        public void ThenIShouldSeeSuitePriceShouldNotBeAIncreased()
        {

            Boolean ISsamePrice = Pages.Suite.VerifySuitePriceBeforeAfterIsSame();
            Assert.IsTrue(ISsamePrice, "Best Available Rate is displayed in Suite page");
        }

        [When(@"I Select Room Type and accessible suite")]
        public void SelectRoomTypeWithAccessibleType(Table table)
        {
            String Theme = table.Rows[0]["ThemeType"];
            String Premium = table.Rows[0]["PremiumType"];
            String Standard = table.Rows[0]["StandardType"];
            String Accessible = table.Rows[0]["AccessibleToggle"];
            Pages.Suite.selectRoomType(Theme, Premium, Standard, Accessible);
        }

        [When(@"I click on Clear filter link")]
        public void clickONClearFilterlink()
        {
            Pages.Suite.clickOnClearFilterLink();
        }

        [Then(@"I should not see clear filter link")]
        public void ThenIShouldNotSeeClearFilterLink()
        {
            Boolean clearFilter = Pages.Suite.clearFilterIsNotDisplayed();
            Assert.IsTrue(clearFilter, "I should not see clear filter link");
        }

        [When(@"I click on Room Type drop down")]
        public void clickONRoomTypeDrp()
        {
            Pages.Suite.clickOnRoomTypeFilter();
        }

        [When(@"I click on Close Room Type link")]
        public void clickONCloseRoomType()
        {
            Pages.Suite.clickOnCloseRoomTypeFilter();
        }

        [Then(@"I should see Not see Theme room type option")]
        public void ThenIShouldNotSeeThemeroomTypeOption()
        {
            Boolean ThemeOption = Pages.Suite.verifyThemeRoomTypeOptionShouldNotDisplayed();
            Assert.IsFalse(ThemeOption, "I should Not see Theme room type option");
        }

        [Then(@"I should see Theme room type option")]
        public void ThenIShouldSeeThemeroomTypeOption()
        {
            Boolean ThemeOption = Pages.Suite.verifyThemeRoomTypeOption();
            Assert.IsTrue(ThemeOption, "I should see Theme room type option");
        }

        [Then(@"I should see Premium room type option")]
        public void ThenIShouldSeePremiumroomTypeOption()
        {
            Boolean PremiumOption = Pages.Suite.verifyPremiumRoomTypeOption();
            Assert.IsTrue(PremiumOption, "I should see Premium room type option");
        }

        [Then(@"I should see Standard room type option")]
        public void ThenIShouldSeeStandardroomTypeOption()
        {
            Boolean StandardOption = Pages.Suite.verifyStandardRoomTypeOption();
            Assert.IsTrue(StandardOption, "I should see Standard room type option");
        }

        [Then(@"I should see Themed suites")]
        public void ThenIShouldSeeThemedSuite()
        {
            Boolean ThemedSuite = Pages.Suite.verifyThemedLabelOnSuite();
            Assert.IsTrue(ThemedSuite, "I should see Themed suites");
        }
        
        [Then(@"I should see Standard suites")]
        public void ThenIShouldSeeStandardSuite()
        {
            Boolean StandardSuite = Pages.Suite.verifyStandardLabelOnSuite();
            Assert.IsTrue(StandardSuite, "I should see Standard suites");
        }

        [Then(@"I should see Premium suites")]
        public void ThenIShouldSeePremiumSuite()
        {
            Boolean PremiumSuite = Pages.Suite.verifyPremiumLabelOnSuite();
            Assert.IsTrue(PremiumSuite, "I should see Premium suites");
        }

        [Then(@"I should see Lowest To Highest Sort Option")]
        public void ThenIShouldSeeLowestToHighestSortOption()
        {
            Boolean LowToHigh = Pages.Suite.verifyLowestToHighestSortOption();
            Assert.IsTrue(LowToHigh, "I should see Lowest To Highest Sort Option");
        }

        [Then(@"I should see Highest to Lowest Sort Option")]
        public void ThenIShouldSeeHighestToLowestSortOption()
        {
            Boolean HighToLow = Pages.Suite.verifyHighestToLowestSortOption();
            Assert.IsTrue(HighToLow, "I should see Highest to Lowest Sort Option");
        }

        [Then(@"I should see Recommended Sort Option")]
        public void ThenIShouldSeeRecommendedSortOption()
        {
            Boolean RecommendedOption = Pages.Suite.verifyRecommendedSortOption();
            Assert.IsTrue(RecommendedOption, "I should see Recommended Sort Option");
        }

        [When(@"I click on Sort Option fillter dropdown")]
        public void clickONSortDropDown()
        {
            Pages.Suite.clickOnSortOptionDrpDwn();
        }

        [When(@"I click on Low to High sort option")]
        public void clickONLowToHighSort()
        {
            Pages.Suite.clickOnLowToHighOption();
        }
        
        [When(@"I click on High to Low sort option")]
        public void clickONHighToLowSort()
        {
            Pages.Suite.clickOnHighToLowOption();
        }

        [When(@"I click on Recommended sort option")]
        public void clickONRecommendedSort()
        {
            Pages.Suite.clickOnRecommendedOption();
        }
        
        [Then(@"I should see Recommended label for suite")]
        public void ThenIShouldSeeRecommendedLable()
        {
            Boolean recomded = Pages.Suite.verifyRecommendedLabel();
            Assert.IsTrue(recomded, "I should see Recommended label for suite");
        }

        [Then(@"I should see Suite sorted as Low to High")]
        public void ThenIShouldSeeSuiteSortedLowToHigh()
        {
            Boolean suiteSort = Pages.Suite.VerifyLowToHighSortOfSuite();
            Assert.IsTrue(suiteSort, "I should see Suite sorted as Low to High");
        }

        [Then(@"I should see Suite sorted as High to Low")]
        public void ThenIShouldSeeSuiteSortedHighToLow()
        {
            Boolean suiteSort = Pages.Suite.VerifyHighToLowSortOfSuite();
            Assert.IsTrue(suiteSort, "I should see Suite sorted as High to Low");
        }
        
         [Then(@"I should see user navigate on Plan pageafter change password")]
        public void ThenIShouldNavigateONPlanPage()
        {
            Boolean suiteSort = Pages.Suite.verifyCheckInField();
            Assert.IsTrue(suiteSort, "I should see user navigate on Plan pageafter change password");
        }

    }
}
