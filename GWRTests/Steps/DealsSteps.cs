﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GWRFramework;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class DealsSteps
    {
        [When(@"I navigate from Home page to deals page")]
        public void homeToDealsPage()
        {
            
            Pages.PropertyHome.closeDealsSignUp();
            Pages.Deals.naviagteOnSpecialOffersLink();
            Pages.Deals.clickOnDealsLink();
            Pages.Deals.checkDealsPageDisplayed();
        }

        [When(@"I navigate from Home page to Birthday Parties page")]
        public void homeToBirthdayPartiesPage()
        {

            Pages.PropertyHome.closeDealsSignUp();
            Pages.Deals.naviagteOnSpecialOffersLink();
            Pages.Deals.clickOnBirthdayPartiesLink();
            //Pages.Deals.checkDealsPageDisplayed();
        }
        

        [When(@"I click on exclusive deal sign in link")]
        public void WhenclickSigninlink()
        {
            if (Pages.PropertyHome.userSignedInMenu.Exists() == true)
            {
                Pages.PropertyHome.clickToSignOut();
            }
            Pages.Deals.clickOnDealSignInLink();
        }
        [When(@"I click on exclusive deal Sign Up Now button")]
        public void WhenclickSignUpNowBtn()
        {
            if (Pages.PropertyHome.userSignedInMenu.Exists() == true)
            {
                Pages.PropertyHome.clickToSignOut();
            }
            Pages.Deals.clickOnDealSignUpBtn();
        }

        [Then(@"I should see Promo code on T3 page and inside booking engine")]
        public void ThenIShouldSeePromoONT3()
        {
            Boolean Result = Pages.Deals.offerCodeOnRT3Detail();
            Assert.IsTrue(Result, " I should see Promo code on T3 page and inside booking engine");
        }
        
        [Then(@"I should not see Promo code on inside booking engine")]
        public void ThenIShouldNotSeePromoONT3Engine()
        {
            Boolean Result = Pages.Deals.offerCodeOnSholdNotOnT3Detail();
            Assert.IsTrue(Result, "I should not see Promo code on inside booking engine");
        }

        [When(@"I Select Date with Applied offer code")]
        public void WhenISelectDateandOffercode()
        {
            Pages.Deals.getEndDateOfercodeAvailbilty();
        }

        [Then(@"I should see correct offercode applied for suites")]
        public void ThenIShouldSeeCrctOfferCdAppliedForSuites()
        {
            Boolean Result = Pages.Deals.appliedOfrCode();
            Assert.IsTrue(Result, "I should see correct offercode applied for suites");
        }

        [Then(@"I should not see Promo code on inside Deal Info section")]
        public void ThenIShouldNotSeePromoONDealInfo()
        {
            Boolean Result = Pages.Deals.PromoSecNotShow();
            Assert.IsTrue(Result, "I should not see Promo code on inside Deal Info section");
        }

        [Then(@"I should not see Sign up Now button")]
        public void WhenIShouldNotSeeSignUpNowBth()
        {
            Boolean SignUpBtn = Pages.Deals.verifyDealSignUpBtn();
            Assert.IsFalse(SignUpBtn, " I should not see Sigup up Now button");
        }
        
        [When(@"I click on apply code button")]
        public void WhenclickOnApplyCodeBtn(Table table)
        {
            String OfferCode = table.Rows[0]["OfferCode"];
            Pages.Deals.clickOnApplyCodeBtn(OfferCode);
        }

        [Then(@"I should see automatically populated correct offer code in booking engine")]
        public void ThenIShouldSeecorrectOffercodeUnderBookingEngine(Table table)
        {
            String OfferCode = table.Rows[0]["OfferCode"];
            Boolean OfferCdValue = Pages.Deals.verifyOffercodeUnderBookingEngine(OfferCode);
            Assert.IsTrue(OfferCdValue, "I should see automatically populated correct offer code in booking engine");
        }

        [Then(@"I should see automatically populated correct offer code in booking engine on T3")]
        public void ThenIShouldSeecorrectOffercodeUnderBookingEngineOnT3()
        {
            //String OfferCode = table.Rows[0]["OfferCode"];
            Boolean OfferCdValue = Pages.Deals.verifyOffercodeUnderBookingEngine(DealsPage.PromoCode);
            Assert.IsTrue(OfferCdValue, "I should see automatically populated correct offer code in booking engine on T3 Page");
        }

        [Then(@"I should see 'SIGN UP NOW!' button on deals page")]
        public void ThenIShouldSeeSignUpNowButton(Table table)
        {
            String SigupTxt = table.Rows[0]["SignUpButtonText"];
            Boolean SignupBtn = Pages.Deals.verifySignUpNowBtnText(SigupTxt);
            Assert.IsTrue(SignupBtn, "I should see 'SIGN UP NOW!' button text");
        }

        [When(@"I click on learn more link")]
        public void WhenclickOnLearnMorelnk()
        {
            Pages.Deals.clickOnLearnMoreLink();
        }

        [Then(@"I should navigate on package detail page")]
        public void ThenIShouldNavigateonPackageDetailPage()
        {            
            Boolean DealTitle = Pages.Deals.VerifyDealspkgDetailPage();
            Assert.IsTrue(DealTitle, "I should navigate on package detail page");
        }

        [Then(@"I should navigate on package detail page if Deal bar available")]
        public void ThenIShouldNavigateonPackageDetailPageIfDealBarAvlb()
        {
            Boolean DealTitle = Pages.Deals.VerifyDealspkgDetailPageIfDealBarAvailable();
            Assert.IsTrue(DealTitle, "I should navigate on package detail page if Deal bar available");
        }

        [When(@"I click on deal title that required sign in")]
        public void WhenclickSignInDealTitlelnk()
        {
            Pages.Deals.clickOnSignInDealTitleLink();
        }

        [Then(@"I should not see offer code auto fill into booking engine")]
        public void ThenIShouldNotSeeOfferCdUnderBookingEngine()
        { 
            Boolean OffercodeValue = Pages.Deals.VerifyOfferCodeFieldEmptyunderBookingEngine();
            Assert.IsTrue(OffercodeValue, "I should not see offer code auto fill into booking engine for sign in deals");
        }


        [When(@"I Sign in into application through 'Sign In Now To Reveal The Offer Code' button")]
        public void WhenclickSignInBtnT3Page(Table table)
        {
            Pages.Deals.clickOnSignInBtnT3Page();
            String email = table.Rows[0]["Email"];
            Pages.SignIn.sendEmail(email);
            String password = table.Rows[0]["Password"];
            Pages.SignIn.sendPass(password);
            Pages.SignIn.clickSignInbtn();

        }

        [Then(@"I should see autofilled correct offer code under booking engine")]
        public void ThenIShouldSeeOfferCdUnderBookingEngine()
        {
            Boolean OffercodeValue = Pages.Deals.VerifyOfferCodeunderBookingEngine();
            Assert.IsTrue(OffercodeValue, "I should see autofilled correct offer code under booking engine after sign in on deals T3 Page");
        }

        [When(@"I click on ESAVER deal title that required sign in")]
        public void WhenclickESAVERSignInDealTitlelnk()
        {
            Pages.Deals.clickOnESAVERSignInDealTitleLink();
        }

        [Then(@"I should not see 'SIGN IN NOW TO REVEAL THE OFFER CODE' button on T3 Page")]
        public void ThenIShouldnotSeeSignInbtn()
        {
            Boolean OffercodeValue = Pages.Deals.VerifySignInBtnT3Page();
            Assert.IsFalse(OffercodeValue, "I should not see 'SIGN IN NOW TO REVEAL THE OFFER CODE' button after sign in on deals T3 Page");
        }

        [Then(@"I should see 'SIGN IN NOW TO REVEAL THE OFFER CODE' button on T3 Page")]
        public void ThenIShouldSeeSignInbtn()
        {
            Boolean OffercodeValue = Pages.Deals.VerifySignInBtnT3Page();
            Assert.IsTrue(OffercodeValue, "I should see 'SIGN IN NOW TO REVEAL THE OFFER CODE' button without sign in on deals T3 Page");
        }

        [When(@"I enter check in dates within 60 days from today and check out date on booking widget of deals page")]
        public void WhenIEnterValidDetailsOnBookingWidgetWithinDates60DaysrthroughdealsPage(Table table)
        {
            String CheckInDate = DateTime.Today.AddDays(50).ToString("MM/dd/yyyy");
            String newdate = CheckInDate.Replace("-", "/");
            Pages.PropertyHome.enterCheckInDate(newdate);

            String CheckoutDate = DateTime.Today.AddDays(51).ToString("MM/dd/yyyy");
            String newOutdate = CheckoutDate.Replace("-", "/");
            Pages.PropertyHome.clearCheckOutDate();
            Pages.PropertyHome.enterCheckOutDate(newOutdate);

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Deals.SelectAdults(adults);
        }

        [Then(@"I should see valid values in no of children drop down")]
        public void ThenIShouldSeevalChildrnCnt()
        {
            Boolean childCnt = Pages.Deals.ChildDrpValueVerify();
            Assert.IsTrue(childCnt, "I should see valid values in no of children drop down");
        }
        

        [Then(@"I should see valid values in no of Adult drop down")]
        public void ThenIShouldSeevalAdultdrnCnt()
        {
            Boolean adultCnt = Pages.Deals.AdultDrpValueVerify();
            Assert.IsTrue(adultCnt, "I should see valid values in no of Adult drop down");
        }

        [When(@"I eneter contact information on birthday form")]
        public void iEnterContInfo(Table table)
        {
            String FnameTxt = table.Rows[0]["First Name"];
            String LnameTxt = table.Rows[0]["Last Name"];
            String AddressTxt = table.Rows[0]["Address"];
            String PCode = table.Rows[0]["Postal Code"];
            String CityTxt = table.Rows[0]["City"];
            String EmailTxt = table.Rows[0]["Email"];
            String PhoneTxt = table.Rows[0]["Phone"];
            String ConMeth = table.Rows[0]["Contact Method"];
            String Country = table.Rows[0]["Country"];
            Pages.Deals.BirthdayPatyFormContactInfo(FnameTxt, LnameTxt, AddressTxt, PCode, CityTxt, EmailTxt, PhoneTxt, ConMeth, Country);
        }

        [When(@"I eneter Party Details on birthday form")]
        public void iEnterPartyDetails(Table table)
        {
            String MM = table.Rows[0]["Month"];
            String DD = table.Rows[0]["Date"];
            String YY = table.Rows[0]["Year"];
            String Choice = table.Rows[0]["Children Attending Unser Age 3"];            
            Pages.Deals.BirthdayPartDetailsForm(MM, DD, YY, Choice);
        }

        [When(@"I eneter birthday child info on birthday form")]
        public void iEnterBirthdayChild(Table table)
        {
            String Fname = table.Rows[0]["Child FName"];
            String Lname = table.Rows[0]["Child LName"];
            String MM = table.Rows[0]["Month"];
            String DD = table.Rows[0]["Date"];
            String YY = table.Rows[0]["Year"];
            Pages.Deals.WhoIsBirthdayChildForm(Fname, Lname, MM, DD, YY);
        }

        [When(@"I click on Submit Form Button")]
        public void iClickOnSubmitFormBtn()
        {
            Pages.Deals.clickOnSubmitFormBtn();
        }
    }
}
