﻿using GWRFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class DiningPageSteps
    {
        [When(@"I navigate Dining page to Activity through Progress bar")]
        public void WhenIClickOnactivityProgressBar()
        {
            Pages.Dining.navigateOnActivity();
        }

        [When(@"I navigate Dining page to Suite through Progress bar")]
        public void WhenIClickOnSuiteProgressBar()
        {
            Pages.Dining.navigateOnSuite();
        }

        [When(@"I click on Continue to Payment page button")]
        public void whenIClickOnContinueToPaymentPagaButton()
        {
            Browser.waitForElementToBeClickable(Pages.Suite.continueToPaymentPageBtn, 40);
            Pages.Suite.continueToPaymentPageBtn.Click();
        }

        [Then(@"I should see valid adult count")]
        public void ThenISeeShouldAdultCount(Table table)
        {
            String adultcnt = table.Rows[0]["Adults"];
            Boolean adultOnDining = Pages.Dining.verifyAdultcount(adultcnt);
            Assert.IsTrue(adultOnDining, "I should see valid adults count on Dining page");
        }

        [Then(@"I should see valid Kid count")]
        public void ThenISeeShouldKidCount(Table table)
        {
            String kidcnt = table.Rows[0]["Kids"];
            Boolean kidOnDining = Pages.Dining.verifyKidscount(kidcnt);
            Assert.IsTrue(kidOnDining, "I should see valid Kids count on Dining page");
        }

        [When(@"I click on Dining package Add button")]
        public void whenIClickOnDiningPckgAddButton()
        {
            Pages.Dining.clickONAddButton();
        }

        [When(@"I click on Dining package Update button")]
        public void whenIClickOnDiningPckgUpdateButton()
        {
            Pages.Dining.clickONUpdateButton();
        }

        [Then(@"I should see added package in cost summary")]
        public void verufyAddedDiningPckg()
        {
            Boolean packgTitle = Pages.Dining.verifyPackageaddedAfterAddBtn();
            Assert.IsTrue(packgTitle, "I should see package added in cost summarry.");
        }

        [When(@"I click on dining package Remove link")]
        public void whenIClickOnDiningPckgRemovLink()
        {
            Pages.Dining.clickONRemoveLink();
        }

        [Then(@"I should see added package is removed from cost summary")]
        public void verifyRemovdDiningPckg()
        {
            Boolean packgRemoved = Pages.Dining.verifyPckgRemoved();
            Assert.IsTrue(packgRemoved, "I should see package removed from cost summarry.");
        }

        [When(@"I select package quanitity")]
        public void whenIClickOnDiningPckgRemovLink(Table table)
        {
            String pckgQuantity = table.Rows[0]["Package Quantity"];
            Pages.Dining.selectPackgeQuantityOption(pckgQuantity);
        }

        [When(@"I observe package cost for single quantity")]
        public void whenIobservePckgCost()
        {
            Pages.Dining.verifyCostOfOneDiningPckg();
        }

        [Then(@"I should see correct package cost after update quantity")]
        public void whenIobservePckgCost(Table table)
        {
            String pckgQuantity = table.Rows[0]["Second Package Quantity"];
            int inPckgQntity = Int32.Parse(pckgQuantity);
            Boolean quantity =  Pages.Dining.verifyCostAftrDiningPckgQuantityUpdate(inPckgQntity);
            Assert.IsTrue(quantity, "I should see correct cost after package quantity update.");
        }

        [Then(@"I should see correct Check In Date")]
        public void shouldSeeCheckInDtae()
        {
            Boolean checkInDate = Pages.Dining.verifyCheckinDateOnCost();
            Assert.IsTrue(checkInDate, "I should see correct Check In date on cost summary.");
        }

        [Then(@"I should see correct Check Out Date")]
        public void shouldSeeCheckOutDtae()
        {
            Boolean checkOutDate = Pages.Dining.verifyCheckOutDateOnCost();
            Assert.IsTrue(checkOutDate, "I should see correct Check Out date on cost summary.");
        }

        [Then(@"I should see no of nights set as quantity")]
        public void shouldSeenightQuantity()
        {
            Boolean noOfNights = Pages.Dining.packgDrpVal();
            Assert.IsTrue(noOfNights, "I should see correct correct no of night quantity.");
        }

        [Then(@"I should see correct chareges applied for Kids")]
        public void shouldSeeCrctVal(Table table)
        {
            String guestCnt = table.Rows[0]["Billable Guest Count"];
            int inGuestCnt = Int32.Parse(guestCnt);
            Boolean noOfNights = Pages.Dining.verifyBillableCost(inGuestCnt);
            Assert.IsTrue(noOfNights, "I should see correct charges applied for Kids");
        }

        [Then(@"I should see correct Promotional and Your Saving cost")]
        public void shouldSeeCrctpromoNdYorSavngcost()
        {
            Boolean noOfNights = Pages.Dining.verifyPromotioanlSaving();
            Assert.IsTrue(noOfNights, "I should see correct Promotional and Your Saving cost");
        }

        [Then(@"I should see Same suite total and Total when package not added")]
        public void shouldSeeCrtSuiteTotalNdTotal()
        {
            Boolean sameTotal = Pages.Dining.verifyAddintionOfPackage();
            Assert.IsTrue(sameTotal, "I should see Same suite total and Total when package not added");
        }

        [Then(@"I should see correct package total for person/day per package")]
        public void shouldSeeCrctpackgTotal(Table table)
        {
            String guestCnt = table.Rows[0]["Billable Guest Count"];
            int inGuestCnt = Int32.Parse(guestCnt);
            Boolean noOfNights = Pages.Dining.verifyBillableCost(inGuestCnt);
            Assert.IsTrue(noOfNights, "I should see correct package total for person/day per package");
        }

        [Then(@"I should see correct person/day per cost for package")]
        public void shouldSeeCrctPersonPerDayCost(Table table)
        {
            String guestCnt = table.Rows[0]["Billable Guest Count"];
            int inGuestCnt = Int32.Parse(guestCnt);
            Boolean noOfNights = Pages.Dining.verifyPersonPerDayCost(inGuestCnt);
            Assert.IsTrue(noOfNights, "I should see correct person/day per cost for package");
        }

        [When(@"I added multiple dining packages")]
        public void addMultiplePackge()
        {
            Pages.Dining.addmutilpleDiningPackage();            
        }

        [Then(@"I should see correct suite total after added packages")]
        public void shouldSeeCrctSuiteTotal(Table table)
        {
            String noDays = table.Rows[0]["No of Days"];
            int inDaysCnt = Int32.Parse(noDays);
            Boolean suiteTotal = Pages.Dining.verifyaddedPackageCostTotal(inDaysCnt);
            Assert.IsTrue(suiteTotal, "I should see correct suite total after added packages");
        }

        [Then(@"I should see correct total after added packages")]
        public void shouldSeeCrctTotal()
        {
            Boolean Total = Pages.Dining.verifyTotalAfterPckgAdd();
            Assert.IsTrue(Total, "I should see correct total after added packages");
        }

        [Then(@"I should see correct package total after update packages")]
        public void shouldSeeCrctPckgTotalAftrUpdatePckg()
        {
            Boolean Total = Pages.Dining.verifyUpdatePckgCost();
            Assert.IsTrue(Total, "I should see correct package total after update packages");
        }

        [When(@"I click on Dining view details link")]
        public void WhenIclickOnDiningviewDetailLink()
        {
            Pages.Activities.clickOnFamilyViewDetailsLink();
        }

        [Then(@"I should see dining Per Package drop down value if correct")]
        public void shouldSeeCrctPerPckgDrpVal()
        {
            Boolean Total = Pages.Dining.perPackgeDrpVal();
            Assert.IsTrue(Total, "I should see dining Per Package drop down value if correct");
        }
        

    }
}
