﻿using TechTalk.SpecFlow;
using GWRFramework;
using System;
using NUnit.Framework;
using System.Threading;

namespace GWRTests.Steps
{
    [Binding]
    public class ActivitiesPageSteps
    {

        [When(@"I navigate from Home page to Activities page")]
        public void WhenINavigateFromHomeToActivitiesPage(Table table)
        {
            Pages.PropertyHome.closeDealsSignUp();
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            String kids1age = table.Rows[0]["Kids1Age"];
            Pages.Suite.SelectKids(kids, kids1age);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

            Pages.Suite.clickBookNowBtn();
            Pages.Activities.navigateSuiteToActivities();
        }

        [When(@"I navigate to Attractions Tab")]
        public void WhenINavigateToAttractionsTab()
        {
            Pages.Activities.navigateToAttractionsTab();
        }
        [When(@"I navigate to Family Tab")]
        public void WhenINavigateToFamilyTab()
        {

            Pages.Activities.navigateToFamilyTab();
        }

        [When(@"I click on Family tab view details link")]
        public void WhenIclickOnFamilyviewDetailLink()
        {
            Pages.Activities.clickOnFamilyViewDetailsLink();
        }

        [When(@"I click on birthday tab view details link")]
        public void WhenIclickOnBirthdayviewDetailLink()
        {
            Pages.Activities.clickOnFamilyViewDetailsLink();
        }

        [Then(@"I should see View details is expanded")]
        public void ThenIShouldSeeViewDeatilsLinkExpanded()
        {
            Boolean ViewDetExpnded = Pages.Activities.verifyExpandedLink();
            Assert.IsTrue(ViewDetExpnded, "I should see View details is expanded");
        }
        

        [When(@"I navigate to Birthday Tab")]
        public void WhenINavigateToBirthdayTab()
        {
            Pages.Activities.navigateToBirthdayTab();
        }

        [When(@"I add Attractions Package")]
        public void WhenIAddAttractionsPackage()
        {
            Pages.Activities.clickOnPackageAddButton();
        }

        [When(@"I add Family Package")]
        public void WhenIAddFamilyPackage()
        {
            Pages.Activities.clickOnPackageAddButton();
        }

        [When(@"I add Birthday Package")]
        public void WhenIAddBirthdayPackage()
        {
            Pages.Activities.clickOnPackageAddButton();
        }

        [Then(@"I should see added package under cost summary")]
        public void ThenIShouldSeeAddedPackageUnderCostSummary()
        {
            Boolean PackageUnderCostSummary = Pages.Activities.addedPackageUnderCostSummary();
            Assert.IsTrue(PackageUnderCostSummary, "I should see added package under cost summary when click on package add button");
        }

        [When(@"I click on Package Remove link")]
        public void WhenIClickOnPackageRemoveLink()
        {
            Pages.Activities.clickOnPackageRemoveLink();
        }

        [Then(@"I should remove added package under cost summary")]
        public void ThenIShouldRemoveAddedPackageUnderCostSummary()
        {
            Boolean PackageUnderCostSummary = Pages.Activities.removePackageUnderCostSummary();
            Assert.IsTrue(PackageUnderCostSummary, "I should remove added package from cost summary when click on package remove link");
        }

        [When(@"I click on Suites Icon")]
        public void WhenIClickOnSuitesIcon()
        {
            Pages.Activities.clickOnSuitesIcon();
        }

        [Then(@"I should navigate on suite page")]
        public void ThenIShouldNaviagteOnSuitePage()
        {
            Boolean SuiteRoom = Pages.Activities.iAmOnSuitePage();
            Assert.IsTrue(SuiteRoom, "I should Navigate on Suite page after click on suite Icon on Activities Page");
        }

        [When(@"I click on Continue to Dining Packages button")]
        public void WhenIClickOnContinueToDiningPackagesButton()
        {
            Pages.Activities.clickOnContinueToDiningPackagesButton();
        }

        [Then(@"I should navigate on dining page")]
        public void ThenIShouldNaviagteOnDiningPage()
        {
            Boolean ContinueToPayment = Pages.Activities.iAmOnDiningPage();
            Assert.IsTrue(ContinueToPayment, "I should Navigate on Dining page after click on Continue To Dining packages button on Activities Page");
        }

        [Then(@"I should see Promotional saving and Your Savings price are same")]
        public void ThenIShouldSeePromotionalSavingAndYourSavingsPriceAreSame()
        {
            Pages.Activities.getPromotionalSavingPrice();
            Pages.Activities.getYourSavingPrice();
            Assert.IsTrue(ActivitiesPage.promotionalSavingVal == ActivitiesPage.yourSavingPrice, "I should see 'Promotional saving' price and 'Your Savings' price are same on Activities Page");
        }

        [When(@"I navigate from suite Page to Activities page with LCO")]
        public void WhenINavigateSuitePageToActivitiesPageWithLCO()
        {
            Pages.Suite.navigateSuiteToActivitiesWithLCO();
        }

        [When(@"I navigate from suite Page to Activities page without LCO")]
        public void WhenINavigateSuitePageToActivitiesPageWithoutLCO()
        {
            Pages.Suite.navigateSuiteToActivitiesWithoutLCO();
        }

        [Then(@"I should see 2 PM LOC package added under cost summary")]
        public void ThenIShouldSee2PMLCOPackageUnderCostSummary()
        {
            Boolean LCO = Pages.Activities.observe2PMLOCPackageUnderCostSummary();
            Assert.IsTrue(LCO, "I should see '2 PM LOC' package added under cost summary on Activities Page when added on Suite page");
        }

        [When(@"I observe Guests count")]
        public void WhenIObserveGuestsCount()
        {
            Pages.Suite.observeGuestsCount();
        }

        [Then(@"I should see package quantity upto selected guest count")]
        public void ThenIShouldSeePackageQtyUptoSelectedGuest()
        {
            Pages.Activities.getActivitiesPackageLastDropDownValue();
            Assert.IsTrue(SuitePage.GuestCount == ActivitiesPage.PackageQtyLastValue, "I should see Attactions packages quantity upto selected guest count.");
        }

        [When(@"I add attraction package for multiple quantity")]
        public void WhenIAddMultipleQtyAttractionPackage()
        {
            Pages.Activities.SelectMultipleQuantityPackage(Pages.Activities.packagequantityDpDn);
            Pages.Activities.clickOnPackageAddButton();
        }

        [Then(@"I should see correct attraction package price under cost summary")]
        public void ThenIShouldSeeCorrectAttractionPackgePrice()
        {
            Pages.Activities.getPackageTotalPriceWithQty();
            double PackagePriceunderCostSummary = Browser.stringToDoubleConvert(Pages.Activities.packagepriceunderCostSummary);
            Assert.IsTrue(ActivitiesPage.packagefinalprice == PackagePriceunderCostSummary, "I should see correct added attraction package price under cost summary");
        }

        [Then(@"I should see added package quantity drop down under cost summary")]
        public void ThenIShouldSeeAddedPackageQtyDpDnUnderCostSummary()
        {
            Boolean PackageDnDnUnderCostSummary = Pages.Activities.addedPackageDpDnUnderCostSummary();
            Assert.IsTrue(PackageDnDnUnderCostSummary, "I should see added package quantity drop down under cost summary when click on package add button");
        }

        [When(@"I update package quantity under cost summary")]
        public void WhenIUpdatePackageQtyUnderCostSummary()
        {
            Pages.Activities.SelectMultipleQuantityPackage(Pages.Activities.packageDpDnSection);
        }

        [Then(@"I should see correct reflected package cost under cost summary")]
        public void ThenIShouldSeeCorrectReflectedPackgePrice()
        {
            Pages.Activities.getPackageTotalPriceWithQty();
            double PackagePriceunderCostSummary = Browser.stringToDoubleConvert(Pages.Activities.packagepriceunderCostSummary);
            Assert.IsTrue(PackagePriceunderCostSummary.Equals(ActivitiesPage.packagefinalprice), "I should see correct reflected package cost under cost summary after update package quantity");
        }

        [Then(@"I see Package Total price should equals to addition of all added packages")]
        public void ThenIShouldSeePackgeTotalequalsToAdditonOfPackagesPrice()
        {
            Pages.Activities.packagesaddition(Pages.Activities.package1priceunderCostSummary, Pages.Activities.package2priceunderCostSummary);
            Pages.Activities.getPackageTotalPrice();
            Assert.IsTrue(ActivitiesPage.PackagesAdditionprice1 == ActivitiesPage.packageTotalPrice1, "I see Package Total price should equals to addition of all added packages");
        }

        [Then(@"I Should see Suite Total price equals to Total price on Activities page without any package added")]
        public void ThenIShouldSeeSuiteTotalequalsToTotalPrice()
        {
            Pages.Payment.convertSuiteTotal1();
            Pages.Activities.getTotalPrice();
            Assert.IsTrue(PaymentPage.suitePrice1 == ActivitiesPage.totalPrice1, "I Should see Suite Total price equals to Total price on Activities page without any package added");
        }

        [Then(@"I should see correct Total after added package")]
        public void ThenIShouldSeeCorrectTotalAfterAddedPackage()
        {
            Pages.Activities.getSuiteTotal();
            Pages.Activities.getPackageTotalPrice();
            Pages.Activities.getTotalPrice();
            decimal Priceaddition = ActivitiesPage.suiteTotal1 + ActivitiesPage.packageTotalPrice1;
            //Double PriceAddition = Math.Round((Double)Priceaddition, 2);
            Assert.IsTrue(Priceaddition == ActivitiesPage.totalPrice1, "I should see correct Total after added package");
        }

        [Then(@"I should see correct package total after update package quantity under cost summary")]
        public void ThenIShouldSeeCorrectPackageTotalAfterUpdatePackageQuantity()
        {
            Pages.Activities.getAddedPackagePrice();
            Pages.Activities.getPackageTotalPrice();
            Assert.IsTrue(ActivitiesPage.packagePrice1 == ActivitiesPage.packageTotalPrice1, " I should see correct package total after update package quantity under cost summary on activities page");
        }

        [Then(@"I should see updated package quantity under cost summary")]
        public void ThenIShouldSeeUpdatePackageQuantityUnderCostSummary()
        {
            Pages.Activities.getPackageSelectedQtyValue(Pages.Activities.packageDpDnSection);
            Boolean result = Pages.Activities.verifyUpdatedPackageQty();
            Assert.IsTrue(result, "I should see updated package quantity under cost summary when update package quantity from cost summary");
        }

        [Then(@"I should see suite total equals to selected suite price with number of stay nights")]
        public void ThenIShoulSeeSuiteTotalEqualsToSelectedSuitePriceWithStayNights()
        {
            //Double finalSuitePrice = SuitePageSteps.SuitePrice * SuitePage.StayNights;
            decimal finalSuitePrice = SuitePageSteps.SuitePrice1 * SuitePage.StayNights;
            //Double FinalSuitePrice = Math.Round((Double)finalSuitePrice, 2);
            Pages.Activities.getSuiteTotal();
            Assert.IsTrue(finalSuitePrice.Equals(ActivitiesPage.suiteTotal1), "I should see suite total equals to selected suite price multiply with number of stay nights");
        }

        [Then(@"I should see Suite Total equals to Base price minus Promotional Saving price")]
        public void ThenIShoulSeeSuiteTotalEqualsToBaseMinusPromotionalSaving()
        {
            Pages.Activities.getSuiteBasePrice1();
            Pages.Activities.getPromotionalSavingPrice1();
            decimal ActualSuitePrice = ActivitiesPage.suiteBasePrice1 - ActivitiesPage.promotionalSavingVal1;
            Pages.Activities.getSuiteTotal();
            Assert.IsTrue(ActivitiesPage.suiteTotal1 == ActualSuitePrice, "I should see Suite Total equals to Base price minus Promotional Saving price");
        }

        [Then(@"I should see suite base price equals to multiplication of suite was price and number of stays nights")]
        public void ThenIShoulSeeSuiteTotalEqualsToMultiplicationOfSuiteWasPriceAndStayNights()
        {
            Double totalSuiteWasPrice = SuitePage.suiteWasPrice * SuitePage.StayNights;
            Double TotalSuiteWasPrice = Math.Round((Double)totalSuiteWasPrice, 2);
            Pages.Activities.getSuiteBasePrice();
            Assert.IsTrue(TotalSuiteWasPrice == ActivitiesPage.suiteBasePrice, "I should see suite base price equals to multiplication of suite was price and number of stays nights");
        }

        [Then(@"I should see Promotional Saving price correctly under cost summary on activities page")]
        public void ThenIShoulSeePromotionalSavingCorrectly()
        {
            Pages.Activities.getSuiteBasePrice1();
            Pages.Activities.getSuiteTotal();
            decimal ActualPromotionalPrice = ActivitiesPage.suiteBasePrice1 - ActivitiesPage.suiteTotal1;
            Pages.Activities.getPromotionalSavingPrice1();
            decimal Pro = ActivitiesPage.promotionalSavingVal1;
            Assert.IsTrue(ActivitiesPage.promotionalSavingVal1 == ActualPromotionalPrice, "I should see Promotional Saving price equals to Base price minus Suie Total price");
        }

        [Then(@"I should see correct adults count under cost summary")]
        public void ThenIShoulSeeCorrectAdultsCount()
        {
            Pages.Activities.getAdultsCountunderCostSummary();
            Assert.IsTrue(ActivitiesPage.AdultsCountUnderCostSummary == SuitePage.AdultsCount, "I should see same adults count under cost summary that is selected while searcing suites");
        }

        [Then(@"I should see correct kids count under cost summary")]
        public void ThenIShoulSeeCorrectKidsCount()
        {
            Pages.Activities.getKidsCountunderCostSummary();
            Assert.IsTrue(ActivitiesPage.KidsCountUnderCostSummary == SuitePage.KidsCount, "I should see same kids count under cost summary that is selected while searcing suites");
        }

        [Then(@"I should not see previously added packages under cost summary")]
        public void ThenIShouldNotSeePreviouslyAddedPackageUnderCostSummary()
        {
            Boolean PackageUnderCostSummary = Pages.Activities.removePackageUnderCostSummary();
            Assert.IsTrue(PackageUnderCostSummary, "I should not see previously added packages under cost summary when go to suite page from activities page and select other suite");
        }

        [Then(@"I should see latest 2 PM LCO package added under cost summary")]
        public void ThenIShouldSeeLatest2PMLCOPackageUnderCostSummary()
        {
            Boolean Standard2pmLCOunderCostSummary = Pages.Activities.verifylatest2pmLCOUnderCOstSummary();
            Assert.IsTrue(Standard2pmLCOunderCostSummary, "I should see latest 2 pm LCO package under cost summary when navigate from activites to suite page and select other 2 pm LCO");
        }

        [Then(@"I should see correct latest 2 PM LCO package price under cost summary")]
        public void ThenIShouldSeeCorrectLatest2PMLCOPackagePriceUnderCostSummary()
        {
            Boolean LCO;
            Pages.Activities.get2pmLCOpriceunderCostSummary();
            if (SuitePage.LCOExist == true)
            {
                LCO = SuitePage.LCOPrice == ActivitiesPage.LCOPriceCostSummary;
                    }
            else {
                LCO = true;
            }
                Assert.IsTrue(LCO, "I should see correct 2 pm LCO package price under cost summary when navigate from activites to suite page and select other 2 pm LCO");
        }

        [Then(@"I should not see old 2 PM LCO package under cost summary")]
        public void ThenIShouldNotSeeOld2PMLCOPackageUnderCostSummary()
        {
            Boolean themed2pmLCOunderCostSummary = Pages.Activities.verifyOld2pmLCOUnderCOstSummary();
            Assert.IsTrue(themed2pmLCOunderCostSummary, "I should not see old 2 pm LCO package under cost summary when navigate from activites to suite page and select other 2 pm LCO");
        }

        [Then(@"I should see correct Check In and Check Out date under cost summary through rate calendar")]
        public void ThenIShouldseeCorrectCheckInAndCheckOutDate()
        {
            String CheckIn = RateCalendar.CheckInDate;
            String Checkout = RateCalendar.CheckOutDate;
            Pages.Activities.getCheckInDateUnderCostSummary();
            Pages.Activities.getCheckOutDateUnderCostSummary();
            ActivitiesPage.CostSummaryCheckIntDate = ActivitiesPage.CostSummaryCheckIntDate.Substring(3, 2);
            ActivitiesPage.CostSummaryCheckOutDate = ActivitiesPage.CostSummaryCheckOutDate.Substring(3, 2);
            Boolean Result1 = CheckIn.Equals(ActivitiesPage.CostSummaryCheckIntDate);
            Boolean Result2 = Checkout.Equals(ActivitiesPage.CostSummaryCheckOutDate);
            Assert.IsTrue(Result1 && Result2, "I should see correct check in and check out date in cost summary which is selected on rate calendar");
        }

        [Then(@"I should see correct Suite Total under cost summary through rate calendar")]
        public void ThenIShouldseeCorrectSuiteTotalThroughRateCalendar()
        {
            Pages.Activities.getSuiteTotal();
            Assert.IsTrue(RateCalendar.RateCalendarSuitePrice1 == ActivitiesPage.suiteTotal1, "I should see correct Suite total under cost summary which is selected on rate calendar");
        }

        [Then(@"I should see correct suite base price should displayed on activities page through rate calendar")]
        public void ThenIShouldSeeCorrectSuiteBasePrice()
        {
            //Thread.Sleep(15000);
            Browser.scrollVerticalBy("400");
            Pages.Activities.getSuiteTotal();
            Pages.Activities.getPromotionalSavingPrice1();
            Pages.Activities.getSuiteBasePrice1();
            decimal ExpectedSuiteBasePrice = ActivitiesPage.suiteTotal1 + ActivitiesPage.promotionalSavingVal1;
            Assert.IsTrue(ActivitiesPage.suiteBasePrice1 == ExpectedSuiteBasePrice, "I should see suite base price equals to addition of Suite total and promotional saving price on activities page through rate calendar");
        }

        [Then(@"I should see corret Total price should displayed on activities page through rate calendar")]
        public void ThenIShouldSeeCorrectTotalPrice()
        {
            Pages.Activities.getSuiteTotal();
            Pages.Activities.getTotalPrice();
            Assert.IsTrue(ActivitiesPage.suiteTotal1 == ActivitiesPage.totalPrice1, "I should see total price equals to Suite total on activities page through rate calendar");
        }

        [Then(@"I should see correct promotional saving price on activities page through rate calendar")]
        public void ThenIShouldSeeCorrectPromotionalSavingPriceThroughRateCalendar()
        {
            Pages.Activities.getSuiteTotal();
            Thread.Sleep(15000);
            Pages.Activities.getPromotionalSavingPrice1();
            Pages.Activities.getSuiteBasePrice1();
            decimal ExpectedPromotionalSavingPrice = ActivitiesPage.suiteBasePrice1 - ActivitiesPage.suiteTotal1;
            Assert.IsTrue(ActivitiesPage.promotionalSavingVal1 == ExpectedPromotionalSavingPrice, "I should see promotinal saving price equals to Suite base price minus Suite total price on activities page through rate calendar");
        }

        //Rohit ======================================================
        [Then(@"I should see valid offercode on Actvity")]
        public void ThenIShouldseeValidOffCdOnActvity(Table table)
        {
            String offerCodeTD = table.Rows[0]["Offer Code"];
            Boolean offerCode = Pages.Activities.verifyOffercodeOnCostSumry(offerCodeTD);
            Assert.IsTrue(offerCode, "I should valid offer code on Activities Page");
        }

        [Then(@"I should not see offercode on Actvity")]
        public void ThenIShouldNtseeValidOffCdOnActvity()
        {
            Boolean offerCode = Pages.Activities.verifyOffercodeNotOnCostSumry();
            Assert.IsTrue(offerCode, "I should not see offer code on Activities Page");
        }

        [Then(@"I should not see offercode on in cost summary")]
        public void ThenIShouldNtseeValidOffCdOnCostSummary()
        {
            Boolean offerCode = Pages.Activities.verifyOffercodeNotOnCostSumry();
            Assert.IsTrue(offerCode, "I should not see offercode on in cost summary");
        }

        [Then(@"I should see valid suite title with Option Under cost summary")]
        public void ThenIShouldValidSuiteTitleNdOptionOnAct(Table table)
        {
            String suiteTitle = table.Rows[0]["Suite Option Only First Word"];
            Boolean suiteTitleBool = Pages.Activities.verifSuiteTitleOnActivity(suiteTitle);
            Assert.IsTrue(suiteTitleBool, "I should see valid suite name on Activities Page");
        }

        [When(@"I click on Continue to Dining Package button")]
        public void WhenIClickOnContinueToDiningPackage()
        {
            Pages.Activities.clickONContnueToDiningPckgBtn();
        }

        [Then(@"I should see Continue to Dining Package button")]
        public void ThenISeeShouldContinueToDiningPackage()
        {
            Boolean ContinueToDiningBtn = Pages.Activities.shouldSeeContnueToDiningPckgBtn();
            Assert.IsTrue(ContinueToDiningBtn, "I should see Continue to Dining Package button on Activity");
        }

        [Then(@"I should see LCO price is correct on Activity Page")]
        public void ThenISeeShouldCoorectLCOPrice()
        {
            Boolean LCOPrice = Pages.Activities.verifyLCOCostInSummary();
            Assert.IsTrue(LCOPrice, "I should see correct LCO price on Activity");
        }

        [Then(@"I should see ESAVER offer code is applied and displayed inside cost summary")]
        public void ThenISeeShouldSeeESaverOfferCode()
        {
            Boolean Result = Pages.Suite.verifyEsaverOfferCod();
            Assert.IsTrue(Result, "I should see ESAVER offer code is applied and displayed inside cost summary");
        }

        [Then(@"I should see MOREFUN offer code is applied and displayed inside cost summary")]
        public void ThenISeeShouldSeeMOREFUNOfferCode()
        {
            Boolean Result = Pages.Suite.verifyMoreFunOfferCod();
            Assert.IsTrue(Result, "I should see MOREFUN offer code is applied and displayed inside cost summary");
        }

    }
}
