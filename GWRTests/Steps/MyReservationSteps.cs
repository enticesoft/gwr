﻿using GWRFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace GWRTests.steps
{
    [Binding]
    public class MyReservationSteps
    {
        [When(@"I navigate on My Reservation Page")]
        public void WhenINavigateONMyReservationPage()
        {
            Pages.MyReserv.navigateOnMyReservation();
        }

        [When(@"I click on View Details button")]
        public void WhenIClickOnViewdetails()
        {
            Pages.MyReserv.clickOnViewDetails();
        }

        [Then(@"I should see user navigate on valid property")]
        public void ThenIShouldSeeValPropertyNavigation()
        {
            Boolean Result = Pages.MyReserv.verifyURlIsNavigatewithReservation();
            Assert.IsTrue(Result, "I should see user navigate on valid property");
        }
        

        [When(@"I click on Add A Package button")]
        public void WhenIClickOnAddAPckgBtn()
        {
            Pages.MyReserv.clickOnAddAPckgBtn();
        }

        [When(@"I click on Add A Pass button")]
        public void WhenIClickOnAddAPassBtn()
        {
            Pages.MyReserv.clickOnAddAPassBtn();
        }

        [When(@"I click on Add Cabana button")]
        public void WhenIClickOnAddCabanaBtn()
        {
            Pages.MyReserv.clickOnAddCabanaBtn();
        }

        [When(@"I click on Add Cabana button on Confirmation")]
        public void WhenIClickOnAddCabanaBtnOnConfrmtn()
        {
            Pages.MyReserv.clickOnAddCabanaBtnOnCofirmation();
        }
        

        [When(@"I click on Packages Add button")]
        public void WhenIClickOnAddBtn()
        {
            Pages.MyReserv.clickAddBtnFrsPckg();
        }

        [When(@"I click on Save to My Reservation button")]
        public void WhenIClickOnSavToResBtn()
        {
            Pages.MyReserv.clickOnSavetoResBtn();
        }

        [When(@"I click on Save to My Reservation button if cabana available")]
        public void WhenIClickOnSavToResBtnIfAvailbl()
        {
            Pages.MyReserv.clickOnSavetoResBtnIfAvlbl();
        }

        [When(@"I click on Indoor cabana view link")]
        public void WhenIClickOnIndoorCabanaViewLink()
        {
            Pages.MyReserv.indoorCabanaClickLink();
        }

        [When(@"I click on Indoor cabana view link and verify reservation number")]
        public void WhenIClickOnIndoorCabanaViewLinkResrNumber()
        {
            Pages.MyReserv.indoorCabanaClickLinkNdVerifyResNum();
        }

        [When(@"I click on Indoor cabana view link and verify cost summary if cabana is added")]
        public void WhenIClickOnIndoorCabanaViewLinkIfAdded()
        {
            Pages.MyReserv.indoorCabanaClickLinkIfAdded();
        }

        [When(@"I click on Outdoor cabana view link")]
        public void WhenIClickOnOutdoorCabanaViewLink()
        {
            Pages.MyReserv.outdoorCabanaClickLink();
        }



        [Then(@"I should see added packages notification message")]
        public void IshoudseeNotificationMessage()
        {
            Pages.MyReserv.veriAddedPackageNotificationMsg();
        }
        

        [When(@"I click on Activity Packages Add button")]
        public void WhenIClickOnAddBtnActivity()
        {
            Pages.MyReserv.clickAddBtnActivityPckg();
        }

        

        [Then(@"I should see valid reservation number")]
        public void ThenIShouldSeeValResNumber()
        {
            Boolean ResNumber = Pages.MyReserv.verifyReseNum();
            Assert.IsTrue(ResNumber, "I should see valid reservation number");
        }

        [Then(@"I should see valid Dining titles")]
        public void ThenIShouldSeeValDinignTitls()
        {
            Boolean firstTitle = Pages.MyReserv.verifyFirstDinignPckgsTitles();
            Assert.IsTrue(firstTitle, "I should see valid first Dinign title");

            Boolean secondTitle = Pages.MyReserv.verifySecondDinignPckgsTitles();
            Assert.IsTrue(secondTitle, "I should see valid second Dinign title");

            Boolean thirdTitle = Pages.MyReserv.verifyThirdDinignPckgsTitles();
            Assert.IsTrue(thirdTitle, "I should see valid third Dinign title");
        }

        [Then(@"I should see valid Activity Pass titles")]
        public void ThenIShouldSeeValActivityTitls()
        {
            Boolean firstActTitle = Pages.MyReserv.verifyFirstActivityPckgsTitles();
            Assert.IsTrue(firstActTitle, "I should see valid first Activity title");

            Boolean secondActTitle = Pages.MyReserv.verifySecondActivityPckgsTitles();
            Assert.IsTrue(secondActTitle, "I should see valid second Activity title");

            Boolean thirdActTitle = Pages.MyReserv.verifyThirdActivityPckgsTitles();
            Assert.IsTrue(thirdActTitle, "I should see valid third Activity title");
        }
        

        [Then(@"I should see valid Dining Package Cost")]
        public void ThenIShouldSeeValDinignCost()
        {
            Boolean firstCost = Pages.MyReserv.verifyFirstDinignPckgscost();
            Assert.IsTrue(firstCost, "I should see valid first Dinign Cost");

            Boolean secondCost = Pages.MyReserv.verifySecondDinignPckgscost();
            Assert.IsTrue(secondCost, "I should see valid second Dinign Cost");

            Boolean thirdCost = Pages.MyReserv.verifyThirdDinignPckgscost();
            Assert.IsTrue(thirdCost, "I should see valid third Dinign Cost");
        }

        [Then(@"I should see valid Acivity Packages Cost")]
        public void ThenIShouldSeeValActivityCost()
        {
            Boolean firstCost = Pages.MyReserv.verifyFirstActivityPckgscost();
            Assert.IsTrue(firstCost, "I should see valid first Activity pass Cost");

            Boolean secondCost = Pages.MyReserv.verifySecondActivityPckgscost();
            Assert.IsTrue(secondCost, "I should see valid second Activity pass Cost");

            Boolean thirdCost = Pages.MyReserv.verifyThirdActivityPckgscost();
            Assert.IsTrue(thirdCost, "I should see valid third Activity pass Cost");
        }

        

        [Then(@"I should see added packages titles are correct")]
        public void ThenIShouldSeeValAddedDinignTitls()
        {
            Boolean firstTitle = Pages.MyReserv.verifyAddedPackageFisrTitle();
            Assert.IsTrue(firstTitle, "I should see valid added first Dinign title");

            //Boolean secondTitle = Pages.MyReserv.verifyAddedPackageScndTitle();
            //Assert.IsTrue(secondTitle, "I should see valid added second Dinign title");
        }

        [Then(@"I should see added activity packages titles are correct")]
        public void ThenIShouldSeeValAddedActivityTitls()
        {
            Boolean firstTitle = Pages.MyReserv.verifyAddedActvityPackageFisrTitle();
            Assert.IsTrue(firstTitle, "I should see valid added first Activity title");

            Boolean secondTitle = Pages.MyReserv.verifyAddedActvityPackageScndTitle();
            Assert.IsTrue(secondTitle, "I should see valid added second Activity title");
        }



        [Then(@"I should see added packages cost are correct")]
        public void ThenIShouldSeeValAddedDinignCost()
        {
            Boolean firstTitle = Pages.MyReserv.verifyAddedPackagefirstCost(); 
            Assert.IsTrue(firstTitle, "I should see valid added first Dinign Cost");

            //Boolean secondTitle = Pages.MyReserv.verifyAddedPackagescndCost();
            //Assert.IsTrue(secondTitle, "I should see valid added second Dinign Cost");
        }

        [Then(@"I should see added different packages cost are correct")]
        public void ThenIShouldSeeValAddedDifferntDinignCost()
        {
            Boolean firstTitle = Pages.MyReserv.verifyAddedPckgTotal();
            Assert.IsTrue(firstTitle, "I should see added different packages cost are correct");

        }
        

        [Then(@"I should see added packages cost are correct for Activity Pass")]
        public void ThenIShouldSeeValAddedActivityCost()
        {
            Boolean firstTitle = Pages.MyReserv.verifyAddedActivityPackagefirstCost();
            Assert.IsTrue(firstTitle, "I should see valid added first activity Cost");

            Boolean secondTitle = Pages.MyReserv.verifyAddedActivityPackageScndCost();
            Assert.IsTrue(secondTitle, "I should see valid added Second activity Cost");
        }



        [Then(@"I should see correct suite total in cost summary of added packages")]
        public void ThenIShouldSeeValSuiteTotal()
        {
            Boolean firstTitle = Pages.MyReserv.verifyAddedPackagesSUiteTotal();
            Assert.IsTrue(firstTitle, "I should see correct suite total in cost summary of added packages");
        }

        [Then(@"I should see correct suite total in cost summary of added activity packages")]
        public void ThenIShouldSeeValSuiteTotalOfActivity()
        {
            Boolean suiteTotal = Pages.MyReserv.verifyAddedActivityPackagesSUiteTotal();
            Assert.IsTrue(suiteTotal, "I should see correct suite total in cost summary of added activity packages");
        }

        [Then(@"I should see correct suite total in cost summary of added cabana packages")]
        public void ThenIShouldSeeValSuiteTotalOfCabana()
        {
            Boolean suiteTotal = Pages.MyReserv.verifyAddedCabanaPackagesSUiteTotal();
            Assert.IsTrue(suiteTotal, "I should see correct suite total in cost summary of added cabana packages");
        }


        [Then(@"I should see correct total in cost summary of added packages")]
        public void ThenIShouldSeeValTotal()
        {
            Boolean firstTitle = Pages.MyReserv.verifyCostSummaryTotal();
            Assert.IsTrue(firstTitle, "I should see correct total in cost summary of added packages");
        }

        [Then(@"I should see correct total in cost summary of added Indoor cabana")]
        public void ThenIShouldSeeValCabanaTotal()
        {
            Boolean firstTitle = Pages.MyReserv.verifyCostSummaryTotalOfCabanaRes();
            Assert.IsTrue(firstTitle, "I should see correct total in cost summary of added Indoor cabana");
        }

        [Then(@"I should see correct total and due at check in at cost summary of added packages")]
        public void ThenIShouldSeeValTotalNdDueAtChin()
        {
            Boolean dueAtCheckin = Pages.MyReserv.verifyTotalandCheckInSame();
            Assert.IsTrue(dueAtCheckin, "I should see correct total and due at check in at cost summary of added packages");
        }

        

        [Then(@"I should see valid suite suite title")]
        public void ThenIShouldSeeValSuiteTitle()
        {
            Boolean suiteTitle = Pages.MyReserv.verifySuiteTitleAtCOst();
            Assert.IsTrue(suiteTitle, "I should see valid suite suite title");
        }

        [Then(@"I should see valid cabana titles")]
        public void ThenIShouldSeeValCabanaTitle()
        {
            Boolean indoor = Pages.MyReserv.indoorCabanaTitlVerify();
            Assert.IsTrue(indoor, "I should see valid Indoor cabana title");

            Boolean Outdoor = Pages.MyReserv.outdoorCabanaTitlVerify();
            Assert.IsTrue(Outdoor, "I should see valid Outdoor cabana title");
        }

        [Then(@"I should see valid cabana price")]
        public void ThenIShouldSeeValCabanaPrice()
        {
            Boolean indoorPrice = Pages.MyReserv.indoorCabanaPriceverify();
            Assert.IsTrue(indoorPrice, "I should see valid Indoor cabana price");

            Boolean outdoorPrice = Pages.MyReserv.outdoorCabanaPriceverify();
            Assert.IsTrue(outdoorPrice, "I should see valid Outdoor cabana price");
        }

        [Then(@"I should see valid cabana price in cost summarry")]
        public void ThenIShouldSeeValCabanaPriceAtCostSummry()
        {
            Boolean indoorPrice = Pages.MyReserv.indoorCabanaAddedPriceVeri();
            Assert.IsTrue(indoorPrice, "I should see valid Indoor cabana price in cost summarry");

            //Boolean outdoorPrice = Pages.MyReserv.outdoorCabanaAddedPriceVeri();
            //Assert.IsTrue(outdoorPrice, "I should see valid Outdoor cabana price in cost summarry");
        }
        

        [Then(@"I should see valid offer code in cost summary")]
        public void ThenIShouldSeeValValOfferCd()
        {
            Boolean offerCode = Pages.MyReserv.verifyValidOfferCode();
            Assert.IsTrue(offerCode, "I should see valid offer code in cost summary");
        }

        [Then(@"I should see valid reservation number on My reservation Page")]
        public void ThenIShouldSeeValResNum()
        {
            Boolean resNum = Pages.MyReserv.verifyResNumOnMyResPage();
            Assert.IsTrue(resNum, "I should see valid reservation number on My reservation Page");
        }

        [Then(@"I should see valid Check In date on My reservation Page")]
        public void ThenIShouldSeeValCheckInOnRes()
        {
            Boolean resNum = Pages.MyReserv.verifyCheckInDate();
            Assert.IsTrue(resNum, "I should see valid Check In date on My reservation Page");
        }

        [Then(@"I should see valid Check Out date on My reservation Page")]
        public void ThenIShouldSeeValCheckOutOnRes()
        {
            Boolean resNum = Pages.MyReserv.verifyCheckOutDate();
            Assert.IsTrue(resNum, "I should see valid Check Out date on My reservation Page");
        }

        [Then(@"I should see valid Guest count on My reservation Page")]
        public void ThenIShouldSeeValGuestOnRes()
        {
            Boolean resNum = Pages.MyReserv.verifyGuestCOunt();
            Assert.IsTrue(resNum, "I should see valid Guest count on My reservation Page");
        }        


        [Then(@"I should see Upcoming reservation val message")]
        public void ThenIShouldSeeUserMenuLinkAtTop()
        {
            Boolean updacomngValMsg = Pages.MyReserv.verifyMyUpcomingMsg();
            Assert.IsTrue(updacomngValMsg, "I should see Upcoming reservation val message");
        }

        [Then(@"I should see past reservation val message")]
        public void ThenIShouldSeePastResValMsg()
        {
            Boolean pstResValMsg = Pages.MyReserv.verifyPastResMsg();
            Assert.IsTrue(pstResValMsg, "I should see past reservation val message");
        }

        [Then(@"I should see cancelled reservation val message")]
        public void ThenIShouldSeeCanccelledResValMsg()
        {
            Boolean cancceldResValMsg = Pages.MyReserv.verifyCanclledResMsg();
            Assert.IsTrue(cancceldResValMsg, "I should see cancelled reservation val message");
        }

        [Then(@"I should see Find Your Reservation by Confirmation link")]
        public void ThenIShouldSeeFindYourResLink()
        {
            Boolean findResLink = Pages.MyReserv.verifyFindResLink();
            Assert.IsTrue(findResLink, "I should see Find Your Reservation by Confirmation link");
        }

        [Then(@"I should not see Find Your Reservation by Confirmation link")]
        public void ThenIShouldNotSeeFindYourResLink()
        {
            Boolean findResLink = Pages.MyReserv.verifyFindResLinkNotDisplayed();
            Assert.IsTrue(findResLink, "I should see not Find Your Reservation by Confirmation link and its should be navigate on reservation Detail page");
        }

        [When(@"I click on Find Your Reservation by Confirmation link")]
        public void ThenIClickFindYourResLink()
        {
            Pages.MyReserv.clickOnFindResLink();            
        }

        

        [Then(@"I should see Make Reservation link")]
        public void ThenIShouldSeeMakeResLink()
        {
            Boolean makeResLink = Pages.MyReserv.verifyMakeResLink();
            Assert.IsTrue(makeResLink, "I should see Make Reservation link");
        }

        [Then(@"I should not see Add LCO button on CMP as we added booking time")]
        public void ThenIShouldNotSeeLCOBtnAvailb()
        {
            Boolean makeResLink = Pages.MyReserv.checkLCOBtnAvailable();
            Assert.IsTrue(makeResLink, "I should not see Add LCO button on CMP as we added booking time.");
        }

        [Then(@"I should see Add LCO button on CMP as we not added at booking time")]
        public void ThenIShouldSeeLCOBtnAvailb()
        {
            Boolean makeResLink = Pages.MyReserv.checkLCOBtnAvailableAsNotAddedBookingTime();
            Assert.IsTrue(makeResLink, "I should see Add LCO button on CMP as we not added at booking time");
        }

        [Then(@"I should see 2 P.M. Check Out text as LCO added booking time")]
        public void ThenIShouldSee2PMLCOTxt()
        {
            Boolean makeResLink = Pages.MyReserv.check2PMLCOTxt();
            Assert.IsTrue(makeResLink, "I should not see Add LCO button on CMP as we added booking time.");
        }
        
        [When(@"I click on Make Reservation link")]
        public void clickONMakeResLink()
        {
            Pages.MyReserv.clickMakeResLink();            
        }

        [When(@"I added available Cabanas")]
        public void clickONAddCabana()
        {
            Pages.MyReserv.indoorCabanaaddCabana();
            //Pages.MyReserv.outdoorCabanaaddCabana();
        }

        [Then(@"I should not see Add LCO button on CMP as LCO not available at booking time")]
        public void ThenIShouldNotSeeLCOBtnAvailbOnCMP()
        {
            Boolean makeResLink = Pages.MyReserv.checkLCOBtnAvailableOrNotWhenLCONA();
            Assert.IsTrue(makeResLink, "I should not see Add LCO button on CMP as LCO not available at booking time");
        }

        [Then(@"I add LCO package and verify in cost sumarry")]
        public void IClickAddLCO()
        {
            //Pages.MyReserv.addLCOClickLink();
            Boolean LOCCost = Pages.MyReserv.addLCOClickLink();
            Assert.IsTrue(LOCCost, "Added LCO package cost should be display in cost summary");
        }


    }
}
