﻿using GWRFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public 
        class FooterSectionSteps
    {
        [When(@"I scroll down from top of page to bottom")]
        public void WhenIScrollDownFromPageToBottom()
        {
            Pages.Suite.waitforCheckin();
            Pages.PropertyHome.closeDealsSignUp();            
            Browser.scrollDownToBottomOfPage();
        }

        [When(@"I scroll down from top of page to bottom on any page")]
        public void WhenIScrollDownFromPageToBottomAnyPage()
        {
            //Pages.Suite.waitforCheckin();
            Pages.PropertyHome.closeDealsSignUp();
            Browser.scrollDownToBottomOfPage();
        }

        [When(@"I click on FAQ link")]
        public void WhenIClickOnFAQLink()
        {
           Pages.Footer.ClickOnFAQLink();
        }

        [When(@"I click on Customer Service link")]
        public void WhenIClickOnCustomerServiceLink()
        {
            Pages.Footer.ClickOnCustomerServiceLink();
        }

        [When(@"I click on Birthday Parties link")]
        public void WhenIClickOnBirthdayPartiesLink()
        {
            Pages.Footer.ClickOnBirthdayPartiesLink();
        }

        [When(@"I click on Group and Events link")]
        public void WhenIClickOnGroupAndEventsLink()
        {
            Pages.Footer.ClickOnGroupAndEventsLink();
        }
        
        [When(@"I click on Privacy Policy and Terms and Conditions link")]
        public void WhenIClickOnPrivacyPolicyAndTermsLink()
        {
            Pages.Footer.ClickOnTermsAndConditionLink();
        }

        [Then(@"I should navigate to Customer Service page")]
        public void ThenIShouldNavigateToCustomeServicePage()
        {
            Boolean CustomerServicePage = Pages.Footer.IsCustomerServicePageDisplayed();
            //Browser.CloseNewWindow();
            //Browser.SwitchToParentWindow();
            Assert.IsTrue(CustomerServicePage, "User navigates to Customer Service Page.");
        }

        [Then(@"I should navigate to FAQ page")]
        public void ThenIShouldNavigateOnFAQPage()
        {
            Boolean FAQPageDisplayed = Pages.Footer.IsFAQPageDisplayed();
            //Browser.CloseNewWindow();
            //Browser.SwitchToParentWindow();
            Assert.IsTrue(FAQPageDisplayed, "I navigates to FAQ Page.");
        }

        [Then(@"I should navigate to Birthday Parties page")]
        public void ThenIShouldNavigateOnBirthdayPartiesPage()
        {
            Boolean BirthdayPartiesPageDisplayed = Pages.Footer.IsBirthdayPartiesPageDisplayed();
            //Browser.CloseNewWindow();
            //Browser.SwitchToParentWindow();
            Assert.IsTrue(BirthdayPartiesPageDisplayed, "I navigates to Birthday Parties Page.");
        }

        [Then(@"I should navigate to Group and Events page")]
        public void ThenIShouldNavigateOnGroupAndEventsPage()
        {           
            Boolean GroupAndEventsPageDisplayed = Pages.Footer.IsGroupAndEventsPageDisplayed();
            //Browser.CloseNewWindow();
            //Browser.SwitchToParentWindow();
            Assert.IsTrue(GroupAndEventsPageDisplayed, "I should navigate to Group and Events page");
        }

        [Then(@"I should navigate to Privacy Policy and Terms and Conditions page")]
        public void ThenIShouldNavigateOnPrivacyPolicyAndTermsPage()
        {
            Boolean PrivacyPolicyAndTermsPageDisplayed = Pages.Footer.IsPrivacyPolicyAndTermsPageDisplayed();
            Assert.IsTrue(PrivacyPolicyAndTermsPageDisplayed, "I should navigate to Privacy Policy and Terms and Conditions page");
        }

        [Then(@"I should see Lead Gen Call out section is available")]
        public void ThenIShouldSeeLeadGenBtn()
        {
            Boolean result = Pages.Footer.verifyLeadGenButtonAtFooter();
            Assert.IsTrue(result, "I should see Lead Gen Call out section is available");
        }
        
        [When(@"I click on Save and Subscribe button")]
        public void WhenIClickOnSaveandSubscribebutton()
        {
            Pages.Footer.clickOnLeadGenButtonAtFooter();
        }

        [Then(@"I should see Lead Gen Pop Up Open")]
        public void ThenIShouldSeeLeadGenPopUp()
        {
            Boolean result = Pages.PropertyHome.IObserveLeadGenPopUp();
            Assert.IsTrue(result, "I should see Lead Gen Pop Up Open");
        }
        
    }
}
