﻿using GWRFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class NewConfirmationPageSteps
    {
        [When(@"I click on See You Soon Pop Up All Done button")]
        public void clickOnAllDoneBtn()
        {
            Pages.NewConfirmation.clickAllDone();
        }

        [Then(@"I should see New Confirmation Page Welcome Title")]
        public void ThenLandOnConfirmationPage()
        {
            Boolean CMPPage = Pages.NewConfirmation.newConfirmationPageDisplayed();            
            Assert.IsTrue(CMPPage, "I am on New Confirmation Page");
        }

        [Then(@"I should see Reservation Number")]
        public void ThenReservationNumberDisplayed()
        {
            Boolean resNum = Pages.NewConfirmation.reservationNumberDisplayed();
            Assert.IsTrue(resNum, "I can see Reservation Number");
        }

        [Then(@"I should see Cabana Section")]
        public void ThenCabanaSectionDisplayed()
        {
            Boolean cabanaSec = Pages.NewConfirmation.cabanaSectionDisplayed();
            Assert.IsTrue(cabanaSec, "I can see cabana section");
        }

        [When(@"I click on Cost Summarry button")]
        public void clickOnCostSummarryBtn()
        {
            Pages.NewConfirmation.clickCostSummaryBtn();
        }

        [Then(@"I should see correct Suite Total on Cost Summarry")]
        public void ThenCorrectSuiteTotalDisplayed()
        {
            Boolean suiteTotal = Pages.NewConfirmation.compareCMPAndPaymentSuiteTotalPrice();
            Assert.IsTrue(suiteTotal, "I can see correct suite total");
        }

        [Then(@"I should see correct Package Total on Cost Summarry")]
        public void ThenCorrectPackageTotalDisplayed()
        {
            Boolean packageTotal = Pages.NewConfirmation.compareCMPAndPaymentPackagesTotalPrice();
            Assert.IsTrue(packageTotal, "I can see correct package total");
        }

        [Then(@"I should see correct taxes price on Cost Summarry")]
        public void ThenCorrectTaxesDisplayed()
        {
            Boolean taxes = Pages.NewConfirmation.compareCMPAndPaymentTaxesPrice();
            Assert.IsTrue(taxes, "I can see correct taxes price");
        }

        [Then(@"I should see correct resort fee on Cost Summarry")]
        public void ThenCorrectResortFeeDisplayed()
        {
            Boolean resortFee = Pages.NewConfirmation.compareCMPAndPaymentResortFee();
            Assert.IsTrue(resortFee, "I can see correct resort fee");
        }

        [Then(@"I should see correct Total cost on Cost Summarry")]
        public void ThenCorrectTotalDisplayed()
        {
            Boolean total = Pages.NewConfirmation.compareCMPAndPaymentTotalCost();
            Assert.IsTrue(total, "I can see correct total");
        }

        [Then(@"I should see correct deposite paid on Cost Summarry")]
        public void ThenCorrectDepositePaidDisplayed()
        {
            Boolean depositePaid = Pages.NewConfirmation.compareCMPAndPaymentDepositePaid();
            Assert.IsTrue(depositePaid, "I can see correct deposite paid amount");
        }

        [Then(@"I should see correct Due at check in cost on Cost Summarry")]
        public void ThenCorrectDuestAtCheckInDisplayed()
        {
            Boolean dueAtCheckIn = Pages.NewConfirmation.compareCMPAndPaymentDueAtCheckin();
            Assert.IsTrue(dueAtCheckIn, "I can see correct due at check in amount");
        }

        [When(@"I click on cost summarry close link")]
        public void clickOnCloselink()
        {
            Pages.NewConfirmation.clickcloselink();
        }

        [Then(@"I should not see Late Check Out button for add on Confirmation page")]
        public void ThenLCONotDisaplyed()
        {
            Boolean LcoBtn = Pages.NewConfirmation.verifyLCOBtnNotAvailableOnConfrPage();
            Assert.IsTrue(LcoBtn, "I should not see Late Check Out button for add on Confirmation page");
        }


        [Then(@"I should see Daypass Reservation Number")]
        public void ThenDaypassReservationNumberDisplayed()
        {
            Boolean resNum = Pages.NewConfirmation.daypassreservationNumberDisplayed();
            Assert.IsTrue(resNum, "I can see DayPass Reservation Number");
        }

        [Then(@"I should see My Reservation Details Page Welcome Title")]
        public void ThenLandOnMyReservationDetailsPage()
        {
            Boolean CMPPage = Pages.NewConfirmation.newConfirmationPageDisplayed();
            Assert.IsTrue(CMPPage, "I am on My Reservation Details Page");
        }

        [When(@"I click on SEE MORE DETAILS link")]
        public void WhenIClickOnSEEMOREDETAILSLink()
        {
            Pages.NewConfirmation.IclickSeeMoreDetailsBtn();
        }
        [When(@"I Subscribe the SMS sign up")]
        public void WhenISubscribeTheSMSSignUp()
        {
            Pages.NewConfirmation.SubscribeSMSsignup();
        }
        [Then(@"I should see subscribe message")]
        public void ThenIShouldSeeSubscribeMessage()
        {
            Boolean ISSubscribe1 = Pages.NewConfirmation.IseeSubscribeMessage();
            Assert.IsTrue(ISSubscribe1, "I should SMS subscribed message");
        }
        [Then(@"I unsubscribe the SMS sign up")]
        public void ThenIUnsubscribeTheSMSSignUp()
        {
            Pages.NewConfirmation.IUnsubcribeSMSsignUp();
        }
        [Then(@"I should see unsubcribe message")]
        public void ThenIShouldSeeUnsubcribeMessage()
        {
            Boolean ISUnSubscribe1 = Pages.NewConfirmation.UnsubscribeMessageDisplay();
            Assert.IsTrue(ISUnSubscribe1, "I should SMS UNsubscribed message");
        }

        [Then(@"I should see Lodge Location tile")]
        public void ThenIShouldSeeLodgeLocTile()
        {
            Boolean LodgeLoc = Pages.AddAPackage.CheckLodgeLoctionDisplayed();
            Assert.IsTrue(LodgeLoc, "I should see Lodge Location tile");
        }

        [Then(@"I should see Select Dates tile")]
        public void ThenIShouldSeeSelectDateTile()
        {
            Boolean SlectDate = Pages.AddAPackage.CheckStayDatesDisplayed();
            Assert.IsTrue(SlectDate, "I should see Select Dates tile");
        }

        [Then(@"I should see Guest tile")]
        public void ThenIShouldSeeGuestTile()
        {
            Boolean GuestTile = Pages.AddAPackage.CheckGuestTileDisplayed();
            Assert.IsTrue(GuestTile, "I should see Guest tile");
        }

        [Then(@"I should see valid Adult and Kids count")]
        public void ThenIShouldSeeValAdltCnt(Table table)
        {
            String AdultCnt = table.Rows[0]["Adult"];
            Boolean GuestTile = Pages.AddAPackage.CheckAdultCountDisplayed(AdultCnt);
            Assert.IsTrue(GuestTile, "I should see valid Adult count");

            String KidCnt = table.Rows[0]["Kids"];
            Boolean KidTile = Pages.AddAPackage.CheckKidsCountDisplayed(KidCnt);
            Assert.IsTrue(KidTile, "I should see valid Kids count");
            
        }

        [Then(@"I should see Room type with suite name")]
        public void ThenIShouldSeeRoomTypeSuiteName()
        {
            Boolean GuestTile = Pages.AddAPackage.CheckRoomTypeSuiteName();
            Assert.IsTrue(GuestTile, "I should see Room type with suite name");
        }

        [Then(@"I should see valid reservation number on Add A Package page")]
        public void ThenIShouldSeeRResNumOnAddPkg()
        {
            Boolean ResrNum = Pages.AddAPackage.CheckValResNumOnAddPCkGBtn();
            Assert.IsTrue(ResrNum, "I should see valid reservation number on Add A Package page");
        }

        [Then(@"I should see valid suite name on Add A Package page")]
        public void ThenIShouldSeeSUiteNameOnAddPkg()
        {
            Boolean ValSuitName = Pages.AddAPackage.CheckValSuiteName();
            Assert.IsTrue(ValSuitName, "I should see valid suite name on Add A Package page");
        }

        [Then(@"I should see correct total in cost summary of added birthday packages")]
        public void ThenIShouldSeeValTotal()
        {
            Boolean totalCost = Pages.NewConfirmation.birthDayPckgTotal();
            Assert.IsTrue(totalCost, "I should see correct total in cost summary of added birthday packages");
        }

        [Then(@"I should see valid Adult count on Add Package Page")]
        public void ThenIShouldSeeValGuestCnt(Table table)
        {
            String AdultCnt = table.Rows[0]["Adult"];
            Boolean GuestCnt = Pages.AddAPackage.CheckValGuestCnt(AdultCnt);
            Assert.IsTrue(GuestCnt, "I should see valid Adult count on Add Package Page");
        }

        [Then(@"I should see valid Check Out time on Add Package Page")]
        public void ThenIShouldSeeChOuttimeOnAddPkg()
        {
            Boolean ChkOutCnt = Pages.AddAPackage.CheckCheckOutCnt();
            Assert.IsTrue(ChkOutCnt, "I should see valid Adult count on Add Package Page");
        }

        [When(@"I should see Day pass Reservation Number")]
        public void ThenDaypassReserNumberDisplayed()
        {
            Boolean resNum = Pages.NewConfirmation.daypassreservationNumberDisplayed();
            Assert.IsTrue(resNum, "I can see DayPass Reservation Number");
        }

        [When(@"I should see suite Reservation Number")]
        public void WhenReservationNumberDisplayed()
        {
            Boolean resNum = Pages.NewConfirmation.reservationNumberDisplayed();
            Assert.IsTrue(resNum, "I can see Reservation Number");
        }

        [When(@"I should Navigate on cabana Reservation page by click on caban view link")]
        public void WhenINavigateonCabanaReservationPage()
        {
            Pages.NewConfirmation.NaviagteOnCabanaReservationPage();
        }

        [Then(@"I should see Dining Package tile on Confirmation page")]
        public void ThenIShouldSeeDiningPckgTile()
        {
            Boolean DingPckTile = Pages.NewConfirmation.DiningPackageTile();
            Assert.IsTrue(DingPckTile, "I should see correct dining package title on tile");

            //Boolean DingPckTileCount = Pages.NewConfirmation.DiningPackagesAddedTileCount();
            //Assert.IsTrue(DingPckTileCount, "I should see correct dining packages count on tile");
        }

        [Then(@"I should see Cabana tile on Confirmation page")]
        public void ThenIShouldSeeCabanaTile()
        {
            Boolean CabanaTile = Pages.NewConfirmation.CabanaTile();
            Assert.IsTrue(CabanaTile, "I should see Cabana tile on Confirmation page");
        }
        

        [Then(@"I should see Attraction Package tile on Confirmation page")]
        public void ThenIShouldSeeAttractionPckgTile()
        {
            Boolean AttractionPckTile = Pages.NewConfirmation.AttractionPackageTile();
            Assert.IsTrue(AttractionPckTile, "I should see correct attraction package title on tile");
        }

        [Then(@"I should see Birthday Package tile on Confirmation page")]
        public void ThenIShouldSeeBirthdayPckgTile()
        {
            Boolean BirthdayPckTile = Pages.NewConfirmation.BirthdayPackageTile();
            Assert.IsTrue(BirthdayPckTile, "I should see Birthday Package tile on Confirmation page");
        }
        

        [When(@"I click on Add More Pass button")]
        public void WhenIclickOnAddMorePassBtn()
        {
            Pages.NewConfirmation.IclickAddMorePassesBtn();
        }

        [When(@"I click on See All Includes link")]
        public void WhenIclickOnSellAllIncludeslink()
        {
            Pages.NewConfirmation.clickOnSeeAllIncludeLink();
        }

        [When(@"I click on See What You Get link")]
        public void WhenIclickOnSeeWhatYoulink()
        {
            Pages.NewConfirmation.clickOnSeeSeeWhatYouGetLink();
        }
        

        [When(@"I click on Add A Package button of Dining tile")]
        public void WhenIclickOnAddAPckgBtn()
        {
            Pages.NewConfirmation.IclickAddAPackageDingBtn();
        }

        [When(@"I click on Add A Package button of Birthday tile")]
        public void WhenIclickOnAddAPckgBtnOfBirtDay()
        {
            Pages.NewConfirmation.IclickAddAPackageBirthDayBtn();
        }
        

        [When(@"I click on Add A Cabana button of Cabana tile")]
        public void WhenIclickOnCabanaBtn()
        {
            Pages.NewConfirmation.IclickAddACabanaBtn();
        }

        [When(@"I click on Add buttons of Birthday Packages")]
        public void WhenIclickOnBirthdayBtn()
        {
            Pages.NewConfirmation.IclickAddBtnOfBirthDayPckg();
        }
        

        [When(@"I click on Add button of pass")]
        public void WhenIclickOnAddBtn()
        {
            Pages.NewConfirmation.IclickAddBtnFirstPass();
        }

        [Then(@"I should see correct cost and package title of first dining package")]
        public void ThenIShouldSeeFirstCostNdTitle()
        {
            Boolean DingFrsTitle = Pages.NewConfirmation.VerifyPackgesFirstTitle();
            Assert.IsTrue(DingFrsTitle, "I should see correct title of package");

            Boolean DingFrsCost = Pages.NewConfirmation.VerifyPackgesFirstCost();
            Assert.IsTrue(DingFrsCost, "I should see correct cost of package");
        }

        [Then(@"I should see correct cost and package title of second dining package")]
        public void ThenIShouldSeeScndCostNdTitle()
        {
            Boolean DingFrsTitle = Pages.NewConfirmation.VerifyPackgesSecndTitle();
            Assert.IsTrue(DingFrsTitle, "I should see correct title of second package");

            Boolean DingFrsCost = Pages.NewConfirmation.VerifyPackgesSecndCost();
            Assert.IsTrue(DingFrsCost, "I should see correct cost of second package");
        }

        [Then(@"I should see 'Attraction', 'Dining', 'Birthday' and 'Cabana' tiles on Confirmation page")]
        public void ThenIShouldSeePackagesTiles()
        {
            Boolean Result = Pages.NewConfirmation.DisplayPackagesTiles();
            Assert.IsTrue(Result, "I should see 'Attraction', 'Dining', 'Birthday' and 'Cabana' tiles on Confirmation page");
        }
              

        [When(@"I click on learn more box close icon")]
        public void clickOnlearnMoreCloseIcon()
        {
            Pages.NewConfirmation.clickOnlearnMoreBoxCloseIcn();
        }

        [Then(@"I should not see learn more box")]
        public void ThenIShouldNotSeenLearnMoreBox()
        {
            Boolean learnmoreBox = Pages.NewConfirmation.verifylearnMoreBoxCloseIcn();
            Assert.IsFalse(learnmoreBox, "I should not see learn more box");
        }

        [Then(@"I should see Dining package tile under Add Ons section")]
        public void ThenIShouldSeeDiningPckTile()
        {
            Boolean DiningPackageTite = Pages.NewConfirmation.verifyDiningPckgTileUnderAddOns();
            Assert.IsTrue(DiningPackageTite, "I should see Dining package tile under Add Ons");
        }

        [Then(@"I should see Birthday package tile under Add Ons section")]
        public void ThenIShouldSeeBirthdayPckTile()
        {
            Boolean BirhtdayPackageTite = Pages.NewConfirmation.verifyBirhtdayPckgTileUnderAddOns();
            Assert.IsTrue(BirhtdayPackageTite, "I should see Birthday package tile under Add Ons");
        }

        [Then(@"I should see Attraction passes tile under Add Ons section")]
        public void ThenIShouldSeeAttractionPassTile()
        {
            Boolean AttractionPassTile = Pages.NewConfirmation.verifyAttractionPassesTileUnderAddOns();
            Assert.IsTrue(AttractionPassTile, "I should see Attraction passes tile under Add Ons section");
        }

        [Then(@"I should see Attraction Pass Details section")]
        public void ThenIShouldSeeAttractionPassDetailsSec()
        {
            Boolean AttractionPassDetails = Pages.NewConfirmation.verifyPassDetailsShouldbeDispalyed();
            Assert.IsTrue(AttractionPassDetails, "I should see Attraction Pass Details section");
        }
        
        [Then(@"I should see Attraction Pass Price section")]
        public void ThenIShouldSeeAttractionPassPriceSec()
        {
            Boolean AttractionPassPrice = Pages.NewConfirmation.verifyPassPriceSecShouldbeDispalyed();
            Assert.IsTrue(AttractionPassPrice, "I should see Attraction Pass Price section");
        }

        [Then(@"I should see Add Package Price section")]
        public void ThenIShouldSeeAddPcakagePriceSec()
        {
            Boolean AddAPackagePrice = Pages.NewConfirmation.verifyPackagePriceSecShouldbeDispalyed();
            Assert.IsTrue(AddAPackagePrice, "I should see Add Package Price section");
        }
        

        [Then(@"I should see Image section on Modal")]
        public void ThenIShouldSeeAttractionPassImageSec()
        {
            Boolean AttractionPassImage = Pages.NewConfirmation.verifyPassImageSecShouldbeDispalyed();
            Assert.IsTrue(AttractionPassImage, "I should see Attraction Pass Image section");
        }

        [Then(@"I should see Add Package Details section")]
        public void ThenIShouldSeeAddPassDetailsSec()
        {
            Boolean AddPassDetails = Pages.NewConfirmation.verifyAddPackageDetailsShouldbeDispalyed();
            Assert.IsTrue(AddPassDetails, "I should see Add Package Details section");
        }

        [Then(@"I should see Count should be match")]
        public void ThenIShouldSeeNoOfDaysCount()
        {
            Boolean Result = Pages.NewConfirmation.IObserveNoOfDays();
            Assert.IsTrue(Result, "I should see Count should be match");
        }

        [Then(@"I should see Room Type section on Confimation Page")]
        public void ThenIShouldSeeRoomTypeSec()
        {
            Boolean Result = Pages.NewConfirmation.verifyRoomTypeSectionDispalyed();
            Assert.IsTrue(Result, "I should see Room Type section on Confimation Page");
        }

        [Then(@"I should see Room Type price section on Confimation Page")]
        public void ThenIShouldSeeRoomTypePriceSec()
        {
            Boolean Result = Pages.NewConfirmation.verifyRoomTypePriceSectionDispalyed();
            Assert.IsTrue(Result, "I should see Room Type price section on Confimation Page");
        }
        
        [Then(@"I should see Upgrade Now button on Confimation Page")]
        public void ThenIShouldSeeUpgradeNowBtn()
        {
            Boolean Result = Pages.NewConfirmation.verifyRoomTypeUpgradeNowDispalyed();
            Assert.IsTrue(Result, "I should see Upgrade Now button on Confimation Page");
        }

        [When(@"I click on Upgrade Now button")]
        public void clickOnUpgradeNowButton()
        {
            Pages.NewConfirmation.clickOnRoomTypeUpgradeNow();
        }

        [Then(@"I should see Nor upgrade page")]
        public void ThenIShouldSeenorUpgradePage()
        {
            Boolean Result = Pages.NewConfirmation.VerifyNewNorPage();
            Assert.IsTrue(Result, "I should see Nor upgrade page");
        }

        [Then(@"I should see Added Packages cost summary into email")]
        public void ThenIShouldGetAddAPackageEmail()
        {
            String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
            Boolean emailReceived = ReadEmail.CheckEmail.CheckCostSummaryOfAddedPackages(emailMaxCounter);
            Assert.IsTrue(emailReceived, "I should see Added Packages cost summary into email");
        }
        
        [Then(@"I should see Added Cabana Packages cost summary into email")]
        public void ThenIShouldGetAddCabanaPackageEmail()
        {
            String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
            Boolean emailReceived = ReadEmail.CheckEmail.CheckCostSummaryOfAddedPackagesForCabana(emailMaxCounter);
            Assert.IsTrue(emailReceived, "I should see Added Cabana Packages cost summary into email");
        }

        [Then(@"I should see correct Suite total on email")]
        public void ThenIShouldSeeCrctSuiteTotal()
        {
            Boolean Result = Pages.NewConfirmation.compareSuiteTotalAndEmailSuitetotal();
            Assert.IsTrue(Result, "I should see correct Suite total on email");
        }

        [Then(@"I should see correct Suite price on email")]
        public void ThenIShouldSeeCrctSuiteprice()
        {
            Boolean Result = Pages.NewConfirmation.compareSuitepriceAndEmailSuitePrice();
            Assert.IsTrue(Result, "I should see correct Suite price on email");
        }

        [Then(@"I should see correct Package total on email")]
        public void ThenIShouldSeeCrctPackageTotal()
        {
            Boolean Result = Pages.NewConfirmation.comparePackageTotalAndEmailPackageTotal();
            Assert.IsTrue(Result, "I should see correct Package total on email");
        }

        [When(@"I should observe Cost summary on CMP Page")]
        public void ThenIShouldSeeCostSumryPriceONCMP()
        {
            Pages.NewConfirmation.GetPricesfromCostSummary();
        }

        [When(@"I should observe Cabana cost summary on CMP Page")]
        public void ThenIShouldSeeCabanaCostSumryPriceONCMP()
        {
            Pages.NewConfirmation.GetPricesfromCostSummaryForCabana();
        }

        [When(@"I should click on See More Button of Special Request Celebration section")]
        public void ThenIShouldclickonSpecialReqCelBtn()
        {
            Pages.NewConfirmation.clickOnSeeMoreSpecialReqBtn();
        }

        [Then(@"I should see Special request & celebration pop is open")]
        public void ThenIShouldSeSpeclReqCelebratnPopUp()
        {
            Boolean Result = Pages.NewConfirmation.specialrePopTitleverify();
            Assert.IsTrue(Result, "I should see Special request & celebration pop is open");
        }

        [Then(@"I should see Request message added on My reservation")]
        public void ThenIShouldSeeRequestMsg()
        {
            Boolean Result = Pages.NewConfirmation.verifyRequestmessageAfterAdded();
            Assert.IsTrue(Result, "I should see Request message added on My reservation");
        }
        
        [Then(@"I should see First name of celeration person added")]
        public void ThenIShouldSeeFirstNameAdded()
        {
            Boolean Result = Pages.NewConfirmation.verifyFirstNamesIsAdded();
            Assert.IsTrue(Result, "I should see First name of celeration person added");
        }
        
        [Then(@"I should see Second name of celeration person added")]
        public void ThenIShouldSeeScndtNameAdded()
        {
            Boolean Result = Pages.NewConfirmation.verifySecndNamesIsAdded();
            Assert.IsTrue(Result, "I should see Second name of celeration person added");
        }

        [Then(@"I should see Top floor check box is checked")]
        public void ThenIShouldSeeTopFloorCheckBoxIsChecked()
        {
            Boolean Result = Pages.NewConfirmation.IsTopfloorCheckBoxChecked();
            Assert.IsTrue(Result, "I should see Top floor check box is checked");
        }

        [When(@"I should click on Close to elevator checkbox")]
        public void ThenIShouldclickoClsToElevtrChkBx()
        {
            Pages.NewConfirmation.clickOnCloseToEvltrCheckBx();
        }

        [When(@"I should click on Close to elevator checkbox if it is not select already")]
        public void ThenIShouldclickoIfCloseElvtrChkBxIfNotSelect()
        {
            Pages.NewConfirmation.IsElvtrCloseCheckBoxChecked();
        }

        [When(@"I should click on Top floor checkbox")]
        public void ThenIShouldclickoTopFloorChkBx()
        {
            Pages.NewConfirmation.clickOnTopFloorCheckBx();
        }
        

        [When(@"I should click on Save to My Reservatio button")]
        public void ThenIShouldclickoMyReservationBtn()
        {
            Pages.NewConfirmation.clickOnAddToMyreservationBtn();
        }

        [When(@"I should click on Birthday Checkbox")]
        public void ThenIShouldclickonBirthDayCheckBox()
        {
            Pages.NewConfirmation.clickOnBirthDayCheckBtn();
        }

        
        [When(@"I should send name of celebrating person")]
        public void ThenIShouldSendCelebratingPersonName()
        {
            Pages.NewConfirmation.SendNamePersonOfCelebrating();
        }
        
        [When(@"I should send second name of celebrating person")]
        public void ThenIShouldSendScndCelebratingPersonName()
        {
            Pages.NewConfirmation.SendScndNamePersonOfCelebrating();
        }

        [When(@"I should Delete celebrating person name")]
        public void ThenIShouldDeleteCelebratingPersonName()
        {
            Pages.NewConfirmation.DeleteCelebratyName();
        }

                
        [Then(@"I should see name is deleted.")]
        public void ThenIShouldSeNameDeleted()
        {
            Boolean Result = Pages.NewConfirmation.verifyNameIsDeleteOrNot();
            Assert.IsTrue(Result, "I should see name is deleted.");
        }

        
        [Then(@"I should see Online Check-in will be available 24 hours prior to your reservation Message")]
        public void ThenIShouldSeCheCkInMSG()
        {
            Boolean Result = Pages.NewConfirmation.onLinecheckInMSGOnConfirmPage();
            Assert.IsTrue(Result, "I should see Online Check-in will be available 24 hours prior to your reservation Message");
        }

        [When(@"I should click on Add Another Person link")]
        public void IclickOnAddAnotherPesonLink()
        {
            Pages.NewConfirmation.clickOnAddAnotherPerson();
        }

        [Then(@"I should see Special request options")]
        public void ThenIShouldSeSpeclReqOption()
        {
            Boolean Result = Pages.NewConfirmation.closeToElevatrOptionverify();
            Assert.IsTrue(Result, "I should see close to elevator option");

            Boolean Result1 = Pages.NewConfirmation.closeToWaterprkOptionverify();
            Assert.IsTrue(Result1, "I should see close to waterpark option");

            Boolean Result2 = Pages.NewConfirmation.FarFormElvatorOptionverify();
            Assert.IsTrue(Result2, "I should see Far from Elevator option");

            Boolean Result3 = Pages.NewConfirmation.LobyLevelOptionverify();
            Assert.IsTrue(Result3, "I should see Lobby Level option");

            Boolean Result4 = Pages.NewConfirmation.PlayAnParkOptionverify();
            Assert.IsTrue(Result4, "I should see Play an Park option");

            Boolean Result5 = Pages.NewConfirmation.TopfloorOptionverify();
            Assert.IsTrue(Result5, "I should see Top floor option");

            Boolean Result6 = Pages.NewConfirmation.OtherOptionverify();
            Assert.IsTrue(Result6, "I should see Other option");
        }
    }

//}
}
