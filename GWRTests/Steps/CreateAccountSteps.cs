﻿using GWRFramework;
using NUnit.Framework;
using System;
using System.Configuration;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace GWRTests.Steps
{
    [Binding]
    public class CreateAccountSteps
    {
        [Given(@"I am on Property Home page")]
        public void GivenIAmOnPropertyHomePage()
        {
           Browser.NavigateTo();
           //Pages.GlobalHome.clickChooseLocationBtn();
           Pages.GlobalHome.selectProperty();
           Pages.Suite.clickCookiesClose();
        }

        [When(@"I click on Sign In link under header section")]
        public void WhenIClickOnSignInLinkUnderHeaderSection()
        {
            Pages.PropertyHome.closeDealsSignUp();
            Pages.PropertyHome.clickSignIn();
        }
        
        [When(@"I click on Create an Account button")]
        public void WhenIClickOnCreateAnAccountButton()
        {
            Pages.SignIn.clickCreateAccountBtn();
        }

        [When(@"I wait for Led Gen popup to appear")]
        public void WhenIWaitForLeadGenPopup()
        {
            Pages.PropertyHome.waitForDealsSignUpPopup();
        }

        [When(@"I entered valid data on the create account popup")]
        public void WhenIEnterValidDataOnCreateAnAccountPopup(Table table)
        {
            String firstName = table.Rows[0]["First Name"];
            Pages.CreateAccount.enterFirstNameTextbox(firstName);

            String lastName = table.Rows[0]["Last Name"];
            Pages.CreateAccount.enterLastNameTextbox(lastName);

            //String postalCode = table.Rows[0]["Postal Code"];
            Pages.CreateAccount.enterPostalCodeTextbox();

            String emailId = Generator.EmailAddress.GenerateEmail(table.Rows[0]["Email"]);
            Pages.CreateAccount.enterEmailIdTextbox(emailId);

            String password = table.Rows[0]["Password"];
            Pages.CreateAccount.enterPasswordTextbox(password);
        }

        [When(@"I enter valid data on the Led Gen popup")]
        public void WhenIEnterValidDataOnLeadGenPopup(Table table)
        {
            String emailId = Generator.EmailAddress.GenerateEmail(table.Rows[0]["Email"]);
            Pages.DealSignUp.enterEmailIdTextbox(emailId);

            String firstName = table.Rows[0]["First Name"];
            Pages.DealSignUp.enterFirstNameTextbox(firstName);

            String lastName = table.Rows[0]["Last Name"];
            Pages.DealSignUp.enterLastNameTextbox(lastName);

            Pages.DealSignUp.enterPostalCodeTextbox();
        }

        [When(@"I click on Create button")]
        public void WhenIClickOnCreateButton()
        {
            Pages.CreateAccount.clickCreateButton();
        }

        [When(@"I click on Sign Up button")]
        public void WhenIClickOnSignUpButton()
        {
            Pages.DealSignUp.clickSignUpButton();
        }

        [Then(@"I should see User menu link at top")]
        public void ThenIShouldSeeUserMenuLinkAtTop()
        {
            Boolean signInMenu = Pages.PropertyHome.checkUserSingedInMenu();
            Assert.IsTrue(signInMenu, "User able to see Menu link at the top.");

            Pages.PropertyHome.clickToSignOut();
        }

        [Then(@"I should see validation message.")]
        public void ThenIShouldSeeValMessage()
        {
            Boolean signInMenu = Pages.PropertyHome.loginFailedValMessage();
            Assert.IsTrue(signInMenu, "I should see validation message.");

        }

        [Then(@"I should see Success message")]
        public void ThenIShouldSeeSuccessMsg()
        {
            Boolean successMsg = Pages.DealSignUp.checkSuccessMsg();
            Assert.IsTrue(successMsg, "User sees succcessful message.");

            Pages.DealSignUp.clickAllDoneButton();
        }

        [Then(@"I should receive GWR Welcome email")]
        public void ThenIShouldReceiveGWRWelcomeEmail()
        {
           String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
           Boolean emailReceived = ReadEmail.CheckEmail.CheckWelcomeEmail(emailMaxCounter);
           //Boolean emailReceived = ReadEmail.CheckEmail.CheckWelcomeEmail("120");
            Assert.IsTrue(emailReceived, "User received Welcome Email from GWR.");
        }
                

        [When(@"I click on Sign In link on 'CREATE AN ACCOUNT' pop up")]
        public void WhenIClickOnSignInLinkOnCeateAccount()
        {
            Pages.CreateAccount.clickSignInLinkOnCreateAccount();
        }

        [Then(@"I should navigate 'Sign In' pop up")]
        public void ThenIShouldNavigateSignInPopUp()
        {

            Boolean SignInPopUpDisplayed = Pages.CreateAccount.IsSignInPopUpDispalyed();
            Assert.IsTrue(SignInPopUpDisplayed, "I should navigate 'Sign In' pop up");
        }

        [When(@"I entered Email and Password on the create account popup")]
        public void WhenIEnterEmailAndPassword(Table table)
        {
            String emailId = Generator.EmailAddress.GenerateEmail(table.Rows[0]["Email"]);
            Pages.CreateAccount.enterEmailIdTextbox(emailId);

            String password = table.Rows[0]["Password"];
            Pages.CreateAccount.enterPasswordTextbox(password);

        }

        [Then(@"I should see that 'Create' button get enabled")]
        public void ThenIShouldSeeCreateButtonGetEnabled()
        {
            Boolean CreateButtonEnabled = Pages.CreateAccount.IsCreateButtonEnabled();
            Assert.IsTrue(CreateButtonEnabled, "I should see that 'Create' button get enabled");
        }


        [When(@"I enter Postal code Under postal code field")]
        public void WhenIEnterPostalCodeUnderPostalCodeField(Table table)
        {

            String PostalCode = table.Rows[0]["Postal Code"];
            Pages.CreateAccount.EnterPostalCode(PostalCode);
        }

        [Then(@"I should see 'Privacy Policy' & 'Contact Us' links displayed on create account pop up")]
        public void ThenIShouldSeePrivacyPolicyAndContactUsLinks()
        {
            Boolean PrivacyPolicyAndContactUsLinksDisplayed = Pages.CreateAccount.IsPrivacyPolicyAndContactUsLinksDisplayed();
            Assert.IsTrue(PrivacyPolicyAndContactUsLinksDisplayed, "I should see 'Privacy Policy' & 'Contact Us' links displayed on create account pop up");
        }

        [Then(@"I should able to see 'I agree to receive Great Wolf Resorts' check box")]
        public void ThenIShouldSeeIAgreeToReceiveGreatWolfResortCheckBox()
        {
            Boolean IAgreeToReceiveGreatWolfResortCheckBoxDisplayed = Pages.CreateAccount.IsIAgreeToReceiveGreatWolfResortCheckBoxDisplayed();
            Assert.IsTrue(IAgreeToReceiveGreatWolfResortCheckBoxDisplayed, "I should able to see 'I agree to receive Great Wolf Resorts' check box");
        }

        [When(@"I am sign out from application")]
        public void WhenISignOut()
        {
            Boolean signInMenu = Pages.PropertyHome.checkUserSingedInMenu();
            Pages.PropertyHome.clickToSignOut();
        }

        [Then(@"I should see Privacy policy link")]
        public void ThenIShouldSeePrivacyPolicyLink()
        {
            Boolean candianPrivacyPolicy = Pages.CreateAccount.IsPrivacyPolicyDispalyed();
            Assert.IsTrue(candianPrivacyPolicy, "I should see Privacy policy link");
        }

        [Then(@"I should see Contact Us link")]
        public void ThenIShouldSeeContactUsLink()
        {
            Boolean candianContactUs = Pages.CreateAccount.IsContactUsDispalyed();
            Assert.IsTrue(candianContactUs, "I should see Contact Us link");
        }

        [When(@"I Sign Up with valid details using google sign up")]
        public void WhenIEnterValidLoginDetailsOnGoogle(Table table)
        {
            String email = table.Rows[0]["Email"];
            String password = table.Rows[0]["Password"];
            Pages.SignIn.completeSignUpStepsOnGoogle(email, password);
        }
    }
}
