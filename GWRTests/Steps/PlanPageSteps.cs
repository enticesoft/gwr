﻿using GWRFramework;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class PlanPageSteps
    {

        [Given(@"I relaunch browser and navigate to Property Home page")]
        public void GivenIRelaunchBrowserNNavToPropertyHomePage()
        {
            Browser.NavigateTo();           
            //Pages.GlobalHome.clickChooseLocationBtn();
            Pages.GlobalHome.selectProperty();            
        }

        [Given(@"I navigate to Property plan page")]
        public void GivenINavToPropertyPlanPage()
        {
            //Browser.NavigateTo();
            //Pages.GlobalHome.clickChooseLocationBtn();
            Pages.GlobalHome.NavigateOnPlanProperty();
        }

        

        [Given(@"I Open New browser and navigate to Property Home page")]
        public void GivenIOpenBrowserNNavToPropertyHomePage()
        {
            Browser.Close();
            Browser.LaunchBrowser();            
            Browser.NavigateTo();
            //Pages.GlobalHome.clickChooseLocationBtn();
            Pages.GlobalHome.selectProperty();
            Pages.PropertyHome.waitForDealsSignUpPopup();
            Pages.PropertyHome.closeDealsSignUp();
        }

        [Given(@"I Open New browser and navigate to Property Home page and wait for Lead Gen Pop up")]
        public void GivenIOpenBrowserNNavToPropertyHomePageForLeadGen()
        {
            Browser.Close();
            Browser.LaunchBrowser();
            Browser.NavigateTo();
            //Pages.GlobalHome.clickChooseLocationBtn();
            Pages.GlobalHome.selectProperty();
            Pages.PropertyHome.waitForDealsSignUpPopup();
            //Pages.PropertyHome.closeDealsSignUp();
        }

        
        [When(@"I navigate to Gurnee property page")]
        public void GivenINavToPropertyGurnnePage()
        {
            Pages.GlobalHome.selectPropertyandNavigateOnGurnee();
        }



        [Then(@"I should see not see Lead Gen Modal Pop Up")]
        public void ThenIShouldNotSeeLeadGenPopUp()
        {
            Boolean validation = Pages.PropertyHome.shouldNotSeeDealsSignUpPopUp();
            Assert.IsTrue(validation, "I should see not see Lead Gen Modal Pop Up");
        }


        [When(@"I enter valid details on Booking Widget with dates more than 60 days")]
        public void WhenIEnterValidDetailsOnBookingWidgetWithDates60DaysLater(Table table)
        {
            String futureCheckInDate = DateTime.Today.AddDays(80).ToString("MM/dd/yyyy");
            String newFuturedate = futureCheckInDate.Replace("-", "/");
            Pages.PropertyHome.enterCheckInDate(newFuturedate);

            String futureCheckoutDate = DateTime.Today.AddDays(82).ToString("MM/dd/yyyy");
            String newFutureOutdate = futureCheckoutDate.Replace("-", "/");
            Pages.PropertyHome.clearCheckOutDate();
            Pages.PropertyHome.enterCheckOutDate(newFutureOutdate);

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

        }

        [When(@"I enter valid details on Booking Widget with dates more than 60 days with 3 nights stay")]
        public void WhenIEnterValidDetailsOnBookingWidgetWithDates60DaysLater3NightsStay(Table table)
        {
            String futureCheckInDate = DateTime.Today.AddDays(80).ToString("MM/dd/yyyy");
            String newFuturedate = futureCheckInDate.Replace("-", "/");
            Pages.PropertyHome.enterCheckInDate(newFuturedate);

            String futureCheckoutDate = DateTime.Today.AddDays(84).ToString("MM/dd/yyyy");
            String newFutureOutdate = futureCheckoutDate.Replace("-", "/");
            Pages.PropertyHome.clearCheckOutDate();
            Pages.PropertyHome.enterCheckOutDate(newFutureOutdate);

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

        }

        [When(@"I enter valid details on Booking Widget with dates less than 60 days with 3 nights stay")]
        public void WhenIEnterValidDetailsOnBookingWidgetWithDates60DaysBefore3NightsStay(Table table)
        {
            String futureCheckInDate = DateTime.Today.AddDays(40).ToString("MM/dd/yyyy");
            String newFuturedate = futureCheckInDate.Replace("-", "/");
            Pages.PropertyHome.enterCheckInDate(newFuturedate);

            String futureCheckoutDate = DateTime.Today.AddDays(44).ToString("MM/dd/yyyy");
            String newFutureOutdate = futureCheckoutDate.Replace("-", "/");
            Pages.PropertyHome.clearCheckOutDate();
            Pages.PropertyHome.enterCheckOutDate(newFutureOutdate);

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

        }

        [When(@"I enter valid details on Booking Widget with dates within a 10 days")]
        public void WhenIEnterValidDetailsOnBookingWidgetWithDateswitin10Days(Table table)
        {
            String futureCheckInDate = DateTime.Today.AddDays(5).ToString("MM/dd/yyyy");
            String newFuturedate = futureCheckInDate.Replace("-", "/");
            Pages.PropertyHome.enterCheckInDate(newFuturedate);

            String futureCheckoutDate = DateTime.Today.AddDays(6).ToString("MM/dd/yyyy");
            String newFutureOutdate = futureCheckoutDate.Replace("-", "/");
            Pages.PropertyHome.clearCheckOutDate();
            Pages.PropertyHome.enterCheckOutDate(newFutureOutdate);

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);
        }


        [When(@"I enter valid details on Booking Widget with dates within 60 days")]
        public void WhenIEnterValidDetailsOnBookingWidgetWithDatesWithin60Days(Table table)
        {
            String futureCheckInDate = DateTime.Today.AddDays(50).ToString("MM/dd/yyyy");
            String newFuturedate = futureCheckInDate.Replace("-", "/");
            Pages.PropertyHome.enterCheckInDate(newFuturedate);

            String futureCheckoutDate = DateTime.Today.AddDays(52).ToString("MM/dd/yyyy");
            String newFutureOutdate = futureCheckoutDate.Replace("-", "/");
            Pages.PropertyHome.clearCheckOutDate();
            Pages.PropertyHome.enterCheckOutDate(newFutureOutdate);

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

        }

        [When(@"I enter valid details on Booking Widget with dates within 60 days and 1 night")]
        public void WhenIEnterDetailsOnBookingWidgetWithDatesWithin60Days1night(Table table)
        {
            String futureCheckInDate = DateTime.Today.AddDays(55).ToString("MM/dd/yyyy");
            String newFuturedate = futureCheckInDate.Replace("-", "/");
            Pages.PropertyHome.enterCheckInDate(newFuturedate);

            String futureCheckoutDate = DateTime.Today.AddDays(56).ToString("MM/dd/yyyy");
            String newFutureOutdate = futureCheckoutDate.Replace("-", "/");
            Pages.PropertyHome.clearCheckOutDate();
            Pages.PropertyHome.enterCheckOutDate(newFutureOutdate);

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

        }

        [When(@"I enter valid details on Booking Widget with dates within 60 days and more than 2 nights")]
        public void WhenIEnterDetailsOnBookingWidgetWithDatesWithin60DaysMoreThan2Nights(Table table)
        {
            String futureCheckInDate = DateTime.Today.AddDays(53).ToString("MM/dd/yyyy");
            String newFuturedate = futureCheckInDate.Replace("-", "/");
            Pages.PropertyHome.enterCheckInDate(newFuturedate);

            String futureCheckoutDate = DateTime.Today.AddDays(56).ToString("MM/dd/yyyy");
            String newFuturecheckOutdate = futureCheckoutDate.Replace("-", "/");
            Pages.PropertyHome.clearCheckOutDate();
            Pages.PropertyHome.enterCheckOutDate(newFuturecheckOutdate);

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

        }

        [When(@"I enter Kids count and do not enter Adults count on Booking Widget")]
        public void WhenIEnterKidaNDoNotEnterAdluts(Table table)
        {
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();

            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            if (kids != 0)
            {
                String kids1age = table.Rows[0]["Kids1Age"];
                Pages.Suite.SelectKids(kids, kids1age);
                //String kids1age = table.Rows[0]["Kids1Age"];
                //int kids1Age = Int32.Parse(kids1age);
                //Pages.Suite.SelectKids1Age(kids1Age);
            }

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);
        }

        [When(@"I select adults")]
        public void WhenISelectAdults(Table table)
        {
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);
        }

        [When(@"I select maximum adults")]
        public void WhenISelectMaxAdults(Table table)
        {
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);
        }

        [When(@"I decrement the no of adults")]
        public void WhenIDecrementNoOfAdults(Table table)
        {
            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.decreamentAdults(adults);
        }

        [When(@"I increament the no of adults")]
        public void WhenIIncreamentAdults(Table table)
        {
            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);
        }

        [When(@"I select 2 kids")]
        public void WhenISelect2Kids(Table table)
        {
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            if (kids != 0)
            {
                String kids1age = table.Rows[0]["Kids1Age"];
                Pages.Suite.SelectKids(kids, kids1age);
                //String kids1age = table.Rows[0]["Kids1Age"];
                //int kids1Age = Int32.Parse(kids1age);
                //Pages.Suite.SelectKids1Age(kids1Age);
                String kids2age = table.Rows[0]["Kids2Age"];
                int kids2Age = Int32.Parse(kids2age);
                Pages.Suite.SelectKids2Age(kids2Age);
            }           
        }

        [When(@"I try increasing the no of kids")]
        public void WhenITryIncrementingKids(Table table)
        {
            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            String kids1age = table.Rows[0]["Kids1Age"];
            //Pages.Suite.SelectKids(kids, kids1age);
            Pages.Suite.SelectKids1();
            /*if (kids != 0)
            {
                String kids1age = table.Rows[0]["Kids1Age"];
                Pages.Suite.SelectKids(kids, kids1age);
            }*/
        }

        [When(@"I select only kids")]
        public void WhenISelectOnlyKids(Table table)
        {
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();

            /*String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            Thread.Sleep(3000);*/

            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            String kids1age = table.Rows[0]["Kids1Age"];
            Pages.Suite.SelectKids(kids, kids1age);

            Pages.Suite.clickBookNowBtn();
            /*if (kids != 0)
            {
                String kids1age = table.Rows[0]["Kids1Age"];
                Pages.Suite.SelectKids(kids, kids1age);
            }*/
        }

        [When(@"I select maximum kids")]
        public void WhenISelectMaxKids(Table table)
        {
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            Thread.Sleep(3000);

            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            String kids1age = table.Rows[0]["Kids1Age"];
            Pages.Suite.SelectKids(kids, kids1age);
            
            /*if (kids != 0)
            {
                String kids1age = table.Rows[0]["Kids1Age"];
                Pages.Suite.SelectKids(kids, kids1age);
            }*/
        }

        [When(@"I decreament the no of kids")]
        public void WhenIDecrementNoOfKids(Table table)
        {
            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            Pages.Suite.decrementKids(kids);
        }

        [When(@"I select 1 kid")]
        public void WhenISelect1Kid(Table table)
        {
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            if (kids != 0)
            {
                String kids1age = table.Rows[0]["Kids1Age"];
                Pages.Suite.SelectKids(kids, kids1age);
                //String kids1age = table.Rows[0]["Kids1Age"];
                //int kids1Age = Int32.Parse(kids1age);
                //Pages.Suite.SelectKids1Age(kids1Age);
            }
        }

        [When(@"I increament the no of kids")]
        public void WhenIIncrementNoOfKids(Table table)
        {
            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            if (kids != 0)
            {
                String kids2age = table.Rows[0]["Kids2Age"];
                Pages.Suite.SelectKids(kids, kids2age);
                //String kids2age = table.Rows[0]["Kids2Age"];
                //int kids2Age = Int32.Parse(kids2age);
                //Pages.Suite.SelectKids2Age(kids2Age);
            }
        }

        [When(@"I enter booking dates")]
        public void WhenIEnterBookingDates()
        {
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();
        }

        [When(@"I click to open Guest section")]
        public void WhenIClickToOpenGuestSection()
        {
            Pages.Suite.clickToOpenGuestSection();
         }

        [When(@"I switch ON the Accessible Rooms only option")]
        public void WhenISwitchOnAccessibleRooms()
        {
            Pages.Plan.switchOnAccessibleRoomOption();
        }

        [When(@"I switch Off the Accessible Rooms only option")]
        public void WhenISwitchOffAccessibleRooms()
        {
            Pages.Plan.switchOffAccessibleRoomOption();
        }

        [When(@"I try increasing the no of adults")]
        public void WhenITryIncreamentAdults(Table table)
        {
            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);
        }

        [When(@"I enter weekdays on Booking Widget")]
        public void WhenIEnterWeekDaysOnBookingWidget(Table table)
        {
            Pages.Suite.selectCheckInWeekday();
            Pages.Suite.selectCheckOutWeekday();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

        }

        [When(@"I enter long weekdays on Booking Widget")]
        public void WhenIEnterLongWeekDaysOnBookingWidget(Table table)
        {
            Pages.Suite.selectCheckInLongWeekday();
            Pages.Suite.selectCheckOutLongWeekday();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

        }

        [When(@"I enter weekend dates on Booking Widget")]
        public void WhenIEnterWeekendDatesOnBookingWidget(Table table)
        {
            Pages.Suite.selectCheckInWeekend();
            Pages.Suite.selectCheckOutWeekend();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

        }

        [When(@"I close Offer Code error pop up")]
        public void WhenICloseOfferCodeErrorPopup()
        {
            Pages.Plan.clickSearchAgainButton();
        }

        [When(@"I click View Special Offers button")]
        public void WhenIClickOnViewSpecialOffersBtn()
        {
            Pages.Plan.clickViewSpecialOffersButton();
        }

        [Then(@"I should see validation error")]
        public void ThenIShouldSeeValidationError()
        {
            Boolean validation = Pages.Plan.validationAlertDisplayed();
            Assert.IsTrue(validation, "User sees Validation error.");
        }

        [Then(@"I should see validation error for no adults")]
        public void ThenIShouldSeeValidationErrorForNoAdults()
        {
            Boolean validation = Pages.Plan.noAdultsValidationErrorDisplayed();
            Assert.IsTrue(validation, "User sees Validation error for no adults entered.");
        }
               

        [Then(@"I should see changed Guests count")]
        public void ThenIShouldSeeChangedGuestsCount(Table table)
        {
            String noOfAdultsExpected = table.Rows[0]["Guests"] + " Guests";
            String noOfAdultsActual = Pages.Suite.getGuestsText();

            Assert.IsTrue(noOfAdultsActual.Equals(noOfAdultsExpected), "User should see changed Guests count.");
        }

        [Then(@"I should see the Accessible Rooms only option On")]
        public void ThenIShouldSeeAccessibleRoomsOn()
        {
            Boolean optionOn = Pages.Plan.checkAccessibleRoomOptionIsOn();
            Assert.IsTrue(optionOn, "User sees the Accessible Rooms only option On.");
        }

        [Then(@"I should see the Accessible Rooms only option Off")]
        public void ThenIShouldSeeAccessibleRoomsOff()
        {
            Boolean optionOff = Pages.Plan.checkAccessibleRoomOptionIsOff();
            Assert.IsTrue(optionOff, "User sees the Accessible Rooms only option Off.");
        }

        [Then(@"I should not see change in Guests count")]
        public void ThenIShouldNotSeeChangeInGuestsCount(Table table)
        {
            String noOfGuestsExpected = table.Rows[0]["Guests"] + " Guests";
            String noOfGuestsActual = Pages.Suite.getGuestsText();

            Assert.IsTrue(noOfGuestsActual.Equals(noOfGuestsExpected), "User should not see change in Guests count.");
        }

        [Then(@"I should see invalid offer code error")]
        public void ThenIShouldSeeInvalidOfferCodeValidationError()
        {
            Boolean validation = Pages.Plan.checkInvalidOfferCodeErrorDisplayed();
            Assert.IsTrue(validation, "User sees invalid offer code error.");
        }

        [Then(@"I should see correct message for MOREFUN offer code")]
        public void ThenIShouldSeeMsgOnOfferCodeError()
        {
            Boolean validation = Pages.Plan.checkMsgOnOfferCodeErrorDisplayed();
            Assert.IsTrue(validation, "User sees correct message for MOREFUN offer code.");
        }

        [When(@"I enter valid details on Booking Widget with dates more than 60 days through deals page")]
        public void WhenIEnterValidDetailsOnBookingWidgetWithDates60DaysLaterthroughdealsPage(Table table)
        {
            String futureCheckInDate = DateTime.Today.AddDays(80).ToString("MM/dd/yyyy");
            String newFuturedate = futureCheckInDate.Replace("-", "/");
            Pages.PropertyHome.enterCheckInDate(newFuturedate);

            String futureCheckoutDate = DateTime.Today.AddDays(82).ToString("MM/dd/yyyy");
            String newFutureOutdate = futureCheckoutDate.Replace("-", "/");
            Pages.PropertyHome.clearCheckOutDate();
            Pages.PropertyHome.enterCheckOutDate(newFutureOutdate);

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);
        }

        [When(@"I enter valid details on Booking Widget with dates within 60 days through deals page")]
        public void WhenIEnterValidDetailsOnBookingWidgetWithinDates60DaysrthroughdealsPage(Table table)
        {
            String futureCheckInDate = DateTime.Today.AddDays(50).ToString("MM/dd/yyyy");
            String newFuturedate = futureCheckInDate.Replace("-", "/");
            Pages.PropertyHome.enterCheckInDate(newFuturedate);

            String futureCheckoutDate = DateTime.Today.AddDays(52).ToString("MM/dd/yyyy");
            String newFutureOutdate = futureCheckoutDate.Replace("-", "/");
            Pages.PropertyHome.clearCheckOutDate();
            Pages.PropertyHome.enterCheckOutDate(newFutureOutdate);

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);
        }

        [When(@"I click On Check In date Calendar")]
        public void WhenIClickOnCalendar()
        {
            Pages.Plan.clickOnCalendar();
        }

        /* [When(@"I click om Past date under calendar")]
         public void WhenIClickOnPastDate()
         {
             Pages.Plan.clickOnPastDate();
         }*/

        [Then(@"I should not able to select past date as 'Check In' date")]
        public void ThenIShouldNotAbleToSelectPastDate()
        {
            Boolean validation = Pages.Plan.NotAbleToSelectPastDate();
            Assert.IsTrue(validation, "I should not able to select past date as 'Check In' date");
        }

        [Then(@"I should see offer code description")]
        public void ThenIShouldSeeOfferCodeDescription()
        {
            Boolean OffercodeDesc = Pages.Plan.verifyOfferCodeDescription();
            Assert.IsTrue(OffercodeDesc, "I should see offer code description");
        }
    }
}
