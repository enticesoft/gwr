﻿using GWRFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class DiningAndShoppingPageSteps
    {
        [When(@"I navigate to '(.*)' Page")]
        public void WhenINavigateToPage(string p0)
        {
            Pages.PropertyHome.closeDealsSignUp();
            Pages.DiningAndShoppingPage.NavigateToDiningPage();
        }
        
        [Then(@"I should see total packages available are equal to Dining packages")]
        public void ThenIShouldSeeTotalPackagesAvailableAreEqualToDiningPackages()
        {
            Boolean isCountMatching = Pages.DiningAndShoppingPage.VerifyTotalPkgAndDininPkgCount();
            Assert.IsTrue(isCountMatching, "I can see all Dining Packags available");
        }
        [When(@"I navigate to '(.*)' page sub menu")]
        public void WhenINavigateTopage(string p0)
        {
            Pages.PropertyHome.closeDealsSignUp();
            Pages.DiningAndShoppingPage.NavigateToShoppingPage();
        }


        [Then(@"I should see total packages available are equal to Shopping package")]
        public void ThenIShouldSeeTotalPackagesAvailableAreEqualToShoppingPackage()
        {
            Boolean IsCountMatching = Pages.DiningAndShoppingPage.VerifyTotalPkgAndShoppingPkgCount();
            Assert.IsTrue(IsCountMatching, "All packages available are shopping packages");
        }

        [Then(@"I should see Dining page opened")]
        public void ThenIShouldSeeDiningPageOpened()
        {
            Boolean IsDiningHeaderDisplays = Pages.DiningAndShoppingPage.VerifyDiningPageOpened();
            Assert.IsTrue(IsDiningHeaderDisplays, "Dining page is displaying");

        }


        [Then(@"I should see Shopping page is opened")]
        public void ThenIShouldSeeShoppingPageIsOpned()
        {
            Boolean IsShoppingHeaderDisplays = Pages.DiningAndShoppingPage.VerifyShoppingPageOpened();
            Assert.IsTrue(IsShoppingHeaderDisplays, "Dining page is displaying");
        }

        [When(@"I click on Title of the package")]
        public void WhenIClickOnTitleOfThePackage()
        {
            Pages.DiningAndShoppingPage.ClickOnTitleOfShoppingPkg();
        }

        /*  [Then(@"I should see details page of title")]
          public void ThenIShouldSeeDetailsPageOfTitle(Table table)
          {
              String ShoppingPkgName = table.Rows[0]["PackageTitle"];
              Boolean IsShoppingDetailOpened = Pages.DiningAndShoppingPage.VerifyShoppingpkgDetailPage(ShoppingPkgName);
              Assert.IsTrue(IsShoppingDetailOpened, "Shopping Details page opened when click on title of package");

          }*/

        [Then(@"I should see details page of title")]
        public void ThenIShouldSeeDetailsPageOfTitle()
        {

            Boolean IsShoppingDetailOpened = Pages.DiningAndShoppingPage.VerifyShoppingpkgDetailPage();
            Assert.IsTrue(IsShoppingDetailOpened, "Shopping Details page opened when click on title of package");

        }



        [When(@"I click on Title of the Dining package")]
        public void WhenIClickOnTitleOfTheDiningPackage()
        {
            Pages.DiningAndShoppingPage.ClickOnTitleOfPkg();
        }


      /*  [Then(@"I should see details page of package")]
        public void ThenIShouldSeeDetailsPageOfPackage(Table table)
        {
            String DiningPkgName = table.Rows[0]["PackageTitle"];
            Boolean IsDiningDetailOpened = Pages.DiningAndShoppingPage.VerifyDiningpkgDetailPage(DiningPkgName);
            Assert.IsTrue(IsDiningDetailOpened, "Dining Details page opened when click on title of package");
        }*/

        [Then(@"I should see details page of package")]
        public void ThenIShouldSeeDetailsPageOfPackage()
        {

            Boolean IsDiningDetailOpened = Pages.DiningAndShoppingPage.VerifyDiningpkgDetailPage();
            Assert.IsTrue(IsDiningDetailOpened, "Dining Details page opened when click on title of package");
        }

    }
}
