﻿using GWRFramework;
using GWRFramework.TestData;
using NUnit.Framework;
using System;
using System.Configuration;
using System.Threading;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    class DayPassSteps
    {

        [When(@"I click on the DayPass Header")]
        public void WhenIclickOnDayPassHeader()
        {
            Pages.DayPass.clickOnDayPassHeaderLink();     
        }

        [When(@"I navigate from Home page to DayPass page")]
        public void WhenIamNavigatetoDayPassPage()
        {
            Pages.DayPass.clickOnDayPassHeaderLink();

        }

        [When(@"I enter valid Promo Code details")]
        public void WhenIEnterPromoCodeDetails(Table table)
        {
            String Promocode = table.Rows[0]["PromoCode"];
            Pages.DayPass.sendDaypassPromo(Promocode);
            
        }
        [When(@"I Enter valid date in Arrival Date")]
        public void WhenIEnterValiddateInArrivalDate()
        {
            Pages.DayPass.selectArrivalDate();
        }
        [Then(@"I should see Promo Terms Information")]
        public void ThenIShouldSeePromoTermsInfo()
        {
            Boolean dayPassPromoTerms = Pages.DayPass.checkDayPassPromoCodeInfoDisplayed();
            Assert.IsTrue(dayPassPromoTerms, "User see DayPass Promo Code Information .");
        }

        [Then(@"I should see ADD GUESTS section")]
        public void ThenIShouldSeeGuestSection()
        {
            Boolean dayPassGuestfield = Pages.DayPass.checkDayPassPromoCodeInfoDisplayed();
            Assert.IsTrue(dayPassGuestfield, "User see DayPass Number Of Guest Field .");
        }

        [Then(@"I should see Total section")]
        public void ThenIShouldSeeTotalSection()
        {
            Boolean dayPassTotalLabel = Pages.DayPass.checkDayPassTotalLabelIsDisplayed();
            Assert.IsTrue(dayPassTotalLabel, "User see Total label on DayPass Page.");
        }

        [Then(@"I should see Checkout button")]
        public void ThenIShouldSeeCheckoutBtn()
        {
            Boolean dayPassCheckoutBtn = Pages.DayPass.checkDayPassCheckoutBtnIsDisplayed();
            Assert.IsTrue(dayPassCheckoutBtn, "User see Checkout Button on DayPass Page.");
        }

        [When(@"I Enter valid Guest Quantity")]
        public void WhenIEnterGuestQuantityDetails(Table table)
        {
            String GuestQty= table.Rows[0]["GuestQuantity"];
            Pages.DayPass.sendDaypassGuestQty(GuestQty);

        }

        [When(@"I click on the Checkout button")]
        public void WhenIclickOnDayPassCheckoutBtn()
        {
            Pages.DayPass.clickOnDayPassCheckoutBtn();
            
        }

        [Then(@"I should see DayPass Payment Page")]
        public void ThenIShouldSeeDayPassPaymentPage()
        {
            Boolean dayPassPaymentPage = Pages.DayPass.checkDayPassPaymentPageDisplayed();
            Assert.IsTrue(dayPassPaymentPage, "User see DayPass Payment Page.");
        }

        [When(@"I navigate from Home page to DayPass Payment page")]
        public void WhenInavigateToDayPassPaymentPage(Table table)
        {
            Pages.DayPass.clickOnDayPassHeaderLink();
            
            String Promocode = table.Rows[0]["PromoCode"];
            Pages.DayPass.sendDaypassPromo(Promocode);

            Pages.DayPass.selectArrivalDate();

            String GuestQty = table.Rows[0]["GuestQuantity"];
            Pages.DayPass.sendDaypassGuestQty(GuestQty);

            Pages.DayPass.clickOnDayPassCheckoutBtn();

        }
        [Then(@"I should Invalid Promo Code Message")]
        public void ThenIShouldSeeDayPassInvalidPromoMessage()
        {
            Boolean dayPassPromoMessage= Pages.DayPass.checkInvalidPromoMessageDisplayed();
            Assert.IsTrue(dayPassPromoMessage, "User see Invalid Promo Code Message. ");
        }
        [Then(@"I should see correct Number of DayPasses on Payment Page")]
        public void ThenIShouldSeeCorrectNumberOfDayPasses(Table table)
        {
            String GuestQty = table.Rows[0]["GuestQuantity"];
            Boolean dayPasspassescount = Pages.DayPass.checkNumberOfPassesOnPaymentPage(GuestQty);
            Assert.IsTrue(dayPasspassescount, "User see correct Number of DayPasses on Payment Page.");
        }
        [Then(@"I should see correct PromoCode on Payment Page")]
        public void ThenIShouldSeeCorrectPrpmoOnPaymentPage(Table table)
        {
            String PromoCode = table.Rows[0]["PromoCode"];
            Boolean dayPassPrmoOnPayment = Pages.DayPass.checkPromoCodeOnPaymentPage(PromoCode);
            Assert.IsTrue(dayPassPrmoOnPayment, "User see correct PromoCode on Payment Page.");
        }

        [Then(@"I should see correct PromoCode on Payment Page for logged user")]
        public void ThenIShouldSeeCorrectPrpmoOnPaymentPageForLogedUser(Table table)
        {
            String PromoCode = table.Rows[0]["PromoCode"];
            Boolean dayPassPrmoOnPayment = Pages.DayPass.checkPromoCodeOnPaymentPageWithLogin(PromoCode);
            Assert.IsTrue(dayPassPrmoOnPayment, "User see correct PromoCode on Payment Page.");
        }

        

        [Then(@"I should see correct DayPass Date on Payment Page")]
        public void ThenIShouldSeeCorrectDateonPayment()
        {
            Boolean dayPassDateonPayment = Pages.DayPass.checkDateOnDaypassPayment();
            Assert.IsTrue(dayPassDateonPayment, "User see correct Date on Payment Page. ");
        }

        [Then(@"I should see correct Unit Price on Payment Page")]
        public void ThenIShouldSeeCorrectUnitTotalCostonPayment()
        {
            Boolean dayPassUnitPriceonPayment = Pages.DayPass.checkUnitPriceOnDaypassPayment();
            Assert.IsTrue(dayPassUnitPriceonPayment, "User see correct Unit Cost on Payment Page. ");
        }

        [Then(@"I should see correct Total on Payment Page")]
        public void ThenIShouldSeeCorrectTotalCostonPayment()
        {
            Boolean dayPassUnitPriceonPayment = Pages.DayPass.checkTotalOnDaypassPayment();
            Assert.IsTrue(dayPassUnitPriceonPayment, "User see correct Total Cost on Payment Page. ");
        }

        [When(@"I click on the DayPass Tile")]
        public void WhenIclickOnDayPassTileBtn()
        {
            Pages.DayPass.clickOnDayPassHomeTile();

        }
        [Then(@"I should see correct Sub Total on DayPass Payment Page")]
        public void ThenIShouldSeeCorrectSubTotalCostonPayment()
        {
            Boolean dayPassSubTotalPriceonPayment = Pages.DayPass.checkSubTotalPriceOnDaypassPayment();
            Assert.IsTrue(dayPassSubTotalPriceonPayment, "User see correct Sub Total Cost on Payment Page. ");
        }

        [Then(@"I should seee correct Taxes amount on DayPass Payment Page")]
        public void ThenIShouldSeeCorrectTaxesCostonPayment()
        {
            Boolean dayPassSubTotalPriceonPayment = Pages.DayPass.checkDayPassTaxesDisplayed();
            Assert.IsTrue(dayPassSubTotalPriceonPayment, "User see correct Taxes Cost on DayPass Payment Page. ");
        }

        [When(@"I click on the Full Day Pass")]
        public void WhenIClickOnTheFullDayPass()
        {
            Pages.DayPass.clickOnFullDayPass();
        }

        [Then(@"I should see correct Full Day Pass Per Guest Cost on Payment Page")]
        public void ThenIShouldSeeCorrectFullDayPassPerGuestCostOnPaymentPage()
        {
            Boolean fullDayPassPerGuestCost = Pages.DayPass.checkFullDayPassPerGuestonPaymentPageDisplayed();
            Assert.IsTrue(fullDayPassPerGuestCost, "User see correct Full Day Pass (per guest) on DayPass Payment Page. ");
        }

        [Then(@"I should see correct DayPass Total for Full day Pass on Payment Page")]
        public void ThenIShouldSeeCorrectDayPassTotalForFullDayPassOnPaymentPage()
        {
            Boolean fullDayPasstotalCost = Pages.DayPass.checkFullDayPassTotalonPaymentPageDisplayed();
            Assert.IsTrue(fullDayPasstotalCost, "User see correct Day Pass total on DayPass Payment Page.");
        }

        [Then(@"I should see correct Total on Payment Page for FullDay Pass")]
        public void ThenIShouldSeeCorrectTotalOnPaymentPageForFullDayPass()
        {
            Boolean fullDayPassFinalCost = Pages.DayPass.checkTotalOnDaypassPaymentForFullDayPass();
            Assert.IsTrue(fullDayPassFinalCost, "User see correct Day Pass total on DayPass Payment Page for FullDayPass. ");
        }

        [Then(@"I should successfully login to application with no error")]
        public void ThenIShouldLogOutAppWithNoError()
        {
            Boolean noError = Pages.DayPass.NotFoundMessageHandle();            
            Assert.IsTrue(noError, "I should successfully login to application with no error");
           /* if (noError == false)
            {
                Pages.DayPass.clickOnDayPassHeaderLink();
                Pages.PropertyHome.clickToSignOut();
            }*/
        }

        [When(@"I click on the Add a Package button")]
        public void WhenIClickOnTheAddAPackageButton()
        {
            Pages.NewConfirmation.clickOnAddAPackageBtn();
        }

        [When(@"I should see Add a Package Page")]
        public void WhenIShouldSeeAddAPackagePage()
        {
            Boolean addpackagePge = Pages.AddAPackage.CheckAddAPackagePageDisplayed();
            Assert.IsTrue(addpackagePge, "User see Add A Package Page. ");
        }

        [Then(@"I should see Cost Summary Section")]
        public void ThenIShouldSeeCostSummarySection()
        {
            Boolean costsummaryDayPass = Pages.NewConfirmation.costSummarysectionDisplayed();
            Assert.IsTrue(costsummaryDayPass, "User see DayPass Cost Summary Section. ");

        }
        //
        [Then(@"I should see Validation message fired for less than one quantity")]
        public void ThenIShouldSeeValidationMessageFiredForLessThanOneQuantity()
        {
            Boolean guestErrorMessage = Pages.DayPass.checkValidationMessageWhenEnterZeroGuestDisplayed();
            Assert.IsTrue(guestErrorMessage, "User see message as numberOfGuests must be greater than or equal to 1");
        }

        [Then(@"I should see correct hours for Half Day Pass")]
        public void ThenIShouldSeeCorrectHoursForHalfDayPass()
        {
            Boolean halfDayPassTimeMessage = Pages.DayPass.checkHalfDaypassHoursDisplayed();
            Assert.IsTrue(halfDayPassTimeMessage, "User see correct hours for Half Day Pass");
        }

        [When(@"I click on the Checkout button to see error message")]
        public void WhenIClickOnTheCheckoutButtonToSeeErrorMessage()
        {
            Pages.DayPass.clickOnDayPassCheckoutBtntoSeeError();
        }

        [Then(@"I should not see Affirm Section")]
        public void ThenIShouldNotSeeAffirmSection()
        {
            Boolean Affirmsection = Pages.DayPass.verifyAffirmRadioBtn();
            Assert.IsFalse(Affirmsection, "I should not see Affirm Section");

        }

        /* [Then(@"I should see Validation message fired for less than one quantity")]
         public void ThenIShouldSeeValidationMessageFiredForLessThanOneQuantity()
         {
             Boolean guestErrorMessage = Pages.DayPass.checkValidationMessageWhenEnterZeroGuestDisplayed();
             Assert.IsTrue(guestErrorMessage, "User see message as numberOfGuests must be greater than or equal to 1");
         }

         [Then(@"I should see correct hours for Half Day Pass")]
         public void ThenIShouldSeeCorrectHoursForHalfDayPass()
         {
             Boolean halfDayPassTimeMessage = Pages.DayPass.checkHalfDaypassHoursDisplayed();
             Assert.IsTrue(halfDayPassTimeMessage, "User see correct hours for Half Day Pass");
         }

         [When(@"I click on the Checkout button to see error message")]
         public void WhenIClickOnTheCheckoutButtonToSeeErrorMessage()
         {
             Pages.DayPass.clickOnDayPassCheckoutBtntoSeeError();
         }*/

        [Then(@"I should able to see that 'Your credit card will be charged-----' under 'Book and Agree Button' and 'Total Cost' on cost summary on payment page are same")]
        public void ThenIShouldSeeYourCreditCardChargedAndTotalCostSame()
        {
            Boolean Result = Pages.DayPass.YourCreditCardChargedAndTotalCostSame();
            Assert.IsTrue(Result, "I should able to see that 'Your credit card will be charged-----' under 'Book and Agree Button' and 'Total Cost' on cost summary on payment page are same");
        }

        /*[When(@"I click on the Half Day Pass")]
        public void WhenIClickOntheHalfDayPass()
        {
            Pages.DayPass.ClickOnHalfDayPass();
        }*/

        [Then(@"I should see correct Half Day Pass Per Guest Cost on Payment Page")]
        public void ThenIShouldSeeCorrectHalfDayPassPerGuestCostOnPaymentPage()
        {
            Boolean halfDayPassPerGuestCost = Pages.DayPass.checkhalfDayPassPerGuestonPaymentPageDisplayed();
            Assert.IsTrue(halfDayPassPerGuestCost, "I should see correct Half Day Pass Per Guest Cost on Payment Page ");
        }

        [Then(@"I should see correct DayPass Total for Half day Pass on Payment Page")]
        public void ThenIShouldSeeCorrectDayPassTotalForHalfDayPassOnPaymentPage()
        {
            Boolean fullDayPasstotalCost = Pages.DayPass.checkhalfDayPassTotalonPaymentPageDisplayed();
            Assert.IsTrue(fullDayPasstotalCost, "User see correct Day Pass total for half day pass on DayPass Payment Page.");
        }

        [Then(@"I should see correct Total on Payment Page for Half day Pass")]
        public void ThenIShouldSeeCorrectTotalOnPaymentPageForHalfDayPass()
        {
            Boolean HalfDayPassFinalCost = Pages.DayPass.checkTotalOnDaypassPaymentForHalfDayPass();
            Assert.IsTrue(HalfDayPassFinalCost, "User see correct Day Pass total on DayPass Payment Page for HalfDayPass. ");
        }

        [When(@"I Enter Guest Quantity more than 2 digits")]
        public void WhenIEnterGuestQuantityMoreThanTwo(Table table)
        {

            String GuestQty = table.Rows[0]["GuestQuantity"];
            Pages.DayPass.sendDaypassGuestQtyMoreThanTwo(GuestQty);

        }

        /*[Then(@"I should see 'Value must be less than or equal to 99' validation message")]
        public void ThenIShouldSeeValueMustBeLessThanOrEqualTo99ValidationMessage()
        {
            Boolean Result = Pages.DayPass.ValidationMessageDisplayed();
            Assert.IsTrue(Result, "I should see 'Value must be less than or equal to 99' validation message");
        }*/

        [Then(@"I should not navigate on day pass payment page")]
        public void ThenIShouldNotSeeDayPassPaymentPage()
        {
            Boolean dayPassPaymentPage = Pages.DayPass.checkDayPassPaymentPageExist();
            Assert.IsFalse(dayPassPaymentPage, "I should not navigate on day pass payment page");
        }

        [When(@"I navigate from payment to reservation page")]
        public void WhenINavigateFromPaymentToReservationPage()
        {
            Pages.DayPass.NavigateToReservationPage();
        }

        [Then(@"I should see recently booked daypass reservation")]
        public void ThenIShouldSeeRecentlyBookedDaypassReservation()
        {
            Boolean ISReservationMatch = Pages.DayPass.MatchReservationNumOnreservationPage();
            Assert.IsTrue(ISReservationMatch, "I should see recent reservation  on reservation page");
            Pages.PropertyHome.clickToSignOut();
        }

        [When(@"I navigate to Reservation details page")]
        public void WhenINavigateToReservationDetailsPage()
        {
            Pages.DayPass.ClickViewdetailsBtn();
        }

        [Then(@"I should see correct reservation details on reservation Page")]
        public void ThenIShouldSeeCorrectReservationDetailsOnReservationPage()
        {
            Boolean ISDayPassDetailsCorrect = Pages.DayPass.VerifyDayPassreservationdetails();
            Assert.IsTrue(ISDayPassDetailsCorrect, "I should see correct reservation details for day pass");
            Pages.PropertyHome.clickToSignOut();

        }

        [When(@"I entered payment details on day Pass Payment Page for Guest user")]
        public void WhenIEnteredPaymentDetailsOnDayPassPaymentPageForGuestUser(Table table)
        {
            String firstName = table.Rows[0]["First Name"];
            Pages.Payment.enterfirstName(firstName);

            String lastName = table.Rows[0]["Last Name"];
            Pages.Payment.enterlastName(lastName);

            String email = table.Rows[0]["Email"];
            Pages.Payment.enterEmail(email);

            String phoneNumber = table.Rows[0]["Phone Number"];
            Pages.Payment.enterPhoneNumber(phoneNumber);

            String nameONCard = table.Rows[0]["Card Name"];
            Pages.Payment.enterNameOnCard(nameONCard);

            String numONCard = table.Rows[0]["Card Number"];
            Pages.Payment.enterNumOnCard(numONCard);

            Pages.Payment.selectMonth();

            Pages.Payment.selectYear();

            String CVV = table.Rows[0]["CVV"];
            Pages.Payment.enterCVV(CVV);

            //String postalCode = table.Rows[0]["Postal Code"];
            Pages.Payment.enterPostalCode();

            String city = table.Rows[0]["City"];
            Pages.Payment.enterCity(city);

            Pages.Payment.selectState();

            String billingAddress = table.Rows[0]["Billing Address"];
            Pages.Payment.enterBillingAddress(billingAddress);

            Pages.DayPass.DataOnDayPassPaymentPage();
            Pages.Payment.clickOnBookAndAgree();

        }

        [Then(@"I should see correct date on confirmation")]
        public void ThenIShouldSeeCorrectDateOnConfirmation()
        {
            Boolean ISMatchingDate = Pages.DayPass.ISMatchingDaypAssDate();
            Assert.IsTrue(ISMatchingDate, "Date is correct on confirmation page as per day pass payment page");
        }

        [Then(@"I should see correct pass quantity on confirmation")]
        public void ThenIShouldSeeCorrectPassQuantityOnConfirmation()
        {
            Boolean ISMatchingPass = Pages.DayPass.ISMatchingDayPassPass();
            Assert.IsTrue(ISMatchingPass, "Pass Quantity is correct on confirmation page as per day pass payement");
        }

        //=============================================================================
        [When(@"I click on cost summary Tab")]
        public void WhenIClickOnCostSUmmaryTab()
        {
            Pages.DayPass.clickOnCostSummaryTab();
        }

        [Then(@"I should see cost summary Section Collapse")]
        public void ThenIShouldSeeCostSummarySectionCollapse()
        {
            Boolean CostSmmyCllpse = Pages.DayPass.verifyCostSummarycollapse();
            Assert.IsTrue(CostSmmyCllpse, "I should see cost summary Section Collapse");
        }

        [Then(@"I should see cost summary Section Expanded")]
        public void ThenIShouldSeeCostSummarySectionExpand()
        {
            Boolean CostSmmyCllpse = Pages.DayPass.verifyCostSummaryExpand();
            Assert.IsTrue(CostSmmyCllpse, "I should see cost summary Section Expanded");
        }
        [When(@"I double click on cost summary Tab")]
        public void WhenIdoubleClickOnCostSUmmaryTab()
        {
            Pages.DayPass.clickOnCostSummaryTab();
            Pages.DayPass.clickOnCostSummaryTab();
        }

        [Then(@"I should see Invalid promo code error message")]
        public void ThenIShouldSeeInvalidPromoCodeError()
        {
            Boolean InvldProCodeErr = Pages.DayPass.verifyInvalidPromoCodeError();
            Assert.IsTrue(InvldProCodeErr, "I should see Invalid promo code error message");
        }

        [When(@"I click on invalid promo code error message close icon")]
        public void WhenIclickinvalidPrCodErrCloseIcon()
        {
            Pages.DayPass.clickInvalidPrmCodErrClsIcon();
        }
        
        [Then(@"I should not see Invalid promo code error message")]
        public void ThenIShouldNotSeeInvalidPromoCodeError()
        {
            Boolean InvldProCodeErr = Pages.DayPass.verifyInvalidPromoCodeError();
            Assert.IsFalse(InvldProCodeErr, "I should not see Invalid promo code error message");
        }

        [Then(@"I should see correct Total on day pass page")]
        public void ThenIShouldseeCorrectTotal()
        {
            DayPassPage.fullDPUnitPriceFromDaypassPge = DayPassPage.fullDPUnitPriceFromDaypassPge.Substring(1);
            double fullDPValue = Convert.ToDouble(DayPassPage.fullDPUnitPriceFromDaypassPge);
            //  fullDPValue = Math.Round((Double)fullDPValue, 2);
            double ActualTotal = DayPassPage.guestQty * fullDPValue;
            Pages.DayPass.getTotalonDayPassPage();
            Assert.IsTrue(DayPassPage.TotalonDaypasspage.Equals(ActualTotal), "I should see correct Total on day pass page");
        }

        [When(@"I click on the Half Day Pass")]
        public void WhenIClickOnTheHalfDayPass()
        {
            Pages.DayPass.clickOnhalfDayPass();
        }

        [Then(@"I should see correct Total on day pass page having selected half apss")]
        public void ThenIShouldseeCorrectTotalWithHalfPass()
        {
            double ActualTotal = DayPassPage.guestQty * DayPassPage.HalfDayPrice;
            Pages.DayPass.getTotalonDayPassPage();
            Assert.IsTrue(DayPassPage.TotalonDaypasspage.Equals(ActualTotal), "I should see correct Total on day pass page");
        }

        [Then(@"I should see Attraction Tile title")]
        public void ThenIShouldseeAttractionTileTitle()
        {
            Boolean result = Pages.DayPass.verifyattractionTileTitle();
            Assert.IsFalse(result, "I should see Attraction Tile title");
        }

        [Then(@"I should see Attraction Tile button")]
        public void ThenIShouldseeAttractionTileButton()
        {
            Boolean result = Pages.DayPass.verifyattractionTileButton();
            Assert.IsFalse(result, "I should see Attraction Tile button");
        }

        [Then(@"I should see Birthday Tile title")]
        public void ThenIShouldseeBirthdayTileTitle()
        {
            Boolean result = Pages.DayPass.verifyBirthdayTileTitle();
            Assert.IsFalse(result, "I should see Birthday Tile title");
        }

        [Then(@"I should see Birthday Tile button")]
        public void ThenIShouldseeBirthdayTileButton()
        {
            Boolean result = Pages.DayPass.verifyBirthdayTileButton();
            Assert.IsFalse(result, "I should see Birthday Tile button");
        }

        [Then(@"I should see Dining Tile title")]
        public void ThenIShouldseeDiningTileTitle()
        {
            Boolean result = Pages.DayPass.verifyDiningTileTitle();
            Assert.IsFalse(result, "I should see Dining Tile title");
        }

        [Then(@"I should see Dining Tile button")]
        public void ThenIShouldseeDiningTileButton()
        {
            Boolean result = Pages.DayPass.verifyDiningTileButton();
            Assert.IsFalse(result, "I should see Dining Tile button");
        }

        [Then(@"I should see Cabana Tile title")]
        public void ThenIShouldseeCabanaTileTitle()
        {
            Boolean result = Pages.DayPass.verifyCabanaTileTitle();
            Assert.IsFalse(result, "I should see Cabana Tile title");
        }

        [Then(@"I should see Cabana Tile button")]
        public void ThenIShouldseeCabanaTileButton()
        {
            Boolean result = Pages.DayPass.verifyCabanaTileButton();
            Assert.IsFalse(result, "I should see Cabana Tile button");
        }

        [Then(@"I should see Pup pass title on Pop up")]
        public void ThenIShouldseeverifyPupPssTitlOnPopUp()
        {
            Boolean result = Pages.DayPass.verifyPupPssTitlOnPopUp();
            Assert.IsFalse(result, "I should see Pup pass title on Pop up");
        }

        [Then(@"I should see Paw pass title on Pop up")]
        public void ThenIShouldseeverifyPawPssTitlOnPopUp()
        {
            Boolean result = Pages.DayPass.verifyPawPssTitlOnPopUp();
            Assert.IsFalse(result, "I should see Paw pass title on Pop up");
        }

        [Then(@"I should see Wolf pass title on Pop up")]
        public void ThenIShouldseeverifyWolfPssTitlOnPopUp()
        {
            Boolean result = Pages.DayPass.verifyWolfPssTitlOnPopUp();
            Assert.IsFalse(result, "I should see Wolf pass title on Pop up");
        }

        [Then(@"I should see Pup pass See All Include link on Pop up")]
        public void ThenIShouldseeVerifyPupSeeAllLnkOnPopUp()
        {
            Boolean result = Pages.DayPass.verifyPupSeeAllLnkOnPopUp();
            Assert.IsFalse(result, "I should see Pup pass See All Include link on Pop up");
        }

        [Then(@"I should see Paw pass See All Include link on Pop up")]
        public void ThenIShouldseeVerifyPawSeeAllLnkOnPopUp()
        {
            Boolean result = Pages.DayPass.verifyPawSeeAllLnkOnPopUp();
            Assert.IsFalse(result, "I should see Paw pass See All Include link on Pop up");
        }

        [Then(@"I should see Wolf pass See All Include link on Pop up")]
        public void ThenIShouldseeVerifyWolfSeeAllLnkOnPopUp()
        {
            Boolean result = Pages.DayPass.verifyWolfSeeAllLnkOnPopUp();
            Assert.IsFalse(result, "I should see Wolf pass See All Include link on Pop up");
        }

        [Then(@"I should see Pup pass See Price Section on Pop up")]
        public void ThenIShouldseeverifyPupPassPriceSecOnPopUp()
        {
            Boolean result = Pages.DayPass.verifyPupPassPriceSecOnPopUp();
            Assert.IsFalse(result, "I should see Pup pass See Price Section on Pop up");
        }

        [Then(@"I should see Paw pass See Price Section on Pop up")]
        public void ThenIShouldseeverifyPawPassPriceSecOnPopUp()
        {
            Boolean result = Pages.DayPass.verifyPawPassPriceSecOnPopUp();
            Assert.IsFalse(result, "I should see Paw pass See Price Section on Pop up");
        }

        [Then(@"I should see Wolf pass See Price Section on Pop up")]
        public void ThenIShouldseeverifyWolfPassPriceSecOnPopUp()
        {
            Boolean result = Pages.DayPass.verifyWolfPassPriceSecOnPopUp();
            Assert.IsFalse(result, "I should see Wolf pass See Price Section on Pop up");
        }

        [When(@"I click on Add Passes button of Attraction tile")]
        public void WhenIClickOnTheAddPassBtnonAttrct()
        {
            Pages.DayPass.clickOnAddPassBtn();
        }

        [When(@"I click on Add Package button of Dining tile")]
        public void WhenIClickOnTheAddPkgBtnonDining()
        {
            Pages.DayPass.clickOnAddPkgDinigBtn();
        }
        
        [When(@"I click on Add Package button of Birthday tile")]
        public void WhenIClickOnTheAddPkgBtnonBirthday()
        {
            Pages.DayPass.clickOnAddPkgBirthBtn();
        }

        [When(@"I click on Add A Cabana button of Cabana tile New confirmation")]
        public void WhenIClickOnTheAddCabanaBtnnonCabana()
        {
            Pages.DayPass.clickOnAddCabanaBtn();
        }

        [Then(@"I should see Title is correct on Dining Packages Pop up")]
        public void ThenIShouldseeDiningPackgTitleDingPckgPopUp()
        {
            Boolean result = Pages.DayPass.verifyDiningpckgfirst();
            Assert.IsTrue(result, "I should see Dining First Package title is displayed correct");
        }

        [Then(@"I should see Title is correct on Pop up")]
        public void ThenIShouldseeDiningPackgTitle()
        {
            Boolean result = Pages.DayPass.verifyDiningpckgfirst();
            Assert.IsTrue(result, "I should see Dining First Package title is displayed correct");

            Boolean result1 = Pages.DayPass.verifyDiningpckgScnd();
            Assert.IsTrue(result1, "I should see Dining Second Package title is displayed correct");

            Boolean result2 = Pages.DayPass.verifyDiningpckgThird();
            Assert.IsTrue(result2, "I should see Dining Third Package title is displayed correct");
        }

        [Then(@"I should see See What You Get link on Pop up")]
        public void ThenIShouldseeDiningPackgSeeWhtYouGetlink()
        {
            Boolean result = Pages.DayPass.verifyDiningpckgfirstSeeWhatLink();
            Assert.IsTrue(result, "I should see Dining First Package See What You Get link");

            Boolean result1 = Pages.DayPass.verifyDiningpckgScndSeeWhatLink();
            Assert.IsTrue(result1, "I should see Dining Second Package See What You Get link");

            Boolean result2 = Pages.DayPass.verifyDiningpckgTthirdSeeWhatLink();
            Assert.IsTrue(result2, "I should see Dining Third Package See What You Get link");
        }
        
        [Then(@"I should see correct cost on dining package Pop up")]
        public void ThenIShouldseeDiningPackgCostOnPckgPopUp()
        {
            Boolean result = Pages.DayPass.verifyDiningpckgFirstCostOnDingPkgPopUp();
            Assert.IsTrue(result, "I should see correct cost on dining package Pop up");
        }

        [Then(@"I should see correct cost on dining Pop up")]
        public void ThenIShouldseeDiningPackgCost()
        {
            Boolean result = Pages.DayPass.verifyDiningpckgFirstCost();
            Assert.IsTrue(result, "I should see Dining First Package Cost");

            Boolean result1 = Pages.DayPass.verifyDiningpckgScndCost();
            Assert.IsTrue(result1, "I should see Dining Second Package Cost");

            Boolean result2 = Pages.DayPass.verifyDiningpckgThirdCost();
            Assert.IsTrue(result2, "I should see Dining Third Package Cost");
        }

        [Then(@"I should see Birthday Titles are correct")]
        public void ThenIShouldseeBirthdayPackgTitle()
        {
            Boolean result = Pages.DayPass.verifyBirthpckgfirsttitl();
            Assert.IsTrue(result, "I should see Birthday First Package title is displayed correct");

            Boolean result1 = Pages.DayPass.verifyBirthpckgScndtitl();
            Assert.IsTrue(result1, "I should see Birthday Second Package title is displayed correct");

            Boolean result2 = Pages.DayPass.verifyBirthpckgthrdtitl();
            Assert.IsTrue(result2, "I should see Birthday Third Package title is displayed correct");
        }

        [Then(@"I should see Birthday packages cost are correct")]
        public void ThenIShouldseeBirthdayPackgCost()
        {
            Boolean result = Pages.DayPass.verifyBirthpckgfirstCost();
            Assert.IsTrue(result, "I should see Birthday First Package cost is displayed correct");

            Boolean result1 = Pages.DayPass.verifyBirthpckgscndCost();
            Assert.IsTrue(result1, "I should see Birthday Second Package cost is displayed correct");

            Boolean result2 = Pages.DayPass.verifyBirthpckgthrdCost();
            Assert.IsTrue(result2, "I should see Birthday Third Package cost is displayed correct");
        }


        [Then(@"I should see Birthday packages Add button")]
        public void ThenIShouldseeBirthdayPackgAddBtn()
        {
            Boolean result = Pages.DayPass.verifyBirthpckgFirstAddBtn();
            Assert.IsTrue(result, "I should see Birthday First Package Add Button");

            Boolean result1 = Pages.DayPass.verifyBirthpckgScndAddBtn();
            Assert.IsTrue(result1, "I should see Birthday Second Package Add Button");

            Boolean result2 = Pages.DayPass.verifyBirthpckgthirdAddBtn();
            Assert.IsTrue(result2, "I should see Birthday Third Package Add Button");
        }

        [Then(@"I should see Birthday packages count")]
        public void ThenIShouldseeBirthdayPackgCnt()
        {
            Boolean result = Pages.DayPass.verifyBirthFirstpckgCount();
            Assert.IsTrue(result, "I should see Birthday First Package count");

            Boolean result1 = Pages.DayPass.verifyBirthSecndpckgCount();
            Assert.IsTrue(result1, "I should see Birthday Second Package count");

            Boolean result2 = Pages.DayPass.verifyBirthThirdpckgCount();
            Assert.IsTrue(result2, "I should see Birthday Third Package count");
        }

        [Then(@"I should see confirmation email received")]
        public void ThenIShouldGetResetLinkFromEmail()
        {
            String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
            Boolean emailReceived = ReadEmail.CheckEmail.CheckConfirmationEmail(emailMaxCounter);
            Assert.IsTrue(emailReceived, "I should see confirmation email received");
        }

        [Then(@"I should see Cabana confirmation email received")]
        public void ThenIShouldGetCabanaConfirmationEmail()
        {
            if (MyReservationPage.cabanAvailableForAdd == true)
            {
                String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
                Boolean emailReceived = ReadEmail.CheckEmail.CheckCabanaConfirmationEmail(emailMaxCounter);
                Assert.IsTrue(emailReceived, "I should see Cabana confirmation email received");
            }
            else {
                Assert.IsTrue(true, "Cabana was not available on selected date");
            }
        }

        [Then(@"I should see Add A Package confirmation email received")]
        public void ThenIShouldGetAddAPackageEmail()
        {
            String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
            Boolean emailReceived = ReadEmail.CheckEmail.CheckAddPackageConfirmationEmail(emailMaxCounter);
            Assert.IsTrue(emailReceived, "I should see Add A Package confirmation email received");
        }
        
        [When(@"I should observe Cost summary on CMP Page for Day Pass")]
        public void ThenIShouldSeeCostSumryPriceONCMPForDayPass()
        {
            Pages.NewConfirmation.GetPricesfromCostSummaryForDayPass();
        }

    }

//}
}

