﻿using GWRFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class PaymentPageSteps
    {
               
        //private object table;

        [When(@"I observe suite total on Payment Page")]
        public void observeSuitePrice()
        {
            Pages.Payment.convertSuiteTotal();
        }

        [When(@"I observe taxes on Payment Page")]
        public void observeTaxesPrice()
        {
            Pages.Payment.convertTaxesToDouble();
        }

        [When(@"I observe Resort fee Payment Page")]
        public void observeResortPrice()
        {
            Pages.Payment.convertResortFeeToDouble();
        }

        [When(@"I observe total Payment Page")]
        public void observeTotalPrice()
        {
            Pages.Payment.convertTotalCostToDouble();
        }

        [When(@"I observe Due Today price on Payment Page")]
        public void observeDueTodayPrice()
        {
            Pages.Payment.convertDueTodayToDouble();
        }

        [When(@"I observe Due at check in price on Payment Page")]
        public void observeDueAtCheckInPrice()
        {
            Pages.Payment.convertDueAtCheckInToDouble();
        }

        [When(@"I observe package total on Payment Page")]
        public void observePckgTotalPrice()
        {
            Pages.Payment.convertPckgTotal();
        }
        
        [When(@"I entered payment details on Payment Page for log in user")]
        public void enterPaymentdetails(Table table)
        {            
            Pages.Payment.waitLoginPostlCodeLoad();            
            Pages.Payment.waitLoginPhoneNumLoad();
            Pages.Payment.waitLoginBiiligAddress();

            String phoneNumber = table.Rows[0]["Phone Number"];
            Pages.Payment.enterPhoneNumber(phoneNumber);


            //String postalCode = table.Rows[0]["Postal Code"];
            /*Pages.Payment.enterPostalCode();

            String city = table.Rows[0]["City"];
            Pages.Payment.enterCity(city);

            Pages.Payment.selectState();*/

            String billingAddress = table.Rows[0]["Billing Address"];
            Pages.Payment.enterBillingAddress(billingAddress);

            String nameONCard = table.Rows[0]["Card Name"];
            Pages.Payment.enterNameOnCard(nameONCard);

            String numONCard = table.Rows[0]["Card Number"];
            Pages.Payment.enterNumOnCard(numONCard);

            Pages.Payment.selectMonth();
            Pages.Payment.selectYear();

            String CVV = table.Rows[0]["CVV"];
            Pages.Payment.enterCVV(CVV);

            //String firstName = table.Rows[0]["First Name"];
            Pages.Payment.enterfirstName("QATester");

            //String lastName = table.Rows[0]["Last Name"];
            Pages.Payment.enterlastName("Nextgenqa");

            //String email = table.Rows[0]["Email"];
            Pages.Payment.enterEmail("gwr.nextgenqa1@gmail.com");

            Pages.Payment.clickOnBookAndAgree();
        }

        [When(@"I entered payment details on Payment Page for newly created user")]
        public void enterPaymentdetailsForNewlyCreated(Table table)
        {

            Pages.Payment.waitLoginPostlCodeLoad();
            //Pages.Payment.waitLoginPhoneNumLoad();
            //Pages.Payment.waitLoginBiiligAddress();
            
            String phoneNumber = table.Rows[0]["Phone Number"];
            Pages.Payment.enterPhoneNumber(phoneNumber);

            String billingAddress = table.Rows[0]["Billing Address"];
            Pages.Payment.enterBillingAddress(billingAddress);

            String nameONCard = table.Rows[0]["Card Name"];
            Pages.Payment.enterNameOnCard(nameONCard);

            String numONCard = table.Rows[0]["Card Number"];
            Pages.Payment.enterNumOnCard(numONCard);

            Pages.Payment.selectMonth();
            Pages.Payment.selectYear();

            String CVV = table.Rows[0]["CVV"];
            Pages.Payment.enterCVV(CVV);

            String city = table.Rows[0]["City"];
            Pages.Payment.enterCity(city);

            Pages.Payment.selectState();

            Pages.Payment.clickOnBookAndAgree();
        }

        [When(@"I entered payment details on Payment Page for Guest user")]
        public void enterGuestUserPaymentdetails(Table table)
        {
            String firstName = table.Rows[0]["First Name"];
            Pages.Payment.enterfirstName(firstName);

            String lastName = table.Rows[0]["Last Name"];
            Pages.Payment.enterlastName(lastName);

            String email = table.Rows[0]["Email"];
            Pages.Payment.enterEmail(email);

            String phoneNumber = table.Rows[0]["Phone Number"];
            Pages.Payment.enterPhoneNumber(phoneNumber);

            String nameONCard = table.Rows[0]["Card Name"];
            Pages.Payment.enterNameOnCard(nameONCard);

            String numONCard = table.Rows[0]["Card Number"];
            Pages.Payment.enterNumOnCard(numONCard);

            Pages.Payment.selectMonth();

            Pages.Payment.selectYear();

            String CVV = table.Rows[0]["CVV"];
            Pages.Payment.enterCVV(CVV);

            //String postalCode = table.Rows[0]["Postal Code"];
            Pages.Payment.enterPostalCode();

            String city = table.Rows[0]["City"];
            Pages.Payment.enterCity(city);

            Pages.Payment.selectState();            

            String billingAddress = table.Rows[0]["Billing Address"];
            Pages.Payment.enterBillingAddress(billingAddress);
                     

            Pages.Payment.clickOnBookAndAgree();
        }

        [When(@"I entered Contact Info for Guest user")]
        public void enterGuestUserContactdetails(Table table)
        {
            String firstName = table.Rows[0]["First Name"];
            Pages.Payment.enterfirstName(firstName);

            String lastName = table.Rows[0]["Last Name"];
            Pages.Payment.enterlastName(lastName);

            String email = table.Rows[0]["Email"];
            Pages.Payment.enterEmail(email);

            String phoneNumber = table.Rows[0]["Phone Number"];
            Pages.Payment.enterPhoneNumber(phoneNumber);

            Pages.Payment.clickOnContinue();
            Browser.opt.AddArgument("--disable - geolocation");
            Browser.opt.AddArguments("disable-geolocation");
        }

        [When(@"I entered Payment and billing for Guest user")]
        public void enterPaymentAndBillignInfo(Table table)
        {
            Thread.Sleep(4000);
            Browser.opt.AddArgument("--disable - geolocation");
            Browser.opt.AddArguments("disable-geolocation");

            if (Pages.Payment.phoneTxt.Exists()) { 
            //String phoneNumber = table.Rows[0]["Phone Number"];
            Pages.Payment.enterPhoneNumber("4521455855");

                Pages.Payment.clickOnContinue();
            }

            String nameONCard = table.Rows[0]["Card Name"];
            Pages.Payment.enterNameOnCard(nameONCard);

            String numONCard = table.Rows[0]["Card Number"];
            Pages.Payment.enterNumOnCard(numONCard);

            Pages.Payment.selectMonth();

            Pages.Payment.selectYear();

            String CVV = table.Rows[0]["CVV"];
            Pages.Payment.enterCVV(CVV);

            //String postalCode = table.Rows[0]["Postal Code"];
            Pages.Payment.enterPostalCode();

            String city = table.Rows[0]["City"];
            Pages.Payment.enterCity(city);

            Pages.Payment.selectState();

            String billingAddress = table.Rows[0]["Billing Address"];
            Pages.Payment.enterBillingAddress(billingAddress);

            Pages.Payment.clickOnContinue();
        }

        [When(@"I click on Book And Agree button")]
        public void clickonBookNdAgree()
        {
            Pages.Payment.clickOnBookAndAgree();
        }
        
        [When(@"I click on Save changes button")]
        public void clickonSaevChangesBtn()
        {
            Pages.Payment.clickOnContinue();
        }

        [When(@"I entered billing info for Affirm Option")]
        public void enterAffirmBillignInfo(Table table)
        {
            Browser.opt.AddArgument("--disable - geolocation");
            Browser.opt.AddArguments("disable-geolocation");
            //String postalCode = table.Rows[0]["Postal Code"];
            Pages.Payment.enterPostalCode();

            String city = table.Rows[0]["City"];
            Pages.Payment.enterCity(city);

            Pages.Payment.selectState();

            String billingAddress = table.Rows[0]["Billing Address"];
            Pages.Payment.enterBillingAddress(billingAddress);

            Pages.Payment.clickOnContinue();
        }

        [Then(@"I should see Payment Page Heading")]
        public void thenIShouldSeeLCODescrptn()
        {
            Boolean paymentPageHead = Pages.Payment.verifyPaymentPageHeading();
            Assert.IsTrue(paymentPageHead, "User able to see Payment page heading");
        }

        [When(@"I Sign In to Application through Already have account link")]
        public void iSignInToApplicationThroughAlreadyHaveAccount(Table table)
        {
            Pages.Payment.clickAlreadyHaveAccountLink();

            String email = table.Rows[0]["Email Address"];
            // Pages.SignIn.sendEmail(email);
            Pages.Payment.sendEmailonSigninPopup(email);
            String pass = table.Rows[0]["Password"];
            Pages.SignIn.sendPass(pass);

            Pages.SignIn.clickSignInbtn();
            Pages.DayPass.waitForElemntOutorInvisible(DayPassPage.waitForSignIn);
        }

        [When(@"I click on Activities Icon in progress bar")]
        public void WhenIClickOnactivityProgressBar()
        {
            Pages.Dining.navigateOnActivity();
        }

        [When(@"I click on Dining Icon in progress bar")]
        public void WhenIClickOnDiningIcon()
        {
            Pages.Payment.clickOnDiningIcon();
        }

        [Then(@"I should be navigate on dining page")]
        public void ThenIShouldNavigateOnDiningPage()
        {
            Boolean ContinueToPaymentBtn = Pages.Payment.iAmOnDiningPage();
            Assert.IsTrue(ContinueToPaymentBtn, "I should Navigate on Dining page after click on Dining Icon on Payment Page");
        }

        [When(@"I check SMS check box")]
        public void WhenIClickOnSMSCheckBox()
        {
            Pages.Payment.clickOnSMSCheckBox();
        }

        [When(@"I Uncheck SMS check box")]
        public void WhenIClickOncheckedSMSCheckBox()
        {
            Pages.Payment.clickOnSMSCheckBox();
        }

        [Then(@"I should see SMS check box checked")]
        public void ThenIShouldSeeSMSCheckBoxChecked()
        {
            Boolean Result = Pages.Payment.verifySMSCheckBoxChecked();
            Assert.IsTrue(Result, "I should see SMS check box checked on Payment Page");
        }

        [Then(@"I should see SMS check box Unchecked")]
        public void ThenIShouldSeeSMSCheckBoxUnChecked()
        {
            Boolean Result = Pages.Payment.verifySMSCheckBoxunChecked();
            Assert.IsTrue(Result, "I should see SMS check box unchecked on Payment Page");
        }

       /* [When(@"I Sign In to Application through Already have account link")]
        public void iSignInToApplicationThroughAlreadyHaveAccount(Table table)
        {
            Pages.Payment.clickAlreadyHaveAccountLink();

            String email = table.Rows[0]["Email Address"];
            // Pages.SignIn.sendEmail(email);
            Pages.Payment.sendEmailonSigninPopup(email);
            String pass = table.Rows[0]["Password"];
            Pages.SignIn.sendPass(pass);

            Pages.SignIn.clickSignInbtn();
        }*/

        [When(@"I check FlexTrip check box")]
        public void WhenIClickOnFlexTripCheckBox()
        {
            Pages.Payment.clickOnFlextTripCheckBox();
        }

        [When(@"I uncheck FlexTrip check box")]
        public void WhenIClickOnCheckedFlexTripCheckBox()
        {
            Pages.Payment.clickOnFlextTripCheckBox();
        }

        [Then(@"I should see FlexTrip check box checked")]
        public void ThenIShouldSeeFlexTripCheckBoxChecked()
        {
            Boolean Result = Pages.Payment.verifyFlexTripCheckBoxChecked();
            Assert.IsTrue(Result, "I should see FlexTrip check box checked on Payment Page");
        }

        [Then(@"I should see FlexTrip check box unchecked")]
        public void ThenIShouldSeeFlexTripCheckBoxunChecked()
        {
            Boolean Result = Pages.Payment.verifyFlexTripCheckBoxUnChecked();
            Assert.IsTrue(Result, "I should see FlexTrip check box unchecked on Payment Page");
        }

        [When(@"I observe FlexTrip price")]
        public void WhenIObservedFlexTripPrice()
        {
            Pages.Payment.getFlexTripSectionPrice();
        }

        [Then(@"I should see correct added FlexTrip price under cost summary")]
        public void ThenIShouldSeeCorrectAddedFlexTripPrice()
        {
            Pages.Payment.getFlexTripPriceunderCostSummary();
            Assert.IsTrue(PaymentPage.FlexTripPrice == PaymentPage.FlexTripSectionPrice, "I should see correct FlexTrip price under cost summary when added it");
        }

        [Then(@"I should see by default 'Credit Card' radio button selected in Payment Option section")]
        public void ThenIShouldSeeByDefaultCreditCardRadioBtnSelected()
        {
            Boolean Result = Pages.Payment.verifyCreditCardRadioBtnSelected();
            Assert.IsTrue(Result, "I should see by default 'Credit Card' radio button selected in Payment Option section");
        }

        [When(@"I click on affirm radio button")]
        public void WhenIClickOnAffirmRadioButton()
        {
            Pages.Payment.clickOnAffirmRadioBtn();
        }

        [Then(@"I should see Affirm radio button selected in Payment Option section")]
        public void ThenIShouldSeeAffirmRadioBtnSelected()
        {
            Boolean Result = Pages.Payment.verifyAffirmRadioBtnSelected();
            Assert.IsTrue(Result, "I should see Affirm radio button selected in Payment Option section when click on Affirm radio button");
        }

        [Then(@"I should see Check Out With Affirm button")]
        public void ThenIShouldSeeCheckOutWithAffirmBtn()
        {
            Boolean CheckOutWithAffirmBtn = Pages.Payment.verifyCheckOutwithAffirmBtn();
            Assert.IsTrue(CheckOutWithAffirmBtn, "I should see Check Out with Affirm button when select Affirm radio button");
        }

        
        [When(@"I click on check out with affirm button")]
        public void WhenIClickOnAffirmButton()
        {
            Pages.Payment.clickOnCheckOutwithAffirmBtn();
        }

        [When(@"I click on credit card radio button")]
        public void WhenIClickOnCreditCardRadioButton()
        {
            Pages.Payment.clickOnCreditCardRadioBtn();
        }

        [Then(@"I should see 'Credit Card' radio button selected in Payment Option section")]
        public void ThenIShouldSeeCreditCardRadioBtnSelected()
        {
            Boolean Result = Pages.Payment.verifyCreditCardRadioBtnSelected();
            Assert.IsTrue(Result, "I should see 'Credit Card' radio button selected in Payment Option section when click on credit card radio button");
        }

        [When(@"I entered Canadian postal code")]
        public void enterCanadianPostalCode(Table table)
        {
            String postalCode = table.Rows[0]["Postal Code"];
            Pages.Payment.enterPostalCode(postalCode);
        }

        [When(@"I select canada country")]
        public void enterCanadianCountry()
        {
            //String postalCode = table.Rows[0]["Postal Code"];
            Pages.Payment.selectCanadaCountry();
        }

        [Then(@"I should see Affirm Monthly Payment radio button disable")]
        public void ThenIShouldSeeAffirmRadioBtnDisable()
        {
            Boolean Result = Pages.Payment.verifyAffirmRadioBtnEnable();
            Assert.IsTrue(Result, "I should see Affirm Monthly Payment radio button disable When enter canadian postal code");
        }

        [Then(@"I should see 'Add flexibility to your trips’ section")]
        public void ThenIShouldSeeFlexTripSection()
        {
            Boolean FlexTripSection = Pages.Payment.verifyFlexTripSection();
            Assert.IsTrue(FlexTripSection, "I should see 'Add flexibility to your trips’ section on payment page");
        }

        [Then(@"I should not see 'Add flexibility to your trips’ section")]
        public void ThenIShouldNotSeeFlexTripSection()
        {
            Boolean FlexTripSection = Pages.Payment.verifyFlexTripSection();
            Assert.IsFalse(FlexTripSection, "I should not see 'Add flexibility to your trips’ section on payment page.");
        }

        [When(@"I click on flexTrip remove link")]
        public void WhenIClickOnFlexTripRemoveLink()
        {
            Pages.Payment.clickFlexTripRemoveLink();
        }

        [Then(@"I should see valid offercode under cost summary")]
        public void ThenIShouldseeValidOffCdUnderCostSumry(Table table)
        {
            String offerCodeTD = table.Rows[0]["Offer Code"];
            Boolean offerCode = Pages.Activities.verifyOffercodeOnCostSumry(offerCodeTD);
            Assert.IsTrue(offerCode, "I should see valid offer code under cost summary which is applied while searching suite");
        }

        [Then(@"I should see 'Your credit card will be charged' price and 'Due Today' price are same")]
        public void ThenIShouldSeeYourCrdCardAndDueTodayPriceSame()
        {
            Pages.Payment.convertDueTodayToDouble();
            Pages.Payment.getYourCredCrdChargedPrice();
            Assert.IsTrue(PaymentPage.dueTodayPrice == PaymentPage.CreditCardChargedPrice, "I should see 'Your credit card will be charged' price and 'Due Today' price are same");
        }


        [Then(@"I should see 'Total' price minus 'Due today' price should equals to 'Due at Check-In' price")]
        public void ThenIShouldSeeCorrectDueAtCheckInPrice()
        {
            Pages.Activities.getTotalPrice();
            Pages.Payment.convertDueTodayToDouble1();
            decimal ExpectedDueAtChckInPrice = ActivitiesPage.totalPrice1 - PaymentPage.dueTodayPrice1;
            Pages.Payment.convertDueAtCheckInToDouble1();
            Assert.IsTrue(ExpectedDueAtChckInPrice == PaymentPage.dueATCheckInPrice1, "I should see 'Total' price minus 'Due today' price should equals to 'Due at Check-In' price");
        }

        [Then(@"I should see correct Total with having packages under cost summary")]
        public void ThenIShouldSeeCorrectTotalPriceWithHavingPackages()
        {
            Pages.Activities.getSuiteTotal();
            Pages.Activities.getPackageTotalPrice();
            Pages.Payment.convertTaxesToDouble1();
            Pages.Payment.convertResortFeeToDouble1();
            decimal expectedTotalPrice = ActivitiesPage.suiteTotal1 + ActivitiesPage.packageTotalPrice1 + PaymentPage.taxPrice1 + PaymentPage.resortPrice1;
            //Double ExpectedTotalPrice = Math.Round((Double)expectedTotalPrice, 2);
            Pages.Activities.getTotalPrice();
            Assert.IsTrue(expectedTotalPrice == ActivitiesPage.totalPrice1, "I should see correct Total with having packages under cost summary");
        }

        [When(@"I click on Card holder not present at check-in question mark Icon")]
        public void WhenIClickOnCrdHldNtPrsntAtChkInQuestionMarkIcon()
        {
            Pages.Payment.clickCrdHldNtPrsntAtChkInQuestionMarkIcon();
        }

        [When(@"I click on Back to payment button")]
        public void WhenIClickOnBackToPaymentBtn()
        {
            Pages.Payment.clickBackToPaymentBtn();
        }

        [Then(@"I should not see Don't Warry pop up")]
        public void ThenIShouldNotSeeDoNotWarrypopUp()
        {
            Boolean result = Pages.Payment.verifyDoNotWarryPopUp();
            Assert.IsFalse(result, "I should not see Don't Warry pop up");
        }

        [When(@"I click on Privacy Policy link")]
        public void WhenIClickOnPrivacyPolicyLink()
        {
            Pages.Payment.clickPrivacyPolicyLink();
        }

        [When(@"I click on Terms and Conditions link")]
        public void WhenIClickOnTermsAndConditionsLink()
        {
            Pages.Payment.clickTermsAndConditionsLink();
        }

        [Then(@"I should navigate on privacy policy page")]
        public void ThenIShouldNavigateOnPrivacyPolicyPage()
        {
            Boolean privacyPolicy = Pages.Payment.verifyprivacyPolicyPage();
            Browser.CloseNewWindow();
            Browser.SwitchToParentWindow();
            Assert.IsTrue(privacyPolicy, "I should navigate on privacy policy page when click on privacy policy link");
        }

        [Then(@"I should navigate on Terms and Conditions page")]
        public void ThenIShouldNavigateOnTermsAndConditionsPage()
        {
            Boolean TermsConditions = Pages.Payment.verifyTermsAndConditionsPage();
            Browser.CloseNewWindow();
            Browser.SwitchToParentWindow();
            Assert.IsTrue(TermsConditions, "I should navigate on Terms and Conditions page when click on Terms and Conditions link");
        }

        [When(@"I click on Download Credit Card Authorization Form link")]
        public void WhenIClickOnDndCredCrdAuthFmLink()
        {
            Pages.Payment.clickDwnldCredtCrdAuthorForm();
        }


        [When(@"I enter valid details on Credit Card Authorization signing form")]
        public void ienterVldDataOnCrdtCrdAuthSigninForm(Table table)
        {
            Browser.SwitchToNewWindow();
            Browser.waitForImplicitTime(15);
            String email = table.Rows[0]["Email Address"];
            Pages.Payment.sendEmailonCredtCardAuthSigninForm(email);
            String guestName = table.Rows[0]["Guest Name"];
            Pages.Payment.sendGuestNameonCredtCardAuthSigninForm(guestName);
            Pages.Payment.enterArrivalDate();

        }

        [When(@"I click on Submit button")]
        public void WhenIClickOnSubmitBtn()
        {
            Pages.Payment.clickSubmitBtn();
        }

        [Then(@"I should see Credit Card Authorization Form")]
        public void ThenIShouldseeCeditCardAuthorizationForm()
        {
            Boolean CredtCardAuthForm = Pages.Payment.VerifyCeditCardAuthorizationForm();
            Browser.CloseNewWindow();
            Browser.SwitchToParentWindow();
            Assert.IsTrue(CredtCardAuthForm, "I should see Credit Card Authorization Form");
        }

        [Then(@"I should see correct first name")]
        public void ThenIShouldSeeCorrectFirstName(Table table)
        {
            String firstName = table.Rows[0]["First Name"];
            Boolean FirstName = Pages.Payment.verifyfirstName(firstName);
            Assert.IsTrue(FirstName, "I should see Correct First Name");
        }

        [Then(@"I should see correct last name")]
        public void ThenIShouldSeeCorrectlastName(Table table)
        {
            String lastName = table.Rows[0]["Last Name"];
            Boolean LastName = Pages.Payment.verifylastName(lastName);
            Assert.IsTrue(LastName, "I should see Correct Last Name");
        }

        [Then(@"I should see correct phone number")]
        public void ThenIShouldSeeCorrectPhNum(Table table)
        {
            String phNum = table.Rows[0]["Phone Number"];
            Boolean PhNum = Pages.Payment.verifyphoneNumber(phNum);
            Assert.IsTrue(PhNum, "I should see Correct Phone Number");
        }

        [Then(@"I should see correct email address")]
        public void ThenIShouldSeeCorrectEmailAdress(Table table)
        {
            String emailAdrss = table.Rows[0]["Email Address"];
            Boolean EmailAddress = Pages.Payment.verifyEmailAddress(emailAdrss);
            Assert.IsTrue(EmailAddress, "I should see Correct Email Address");
        }

        [Then(@"I should see correct billing address")]
        public void ThenIShouldSeeCorrectBillingAdress(Table table)
        {
            String billAdrss = table.Rows[0]["Billing Address"];
            Boolean BillingAddress = Pages.Payment.verifyBillingAdrss(billAdrss);
            Assert.IsTrue(BillingAddress, "I should see Correct Billing Address");
        }

        [Then(@"I should see correct postal code")]
        public void ThenIShouldSeeCorrectPostalCode(Table table)
        {
            String pstCode = table.Rows[0]["Postal Code"];
            Boolean PostalCode = Pages.Payment.verifyPostalCode(pstCode);
            Assert.IsTrue(PostalCode, "I should see Correct Postal Code");
        }

        [Then(@"I should see correct city")]
        public void ThenIShouldSeeCorrectcity(Table table)
        {
            String city = table.Rows[0]["City"];
            Boolean City = Pages.Payment.verifyCity(city);
            Assert.IsTrue(City, "I should see Correct City");
        }

        [Then(@"I should see correct State")]
        public void ThenIShouldSeeCorrectState(Table table)
        {
            String state = table.Rows[0]["State"];
            Boolean State = Pages.Payment.verifyState(state);
            Assert.IsTrue(State, "I should see Correct State");
        }

        [Then(@"I should see correct country")]
        public void ThenIShouldSeeCorrectCountry(Table table)
        {
            String country = table.Rows[0]["Country"];
            Boolean Country = Pages.Payment.verifyCountry(country);
            Assert.IsTrue(Country, "I should see Correct Country");
        }

        [Then(@"I should see 'Water Park Passes' field under cost summary section")]
        public void ThenIShouldSeeWaterParkPassSection()
        {
            Boolean WatrPakPass = Pages.Payment.verifyWaterParkPassesSection();
            Assert.IsTrue(WatrPakPass, "I should see 'Water Park Passes' field under cost summary section");
        }
        [When(@"I click on paypal radio button")]
        public void WhenIClickOnPaypalRadioButton()
        {
            Pages.Payment.clickOnPaypalRadioBtn();
        }

        [Then(@"I should see pay with paypal button")]
        public void ThenIShouldSeePayWithPaypalBtn()
        {
            Boolean PaypalBtn = Pages.Payment.verifyPayWithPayPalBtn();
            Assert.IsTrue(PaypalBtn, "I should see pay with paypal button");
        }

        [When(@"I click on Affirm link under cost summary")]
        public void WhenIClickOnAffirmlink()
        {
            Pages.Payment.clickAffirmLkUnderCostSummary();
        }

        [When(@"I click on Affirm link under Suite")]
        public void WhenIClickOnAffirmlinkOnSuite()
        {
            Pages.Payment.clickAffirmLkOnSuitePage();
        }
        

        [Then(@"I should see Affirm sandbox pop up open")]
        public void ThenIShouldSeeAffirmSandboxPopUpOpen()
        {
            Boolean AffirmSandbox = Pages.Payment.verifyAffirmsandboxpopup();
            Assert.IsTrue(AffirmSandbox, "I should see Affirm sandbox pop up open");
        }

        [Then(@"I should see I Agree check box displayed")]
        public void ThenIShouldSeeIAgreecheckBox()
        {
            Boolean IAgree = Pages.Payment.verifyIAgreeCheckBox();
            Assert.IsTrue(IAgree, "I should see I Agree check box");
        }
        [When(@"I entered postal code")]
        public void enterPostalCode(Table table)
        {
            String postalCode = table.Rows[0]["Postal Code"];
            Pages.Payment.enterPostalCode(postalCode);
        }
        [Then(@"I should not see I Agree check box")]
        public void ThenIShouldNotSeeIAgreecheckBox()
        {
            Boolean IAgree = Pages.Payment.verifyIAgreeCheckBox();
            Assert.IsFalse(IAgree, "I should not see I Agree check box");
        }

        [When(@"I click on Contact Info Edit link")]
        public void WhenIClickOnContactInfoEdit()
        {
            Pages.Payment.clickONContactInfoEdit();
        }

        [When(@"I click on billing Info Edit link")]
        public void WhenIClickOnbillingInfoEdit()
        {
            Pages.Payment.clickONBillingInfoEdit();
        }

        [Then(@"I should see dont miss section on Payment page")]
        public void ThenIShouldSeeDontSee()
        {
            Boolean dontSee = Pages.Payment.verifyDontMissOutSec();
            Assert.IsTrue(dontSee, "I should see dont miss section on Payment page");
        }
        
    }
}
