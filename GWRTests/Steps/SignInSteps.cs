﻿using GWRFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class SignInSteps
    {
        

        [When(@"I Sign In to Application")]
        public void iSignInToApplication(Table table)
        {
            String email = table.Rows[0]["Email Address"];
            String pass = table.Rows[0]["Password"];
            Pages.PropertyHome.SignInToAppBeforeCheckLogOut(email, pass);            
        }

        [When(@"I Sign In to Application with newly created Email")]
        public void iSignInToApplicationCretdEmail(Table table)
        {
            //String email = table.Rows[0]["Email Address"];
            String pass = table.Rows[0]["Password"];
            Pages.PropertyHome.SignInToAppBeforeCheckLogOut(SignInPage.emailIdForLogin, pass);
        }
        
        [When(@"I Sign In to Application with newly created Email and invalid password")]
        public void iInvalidAttemptsToApplicationCretdEmail(Table table)
        {
            //String email = table.Rows[0]["Email Address"];
            String pass = table.Rows[0]["Password"];
            Pages.PropertyHome.InvalidAttmpt5SignInToAppBeforeCheckLogOut(SignInPage.emailIdForLogin, pass);
        }

        [When(@"I Sign In to Application with newly created Password")]
        public void iSignInToApplicationCretdPasswrd(Table table)
        {
            //String email = table.Rows[0]["Email Address"];
            String pass = table.Rows[0]["Password"];
            Pages.PropertyHome.SignInToAppWithNewPass(SignInPage.emailIdForLogin, pass);
        }
        

        [When(@"I create an account to Application")]
        public void iSignUpToApplication(Table table)
        {
            //String email = table.Rows[0]["Email Address"];
            //String pass = table.Rows[0]["Password"];
            Pages.PropertyHome.closeDealsSignUp();
            Pages.PropertyHome.clickSignIn();
            Pages.SignIn.clickCreateAccountBtn();

            String firstName = table.Rows[0]["First Name"];
            Pages.CreateAccount.enterFirstNameTextbox(firstName);

            String lastName = table.Rows[0]["Last Name"];
            Pages.CreateAccount.enterLastNameTextbox(lastName);

            //String postalCode = table.Rows[0]["Postal Code"];
            Pages.CreateAccount.enterPostalCodeTextbox();

            String emailId = Generator.EmailAddress.GenerateEmail(table.Rows[0]["Email"]);
            Pages.CreateAccount.enterEmailIdTextbox(emailId);

            String password = table.Rows[0]["Password"];
            Pages.CreateAccount.enterPasswordTextbox(password);

            SignInPage.emailIdForLogin = Browser.getTextboxValue(Pages.CreateAccount.emailIdTextbox);
            //Browser.scrollVerticalBy("500");
            //Pages.PropertyHome.SignInToAppBeforeCheckLogOut(email, pass);
        }

        [When(@"I Sign In to Application but not from Home Page")]
        public void iSignInToApplicationFromOtherPags(Table table)
        {
            String email = table.Rows[0]["Email Address"];
            String pass = table.Rows[0]["Password"];
            Pages.PropertyHome.SignInToAppBeforeCheckLogOutfromOtherpage(email, pass);
        }

        

        [When(@"I enter valid login details")]
        public void WhenIEnterValidLoginDetails(Table table)
        {
            String email = table.Rows[0]["Email"];
            Pages.SignIn.sendEmail(email);

            String password = table.Rows[0]["Password"];
            Pages.SignIn.sendPass(password);
        }

        [When(@"I click Sign In button")]
        public void WhenIClickSignInButton()
        {
            Pages.SignIn.clickSignInbtn();
        }

        [When(@"I click Sign In pop up close link")]
        public void WhenIClickSignInCloseLink()
        {
            Pages.SignIn.closeSingInPopUp();
        }        

        [When(@"I click on Sign In with Google button")]
        public void WhenIClickSignInWithGoogleButton()
        {
            Pages.SignIn.clickSignInWithGoogleBtn();
        }

        [When(@"I click on Sign In with Facebook button")]
        public void WhenIClickSignInWithFacebookButton()
        {
            Pages.SignIn.clickContinueWithFacebookBtn();
        }

        [When(@"I login with valid details using google sign in")]
        public void WhenIEnterValidLoginDetailsOnGoogle(Table table)
        {
            String email = table.Rows[0]["Email"];
            String password = table.Rows[0]["Password"];
            Pages.SignIn.completeLoginStepsOnGoogle(email, password);
        }

        [When(@"I enter valid login details on Facebook")]
        public void WhenIEnterValidLoginDetailsOnFacebook(Table table)
        {
            String email = table.Rows[0]["Email"];
            String password = table.Rows[0]["Password"];
            Pages.SignIn.completeLoginStepsOnFacebook(email, password);
        }

        [Then(@"I should see User locked message on Sign In pop up")]
        public void ThenIShouldSeeUserLockedMsg()
        {
            Boolean result = Pages.SignIn.verifyLockAcMessage();
            Assert.IsTrue(result, "I should see User locked message on Sign In pop up");
        }
        
        [Then(@"I should see User locked message on Sign In pop up after first invalid attempt")]
        public void ThenIShouldSeeUserLockedMsgFrstInvalattmpts()
        {
            Boolean result = Pages.SignIn.verifyLockAcMessageForFirstMessage();
            Assert.IsTrue(result, "I should see User locked message on Sign In pop up after first invalid attempt");
        }

        [Then(@"I should receive Suspicious Account Activity email")]
        public void ThenIShouldReceiveSuspiciosEmail()
        {
            String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
            Boolean emailReceived = ReadEmail.CheckEmail.CheckAcountLockEmail(emailMaxCounter);
            Assert.IsTrue(emailReceived, "I should receive Suspicious Account Activity email");
        }

        [Then(@"I should click on unlock link Suspicious Account Activity email")]
        public void ThenIShouldGetUnlockLinkFromEmail()
        {
            String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
            Boolean emailReceived = ReadEmail.CheckEmail.CheckAcountLockEmailandGetUnlockLink(emailMaxCounter);
            Assert.IsTrue(emailReceived, "I should click on unlock link Suspicious Account Activity email");
        }
        
        [Then(@"I should click on Reset link Suspicious Account Activity email")]
        public void ThenIShouldGetResetLinkFromEmail()
        {
            String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
            Boolean emailReceived = ReadEmail.CheckEmail.CheckAcountLockEmailandGetResetLink(emailMaxCounter);
            Assert.IsTrue(emailReceived, "I should click on Reset link Suspicious Account Activity email");
        }
        
        [Then(@"I should see success message after password reset.")]
        public void ThenIShouldSeeSuccessMsgAfterPassReset()
        {
            Boolean result = Pages.SignIn.verifySuccessMsgAfterResetPass();
            Assert.IsTrue(result, "I should see success message after password reset.");
        }

        [Then(@"I should Delete email")]
        public void ThenIShouldDeleteEmail()
        {
            String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
            Boolean emailReceived = ReadEmail.CheckEmail.DeleteEmail(emailMaxCounter);
            Assert.IsTrue(emailReceived, "I should Delete email");
        }
    }
}
