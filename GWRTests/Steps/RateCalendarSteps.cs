﻿using GWRFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class RateCalendarSteps
    {
        [When(@"I click on 'Rate Calendar' link")]
        public void whenIclickRateCalLink()
        {
            Pages.rateCalendar.clickOnRateCalLink();
        }

        [When(@"I click on 'Rate Calendar' link with suite option")]
        public void whenIclickRateCalLinkOption()
        {
            Pages.rateCalendar.clickOnRateCalLinkWithSuiteOption();
        }
        
        [Then(@"I should see 'CONTINUE TO PACKAGE' button disabled")]
        public void thneContToPackgDisabldBtn()
        {
            Boolean continueToPackgBtn = Pages.rateCalendar.verifyDissabledContToPackgBtn();
            Assert.IsTrue(continueToPackgBtn, "User able to see 'CONTINUE TO PACKAGE' button disabled.");
        }

        [When(@"I click on 'Rate Calendar Close' link")]
        public void whenIclickRateCalCloseLink()
        {
            Pages.rateCalendar.clickOnCloseRateCalLink();
        }

        [Then(@"I should see 'Rate Calendar Close' link")]
        public void thenIShouldSeeRateCalCloseLink()
        {
            Pages.rateCalendar.seeCloseRateCalLink();
        }

        [Then(@"I should see valid offercode on Rate calendar")]
        public void thneValidOfferCodeOnRateCal(Table table)
        {
            String offerCode = table.Rows[0]["Offer Code"];
            Boolean offerCodeRateCal = Pages.rateCalendar.verifyOfferCodeIsDisplayed(offerCode);
            Assert.IsTrue(offerCodeRateCal, "User able to see valid offercode on Rate calendar.");
        }

        [Then(@"I should see valid suite name on Rate calendar")]
        public void thneValidSuieNameRateCal()
        {
            Boolean suiteNameRateCal = Pages.rateCalendar.verifySuiteNameIsDisplayed();
            Assert.IsTrue(suiteNameRateCal, "User able to see valid suite name on Rate calendar.");
        }

        [Then(@"I should see 'Best Available Rate' on Rate calendar")]
        public void thneBBAROnRateCal()
        {
            Boolean BBARRateCal = Pages.rateCalendar.verifyBBARIsDisplayed();
            Assert.IsTrue(BBARRateCal, "User able to see Best Available on Rate calendar.");
        }

        [Then(@"I click on 'Next Month Link' link and verify Month Change")]
        public void whenIclickNextMnthLink(Table table)
        {
            String mnthCnt = table.Rows[0]["Check Month Count"];
            int mtCnt = Int32.Parse(mnthCnt);
            Boolean mnthChange = Pages.rateCalendar.nextMnthVerfication(mtCnt);
            Assert.IsTrue(mnthChange, "User able to change Rate calendar month.");
        }

        [Then(@"I should see Not Available label for Dates")]
        public void ThenIShouldSeeNotAvalDatesLbl()
        {
            Boolean notAvailableDates = Pages.rateCalendar.notAvailableDatesLabel();
            Assert.IsTrue(notAvailableDates, "I should see Not available dates on Rate calendar");
        }

        [When(@"I click on Delete Icon link")]
        public void whenIclickDeleteink()
        {
            Pages.rateCalendar.deleteOffercode();
        }

        [When(@"I select Chek In and Check Out date")]
        public void whenIselectDate(Table table)
        {            
            String NMonth = table.Rows[0]["No Of Month"];
            int intNUmMnth = Int32.Parse(NMonth);
            Pages.rateCalendar.selectCheckInDate(intNUmMnth);
        }

        [When(@"I click on Continue to Package button")]
        public void whenIclickOnPackageBtn()
        {
            Pages.rateCalendar.clickOnConToPackgBtn();
        }

        [Then(@"I should see date available for selection")]
        public void ThenIShouldSeeDatesAvalSelection(Table table)
        {
            String NMonth = table.Rows[0]["No Of Month"];
            int intNUmMnth = Int32.Parse(NMonth);
            Boolean notAvailableDates = Pages.rateCalendar.verifyOfferCodeIsDisplayed(intNUmMnth);
            Assert.IsTrue(notAvailableDates, "I should see dates available on Rate calendar for selection");
        }

        [When(@"I observe Check In and Check out Date")]
        public void WhenIObserveCheckInAndCheckOutDate()
        {
            Pages.rateCalendar.getCheckInDate();
            Pages.rateCalendar.getCheckOutDate();
        }

        [When(@"I observe Suite Price on Rate Calendar")]
        public void WhenIObserveSuitePrice()
        {
            Pages.rateCalendar.getSuitePriceOnRateCalendar();
        }

        [When(@"I click on sold out suite 'Rate Calendar' link")]
        public void whenIclickSoldOutSuiteRateCalLink()
        {
            Pages.rateCalendar.clickOnSoldOutSuiteRateCalLink();
        }

    }
}
