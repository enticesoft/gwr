﻿using System;
using TechTalk.SpecFlow;
using Dynamitey.DynamicObjects;
using GWRFramework;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading;

namespace GWRTests
{
    [Binding]
    public class MyRecentSearchesPageSteps
    {
        [When(@"I am logged in with valid inputs")]
        public void WhenIAmLoggedInWithValidInputs(Table table)
        {
            if (Pages.MyRecentSearchesPage.isCloseDealPopup())
            {
                Pages.PropertyHome.closeDealsSignUp();
            }

            if (Pages.MyRecentSearchesPage.isNotSignIn())
            {
                Pages.PropertyHome.clickSignIn();

                String email = table.Rows[0]["Email"];
                Pages.SignIn.sendEmail(email);

                String pass = table.Rows[0]["Password"];
                Pages.SignIn.sendPass(pass);

                Pages.SignIn.clickSignInbtn();
            }

        }
        
        [When(@"I search suite")]
        public void WhenISearchSuite(Table table)
        {
            
          
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

           

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

            Pages.Suite.clickBookNowBtn();

            Boolean SuiteRoom = Pages.Suite.suitepageDisplayed();
            Assert.IsTrue(SuiteRoom, "Suite Room is displayed on suite page.");
           
        }
        
        [When(@"I click on My Recent Searches page link")]
        public void WhenIClickOnMyRecentSearchesPageLink()
        {
            //   Thread.Sleep(2000);
            
            Pages.MyRecentSearchesPage.clickOnMyRecentSearchesLink();
         
        }

        [Then(@"I should see correct Guest count as recently search")]
        public void ThenIShouldSeeCorrectGuestCountAsRecentlySearch(Table table)
        {
            Boolean IsGuestCount = Pages.MyRecentSearchesPage.VerifyGuestCount(table);          
            Assert.IsTrue(IsGuestCount, "I see correct guest count on Recent Search page.");
        }

        [Then(@"I Should see Make My reservation Link")]
        public void ThenIShouldSeeMakeMyReservationLink()
        {          
           Boolean DeletedAllSearches = Pages.MyRecentSearchesPage.seeMakeMyReservationLink();
           /* Pages.PropertyHome.clickToSignOut();
            Thread.Sleep(2000);
            Pages.MyRecentSearchesPage.navigatePropertyHomePage();*/
            Assert.IsTrue(DeletedAllSearches, "I am Able to delete all searches");

        }
        [When(@"I click on Delete All Searches button")]
        public void WhenIClickOnDeleteAllSearchesButton()
        {
         
            Pages.MyRecentSearchesPage.DeleteAllSearches();
        }
        [When(@"I click on Delete button of Recent Search")]
        public void WhenIClickOnDeleteButtonOfRecentSearch()
        {
            Pages.MyRecentSearchesPage.DeleteOneRecentSearch();
          

           
        }
        [Then(@"I should not see deleted search item")]
        public void ThenIShouldNotSeeDeletedSearchItem()
        {
            
            Boolean DeletedAllSearches = Pages.MyRecentSearchesPage.seeMakeMyReservationLink();

        /*    Pages.PropertyHome.clickToSignOut();
            Thread.Sleep(2000);

            Pages.MyRecentSearchesPage.navigatePropertyHomePage();*/

            Assert.IsTrue(DeletedAllSearches, "I am Able to delete recent search");
        }

        [When(@"I delete recently added all searches")]
        public void WhenIDeleteRecentlyAddedAllSearches()
        {
            Pages.MyRecentSearchesPage.DeleteAllSearches();
        }

        [When(@"I click on Make Reservation Link")]
        public void WhenIClickOnMakeReservationLink()
        {
            Pages.MyRecentSearchesPage.clickOnMakeReservationsLink();

        }

        [When(@"I click on Continue search link")]
        public void WhenIClickOnContinueSearchLink()
        {
            Pages.MyRecentSearchesPage.ClickContinueSearchLnk();
        }

        [Then(@"I should see Suite page")]
        public void ThenIShouldSeeSuitePage()
        {
            
              Boolean ISContinueSrchWorks =  Pages.Suite.suitepageDisplayed();

         /*   Pages.PropertyHome.clickToSignOut();
            Thread.Sleep(2000);

            Pages.MyRecentSearchesPage.navigatePropertyHomePage();*/

            Assert.IsTrue(ISContinueSrchWorks, "User is able to navigate on Suite Page");
        }



        [When(@"I click Update your stay Button '(.*)' times")]
        public void WhenIClickUpdateYourStayButtonTimes(int p0)
        {
            Pages.MyRecentSearchesPage.IsearchSuiteForMultiTime(p0);
        }


        [Then(@"I should see recent seraches count as '(.*)'")]
        public void ThenIShouldSeeRecentSerachesCountAs(int Count)
        {
            Boolean ISSearchCnt = Pages.MyRecentSearchesPage.VerifyRecentSearchCnt(Count);
            //Pages.PropertyHome.clickToSignOut();
           // Thread.Sleep(2000);

           // Pages.MyRecentSearchesPage.navigatePropertyHomePage();
            Assert.True(ISSearchCnt, "i should see Count " + Count);


        }

        [When(@"I search second time for suite")]
        public void WhenISearchSecondTimeForSuit(Table table)
        {
            String futureCheckInDate = DateTime.Today.AddDays(60).ToString("MM/dd/yyyy");
            String newFuturedate = futureCheckInDate.Replace("-", "/");
            Pages.PropertyHome.enterCheckInDate(newFuturedate);

            String futureCheckoutDate = DateTime.Today.AddDays(61).ToString("MM/dd/yyyy");
            String newFutureOutdate = futureCheckoutDate.Replace("-", "/");
            Pages.PropertyHome.clearCheckOutDate();
            Pages.PropertyHome.enterCheckOutDate(newFutureOutdate);

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String kids1 = table.Rows[0]["Kids"];
            int kids = Int32.Parse(kids1);
            String kids1age = table.Rows[0]["Kids1Age"];
            Pages.Suite.SelectKids(kids, kids1age);



            Pages.Suite.clickUpdateyourStayBtn();
        }


        [When(@"I Click data on Recent seraches list")]
        public void WhenIClickDataOnRecentSerachesList()
        {
            Pages.MyRecentSearchesPage.clickOnRecentSrcDropDown();
            Pages.MyRecentSearchesPage.SelectOptionFromRecentList();
        }

        [Then(@"I should see updated data in Booking widget")]
        public void ThenIShouldSeeUpdatedDataInBookingWidget()
        {
           Boolean UpdatedData =  Pages.MyRecentSearchesPage.VerifyUpdatedSearch();
            Assert.IsTrue(UpdatedData, "I should see updated data in Booking widget");


        }
        [When(@"I select Another property location")]
        public void WhenISelectAnotherPropertyLocation(Table table)
        {
            Pages.MyRecentSearchesPage.selectLocation(table);
               
        }

        [When(@"I click on continue search button for not selected property")]
        public void WhenIClickOnContinueSearchButtonForNotSelectedProperty()
        {
            Pages.MyRecentSearchesPage.ClickContinueSearchLnk();
        }

        [Then(@"I should see property selected")]
        public void ThenIShouldSeePropertySelected(Table table)
        {
            Boolean IsCurrentProperty = Pages.MyRecentSearchesPage.VerifyCurrentSelectedProperty();
          /*  Pages.PropertyHome.clickToSignOut();
            Thread.Sleep(2000);

            Pages.MyRecentSearchesPage.navigatePropertyHomePage();*/
            Assert.IsTrue(IsCurrentProperty, "Current Property updated is correct");

        }
        [When(@"I click on Continue search button")]
        public void WhenIClickOnContinueSearchBtn()
        {
            Pages.MyRecentSearchesPage.ClickContinueSearchBtn();
        }

        [Then(@"I should see Make My reservation Link when no any recent searches")]
        public void ThenIShouldSeeMakeMyReservationlink()
        {
            Boolean MakeReservationLink = Pages.MyRecentSearchesPage.seeMakeMyReservationLink();
            Assert.IsTrue(MakeReservationLink, "I should see Make My reservation Link when no any recent searches");

        }

    }
}
