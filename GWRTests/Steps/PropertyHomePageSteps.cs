﻿using GWRFramework;
using GWRFramework.TestData;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace GWRTests.Steps
{
    [Binding]
    public class PropertyHomePageSteps
    {


        [Then(@"I should see Booking Widget")]
        public void ThenIShouldSeeBookngWidget()
        {
            Boolean CheckInDate = Pages.PropertyHome.checkBookingWidgetDisplayed();
            Assert.IsTrue(CheckInDate, "Check In Date field is displayed on property page.");
        }

        [When(@"I click on Book Now button on Room Card")]
        public void WhenIClickBookNowOnRoomCard()
        {
            Pages.PropertyHome.closeDealsSignUp();
            Pages.PropertyHome.clickBookNowOnRoomCard();
        }

        [When(@"I click on Customer Service tile on Clickable Tiles section")]
        public void WhenIClickOnCustomerServiceTile()
        {
            Pages.PropertyHome.clickOnCustomerService();
        }

        [When(@"I scroll to Mini Map and click on Get Direction button")]
        public void WhenIScrollToMiniMapNClickGetDirections()
        {
            Pages.PropertyHome.closeDealsSignUp();
            Pages.PropertyHome.clickOnGetDirections();
        }

        [When(@"I enter valid details on Booking Widget")]
        public void WhenIEnterValidDetailsOnBookingWidget(Table table)
        {
            Pages.Suite.selectCheckIn();
            Pages.Suite.selectCheckOut();

            String adults1 = table.Rows[0]["Adults"];
            int adults = Int32.Parse(adults1);
            Pages.Suite.SelectAdults(adults);

            String offerCode = table.Rows[0]["Offer Code"];
            Pages.Suite.enterOfferCodeTextbox(offerCode);

        }

        [When(@"I scroll down the page to bottom")]
        public void WhenIScrollDownToPageBottom()
        {
            Browser.scrollDownToBottomOfPage();
        }

        [When(@"I scroll up the page to Booking Widget")]
        public void WhenIScrollUpToBookingWidget()
        {
            Pages.PropertyHome.scrollToBookingWidget();
        }

        [When(@"I click on View Offer button")]
        public void WhenIClickViewOfferBtn()
        {
            Pages.Suite.clickCookiesClose();
            Pages.PropertyHome.clickViewOfferLink();
        }
        
        [When(@"I click Book Now button")]
        public void WhenIClickBookNowBtn()
        {
            Pages.PropertyHome.clickBookNowBtn();
        }

        [When(@"I click on Day pass tile")]
        public void WhenIClickDayPassTile()
        {
            Pages.PropertyHome.clickOnDayPassTile();
        }

        [Then(@"I should navigate to Plan page")]
        public void ThenIShouldNavigateToPlanPage()
        {
            Boolean CheckInDate = Pages.PropertyHome.checkBookingWidgetDisplayed();
            Assert.IsTrue(CheckInDate, "Check In Date field is displayed on plan page.");
        }

        [Then(@"I should navigate on Customer Service page")]
        public void ThenIShouldNavigateToCustomerServicePage()
        {
            Boolean CustomerServicePage = Pages.PropertyHome.checkCustomerServicePage();
            Assert.IsTrue(CustomerServicePage, "Customer Service page is displayed.");
        }

        [Then(@"I should navigate to the Google Map")]
        public void ThenIShouldNavigateToGoogleMap()
        {
            Boolean gMap = Pages.PropertyHome.checkGoogleMapInNewTab();
            Assert.IsTrue(gMap, "User navigated to Google Map.");
        }

        [Then(@"I should see data which was entered on Booking Widget is retained")]
        public void ThenIShouldSeeDataRetainedInBookingWidget(Table table)
        {
            String checkInDateExpected = DataAccess.getData(3, 2);
            String checkOutDateExpected = DataAccess.getData(4, 2);
            String noOfAdultsExpected = table.Rows[0]["Adults"] + " Guests";
            String offerCodeExpected = table.Rows[0]["Offer Code"];

            Pages.Suite.getCheckInDateText();
            String checkInDateActual = SuitePage.checkIndtTxt;
            
            Pages.Suite.getCheckOutDateText();
            String checkOutDateActual = SuitePage.checkOutdtTxt;

            String noOfAdultsActual = Pages.Suite.getGuestsText();
            String offerCodeActual = Pages.Suite.getOfferCodeText();

            Assert.IsTrue(checkInDateActual.Equals(checkInDateExpected), "User should see Check In date is retained.");
            Assert.IsTrue(checkOutDateActual.Equals(checkOutDateExpected), "User should see Check Out date is retained.");
            Assert.IsTrue(noOfAdultsActual.Equals(noOfAdultsExpected), "User should see number of Guests is retained.");
            Assert.IsTrue(offerCodeActual.Equals(offerCodeExpected), "User should see Offer Code is retained.");
        }

        [Then(@"I should navigate to Special Deal Details page")]
        public void ThenIShouldNavigateToSpecialDealsPage()
        {
            Boolean dealsPage = Pages.Deals.checkDealsPageDisplayed();
            Assert.IsTrue(dealsPage, "User navigated to Special Deal Details page.");
        }

        [Then(@"I should navigate to Suite Page")]
        public void ThenIShouldNavigateToSuitePage()
        {
            Boolean SuiteRoom = Pages.Suite.suitepageDisplayed();
            Assert.IsTrue(SuiteRoom, "User navigates to Suite Page.");
        }

        [Then(@"I should navigate to Day Pass page")]
        public void ThenIShouldNavigateToDayPassPage()
        {
            Boolean dayPass = Pages.DayPass.checkDayPassPageDisplayed();
            Assert.IsTrue(dayPass, "User navigates to Day Pass Page.");
        }

        [When(@"I enter Canadian postal code on Lead Gen pop-up")]
        public void WhenIEnterCanadianPostalCode(Table table)
        {
            Pages.PropertyHome.waitForDealsSignUpPopup();
            String PostalCode = table.Rows[0]["Postal Code"];
            Pages.PropertyHome.EnterPostalCode(PostalCode);
        }

        [When(@"I Check 'I Agree to receive email' checkbox")]
        public void WhenICheckCheckbox()
        {
            Pages.PropertyHome.CheckCheckbox();
        }

        [Then(@"I should able to Check checkbox")]
        public void ThenIShouldCheckCheckBox()
        {
            Boolean Result = Pages.PropertyHome.IsCheckBoxChecked();
            Assert.IsTrue(Result, "I should able to Check checkbox");
        }

        [When(@"I Uncheck 'I Agree to receive email' checkbox")]
        public void WhenIUncheckCheckbox()
        {
            Pages.PropertyHome.UncheckCheckbox();
        }
        [Then(@"I should able to Uncheck checkbox")]
        public void ThenIShouldUncheckCheckBox()
        {
            Boolean Result = Pages.PropertyHome.IsCheckBoxUnchecked();
            Assert.IsFalse(Result, "I should able to Uncheck checkbox");
        }

        [When(@"I click on Book Your Stay link under Header section")]
        public void WhenIClickOnBookYourStayLink()
        {
            //Pages.PropertyHome.waitForDealsSignUpPopup();
            //Pages.PropertyHome.closeDealsSignUp();
            Pages.PropertyHome.ClickOnBookYourStay();
        }

        [Then(@"I should navigate on 'Plan Your Stay' section")]
        public void ThenIShouldNavigateOnPlanYourStaySection()
        {
            Boolean Result = Pages.PropertyHome.IShouldNavigateOnPlanYourStaySection();
            Assert.IsTrue(Result, "I should navigate on 'Plan Your Stay' section");
        }

        [When(@"I click on 'Property dropdown' link under header section")]
        public void WhenIClickOnPropertyDropdownLink()
        {
            //Pages.PropertyHome.waitForDealsSignUpPopup();
            //Pages.PropertyHome.closeDealsSignUp();
            Pages.PropertyHome.ClickOnPropertyDropdown();
        }

        [When(@"I click on any property link")]
        public void WhenIClickOnAnyPropertyLink(Table table)
        {
            String propertyName = table.Rows[0]["propertyName"];
            Pages.PropertyHome.ClickOnAnyPropertyLink(propertyName);
        }

        [Then(@"I should navigate to that specific property")]
        public void ThenIShouldNavigateToPerticularPropertyPage(Table table)
        {
            String propertyName = table.Rows[0]["propertyName"];
            Boolean Result = Pages.PropertyHome.IShouldNavigateToPerticularPropertyPage(propertyName);
            Assert.IsTrue(Result, "I should navigate to that specific property");
        }

        [When(@"I click on Customer Service tile under Header section")]
        public void WhenIClickOnCustomerServiceUnderHeader()
        {
            Pages.PropertyHome.clickOnCustomerServiceFromHeader();
        }

    }
}
