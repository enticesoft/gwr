﻿using TechTalk.SpecFlow;
using GWRFramework;
using System;
using NUnit.Framework;
using System.Threading;
using GWRFramework.TestData;

namespace GWRTests.Steps
{
    [Binding]
    public class SearchYourReservationSteps
    {

        public static String confirmationNum;

        [When(@"I click on Find Your Reservation link under header section")]
        public void WhenIClickOnFindYourReservationLinkUnderHeaderSection()
        {
            Pages.PropertyHome.closeDealsSignUp();
            Pages.SearchYourReservation.clickOnFindYourReservationLink();
        }

        [When(@"I enter valid Day pass confimation number and last name")]
        public void WhenIEnterValidDayPassConfimationNumAndLstName(Table table)
        {
            confirmationNum = DataAccess.getReservationData(1, 2);
            Pages.SearchYourReservation.sendConfirmationNum(confirmationNum);
            
            String lastName = table.Rows[0]["Last Name"];
            Pages.SearchYourReservation.sendLastName(lastName);
        }

        [When(@"I enter valid Suite confimation number and last name")]
        public void WhenIEnterValidSuiteConfimationNumAndLstName(Table table)
        {
            confirmationNum = DataAccess.getReservationData(2, 2);
            Pages.SearchYourReservation.sendConfirmationNum(confirmationNum);

            String lastName = table.Rows[0]["Last Name"];
            Pages.SearchYourReservation.sendLastName(lastName);
        }

        [When(@"I enter valid Suite confimation number and last name to verify Property redirection")]
        public void WhenIEnterValidSuiteConfimationNumAndLstNameToverifyRedirection(Table table)
        {
            Pages.SearchYourReservation.sendConfirmationNum(NewConfirmationPage.afterSplitResNum);

            String lastName = table.Rows[0]["Last Name"];
            Pages.SearchYourReservation.sendLastName(lastName);
        }

        [When(@"I search with valid Suite confimation number and last name")]
        public void WhenISearchValidSuiteConfimationNumAndLstName(Table table)
        {
            confirmationNum = DataAccess.getReservationData(2, 2);
            Pages.SearchYourReservation.sendConfirmationNum(NewConfirmationPage.afterSplitResNum);

            String lastName = table.Rows[0]["Last Name"];
            Pages.SearchYourReservation.sendLastName(lastName);
        }

        [When(@"I click on Find Your Reservation link at Top")]
        public void WhenIClickOnFindYourReservationLinkAtTop()
        {
            Pages.SearchYourReservation.ClickFindYourReservationtopLink();
        }


        [When(@"I enter valid Cabana confimation number and last name")]
        public void WhenIEnterValidCabanaConfimationNumAndLstName(Table table)
        {
            confirmationNum = DataAccess.getReservationData(3, 2);
            Pages.SearchYourReservation.sendConfirmationNum(confirmationNum);

            String lastName = table.Rows[0]["Last Name"];
            Pages.SearchYourReservation.sendLastName(lastName);
        }



        [When(@"I enter invalid confimation number and last name")]
        public void WhenIEnterInValidConfimationNumAndLstName(Table table)
        {
            String confirmationNum1 = table.Rows[0]["Confirmation Number"];
            Pages.SearchYourReservation.sendConfirmationNum(confirmationNum1);
            String lastName = table.Rows[0]["Last Name"];
            Pages.SearchYourReservation.sendLastName(lastName);
        }
       
        [When(@"I click on Search button")]
        public void WhenIClickOnSearchBtn()
        {
            Pages.SearchYourReservation.clickOnSearchBtn();   
        }

        [Then(@"I Should see error validation message")]
        public void ThenIShouldSeeErrorValidationMessage()
        {
           Boolean error = Pages.SearchYourReservation.verifyWeCoundNotFindReservationError();
            Assert.IsTrue(error, "I Should see error validation message when enter invalid confimation number on find your reservation pop up");
        }

        [Then(@"I should see correct reservation number on My reservation details page")]
        public void ThenIshouldSeeCorrectReserNum()
        {
            Boolean Result = Pages.SearchYourReservation.verifyCorrectConfirmationNum(confirmationNum);
            Assert.IsTrue(Result, "I Should see correct reservation number on My reservation details page");
        }

        [Then(@"I should see signup pop up")]
        public void ThenIShouldseeSignupPOPup()
        {
            Boolean SignUp = Pages.SearchYourReservation.verifySignupPopUp();
            Assert.IsTrue(SignUp, "I Should see SignUp pop up");
        }

    }
}
