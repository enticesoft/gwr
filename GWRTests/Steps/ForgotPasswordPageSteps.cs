﻿using GWRFramework;
using NUnit.Framework;
using System;
using System.Configuration;
using TechTalk.SpecFlow;

namespace GWRTests
{
    [Binding]
    public class ForgotPasswordPageSteps
    {
        [When(@"I click on forgot Password link")]
        public void WhenIClickOnForgotPasswordLink()
        {
            
            Pages.ForgotPasswordPage.IClickForgotPasswordLnk();
        }

        [When(@"I enter new password on Change password pop up")]
        public void WhenIEnterNewPass(Table table)
        {
            String password = table.Rows[0]["New Password"];
            Pages.ForgotPasswordPage.IEnterNewPass(password);
        }

        [When(@"I click on Change Password button")]
        public void WhenIClickOnChangePassBtn()
        {

            Pages.ForgotPasswordPage.ClickChangePasswordBtn();
        }

        [When(@"I click on Continue button")]
        public void WhenIClickOnContinueBtn()
        {
            Pages.ForgotPasswordPage.ClickContinueBtn();
        }

        [When(@"I enter valid Email Address of forgot password")]
        public void WhenIEnterValidEmailAddress()
        {
            //String email = table.Rows[0]["Email"];
            Pages.SignIn.sendEmail(SignInPage.emailIdForLogin);
        }
        
        [When(@"I click on RESET PASSWORD button")]
        public void WhenIClickOnRESETPASSWORDButton()
        {
            Pages.ForgotPasswordPage.ClickResetPassword();
        }

        [Then(@"I should receive Forgot password email")]
        public void ThenIShouldReceiveForGotPassEmail()
        {
            String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
            Boolean emailReceived = ReadEmail.CheckEmail.CheckForgotPassEmail(emailMaxCounter);
            Assert.IsTrue(emailReceived, "User received Forgot email Email from GWR.");
        }

        [When(@"I should receive Forgot password email and click Change Password button")]
        public void ThenIShouldReceiveForGotPassEmailClickChngePassBtn()
        {
            String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
            Boolean emailReceived = ReadEmail.CheckEmail.CheckForgotPassEmailandGetEmailLink(emailMaxCounter);
            Assert.IsTrue(emailReceived, "I should receive Forgot password email and click Change Password button");
        }

        
        /* [Then(@"I should receive email change password")]
         public void ThenIShouldReceiveEmailChangePassword()
         {
             String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
             Boolean emailReceived1 = ReadEmail.CheckEmail.CheckResetPasswordEmail(emailMaxCounter);
             //Boolean emailReceived = ReadEmail.CheckEmail.CheckWelcomeEmail("120");
             Assert.IsTrue(emailReceived1, "User received Reset Password Email from GWR.");
         }*/

        /*[When(@"I click on Forgot Password Link in mail")]
        public void WhenIClickOnForgotPasswordLinkInMail()
        {
            String emailMaxCounter = ConfigurationManager.AppSettings["EmailMaxCounter"];
             ReadEmail.CheckEmail.GetPasswordLink(emailMaxCounter);

        }*/

        [When(@"I Enter New Password")]
        public void WhenIEnterNewPassword()
        {
            Pages.ForgotPasswordPage.EnterNewPassword();
        }
        [When(@"I click on Reset Password Button")]
        public void WhenIClickOnResetPasswordButton()
        {
            Pages.ForgotPasswordPage.ClickChangePassBtn();
        }


    }
}
